# TruthEngine - DirectX11
The TruthEngine is a C++ real-time rendering engine that has different futures. This version of TruthEngine has used the DirectX11 API and HLSL 5.0 language for graphical objects and GPU codes.
The engine uses individual and its own classes and object wrappers for data process and management.
## Real-time Rendering
Forward Rendering and Deferred Shading are two real-time rendering techniques implemented in TE. HDR, Bloom, SSAO, Normal Mapping, Compute Shaders, Various shadow algorithms(PCF, PCSS, VSM, VSSM, MSM, and my own researched algorithm "OSSSS"), Gaussian Blur are some of the visual ability of the TE.
## Other Futures
• Animated Model Support (Skinned models)

• GUI

• GPU/CPU Profiling

• Take the advantage of latest standards of C++ language (C++11 / C++ 14 / C++17)
## Third party libraries
• Assimp: intermediate model importing

• IMGUI: engine's UI

• DirectXTex: textures importing

• DirectXMath: SSE supported math operations
