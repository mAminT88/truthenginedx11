#pragma once

#include "WindowContainer.h"

class RenderEngine;
class Model;
class SkinnedModel;

class Engine : WindowContainer
{
public:
	Engine();
	~Engine();

	bool Initialize(HINSTANCE hInstance, std::string window_title, std::string window_class, int width, int heigth);
	bool ProcessMessages();
	void Update();
	void Run();

	void ImportModel(const std::string& filepath);
	void ImportSkinnedModel(const std::string& filepath);
	HRESULT SelectFile(std::string& filePath, const COMDLG_FILTERSPEC* c_rgSaveTypes,const  int extensionCount);

	/*void SaveModels(const std::string& filePath);
	void LoadModels(const std::string& filePath);*/

private:
	std::unique_ptr<RenderEngine> mRenderer;
	/*std::vector<Model*> mBasicModels;
	std::vector<SkinnedModel*> mSkinnedModels;*/

	//bool ReleaseModels();
};
