#ifndef GAMETIMER_H
#define GAMETIMER_H

class GameTimer {
public:
	GameTimer();

	float TotalTime()const;
	float DeltaTime()const;

	void Start();
	void Reset();
	void Stop();
	void Tick();

	void CalculateFrameStats(HWND mainWindow, std::wstring mainWindowCaption);

private:
	double mSecondsPerCount;
	double mDeltaTime;

	__int64 mBaseTime;
	__int64 mPausedTime;
	__int64 mStoppedTime;
	__int64 mPrevTime;
	__int64 mCurrTime;

	bool mStopped;

};

#endif // !GameTIMER_H
