#pragma once


class COMException;

class ErrorLogger
{
public :
	static void Log(std::string msg);
	static void Log(HRESULT, std::string msg);
	static void Log(COMException& exception);
};