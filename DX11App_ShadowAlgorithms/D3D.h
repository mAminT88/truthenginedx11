#pragma once


namespace D3D
{
	void SetGlobalContextStates(ID3D11DeviceContext* context, bool Samplers = true, bool lights_ps = true, bool lights_vs = false);

	void XM_CALLCONV CalcFrustumSplits(FXMMATRIX ProjMatrix);
											  
}
