#include "stdafx.h"
#include "Globals.h"

namespace Globals
{

//--------------------------------------------------------------------------------------
// DirectX Globals
//--------------------------------------------------------------------------------------

	ComPtr<ID3D11Device> gDevice = nullptr;
	ComPtr<ID3D11DeviceContext> gContext = nullptr;
	ComPtr<ID3DUserDefinedAnnotation> gD3DUserDefinedAnnotation = nullptr;

	ComPtr<ID3D11Buffer> gVertexBuffer_Basic32 = nullptr;
	ComPtr<ID3D11Buffer> gVertexBuffer_Skinned = nullptr;
	ComPtr<ID3D11Buffer> gIndexBuffer = nullptr;

	std::vector<Vertex::Basic32> gVertecies_Basic32;
	std::vector<Vertex::Skinned> gVertecies_Skinned;
	std::vector<UINT> gIndecies;

	SwapChain gSwapChain;
	D3D11_VIEWPORT gViewPort;

	int gD3DComplieShaderFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;

	//--------------------------------------------------------------------------------------
	// Global units
	//--------------------------------------------------------------------------------------

	float gDt = 0.0f;

	int gClientWidth = 1920;
	int gClientHeight = 1080;

	int gCurrentFrame = 0;
	int gLastFrame = 0;

	float gShadowMapResolution = 4096.0f;
	float gShadowMapDXY = 1.0f / gShadowMapResolution;

	bool gAppPaused = false;

	//--------------------------------------------------------------------------------------
	// Global Window App
	//--------------------------------------------------------------------------------------

	HWND gHWND_Rendering;
	HWND gHWND_App;
	HINSTANCE gHInstance;

	POINT gLastMousePos;

	//--------------------------------------------------------------------------------------
	// Global Variable
	//--------------------------------------------------------------------------------------

	KeyboardClass gKeyboard;
	MouseClass gMouse;


	//--------------------------------------------------------------------------------------
	// Global Resource Managers
	//--------------------------------------------------------------------------------------

	ModelManager     gModelManager;
	CharacterManager gCharacterManager;


	//--------------------------------------------------------------------------------------
	// Global D3D App Resource
	//--------------------------------------------------------------------------------------

	IMesh* gSelectedMesh = nullptr;
	Camera gCamera_main;
	Lights gLights;

	
}
