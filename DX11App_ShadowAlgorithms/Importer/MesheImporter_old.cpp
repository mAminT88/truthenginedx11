//#include "stdafx.h"
//#include "MesheImporter.h"
//#include <strstream>
//
//
//Meshes::Meshes(VERTEX_TYPE _vType, D3D11_PRIMITIVE_TOPOLOGY _topology) {
//	VertexBuffer = nullptr;
//	IndexBuffer = nullptr;
//	mVertexType = _vType;
//	mPrimitiveTopology = _topology;
//}
//
//Meshes::~Meshes() {
//
//	FreeMemory();
//}
//
//void Meshes::BuildVertexBufferObjMesh(std::wstring FileName, ID3D11Device *device, XMFLOAT2 _billboardSize)
//{
//	FreeMemory();
//	switch (mPrimitiveTopology) {
//	case D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST:
//		switch (mVertexType)
//		{
//		case VERTEX_TYPE_BASIC32:
//			Build_Basic32VertexObjects(device, FileName);
//			break;
//		case VERTEX_TYPE_POINTBILLBOARD:
//			Build_BillboardVertexObjects(device, FileName, _billboardSize);
//			break;
//		default:
//			break;
//		}
//		break;
//	case D3D11_PRIMITIVE_TOPOLOGY_4_CONTROL_POINT_PATCHLIST:
//		switch (mVertexType)
//		{
//		case VERTEX_TYPE_BASIC32:
//			Build_Basic32QuadPatchObjects(device, FileName);
//			break;
//		default:
//			break;
//		}
//	}
//}
//
//UINT Meshes::AddVertex(UINT vertexIndex, Vertex::Basic32 vertex, std::vector<Vertex::Basic32> &allVertices, bool &newVertex)
//{
//	UINT index;
//	newVertex = true;
//
//	if (vertexIndex < mVertexChache.size())
//	{
//		CacheEntry* cache = mVertexChache[vertexIndex];
//		while (cache != nullptr)
//		{
//			Vertex::Basic32 v = allVertices[cache->index];
//			if (vertex.Normal.x == v.Normal.x && vertex.Normal.y == v.Normal.y && vertex.Normal.z == vertex.Normal.z)
//			{
//				newVertex = false;
//				index = cache->index;
//				break;
//			}
//			cache = cache->pNext;
//		}
//	}
//	if (newVertex)
//	{
//		allVertices.push_back(vertex);
//		index = allVertices.size() - 1;
//		while (vertexIndex >= mVertexChache.size())
//		{
//			mVertexChache.push_back(nullptr);
//		}
//		CacheEntry *pNewEntry = new CacheEntry;
//		pNewEntry->index = index;
//		pNewEntry->pNext = nullptr;
//
//		CacheEntry *pCurEntry = mVertexChache[vertexIndex];
//		if (pCurEntry == nullptr)
//		{
//			mVertexChache[vertexIndex] = pNewEntry;
//		}
//		else
//		{
//			while (pCurEntry->pNext != nullptr)
//			{
//				pCurEntry = pCurEntry->pNext;
//			}
//			pCurEntry->pNext = pNewEntry;
//		}
//	}
//	return index;
//}
//
//void Meshes::ImportMaterialLib(std::string filename)
//{
//	mMaterials.clear();
//	mMaterials.shrink_to_fit();
//
//	UINT i = 0;
//	std::string addr;
//	addr.append("Models/");
//	addr.append(filename);
//	std::ifstream fin(addr);
//	std::wstring s1 = L"The file: ";
//	std::wstring sfilename(filename.begin(), filename.end());
//	//std::copy(filename.begin(), filename.end(), sfilename.begin());
//	std::wstring s2 = L"was not found";
//	auto s = s1 + sfilename + s2;
//
//
//	if (!fin)
//	{
//		MessageBox(NULL, s.c_str(), L"error", 0);
//		return;
//	}
//
//	MatLib *m = new MatLib();
//	//m->DiffuseMapIndex = -1;
//
//	std::string command;
//	std::string ignore;
//
//	while (!fin.eof())
//	{
//		fin >> command;
//
//		if (command == "newmtl")
//		{
//			if (i != 0)
//			{
//				mMaterials.push_back(*m);
//				m = new MatLib();
//			}
//			std::string mname;
//			fin >> mname;
//			m->Name = mname;
//			i++;
//		}
//		else if (command == "Ns")
//		{
//			fin >> m->Mat.Specular.w;
//		}
//		else if (command == "Ks")
//		{
//			fin >> m->Mat.Specular.x >> m->Mat.Specular.y >> m->Mat.Specular.z;
//		}
//		else if (command == "Ka")
//		{
//			fin >> m->Mat.Ambient.x >> m->Mat.Ambient.y >> m->Mat.Ambient.z;
//		}
//		else if (command == "Kd")
//		{
//			fin >> m->Mat.Diffuse.x >> m->Mat.Diffuse.y >> m->Mat.Diffuse.z;
//		}
//		else if (command == "d")
//		{
//			fin >> m->Mat.Diffuse.w;
//		}
//		else if (command == "map_Kd")
//		{
//			fin >> m->DiffuseMap;
//		}
//		else if (command == "map_Ka")
//		{
//			fin >> m->AmbientMap;
//		}
//		else if (command == "map_Kd_isArray")
//		{
//			m->TextureType = TEXTURE_2D_ARRAY;
//		}
//		else {
//			fin.ignore(1000, '\n');
//		}
//	}
//	mMaterials.push_back(*m);
//}
//
//int Meshes::SearchMaterialIndex(std::string materialName)
//{
//	IsTransparent = false;
//	IsMirror = false;
//	bool founded = false;
//	int i = 0;
//	while (i < mMaterials.size())
//	{
//		if (mMaterials[i].Name == materialName)
//		{
//			IsTransparent = mMaterials[i].Mat.Diffuse.w >= 1.0f ? false : true;
//			founded = true;
//			break;
//		}
//		else {
//			i++;
//		}
//	}
//	if (founded)
//	{
//		return i;
//	}
//	return -1;
//}
//
//void Meshes::InitDiffuseMaps(ID3D11Device *device)
//{
//	for (size_t i = 0; i < mMaterials.size(); i++)
//	{
//		MatLib m = mMaterials[i];
//
//		std::wstring filepath(m.DiffuseMap.begin(), m.DiffuseMap.end());
//		Texture *TexObj = new Texture();
//		TexObj->InitTextureFromFile(device, filepath, m.TextureType);
//		mDiffuseMaps.push_back(*TexObj);
//	}
//}
//
//void Meshes::ReleaseDiffuseMaps()
//{
//	for (auto tex : mDiffuseMaps)
//	{
//		tex.Release();
//	}
//	mDiffuseMaps.clear(); mDiffuseMaps.shrink_to_fit();
//}
//
//void Meshes::FreeMemory()
//{
//	//Release Pointers
//	ReleaseCOM(VertexBuffer);
//	ReleaseCOM(IndexBuffer);
//
//	ReleaseDiffuseMaps();
//
//	//Release Vectors Memory
//	mGeometries.clear(); mGeometries.shrink_to_fit();
//	mGeometries_Transparent.clear(); mGeometries_Transparent.shrink_to_fit();
//	mGeometries_Mirror.clear(); mGeometries_Mirror.shrink_to_fit();
//	mMaterials.clear(); mMaterials.shrink_to_fit();
//	mVertexChache.clear(); mVertexChache.shrink_to_fit();
//}
//
//void Meshes::Build_Basic32VertexObjects(ID3D11Device *_device, std::wstring _filePath)
//{
//	std::ifstream fin(_filePath);
//	std::wstring s1 = L"Loading Obj File Failed: ";
//	std::wstring s2 = L" not founded";
//	auto s = s1 + _filePath + s2;
//
//	if (!fin)
//	{
//		MessageBox(0, s.c_str(), L"error", 0);
//	}
//
//	std::string ignore;
//	std::string command;
//	std::string currentMat;
//	char c;
//
//	UINT iv = 0;
//	UINT ii = 0;
//	UINT in = 0;
//	UINT it = 0;
//
//
//	UINT geoIndex = 0;
//	Geometry* g = new Geometry();
//
//	UINT vertexIndex;
//	int texIndex;
//	UINT normalIndex;
//
//	std::vector<Vertex::Basic32> allvertices;
//	std::vector<XMFLOAT3> normals;
//	std::vector<UINT> indices;
//	std::vector<XMFLOAT2> texCoords;
//
//	std::vector<Vertex::Simple> vertecies;
//
//	Vertex::Simple sVertex;
//	XMFLOAT3 normal;
//	XMFLOAT2 tex;
//
//	bool newVertex;
//
//
//	while (!fin.eof())
//	{
//		fin >> command;
//		if (command == "v")
//		{
//			fin >> sVertex.Pos.x >> sVertex.Pos.y >> sVertex.Pos.z;
//			vertecies.push_back(sVertex);
//			iv++;
//		}
//		else if (command == "vn")
//		{
//			fin >> normal.x >> normal.y >> normal.z;
//			normals.push_back(normal);
//			in++;
//		}
//		else if (command == "vt")
//		{
//			fin >> tex.x >> tex.y >> ignore;
//			texCoords.push_back(tex);
//			it++;
//		}
//		else if (command == "g")
//		{
//			if (geoIndex != 0)
//			{
//				g->indexCount = indices.size() - g->indexOffset;
//
//				BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));
//
//				if (IsMirror)
//				{
//					mGeometries_Mirror.push_back(*g);
//				}
//				else if (IsTransparent)
//				{
//					mGeometries_Transparent.push_back(*g);
//				}
//				else {
//					mGeometries.push_back(*g);
//				}
//
//			}
//
//			geoIndex++;
//
//			g = new Geometry();
//			g->indexOffset = indices.size();
//
//			std::string tempName;
//			fin >> tempName;
//			g->Name = std::wstring(tempName.begin(), tempName.end());
//		}
//		else if (command == "f")
//		{
//			UINT index;
//			texIndex = -1;
//			Vertex::Basic32 vertex;
//
//			//vertex 1
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex == -1 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 2
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex == -1 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 3
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex == -1 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			g->indexCount += 3;
//		}
//		else if (command == "mtllib")
//		{
//			fin >> command;
//			ImportMaterialLib(command);
//		}
//		else if (command == "usemtl")
//		{
//
//			std::string materialName;
//			fin >> materialName;
//			g->materilaIndex = SearchMaterialIndex(materialName);
//			g->UseTexture = mMaterials[g->materilaIndex].DiffuseMap != "";
//		}
//		else {
//			fin.ignore(1000, '\n');
//		}
//
//	}
//
//	g->indexCount = indices.size() - g->indexOffset;
//	BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));
//	if (IsMirror)
//	{
//		mGeometries_Mirror.push_back(*g);
//	}
//	else if (IsTransparent)
//	{
//		mGeometries_Transparent.push_back(*g);
//	}
//	else {
//		mGeometries.push_back(*g);
//	}
//
//
//	D3D11_BUFFER_DESC ibd;
//	ibd.Usage = D3D11_USAGE_IMMUTABLE;
//	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	ibd.CPUAccessFlags = 0;
//	ibd.MiscFlags = 0;
//	ibd.ByteWidth = sizeof(UINT) * indices.size();
//	D3D11_SUBRESOURCE_DATA IInitData;
//	IInitData.pSysMem = &indices[0];
//	IInitData.SysMemPitch = 0;
//	IInitData.SysMemSlicePitch = 0;
//	_device->CreateBuffer(&ibd, &IInitData, &IndexBuffer);
//
//	D3D11_BUFFER_DESC vbd;
//	vbd.Usage = D3D11_USAGE_IMMUTABLE;
//	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	vbd.CPUAccessFlags = 0;
//	vbd.MiscFlags = 0;
//	vbd.ByteWidth = sizeof(Vertex::Basic32) * allvertices.size();
//	D3D11_SUBRESOURCE_DATA VInitData;
//	VInitData.pSysMem = &allvertices[0];
//	VInitData.SysMemPitch = 0;
//	VInitData.SysMemSlicePitch = 0;
//	_device->CreateBuffer(&vbd, &VInitData, &VertexBuffer);
//
//	InitDiffuseMaps(_device);
//
//
//	return;
//}
//
//void Meshes::Build_BillboardVertexObjects(ID3D11Device *_device, std::wstring _filePath, XMFLOAT2 _billboardSize)
//{
//	std::ifstream fin(_filePath);
//	std::wstring s1 = L"Loading Obj File Failed: ";
//	std::wstring s2 = L" not founded";
//	auto s = s1 + _filePath + s2;
//
//	if (!fin)
//	{
//		MessageBox(0, s.c_str(), L"error", 0);
//	}
//
//	std::string ignore;
//	std::string command;
//	std::string currentMat;
//	char c;
//
//	UINT iv = 0;
//
//	UINT geoIndex = 0;
//	Geometry* g = new Geometry();
//
//	UINT vertexIndex;
//
//	std::vector<Vertex::PointBillboard> allvertices;
//
//	Vertex::PointBillboard sVertex;
//
//	while (!fin.eof())
//	{
//		fin >> command;
//		if (command == "v")
//		{
//			fin >> sVertex.Pos.x >> sVertex.Pos.y >> sVertex.Pos.z;
//			sVertex.Size = _billboardSize;
//			allvertices.push_back(sVertex);
//
//			iv++;
//		}
//		else if (command == "g")
//		{
//			std::string tempName;
//			fin >> tempName;
//			g->Name = std::wstring(tempName.begin(), tempName.end());;
//		}
//		else if (command == "mtllib")
//		{
//			fin >> command;
//			ImportMaterialLib(command);
//		}
//		else if (command == "usemtl")
//		{
//			if (geoIndex != 0)
//			{
//				g->indexCount = iv;
//				if (IsMirror)
//				{
//					mGeometries_Mirror.push_back(*g);
//				}
//				else if (IsTransparent)
//				{
//					mGeometries_Transparent.push_back(*g);
//				}
//				else {
//					mGeometries.push_back(*g);
//				}
//			}
//			g = new Geometry();
//			g->indexOffset = iv;
//			std::string materialName;
//			fin >> materialName;
//			g->materilaIndex = SearchMaterialIndex(materialName);
//			g->UseTexture = mMaterials[g->materilaIndex].DiffuseMap != "";
//			geoIndex++;
//		}
//		else {
//			fin.ignore(1000, '\n');
//		}
//
//	}
//
//	g->indexCount = iv - g->indexOffset;
//	if (IsMirror)
//	{
//		mGeometries_Mirror.push_back(*g);
//	}
//	else if (IsTransparent)
//	{
//		mGeometries_Transparent.push_back(*g);
//	}
//	else {
//		mGeometries.push_back(*g);
//	}
//
//	D3D11_BUFFER_DESC vbd;
//	vbd.Usage = D3D11_USAGE_IMMUTABLE;
//	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	vbd.CPUAccessFlags = 0;
//	vbd.MiscFlags = 0;
//	vbd.ByteWidth = sizeof(Vertex::PointBillboard) * allvertices.size();
//	D3D11_SUBRESOURCE_DATA VInitData;
//	VInitData.pSysMem = &allvertices[0];
//	VInitData.SysMemPitch = 0;
//	VInitData.SysMemSlicePitch = 0;
//	_device->CreateBuffer(&vbd, &VInitData, &VertexBuffer);
//
//	InitDiffuseMaps(_device);
//
//	return;
//}
//
//void Meshes::Build_Basic32QuadPatchObjects(ID3D11Device *_device, std::wstring _filePath)
//{
//	std::ifstream fin(_filePath);
//	std::wstring s1 = L"Loading Obj File Failed: ";
//	std::wstring s2 = L" not founded";
//	auto s = s1 + _filePath + s2;
//
//	if (!fin)
//	{
//		MessageBox(0, s.c_str(), L"error", 0);
//	}
//
//	std::string ignore;
//	std::string command;
//	std::string currentMat;
//	char c;
//
//	UINT iv = 0;
//	UINT ii = 0;
//	UINT in = 0;
//	UINT it = 0;
//
//
//	UINT geoIndex = 0;
//	Geometry* g = new Geometry();
//
//	UINT vertexIndex;
//	int texIndex;
//	UINT normalIndex;
//
//	std::vector<Vertex::Basic32> allvertices;
//	std::vector<XMFLOAT3> normals;
//	std::vector<UINT> indices;
//	std::vector<XMFLOAT2> texCoords;
//
//	std::vector<Vertex::Simple> vertecies;
//
//	Vertex::Simple sVertex;
//	XMFLOAT3 normal;
//	XMFLOAT2 tex;
//
//	bool newVertex;
//
//
//	while (!fin.eof())
//	{
//		fin >> command;
//		if (command == "v")
//		{
//			fin >> sVertex.Pos.x >> sVertex.Pos.y >> sVertex.Pos.z;
//			vertecies.push_back(sVertex);
//			iv++;
//		}
//		else if (command == "vn")
//		{
//			fin >> normal.x >> normal.y >> normal.z;
//			normals.push_back(normal);
//			in++;
//		}
//		else if (command == "vt")
//		{
//			fin >> tex.x >> tex.y >> ignore;
//			texCoords.push_back(tex);
//			it++;
//		}
//		else if (command == "g")
//		{
//			std::string tempName;
//			fin >> tempName;
//			g->Name = std::wstring(tempName.begin(), tempName.end());
//		}
//		else if (command == "f")
//		{
//			UINT index;
//			texIndex = -1;
//			Vertex::Basic32 vertex;
//
//			//vertex 1
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex == -1 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 2
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex == -1 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 3
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex == -1 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 4
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex == -1 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			g->indexCount += 4;
//		}
//		else if (command == "mtllib")
//		{
//			fin >> command;
//			ImportMaterialLib(command);
//		}
//		else if (command == "usemtl")
//		{
//			if (geoIndex != 0)
//			{
//				g->indexCount = indices.size() - g->indexOffset;
//				BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));
//
//				if (IsMirror)
//				{
//					mGeometries_Mirror.push_back(*g);
//				}
//				else if (IsTransparent)
//				{
//					mGeometries_Transparent.push_back(*g);
//				}
//				else {
//					mGeometries.push_back(*g);
//				}
//			}
//			g = new Geometry();
//			g->indexOffset = indices.size();
//			std::string materialName;
//			fin >> materialName;
//			g->materilaIndex = SearchMaterialIndex(materialName);
//			g->UseTexture = mMaterials[g->materilaIndex].DiffuseMap != "";
//			geoIndex++;
//		}
//		else {
//			fin.ignore(1000, '\n');
//		}
//
//	}
//
//	g->indexCount = indices.size() - g->indexOffset;
//	BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));
//	if (IsMirror)
//	{
//		mGeometries_Mirror.push_back(*g);
//	}
//	else if (IsTransparent)
//	{
//		mGeometries_Transparent.push_back(*g);
//	}
//	else {
//		mGeometries.push_back(*g);
//	}
//
//
//	D3D11_BUFFER_DESC ibd;
//	ibd.Usage = D3D11_USAGE_IMMUTABLE;
//	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	ibd.CPUAccessFlags = 0;
//	ibd.MiscFlags = 0;
//	ibd.ByteWidth = sizeof(UINT) * indices.size();
//	D3D11_SUBRESOURCE_DATA IInitData;
//	IInitData.pSysMem = &indices[0];
//	IInitData.SysMemPitch = 0;
//	IInitData.SysMemSlicePitch = 0;
//	_device->CreateBuffer(&ibd, &IInitData, &IndexBuffer);
//
//	D3D11_BUFFER_DESC vbd;
//	vbd.Usage = D3D11_USAGE_IMMUTABLE;
//	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	vbd.CPUAccessFlags = 0;
//	vbd.MiscFlags = 0;
//	vbd.ByteWidth = sizeof(Vertex::Basic32) * allvertices.size();
//	D3D11_SUBRESOURCE_DATA VInitData;
//	VInitData.pSysMem = &allvertices[0];
//	VInitData.SysMemPitch = 0;
//	VInitData.SysMemSlicePitch = 0;
//	_device->CreateBuffer(&vbd, &VInitData, &VertexBuffer);
//
//	InitDiffuseMaps(_device);
//
//
//	return;
//}
//
//
//
////
//////////////////////////////////// Static Meshes Triangles
////
//
//Meshes_Static_Traingles::Meshes_Static_Traingles()
//{
//	VertexBuffer = nullptr;
//	IndexBuffer = nullptr;
//}
//
//Meshes_Static_Traingles::~Meshes_Static_Traingles()
//{
//}
//
//void Meshes_Static_Traingles::FreeMemory()
//{
//	IndexBuffer.Reset(); VertexBuffer.Reset();
//
//	mMaterials.clear(); mMaterials.shrink_to_fit();
//
//	mVertexCache.clear(); mVertexCache.shrink_to_fit();
//
//	mGeometries.clear(); mGeometries.shrink_to_fit();
//
//	mGeometries_Transparent.clear(); mGeometries_Transparent.shrink_to_fit();
//
//	ReleaseDiffuseMaps();
//}
//
//UINT Meshes_Static_Traingles::AddVertex(UINT vertexIndex, Vertex::Basic32 vertex,
//	std::vector<Vertex::Basic32>& allVerticies, bool& newVertex)
//{
//	UINT index;
//	newVertex = true;
//
//	if (vertexIndex < mVertexCache.size())
//	{
//		CacheEntry* cache = mVertexCache[vertexIndex];
//		while (cache != nullptr)
//		{
//			Vertex::Basic32 v = allVerticies[cache->index];
//			if (vertex.Normal.x == v.Normal.x && vertex.Normal.y == v.Normal.y && vertex.Normal.z == vertex.Normal.z)
//			{
//				newVertex = false;
//				index = cache->index;
//				break;
//			}
//			cache = cache->pNext;
//		}
//	}
//	if (newVertex)
//	{
//		allVerticies.push_back(vertex);
//		index = allVerticies.size() - 1;
//		while (vertexIndex >= mVertexCache.size())
//		{
//			mVertexCache.push_back(nullptr);
//		}
//		CacheEntry *pNewEntry = new CacheEntry;
//		pNewEntry->index = index;
//		pNewEntry->pNext = nullptr;
//
//		CacheEntry *pCurEntry = mVertexCache[vertexIndex];
//		if (pCurEntry == nullptr)
//		{
//			mVertexCache[vertexIndex] = pNewEntry;
//		}
//		else
//		{
//			while (pCurEntry->pNext != nullptr)
//			{
//				pCurEntry = pCurEntry->pNext;
//			}
//			pCurEntry->pNext = pNewEntry;
//		}
//	}
//	return index;
//}
//
//void Meshes_Static_Traingles::ImportMaterialLib(std::string _filename, std::string _fileDirectory)
//{
//	mMaterials.clear();
//	mMaterials.shrink_to_fit();
//
//	UINT i = 0;
//	std::string addr;
//	addr.append(_fileDirectory);
//	addr.append("\\");
//	addr.append(_filename);
//	std::ifstream fin(addr);
//	std::wstring s1 = L"The file: ";
//	std::wstring sfilename(_filename.begin(), _filename.end());
//	std::wstring s2 = L"was not found";
//	auto s = s1 + sfilename + s2;
//
//
//	if (!fin)
//	{
//		MessageBox(NULL, s.c_str(), L"error", 0);
//		return;
//	}
//
//	MatLib *m = new MatLib();
//
//	std::string command;
//	std::string ignore;
//
//	while (!fin.eof())
//	{
//		fin >> command;
//
//		if (command == "newmtl")
//		{
//			if (i != 0)
//			{
//				mMaterials.push_back(*m);
//				m = new MatLib();
//			}
//			std::string mname;
//			fin >> mname;
//			m->Name = mname;
//			i++;
//		}
//		else if (command == "Ns")
//		{
//			fin >> m->Mat.Specular.w;
//		}
//		else if (command == "Ks")
//		{
//			fin >> m->Mat.Specular.x >> m->Mat.Specular.y >> m->Mat.Specular.z;
//		}
//		else if (command == "Ka")
//		{
//			fin >> m->Mat.Ambient.x >> m->Mat.Ambient.y >> m->Mat.Ambient.z;
//		}
//		else if (command == "Kd")
//		{
//			fin >> m->Mat.Diffuse.x >> m->Mat.Diffuse.y >> m->Mat.Diffuse.z;
//		}
//		else if (command == "d")
//		{
//			fin >> m->Mat.Diffuse.w;
//		}
//		else if (command == "map_Kd")
//		{
//			std::string relativefilepath;
//			fin >> relativefilepath;
//			m->DiffuseMap = _fileDirectory + "\\" + relativefilepath;
//		}
//		else if (command == "map_Ka")
//		{
//			fin >> m->AmbientMap;
//		}
//		else if (command == "map_Kd_isArray")
//		{
//			m->TextureType = TEXTURE_2D_ARRAY;
//		}
//		else {
//			fin.ignore(1000, '\n');
//		}
//	}
//	mMaterials.push_back(*m);
//
//}
//
//int Meshes_Static_Traingles::SearchMaterialIndex(std::string materialName)
//{
//	IsTransparent = false;
//	bool founded = false;
//	int i = 0;
//	while (i < mMaterials.size())
//	{
//		if (mMaterials[i].Name == materialName)
//		{
//			IsTransparent = mMaterials[i].Mat.Diffuse.w >= 1.0f ? false : true;
//			founded = true;
//			break;
//		}
//		else {
//			i++;
//		}
//	}
//	if (founded)
//	{
//		return i;
//	}
//	return -1;
//}
//
//void Meshes_Static_Traingles::InitDiffuseMaps(ComPtr<ID3D11Device> device)
//{
//	for (size_t i = 0; i < mMaterials.size(); i++)
//	{
//		MatLib m = mMaterials[i];
//
//		std::wstring filepath(m.DiffuseMap.begin(), m.DiffuseMap.end());
//		Texture *TexObj = new Texture();
//		TexObj->InitTextureFromFile(device, filepath, m.TextureType);
//		mDiffuseMaps.push_back(*TexObj);
//	}
//}
//
//void Meshes_Static_Traingles::ReleaseDiffuseMaps()
//{
//	for (auto tex : mDiffuseMaps)
//	{
//		tex.Release();
//	}
//	mDiffuseMaps.clear(); mDiffuseMaps.shrink_to_fit();
//}
//
//void Meshes_Static_Traingles::ImportOBJWaveFile(std::wstring _filePath, ComPtr<ID3D11Device> _device)
//{
//	if (_filePath == L"")
//		return;
//
//	FreeMemory();
//
//	boost::filesystem::path boostObjFilePath(_filePath);
//	std::ifstream fin(_filePath);
//	std::wstring s1 = L"Loading Obj File Failed: ";
//	std::wstring s2 = L" not founded";
//	auto s = s1 + _filePath + s2;
//
//	if (!fin)
//	{
//		MessageBox(0, s.c_str(), L"error", 0);
//	}
//
//	std::string ignore;
//	std::string command;
//	std::string currentMat;
//	char c;
//
//	UINT iv = 0;
//	UINT ii = 0;
//	UINT in = 0;
//	UINT it = 0;
//
//
//	UINT geoIndex = 0;
//	Geometry_Static* g = new Geometry_Static();
//
//	UINT vertexIndex;
//	int texIndex;
//	UINT normalIndex;
//
//	std::vector<Vertex::Basic32> allvertices;
//	std::vector<XMFLOAT3> normals;
//	std::vector<UINT> indices;
//	std::vector<XMFLOAT2> texCoords;
//
//	std::vector<Vertex::Simple> vertecies;
//
//	Vertex::Simple sVertex;
//	XMFLOAT3 normal;
//	XMFLOAT2 tex;
//
//	bool newVertex;
//
//
//	while (!fin.eof())
//	{
//		fin >> command;
//
//		if (command == "total_verts")
//		{
//			int totalVerts;
//			fin >> totalVerts;
//			vertecies.reserve(totalVerts);
//		}
//		else if (command == "total_normals")
//		{
//			int totalNormals;
//			fin >> totalNormals;
//			normals.reserve(totalNormals);
//		}
//		else if (command == "total_texCoords")
//		{
//			int total_texCoords;
//			fin >> total_texCoords;
//			texCoords.reserve(total_texCoords);
//		}
//		else if (command == "total_materials")
//		{
//			int total_materials;
//			fin >> total_materials;
//			mMaterials.reserve(total_materials);
//		}
//		else if (command == "total_maps")
//		{
//			int total_maps;
//			fin >> total_maps;
//			mDiffuseMaps.reserve(total_maps);
//		}
//		else if (command == "total_triangles")
//		{
//			int total_triangles;
//			fin >> total_triangles;
//			allvertices.reserve(total_triangles);
//			indices.reserve(total_triangles);
//		}
//
//		else if (command == "v")
//		{
//			fin >> sVertex.Pos.x >> sVertex.Pos.y >> sVertex.Pos.z;
//			vertecies.push_back(sVertex);
//			iv++;
//		}
//		else if (command == "vn")
//		{
//			fin >> normal.x >> normal.y >> normal.z;
//			normals.push_back(normal);
//			in++;
//		}
//		else if (command == "vt")
//		{
//			fin >> tex.x >> tex.y >> ignore;
//			texCoords.push_back(tex);
//			it++;
//		}
//		else if (command == "g")
//		{
//			if (geoIndex != 0)
//			{
//				g->indexCount = indices.size() - g->indexOffset;
//
//				BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));
//
//				if (IsTransparent)
//				{
//					mGeometries_Transparent.push_back(*g);
//				}
//				else {
//					mGeometries.push_back(*g);
//				}
//
//			}
//
//			geoIndex++;
//
//			g = new Geometry_Static();
//			g->indexOffset = indices.size();
//
//			std::string tempName;
//			fin >> tempName;
//			g->Name = std::wstring(tempName.begin(), tempName.end());
//		}
//		else if (command == "f")
//		{
//			UINT index;
//			texIndex = -1;
//			Vertex::Basic32 vertex;
//
//			//vertex 1
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 2
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 3
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			g->indexCount += 3;
//		}
//		else if (command == "mtllib")
//		{
//			fin >> command;
//			ImportMaterialLib(command, boostObjFilePath.parent_path().string());
//
//			InitDiffuseMaps(_device);
//		}
//		else if (command == "usemtl")
//		{
//
//			std::string materialName;
//			fin >> materialName;
//			g->materilaIndex = SearchMaterialIndex(materialName);
//			g->UseTexture = mMaterials[g->materilaIndex].DiffuseMap != "";
//			g->DiffuseMap = &mDiffuseMaps[g->materilaIndex];
//		}
//		else {
//			fin.ignore(1000, '\n');
//		}
//
//	}
//
//	//
//	// Push the last Object
//	//
//
//	g->indexCount = indices.size() - g->indexOffset;
//	BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));
//
//	if (IsTransparent)
//	{
//		mGeometries_Transparent.push_back(*g);
//	}
//	else {
//		mGeometries.push_back(*g);
//	}
//
//
//	D3D11_BUFFER_DESC ibd;
//	ibd.Usage = D3D11_USAGE_IMMUTABLE;
//	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	ibd.CPUAccessFlags = 0;
//	ibd.MiscFlags = 0;
//	ibd.ByteWidth = sizeof(UINT) * indices.size();
//	D3D11_SUBRESOURCE_DATA IInitData;
//	IInitData.pSysMem = &indices[0];
//	IInitData.SysMemPitch = 0;
//	IInitData.SysMemSlicePitch = 0;
//	_device->CreateBuffer(&ibd, &IInitData, &IndexBuffer);
//
//	D3D11_BUFFER_DESC vbd;
//	vbd.Usage = D3D11_USAGE_IMMUTABLE;
//	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	vbd.CPUAccessFlags = 0;
//	vbd.MiscFlags = 0;
//	vbd.ByteWidth = sizeof(Vertex::Basic32) * allvertices.size();
//	D3D11_SUBRESOURCE_DATA VInitData;
//	VInitData.pSysMem = &allvertices[0];
//	VInitData.SysMemPitch = 0;
//	VInitData.SysMemSlicePitch = 0;
//	_device->CreateBuffer(&vbd, &VInitData, &VertexBuffer);
//
//
//	//
//	//resize to fit
//	//
//	/*vertecies.shrink_to_fit();
//	allvertices.shrink_to_fit();
//	indices.shrink_to_fit();
//	normals.shrink_to_fit();
//	texCoords.shrink_to_fit();
//	mMaterials.shrink_to_fit();
//	mDiffuseMaps.shrink_to_fit();*/
//
//
//	return;
//}
//
//
////
//////////////////////////////////// Static Meshes Quads
////
//
//
//Meshes_Static_Quads::Meshes_Static_Quads()
//{
//	VertexBuffer = nullptr;
//	IndexBuffer = nullptr;
//}
//
//void Meshes_Static_Quads::FreeMemory()
//{
//	IndexBuffer.Reset(); VertexBuffer.Reset();
//
//	mMaterials.clear(); mMaterials.shrink_to_fit();
//
//	mVertexCache.clear(); mVertexCache.shrink_to_fit();
//
//	mGeometries.clear(); mGeometries.shrink_to_fit();
//
//	mGeometries_Transparent.clear(); mGeometries_Transparent.shrink_to_fit();
//
//	ReleaseDiffuseMaps();
//}
//
//UINT Meshes_Static_Quads::AddVertex(UINT vertexIndex, Vertex::Basic32 vertex,
//	std::vector<Vertex::Basic32>& allVerticies, bool& newVertex)
//{
//	UINT index;
//	newVertex = true;
//
//	if (vertexIndex < mVertexCache.size())
//	{
//		CacheEntry* cache = mVertexCache[vertexIndex];
//		while (cache != nullptr)
//		{
//			Vertex::Basic32 v = allVerticies[cache->index];
//			if (vertex.Normal.x == v.Normal.x && vertex.Normal.y == v.Normal.y && vertex.Normal.z == vertex.Normal.z)
//			{
//				newVertex = false;
//				index = cache->index;
//				break;
//			}
//			cache = cache->pNext;
//		}
//	}
//	if (newVertex)
//	{
//		allVerticies.push_back(vertex);
//		index = allVerticies.size() - 1;
//		while (vertexIndex >= mVertexCache.size())
//		{
//			mVertexCache.push_back(nullptr);
//		}
//		CacheEntry *pNewEntry = new CacheEntry;
//		pNewEntry->index = index;
//		pNewEntry->pNext = nullptr;
//
//		CacheEntry *pCurEntry = mVertexCache[vertexIndex];
//		if (pCurEntry == nullptr)
//		{
//			mVertexCache[vertexIndex] = pNewEntry;
//		}
//		else
//		{
//			while (pCurEntry->pNext != nullptr)
//			{
//				pCurEntry = pCurEntry->pNext;
//			}
//			pCurEntry->pNext = pNewEntry;
//		}
//	}
//	return index;
//}
//
//void Meshes_Static_Quads::ImportMaterialLib(std::string _filename, std::string _fileDirectory)
//{
//	mMaterials.clear();
//	mMaterials.shrink_to_fit();
//
//	UINT i = 0;
//	std::string addr;
//	addr.append(_fileDirectory);
//	addr.append("\\");
//	addr.append(_filename);
//	std::ifstream fin(addr);
//	std::wstring s1 = L"The file: ";
//	std::wstring sfilename(_filename.begin(), _filename.end());
//	std::wstring s2 = L"was not found";
//	auto s = s1 + sfilename + s2;
//
//
//	if (!fin)
//	{
//		MessageBox(NULL, s.c_str(), L"error", 0);
//		return;
//	}
//
//	MatLib *m = new MatLib();
//
//	std::string command;
//	std::string ignore;
//
//	while (!fin.eof())
//	{
//		fin >> command;
//
//		if (command == "newmtl")
//		{
//			if (i != 0)
//			{
//				mMaterials.push_back(*m);
//				m = new MatLib();
//			}
//			std::string mname;
//			fin >> mname;
//			m->Name = mname;
//			i++;
//		}
//		else if (command == "Ns")
//		{
//			fin >> m->Mat.Specular.w;
//		}
//		else if (command == "Ks")
//		{
//			fin >> m->Mat.Specular.x >> m->Mat.Specular.y >> m->Mat.Specular.z;
//		}
//		else if (command == "Ka")
//		{
//			fin >> m->Mat.Ambient.x >> m->Mat.Ambient.y >> m->Mat.Ambient.z;
//		}
//		else if (command == "Kd")
//		{
//			fin >> m->Mat.Diffuse.x >> m->Mat.Diffuse.y >> m->Mat.Diffuse.z;
//		}
//		else if (command == "d")
//		{
//			fin >> m->Mat.Diffuse.w;
//		}
//		else if (command == "map_Kd")
//		{
//			fin >> m->DiffuseMap;
//		}
//		else if (command == "map_Ka")
//		{
//			fin >> m->AmbientMap;
//		}
//		else if (command == "map_Kd_isArray")
//		{
//			m->TextureType = TEXTURE_2D_ARRAY;
//		}
//		else {
//			fin.ignore(1000, '\n');
//		}
//	}
//	mMaterials.push_back(*m);
//}
//
//int Meshes_Static_Quads::SearchMaterialIndex(std::string materialName)
//{
//	IsTransparent = false;
//	bool founded = false;
//	int i = 0;
//	while (i < mMaterials.size())
//	{
//		if (mMaterials[i].Name == materialName)
//		{
//			IsTransparent = mMaterials[i].Mat.Diffuse.w >= 1.0f ? false : true;
//			founded = true;
//			break;
//		}
//		else {
//			i++;
//		}
//	}
//	if (founded)
//	{
//		return i;
//	}
//	return -1;
//}
//
//void Meshes_Static_Quads::InitDiffuseMaps(ComPtr<ID3D11Device> device)
//{
//	for (size_t i = 0; i < mMaterials.size(); i++)
//	{
//		MatLib m = mMaterials[i];
//
//		std::wstring filepath(m.DiffuseMap.begin(), m.DiffuseMap.end());
//		Texture *TexObj = new Texture();
//		TexObj->InitTextureFromFile(device, filepath, m.TextureType);
//		mDiffuseMaps.push_back(*TexObj);
//	}
//}
//
//void Meshes_Static_Quads::ReleaseDiffuseMaps()
//{
//	for (auto tex : mDiffuseMaps)
//	{
//		tex.Release();
//	}
//	mDiffuseMaps.clear(); mDiffuseMaps.shrink_to_fit();
//}
//
//
//void Meshes_Static_Quads::ImportOBJWaveFile(std::wstring _filePath, ComPtr<ID3D11Device> _device)
//{
//	FreeMemory();
//
//	boost::filesystem::path boostObjFilePath(_filePath);
//	std::ifstream fin(_filePath);
//	std::wstring s1 = L"Loading Obj File Failed: ";
//	std::wstring s2 = L" not founded";
//	auto s = s1 + _filePath + s2;
//
//	if (!fin)
//	{
//		MessageBox(0, s.c_str(), L"error", 0);
//	}
//
//	std::string ignore;
//	std::string command;
//	std::string currentMat;
//	char c;
//
//	UINT iv = 0;
//	UINT ii = 0;
//	UINT in = 0;
//	UINT it = 0;
//
//
//	UINT geoIndex = 0;
//	Geometry_Static* g = new Geometry_Static();
//
//	UINT vertexIndex;
//	int texIndex;
//	UINT normalIndex;
//
//	std::vector<Vertex::Basic32> allvertices;
//	std::vector<XMFLOAT3> normals;
//	std::vector<UINT> indices;
//	std::vector<XMFLOAT2> texCoords;
//
//	std::vector<Vertex::Simple> vertecies;
//
//	Vertex::Simple sVertex;
//	XMFLOAT3 normal;
//	XMFLOAT2 tex;
//
//	bool newVertex;
//
//
//	while (!fin.eof())
//	{
//		fin >> command;
//		if (command == "v")
//		{
//			fin >> sVertex.Pos.x >> sVertex.Pos.y >> sVertex.Pos.z;
//			vertecies.push_back(sVertex);
//			iv++;
//		}
//		else if (command == "vn")
//		{
//			fin >> normal.x >> normal.y >> normal.z;
//			normals.push_back(normal);
//			in++;
//		}
//		else if (command == "vt")
//		{
//			fin >> tex.x >> tex.y >> ignore;
//			texCoords.push_back(tex);
//			it++;
//		}
//		else if (command == "g")
//		{
//			if (geoIndex != 0)
//			{
//				g->indexCount = indices.size() - g->indexOffset;
//
//				BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));
//
//				if (IsTransparent)
//				{
//					mGeometries_Transparent.push_back(*g);
//				}
//				else {
//					mGeometries.push_back(*g);
//				}
//
//			}
//
//			geoIndex++;
//
//			g = new Geometry_Static();
//			g->indexOffset = indices.size();
//
//			std::string tempName;
//			fin >> tempName;
//			g->Name = std::wstring(tempName.begin(), tempName.end());
//		}
//		else if (command == "f")
//		{
//			UINT index;
//			texIndex = -1;
//			Vertex::Basic32 vertex;
//
//			//vertex 1
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 2
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 3
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			//vertex 4
//			fin >> vertexIndex >> c;
//			if (fin.peek() == '/')
//			{
//				fin >> c >> normalIndex;
//			}
//			else
//			{
//				fin >> texIndex >> c >> normalIndex;
//			}
//
//			vertexIndex--; normalIndex--; texIndex--;
//
//			vertex.Pos = vertecies[vertexIndex].Pos;
//			vertex.Normal = normals[normalIndex];
//			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : texCoords[texIndex];
//			index = AddVertex(vertexIndex, vertex, allvertices, newVertex);
//			indices.push_back(index);
//
//			g->Indecies.push_back((index - g->indexOffset));
//
//			if (newVertex)
//			{
//				g->Vertecies.push_back(vertex);
//			}
//
//			g->indexCount += 4;
//		}
//		else if (command == "mtllib")
//		{
//			fin >> command;
//			ImportMaterialLib(command, boostObjFilePath.parent_path().string());
//		}
//		else if (command == "usemtl")
//		{
//
//			std::string materialName;
//			fin >> materialName;
//			g->materilaIndex = SearchMaterialIndex(materialName);
//			g->UseTexture = mMaterials[g->materilaIndex].DiffuseMap != "";
//		}
//		else {
//			fin.ignore(1000, '\n');
//		}
//
//	}
//
//	//
//	// Push the last Object
//	//
//
//	g->indexCount = indices.size() - g->indexOffset;
//	BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));
//
//	if (IsTransparent)
//	{
//		mGeometries_Transparent.push_back(*g);
//	}
//	else {
//		mGeometries.push_back(*g);
//	}
//
//
//	D3D11_BUFFER_DESC ibd;
//	ibd.Usage = D3D11_USAGE_IMMUTABLE;
//	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	ibd.CPUAccessFlags = 0;
//	ibd.MiscFlags = 0;
//	ibd.ByteWidth = sizeof(UINT) * indices.size();
//	D3D11_SUBRESOURCE_DATA IInitData;
//	IInitData.pSysMem = &indices[0];
//	IInitData.SysMemPitch = 0;
//	IInitData.SysMemSlicePitch = 0;
//	_device->CreateBuffer(&ibd, &IInitData, &IndexBuffer);
//
//	D3D11_BUFFER_DESC vbd;
//	vbd.Usage = D3D11_USAGE_IMMUTABLE;
//	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	vbd.CPUAccessFlags = 0;
//	vbd.MiscFlags = 0;
//	vbd.ByteWidth = sizeof(Vertex::Basic32) * allvertices.size();
//	D3D11_SUBRESOURCE_DATA VInitData;
//	VInitData.pSysMem = &allvertices[0];
//	VInitData.SysMemPitch = 0;
//	VInitData.SysMemSlicePitch = 0;
//	_device->CreateBuffer(&vbd, &VInitData, &VertexBuffer);
//
//	InitDiffuseMaps(_device);
//
//
//	return;
//}
//
//
///*
//*************************************Meshes_Static_Line2D
//*/
//
//
//UINT Meshes_Static_Lines2D::GetVertexCount() const
//{
//	return m_vertexCount;
//}
//
//UINT Meshes_Static_Lines2D::AddLine(Line2D& line)
//{
//	UINT lineOffset = mLines.size();
//
//	m_vertecies.push_back(line.Vertex1);
//	m_vertecies.push_back(line.Vertex2);
//
//	line.VertexOffset = m_vertexCount;
//
//	mLines.push_back(line);
//
//	m_dirty = true;
//
//	m_vertexCount += 2;
//
//	return lineOffset;
//}
//
//void Meshes_Static_Lines2D::UpdateLine2D(Line2D& line, UINT lineOffset)
//{
//	mLines[lineOffset].Vertex1 = line.Vertex1;
//	mLines[lineOffset].Vertex2 = line.Vertex2;
//
//	m_vertecies[lineOffset * 2] = line.Vertex1;
//	m_vertecies[lineOffset * 2 + 1] = line.Vertex2;
//
//
//	m_dirty = true;
//}
//
//ID3D11Buffer * Meshes_Static_Lines2D::GetVertexBuffer(ID3D11Device *device, ID3D11DeviceContext* context)
//{
//	if (m_dirty)
//	{
//		if (m_vertecies.size() != m_creationSize)
//		{
//			D3D11_BUFFER_DESC bdesc;
//
//			bdesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//			bdesc.ByteWidth = sizeof(Vertex::Simple) * m_vertecies.size();
//			bdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//			bdesc.MiscFlags = 0;
//			bdesc.StructureByteStride = sizeof(Vertex::Simple);
//			bdesc.Usage = D3D11_USAGE_DYNAMIC;
//
//			D3D11_SUBRESOURCE_DATA data;
//			data.pSysMem = &m_vertecies[0];
//			data.SysMemPitch = 0;
//			data.SysMemSlicePitch = 0;
//
//			device->CreateBuffer(&bdesc, &data, &m_vertexBuffer);
//
//			m_creationSize = m_vertecies.size();
//		}
//
//		else {
//			D3D11_MAPPED_SUBRESOURCE data = {};
//
//
//			auto result = context->Map(m_vertexBuffer.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &data);
//			memcpy(data.pData, &m_vertecies[0], sizeof(Vertex::Simple) * m_vertecies.size());
//			context->Unmap(m_vertexBuffer.Get(), 0);
//		}
//
//		m_dirty = false;
//	}
//
//
//	return m_vertexBuffer.Get();
//}
//
//void Meshes_Static_Lines2D::Destroy()
//{
//	m_vertecies.clear();
//	mLines.clear();
//
//}
