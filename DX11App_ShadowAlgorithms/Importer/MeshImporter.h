#pragma once


#include "Graphics/Objects.h"

namespace Importer
{
	struct CacheEntry {
		UINT index;
		std::shared_ptr<CacheEntry> pNext;
	};

	class MeshImporter_3D_OBJ
	{

	private:

		std::vector<std::shared_ptr<CacheEntry>> vVertexCache;


		int diffuse_number, normal_number, displacement_number;

		std::vector<Texture> vTextures_Diffuse;
		std::vector<Texture> vTextures_Normal;
		std::vector<Texture> vTextures_Displacement;


		std::vector<Material> vMaterials;


		std::vector<std::shared_ptr<Mesh::Mesh_3D>> vMeshes;




		bool IsTransparent = false;
		bool UseTexture = false;



		UINT AddVertex(const UINT vertexIndex, const Vertex::Basic32& vertex, std::vector<Vertex::Basic32>& allVerticies, bool& newVertex);

		void ImportMaterials(std::string _filename, std::string _fileDirectory, ComPtr<ID3D11Device> _device);

		Material* SearchMaterial(std::string materialName);



	public:

		ComPtr<ID3D11Buffer> pIndexBuffer, pVertexBuffer;


		///<summary>Import And Create Vertex And Index Buffer From .Obj File - (Attention: The vertex index which sended to this Method begins from 0)
		///</summary>
		void ImportOBJWaveFile(std::wstring _filePath, std::vector<Objects>& vObjects);

		void FreeMemory();

	};
}
