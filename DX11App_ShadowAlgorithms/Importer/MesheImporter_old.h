#pragma once


#include "Texture.h"
#include "MathHelper.h"
#include "Geometries.h"
#include "Lines.h"


//struct Geometry {
//public:
//	Geometry()
//	{
//		ZeroMemory(this, sizeof(Geometry));
//		GetLight = true;
//		WorldMatrix = XMMatrixIdentity();
//	}
//
//	std::wstring Name = L"";
//
//	UINT indexOffset;
//	int materilaIndex;
//	UINT indexCount;
//
//	BoundingBox AABB;
//	std::vector<Vertex::Basic32> Vertecies;
//	std::vector<UINT> Indecies;
//
//	XMMATRIX WorldMatrix;
//
//	VERTEX_TYPE VertexType;
//
//	Texture *DiffuseMap = nullptr;
//
//	bool Disabled;
//	bool UseTexture;
//	bool GetLight;
//	bool UseNormalMap;
//	bool UseAlphaMap;
//	bool CastShadow;
//
//
//	XMMATRIX GetWorldInvTranspose()
//	{
//		return MathHelper::InverseTranspose(WorldMatrix);
//	}
//
//	void SetTextures(ID3D11DeviceContext* _context, UINT _registerSlot)
//	{
//		DiffuseMap->SetSRV(_context, _registerSlot);
//	}
//
//};


struct CacheEntry {
	UINT index;
	CacheEntry* pNext;
};


//class Meshes {
//public:
//	Meshes(VERTEX_TYPE _vType, D3D11_PRIMITIVE_TOPOLOGY _topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
//	~Meshes();
//
//	ID3D11Buffer *IndexBuffer, *VertexBuffer;
//
//	std::vector<Geometry> mGeometries;
//	std::vector<Geometry> mGeometries_Transparent;
//	std::vector<Geometry> mGeometries_Mirror;
//	std::vector<Texture> mDiffuseMaps;
//	std::vector<MatLib> mMaterials;
//	/**
//	 * \brief The Sphere which all of meshes are inside of it
//	 */
//
//	///<summary>Import And Create Vertex And Index Buffer From .Obj File - (Attention: The vertex index which sended to this Method begins from 0)
//	///</summary>
//	void BuildVertexBufferObjMesh(std::wstring FileName, ID3D11Device *device, XMFLOAT2 _billboardSize = XMFLOAT2(0.0f, 0.0f));
//	
//private:
//	std::vector<CacheEntry*> mVertexChache;
//
//	void FreeMemory();
//	void Build_Basic32VertexObjects(ID3D11Device *_device, std::wstring _filePath);
//	void Build_Basic32QuadPatchObjects(ID3D11Device *_device, std::wstring _filePath);
//	void Build_BillboardVertexObjects(ID3D11Device *_device, std::wstring _filePath, XMFLOAT2 _billboardSize);
//
//	UINT AddVertex(UINT vertexIndex, Vertex::Basic32 vertex, std::vector<Vertex::Basic32> &allVerticies, bool &newVertex);
//	void ImportMaterialLib(std::string filename);
//	int SearchMaterialIndex(std::string materialName);
//	void InitDiffuseMaps(ID3D11Device *device);
//	void ReleaseDiffuseMaps();
//
//	VERTEX_TYPE mVertexType;
//	D3D11_PRIMITIVE_TOPOLOGY mPrimitiveTopology;
//
//	//std::vector<Vertex::Simple> mVerteciesPos;
//
//	bool IsTransparent = false;
//	bool IsMirror = false;
//	bool UseTexture = false;
//
//
//};

class Meshes_Static_Traingles
{
public:
	Meshes_Static_Traingles();
	~Meshes_Static_Traingles();

	ComPtr<ID3D11Buffer> IndexBuffer, VertexBuffer;

	std::vector<Geometry_Static> mGeometries;
	std::vector<Geometry_Static> mGeometries_Transparent;
	std::vector<Texture> mDiffuseMaps;
	std::vector<MatLib> mMaterials;

	///<summary>Import And Create Vertex And Index Buffer From .Obj File - (Attention: The vertex index which sended to this Method begins from 0)
	///</summary>
	void ImportOBJWaveFile(std::wstring _fileName, ComPtr<ID3D11Device> _device);

	void FreeMemory();

private:
	std::vector<CacheEntry*> mVertexCache;

	UINT AddVertex(UINT vertexIndex, Vertex::Basic32 vertex, std::vector<Vertex::Basic32> &allVerticies, bool &newVertex);
	void ImportMaterialLib(std::string filename, std::string _fileDirectory);
	int SearchMaterialIndex(std::string materialName);
	void InitDiffuseMaps(ComPtr<ID3D11Device> device);
	void ReleaseDiffuseMaps();

	bool IsTransparent = false;
	bool UseTexture = false;
};

class Meshes_Static_Lines2D
{
private:

	std::vector<Vertex::Simple> m_vertecies;

	bool m_isTransparent = false;

	bool m_dirty = false;

	UINT m_vertexCount = 0;

	UINT m_creationSize = 0;

	ComPtr<ID3D11Buffer> m_vertexBuffer;

public:

	std::vector<Line2D> mLines;

	UINT GetVertexCount()const;

	UINT AddLine(Line2D& line);
	void UpdateLine2D(Line2D& line, UINT lineOffset);

	ID3D11Buffer* GetVertexBuffer(ID3D11Device *device, ID3D11DeviceContext* context);

	void Destroy();


};

class Meshes_Static_Quads
{
	Meshes_Static_Quads();
	~Meshes_Static_Quads();

	ComPtr<ID3D11Buffer> IndexBuffer, VertexBuffer;

	std::vector<Geometry_Static> mGeometries;
	std::vector<Geometry_Static> mGeometries_Transparent;
	std::vector<Texture> mDiffuseMaps;
	std::vector<MatLib> mMaterials;

	///<summary>Import And Create Vertex And Index Buffer From .Obj File - (Attention: The vertex index which sended to this Method begins from 0)
	///</summary>
	void ImportOBJWaveFile(std::wstring _fileName, ComPtr<ID3D11Device> _device);

	void FreeMemory();

private:
	std::vector<CacheEntry*> mVertexCache;

	UINT AddVertex(UINT vertexIndex, Vertex::Basic32 vertex, std::vector<Vertex::Basic32> &allVerticies, bool &newVertex);
	void ImportMaterialLib(std::string filename, std::string _fileDirectory);
	int SearchMaterialIndex(std::string materialName);
	void InitDiffuseMaps(ComPtr<ID3D11Device> device);
	void ReleaseDiffuseMaps();

	bool IsTransparent = false;
	bool UseTexture = false;
};