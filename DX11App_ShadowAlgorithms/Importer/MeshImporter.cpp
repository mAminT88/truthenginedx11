#include "stdafx.h"
#include "MeshImporter.h"
#include "Globals.h"

using namespace Globals;

void Importer::MeshImporter_3D_OBJ::ImportOBJWaveFile(std::wstring _filePath, std::vector<Objects>& vObjects)
{

	if (_filePath == L"")
		return;

	FreeMemory();

	boost::filesystem::path boostObjFilePath(_filePath);
	std::ifstream fin(_filePath);
	std::wstring s1 = L"Loading Obj File Failed: ";
	std::wstring s2 = L" not founded";
	auto s = s1 + _filePath + s2;

	if (!fin)
	{
		MessageBox(0, s.c_str(), L"error", 0);
	}

	Objects* Obj = nullptr;

	std::string ignore;
	std::string command;
	std::string currentMat;
	char c;

	UINT iv = 0;
	UINT ii = 0;
	UINT in = 0;
	UINT it = 0;


	UINT geoIndex = 0;
	std::shared_ptr<Mesh::Mesh_3D> g = std::make_shared<Mesh::Mesh_3D>();

	UINT vertexIndex;
	int texIndex;
	UINT normalIndex;

	UINT index;

	std::vector<Vertex::Basic32> vAllVertices;
	std::vector<XMFLOAT3> vNormals;
	std::vector<UINT> vIndices;
	std::vector<XMFLOAT2> vTexCoords;

	std::vector<Vertex::Simple> vVertecies;

	Vertex::Simple sVertex;
	XMFLOAT3 normal;
	XMFLOAT2 tex;

	Vertex::Basic32 vertex;

	bool newVertex;

	while (!fin.eof())
	{
		fin >> command;

		if (command == "total_verts")
		{
			int totalVerts;
			fin >> totalVerts;
			vVertecies.reserve(totalVerts);
		}
		else if (command == "total_normals")
		{
			int totalNormals;
			fin >> totalNormals;
			vNormals.reserve(totalNormals);
		}
		else if (command == "total_texCoords")
		{
			int total_texCoords;
			fin >> total_texCoords;
			vTexCoords.reserve(total_texCoords);
		}
		else if (command == "total_materials")
		{
			int total_materials;
			fin >> total_materials;
			vMaterials.reserve(total_materials);
		}
		else if (command == "total_textures")
		{
			int total_maps;
			fin >> total_maps;
			vTextures_Diffuse.reserve(total_maps);
		}
		else if (command == "total_triangles")
		{
			int total_triangles;
			fin >> total_triangles;
			vAllVertices.reserve(total_triangles);
			vIndices.reserve(total_triangles);
		}

		else if (command == "v")
		{
			fin >> sVertex.Pos.x >> sVertex.Pos.y >> sVertex.Pos.z;
			vVertecies.push_back(sVertex);
			iv++;
		}
		else if (command == "vn")
		{
			fin >> normal.x >> normal.y >> normal.z;
			vNormals.push_back(normal);
			in++;
		}
		else if (command == "vt")
		{
			fin >> tex.x >> tex.y >> ignore;
			vTexCoords.push_back(tex);
			it++;
		}
		else if (command == "g")
		{
			vObjects.emplace_back();
			Obj = &vObjects.back();

			if (geoIndex != 0)
			{

				g->IndexCount = vIndices.size() - g->IndexOffset;

				BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));

				vMeshes.push_back(g);

			}

			geoIndex++;

			g = std::make_shared<Mesh::Mesh_3D>();

			Obj->Name = "Obj_" + std::to_string(geoIndex);
			Obj->pMesh = g.get();
			Obj->ID = geoIndex;
			
			g->IndexOffset = vIndices.size();

			std::string tempName;
			fin >> tempName;
			g->Name = tempName;
		}
		else if (command == "f")
		{
			texIndex = -1;

			//vertex 1
			fin >> vertexIndex >> c;
			if (fin.peek() == '/')
			{
				fin >> c >> normalIndex;
			}
			else
			{
				fin >> texIndex >> c >> normalIndex;
			}

			vertexIndex--; normalIndex--; texIndex--;

			vertex.Pos = vVertecies[vertexIndex].Pos;
			vertex.Normal = vNormals[normalIndex];
			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : vTexCoords[texIndex];
			index = AddVertex(vertexIndex, vertex, vAllVertices, newVertex);
			vIndices.push_back(index);
			g->Indecies.push_back((index - g->IndexOffset));

			if (newVertex)
			{
				g->Vertecies.push_back(vertex);
			}

			//vertex 2
			fin >> vertexIndex >> c;
			if (fin.peek() == '/')
			{
				fin >> c >> normalIndex;
			}
			else
			{
				fin >> texIndex >> c >> normalIndex;
			}

			vertexIndex--; normalIndex--; texIndex--;

			vertex.Pos = vVertecies[vertexIndex].Pos;
			vertex.Normal = vNormals[normalIndex];
			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : vTexCoords[texIndex];
			index = AddVertex(vertexIndex, vertex, vAllVertices, newVertex);
			vIndices.push_back(index);

			g->Indecies.push_back((index - g->IndexOffset));

			if (newVertex)
			{
				g->Vertecies.push_back(vertex);
			}

			//vertex 3
			fin >> vertexIndex >> c;
			if (fin.peek() == '/')
			{
				fin >> c >> normalIndex;
			}
			else
			{
				fin >> texIndex >> c >> normalIndex;
			}

			vertexIndex--; normalIndex--; texIndex--;

			vertex.Pos = vVertecies[vertexIndex].Pos;
			vertex.Normal = vNormals[normalIndex];
			vertex.Tex = texIndex < 0 ? XMFLOAT2(0.0f, 0.0f) : vTexCoords[texIndex];
			index = AddVertex(vertexIndex, vertex, vAllVertices, newVertex);
			vIndices.push_back(index);

			g->Indecies.push_back((index - g->IndexOffset));

			if (newVertex)
			{
				g->Vertecies.push_back(vertex);
			}

			g->IndexCount += 3;
		}
		else if (command == "mtllib")
		{
			fin >> command;
			ImportMaterials(command, boostObjFilePath.parent_path().string(), gDevice);
		}
		else if (command == "usemtl")
		{

			std::string materialName;
			fin >> materialName;
			Obj->pMaterial = SearchMaterial(materialName);
			
		}
		else {
			fin.ignore(1000, '\n');
		}

	}

	g->IndexCount = vIndices.size() - g->IndexOffset;

	BoundingBox::CreateFromPoints(g->AABB, g->Vertecies.size(), &g->Vertecies[0].Pos, sizeof(Vertex::Basic32));

	vMeshes.push_back(g);

	//Create and Init Vertex and Index Buffers

	D3D11_BUFFER_DESC ibd;
	ibd.Usage = D3D11_USAGE_IMMUTABLE;
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.CPUAccessFlags = 0;
	ibd.MiscFlags = 0;
	ibd.ByteWidth = sizeof(UINT) * vIndices.size();
	D3D11_SUBRESOURCE_DATA IInitData;
	IInitData.pSysMem = vIndices.data();
	IInitData.SysMemPitch = 0;
	IInitData.SysMemSlicePitch = 0;
	gDevice->CreateBuffer(&ibd, &IInitData, &pIndexBuffer);

	D3D11_BUFFER_DESC vbd;
	vbd.Usage = D3D11_USAGE_IMMUTABLE;
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.CPUAccessFlags = 0;
	vbd.MiscFlags = 0;
	vbd.ByteWidth = sizeof(Vertex::Basic32) * vAllVertices.size();
	D3D11_SUBRESOURCE_DATA VInitData;
	VInitData.pSysMem = vAllVertices.data();
	VInitData.SysMemPitch = 0;
	VInitData.SysMemSlicePitch = 0;
	gDevice->CreateBuffer(&vbd, &VInitData, &pVertexBuffer);

}

void Importer::MeshImporter_3D_OBJ::FreeMemory()
{
	pIndexBuffer.Reset(); pVertexBuffer.Reset();

	vMaterials.clear(); vMaterials.shrink_to_fit();

	vVertexCache.clear(); vVertexCache.shrink_to_fit();

	vMeshes.clear(); vMeshes.shrink_to_fit();

	vTextures_Diffuse.clear(); vTextures_Diffuse.shrink_to_fit();

	vTextures_Displacement.clear(); vTextures_Displacement.shrink_to_fit();

	vTextures_Normal.clear(); vTextures_Normal.shrink_to_fit();
}

UINT Importer::MeshImporter_3D_OBJ::AddVertex(const UINT vertexIndex, const Vertex::Basic32 & vertex, std::vector<Vertex::Basic32>& allVerticies, bool & newVertex)
{
	UINT index;
	newVertex = true;

	std::shared_ptr<CacheEntry> cache;

	if (vertexIndex < vVertexCache.size())
	{
		cache = vVertexCache[vertexIndex];
		while (cache != nullptr)
		{
			Vertex::Basic32 v = allVerticies[cache->index];
			if (vertex.Normal.x == v.Normal.x && vertex.Normal.y == v.Normal.y && vertex.Normal.z == vertex.Normal.z)
			{
				newVertex = false;
				index = cache->index;
				break;
			}
			cache = cache->pNext;
		}
	}
	if (newVertex)
	{
		allVerticies.push_back(vertex);
		index = allVerticies.size(); index--; // index = allVertecies.size() - 1;
		while (vertexIndex >= vVertexCache.size())
		{
			vVertexCache.push_back(nullptr);
		}
		std::shared_ptr<CacheEntry> pNewEntry = std::make_shared<CacheEntry>();
		pNewEntry->index = index;
		pNewEntry->pNext = nullptr;

		cache = vVertexCache[vertexIndex];
		if (cache == nullptr)
		{
			vVertexCache[vertexIndex] = pNewEntry;
		}
		else
		{
			while (cache->pNext != nullptr)
			{
				cache = cache->pNext;
			}
			cache->pNext = pNewEntry;
		}
	}
	return index;
}

void Importer::MeshImporter_3D_OBJ::ImportMaterials(std::string _filename, std::string _fileDirectory, ComPtr<ID3D11Device> gDevice)
{
	vMaterials.clear();
	vMaterials.shrink_to_fit();

	int i = 0;
	std::string addr;
	addr.append(_fileDirectory);
	addr.append("\\");
	addr.append(_filename);
	std::ifstream fin(addr);
	std::wstring s1 = L"The file: ";
	std::wstring sfilename(_filename.begin(), _filename.end());
	std::wstring s2 = L"was not found";
	auto s = s1 + sfilename + s2;

	XMFLOAT4 s4, d4, a4;


	if (!fin)
	{
		MessageBox(NULL, s.c_str(), L"error", 0);
		return;
	}

	Material* m = nullptr;

	std::string command;
	std::string ignore;

	while (!fin.eof())
	{
		fin >> command;

		if (command == "newmtl")
		{
			vMaterials.emplace_back();

			m = &vMaterials.back();


			std::string mname;
			fin >> mname;
			m->mName = mname;
			m->ID = i;
			i++;
		}
		else if (command == "Ns")
		{
			fin >> m->mShadingColor.specularColor.w;
		}
		else if (command == "Ks")
		{
			fin >> m->mShadingColor.specularColor.x >> m->mShadingColor.specularColor.y >> m->mShadingColor.specularColor.z;
		}
		else if (command == "Ka")
		{
			fin >> m->mShadingColor.ambientColor.x >> m->mShadingColor.ambientColor.y >> m->mShadingColor.ambientColor.z;
		}
		else if (command == "Kd")
		{
			fin >> m->mShadingColor.diffuseColor.x >> m->mShadingColor.diffuseColor.y >> m->mShadingColor.diffuseColor.z;
		}
		else if (command == "d")
		{
			fin >> m->mShadingColor.diffuseColor.w;
		}
		else if (command == "map_Kd")
		{
			std::string relativefilepath;
			fin >> relativefilepath;

			if (relativefilepath == "")
			{
				continue;
			}

			std::string filepath = _fileDirectory + "\\" + relativefilepath;

			CHAR fname[_MAX_FNAME];
			CHAR ext[_MAX_EXT];

			_splitpath_s(filepath.c_str(), nullptr, 0, nullptr, 0, fname, _MAX_FNAME, ext, _MAX_EXT);

			vTextures_Diffuse.emplace_back();

			Texture* tex = &vTextures_Diffuse.back();
			tex->ID = diffuse_number++;
			
			TEXTURE_FORMAT tformat = TEXTURE_FORMAT_WIC;

			if (_stricmp(ext, ".dds") == 0)
			{
				tformat = TEXTURE_FORMAT_DDS;
			}

#ifdef USE_DIRECTXTEX
			tex->InitTextureFromFile(std::wstring(filepath.begin(), filepath.end()), TEXTURE_FORMAT_WIC);
#else
			tex->InitTextureFromFile(std::wstring(filepath.begin(), filepath.end()), TEXTURE_2D, TEXTURE_FORMAT_WIC);
#endif
			m->SetDiffueMap(tex, std::string(""));

		}
		else if (command == "map_Ka")
		{

		}
		else {
			fin.ignore(1000, '\n');
		}
	}

	

}

Material* Importer::MeshImporter_3D_OBJ::SearchMaterial(std::string materialName)
{
	int i = 0;
	while (i < vMaterials.size())
	{
		if (vMaterials[i].mName == materialName)
		{
			return &vMaterials[i];
			break;
		}
		else {
			i++;
		}
	}
	return nullptr;
}

