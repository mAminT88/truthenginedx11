#include "stdafx.h"
#include "ShaderList.h"

#include "Graphics/ShaderList/VS_Render2D.h"
#include "Graphics/ShaderList/VS_ProcessMesh.h"
#include "Graphics/ShaderList/VS_BuildShadowMap.h"

#include "Graphics/ShaderList/PS_DeferredShading.h"
#include "Graphics/ShaderList/PS_GenerateGBuffers.h"
#include "Graphics/ShaderList/PS_GenerateVarianceShadowMap.h"
#include "Graphics/ShaderList/PS_SeperableBlurFilter.h"
#include "Graphics/ShaderList/PS_DownUpSampling.h"
#include "Graphics/ShaderList/PS_GenerateSAT.h"
#include "Graphics/ShaderList/PS_OSSSS_GenerateShadowData_Pass0.h"
#include "Graphics/ShaderList/PS_OSSSS_GenerateShadowData_Pass1.h"
#include "Graphics/ShaderList/PS_OSSSS_GenerateShadowMap.h"
#include "Graphics/ShaderList/PS_SSSM_GenerateShadowData.h"
#include "Graphics/ShaderList/PS_BlurBilateralByDepthNormal.h"
#include "Graphics/ShaderList/PS_SSAO.h"
#include "Graphics/ShaderList/PS_PostFX_FinalPass.h"
#include "Graphics\ShaderList\PS_GenerateSAT_UINT.h"
#include "Graphics\ShaderList\PS_GenerateMomentShadowMap.h"
#include "Graphics\ShaderList\PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch.h"

#include "Graphics/ShaderList/CS_GenerateSAT.h"
#include "Graphics/ShaderList/CS_HDR_AvgLuminance.h"
#include "Graphics/ShaderList/CS_Bloom.h"
#include "Graphics/ShaderList/CS_GaussianBlur.h"
#include "Graphics/ShaderList/CS_DownSampling.h"
#include "Graphics\ShaderList\CS_FindBlocker.h"

void ShaderList::InitAllStatics(bool fromHLSLFile)
{
	VertexShaders::VS_Render2D::InitStatics(fromHLSLFile);
	VertexShaders::VS_ProcessMesh::InitStatics(fromHLSLFile);
	VertexShaders::VS_BuildShadowMap::InitStatics(fromHLSLFile);

	PixelShaders::PS_DeferredShading::InitStatics(fromHLSLFile);

	PixelShaders::PS_GenerateGBuffers::InitStatics(fromHLSLFile);
	PixelShaders::PS_GenerateVarianceShadowMap::InitStatics(fromHLSLFile);
	PixelShaders::PS_SeperableBlurFilter::InitStatics(fromHLSLFile);
	PixelShaders::PS_DownUpSampling::InitStatics(fromHLSLFile);
	PixelShaders::PS_GenerateSAT::InitStatics(fromHLSLFile);
	PixelShaders::PS_GenerateSAT_UINT::InitStatics(fromHLSLFile);
	PixelShaders::PS_GenerateMomentShadowMap::InitStatics(fromHLSLFile);
	PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::InitStatics(fromHLSLFile);
	PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::InitStatics(fromHLSLFile);
	PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::InitStatics(fromHLSLFile);
	PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::InitStatics(fromHLSLFile);
	PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::InitStatics(fromHLSLFile);
	PixelShaders::PS_SSSM_GenerateShadowData_0::InitStatics(fromHLSLFile);
	PixelShaders::PS_SSSM_GenerateShadowData_1::InitStatics(fromHLSLFile);
	PixelShaders::PS_SSSM_GenerateShadowData_2::InitStatics(fromHLSLFile);
	PixelShaders::PS_BlurBilateralByDepthNormal::InitStatics(fromHLSLFile);
	PixelShaders::PS_SSAO::InitStatics(fromHLSLFile);
	PixelShaders::PS_PostFX_FinalPass::InitStatics(fromHLSLFile);

	ComputeShaders::CS_FindBlocker::InitStatics(fromHLSLFile);
	ComputeShaders::CS_GenerateSAT_Horz::InitStatics(fromHLSLFile);
	ComputeShaders::CS_GenerateSAT_Vert::InitStatics(fromHLSLFile);
	ComputeShaders::CS_HDR_AvgLuminance::InitStatics(fromHLSLFile);
	ComputeShaders::CS_Bloom::InitStatics(fromHLSLFile);
	ComputeShaders::CS_GaussianBlur::InitStatics(fromHLSLFile);
	ComputeShaders::CS_DownSampling::InitStatics(fromHLSLFile);
}
