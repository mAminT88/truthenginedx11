#pragma once

//#include "Vertex.h"
//#include "Texture.h"

namespace Vertex
{
	struct Basic32;
}

enum VERTEX_TYPE : int;

namespace Mesh
{

	class Mesh_3D {


	protected:

		ID3D11Buffer* VertexBuffer;
		ID3D11Buffer* IndexBuffer;


	public:

		Mesh_3D(); 
		

		std::string Name = "";

			   
		UINT IndexOffset;
		UINT IndexCount;


		BoundingBox AABB;


		std::vector<Vertex::Basic32> Vertecies;
		std::vector<UINT> Indecies;


		VERTEX_TYPE VertexType;

		void SetIndexBuffer(ID3D11Buffer* _indexBuffer)
		{
			IndexBuffer = _indexBuffer;
		}

		void SetVertexBuffer(ID3D11Buffer* _vertexBuffer)
		{
			VertexBuffer = _vertexBuffer;
		}

		ID3D11Buffer* GetVertexBuffer()
		{
			return VertexBuffer;
		}


		ID3D11Buffer* GetIndexBuffer()
		{
			return IndexBuffer;
		}

	};



	/*struct Geometry_Billboard {
	public:
		Geometry_Billboard()
		{
			ZeroMemory(this, sizeof(Geometry_Billboard));
			GetLight = true;
		}

		std::wstring Name = L"";

		UINT vertexOffset;
		int materilaIndex;
		UINT vertexCount;

		BoundingBox AABB;
		std::vector<Vertex::PointBillboard> Vertecies;

		VERTEX_TYPE VertexType = VERTEX_TYPE_POINTBILLBOARD;

		bool Disabled;
		bool UseTexture;
		bool GetLight;
		bool UseNormalMap;
		bool UseAlphaMap;
		bool CastShadow;
	};

	struct Geometry_Particles
	{
		Geometry_Particles()
		{
			ZeroMemory(this, sizeof(Geometry_Particles));
		}
		~Geometry_Particles();

		std::wstring Name = L"";

		UINT vertexOffset;
		int materilaIndex;
		UINT vertexCount;

		BoundingBox AABB;
		std::vector<Vertex::Particle> Vertecies;

		VERTEX_TYPE VertexType = VERTEX_TYPE_PARTICLE;

		bool Disabled;
		bool UseTexture;
		bool GetLight;
		bool UseNormalMap;
		bool UseAlphaMap;
		bool CastShadow;
	};*/

}