#pragma once

#include "Vertex.h"

class Line2D
{
	XMFLOAT4 m_color;
	XMMATRIX WorldMatrix = XMMatrixIdentity();


public:
	std::wstring Name = L"";
	
	Vertex::Simple Vertex1;
	Vertex::Simple Vertex2;

	UINT VertexOffset;

	XMFLOAT4& GetColor();
	XMMATRIX& GetWorldMat();

};
