#pragma once

#include "Shaders.h"
#include "RenderStates.h"
#include "ConstantBuffer.h"


enum PS_SHADER_TYPE
{
	PS_SHADER_NOLIGHT_NOTEX,
	PS_SHADER_LIGHT_NOTEX,
	PS_SHADER_NOLIGHT_TEX,
	PS_SHADER_LIGHT_TEX
};

class Pipeline
{

protected:
		
	UINT mVertexStride, mVertexOffset = 0;

	std::string mShaderProfile;


	ComPtr<ID3D11InputLayout> pInputLayout;


	D3D11_PRIMITIVE_TOPOLOGY mPrimitiveTopology;


	RENDER_STATE_RS mRasterizerStateType;
	RENDER_STATE_BS	mBlendStateType;
	RENDER_STATE_DS mDepthStateType;


	ShaderObject<ID3D11VertexShader>	mVSObject;
	ShaderObject<ID3D11PixelShader>		mPSObject;
	ShaderObject<ID3D11DomainShader>	mDSObject;
	ShaderObject<ID3D11HullShader>		mHSObject;
	ShaderObject<ID3D11GeometryShader>	mGSObject;

public:
	virtual void Init();
	virtual void Destroy();

	virtual void Apply_Preliminiary(ID3D11DeviceContext* _context);
	virtual void Apply_Preliminiary(ComPtr<ID3D11Buffer> _vertexBuffer, ComPtr<ID3D11Buffer> _indexBuffer, ID3D11DeviceContext* _context);

	void ChangeRenderStates(RENDER_STATE_RS _rs = RENDER_STATE_RS_NONE, RENDER_STATE_BS _bs = RENDER_STATE_BS_NONE , RENDER_STATE_DS _ds = RENDER_STATE_DS_NONE);

};

