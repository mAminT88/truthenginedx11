#pragma once

//#include "D3DUtil.h"

class SRVCollector
{
private:
	
	std::vector<ID3D11ShaderResourceView*> m_srv_vector;
	size_t m_srv_count = 0;
public:

	// Add a SRV to Collector
	void AddSRV(ID3D11ShaderResourceView* _srv);
	//Clear And Add a SRV
	void ClearAndAddSRV(ID3D11ShaderResourceView *_srv, int reserveSize = -1);
	// Set Shader Resources To Pixel Shader
	void BindSRVs_PS(ComPtr<ID3D11DeviceContext>& _context, UINT _startSlot);
	void BindSRVs_PS(ComPtr<ID3D11DeviceContext>& _context, UINT _startSlot, UINT _lowerBound);
	void BindSRVs_PS(ComPtr<ID3D11DeviceContext>& _context, UINT _startSlot, UINT _lowerBound, UINT _upperBound);

	void BindSRVs_CS(ComPtr<ID3D11DeviceContext>& _context, UINT _startSlot);
	//clear Collector
	void ClearCollector();
	//replace a SRV with a previous one
	void ReplaceSRV(ID3D11ShaderResourceView *_srv, UINT _prev_num);
};

class RTVCollector
{
private:

	using RGBA = float[4];

	std::vector<ID3D11RenderTargetView*> m_vector_rtv;
	size_t m_size;
public:
	void AddRTV(ID3D11RenderTargetView* _rtv);
	void ClearAndAddRTV(ID3D11RenderTargetView* _rtv, int reserveSize = -1);
	void ClearRTVs(ID3D11DeviceContext* _context, RGBA Color = RGBA{0.0f, 0.0f, 0.0f, 0.0f});
	void ClearCollector();
	ID3D11RenderTargetView** GetRTVs();
	size_t GetSize();
	void ReplaceRTV(ID3D11RenderTargetView *_rtv, UINT _prev_num);

};
