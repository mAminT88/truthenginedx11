#include "stdafx.h"
#include "RenderEngine.h"

#include "Globals.h"
#include "Engine.h"
#include "ShaderList.h"
#include "AdapterReader.h"
#include "ErrorLogger.h"
#include "MathHelper.h"
#include "enums.h"
#include "D3D.h"

#include "Graphics/StaticBuffers.h"
#include "Graphics/RenderStates.h"
#include "Graphics/Material.h"

#include "Objects/Mesh.h"

#include "ShaderList/CommonConstantBuffers_ComputeShader.h"

//////////////////////////////////////////////////////////////////////////
// Include Render Techniques
#include "RenderTechnique/IRenderTechnique.h"
#include "RenderTechnique/RenderTechnique_DeferredShading.h"
#include "RenderTechnique/RenderTechnique_DeferredShading_2.h"

using namespace Globals;


RenderEngine::RenderEngine() = default;

RenderEngine::~RenderEngine() = default;

void RenderEngine::Init(Engine* engine, int clientWidth, int clientHeight)
{

	mEngine = engine;

	gClientWidth = clientWidth; gClientHeight = clientHeight;

#ifdef _DEBUG
	FilterDirectxMessages();
#endif

	//auto task_initStatics = concurrency::create_task([this]() { InitStatics(); });
	InitStatics();

	InitCamera();
	InitViewPort();
	InitSwapChain();
	LoadLights(L"Data/Lights/LightSetLvl1.light");

	//task_initStatics.wait();

	InitRenderTechniques();

	//ImGUI Initialization
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO();
	ImGui_ImplWin32_Init(gHWND_Rendering);
	ImGui_ImplDX11_Init(gDevice.Get(), gContext.Get());
	ImGui::StyleColorsDark();


}

void RenderEngine::Draw()
{

	gContext->IASetIndexBuffer(gIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

	pCurrentTechnique->Draw3D();
	/*pCurrentTechnique->Draw2D();
	pCurrentTechnique->DrawTxt();*/

	RenderUI();


	gSwapChain.Present();
}

void RenderEngine::Update()
{

	ProcessKeyInputs();


	gCamera_main.UpdateViewMatrix();

	gLights.Update(gDt);

	UpdateBuffers();

	pCurrentTechnique->Update();

}

void RenderEngine::Catch_WCOMMAND(UINT msg, WPARAM wparam, LPARAM lparam)
{
	switch (msg)
	{
	case WM_COMMAND:
		pCurrentTechnique->Catch_WCommand(wparam, lparam);
		break;
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_MBUTTONDOWN:
		OnMouseDown(wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
		return;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MBUTTONUP:
		OnMouseUp(wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
		return;
	case WM_MOUSEMOVE:
		OnMouseMove(wparam, GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
		return;
	case WM_CONTEXTMENU:
		pCurrentTechnique->ShowUpContextMenu(GET_X_LPARAM(lparam), GET_Y_LPARAM(lparam));
		break;
		/*case WM_KEYUP:
			ProcessKeyInput(wparam);
			return;*/
	default:
		break;
	}
}

void RenderEngine::RecompileShaders()
{
	//pCurrentTechnique->RecompileShaders();
	ShaderList::InitAllStatics(true);
}

void RenderEngine::InitDXDevice()
{
	std::vector<AdapterData> adapters = AdapterReader::GetAdapters();

	if (adapters.size() < 0)
	{
		ErrorLogger::Log("No DXGI Adapter was found.");
		exit(-1);
	}

	UINT device_flags = 0;

//#ifdef _DEBUG
	device_flags |= D3D11_CREATE_DEVICE_DEBUG;
//#endif

//#ifdef _DEVICE_BGRA_SPPORT
	device_flags |= D3D11_CREATE_DEVICE_BGRA_SUPPORT;
//#endif

	D3D_FEATURE_LEVEL featureLevel[] = {
	D3D_FEATURE_LEVEL_11_0,
	// 	D3D_FEATURE_LEVEL_10_1,
	// 	D3D_FEATURE_LEVEL_10_0,
	// 	D3D_FEATURE_LEVEL_9_3,
	// 	D3D_FEATURE_LEVEL_9_2,
	// 	D3D_FEATURE_LEVEL_9_1,
	};

	/*ComPtr<ID3D11Device> dev11;
	ComPtr<ID3D11DeviceContext> context11;*/

	//Create ID3D11Device
	auto hRESULT = D3D11CreateDevice(adapters[0].pAdapter.Get(), D3D_DRIVER_TYPE_UNKNOWN, nullptr, device_flags, featureLevel, 1, D3D11_SDK_VERSION, gDevice.ReleaseAndGetAddressOf(), nullptr, gContext.ReleaseAndGetAddressOf());

	if (FAILED(hRESULT))
	{
		ErrorLogger::Log(hRESULT, "Failed to create D3D11Device.");
		exit(-1);
	}

	/*dev11.As(&gDevice);
	context11.As(&gContext);*/


	hRESULT = gContext.As(&gD3DUserDefinedAnnotation);

	if (FAILED(hRESULT))
	{
		ErrorLogger::Log(hRESULT, "Failed to create D3DUserDefinedAnnotation.");
		exit(-1);
	}
}

void RenderEngine::InitCamera()
{
	//gCamera_main.SetPosition(-54.482f, 59.501f, 32.719f); // Sponza
	gCamera_main.SetPosition(-210.804f, 750.502f, -882.444f); //3Cylinder_Big

	gCamera_main.LookAt(gCamera_main.GetPosition(), XMFLOAT3(-161.799178186f, 108.88138003128f, -3.62417474554f), XMFLOAT3(0.0, 1.0, 0.0));

	gCamera_main.SetLens(XM_PIDIV4, MathHelper::AspectRatio(gClientWidth, gClientHeight), 1.0f, 4500.0f);

	gCamera_main.UpdateViewMatrix();

	gCamera_main.CreateBoundingFrustum();
	
	return;
}

void RenderEngine::InitViewPort()
{
	gViewPort.Height = gClientHeight;
	gViewPort.Width = gClientWidth;
	gViewPort.MinDepth = 0.0f;
	gViewPort.MaxDepth = 1.0f;
	gViewPort.TopLeftX = 0;
	gViewPort.TopLeftY = 0;
}

void RenderEngine::LoadLights(std::wstring lightsFilePath)
{
	ILightObj::InitStaticConstantBuffers();

	gLights.ImportLights(lightsFilePath, gContext.Get());
}

void RenderEngine::ImportOBJ(std::wstring ObjFilePath)
{

}

void RenderEngine::ChangeRenderTechnique(IRenderTechnique * pRTechnique)
{

	if (pCurrentTechnique && pCurrentTechnique != pRTechnique)
	{
		pCurrentTechnique->DestroyGpuResources();
	}

	pCurrentTechnique = pRTechnique;

	pCurrentTechnique->InitGpuResources();
	   
}

void RenderEngine::OnMouseDown(WPARAM btnState, int x, int y)
{
	gLastMousePos.x = x;
	gLastMousePos.y = y;

	SetCapture(gHWND_Rendering);

}

void RenderEngine::OnMouseUp(WPARAM btnState, int x, int y)
{
	ReleaseCapture();
}

void RenderEngine::OnMouseMove(WPARAM btnState, int x, int y)
{
	if ((btnState & MK_LBUTTON) != 0)
	{
		float dx = XMConvertToRadians(0.25f * static_cast<float>(x - gLastMousePos.x));
		float dy = XMConvertToRadians(0.25f * static_cast<float>(y - gLastMousePos.y));

		gCamera_main.Pitch(dy / 10);
		gCamera_main.RotateY(dx / 10);
	}

	gLastMousePos.x = x;
	gLastMousePos.y = y;
}

void RenderEngine::ProcessKeyInputs()
{
	if (GetAsyncKeyState('W') & 0x8000)
	{
		gCamera_main.Walk(gDt);
	}

	if (GetAsyncKeyState('S') & 0x8000)
	{
		gCamera_main.Walk(-gDt);
	}

	if (GetAsyncKeyState('A') & 0x8000)
	{
		gCamera_main.Strafe(-gDt);
	}

	if (GetAsyncKeyState('D') & 0x8000)
	{
		gCamera_main.Strafe(gDt);
	}

	/*if (GetAsyncKeyState('Q') & 0x8000)
	{
		gCamera_main.RotateCamera(gDt);
	}*/


}

void RenderEngine::FilterDirectxMessages()
{
	ComPtr<ID3D11Debug> d3dDebug;
	auto hr = gDevice.As(&d3dDebug);
	if (SUCCEEDED(hr))
	{
		ComPtr<ID3D11InfoQueue> d3dInfoQueue;
		hr = d3dDebug.As(&d3dInfoQueue);
		if (SUCCEEDED(hr))
		{
			D3D11_MESSAGE_ID hide[] =
			{
				D3D11_MESSAGE_ID_DEVICE_OMSETRENDERTARGETS_HAZARD,
				D3D11_MESSAGE_ID_DEVICE_PSSETSHADERRESOURCES_HAZARD,
				D3D11_MESSAGE_ID_QUERY_END_ABANDONING_PREVIOUS_RESULTS,
				D3D11_MESSAGE_ID_DEVICE_DRAW_RESOURCE_FORMAT_SAMPLE_C_UNSUPPORTED
				// TODO: Add more message IDs here as needed 
			};
			D3D11_INFO_QUEUE_FILTER filter;
			memset(&filter, 0, sizeof(filter));
			filter.DenyList.NumIDs = _countof(hide);
			filter.DenyList.pIDList = hide;
			d3dInfoQueue->AddStorageFilterEntries(&filter);
		}
	}
}

void RenderEngine::RenderUI()
{
	//Draw ImGui UI
	//Start ImGui New Frame
	ImGui_ImplDX11_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
	//Create Default Config Window
	ImGui::Begin("Configuration");

	std::string currentTechniqueName = "Current Render Technique: ";
	currentTechniqueName += pCurrentTechnique->GetName();

	ImGui::Text(currentTechniqueName.c_str());

	if (ImGui::BeginCombo("Render Techniques", pCurrentTechnique->GetName().c_str()))
	{

		if (ImGui::Selectable("Deferred Shading Sync"))
		{
			ChangeRenderTechnique(mRenderTechnique_DeferredShading.get());
		}

		if (ImGui::Selectable("Deferred Shading ASync"))
		{
			ChangeRenderTechnique(mRenderTechnique_DeferredShading_2.get());
		}

		ImGui::EndCombo();
	}

	if (ImGui::Button("AddModel"))
	{
		const COMDLG_FILTERSPEC fileExtensions[] =
		{
			{L"Wave OBJ File (*.obj)", L"*.obj"},
			{L"Autodesk FBX File", L"*.fbx"},
			{L"Collada", L"*.dae"},
		};

		std::string filePath;

		mEngine->SelectFile(filePath, fileExtensions, 3);
		mEngine->ImportModel(filePath);
	}

	if (ImGui::Button("AddSkinnedModel"))
	{
		const COMDLG_FILTERSPEC fileExtensions[] =
		{
			{L"Autodesk FBX File", L"*.fbx"},
			{L"Colladae", L"*.dae"},
		};

		std::string filePath;

		mEngine->SelectFile(filePath, fileExtensions, 2);
		mEngine->ImportSkinnedModel(filePath);
	}

	if (ImGui::Button("Save Models"))
	{
		//mEngine->SaveModels("Data\\Levels\\default_01.telvl");
	}

	ImGui::SameLine();
	if (ImGui::Button("Load Models"))
	{

		const COMDLG_FILTERSPEC fileExtensions[] =
		{
			{L"TrustEngine Level Files (*.telvl)", L"*.telvl"}
		};

		std::string filePath;

		mEngine->SelectFile(filePath, fileExtensions, 1);

		//mEngine->LoadModels(filePath);
	}

	static std::string selectedModelName = "";

	if (ImGui::BeginCombo("Models", selectedModelName.c_str()))
	{

		mSelectedModel = gModelManager.GetSelectedModel();

		if (ImGui::Selectable("", mSelectedModel == nullptr))
		{
			gModelManager.SelectModel(nullptr);
			selectedModelName = "";
		}

		for (std::pair<const std::string, Model*>& model_pair : gModelManager.GetAllModels())
		{
			bool selected = false;
			if (mSelectedModel != nullptr)
			{
				if (mSelectedModel->mName == model_pair.first)
				{
					selected = true;
				}
			}
			if (ImGui::Selectable(model_pair.first.c_str(), selected))
			{
				gModelManager.SelectModel(model_pair.second);
				selectedModelName = model_pair.first;
			}
		}

		/*if (ImGui::Selectable("", mSelectedModel == nullptr))
		{
			mSelectedModel = nullptr;
		}*/

		/*for (auto m : *gModelManager.GetBasicModels())
		{
			bool selected = false;
			if (mSelectedModel != nullptr)
				if (mSelectedModel->mName == m->mName)
				{
					selected = true;
				}
			if (ImGui::Selectable(m->mName.c_str(), selected))
			{
				mSelectedModel = m;
			}
		}*/

		ImGui::EndCombo();
	}

	if (ImGui::Button("Recompile Shaders"))
	{
		RecompileShaders();
	}

	RenderUI_ModelModification();
	

	pCurrentTechnique->RenderConfigurationUI();


	ImGui::End();


	ImGui::Begin("Render States");

	if (ImGui::DragFloat("Depth Bias", &RenderStates::DepthBias, 1.0f, 0.0001f, 10000.0f))
	{
		RenderStates::InitAll();
	}

	if (ImGui::DragFloat("Slope Scaled Depth Bias", &RenderStates::SlopeScaledDepthBias, 1.0f, 0.0001f, 10000.0f))
	{
		RenderStates::InitAll();
	}

	if (ImGui::DragFloat("Depth Bias Clamp", &RenderStates::DepthBiasClamp, 1.0f, 0.0001f, 10000.0f))
	{
		RenderStates::InitAll();
	}

	ImGui::End();

	gLights.RenderConfigurationUI();
	gCamera_main.RenderConfigurationUI();


	pCurrentTechnique->ProfileGPUTime();

	//Assemble Together Draw Data
	ImGui::Render();
	//Render Draw Data By DirectX Interface
	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
}

//void RenderEngine::RenderUI()
//{
//	//Draw ImGui UI
//	//Start ImGui New Frame
//	ImGui_ImplDX11_NewFrame();
//	ImGui_ImplWin32_NewFrame();
//	ImGui::NewFrame();
//	//Create Default Config Window
//	ImGui::Begin("Configuration");
//
//	std::string currentTechniqueName = "Current Render Technique: ";
//	currentTechniqueName += pCurrentTechnique->GetName();
//
//	ImGui::Text(currentTechniqueName.c_str());
//
//	if (ImGui::BeginCombo("Render Techniques", pCurrentTechnique->GetName().c_str()))
//	{
//
//		if (ImGui::Selectable("Deferred Shading Sync"))
//		{
//			ChangeRenderTechnique(&mRenderTechnique_DeferredShading);
//		}
//
//		if (ImGui::Selectable("Deferred Shading ASync"))
//		{
//			ChangeRenderTechnique(&mRenderTechnique_DeferredShading_2);
//		}
//
//		ImGui::EndCombo();
//	}
//
//	pCurrentTechnique->RenderConfigurationUI();
//
//	//gLights.RenderConfigurationUI();
//
//	ImGui::End();
//	
//	//Assemble Together Draw Data
//	ImGui::Render();
//	//Render Draw Data By DirectX Interface
//	ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
//}

void RenderEngine::RenderUI_ModelModification()
{
	if (mSelectedModel == nullptr)
		return;

	ImGui::Begin("Model");

	ImGui::InputText("Name", &mSelectedModel->mName);

	static float translate[3] = { 0.0f, 0.0f, 0.0f };
	float prevtranslate[3] = { translate[0], translate[1], translate[2] };

	if (ImGui::DragFloat3("Translate", translate, 0.1f))
	{
		auto tMatrix = XMMatrixTranslation(translate[0] - prevtranslate[0], translate[1] - prevtranslate[1], translate[2] - prevtranslate[2]);

		auto worldMatrix = XMLoadFloat4x4A(&mSelectedModel->mWorldMatrix);

		worldMatrix = tMatrix * worldMatrix;

		XMStoreFloat4x4A(&mSelectedModel->mWorldMatrix, worldMatrix);
	}

	static float scale[3] = { 0.0f, 0.0f, 0.0f };
	float prevscale[3] = { scale[0], scale[1], scale[2] };

	if (ImGui::DragFloat3("Scale", scale, 0.1f))
	{
		auto sMatrix = XMMatrixScaling(scale[0] - prevscale[0], scale[1] - prevscale[1], scale[2] - prevscale[2]);

		auto worldMatrix = XMLoadFloat4x4A(&mSelectedModel->mWorldMatrix);

		worldMatrix = sMatrix * worldMatrix;

		XMStoreFloat4x4A(&mSelectedModel->mWorldMatrix, worldMatrix);
	}

	static float rotate[3] = { 0.0f, 0.0f, 0.0f };
	float prevrotate[3] = { rotate[0], rotate[1], rotate[2] };

	if (ImGui::DragFloat3("Rotate", rotate, 0.1f))
	{
		auto rMatrix = XMMatrixRotationRollPitchYaw(rotate[0] - prevrotate[0], rotate[1] - prevrotate[1], rotate[2] - prevrotate[2]);

		auto worldMatrix = XMLoadFloat4x4A(&mSelectedModel->mWorldMatrix);

		worldMatrix = rMatrix * worldMatrix;

		XMStoreFloat4x4A(&mSelectedModel->mWorldMatrix, worldMatrix);
	}

	static float UVTranslate[] = { 0.0f, 0.0f };

	//static IMesh* mSelectedMesh = nullptr;

	if (ImGui::BeginCombo("Meshes", ""))
	{
		if (ImGui::Selectable("", gSelectedMesh == nullptr))
		{
			gSelectedMesh = nullptr;
		}

		for (auto mesh : mSelectedModel->mMeshes)
		{
			bool selected = false;
			if (gSelectedMesh != nullptr)
				if (gSelectedMesh->mName == mesh->mName)
				{
					selected = true;
				}
			if (ImGui::Selectable(mesh->mName.c_str(), selected))
			{
				gSelectedMesh = mesh;
			}
		}

		ImGui::EndCombo();
	}

	if (gSelectedMesh != nullptr)
	{
		static float UVTranslate[] = { 0.0f, 0.0f };

		if (ImGui::DragFloat2("Mesh UV Translate", UVTranslate, 0.05, 0.0f, 1.0f))
		{
			gSelectedMesh->mMatrix_TexTransform._14 = UVTranslate[0];
			gSelectedMesh->mMatrix_TexTransform._24 = UVTranslate[1];
		}

		static float UVScale[] = { 0.0f, 0.0f };

		if (ImGui::DragFloat2("Mesh UV Scale", UVScale, 0.1, 0.0f, 10.0f))
		{
			gSelectedMesh->mMatrix_TexTransform._11 = UVScale[0];
			gSelectedMesh->mMatrix_TexTransform._22 = UVScale[1];
		}

		auto material = gModelManager.gMaterials[gSelectedMesh->GetMatrialIndex()].get();

		ImGui::ColorEdit3("Diffuse Color", &material->mShadingColor.diffuseColor.x);
		ImGui::ColorEdit3("Ambient Color", &material->mShadingColor.ambientColor.x);
		ImGui::ColorEdit3("Specular Color", &material->mShadingColor.specularColor.x);
		ImGui::DragFloat("Specular Power ", &material->mShadingColor.specularColor.w);

	}

	ImGui::End();
}

void RenderEngine::UpdateBuffers()
{
	gLights.UpdateLightBuffers(gContext.Get());


	XMStoreFloat4x4(&CommonConstantBuffers::ComputeShader::mDataPerFrame.gViewInv, gCamera_main.ViewInv());
	XMStoreFloat4(&CommonConstantBuffers::ComputeShader::mDataPerFrame.gEyePerspectiveValues, gCamera_main.GetPerspectiveValues());
	CommonConstantBuffers::ComputeShader::mDataPerFrame.gEyePos = gCamera_main.GetPosition();

	CommonConstantBuffers::ComputeShader::UpdateBuffer_PerFrame(Globals::gContext.Get());
}

void RenderEngine::InitStatics()
{
	//Init Static DirectX Resources
	RenderStates::InitAll();
	RenderStates::SetSamplerStates(gContext.Get());

	Vertex::InitInputLayouts();


	ShaderList::InitAllStatics();

	StaticBuffers::InitStaticBuffers();

	CommonConstantBuffers::ComputeShader::InitBuffers(Globals::gContext.Get());

	//concurrency::task_group tasks;

	////Init Static DirectX Resources
	//tasks.run([]() {RenderStates::InitAll(); });
	//tasks.run([]() {RenderStates::SetSamplerStates(); });

	//tasks.run([]() {Vertex::InitInputLayouts(); });


	//tasks.run([]() {ShaderList::InitAllStatics(); });

	//tasks.run([]() {StaticBuffers::InitStaticBuffers(); });

	//tasks.run([]() {CommonConstantBuffers::ComputeShader::InitBuffers(); });

	//tasks.wait();
}

void RenderEngine::InitRenderTechniques()
{
	//mRenderTechnique_DeferredShading = new RenderTechnique_DeferredShading();
	//mRenderTechnique_DeferredShading_2 = new RenderTechnique_DeferredShading_2();
	mRenderTechnique_DeferredShading.reset(new RenderTechnique_DeferredShading());
	mRenderTechnique_DeferredShading_2.reset(new RenderTechnique_DeferredShading_2());

	mRenderTechnique_DeferredShading->Init();
	mRenderTechnique_DeferredShading_2->Init();

	ChangeRenderTechnique(mRenderTechnique_DeferredShading.get());
}

void RenderEngine::InitSwapChain()
{
	gSwapChain.Init(MSAA_1X, 0);

	//gContext->ClearRenderTargetView(gSwapChain.GetBackBufferRTV(), RGBA{ 0.1f, 0.1f, 0.8f, 1.0f });
}
