#include "stdafx.h"
#include "Globals.h"

#include "ErrorLogger.h"
#include "COMException.h"

using namespace Vertex;
using namespace Globals;


//ID3D11InputLayout* Vertex::CreateInputLayouts(ComPtr<ID3D11Device> gDevice)
//{
//		if (IL_Simple == nullptr)
//		{
//			D3D11_INPUT_ELEMENT_DESC inputElement[1] =
//			{
//				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//			};
//
//			gDevice->CreateInputLayout(inputElement, 1, _vertexBlob->GetBufferPointer(), _vertexBlob->GetBufferSize(), &IL_Simple);
//		}
//		return IL_Simple;
//		break;
//	case VERTEX_TYPE_POINTBILLBOARD:
//		if (IL_PointBillboard == nullptr)
//		{
//			D3D11_INPUT_ELEMENT_DESC inputElement[2] =
//			{
//				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//				{ "SIZE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
//			};
//
//			gDevice->CreateInputLayout(inputElement, 2, _vertexBlob->GetBufferPointer(), _vertexBlob->GetBufferSize(), &IL_PointBillboard);
//		}
//		return IL_PointBillboard;
//		break;
//	case VERTEX_TYPE_BASIC32:
//		if (IL_Basic32 == nullptr)
//		{
//			D3D11_INPUT_ELEMENT_DESC inputElement[3] =
//			{
//				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//				{ "NORMAL_USE_POSSION_FIND_BLOCKER", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//				{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
//			};
//
//			gDevice->CreateInputLayout(inputElement, 3, _vertexBlob->GetBufferPointer(), _vertexBlob->GetBufferSize(), &IL_Basic32);
//		}
//		return IL_Basic32;
//		break;
//	case VERTEX_TYPE_BASIC32_TANGET:
//		if (IL_Basic32_Tanget == nullptr)
//		{
//			D3D11_INPUT_ELEMENT_DESC inputElement[4] =
//			{
//				{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//				{ "NORMAL_USE_POSSION_FIND_BLOCKER", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//				{ "TEXCOORD0", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//				{ "TEXCOORD1", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 }
//			};
//
//			gDevice->CreateInputLayout(inputElement, 4, _vertexBlob->GetBufferPointer(), _vertexBlob->GetBufferSize(), &IL_Basic32_Tanget);
//		}
//		return IL_Basic32_Tanget;
//		break;
//	case VERTEX_TYPE_PARTICLE:
//		if(IL_Particle == nullptr)
//		{
//			D3D11_INPUT_ELEMENT_DESC inputelement[5]
//			{
//				{"InitialPosW"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0},
//				{ "InitialVelW"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//				{ "SizeW"		, 0, DXGI_FORMAT_R32G32_FLOAT	, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//				{ "Age"			, 0, DXGI_FORMAT_R32_FLOAT		, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//				{ "Type"		, 0, DXGI_FORMAT_R32_UINT		, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 }
//			};
//
//			gDevice->CreateInputLayout(inputelement, 5, _vertexBlob->GetBufferPointer(), _vertexBlob->GetBufferSize(), &IL_Particle);
//		}
//		return IL_Particle;
//		break;
//	default:
//		return nullptr;
//	}
//}

namespace Vertex
{

	ComPtr<ID3D11InputLayout> IL_Simple = nullptr;
	ComPtr<ID3D11InputLayout> IL_PointBillboard = nullptr;
	ComPtr<ID3D11InputLayout> IL_Basic32 = nullptr;
	ComPtr<ID3D11InputLayout> IL_Basic32_Tanget = nullptr;
	ComPtr<ID3D11InputLayout> IL_Particle = nullptr;
	ComPtr<ID3D11InputLayout> IL_Skinned = nullptr;


	UINT VertexStride_Simple = sizeof(Vertex::Simple);
	UINT VertexStride_Basic32 = sizeof(Vertex::Basic32);
	UINT VertexStride_Basic32_Tangent = sizeof(Vertex::PosNormalTexTan);
	UINT VertexStride_PointBillboard = sizeof(Vertex::PointBillboard);
	UINT VertexStride_Particle = sizeof(Vertex::Particle);
	UINT VertexStride_Skinned = sizeof(Vertex::Skinned);
}

void Vertex::InitInputLayouts()
{
	try
	{

		ComPtr<ID3DBlob> blob;
		auto hr = D3DReadFileToBlob(L"Graphics/Shaders/DummyShaders/vs_inputSimple.cso", blob.ReleaseAndGetAddressOf());
		COM_ERROR_IF_FAILED(hr, "Failed to create input layout from dummy shaders. simple layout");

		std::vector<D3D11_INPUT_ELEMENT_DESC> inputElement =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};

		gDevice->CreateInputLayout(inputElement.data(), 1, blob->GetBufferPointer(), blob->GetBufferSize(), &IL_Simple);

		hr = D3DReadFileToBlob(L"Graphics/Shaders/DummyShaders/vs_inputPointBillboard.cso", blob.ReleaseAndGetAddressOf());
		COM_ERROR_IF_FAILED(hr, "Failed to create input layout from dummy shaders. pointBillboard layout");

		inputElement =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "SIZE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		gDevice->CreateInputLayout(inputElement.data(), 2, blob->GetBufferPointer(), blob->GetBufferSize(), &IL_PointBillboard);

		hr = D3DReadFileToBlob(L"Graphics/Shaders/DummyShaders/vs_inputBasic32.cso", blob.ReleaseAndGetAddressOf());
		COM_ERROR_IF_FAILED(hr, "Failed to create input layout from dummy shaders. Basic32 layout");

		inputElement =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		gDevice->CreateInputLayout(inputElement.data(), 3, blob->GetBufferPointer(), blob->GetBufferSize(), &IL_Basic32);

		hr = D3DReadFileToBlob(L"Graphics/Shaders/DummyShaders/vs_inputBasic32Tanget.cso", blob.ReleaseAndGetAddressOf());
		COM_ERROR_IF_FAILED(hr, "Failed to create input layout from dummy shaders. Basic32Tangent layout");


		inputElement =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 1, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		gDevice->CreateInputLayout(inputElement.data(), 4, blob->GetBufferPointer(), blob->GetBufferSize(), &IL_Basic32_Tanget);

		hr = D3DReadFileToBlob(L"Graphics/Shaders/DummyShaders/vs_inputParticles.cso", blob.ReleaseAndGetAddressOf());
		COM_ERROR_IF_FAILED(hr, "Failed to create input layout from dummy shaders. particles layout");

		inputElement =
		{
			{"InitialPosW"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0,  D3D11_INPUT_PER_VERTEX_DATA, 0},
			{ "InitialVelW"	, 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "SizeW"		, 0, DXGI_FORMAT_R32G32_FLOAT	, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "Age"			, 0, DXGI_FORMAT_R32_FLOAT		, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "Type"		, 0, DXGI_FORMAT_R32_UINT		, 0, 36, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		gDevice->CreateInputLayout(inputElement.data(), 5, blob->GetBufferPointer(), blob->GetBufferSize(), &IL_Particle);


		//Create Skinned Vertecies Input layouts
		hr = D3DReadFileToBlob(L"Graphics/Shaders/DummyShaders/vs_inputSkinned.cso", blob.ReleaseAndGetAddressOf());
		COM_ERROR_IF_FAILED(hr, "Failed to create input layout from dummy shaders. Skinned Vertecies layout");

		inputElement =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 32, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BONEWEIGHT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 44, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BONEINDEX", 0, DXGI_FORMAT_R8G8B8A8_SINT, 0, 56, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		gDevice->CreateInputLayout(inputElement.data(), 6, blob->GetBufferPointer(), blob->GetBufferSize(), &IL_Skinned);
	}
	catch (COMException ex)
	{
		ErrorLogger::Log(ex);
		exit(-1);
	}

}

ComPtr<ID3D11InputLayout> Vertex::GetInputLayout(VERTEX_TYPE _vertexType)
{
	switch (_vertexType)
	{
	case VERTEX_TYPE_NONE:
		return nullptr;
		break;
	case VERTEX_TYPE_SIMPLE:
		return IL_Simple;
		break;
	case VERTEX_TYPE_POINTBILLBOARD:
		return IL_PointBillboard;
		break;
	case VERTEX_TYPE_BASIC32:
		return IL_Basic32;
		break;
	case VERTEX_TYPE_BASIC32_TANGET:
		return IL_Basic32_Tanget;
		break;
	case VERTEX_TYPE_PARTICLE:
		return IL_Particle;
		break;
	case VERTEX_TYPE_SKINNED:
		return IL_Skinned;
		break;
	}
}

void Vertex::GetVertexStride(VERTEX_TYPE _vertexType, UINT& _stride)
{
	switch (_vertexType)
	{
	case VERTEX_TYPE_NONE:
		_stride = 0;
		break;
	case VERTEX_TYPE_SIMPLE:
		_stride = sizeof(Vertex::Simple);
		break;
	case VERTEX_TYPE_BASIC32:
		_stride = sizeof(Vertex::Basic32);
		break;
	case VERTEX_TYPE_BASIC32_TANGET:
		_stride = sizeof(Vertex::PosNormalTexTan);
		break;
	case VERTEX_TYPE_POINTBILLBOARD:
		_stride = sizeof(Vertex::PointBillboard);
		break;
	case VERTEX_TYPE_PARTICLE:
		_stride = sizeof(Vertex::Particle);
		break;
	case VERTEX_TYPE_SKINNED:
		_stride = sizeof(Vertex::Skinned);
		break;
	}
}

void Vertex::ReleaseMemory()
{
	IL_Simple.Reset();
	IL_Basic32.Reset();
	IL_Basic32_Tanget.Reset();
	IL_PointBillboard.Reset();
	IL_Particle.Reset();
	IL_Skinned.Reset();
}
