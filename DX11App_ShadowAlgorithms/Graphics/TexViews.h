#pragma once



class TexViews
{
public:

	void Init(UINT _bindFlag
		, UINT _height, UINT _width
		, DXGI_FORMAT _format
		, D3D11_USAGE _usageFlag = D3D11_USAGE_DEFAULT
		, D3D11_CPU_ACCESS_FLAG _cpuAccessFlag = static_cast<D3D11_CPU_ACCESS_FLAG>(0)
		, UINT _arraySize = 1
		, UINT _mipLevels = 1
		, UINT _sampleCount = 1
		, UINT _sampleQuality = 0
		, UINT _miscFlag = 0
		, bool isTextureArray = false
		, const D3D11_SUBRESOURCE_DATA* initData = nullptr);

	void BindSRV_PS(UINT _startSlot)const;

	ID3D11ShaderResourceView* GetSRV()const;
	ID3D11ShaderResourceView* const* GetSRV_AddressOf()const;

	ID3D11RenderTargetView* GetRTV(int index = 0)const;

	ID3D11UnorderedAccessView* GetUAV()const;

	ID3D11DepthStencilView* GetDSV(int index = 0)const;

	ID3D11Texture2D* GetTexture()const;

	void ClearRTVs();

	void Release();

private:
	UINT mBindFlag, mWidth, mHeight, mCpuAccessFlag, mArraySize, mMipLevels, mMiscFlags, mSampleCount, mSampleQuality;

	D3D11_USAGE mUsageFlags;

	DXGI_FORMAT mFormat;

	ComPtr<ID3D11Texture2D> mTex;

	ComPtr<ID3D11ShaderResourceView> mSRV;

	std::vector<ComPtr<ID3D11RenderTargetView>> mRTV;

	ComPtr<ID3D11UnorderedAccessView> mUAV;

	std::vector<ComPtr<ID3D11DepthStencilView>> mDSV;

};