#include "stdafx.h"
#include "ResourceViewCollectors.h"

// Add a SRV to Collector
void SRVCollector::AddSRV(ID3D11ShaderResourceView* _srv)
{
	m_srv_vector.push_back(_srv);
	m_srv_count++;
}

void SRVCollector::ClearAndAddSRV(ID3D11ShaderResourceView * _srv, int reserveSize)
{
	m_srv_vector.clear();
	m_srv_count = 0;

	if (reserveSize != -1)
	{
		m_srv_vector.reserve(reserveSize);
	}

	m_srv_vector.push_back(_srv);
	m_srv_count++;
}

// Set Shader Resources To Pipeline Stage
void SRVCollector::BindSRVs_PS(ComPtr<ID3D11DeviceContext>& _context, UINT _startSlot)
{
	_context->PSSetShaderResources(_startSlot, m_srv_count, &m_srv_vector[0]);
}

void SRVCollector::BindSRVs_PS(ComPtr<ID3D11DeviceContext>& _context, UINT _startSlot, UINT _lowerBound)
{
	auto srv_count = m_srv_count - _lowerBound;
	_context->PSSetShaderResources(_startSlot, srv_count, &m_srv_vector[_lowerBound]);
}

void SRVCollector::BindSRVs_PS(ComPtr<ID3D11DeviceContext>& _context, UINT _startSlot, UINT _lowerBound, UINT _upperBound)
{
	auto srv_count = (_upperBound - _lowerBound) + 1;
	_context->PSSetShaderResources(_startSlot, srv_count, &m_srv_vector[_lowerBound]);
}

void SRVCollector::BindSRVs_CS(ComPtr<ID3D11DeviceContext>& _context, UINT _startSlot)
{
	_context->CSSetShaderResources(_startSlot, m_srv_count, &m_srv_vector[0]);
}

void SRVCollector::ClearCollector()
{
	m_srv_vector.clear();
	m_srv_vector.shrink_to_fit();
	m_srv_count = 0;
}

void SRVCollector::ReplaceSRV(ID3D11ShaderResourceView * _srv, UINT _prev_num)
{
	if (_prev_num < m_srv_count)
	{
		m_srv_vector[_prev_num] = _srv;
	}
}


//
////////////////////////////////////////////////////// RTV Collector
//



void RTVCollector::AddRTV(ID3D11RenderTargetView* _rtv)
{
	m_vector_rtv.push_back(_rtv);
	m_size++;
}

void RTVCollector::ClearAndAddRTV(ID3D11RenderTargetView * _rtv, int reserveSize)
{
	m_vector_rtv.clear();

	if (reserveSize != -1)
	{
		m_vector_rtv.reserve(reserveSize);
	}

	m_size = 0;

	m_vector_rtv.push_back(_rtv);
	m_size++;
}

void RTVCollector::ClearRTVs(ID3D11DeviceContext* _context, RGBA Color)
{
	for (auto rtv : m_vector_rtv)
	{
		if (rtv != nullptr)
			_context->ClearRenderTargetView(rtv, Color);
	}
}

void RTVCollector::ClearCollector()
{
	m_vector_rtv.clear();
	m_vector_rtv.shrink_to_fit();
	m_size = 0;
}

ID3D11RenderTargetView ** RTVCollector::GetRTVs()
{
	return m_vector_rtv.data();
}

size_t RTVCollector::GetSize()
{
	return m_size;
}

void RTVCollector::ReplaceRTV(ID3D11RenderTargetView * _rtv, UINT _prev_num)
{
	if (_prev_num < m_size)
	{
		m_vector_rtv[_prev_num] = _rtv;
	}
}