#include "stdafx.h"
#include "Material.h"
#include "Objects/Mesh.h"
#include "Texture.h"


Material::Material() : ID(-1)
, mName("")
, mShaderClassInstance_material(SURFACE_SHADER_CLASSINSTANCES_MATERIAL_BASIC)
, mShaderClassInstance_normal(SURFACE_SHADER_CLASSINSTANCES_NORMAL_VECTOR)
, mDiffuseTextureKey("")
, mNormalTextureKey("")
, mDisplacementTextureKey("")
, IsTransparent(false)
{
	vSRVs.resize(3);
}

void Material::SetDiffueMap(Texture* diffusem, std::string& DiffuseTextureKey)
{
	p_diffusemap = diffusem;
	vSRVs[0] = p_diffusemap->GetSRV();

	mDiffuseTextureKey = DiffuseTextureKey;
}

void Material::SetNormalMap(Texture* normalm, std::string& NormalTextureKey)
{
	p_normalmap = normalm;
	vSRVs[1] = p_normalmap->GetSRV();

	mNormalTextureKey = NormalTextureKey;
}

void Material::SetDisplacementMap(Texture* displacementm, std::string& DisplacementTextureKey)
{
	p_displacementmap = displacementm;
	vSRVs[2] = p_displacementmap->GetSRV();

	mDisplacementTextureKey = DisplacementTextureKey;
}

void Material::SetShaderClass_Material(SURFACE_SHADER_CLASSINSTANCES_MATERIAL shaderClass)
{
	mShaderClassInstance_material = shaderClass;
}

void Material::SetShaderClass_Normal(SURFACE_SHADER_CLASSINSTANCES_NORMAL shaderClass)
{
	mShaderClassInstance_normal = shaderClass;
}


void Material::SetShaderResources_PS(UINT SRVSlot_diffuse_normal_displacement, ID3D11DeviceContext* context)
{
	context->PSSetShaderResources(SRVSlot_diffuse_normal_displacement, vSRVs.size(), vSRVs.data());
}

SURFACE_SHADER_CLASSINSTANCES_MATERIAL Material::GetSurfaceShaderClassInstance_Material()
{
	return mShaderClassInstance_material;
}

SURFACE_SHADER_CLASSINSTANCES_NORMAL Material::GetSurfaceShaderClassInstance_Normal()
{
	return mShaderClassInstance_normal;
}

void Material::AddMesh(IMesh* mesh)
{
	mMeshes.push_back(mesh);
}

const std::vector<IMesh*>& Material::GetMeshes()
{
	return mMeshes;
}
