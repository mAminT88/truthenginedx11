#pragma once


//--------------------------------------------------------------------------------------
// Technique : Deferred Shading
//--------------------------------------------------------------------------------------

/*Vertex Shader*/


///**Constant Buffer**/
#define SHADER_SLOTS_VS_CB_SPOTLIGHTS                                    2 
#define SHADER_SLOTS_VS_CB_DIRLIGHTS                                     3
#define SHADER_SLOTS_VS_CB_GENERATEGBUFFERS_PEROBJECT                    5
#define SHADER_SLOTS_VS_CB_GENERATEGBUFFERS_PERMESH                      6
#define SHADER_SLOTS_VS_CB_RENDER2D                                      7
#define SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PEROBJECT                   8
#define SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PERLIGHT                    9
#define SHADER_SLOTS_VS_CB_OSSSS_GENERATESHADOWMAP                       10
#define SHADER_SLOTS_VS_CB_BONETRANSFORMS                                11
#define SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_BONETRANSFORMS              12

/*Pixel Shader*/


///Constant Buffer
#define SHADER_SLOTS_PS_CB_PER_FRAME                    0
#define SHADER_SLOTS_PS_CB_UNFREQ                       1
#define SHADER_SLOTS_PS_CB_SPOTLIGHTS                   2
#define SHADER_SLOTS_PS_CB_DIRLIGHTS                    3


#define SHADER_SLOTS_PS_CB_GENERATEGBUFFERS_PEROBJECT	5
#define SHADER_SLOTS_PS_CB_VSM_FILTERSHADOWMAP	        6
#define SHADER_SLOTS_PS_CB_GENERATESAT	                6
#define SHADER_SLOTS_PS_CB_OSSSS_GENERATESHADOWDATA	    6
#define SHADER_SLOTS_PS_CB_SSSM_GENERATESHADOWDATA	    6

#define SHADER_SLOTS_PS_CB_MSM_PARAMETERS               7
#define SHADER_SLOTS_PS_CB_SSSM_PERFRAME                7

/// ShaderResourceView
#define SHADER_SLOTS_PS_TEX_GENERATEGBUFFERS_DIFFUSEMAP 0
#define SHADER_SLOTS_PS_TEX_GENERATEGBUFFERS_NORMALMAP 1


/*Compute Shader*/

#define SHADER_SLOTS_CS_CB_PERFRAME   0
#define SHADER_SLOTS_CS_CB_UNFREQUENT 1


#define SHADER_SLOTS_CS_CB_GENERATESAT_PERLIGHT 5




//////////////////////////////////////////////////////////////////////////
//HLSL SHADER SLOTS
//////////////////////////////////////////////////////////////////////////

/*Vertex Shader*/


///**Constant Buffer**/
#define HLSL_SHADER_SLOTS_VS_CB_SPOTLIGHTS                                    b2
#define HLSL_SHADER_SLOTS_VS_CB_DIRLIGHTS                                     b3
#define HLSL_SHADER_SLOTS_VS_CB_GENERATEGBUFFERS_PEROBJECT                    b5
#define HLSL_SHADER_SLOTS_VS_CB_GENERATEGBUFFERS_PERMESH                      b6
#define HLSL_SHADER_SLOTS_VS_CB_RENDER2D                                      b7
#define HLSL_SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PEROBJECT                   b8
#define HLSL_SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PERLIGHT                    b9
#define HLSL_SHADER_SLOTS_VS_CB_OSSSS_GENERATESHADOWMAP                       b10
#define HLSL_SHADER_SLOTS_VS_CB_BONETRANSFORMS                                b11
#define HLSL_SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_BONETRANSFORMS              b12

/*Pixel Shader*/


///Constant Buffer
#define HLSL_SHADER_SLOTS_PS_CB_PER_FRAME                    b0
#define HLSL_SHADER_SLOTS_PS_CB_UNFREQ                       b1
#define HLSL_SHADER_SLOTS_PS_CB_SPOTLIGHTS                   b2
#define HLSL_SHADER_SLOTS_PS_CB_DIRLIGHTS                    b3
		

#define HLSL_SHADER_SLOTS_PS_CB_GENERATEGBUFFERS_PEROBJECT	b5
#define HLSL_SHADER_SLOTS_PS_CB_VSM_FILTERSHADOWMAP	        b6
#define HLSL_SHADER_SLOTS_PS_CB_GENERATESAT	                b6
#define HLSL_SHADER_SLOTS_PS_CB_OSSSS_GENERATESHADOWDATA	b6
#define HLSL_SHADER_SLOTS_PS_CB_SSSM_GENERATESHADOWDATA	    b6

#define HLSL_SHADER_SLOTS_PS_CB_MSM_PARAMETERS	            b7
#define HLSL_SHADER_SLOTS_PS_CB_SSSM_PERFRAME               b7

/// ShaderResourceView
#define HLSL_SHADER_SLOTS_PS_TEX_GENERATEGBUFFERS_DIFFUSEMAP 0
#define HLSL_SHADER_SLOTS_PS_TEX_GENERATEGBUFFERS_NORMALMAP 1


/***********************************TEXTURES***************************************/

#define HLSL_SHADER_SLOTS_PS_TEX_DIFFUSEMAP	     t0
#define HLSL_SHADER_SLOTS_PS_TEX_NORMALMAP	     t1
#define HLSL_SHADER_SLOTS_PS_TEX_SPECULARMAP	 t2