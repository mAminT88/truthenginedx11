#pragma once



class RenderText
{
public:
	RenderText(HWND hwnd, ComPtr<IDXGISurface> dxgiSurface);

	/*void Init(HWND hwnd, IDXGISurface *dxgiSurface);*/
	/*void Destroy();*/

	void Render(const wchar_t *text);
	void ResetTextPosition();
	void ChangeColor(D2D1::ColorF _color);
private:
	HWND mhwnd;
	RECT rc;

	ComPtr<IDXGISurface> mDXGISurface;

	ComPtr<IDWriteFactory> mDWriteFactory;
	ComPtr<IDWriteTextFormat> mTextFormat;
	ComPtr<IDWriteTextLayout> mTextLayout;

	const wchar_t *wsztext;
	UINT32 mTextLength;
	FLOAT originX = 10.0f, originY = 10.0f;

	ComPtr<ID2D1Factory> mD2DFactory;
	ComPtr<ID2D1HwndRenderTarget> mRT;
	ComPtr<ID2D1RenderTarget> mDXGIRT;
	ComPtr<ID2D1SolidColorBrush> mSolidColorBrush;

	void CreateDeviceIndependentResource();

	void CreateDeviceResource();
	void DestroyDeviceResource();

	FLOAT ConvertPointSizeToDIP(FLOAT points);
};

