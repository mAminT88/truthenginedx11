#pragma once

#include "Material.h"
#include "Mesh.h"
#include "MathHelper.h"

class Objects
{
protected:


	void CalcWorldInvTranspose()
	{
		WorldInvTransposeMatrix = MathHelper::InverseTranspose(&WorldMatrix);
	}



public:

	Objects(): Name(""), ID(-1), pMaterial(nullptr), Disabled(false), GetLight(true), CastShadow(true), WorldMatrix(MathHelper::IdentityMatrix), WorldInvTransposeMatrix(MathHelper::IdentityMatrix)
	{
	}

	std::string Name;


	int ID;


	Material* pMaterial;
	
	Mesh::Mesh_3D* pMesh;

	XMFLOAT4X4A WorldMatrix;
	XMFLOAT4X4A WorldInvTransposeMatrix;


	bool Disabled;
	bool GetLight;
	bool CastShadow;
	
	

};

class Objects_Dynamic : public Objects
{

public:

	Objects_Dynamic(): Objects(), Updated(false)
	{
	}

	bool Updated;

};

class Objects_Static : public Objects
{

};
