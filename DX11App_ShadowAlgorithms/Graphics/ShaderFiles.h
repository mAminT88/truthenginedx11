#pragma once

//--------------------------------------------------------------------------------------
// Shader Target Version
//--------------------------------------------------------------------------------------

#define SHADER_TARGET_VERSION "5_0"

//--------------------------------------------------------------------------------------
// Vertex Shaders
//--------------------------------------------------------------------------------------

#define SHADER_FILE_VS_PROCESSMESH L"Graphics/Shaders/VS/vs_processMesh.hlsl"
#define SHADER_FILE_VS_SHADOWMAP_BASIC32VERTEX L"Graphics/Shaders/VS/vs_GenerateShadowMap.hlsl"
#define SHADER_FILE_VS_RENDER2D L"Graphics/Shaders/VS/vs_Render2D.hlsl"
#define SHADER_FILE_VS_OSSSS_BUILDSHADOWMAP L"Graphics/Shaders/VS/vs_OSSSS_GenerateShadowMap.hlsl"

//--------------------------------------------------------------------------------------
// Pixel Shaders
//--------------------------------------------------------------------------------------

#define SHADER_FILE_PS_GENERATEGBUFFERS L"Graphics/Shaders/PS/ps_GenerateGBuffers.hlsl"
#define SHADER_FILE_PS_DEFERREDSHADING L"Graphics/Shaders/PS/ps_DeferredShading.hlsl"
#define SHADER_FILE_PS_GENERATEVARIANCESHADOWMAP L"Graphics/Shaders/PS/ps_GenerateVarianceShdaowMap.hlsl"
#define SHADER_FILE_PS_SEPERABLEGAUSSIANFILTER L"Graphics/Shaders/PS/ps_SeperableGaussianFilter.hlsl"
#define SHADER_FILE_PS_GENERATESAT L"Graphics/Shaders/PS/ps_GenerateSAT.hlsl"
#define SHADER_FILE_PS_GENERATEMOMENTSHADOWMAP L"Graphics/Shaders/PS/ps_GenerateMomentShadowMap.hlsl"
#define SHADER_FILE_PS_OSSSS_GENERATESHADOWDATA L"Graphics/Shaders/PS/ps_OSSSS_GenerateShadowData.hlsl"
#define SHADER_FILE_PS_OSSSS_GENERATESHADOWDATA_SCREENSPACESEPARABLEBLOCKERSEARCH L"Graphics/Shaders/PS/ps_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch.hlsl"
#define SHADER_FILE_PS_OSSSS_GENERATESHADOWMAP L"Graphics/Shaders/PS/ps_OSSSS_GenerateShadowMap.hlsl"
#define SHADER_FILE_PS_SSSM_GENERATESHADOWDATA L"Graphics/Shaders/PS/ps_SSSM_GenerateShadowData.hlsl"
#define SHADER_FILE_PS_DOWNUPSAMPLING L"Graphics/Shaders/PS/ps_DownUpSampling.hlsl"
#define SHADER_FILE_PS_BLURBILATERALBYDEPTHNORMAL L"Graphics/Shaders/PS/ps_blurBilateralByDepthNormal.hlsl"
#define SHADER_FILE_PS_SSAO L"Graphics/Shaders/PS/ps_SSAO.hlsl"
#define SHADER_FILE_PS_POSTFX_FINALPASS L"Graphics/Shaders/PS/ps_PostFX_FinalPass.hlsl"

//--------------------------------------------------------------------------------------
// Compute Shaders
//--------------------------------------------------------------------------------------

#define SHADER_FILE_CS_OSSSS_SEPARABLEFindBlocker L"Graphics/Shaders/CS/cs_separableFindBlocker.hlsl"
#define SHADER_FILE_CS_OSSSS_GENERATESAT L"Graphics/Shaders/CS/cs_generateSummedAreaTable.hlsl"
#define SHADER_FILE_CS_HDR_AvgLuminance L"Graphics/Shaders/CS/cs_HDR_AvgLuminance.hlsl"
#define SHADER_FILE_CS_DOWNSAMPLING L"Graphics/Shaders/CS/cs_downSampling.hlsl"
#define SHADER_FILE_CS_BLOOM L"Graphics/Shaders/CS/cs_Bloom.hlsl"
#define SHADER_FILE_CS_GAUSSIANBLUR L"Graphics/Shaders/CS/cs_GaussianBlur.hlsl"

