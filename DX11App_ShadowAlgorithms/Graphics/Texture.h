#pragma once

//class ScratchImage;

#define USE_DIRECTXTEX


enum Texture_TYPE
{
	TEXTURE_2D,
	TEXTURE_2D_ARRAY
};

enum TEXTURE_FORMAT
{
	TEXTURE_FORMAT_DDS,
	TEXTURE_FORMAT_WIC
};


class Texture
{
public:
	int ID;


	Texture();

#ifdef USE_DIRECTXTEX
	Texture(const std::wstring& _filePath, TEXTURE_FORMAT _textureFormat = TEXTURE_FORMAT_WIC);
	Texture(const uint8_t* data, int width, int height, TEXTURE_FORMAT _textureFormat = TEXTURE_FORMAT_WIC);
	Texture(Texture&& texture);
	Texture(const Texture& texture);

	void InitTextureFromFile(const std::wstring& _filePath, TEXTURE_FORMAT _textureFormat = TEXTURE_FORMAT_WIC);
	void InitTextureFromMemory(const uint8_t* data, size_t dataSize, TEXTURE_FORMAT _textureFormat = TEXTURE_FORMAT_WIC);

	void InitTextureFromWICFile(const std::wstring& _filePath);
	void InitTextureFromWICMemory(const uint8_t* data, size_t dataSize);
	void InitTextureFromDDSFile(const std::wstring& _filePath);
	void InitTextureFromDDSMemory(const uint8_t* data, size_t dataSize);
#else
	Texture(const std::wstring _filePath, Texture_TYPE _textureType, TEXTURE_FORMAT _textureFormat = TEXTURE_FORMAT_WIC);
	Texture(const uint8_t* data, int width, int height, Texture_TYPE _textureType, TEXTURE_FORMAT _textureFormat = TEXTURE_FORMAT_WIC);

	void InitTextureFromFile(std::wstring _filePath, Texture_TYPE _textureType, TEXTURE_FORMAT _textureFormat = TEXTURE_FORMAT_WIC);
	void InitTextureFromMemory(const uint8_t* data, size_t dataSize, Texture_TYPE _textureType, TEXTURE_FORMAT _textureFormat = TEXTURE_FORMAT_WIC);
#endif
	   

	void SetSRV(const UINT _registerSlot)const noexcept;
	void Release() noexcept;

	ID3D11ShaderResourceView* GetSRV() const noexcept;

	const std::wstring& GetFilePath() const noexcept;

private:

#ifdef  USE_DIRECTXTEX
	std::wstring mFilePath;
	ComPtr<ID3D11ShaderResourceView> mSRV;
	std::shared_ptr<ScratchImage> mScratchImage;
#else
	std::wstring mFilePath;
	ComPtr<ID3D11ShaderResourceView> mSRV;
	std::shared_ptr<uint8_t> mData;
	size_t mDataSize;
	Texture_TYPE mType;
	TEXTURE_FORMAT mFormat;

#endif


#pragma region Serialization 

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive& ar, const unsigned int version) {
		ar& ID;
		ar& mFilePath;
		/*ar& mType;
		ar& mFormat;
		ar& mDataSize;

		uint8_t* data = new uint8_t[mDataSize];

		for (int i = 0; i < mDataSize; ++i)
		{
			ar& *(data + i);
		}

		InitTextureFromMemory(data, mDataSize, mType, mFormat);*/
	}

	template<class Archive>
	void save(Archive& ar, const unsigned int version) const {
		ar& ID;
		ar& mFilePath;
		/*ar& mType;
		ar& mFormat;
		ar& mDataSize;

		for (int i = 0; i < mDataSize; ++i)
		{
			ar& *(mData.get() + i);
		}*/
	}

	BOOST_SERIALIZATION_SPLIT_MEMBER();
#pragma endregion Serialization


};
