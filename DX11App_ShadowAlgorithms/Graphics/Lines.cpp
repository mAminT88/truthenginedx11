#include "stdafx.h"
#include "Lines.h"

XMFLOAT4 & Line2D::GetColor()
{
	return m_color;
}

XMMATRIX & Line2D::GetWorldMat()
{
	return WorldMatrix;
}
