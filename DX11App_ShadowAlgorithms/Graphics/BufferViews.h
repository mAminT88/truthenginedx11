#pragma once



class BufferViews
{
public:

	void Init(UINT _bindFlag
		, UINT _structureByteStride, UINT _byteWidth, UINT _numElement
		, D3D11_USAGE _usageFlag = D3D11_USAGE_DEFAULT
		, D3D11_CPU_ACCESS_FLAG _cpuAccessFlag = static_cast<D3D11_CPU_ACCESS_FLAG>(0)
		, UINT _miscFlag = 0
		, const D3D11_SUBRESOURCE_DATA* initData = nullptr);

	ID3D11ShaderResourceView* GetSRV()const;
	ID3D11ShaderResourceView* const* GetSRV_AddressOf()const;
	ID3D11UnorderedAccessView* GetUAV()const;
	ID3D11UnorderedAccessView*const* GetUAV_AddressOf()const;
	ID3D11Buffer* GetBuffer()const;

	void Release();

private:
	UINT mBindFlag, mStructureByteStride, mByteWidth, mCpuAccessFlag, mMiscFlags;

	D3D11_USAGE mUsageFlags;

	DXGI_FORMAT mFormat;

	ComPtr<ID3D11Buffer> mBuffer;

	ComPtr<ID3D11ShaderResourceView> mSRV;

	ComPtr<ID3D11UnorderedAccessView> mUAV;

};
