#include "stdafx.h"
#include "Lights.h"
#include "MathHelper.h"
#include "StaticInstances.h"

enum LightProperty
{
	View,
	ViewProj,
	ShadowTransform,
	Diffuse,
	Ambient,
	Specular,
	Direction,
	LightSize,
	Position,
	zNear,
	zFar,
	CastShadow,
	PerpectiveValues,
	Range,
	SpotPow,
	Attenuation,
	FrustumWidth
};

const XMFLOAT4X4 ILightObj::mProjToUV = XMFLOAT4X4(0.5f, 0.0f, 0.0f, 0.0f,
	0.0f, -0.5, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.5f, 0.5f, 0.0f, 1.0f);


void ILightObj::SetDirection(XMFLOAT3 _direction)
{
	ILightData->Direction = _direction;
	mUpdated_View = true;
	mUpdated = true;
}


void ILightObj::SetLightTarget(XMFLOAT3 _targetPosition)
{
	mLightTarget = XMFLOAT4(_targetPosition.x, _targetPosition.y, _targetPosition.z, 1.0);
	mUpdated_View = true;
	mUpdated = true;
}


void ILightObj::SetPosition(XMFLOAT3 _position)
{
	ILightData->Position = _position;
	mUpdated_View = true;
	mUpdated = true;
}


void ILightObj::SetUpVector(XMFLOAT4 _up)
{
	mUpVector = _up;
	mUpdated_View = true;
	mUpdated = true;
}


void ILightObj::UpdateAmbientColor(XMFLOAT4 _ambientColor)
{
	ILightData->Ambient = _ambientColor;
}


ILightObj::ILightObj() : mUpdated(false), mLightTarget(XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f)), mUpVector(XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f)), Name(L""), bDisabled(false)
{
}


ILightObj::~ILightObj()
{
}


ILight& ILightObj::GetILightData() const
{
	return *ILightData;
}


void ILightObj::UpdateDiffuseColor(XMFLOAT4 _diffuseColor)
{
	ILightData->Diffuse = _diffuseColor;
}


void ILightObj::UpdateShadowTransformMatrix()
{
	auto viewProj = XMLoadFloat4x4(&ILightData->ViewProj);
	auto projToUV = XMLoadFloat4x4(&mProjToUV);

	auto shadowTransform = XMMatrixMultiply(viewProj, projToUV);
	XMStoreFloat4x4(&ILightData->ShadowTransform, shadowTransform);
}


void ILightObj::UpdateSpecularColor(XMFLOAT4 _specularColor)
{
	ILightData->Specular = _specularColor;
}


void ILightObj::UpdateViewMatrix()
{
	XMVECTOR EyePosition = XMLoadFloat3(&ILightData->Position);
	XMVECTOR EyeDirection = XMLoadFloat3(&ILightData->Direction);

	auto UpVector = XMLoadFloat4(&mUpVector);

	auto viewMatrix = XMMatrixLookToLH(EyePosition, EyeDirection, UpVector);
	XMStoreFloat4x4(&ILightData->View, viewMatrix);
}


void ILightObj::UpdateViewProjMatrix()
{
	auto viewMatrix = XMLoadFloat4x4(&ILightData->View);
	auto projMatrix = XMLoadFloat4x4(&mProjMatrix);

	auto viewProjMatrix = viewMatrix * projMatrix;
	XMStoreFloat4x4(&ILightData->ViewProj, viewProjMatrix);

	CreateBoundingFrustum();
	UpdateShadowTransformMatrix();
}


void ILightObj::UpdateViewMatrix(XMFLOAT4X4 _view)
{
	ILightData->View = _view;
}


void ILightObj::UpdateViewMatrix(XMFLOAT4 _position, XMFLOAT4 _direction, XMFLOAT4 _up)
{
	auto position = XMLoadFloat4(&_position);
	auto direction = XMLoadFloat4(&_direction);
	auto up = XMLoadFloat4(&_up);

	auto viewMatrix = XMMatrixLookToLH(position, direction, up);
	XMStoreFloat4x4(&ILightData->View, viewMatrix);
}


void ILightObj::SetCastShadow(bool _castshadow)
{
	ILightData->CastShadow = _castshadow ? 1 : 0;
}


void ILightObj::CreateBoundingFrustum()
{
	auto projMatrix = XMLoadFloat4x4(&mProjMatrix);

	BoundingFrustum::CreateFromMatrix(mBoundingFrustum, projMatrix);

	auto viewMatrix = XMLoadFloat4x4(&ILightData->View);

	auto InvView = XMMatrixInverse(nullptr, viewMatrix);
	mBoundingFrustum.Transform(mBoundingFrustum, InvView);
}


void ILightObj::UpdateViewProjMatrix(XMFLOAT4X4 _view, XMFLOAT4X4 _proj)
{
	ILightData->View = _view;
	mProjMatrix = _proj;

	auto viewMatrix = XMLoadFloat4x4(&_view);
	auto projMatrix = XMLoadFloat4x4(&_proj);

	auto viewProjMatrix = viewMatrix * projMatrix;

	XMStoreFloat4x4(&ILightData->ViewProj, viewProjMatrix);

	//Update perspective Values

	ILightData->PerpectiveValues = XMFLOAT4(mProjMatrix._11, mProjMatrix._22, mProjMatrix._33, mProjMatrix._43);

	auto projToUV = XMLoadFloat4x4(&mProjToUV);

	auto shadowTransform = viewProjMatrix * projToUV;

	XMStoreFloat4x4(&ILightData->ShadowTransform, shadowTransform);

	CreateBoundingFrustum();
	UpdateShadowTransformMatrix();
}


ContainmentType ILightObj::BoundingBoxContainment(BoundingBox _boundingBox)
{
	return mBoundingFrustum.Contains(_boundingBox);
}



int ILightObj::GetIndex()
{
	return mIndex;
}


LIGHT_TYPE ILightObj::GetLightType()
{
	return mLightType;
}


void ILightObj::SetUpdated(bool _updated)
{
	mUpdated = _updated;
}


void ILightObj::Update(const float & dt)
{
	mFunc_Update(dt);

	if (mUpdated)
	{
		if (mUpdated_View)
			UpdateViewMatrix();
		if (mUpdated_Proj)
			UpdateProjectionMatrix();
		if (mUpdated_View || mUpdated_Proj)
		{
			UpdateViewProjMatrix();
			mUpdated_Proj = false;
			mUpdated_View = false;
		}
	}
}


bool ILightObj::UpdatedFromLastFrame()
{
	return mUpdated;
}

/*
*
******************************* Directional Light
*
*/

DirectionalLightObj::DirectionalLightObj(DirectionalLight* lightData, XMFLOAT4 _diffusecolor, XMFLOAT4 _ambientColor, XMFLOAT4 _specularColor, XMFLOAT4 _target, XMFLOAT3 _direction, XMFLOAT3 _position, std::string _name, float _lightSize, float _znear, float _zfar, float _frustumRadius, int _castShadow, int _index, int _id, int _shadowMapID)
{
	/*ILightData = std::make_shared<DirectionalLight>();

	LightData.reset(static_cast<DirectionalLight*>(ILightData.get()));*/

	//LightData = static_cast<DirectionalLight&>(ILightData);

	LightData = lightData;
	ILightData = lightData;

	mIndex = _index;
	mLightType = LIGHT_TYPE_DIRECTIONAL;
	bDisabled = false;


	LightData->Diffuse = _diffusecolor;
	LightData->Ambient = _ambientColor;
	LightData->Specular = _specularColor;
	LightData->LightSize = _lightSize;
	LightData->zNear = _znear;
	LightData->zFar = _zfar;
	LightData->Position = _position;
	LightData->Direction = _direction;
	LightData->CastShadow = _castShadow;
	LightData->ID = _id;
	LightData->ShadowMapID = _shadowMapID;

	mFrustumRadius = _frustumRadius;
	mLightTarget = _target;

	Name = std::wstring(_name.begin(), _name.end());

	UpdateViewMatrix();
	UpdateProjectionMatrix();
	UpdateViewProjMatrix();
}


DirectionalLightObj::DirectionalLightObj(DirectionalLight* lightData)
{
	LightData = lightData;
	ILightData = lightData;

	mLightType = LIGHT_TYPE_DIRECTIONAL;
	bDisabled = false;

	LightData->Diffuse = XMFLOAT4();
	LightData->Ambient = XMFLOAT4();
	LightData->Specular = XMFLOAT4();
	LightData->LightSize = 0.0f;
	LightData->zNear = 10.0f;
	LightData->zFar = 100.0f;
	LightData->Position = XMFLOAT3(10.0f, 10.0f, 10.0f);
	LightData->Direction = XMFLOAT3(1.0f, 1.0f, 1.0f);
	LightData->CastShadow = 0;
	LightData->ID = 0;
	LightData->ShadowMapID = -1;

	mFrustumRadius = 50.0f;
	mLightTarget = XMFLOAT4();

	Name = L"";
}

//DirectionalLightObj::DirectionalLightObj()
//{
//	/*ILightData = std::make_shared<DirectionalLight>();
//
//	LightData.reset(static_cast<DirectionalLight*>(ILightData.get()));*/
//	//LightData = static_cast<DirectionalLight&>(ILightData);
//
//	if (LightData)
//		delete LightData;
//
//	mLightType = LIGHT_TYPE_DIRECTIONAL;
//	bDisabled = false;
//
//	LightData->Diffuse = XMFLOAT4();
//	LightData->Ambient = XMFLOAT4();
//	LightData->Specular = XMFLOAT4();
//	LightData->LightSize = 0.0f;
//	LightData->zNear = 10.0f;
//	LightData->zFar = 100.0f;
//	LightData->Position = XMFLOAT3(10.0f, 10.0f, 10.0f);
//	LightData->Direction = XMFLOAT3(1.0f, 1.0f, 1.0f);
//	LightData->CastShadow = 0;
//	LightData->ID = 0;
//	LightData->ShadowMapID = -1;
//
//	mFrustumRadius = 50.0f;
//	mLightTarget = XMFLOAT4();
//
//	Name = L"";
//}


DirectionalLightObj::~DirectionalLightObj()
{

}


void DirectionalLightObj::UpdateFrustum(float _znear, float _zfar, float _frustumRadius = -1.0f)
{
	LightData->zNear = _znear;
	LightData->zFar = _zfar;

	mFrustumRadius = _frustumRadius == -1.0f ? mFrustumRadius : _frustumRadius;
}


void DirectionalLightObj::SetLightData(DirectionalLight* directionalLightData)
{
	LightData = directionalLightData;

	ILightData = directionalLightData;
}

void DirectionalLightObj::UpdateProjectionMatrix()
{
	/*XMFLOAT3 sphereCenterLS;
	XMStoreFloat3(&sphereCenterLS, XMVector3TransformCoord(mLightTarget, LightData->View));

	float l = sphereCenterLS.x - mFrustumRadius;
	float r = sphereCenterLS.x + mFrustumRadius;
	float b = sphereCenterLS.y - mFrustumRadius;
	float t = sphereCenterLS.y + mFrustumRadius;
	float n = sphereCenterLS.z - mFrustumRadius;
	float f = sphereCenterLS.z + mFrustumRadius;

	mProjMatrix = XMMatrixOrthographicOffCenterLH(l, r, b, t, n, f);*/

	auto projMatrix = XMMatrixOrthographicLH(mFrustumRadius * 2, mFrustumRadius * 2, LightData->zNear, LightData->zFar);

	XMStoreFloat4x4(&mProjMatrix, projMatrix);

	LightData->PerpectiveValues = XMFLOAT4(mProjMatrix._11, mProjMatrix._22, mProjMatrix._33, mProjMatrix._43);
}


std::vector<std::tuple<std::string, int, std::vector<float>>> DirectionalLightObj::GetEditableProperties()
{
	std::vector<std::tuple<std::string, int, std::vector<float>>> PropertiesName;
	PropertiesName.push_back(std::make_tuple("DiffuseColor(float4)", (int)LightProperty::Diffuse, std::vector<float>({ LightData->Diffuse.x, LightData->Diffuse.y, LightData->Diffuse.z, LightData->Diffuse.w })));
	PropertiesName.push_back(std::make_tuple("AmbientColor(float4)", (int)LightProperty::Ambient, std::vector<float>({ LightData->Ambient.x, LightData->Ambient.y, LightData->Ambient.z, LightData->Ambient.w })));
	PropertiesName.push_back(std::make_tuple("SpecularColor(float4)", (int)LightProperty::Specular, std::vector<float>({ LightData->Specular.x, LightData->Specular.y, LightData->Specular.z, LightData->Specular.w })));
	PropertiesName.push_back(std::make_tuple("LightSize(UNORM)", (int)LightProperty::LightSize, std::vector<float>({ LightData->LightSize })));
	PropertiesName.push_back(std::make_tuple("zNear(float)", (int)LightProperty::zNear, std::vector<float>({ LightData->zNear })));
	PropertiesName.push_back(std::make_tuple("zFar(float)", (int)LightProperty::zFar, std::vector<float>({ LightData->zFar })));
	PropertiesName.push_back(std::make_tuple("CastShadow(bit)", (int)LightProperty::CastShadow, std::vector<float>({ (float)LightData->CastShadow })));
	PropertiesName.push_back(std::make_tuple("FrustumWidth(float)", (int)LightProperty::FrustumWidth, std::vector<float>({ (float)mFrustumRadius })));
	return PropertiesName;
}



void DirectionalLightObj::EditProperty(int _propIndex, float* value)
{
	switch (_propIndex)
	{
	case LightProperty::Specular:
	{
		LightData->Specular = XMFLOAT4(value[0], value[1], value[2], value[3]);
		break;
	}
	case LightProperty::Ambient:
	{
		LightData->Ambient = XMFLOAT4(value[0], value[1], value[2], value[3]);
		break;
	}
	case LightProperty::Diffuse:
	{
		LightData->Diffuse = XMFLOAT4(value[0], value[1], value[2], value[3]);
		break;
	}
	case LightProperty::CastShadow:
	{
		LightData->CastShadow = (int)value[0];
		break;
	}
	case LightProperty::LightSize:
	{
		LightData->LightSize = value[0];
		break;
	}
	case LightProperty::zFar:
	{
		LightData->zFar = value[0];
		UpdateProjectionMatrix();
		UpdateViewProjMatrix();
		break;
	}
	case LightProperty::zNear:
	{
		LightData->zNear = value[0];
		UpdateProjectionMatrix();
		UpdateViewProjMatrix();
		break;
	}
	case LightProperty::FrustumWidth:
		mFrustumRadius = value[0];
		UpdateProjectionMatrix();
		UpdateViewProjMatrix();
	default:
		break;
	}
}


/*
*
******************************* Spot Light
*
*/

SpotLightObj::SpotLightObj(SpotLight* lightData, XMFLOAT4 _diffusecolor, XMFLOAT4 _ambientColor, XMFLOAT4 _specularColor, XMFLOAT4 _target, XMFLOAT3 _direction, XMFLOAT3 _position, XMFLOAT3 _attenuation, std::string _name, float _lightSize, float _znear, float _zfar, float _range, float _spot, int _castShadow, int _index, int _id, int _shadowMapID, int _reversedZ)
{
	LightData = lightData;
	ILightData = lightData;

	mIndex = _index;
	mLightType = LIGHT_TYPE_SPOT;
	bDisabled = false;

	LightData->Diffuse = _diffusecolor;
	LightData->Ambient = _ambientColor;
	LightData->Specular = _specularColor;
	LightData->LightSize = _lightSize;
	LightData->Direction = _direction;
	LightData->Position = _position;
	LightData->Attenuation = _attenuation;
	LightData->zNear = _znear;
	LightData->zFar = _zfar;
	LightData->Range = _range;
	LightData->Spot = _spot;
	LightData->CastShadow = _castShadow;
	LightData->ID = _id;
	LightData->ShadowMapID = _shadowMapID;

	mLightTarget = _target;

	Name = std::wstring(_name.begin(), _name.end());

	UpdateViewMatrix();
	UpdateProjectionMatrix();
	UpdateViewProjMatrix();
}

SpotLightObj::SpotLightObj(SpotLight* lightData)
{

	LightData = lightData;
	ILightData = lightData;

	mLightType = LIGHT_TYPE_SPOT;
	bDisabled = false;

	LightData->Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LightData->Ambient = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LightData->Specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LightData->LightSize = 0.5f;
	LightData->Direction = XMFLOAT3(1.0f, 1.0f, 1.0f);
	LightData->Position = XMFLOAT3(10.0f, 10.0f, 10.0f);
	LightData->Attenuation = XMFLOAT3(1.0f, 0.0f, 0.0f);
	LightData->zNear = 10.0f;
	LightData->zFar = 100.0f;
	LightData->Range = 50.0f;
	LightData->Spot = 10.0f;
	LightData->CastShadow = 0;
	LightData->ID = 0;
	LightData->ShadowMapID = -1;

	mLightTarget = XMFLOAT4();
	Name = L"";

}

SpotLightObj::~SpotLightObj()
{

}

void SpotLightObj::SetLightData(SpotLight* spotLightData)
{
	LightData = spotLightData;

	ILightData = spotLightData;
}


void SpotLightObj::UpdateFrustum(float _znear, float _zfar)
{
	LightData->zNear = _znear;
	LightData->zFar = _zfar;
}


void SpotLightObj::UpdateProjectionMatrix()
{
	auto projMatrix = XMMatrixPerspectiveFovLH(mFOVYAngle, mAspectRatio, LightData->zNear, LightData->zFar);
	XMStoreFloat4x4(&mProjMatrix, projMatrix);

	LightData->PerpectiveValues = XMFLOAT4(mProjMatrix._11, mProjMatrix._22, mProjMatrix._33, mProjMatrix._43);
}


void SpotLightObj::SetAttenuation(float _d1, float _d2, float _d3)
{
	LightData->Attenuation = XMFLOAT3(_d1, _d2, _d3);
}


void SpotLightObj::SetRange(float _range)
{
	LightData->Range = _range;
}


void SpotLightObj::SetSpotSize(float _spotPow)
{
	LightData->Spot = _spotPow;
}


void SpotLightObj::EditProperty(int _propIndex, float* value)
{
	switch (_propIndex)
	{
	case LightProperty::Specular:
	{
		LightData->Specular = XMFLOAT4(value[0], value[1], value[2], value[3]);
		break;
	}
	case LightProperty::Ambient:
	{
		LightData->Ambient = XMFLOAT4(value[0], value[1], value[2], value[3]);
		break;
	}
	case LightProperty::Diffuse:
	{
		LightData->Diffuse = XMFLOAT4(value[0], value[1], value[2], value[3]);
		break;
	}
	case LightProperty::CastShadow:
	{
		LightData->CastShadow = (int)value[0];
		break;
	}
	case LightProperty::LightSize:
	{
		LightData->LightSize = value[0];
		break;
	}
	case LightProperty::zFar:
	{
		LightData->zFar = value[0];
		UpdateProjectionMatrix();
		UpdateViewProjMatrix();
		break;
	}
	case LightProperty::zNear:
	{
		LightData->zNear = value[0];
		UpdateProjectionMatrix();
		UpdateViewProjMatrix();
		break;
	}
	case LightProperty::Range:
	{
		LightData->Range = value[0];
		break;
	}
	case LightProperty::SpotPow:
	{
		LightData->Spot = value[0];
		break;
	}
	case LightProperty::Attenuation:
	{
		LightData->Attenuation = XMFLOAT3(value[0], value[1], value[2]);
		break;
	}
	default:
		break;
	}
}


std::vector<std::tuple<std::string, int, std::vector<float>>> SpotLightObj::GetEditableProperties()
{
	std::vector<std::tuple<std::string, int, std::vector<float>>> PropertiesName = {
		std::make_tuple("DiffuseColor(float4)", (int)LightProperty::Diffuse, std::vector<float>({ LightData->Diffuse.x, LightData->Diffuse.y, LightData->Diffuse.z, LightData->Diffuse.w })),
		std::make_tuple("AmbientColor(float4)", (int)LightProperty::Ambient, std::vector<float>({ LightData->Ambient.x, LightData->Ambient.y, LightData->Ambient.z, LightData->Ambient.w })),
		std::make_tuple("SpecularColor(float4)", (int)LightProperty::Specular, std::vector<float>({ LightData->Specular.x, LightData->Specular.y, LightData->Specular.z, LightData->Specular.w })),
		std::make_tuple("LightSize(UNORM)", (int)LightProperty::LightSize, std::vector<float>({LightData->LightSize})),
		std::make_tuple("Range(float)", (int)LightProperty::Range, std::vector<float>({ LightData->Range })),
		std::make_tuple("SpotPower(float)", (int)LightProperty::SpotPow, std::vector<float>({ LightData->Spot })),
		std::make_tuple("Attenuation(float3)", (int)LightProperty::Attenuation , std::vector<float>({ LightData->Attenuation.x, LightData->Attenuation.y, LightData->Attenuation.z })),
		std::make_tuple("zNear(float)", (int)LightProperty::zNear, std::vector<float>({ LightData->zNear })),
		std::make_tuple("zFar(float)", (int)LightProperty::zFar, std::vector<float>({ LightData->zFar })),
		std::make_tuple("CastShadow(bit)", (int)LightProperty::CastShadow, std::vector<float>({ (float)LightData->CastShadow })),
	};
	return PropertiesName;
}


//
// Class Lights
//

Lights::Lights()
{

	mDirectionalLights = {};
	mSpotLights = { };
}

Lights::~Lights()
{

}

void Lights::Destroy()
{
	mCB_SpotLights.Release();
}

void Lights::ResetShadowMapIDs()
{
	UINT ShadowMapID = 0;

	for (auto& dlight : mDirectionalLights)
	{
		if (mData_CB_DirectionalLight.gDirectionalLightArray[dlight.GetIndex()].CastShadow > 0)
		{
			mData_CB_DirectionalLight.gDirectionalLightArray[dlight.GetIndex()].ShadowMapID = ShadowMapID++;
		}
	}

	for (auto& slight : mSpotLights)
	{
		if (mData_CB_SpotLights.gSpotLightArray[slight.GetIndex()].CastShadow > 0)
		{
			mData_CB_SpotLights.gSpotLightArray[slight.GetIndex()].ShadowMapID = ShadowMapID++;
		}
	}

	mShadowMapCount = ShadowMapID;
}

UINT Lights::GetShadowMapCount()
{
	return mShadowMapCount;
}


void Lights::Update(const float & dt)
{
	if (mSelectedLightByListBox > -1)
	{
		MoveLight(mSelectedLightByListBox, gCamera_main.GetPosition(), gCamera_main.GetLook());
	}

	for (auto ilight : mCollectionILights)
	{
		ilight->Update(dt);
	}
}

void Lights::MoveLight(int lightIndex, XMFLOAT3 position, XMFLOAT3 direction)
{
	auto light = mCollectionILights[lightIndex];

	light->SetPosition(position);
	light->SetDirection(direction);
	
}

void Lights::LightSelected(int lightIndex)
{
	mSelectedLightByListBox = lightIndex;
}

void Lights::ImportLights(std::wstring _lightFile, ID3D11DeviceContext *_context)
{
	std::ifstream fin(_lightFile);
	std::wstring s1 = L"Loading Light File Failed: ";
	std::wstring s2 = L" not founded";
	auto s = s1 + _lightFile + s2;

	if (!fin)
	{
		MessageBox(0, s.c_str(), L"error", 0);
	}

	size_t light_count = 0;
	std::string ignore;
	std::string command;
	char c;

	std::string light_name = "";
	std::string light_type = "";
	XMFLOAT4 light_diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	XMFLOAT4 light_ambient = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	XMFLOAT4 light_specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	XMFLOAT4 light_target = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT3 light_direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 light_position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 light_up = XMFLOAT3(0.0f, 0.0f, 0.0f);
	float light_frustumRadius = 0.0f;
	float light_size = 0.0f;
	float light_znear = 0.0f;
	float light_zfar = 0.0f;
	float light_range = 0.0f;
	float light_spot = 0.0f;
	XMFLOAT3 light_attenuation = XMFLOAT3(0.0f, 0.0f, 0.0f);
	int light_castShadow = 0;
	int light_disabled = 0;
	int light_ID = 0;
	int light_index = 0;
	int light_ShadowMapID = -1;
	int light_reversedZ = 0;

	while (!fin.eof())
	{

		fin >> command;
		if (command == "l")
		{
			if (light_count > 0)
			{
				if (light_type == "directional")
				{
					mDirectionalLights.emplace_back(
						DirectionalLightObj(
							&mData_CB_DirectionalLight.gDirectionalLightArray[light_index],
							light_diffuse,
							light_ambient,
							light_specular,
							light_target,
							light_direction,
							light_position,
							light_name,
							light_size,
							light_znear,
							light_zfar,
							light_frustumRadius,
							light_castShadow,
							light_index,
							light_ID,
							light_ShadowMapID
						)
					);

					mCollectionILights.push_back(&mDirectionalLights.back());

					light_index++;
				}
				else if (light_type == "spot")
				{
					mSpotLights.emplace_back(SpotLightObj(
						&mData_CB_SpotLights.gSpotLightArray[light_index],
						light_diffuse,
						light_ambient,
						light_specular,
						light_target,
						light_direction,
						light_position,
						light_attenuation,
						light_name,
						light_size,
						light_znear,
						light_zfar,
						light_range,
						light_spot,
						light_castShadow,
						light_index,
						light_ID,
						light_ShadowMapID,
						light_reversedZ
					)
					);

					mCollectionILights.push_back(&mSpotLights.back());

					light_index++;
				}
			}

			fin >> light_name;
			light_count++;
		}
		else if (command == "t")
		{
			fin >> light_type;
		}
		else if (command == "diffuse")
		{
			fin >> light_diffuse.x >> light_diffuse.y >> light_diffuse.z >> light_diffuse.w;
		}
		else if (command == "ambient")
		{
			fin >> light_ambient.x >> light_ambient.y >> light_ambient.z >> light_ambient.w;
		}
		else if (command == "specular")
		{
			fin >> light_specular.x >> light_specular.y >> light_specular.z >> light_specular.w;
		}
		else if (command == "direction")
		{
			fin >> light_direction.x >> light_direction.y >> light_direction.z;
		}
		else if (command == "position")
		{
			fin >> light_position.x >> light_position.y >> light_position.z;
		}
		else if (command == "target")
		{
			fin >> light_target.x >> light_target.y >> light_target.z;
		}
		else if (command == "up")
		{
			fin >> light_up.x >> light_up.y >> light_up.z;
		}
		else if (command == "frustumradius")
		{
			fin >> light_frustumRadius;
		}
		else if (command == "lightsize")
		{
			fin >> light_size;
		}
		else if (command == "znear")
		{
			fin >> light_znear;
		}
		else if (command == "zfar")
		{
			fin >> light_zfar;
		}
		else if (command == "disabled")
		{
			fin >> light_disabled;
		}
		else if (command == "castshadow")
		{
			fin >> light_castShadow;
		}
		else if (command == "range")
		{
			fin >> light_range;
		}
		else if (command == "spot")
		{
			fin >> light_spot;
		}
		else if (command == "attenuation")
		{
			fin >> light_attenuation.x >> light_attenuation.y >> light_attenuation.z;
		}
		else if (command == "id")
		{
			fin >> light_ID;
		}
		else {
			fin.ignore(1000, '\n');
		}
	}

	if (light_count > 0)
	{
		if (light_type == "directional")
		{
			mDirectionalLights.emplace_back(DirectionalLightObj(
				&mData_CB_DirectionalLight.gDirectionalLightArray[light_index],
				light_diffuse,
				light_ambient,
				light_specular,
				light_target,
				light_direction,
				light_position,
				light_name,
				light_size,
				light_znear,
				light_zfar,
				light_frustumRadius,
				light_castShadow,
				light_index,
				light_ID,
				light_ShadowMapID
			)
			);

			mCollectionILights.push_back(&mDirectionalLights.back());
		}
		else if (light_type == "spot")
		{
			/*SpotLightObj slight = new SpotLightObj(
				XMLoadFloat4(&light_diffuse),
				XMLoadFloat4(&light_ambient),
				XMLoadFloat4(&light_specular),
				XMLoadFloat3(&light_target),
				light_direction,
				light_position,
				light_attenuation,
				light_name,
				light_size,
				light_znear,
				light_zfar,
				light_range,
				light_spot,
				light_castShadow,
				light_ID,
				light_ShadowMapID,
				light_reversedZ
			);*/

			mSpotLights.emplace_back(SpotLightObj(
				&mData_CB_SpotLights.gSpotLightArray[light_index],
				light_diffuse,
				light_ambient,
				light_specular,
				light_target,
				light_direction,
				light_position,
				light_attenuation,
				light_name,
				light_size,
				light_znear,
				light_zfar,
				light_range,
				light_spot,
				light_castShadow,
				light_index,
				light_ID,
				light_ShadowMapID,
				light_reversedZ
			)
			);

			mCollectionILights.push_back(&mSpotLights.back());

			//delete slight;
		}
	}
	InitBuffers();


	ResetShadowMapIDs();
	GenerateLBItems();
}

void Lights::LoadLights(std::wstring _lightFile, ID3D11DeviceContext *_context)
{
	std::ifstream bfin(_lightFile, std::ios::in | std::ios::binary);
	if (bfin.is_open())
	{
		boost::archive::binary_iarchive ai(bfin);

		ai& *this;
	}
	InitBuffers();


	ResetShadowMapIDs();
}

void Lights::SaveLights(std::wstring _lightFile)
{
	std::ofstream bfout = std::ofstream(_lightFile, std::ios::out | std::ios::binary | std::ios::app);
	boost::archive::binary_oarchive ao(bfout);

	ao& (*this);
}

void Lights::UpdateLightBuffers(ID3D11DeviceContext *_context)
{

	mCB_SpotLights.Update(_context);

	/*std::vector<DirectionalLight> dlights;
	for (auto e : mDirectionalLights)
	{
		dlights.push_back(*e.LightData);
	}

	std::vector<SpotLight> slights;
	for (auto e : mSpotLights)
	{
		slights.push_back(*e.LightData);
	}
	_context->Map(mD3DDirectionalLightsB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mMapDirectionalLights);
	memcpy(mMapDirectionalLights.pData, &dlights[0], dlights.size() * sizeof(DirectionalLight));
	_context->Unmap(mD3DDirectionalLightsB.Get(), 0);

	_context->Map(mD3DSpotLightsB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mMapSpotLights);
	memcpy(mMapSpotLights.pData, &slights[0], slights.size() * sizeof(SpotLight));
	_context->Unmap(mD3DSpotLightsB.Get(), 0);*/
}

void Lights::SetLightsBuffer_VS(ID3D11DeviceContext* _context, UINT spotLightRegisterSlot, UINT dirLightRegisterSlot)
{

	/*_context->VSSetShaderResources(_directionalLightRegisterNum, 1, &mSRVDirectionalLights);
	_context->VSSetShaderResources(_spotLightRegisterNum, 1, &mSRVSpotLights);*/
	mCB_SpotLights.ChangeRegisterSlot(spotLightRegisterSlot);
	mCB_SpotLights.SetConstantBuffer(gContext.Get());
}

void Lights::SetListBoxItems(ListBox* listBox)
{
	listBox->SetItemList(&m_itemList);

}

void Lights::SetLightsBuffer_PS(ID3D11DeviceContext* _context, UINT spotLightRegisterSlot, UINT dirLightRegisterSlot)
{

	/*_context->PSSetShaderResources(_directionalLightRegisterNum, 1, &mSRVDirectionalLights);
	_context->PSSetShaderResources(_spotLightRegisterNum, 1, &mSRVSpotLights);*/
	
	mCB_SpotLights.ChangeRegisterSlot(spotLightRegisterSlot);
	mCB_SpotLights.SetConstantBuffer(gContext.Get());
}

void Lights::InitBuffers()
{

	mCB_SpotLights.InitConstantBuffer(2, CONSTANT_BUFFER_PS, &mData_CB_SpotLights, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);

	/*mD3DDirectionalLightsB.Reset();
	mD3DSpotLightsB.Reset();
	mSRVDirectionalLights.Reset();
	mSRVSpotLights.Reset();*/

	/*D3D11_BUFFER_DESC bdesc;
	bdesc.ByteWidth = mDirectionalLights.size() * sizeof(DirectionalLight);
	bdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	bdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bdesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	bdesc.StructureByteStride = sizeof(DirectionalLight);
	bdesc.Usage = D3D11_USAGE_DYNAMIC;

	mdevice->CreateBuffer(&bdesc, nullptr, &mD3DDirectionalLightsB);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvdesc;
	srvdesc.Format = DXGI_FORMAT_UNKNOWN;
	srvdesc.ViewDimension = D3D_SRV_DIMENSION_BUFFEREX;
	srvdesc.BufferEx.Flags = 0;
	srvdesc.BufferEx.FirstElement = 0;
	srvdesc.BufferEx.NumElements = mDirectionalLights.size();

	mdevice->CreateShaderResourceView(mD3DDirectionalLightsB.Get(), &srvdesc, &mSRVDirectionalLights);

	bdesc.ByteWidth = mSpotLights.size() * sizeof(SpotLight);
	bdesc.StructureByteStride = sizeof(SpotLight);

	mdevice->CreateBuffer(&bdesc, nullptr, &mD3DSpotLightsB);

	srvdesc.BufferEx.NumElements = mSpotLights.size();

	mdevice->CreateShaderResourceView(mD3DSpotLightsB.Get(), &srvdesc, &mSRVSpotLights);*/
}

void Lights::GenerateLBItems()
{
	m_itemList.ClearAllItems();

	for ( auto l : mCollectionILights)
	{
		m_itemList.AddItem(l->GetIndex(), l->Name);
	}

	m_itemList.Func_SelectItem = std::bind(&Lights::LightSelected, this, std::placeholders::_1);
	m_itemList.Func_DeSelectItem = std::bind(&Lights::LightSelected, this, std::placeholders::_1);
}

