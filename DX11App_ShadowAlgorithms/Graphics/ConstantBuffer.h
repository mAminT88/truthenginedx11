#pragma once
#include "Globals.h"

enum CONSTANT_BUFFER_TYPE
{
	CONSTANT_BUFFER_VS = 0b00000001,
	CONSTANT_BUFFER_HS = 0b00000010,
	CONSTANT_BUFFER_DS = 0b00000100,
	CONSTANT_BUFFER_GS = 0b00001000,
	CONSTANT_BUFFER_PS = 0b00010000,
	CONSTANT_BUFFER_CS = 0b00100000,
};

template<class T>
class ConstantBuffer
{
protected:
	UINT mRegisterSlot;
	int mCBufferType;
	T* pDataStructure;
	ComPtr<ID3D11Buffer> pCB;
	D3D11_MAPPED_SUBRESOURCE mMapCB;



public:
	ConstantBuffer() : mRegisterSlot(0), pCB(nullptr), pDataStructure(nullptr), mCBufferType(CONSTANT_BUFFER_VS)
	{
	}

	void Release()
	{
		pCB.Reset();
	}

	void InitConstantBuffer(UINT _registerSlot, int _cbtype, T* _dataStruct, D3D11_CPU_ACCESS_FLAG _cpuAccessFlag = (D3D11_CPU_ACCESS_FLAG)0, D3D11_USAGE _usageFlag = D3D11_USAGE_DEFAULT, UINT _miscFlag = 0)
	{
		pDataStructure = _dataStruct;
		mRegisterSlot = _registerSlot;
		mCBufferType = _cbtype;

		D3D11_BUFFER_DESC bdesc;
		bdesc.ByteWidth = sizeof(T);
		bdesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bdesc.CPUAccessFlags = _cpuAccessFlag;
		bdesc.MiscFlags = 0;
		bdesc.StructureByteStride = 0;
		bdesc.Usage = _usageFlag;

		Globals::gDevice->CreateBuffer(&bdesc, nullptr, pCB.ReleaseAndGetAddressOf());
	}

	void SetConstantBuffer(ID3D11DeviceContext* _context)
	{
		if (mCBufferType & CONSTANT_BUFFER_VS)
		{
			_context->VSSetConstantBuffers(mRegisterSlot, 1, pCB.GetAddressOf());
		}

		if (mCBufferType & CONSTANT_BUFFER_DS)
		{
			_context->DSSetConstantBuffers(mRegisterSlot, 1, pCB.GetAddressOf());
		}

		if (mCBufferType & CONSTANT_BUFFER_HS)
		{
			_context->HSSetConstantBuffers(mRegisterSlot, 1, pCB.GetAddressOf());
		}

		if (mCBufferType & CONSTANT_BUFFER_GS)
		{
			_context->GSSetConstantBuffers(mRegisterSlot, 1, pCB.GetAddressOf());
		}

		if (mCBufferType & CONSTANT_BUFFER_PS)
		{
			_context->PSSetConstantBuffers(mRegisterSlot, 1, pCB.GetAddressOf());
		}

		if (mCBufferType & CONSTANT_BUFFER_CS)
		{
			_context->CSSetConstantBuffers(mRegisterSlot, 1, pCB.GetAddressOf());
		}
	}

	void Update(ID3D11DeviceContext* _context)
	{
		_context->Map(pCB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mMapCB);
		memcpy(mMapCB.pData, pDataStructure, sizeof(T));
		_context->Unmap(pCB.Get(), 0);
	}

	void Update(ID3D11DeviceContext* _context, const void* Data, size_t DataSize)
	{
		if (DataSize > 0)
		{
			_context->Map(pCB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mMapCB);
			memcpy(mMapCB.pData, Data, DataSize);
			_context->Unmap(pCB.Get(), 0);
		}
	}

	void ChangeRegisterSlot(UINT registerSlot)
	{
		mRegisterSlot = registerSlot;
	}
};