#if defined(__INTELLISENSE__)
#include "../Includes/ps_Include_GenerateGBuffers.hlsli"
#else
#include "Graphics/Shaders/Includes/ps_Include_GenerateGBuffers.hlsli"

#endif



pixelOut PackGBuffers(float2 texCoord, float specIntensity, float3 normalW, int specPow)
{
	pixelOut psOut;

    psOut.Color = float4(g_IMaterialBase.GetDiffuseColor(texCoord).xyz, specIntensity);
    psOut.NormalW_UNORM = float4((normalW * 0.5 + 0.5), 1.0f);
	psOut.SpecPow = specPow;

	return psOut;
}

pixelOut main(pixelIn pin)
{
	
    float3 normalW = g_INormalManager.getNormal(pin);

    float4 specular = g_IMaterialBase.GetSpecularColor();

	float specIntensity = specular.x;
	float specPow = specular.w;

	return PackGBuffers(pin.Tex, specIntensity, normalW, specPow);
}
