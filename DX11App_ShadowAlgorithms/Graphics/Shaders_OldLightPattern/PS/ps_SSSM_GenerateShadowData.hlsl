#if defined(__INTELLISENSE__)

    #include "../Includes/ps_Include_SSSM_GenerateShadowData.hlsli"

#else

    #include "Graphics/Shaders/Includes/ps_Include_SSSM_GenerateShadowData.hlsli"

#endif


pixelOut_0 main_0( vertexOut_render2D pin)
{
    float EyeDepth = tDepthMap.Sample(gSamplerPoint, pin.Tex);

    float EyeDepth_Linear = ConvertToLinearDepth(EyeDepth, gPerspectiveValues.w, gPerspectiveValues.z);

    float3 PosW = CalcPosW(float3(pin.csPos, EyeDepth), EyeDepth_Linear);

    float4 ShadowCoord = gILightArray.CalcShadowPos(PosW, gShadowMapID);

    return CalculateAvgBlockerHorizontal_HardShadow(gILightArray, gShadowMapID, ShadowCoord);
}

pixelOut_1 main_1( vertexOut_render2D pin )
{

    float EyeDepth = tDepthMap.Sample(gSamplerPoint, pin.Tex);

    float EyeDepth_Linear = ConvertToLinearDepth(EyeDepth, gPerspectiveValues.w, gPerspectiveValues.z);

    float3 PosW = CalcPosW(float3(pin.csPos, EyeDepth), EyeDepth_Linear);

    float4 LightPosition = mul(float4(PosW, 1.0f), gILightArray.GetViewMatrix(gLightIndex));

    float4 ShadowCoord = gILightArray.CalcShadowPos(PosW, gShadowMapID);

    return CalculatePenumbraSize_FilterHardShadowsHorizontally(gILightArray, gShadowMapID, EyeDepth, pin.Tex, ShadowCoord, LightPosition);

}

pixelOut_2 main_2( vertexOut_render2D pin )
{
    float EyeDepth = tDepthMap.Sample(gSamplerPoint, pin.Tex);

    float EyeDepth_Linear = ConvertToLinearDepth(EyeDepth, gPerspectiveValues.w, gPerspectiveValues.z);

    float3 PosW = CalcPosW(float3(pin.csPos, EyeDepth), EyeDepth_Linear);

    float4 ShadowCoord = gILightArray.CalcShadowPos(PosW, gShadowMapID);

    return FilterShadowsVertically(pin.Tex, EyeDepth, ShadowCoord);
}