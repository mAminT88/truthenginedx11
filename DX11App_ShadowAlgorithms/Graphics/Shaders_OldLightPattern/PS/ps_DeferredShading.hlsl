#if defined(__INTELLISENSE__)

#include "../Includes/ps_Include_DeferredShading.hlsli"

#else

#include "Graphics/Shaders/Includes/ps_Include_DeferredShading.hlsli"

#endif



float3 CalcWorldPos( float2 posH, float viewDepth )
{
    float4 posV;
    posV.xy = posH.xy * gPerspectiveValues.xy * viewDepth;
    posV.z = viewDepth;
    posV.w = 1.0f;

    float4 posW = mul(posV, gViewInv);
    return posW.xyz;
}

float4 main( vertexOut pin ) : SV_Target
{
    GBufferData gbData = gIGBufferManager.GetData(pin.Tex);

    if (gbData.depth > 0.99999)
    {
        discard;
    }

    float viewDepth = ConvertToLinearDepth(gbData.depth, gPerspectiveValues.w, gPerspectiveValues.z);

    float3 PosW = CalcWorldPos(pin.csPos, viewDepth);

    float3 toEyeW = normalize(gEyePos - PosW);

    float4 diffuse = float4(0.0f, 0.0f, 0.0f, 1.0f), ambient = float4(0.0f, 0.0f, 0.0f, 1.0f), specular = float4(0.0f, 0.0f, 0.0f, 1.0f);


    for (int i = 0; i < gNumSpotLights; ++i)
    {

        //Calculate Shadow Factor
        float ShadowFactor = gIShadowManager.CalcShadowFactor(gILightsArray_Spot
        , i
        , pin.Tex
        , gILightsArray_Spot.CalcShadowPos(PosW, i)
        , tShadowMapArray);

        diffuse += gILightsArray_Spot.IlluminateDiffuse(PosW, gbData.NormalW, i) * ShadowFactor;
        ambient += gILightsArray_Spot.IlluminateAmbient(i);
        specular += gILightsArray_Spot.IlluminateSpecular(toEyeW, i) * ShadowFactor;

            //[branch]
            //if (spotLight.IsCastShadow())
            //{
            //    float4 shadowPos = spotLight.CalcShadowPos(PosW);
            //    ShadowFactor = gIShadow_Base.CalcShadowFactor(shadowPos, spotLight.GetShadowMapID());
            //}

    }

    for (int i = 0; i < gNumDirectLights; ++i)
    {

        //Calculate Shadow Factor
        float ShadowFactor = gIShadowManager.CalcShadowFactor(gILightsArray_Direct
        , i
        , pin.Tex
        , gILightsArray_Direct.CalcShadowPos(PosW, i)
        , tShadowMapArray);

        diffuse += gILightsArray_Direct.IlluminateDiffuse(PosW, gbData.NormalW, i) * ShadowFactor;
        ambient += gILightsArray_Direct.IlluminateAmbient(i);
        specular += gILightsArray_Direct.IlluminateSpecular(toEyeW, i) * ShadowFactor;

            //[branch]
            //if (spotLight.IsCastShadow())
            //{
            //    float4 shadowPos = spotLight.CalcShadowPos(PosW);
            //    ShadowFactor = gIShadow_Base.CalcShadowFactor(shadowPos, spotLight.GetShadowMapID());
            //}

    }

        

    //return float4(((float4(gbData.Color, 1.0) * (diffuse + ambient)) + specular).rgb, 1.0f);
    return float4(gbData.Color, 1.0f) * (diffuse + GLOBAL_AMBIENT);
    
}