#if defined(__INTELLISENSE__)
	#include "../Includes/ps_Include_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch.hlsli"
#else
#include "Graphics/Shaders/Includes/ps_Include_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch.hlsli"
#endif


ILightArray_Basic gILightArray;

IGenerateShadowData_0 g_IGenerateShadowData_0;
IGenerateShadowData_1 g_IGenerateShadowData_1;
IGenerateShadowData_2 g_IGenerateShadowData_2;


pixelOut_0 main_0( vertexOut_render2D pin )
{
    float depth = tDepthMap.Sample(gSamplerPoint, pin.Tex);

    if (depth > 0.99999)
        discard;

    float linearDepth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);

    float3 posW = CalcPosW(float3(pin.csPos.xy, depth), linearDepth);

    float4 shadowCoord = gILightArray.CalcShadowPos(posW, gShadowMapID);

    return g_IGenerateShadowData_0.CalcOutput(shadowCoord, gILightArray.GetZNear(gShadowMapID), gILightArray.GetLightSize(gShadowMapID));

}


pixelOut_1 main_1( vertexOut_render2D pin )
{
    float depth = tDepthMap.Sample(gSamplerPoint, pin.Tex);

    if (depth > 0.99999)
        discard;

    float linearDepth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);

    float3 posW = CalcPosW(float3(pin.csPos.xy, depth), linearDepth);

    float4 shadowCoord = gILightArray.CalcShadowPos(posW, gShadowMapID);

    float2 dz_duv = DepthGradient(shadowCoord.xy, shadowCoord.z);

    return g_IGenerateShadowData_1.CalcOutput(pin.Tex, linearDepth, shadowCoord, posW, gILightArray.GetPosition(gShadowMapID), gILightArray.GetZNear(gShadowMapID), gILightArray.GetLightSize(gShadowMapID), gILightArray.GetPerspectiveValues(gShadowMapID), dz_duv);

}

pixelOut_2 main_2( vertexOut_render2D pin )
{
    float depth = tDepthMap.Sample(gSamplerPoint, pin.Tex);

    if (depth > 0.99999)
        discard;

    float linearDepth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);

    float3 posW = CalcPosW(float3(pin.csPos.xy, depth), linearDepth);

    float4 shadowCoord = gILightArray.CalcShadowPos(posW, gShadowMapID);

    float2 dz_duv = DepthGradient(shadowCoord.xy, shadowCoord.z);

    return g_IGenerateShadowData_2.CalcOutput(pin.Tex, linearDepth, shadowCoord, posW, gILightArray.GetPosition(gShadowMapID), dz_duv);

}