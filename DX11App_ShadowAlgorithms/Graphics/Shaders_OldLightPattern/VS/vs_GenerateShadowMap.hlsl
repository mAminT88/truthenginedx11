
#if defined(__INTELLISENSE__)

#include "../../ShaderSlots_DeferredShading.h"
#include "../Includes/Include_HelperFunc.hlsli"

#define VERTEX_TYPE_SKINNED

#else

#include "Graphics/ShaderSlots_DeferredShading.h"
#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"

#endif

cbuffer cbPerObject : register(HLSL_SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PEROBJECT)
{
	row_major matrix gWorld : packoffset(c0);
}

cbuffer cbPerLight : register(HLSL_SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PERLIGHT)
{
	row_major matrix gLightVP : packoffset(c0);
    float2 gZNearFar : packoffset(c4);
    float2 pad_cbPerLight : packoffset(c5);

}

struct vertexOut
{
    float4 ShadowPos : SV_Position;
    float NormalizedLinearDepth : TEXCOORD0;
};

#ifdef VERTEX_TYPE_SKINNED

struct vertexIn
{
    float3 PosL             : POSITION;
    float3 NormalL          : NORMAL;
    float2 Tex              : TEXCOORD;
    float3 TangentU         : TANGENT;
    float3 BoneWeights      : BONEWEIGHT;
    int4 BoneIndex          : BONEINDEX;
};

cbuffer cb_boneTransforms : register(HLSL_SHADER_SLOTS_VS_CB_PROCESSMESH)
{
    row_major matrix gBoneTransformations[96];
};

vertexOut main( vertexIn vin )
{
    vertexOut vout;

    float weightSum = vin.BoneWeights[0] + vin.BoneWeights[1] + vin.BoneWeights[2];

    float weight4 = 1.0 - weightSum;

    float4 pos = vin.BoneWeights[0] * mul(float4(vin.PosL, 1.0f), gBoneTransformations[vin.BoneIndex[0]]);
    pos += vin.BoneWeights[1] * mul(float4(vin.PosL, 1.0f), gBoneTransformations[vin.BoneIndex[1]]);
    pos += vin.BoneWeights[2] * mul(float4(vin.PosL, 1.0f), gBoneTransformations[vin.BoneIndex[2]]);
    pos += weight4 * mul(float4(vin.PosL, 1.0f), gBoneTransformations[vin.BoneIndex[3]]);
    pos.w = 1.0f;

    vout.ShadowPos = mul(pos, mul(gWorld, gLightVP));
    vout.NormalizedLinearDepth = linstep(gZNearFar.x, gZNearFar.y, vout.ShadowPos.w);

    return vout;
}

#endif

#ifdef VERTEX_TYPE_BASIC32

struct vertexIn
{
    float3 PosL : POSITION;
    float3 NormalL : NORMAL;
    float2 Tex : TEXCOORD;
};

vertexOut main( vertexIn vin )
{
    vertexOut vout;
    vout.ShadowPos = mul(float4(vin.PosL, 1.0), mul(gWorld, gLightVP));
    vout.NormalizedLinearDepth = linstep(gZNearFar.x, gZNearFar.y, vout.ShadowPos.w);
    return vout;
}

#endif
