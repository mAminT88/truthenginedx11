#ifndef HLSL_GAUSSFILTER
#define HLSL_GAUSSFILTER

#if defined(__INTELLISENSE__)

#include "../Includes/Include_GaussianWeights.hlsli"
#include "../Includes/Include_Samplers.hlsli"

#define FILTER_RADIUS 5
#define GaussianWeight Gaussian5

#else

#include "Graphics/Shaders/Includes/Include_GaussianWeights.hlsli"
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"

#endif

#define FILTER_HALF_RADIUS ((FILTER_RADIUS - 1) / 2)



float4 ApplyFilter_1D_Radius_Dir(Texture2DArray textureArray, float texArrayIndex, SamplerState SamplerS, float2 tex, float2 Dir, float Radius )
{
    float3 location = float3(tex, texArrayIndex);

    float4 r;

    float weight_sum = 0.0f;

    r = textureArray.SampleLevel(SamplerS, location, 0) * GaussianWeight[0];
    weight_sum += GaussianWeight[0];

    float3 offset = float3(0.0f, 0.0f, 0.0f);

    float2 step = (Radius / FILTER_HALF_RADIUS) * Dir;

    [unroll]
    for (int i = 1; i <= FILTER_HALF_RADIUS; ++i)
    {

        offset.xy = step * i;

        r += textureArray.SampleLevel(SamplerS, location + offset, 0) * GaussianWeight[i];
        r += textureArray.SampleLevel(SamplerS, location - offset, 0) * GaussianWeight[i];

        weight_sum += GaussianWeight[i] * 2.0f;

    }

    return r / weight_sum;

}

float4 ApplyFilter_1D_Radius_Dir_Bilateral( Texture2DArray textureArray, float texArrayIndex, SamplerState SamplerS, float2 tex, float2 Dir, float Radius, float ref_sample )
{
    float3 location = float3(tex, texArrayIndex);

    float4 r;

    float weight_sum = 0.0f;

    r = textureArray.SampleLevel(SamplerS, location, 0) * GaussianWeight[0];
    weight_sum += GaussianWeight[0];

    float3 offset = float3(0.0f, 0.0f, 0.0f);

    float2 step = (Radius / FILTER_HALF_RADIUS) * Dir;

    [unroll]
    for (int i = 1; i <= FILTER_HALF_RADIUS; ++i)
    {

        offset.xy = step * i;

        r += textureArray.SampleLevel(SamplerS, location + offset, 0) * GaussianWeight[i];
        r += textureArray.SampleLevel(SamplerS, location - offset, 0) * GaussianWeight[i];

        weight_sum += GaussianWeight[i] * 2.0f;

    }

    return r / weight_sum;

}

#endif