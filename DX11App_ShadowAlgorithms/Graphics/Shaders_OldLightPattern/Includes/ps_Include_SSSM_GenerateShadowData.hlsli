#if defined(__INTELLISENSE__)
    
    #include "Include_Samplers.hlsli"
    #include "Include_HelperFunc.hlsli"
    #include "Include_GaussianWeights.hlsli"
    #include "Include_CommonConstantBuffer.hlsli"

#define ALPHA float(0.5f)
#define BETA float(0.2f)

#define BLOCKER_SEARCH_SAMPLE_COUNT float(25.0f)    
#define BLOCKER_SEARCH_HALF_SAMPLE_COUNT float(5.0f)

#define HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT float(5.0f)    
#define HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT float(2.0f)    
#define HARDSHADOW_FILTER_GAUSSIAN_WEIGHT     Gaussian5

#else
    
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"
#include "Graphics/Shaders/Includes/Include_GaussianWeights.hlsli"
#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"

#endif


//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------

Texture2DArray tShadowMapArray : register(t0);
Texture2D tDepthMap : register(t1);
Texture2D tNormalMap : register(t2);
Texture2D<float2> tAvgBlocker : register(t3);
Texture2DArray tShadows : register(t4);
Texture2D tPenumbraSize : register(t5);


//--------------------------------------------------------------------------------------
// Constant Buffers
//--------------------------------------------------------------------------------------


cbuffer CB_SSSM_PERLIGHT : register(b6)
{
    row_major matrix gLightViewInv;

    float gLightIndex;
    float gShadowMapID;
    float2 cb_sssm_perlight_pad0;
};

cbuffer CB_SSSM_PERFRAME : register(b7)
{
    row_major matrix gViewInvT;
}




struct vertexOut_render2D
{
    float4 PosH : SV_Position;
    float2 Tex : TEXCOORD0;
    float2 csPos : TEXCOORD1;
};

struct pixelOut_0
{
    float HardShadow : SV_TARGET0;
    float2 AvgBlocker_Horz : SV_TARGET1;
};

struct pixelOut_1
{
    float PenumbraSize : SV_TARGET0;
    float SoftShadow_Horz : SV_TARGET1;
};


struct pixelOut_2
{
    float SoftShadow : SV_TARGET0;
};

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

ILightArray_Basic gILightArray;



//--------------------------------------------------------------------------------------
// Functions
//--------------------------------------------------------------------------------------

float2 ProjectionToTex( float2 pos )
{
    float2 texCoord = pos;
    texCoord *= float2(0.5f, -0.5f);
    texCoord += float2(0.5f, 0.5f);

    return texCoord;
}


float3 CalcPosW( float3 csPos, float linearDepth )
{
    float3 posV = float3(0.0f, 0.0f, 0.0f);
    posV.xy = csPos.xy * gPerspectiveValues.xy * linearDepth.xx;
    posV.z = linearDepth;
    
    return mul(float4(posV, 1.0), gViewInv).xyz;
}


// Derivatives of light-space depth with respect to texture coordinates
float2 DepthGradient( float2 uv, float z )
{
    float2 dz_duv = 0;

	// Packing derivatives of u,v, and distance to light source w.r.t. screen space x, and y
    float3 duvdist_dx = ddx(float3(uv, z));
    float3 duvdist_dy = ddy(float3(uv, z));

	// Top row of 2x2
    dz_duv.x = duvdist_dy.y * duvdist_dx.z;
    dz_duv.x -= duvdist_dx.y * duvdist_dy.z;
	
	// Bottom row of 2x2
    dz_duv.y = duvdist_dx.x * duvdist_dy.z;
    dz_duv.y -= duvdist_dy.x * duvdist_dx.z;

	// Multiply ddist/dx and ddist/dy by inverse transpose of Jacobian
    float det = (duvdist_dx.x * duvdist_dy.y) - (duvdist_dx.y * duvdist_dy.x);
    dz_duv /= det;

    return dz_duv;
}


// Computes depth offset (bias)
float BiasedZ( float z0, float2 dz_duv, float2 offset )
{
    return z0 + dot(dz_duv, offset);
}


// Using similar triangles from the surface point to the area light
float2 SearchRegionRadiusUV( float zLight, float zNear, float lightSize )
{
    return lightSize * (zLight - zNear) / zLight;
}

float EstimatePenumbraSize( float _lightSize, float _receiver, float _blocker )
{
    return _lightSize * (_receiver - _blocker) / _blocker;
}

//Finds Blockers In Horizontal Direction By Sampling From Shadow Map with Calculating Hard Shadow Bit for Corresponding Point
void FindBlocker_Horz( out float hardShadowBit,
                out float BlockerSum,
				out float numBlockers,
				float2 uv,
				float z0,
				float2 dz_duv,
				float2 searchRegionRadiusUV,
                float shadowMapID )
{

    BlockerSum = 0.0f;
    numBlockers = 0.0f;
    hardShadowBit = 1.0f;

    float3 location = float3(uv, shadowMapID);

    float shadowDepth = tShadowMapArray.SampleLevel(gSamplerPoint, location, 0);

    if (shadowDepth < z0)
    {
        hardShadowBit = 0.0f;
        BlockerSum = shadowDepth;
        numBlockers++;
    }

    float step = searchRegionRadiusUV.x / BLOCKER_SEARCH_HALF_SAMPLE_COUNT;

    float3 offset = 0.0f;

    [unroll]
    for (int i = 1; i <= BLOCKER_SEARCH_HALF_SAMPLE_COUNT; ++i)
    {
        offset.x = step * i;
        shadowDepth = tShadowMapArray.SampleLevel(gSamplerPoint, location + offset, 0);
        float z = BiasedZ(z0, dz_duv, offset.xy);

		// averages depth values that are closer to the light than the receiving point {average blockers only}
        if (shadowDepth < z)
        {
            BlockerSum += shadowDepth;
            numBlockers++;
        }

        shadowDepth = tShadowMapArray.SampleLevel(gSamplerPoint, location - offset, 0);
        z = BiasedZ(z0, dz_duv, offset.xy);

		// averages depth values that are closer to the light than the receiving point {average blockers only}
        if (shadowDepth < z)
        {
            BlockerSum += shadowDepth;
            numBlockers++;
        }
    }
}

void ScreenSpaceFindingBlocker_Vert(
                out float BlockerAvg,
                inout float BlockerSum,
				inout float BlockerCount,
				float2 texCoord,
                float EyeDepth,
				float4 shadowCoord,
                float4 lightViewPos,
				float2 dz_duv,
				float2 searchRegionRadiusUV )
{
    
    BlockerAvg = 0.0f;

    float step = searchRegionRadiusUV.y / BLOCKER_SEARCH_HALF_SAMPLE_COUNT;

    float2 offset = float2(0.0f, 0.0f), location;

    [unroll]
    for (int i = 1; i <= BLOCKER_SEARCH_HALF_SAMPLE_COUNT; ++i)
    {

        float4 lp = lightViewPos;

        float offsetY = step * i;

        lp.y = lightViewPos.y + offsetY;

        float4 wp = mul(lp, gLightViewInv);

        float4 eyeProjectionPos = mul(wp, gViewProj);
        eyeProjectionPos.xyz /= eyeProjectionPos.w;

        eyeProjectionPos.xy = ProjectionToTex(eyeProjectionPos.xy);

        //offset.y = step * i;

        //location = texCoord + offset;

        float sampleEyeDepth = tDepthMap.Sample(gSamplerPoint_BorderColor_1, eyeProjectionPos.xy);

        if (abs(sampleEyeDepth - EyeDepth) < ALPHA)
        {

            float2 blockerSum_Count = tAvgBlocker.Sample(gSamplerPoint_BorderColor_1, eyeProjectionPos.xy);

            BlockerSum += blockerSum_Count.x;
            BlockerCount += blockerSum_Count.y;

        }
        else
        {

            float dummyhardShadow, blockerSum, blockerCount;
            FindBlocker_Horz(dummyhardShadow, blockerSum, blockerCount, shadowCoord.xy + offsetY, shadowCoord.z, dz_duv, searchRegionRadiusUV, gShadowMapID);

            BlockerSum += blockerSum;
            BlockerCount += blockerCount;

        }

        lp.y = lightViewPos.y - offsetY;

        wp = mul(lp, gLightViewInv);

        eyeProjectionPos = mul(wp, gViewProj);
        eyeProjectionPos.xyz /= eyeProjectionPos.w;

        eyeProjectionPos.xy = ProjectionToTex(eyeProjectionPos.xy);

        sampleEyeDepth = tDepthMap.Sample(gSamplerPoint_BorderColor_1, eyeProjectionPos.xy);

        if (abs(sampleEyeDepth - EyeDepth) < ALPHA)
        {

            float2 blockerSum_Count = tAvgBlocker.Sample(gSamplerPoint_BorderColor_1, eyeProjectionPos.xy);

            BlockerSum += blockerSum_Count.x;
            BlockerCount += blockerSum_Count.y;

        }
        else
        {

            float dummyhardShadow, blockerSum, blockerCount;
            FindBlocker_Horz(dummyhardShadow, blockerSum, blockerCount, shadowCoord.xy - offsetY, shadowCoord.z, dz_duv, searchRegionRadiusUV, gShadowMapID);

            BlockerSum += blockerSum;
            BlockerCount += blockerCount;

        }

    }

    BlockerAvg = BlockerSum / BlockerCount;

}

float SampleShadowMap_Horz( float3 LightLocation, float LightDepth, float stepSize )
{
    float softShadow = 0.0f;

    float sample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, LightLocation);

    if (LightDepth <= sample)
    {
        ++softShadow;
    }

    [unroll]
    for (int i = 1; i <= HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT; ++i)
    {
        float3 offset = float3(stepSize * i, 0.0f, 0.0f);

        float3 location = LightLocation + offset;

        sample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, location);

        if (LightDepth <= sample)
        {
            ++softShadow;
        }

        location = LightLocation - offset;

        sample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, location);

        if (LightDepth <= sample)
        {
            ++softShadow;
        }
    }

    return softShadow / HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT;

}

float FilterHardShadow_Horz( float FilterRadius, float EyeDepth, float3 TexCoord, float4 ShadowCoord, float2 dz_duv )
{

    float softShadow, weight_sum = 0.0f;

    softShadow = tShadows.Sample(gSamplerPoint_BorderColor_1, TexCoord) ;

    if(FilterRadius < gScreenDXY.x)
    {
        return softShadow;
    }

    softShadow *= HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[0];

    weight_sum = HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[0];

    float sampleCnt = HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT;

    float2 step = float2(FilterRadius / sampleCnt, 0.0f);

    float3 location;
    location.z = TexCoord.z;

    [unroll]
    for (int i = 1; i <= HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT; ++i)
    {

        float2 offset = step * i;
        location.xy = TexCoord.xy + offset;

        float sampleDepth = tDepthMap.Sample(gSamplerPoint_BorderColor_1, location.xy);

        if (abs(sampleDepth - EyeDepth) <= BETA)
        {
            softShadow += tShadows.Sample(gSamplerPoint_BorderColor_1, location) * HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            weight_sum += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
        }
        else
        {

            float3 csPos = float3(location.xy * 2.0 - 1.0, sampleDepth);
            csPos.y *= -1.0f;
            float sampleEyeViewDepth = ConvertToLinearDepth(sampleDepth, gPerspectiveValues.w, gPerspectiveValues.z);
            float3 posW = CalcPosW(csPos, sampleEyeViewDepth);
            float4 sampleShadowPos = gILightArray.CalcShadowPos(posW, gShadowMapID);

            float shadowMapSample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, float3(sampleShadowPos.xy, gShadowMapID));
            float biasedZReceiver = BiasedZ(sampleShadowPos.z, dz_duv, sampleShadowPos.xy - ShadowCoord.xy);

            if (biasedZReceiver <= shadowMapSample)
            {
                softShadow += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            }
            weight_sum += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];

        }

        location.xy = TexCoord.xy - offset;

        sampleDepth = tDepthMap.Sample(gSamplerPoint_BorderColor_1, location.xy);

        if (abs(sampleDepth - EyeDepth) <= BETA)
        {
            softShadow += tShadows.Sample(gSamplerPoint_BorderColor_1, location) * HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            weight_sum += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
        }
        else
        {
            float3 csPos = float3(location.xy * 2.0 - 1.0, sampleDepth);
            csPos.y *= -1.0f;
            float sampleEyeViewDepth = ConvertToLinearDepth(sampleDepth, gPerspectiveValues.w, gPerspectiveValues.z);
            float3 posW = CalcPosW(csPos, sampleEyeViewDepth);
            float4 sampleShadowPos = gILightArray.CalcShadowPos(posW, gShadowMapID);

            float shadowMapSample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, float3(sampleShadowPos.xy, gShadowMapID));
            float biasedZReceiver = BiasedZ(sampleShadowPos.z, dz_duv, sampleShadowPos.xy - ShadowCoord.xy);

            if (biasedZReceiver <= shadowMapSample)
            {
                softShadow += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            }
            weight_sum += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
        }

    }

    return softShadow / weight_sum;
    
}

float FilterHardShadow_Vert( float FilterRadius, float PenumbraSize, float EyeDepth, float3 TexCoord, float4 ShadowCoord, float2 dz_duv )
{

    float softShadow, weight_sum = 0.0f;

    softShadow = tShadows.Sample(gSamplerPoint_BorderColor_1, TexCoord);

    if(FilterRadius < gScreenDXY.y)
        return softShadow;

    softShadow *= HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[0];

    weight_sum = HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[0];

    float sampleCnt = HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT;

    float2 step = float2(0.0f, FilterRadius / sampleCnt);

    float3 location;
    location.z = TexCoord.z;

    [unroll]
    for (int i = 0; i <= HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT; ++i)
    {

        float2 offset = step * i;
        location.xy = TexCoord.xy + offset;

        float sampleEyeDepth = tDepthMap.Sample(gSamplerPoint_BorderColor_1, location.xy);

        if (abs(sampleEyeDepth - EyeDepth) < BETA)
        {

            softShadow += tShadows.Sample(gSamplerPoint_BorderColor_1, location) * HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            weight_sum += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];

        }
        else
        {
            float3 csPos = float3(location.xy * 2.0 - 1.0, sampleEyeDepth);
            csPos.y *= -1.0f;
            float sampleEyeViewDepth = ConvertToLinearDepth(sampleEyeDepth, gPerspectiveValues.w, gPerspectiveValues.z);
            float3 posW = CalcPosW(csPos, sampleEyeViewDepth);
            float4 sampleShadowPos = gILightArray.CalcShadowPos(posW, gShadowMapID);

            float shadowMapStepSize = PenumbraSize / sampleCnt;

            softShadow += SampleShadowMap_Horz(float3(sampleShadowPos.xy, gShadowMapID), ShadowCoord.z, shadowMapStepSize) * HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            weight_sum += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            
        }

        location.xy = TexCoord.xy - offset;

        sampleEyeDepth = tDepthMap.Sample(gSamplerPoint_BorderColor_1, location.xy);

        if (abs(sampleEyeDepth - EyeDepth) < BETA)
        {
            softShadow += tShadows.Sample(gSamplerPoint_BorderColor_1, location) * HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            weight_sum += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
        }
        else
        {

            float3 csPos = float3(location.xy * 2.0 - 1.0, sampleEyeDepth);
            csPos.y *= -1.0f;
            float sampleEyeViewDepth = ConvertToLinearDepth(sampleEyeDepth, gPerspectiveValues.w, gPerspectiveValues.z);
            float3 posW = CalcPosW(csPos, sampleEyeViewDepth);
            float4 sampleShadowPos = gILightArray.CalcShadowPos(posW, gShadowMapID);

            float shadowMapStepSize = PenumbraSize / sampleCnt;

            softShadow += SampleShadowMap_Horz(float3(sampleShadowPos.xy, gShadowMapID), ShadowCoord.z, shadowMapStepSize) * HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            weight_sum += HARDSHADOW_FILTER_GAUSSIAN_WEIGHT[i];
            
        }

    }

    return softShadow / weight_sum;
    
}


pixelOut_0 CalculateAvgBlockerHorizontal_HardShadow( ILightArray_Basic BasicLightArray, int LightIndex, float4 shadowCoord )
{

    pixelOut_0 result;


    float2 dz_duv = DepthGradient(shadowCoord.xy, shadowCoord.z);

    float LinearDepth = BasicLightArray.ConvertToLinearDepth(LightIndex, shadowCoord.z);
    float LightZNear = BasicLightArray.GetZNear(LightIndex);
    float LightSize = BasicLightArray.GetLightSize(LightIndex);

    float blockerSearchRadius = SearchRegionRadiusUV(LinearDepth, LightZNear, LightSize);


    FindBlocker_Horz(result.HardShadow, result.AvgBlocker_Horz.x, result.AvgBlocker_Horz.y, shadowCoord.xy, shadowCoord.z, dz_duv, blockerSearchRadius, gShadowMapID);


    return result;

}

pixelOut_1 CalculatePenumbraSize_FilterHardShadowsHorizontally( ILightArray_Basic BasicLightArray, int LightIndex, float EyeDepth, float2 texCoord, float4 shadowCoord, float4 lightPos )
{

    pixelOut_1 result;

    float2 BlockerSum_Count = tAvgBlocker.Sample(gSamplerPoint_BorderColor_1, texCoord);

    float2 dz_duv = DepthGradient(shadowCoord.xy, shadowCoord.z);

    float LinearDepth = BasicLightArray.ConvertToLinearDepth(LightIndex, shadowCoord.z);
    float LightZNear = BasicLightArray.GetZNear(LightIndex);
    float LightSize = BasicLightArray.GetLightSize(LightIndex);

    float blockerSearchRadius = SearchRegionRadiusUV(LinearDepth, LightZNear, LightSize);

    float BlockerAvg;

    ScreenSpaceFindingBlocker_Vert(BlockerAvg, BlockerSum_Count.x, BlockerSum_Count.y, texCoord, EyeDepth, shadowCoord, lightPos, dz_duv, blockerSearchRadius);

    if (BlockerSum_Count.y == 0)
    {
        result.PenumbraSize = 0.0f;
        result.SoftShadow_Horz = 1.0f;
        return result;
    }

    float LightDepth_Linear = BasicLightArray.ConvertToLinearDepth(LightIndex, shadowCoord.z);
    float BlockerAvg_Linear = BasicLightArray.ConvertToLinearDepth(LightIndex, BlockerAvg);

    result.PenumbraSize = EstimatePenumbraSize(LightSize, LightDepth_Linear, BlockerAvg_Linear);

    float3 normalW = tNormalMap.Sample(gSamplerPoint, texCoord);
    normalW = normalW * 2.0 - 1.0f;
    float3 normalV = mul(normalW, (float3x3) gViewInvT);

    float filterRadius = result.PenumbraSize;
    filterRadius *= sqrt(1.0f - (normalV.x * normalV.x));
    filterRadius *= (1 / EyeDepth);

    
    result.SoftShadow_Horz = FilterHardShadow_Horz(filterRadius, EyeDepth, float3(texCoord, gShadowMapID), shadowCoord, dz_duv);

    return result;

}

pixelOut_2 FilterShadowsVertically( float2 TexCoord, float EyeDepth, float4 ShadowCoord )
{
    pixelOut_2 result;

    float2 dz_duv = DepthGradient(ShadowCoord.xy, ShadowCoord.z);

    float PenumbraSize = tPenumbraSize.Sample(gSamplerPoint_BorderColor_0, TexCoord);

    float3 normalW = tNormalMap.Sample(gSamplerPoint, TexCoord);
    normalW = normalW * 2.0 - 1.0f;
    float3 normalV = mul(normalW, (float3x3) gViewInvT);

    float filterRadius = PenumbraSize;
    filterRadius *= sqrt(1.0f - (normalV.y * normalV.y));
    filterRadius *= (1 / EyeDepth);

    result.SoftShadow = FilterHardShadow_Vert(filterRadius, PenumbraSize, EyeDepth, float3(TexCoord, 0.0f), ShadowCoord, dz_duv);

    return result;
}


