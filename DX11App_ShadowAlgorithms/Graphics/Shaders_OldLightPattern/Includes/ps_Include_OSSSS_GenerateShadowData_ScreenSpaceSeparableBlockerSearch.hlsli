#ifndef HLSL_OSSSS_SHADOWDATA
#define HLSL_OSSSS_SHADOWDATA

#if defined(__INTELLISENSE__)
	#include "../Includes/Include_Samplers.hlsli"
	#include "../Includes/Include_GaussianWeights.hlsli"
	#include "../Includes/Include_CommonConstantBuffer.hlsli"
	#include "../Includes/Include_HelperFunc.hlsli"
    #include "../Includes/Include_PoissonDisks.hlsli"


#define BLOCKER_SEARCH_WIDTH_SAMPLE_COUNT    5
#define GAUSSIAN_WEIGHTS_BLOCKER_SEARCH     Gaussian5
    
#define FILTER_WIDTH_HARDSHADOW             5
#define GAUSSIAN_WEIGHTS_HARDSHADOW         Gaussian5
    
#define BILATERAL_DEPTH                     0.003f

#else

#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#include "Graphics/Shaders/Includes/Include_GaussianWeights.hlsli"
#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"
#include "Graphics/Shaders/Includes/Include_PoissonDisks.hlsli"

#endif



#define FILTER_RADIUS_HARDSHADOW            (FILTER_WIDTH_HARDSHADOW - 1.0f) * 0.5f
#define FILTER_RADIUS_HARDSHADOW_INV        (1.0f / FILTER_RADIUS_HARDSHADOW)

#define BLOCKER_SEARCH_RADIUS_SAMPLE_COUNT   (BLOCKER_SEARCH_WIDTH_SAMPLE_COUNT - 1.0f) * 0.5f



//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------
Texture2D tDepthMap : register(t0);
Texture2DArray tShadowMapArray : register(t1);
Texture2D tGBuffer_Normal : register(t2);
Texture2D tAvgBlocker : register(t3);
Texture2D tHardShadow : register(t4);
Texture2D tPenumbraSize : register(t5);

//--------------------------------------------------------------------------------------
// Constant Buffer
//--------------------------------------------------------------------------------------

cbuffer cb_ossss_generateShadowData : register(b6)
{
    float gShadowMapID;
    float3 cb_ossss_pad;

    float2 gLight2DDirInEye_horz;
    float2 gLight2DDirInEye_vert;
};


//--------------------------------------------------------------------------------------
// Strcuturs
//--------------------------------------------------------------------------------------


struct vertexOut_render2D
{
    float4 PosH : SV_Position;
    float2 Tex : TEXCOORD0;
    float2 csPos : TEXCOORD1;
};

struct pixelOut_0
{
    float AvgBlocker_Vert : SV_TARGET0;
};

struct pixelOut_1
{
    float HardShadowData : SV_TARGET0;
    float PenumbraSize : SV_TARGET1;
};

struct pixelOut_2
{
    float SoftShadows : SV_TARGET0;
};


//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface IGenerateShadowData_0
{
    pixelOut_0 CalcOutput( float4 shadowCoord, float lightZNear, float lightSize );
};

interface IGenerateShadowData_1
{
    pixelOut_1 CalcOutput( float2 texCoord, float EyeDepth_Linear, float4 shadowCoord, float3 posW, float3 lightPosW, float lightZNear, float lightSize, float4 lightPerspectiveValues, float2 dz_duv );
};

interface IGenerateShadowData_2
{
    pixelOut_2 CalcOutput( float2 texCoord, float EyeDepth_Linear, float4 shadowCoord, float3 posW, float3 lightPosW, float2 dz_duv );
};


//--------------------------------------------------------------------------------------
// Functions
//--------------------------------------------------------------------------------------

// Computes depth offset (bias)
float BiasedZ( float z0, float2 dz_duv, float2 offset )
{
    return z0 + dot(dz_duv, offset);
}


bool IsBlocker( float depthSample, float ref_depth, float2 dz_duv, float2 offset )
{
    float biasedDepth = BiasedZ(ref_depth, dz_duv, offset);

    return depthSample < biasedDepth;
}


float ShadowMapSampling( float frag_shadow_depth, float3 location, float penumbraSize, float2 dz_duv, float2 Dir )
{
    float shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location);

    float shadowValue = shadowMapSample >= frag_shadow_depth;

    float sampleCount = FILTER_RADIUS_HARDSHADOW;

    float2 step = Dir * (penumbraSize / sampleCount).xx;

    float3 offset = 0.0f;
    float z;

    float stepLength = length(step);

    [unroll]
    for (int i = 1; i <= FILTER_RADIUS_HARDSHADOW; ++i)
    {
        offset.xy = step * i.xx;
        float rand_num = rand(offset.xy) * 2.0f - 1.0f;

        offset += rand_num * stepLength;

        z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy);

        shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location + offset);

        shadowValue += shadowMapSample >= z;

        offset.xy *= -1.0f;

        offset -= rand_num * stepLength;

        z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy);

        shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location + offset);

        shadowValue += shadowMapSample >= z;
    }

    return shadowValue / FILTER_WIDTH_HARDSHADOW;

}


float ShadowMapSampling_horz( float frag_shadow_depth, float3 location )
{
    float shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location);

    return shadowMapSample >= frag_shadow_depth;

}


float ShadowMapSampling_vertHorz( float penumbraSize, float frag_shadow_depth, float3 location, float2 dz_duv )
{
    float shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location);

    float shadowValue = shadowMapSample >= frag_shadow_depth;

    if (penumbraSize < gShadowMapDXY.x)
    {
        return shadowValue;
    }

    float sampleCount = FILTER_RADIUS_HARDSHADOW;

    float3 step = float3(penumbraSize / sampleCount, 0.0f, 0.0f);

    float3 offset;
    float z;

    [unroll]
    for (int i = 1; i <= FILTER_RADIUS_HARDSHADOW; ++i)
    {
        offset = step * i;

        shadowMapSample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, location + offset);

        z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy);

        shadowValue += shadowMapSample >= z;

        shadowMapSample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, location - offset);

        z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy * -1.0f);

        shadowValue += shadowMapSample >= z;
    }

    return shadowValue / FILTER_WIDTH_HARDSHADOW;

}


void ApplyFilter_MeanBlocker( Texture2DArray shadowMapArray, out float blockerCount, out float blockerAvg, float2 shadow_coord, float Radius, float ref_depth, float2 dz_duv )
{
    float3 location = float3(shadow_coord, gShadowMapID);

    blockerAvg = 0.0f;
    blockerCount = 0.0f;

    float weight_sum = 0.0f;

    float depthSample;

    depthSample = shadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0);

    if (depthSample < ref_depth)
    {
        blockerAvg += depthSample;
        blockerCount++;
    }


    float3 offset = float3(0.0f, 0.0f, 0.0f);

    float radiusSampleCount = BLOCKER_SEARCH_RADIUS_SAMPLE_COUNT;

    float2 step = (Radius / radiusSampleCount) * float2(0.0f, 1.0f);

    [unroll]
    for (int i = 1; i <= BLOCKER_SEARCH_RADIUS_SAMPLE_COUNT; ++i)
    {

        offset.xy = step * i;

        depthSample = shadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0);

        if (IsBlocker(depthSample, ref_depth, dz_duv, offset.xy))
        {
            blockerAvg += depthSample;
            blockerCount++;
        }


        depthSample = shadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0);
        if (IsBlocker(depthSample, ref_depth, dz_duv, offset.xy))
        {
            blockerAvg += depthSample;
            blockerCount++;
        }

    }

    blockerAvg /= blockerCount;

}


void ApplyFilter_MeanBlocker( Texture2D tAvgBlocker, out float blockerCount, out float blockerAvg, float2 screen_coord, float Radius, float LightDepth, float EyeLinearDepth, float2 Dir )
{
    blockerAvg = 0.0f;
    blockerCount = 0;

    float weight_sum = 0.0f;

    float lightDepthSample, screenDepthSample;

    lightDepthSample = tAvgBlocker.SampleLevel(gSamplerPoint_BorderColor_1, screen_coord, 0);

    if (lightDepthSample < LightDepth)
    {
        blockerAvg += lightDepthSample * GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[0];
        weight_sum += GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[0];
        blockerCount++;
    }


    float2 offset = float2(0.0f, 0.0f), location = float2(0.0f, 0.0f);

    float radiusSampleCount = BLOCKER_SEARCH_RADIUS_SAMPLE_COUNT;

    float2 step = (Radius / radiusSampleCount) * Dir;

    [unroll]
    for (int i = 1; i <= BLOCKER_SEARCH_RADIUS_SAMPLE_COUNT; ++i)
    {

        offset.xy = step * i;

        location = screen_coord + offset;

        screenDepthSample = tDepthMap.Sample(gSamplerPoint_BorderColor_1, location);

        float screenDepthSampleLinear = ConvertToLinearDepth(screenDepthSample, gPerspectiveValues.w, gPerspectiveValues.z);

        if (abs(EyeLinearDepth - screenDepthSampleLinear) < BILATERAL_DEPTH)
        {

            lightDepthSample = tAvgBlocker.SampleLevel(gSamplerPoint_BorderColor_1, location, 0);

            //if (IsBlocker(lightDepthSample, LightDepth, dz_duv, offset.xy))
            if (lightDepthSample < LightDepth)
            {
                blockerAvg += lightDepthSample * GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[i];
                weight_sum += GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[i];
                blockerCount++;
            }
        }

        location = screen_coord - offset;

        screenDepthSample = tDepthMap.Sample(gSamplerPoint_BorderColor_1, location);

        screenDepthSampleLinear = ConvertToLinearDepth(screenDepthSample, gPerspectiveValues.w, gPerspectiveValues.z);

        if (abs(EyeLinearDepth - screenDepthSampleLinear) < BILATERAL_DEPTH)
        {

            lightDepthSample = tAvgBlocker.SampleLevel(gSamplerPoint_BorderColor_1, location, 0);

            //if (IsBlocker(lightDepthSample, LightDepth, dz_duv, offset.xy))
            if (lightDepthSample < LightDepth)
            {
                blockerAvg += lightDepthSample * GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[i];
                weight_sum += GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[i];
                blockerCount++;
            }

        }

    }

    blockerAvg /= weight_sum;

}


void ApplyFilter_FilterHardShadow_Horz( out float SoftShadow, float2 tex_coord, float4 shadowCoord, float ref_depth, float2 step, float PenumbraSize, float ScreenPenumbraSize, float2 dz_duv /*, float Radius, float2 Dir, float LightDepth_Linear, float shadowCoordDerivation*/ )
{

    

    float weight_sum = 0.0f;

    float depthSample;
    
    SoftShadow = tHardShadow.Sample(gSamplerLinear_Border_1, tex_coord);

    if (ScreenPenumbraSize < gScreenDXY.x && ScreenPenumbraSize < gScreenDXY.y)
    {
        return;
    }

    SoftShadow *= GAUSSIAN_WEIGHTS_HARDSHADOW[0];

    weight_sum = GAUSSIAN_WEIGHTS_HARDSHADOW[0];
    
    float2 offset = float2(0.0f, 0.0f);

   

    float2 uv;

    [unroll]
    for (int i = 1; i <= FILTER_RADIUS_HARDSHADOW; ++i)
    {

        offset.xy = step * i;

        uv = tex_coord + offset;


        depthSample = tDepthMap.Sample(gSamplerLinear_Border_1, uv);
        float linearDepth = ConvertToLinearDepth(depthSample, gPerspectiveValues.w, gPerspectiveValues.z);

        if (abs(ref_depth - linearDepth) < BILATERAL_DEPTH)
        {

            SoftShadow += tHardShadow.Sample(gSamplerLinear_Border_1, uv) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];

        }
        else
        {
            float sampleCnt = FILTER_RADIUS_HARDSHADOW;
            float shadowMapStep = PenumbraSize / sampleCnt;

            float3 shadowLocation = float3(shadowCoord.xy, 0.0f);

            float3 shadowMap_offset = (shadowMapStep * i, 0.0f, 0.0f);

            shadowLocation += shadowMap_offset;

            float z = BiasedZ(shadowCoord.z, dz_duv, shadowMap_offset.xy);

            SoftShadow += ShadowMapSampling_horz(z, shadowLocation) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];
        }
        
        uv = tex_coord - offset;


        depthSample = tDepthMap.Sample(gSamplerLinear_Border_1, uv);
        linearDepth = ConvertToLinearDepth(depthSample, gPerspectiveValues.w, gPerspectiveValues.z);

        if (abs(ref_depth - linearDepth) < BILATERAL_DEPTH)
        {
            SoftShadow += tHardShadow.Sample(gSamplerLinear_Border_1, uv) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];
            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];
        }
        else
        {
            float sampleCnt = FILTER_RADIUS_HARDSHADOW;
            float shadowMapStep = PenumbraSize / sampleCnt;

            float3 shadowLocation = float3(shadowCoord.xy, 0.0f);

            float3 shadowMap_offset = (shadowMapStep * -i, 0.0f, 0.0f);

            shadowLocation += shadowMap_offset;

            float z = BiasedZ(shadowCoord.z, dz_duv, shadowMap_offset.xy);

            SoftShadow += ShadowMapSampling_horz(z, shadowLocation) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];
        }

    }

    SoftShadow /= weight_sum;

}


float3 CalcPosW( float3 csPos, float linearDepth )
{
    float3 posV = float3(0.0f, 0.0f, 0.0f);
    posV.xy = csPos.xy * gPerspectiveValues.xy * linearDepth.xx;
    posV.z = linearDepth;
    
    return mul(float4(posV, 1.0), gViewInv).xyz;
}


float EstimatePenumbraSize( float _lightSize, float _receiver, float _blocker )
{
    return _lightSize * (_receiver - _blocker) / _blocker;
}


// Derivatives of light-space depth with respect to texture coordinates
float2 DepthGradient( float2 uv, float z )
{
    float2 dz_duv = 0;

	// Packing derivatives of u,v, and distance to light source w.r.t. screen space x, and y
    float3 duvdist_dx = ddx(float3(uv, z));
    float3 duvdist_dy = ddy(float3(uv, z));

	// Top row of 2x2
    dz_duv.x = duvdist_dy.y * duvdist_dx.z;
    dz_duv.x -= duvdist_dx.y * duvdist_dy.z;
	
	// Bottom row of 2x2
    dz_duv.y = duvdist_dx.x * duvdist_dy.z;
    dz_duv.y -= duvdist_dy.x * duvdist_dx.z;

	// Multiply ddist/dx and ddist/dy by inverse transpose of Jacobian
    float det = (duvdist_dx.x * duvdist_dy.y) - (duvdist_dx.y * duvdist_dy.x);
    dz_duv /= det;

    return dz_duv;
}


// Using similar triangles from the surface point to the area light
float2 SearchRegionRadiusUV( float zLight, float zNear, float lightSize )
{
    return lightSize * (zLight - zNear) / zLight;
    //return lightSize * (zLight - gZNear) / (zLight - gZFar);
    //return lightSize *  (zLight - gZFar) / (zLight - gZNear);
    //return lightSize * zNear / zLight;
}


//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CGenerateShadowData_0 : IGenerateShadowData_0
{

    pixelOut_0 CalcOutput( float4 shadowCoord, float lightZNear, float lightSize )
    {

        pixelOut_0 result;

        float2 dz_duv = DepthGradient(shadowCoord.xy, shadowCoord.z);

        float searchRadius = SearchRegionRadiusUV(shadowCoord.w, lightZNear, lightSize);

        float blockerAvg;
        uint blockerCount;

        ApplyFilter_MeanBlocker(tShadowMapArray, blockerCount, blockerAvg, shadowCoord.xy, searchRadius, shadowCoord.z, dz_duv);
        
        result.AvgBlocker_Vert = blockerCount == 0 ? shadowCoord.z : blockerAvg;

        return result;
    }

};

class CGenerateShadowData_1 : IGenerateShadowData_1
{
    pixelOut_1 CalcOutput( float2 texCoord, float EyeDepth_Linear, float4 shadowCoord, float3 posW, float3 lightPosW, float lightZNear, float lightSize, float4 lightPerspectiveValues, float2 dz_duv )
    {
        pixelOut_1 result;

        float DistanceMultiplier = shadowCoord.w / EyeDepth_Linear;

        float3 normal = tGBuffer_Normal.Sample(gSamplerPoint, texCoord);

        normal = normalize(normal * 2.0f - 1.0f);

        float3 EyeVector = gEyePos - posW;
        float3 LightVector = lightPosW - posW;

        EyeVector = normalize(EyeVector);
        LightVector = normalize(LightVector);

        float EyeAngle = dot(normal, EyeVector);
        float LightAngle = dot(normal, LightVector);

        float AngleMultiplier = EyeAngle / LightAngle;

        AngleMultiplier = clamp(AngleMultiplier, 0.0f, 1.0f);

        float blockerSearchRadius = SearchRegionRadiusUV(shadowCoord.w, lightZNear, lightSize);

        float screenblockerSearchRadius = blockerSearchRadius * DistanceMultiplier * AngleMultiplier;

        float blockerCount, blockerAvg;

        ApplyFilter_MeanBlocker(tAvgBlocker, blockerCount, blockerAvg, texCoord, screenblockerSearchRadius, shadowCoord.z, EyeDepth_Linear, gLight2DDirInEye_horz);

        float blockerAvg_linear = ConvertToLinearDepth(blockerAvg, lightPerspectiveValues.w, lightPerspectiveValues.z);

        float penumbraSize = EstimatePenumbraSize(lightSize, shadowCoord.w, blockerAvg_linear);

        result.HardShadowData = ShadowMapSampling(shadowCoord.z, float3(shadowCoord.xy, gShadowMapID), penumbraSize, dz_duv, float2(0.0f, 1.0f));
        result.PenumbraSize = penumbraSize;

        return result;

    }

};

class CGenerateShadowData_2 : IGenerateShadowData_2
{
    pixelOut_2 CalcOutput( float2 texCoord, float EyeDepth_Linear, float4 shadowCoord, float3 posW, float3 lightPosW, float2 dz_duv )
    {
        pixelOut_2 result;

        float DistanceMultiplier = shadowCoord.w / EyeDepth_Linear;

        float3 normal = tGBuffer_Normal.Sample(gSamplerPoint, texCoord);

        normal = normalize(normal * 2.0f - 1.0f);

        float3 EyeVector = gEyePos - posW;
        float3 LightVector = lightPosW - posW;

        EyeVector = normalize(EyeVector);
        LightVector = normalize(LightVector);

        float EyeAngle = dot(normal, EyeVector);
        float LightAngle = dot(normal, LightVector);

        float AngleMultiplier = clamp(EyeAngle / LightAngle, 0.0f, 1.0f);

        float PenumbraSize = tPenumbraSize.Sample(gSamplerPoint, texCoord);
        
        float ScreenPenumbraSize = PenumbraSize * DistanceMultiplier * AngleMultiplier;

        float filter_radius_sample_count = FILTER_RADIUS_HARDSHADOW;

        float2 step = (ScreenPenumbraSize / filter_radius_sample_count);

        step *= gLight2DDirInEye_horz;

        ApplyFilter_FilterHardShadow_Horz(result.SoftShadows, texCoord, shadowCoord, EyeDepth_Linear, step, PenumbraSize, ScreenPenumbraSize, dz_duv);

        return result;

    }

};

#endif