#if defined(__INTELLISENSE__)

#include "../Includes/Include_GaussianWeights.hlsli"
#include "../Includes/Include_Samplers.hlsli"

#define FILTER_RADIUS 5
#define GaussianWeight Gaussian5

#else

#include "Graphics/Shaders/Includes/Include_GaussianWeights.hlsli"
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"

#endif

#define FILTER_HALF_RADIUS ((FILTER_RADIUS - 1) / 2)

//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------
Texture2DArray tVarianceShadwoMapArray : register(t0);


//--------------------------------------------------------------------------------------
// CBuffer
//--------------------------------------------------------------------------------------

cbuffer cb_vsm_filter_perLight : register(b6)
{
    float2 gTexelSize;
    float gTexArrayIndex;
    float cb_vsm_filter_perLight_pad;

    float2 gHorzDirection;
    float2 gVertDirection;
}


//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------


struct pixelIn
{
    float4 PosH : SV_Position;
    float2 Tex : TEXCOORD0;
    float2 csPos : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface I_GaussFilterManager
{

    float4 ApplyFilter( float2 tex );
    float4 ApplyFilter_Radius_Dir( float2 tex, float Radius );

};

//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CFilterManager_Horizontally : I_GaussFilterManager
{
    float4 ApplyFilter(float2 tex)
    {

        float3 location = float3(tex, gTexArrayIndex);

        float4 r;

        float weight_sum = 0.0f;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0) * GaussianWeight[0];
        weight_sum += GaussianWeight[0];

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        [unroll]
        for (int i = 1; i <= FILTER_HALF_RADIUS; ++i)
        {

            offset.x = gTexelSize.xy * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0) * GaussianWeight[i];
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0) * GaussianWeight[i];

            weight_sum += GaussianWeight[i] * 2.0f;

        }

        return r / weight_sum;

    }

    float4 ApplyFilter_Radius_Dir( float2 tex, float Radius )
    {
        float3 location = float3(tex, gTexArrayIndex);

        float4 r;

        float weight_sum = 0.0f;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0) * GaussianWeight[0];
        weight_sum += GaussianWeight[0];

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        float2 step = (Radius / FILTER_HALF_RADIUS) * gHorzDirection.xy;

        [unroll]
        for (int i = 1; i <= FILTER_HALF_RADIUS; ++i)
        {

            offset.xy = step * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0) * GaussianWeight[i];
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0) * GaussianWeight[i];

            weight_sum += GaussianWeight[i] * 2.0f;

        }

        return r / weight_sum;

    }
};

class CFilterManager_Vertically : I_GaussFilterManager
{
    float4 ApplyFilter( float2 tex )
    {
        float3 location = float3(tex, 0);

        float4 r;

        float weight_sum = 0.0f;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0) * GaussianWeight[0];
        weight_sum += GaussianWeight[0];

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        [unroll]
        for (int i = 1; i <= FILTER_HALF_RADIUS; ++i)
        {

            offset.y = gTexelSize * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0) * GaussianWeight[i];
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0) * GaussianWeight[i];

            weight_sum += GaussianWeight[i] * 2.0f;

        }

        return r / weight_sum;

    }

    float4 ApplyFilter_Radius_Dir( float2 tex, float Radius )
    {
        float3 location = float3(tex, gTexArrayIndex);

        float4 r;

        float weight_sum = 0.0f;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0) * GaussianWeight[0];
        weight_sum += GaussianWeight[0];

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        float2 step = (Radius / FILTER_HALF_RADIUS) * gVertDirection.xy;

        [unroll]
        for (int i = 1; i <= FILTER_HALF_RADIUS; ++i)
        {

            offset.xy = step * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0) * GaussianWeight[i];
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0) * GaussianWeight[i];

            weight_sum += GaussianWeight[i] * 2.0f;

        }

        return r / weight_sum;

    }
};
