#pragma once


enum class SAMPLER_SLOTS
{

	BILINEAR_WRAP = 0,
	BILINEAR_CLAMP = 1,
	BILINEAR_BORDER_0 = 2,
	BILINEAR_BORDER_1 = 3,
	POINT_WRAP = 4,
	POINT_CLAMP = 5,
	POINT_BORDER_0 = 6,
	POINT_BORDER_1 = 7,
	COMPARISON_LESS = 8,
	COMPARISON_GREATER = 9

};

enum RENDER_STATE_RS
{
	RENDER_STATE_RS_NONE = -1,
	RENDER_STATE_RS_DEFAULT,
	RENDER_STATE_RS_NOCULLING,
	RENDER_STATE_RS_SHADOWMAP,
	RENDER_STATE_RS_SHADOWMAP_ReversedZ,
	RENDER_STATE_RS_COUNTERCLOCKWISE,
	RENDER_STATE_RS_COUNTERCLOCKWISE_WIREFRAME,
	RENDER_STATE_RS_WIREFRAME

};

enum RENDER_STATE_BS
{
	RENDER_STATE_BS_NONE = -1,
	RENDER_STATE_BS_DEFAULT,
	RENDER_STATE_BS_TRANSPARENCY,
	RENDER_STATE_BS_NOWRITE_BACKBUFFER
};

enum RENDER_STATE_DS
{
	RENDER_STATE_DS_NONE = -1,
	RENDER_STATE_DS_DEFAULT,
	RENDER_STATE_DS_NODEPTH,
	RENDER_STATE_DS_STENCIL_ENABLE,
	RENDER_STATE_DS_REVERSEDZ
};

class RenderStates
{
public:


	static float DepthBias;
	static float SlopeScaledDepthBias;
	static float DepthBiasClamp;

	static void InitAll();
	static void DestroyAll();

	

	static void SetBlendState(RENDER_STATE_BS _bsType, ID3D11DeviceContext* _context);
	static void SetRasterizerState(RENDER_STATE_RS _rsType, ID3D11DeviceContext* _context);
	static void SetDepthStencilState(RENDER_STATE_DS _dsType, ID3D11DeviceContext* _context );
	static void SetSamplerStates(ID3D11DeviceContext* _context);

	static ComPtr<ID3D11SamplerState> SSLinearWrap;
	static ComPtr<ID3D11SamplerState> SSLinearClamp;
	static ComPtr<ID3D11SamplerState> SSLinearBorder0;
	static ComPtr<ID3D11SamplerState> SSLinearBorder1;
	static ComPtr<ID3D11SamplerState> SSComparisonState_Less;
	static ComPtr<ID3D11SamplerState> SSComparisonState_Greater;
	static ComPtr<ID3D11SamplerState> SSPointWrap;
	static ComPtr<ID3D11SamplerState> SSPointClamp;
	static ComPtr<ID3D11SamplerState> SSPointBorderColor0;
	static ComPtr<ID3D11SamplerState> SSPointBorderColor1;

private:
	static ComPtr<ID3D11RasterizerState> RSCounterClockwise;
	static ComPtr<ID3D11RasterizerState> RSCounterClockwiseWireFrame;
	static ComPtr<ID3D11RasterizerState> RSShadowMap;
	static ComPtr<ID3D11RasterizerState> RSShadowMap_ReversedZ;
	static ComPtr<ID3D11RasterizerState> RSNoCulling;
	static ComPtr<ID3D11RasterizerState> RSWireframe;

	static ComPtr<ID3D11DepthStencilState> DSNoDepth;
	static ComPtr<ID3D11DepthStencilState> DSSetStencil;
	static ComPtr<ID3D11DepthStencilState> DSReversedZ;


	static ComPtr<ID3D11BlendState> BSUseTransparency;
	static ComPtr<ID3D11BlendState> BSNoWriteToBackBuffer;

	static RENDER_STATE_RS mCurrentRSState;
	static RENDER_STATE_DS mCurrentDSState;
	static RENDER_STATE_BS mCurrentBSState;
};
