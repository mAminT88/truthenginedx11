#if defined(__INTELLISENSE__)
	#include "../Includes/ps_Include_OSSSS_GenerateShadowData.hlsli"
#else
	#include "Graphics/Shaders/Includes/ps_Include_OSSSS_GenerateShadowData.hlsli"
#endif




IGenerateShadowData_0 g_IGenerateShadowData_0;
IGenerateShadowData_1 g_IGenerateShadowData_1;


pixelOut_0 main_0( vertexOut_render2D pin )
{
	float depth = tDepthMap.Sample(gSamplerPoint, pin.Tex);

	if (depth > 0.99999)
		discard;

	float linearDepth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);

	float3 posW = CalcPosW(float3(pin.csPos.xy, depth), linearDepth);

	float4 shadowCoord = iLight.CalcShadowPos(posW);

	return g_IGenerateShadowData_0.CalcOutput(shadowCoord);

}


pixelOut_1 main_1( vertexOut_render2D pin )
{
	float depth = tDepthMap.Sample(gSamplerPoint, pin.Tex);

	if(depth > 0.99999)
		discard;

	float linearDepth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);

	float3 posW = CalcPosW(float3(pin.csPos.xy, depth), linearDepth);

	float4 shadowCoord = iLight.CalcShadowPos(posW);

	return g_IGenerateShadowData_1.CalcOutput(pin.Tex, linearDepth, shadowCoord, posW, iLight.GetPosition());

}

//pixelOut_2 main_2( vertexOut_render2D pin )
//{
//    float depth = tDepthMap.Sample(gSamplerPoint, pin.Tex);

//    if (depth > 0.99999)
//        discard;

//    float linearDepth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);

//    float3 posW = CalcPosW(float3(pin.csPos.xy, depth), linearDepth);

//    float4 shadowCoord = gILightArray.CalcShadowPos(posW, gShadowMapID);

//    float4 lightPerspectiveValues = gILightArray.GetPerspectiveValues(gShadowMapID);

//    return g_IGenerateShadowData_1.CalcOutput(pin.Tex, linearDepth, shadowCoord.w, shadowCoord, gILightArray.GetZNear(gShadowMapID), gILightArray.GetLightSize(gShadowMapID), gILightArray.GetPerspectiveValues(gShadowMapID));
//}



//pixelOut main( vertexOut_render2D pin )
//{
//    pixelOut pOut;
//    pOut.HardShadowMap = 1.0f;
//    pOut.PenumbraSizeMap = 0.0f;
	
//    float linearDepth_light = 0.0f;

//    float depth = gDepthMap.Load(int3(pin.PosH.xy, 0));

//    if (depth > 0.9999)
//    {
//        discard;
//    }

//    float linearDepth = ConvertToLinearDepth_Perspective(depth, gPerspectiveValues.z, gPerspectiveValues.w);

//    float3 posW = CalcPosW(float3(pin.csPos.xy, depth), linearDepth);

//    [branch]
//    if (gGetLight)
//    {

//        [branch]
//        if (gSLights[gLightID].CastShadow && gLightID > -1)
//        {
//            float4 shadowPos = mul(float4(posW, 1.0f), gSLights[gLightID].ShadowTransform);
//            shadowPos.xyz /= shadowPos.w;

//            if (shadowPos.z > 1.0f)
//                return pOut;

//            //Calculate Hard Shadow Bit
//            float ShadowMapSample = gShadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, float3(shadowPos.xy, gSLights[gLightID].ShadowMapID), 0).x;
//            pOut.HardShadowMap.x = step(shadowPos.z, ShadowMapSample);

//            float2 dz_duv = DepthGradient(shadowPos.xy, shadowPos.z);
//            float2 searchRadiusUV = SearchRegionRadiusUV(shadowPos.w, gSLights[gLightID].zNear, gSLights[gLightID].LightSize);

//            float blocker, numBlockers;
//            FindBlocker(blocker, numBlockers, shadowPos.xy, shadowPos.z, dz_duv, searchRadiusUV, gSLights[gLightID].ShadowMapID);

//            if (numBlockers != 0.0f)
//            {
//                float linearBlocker = ConvertToLinearDepth_Perspective(blocker, gSLights[gLightID].PerpectiveValues.z, gSLights[gLightID].PerpectiveValues.w);
				
//                pOut.PenumbraSizeMap = EstimatePenumbraSize(gSLights[gLightID].LightSize, shadowPos.w, linearBlocker);
//            }

//        }

//    }
	
//    return pOut;
//}