#if defined(__INTELLISENSE__)
#include "../Includes/ps_Include_blurBilateralByDepthNormal.hlsli"
#else
#include "Graphics/Shaders/Includes/ps_Include_blurBilateralByDepthNormal.hlsli"
#endif

IApplyBlur gIApplyBlur;

struct vertexOut
{
	float4 PosH : SV_Position;
	float2 Tex : TEXCOORD0;
	float2 csPos : TEXCOORD1;
};

float4 main(vertexOut pin) : SV_TARGET
{
	float color = gIApplyBlur.DoBlur(pin.Tex);

	return color.xxxx;
}