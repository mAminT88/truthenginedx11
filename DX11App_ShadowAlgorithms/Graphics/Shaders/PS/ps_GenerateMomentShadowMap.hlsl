
#if defined(__INTELLISENSE__)
    #include "../Includes/Include_GenerateMomentShadowMap.hlsli"
#else
    #include "Graphics/Shaders/Includes/Include_GenerateMomentShadowMap.hlsli"
#endif

float4 main( pixelIn pin) : SV_TARGET
{
    //float FragDepth = pin.ShadowPos.z * 2.0 - 1.0f;

    float LinearDepth = iLight.ConvertToNormalizedLinearDepth(pin.ShadowPos) * 2.0f - 1.0f;

    return Generate4Moments(LinearDepth);
}