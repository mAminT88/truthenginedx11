
#if defined(__INTELLISENSE__)
#include "../Includes/Include_Samplers.hlsli"
#else
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#endif

Texture2D texInput : register(t0);

struct vertexOut
{
	float4 PosH : SV_Position;
	float2 Tex : TEXCOORD0;
	float2 csPos : TEXCOORD1;
};

float4 main(vertexOut pIn) : SV_TARGET
{
	return texInput.Sample(gSamplerLinear, pIn.Tex);
}