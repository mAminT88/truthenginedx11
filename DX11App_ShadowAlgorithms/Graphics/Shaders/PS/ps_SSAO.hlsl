#if defined(__INTELLISENSE__)
#include "../Includes/ps_Include_SSAO.hlsli"
#else
#include "Graphics/Shaders/Includes/ps_Include_SSAO.hlsli"
#endif

struct vertexOut
{
	float4 PosH : SV_Position;
	float2 Tex : TEXCOORD0;
	float2 csPos : TEXCOORD1;
};

float main(vertexOut pin) : SV_TARGET
{
	return gISSAO.Calculate_SSAO(pin.Tex, pin.csPos);
}