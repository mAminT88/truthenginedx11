#if defined(__INTELLISENSE__)
#include "../Includes/ps_Include_SeperableGaussianFilter.hlsli"

#else
#include "Graphics/Shaders/Includes/ps_Include_SeperableGaussianFilter.hlsli"
#endif

IGaussFilterManager gIGaussFilterManager;
IBoxFilterManager gIBoxFilterManager;

float4 main_Gauss( pixelIn pin) : SV_TARGET
{
    return gIGaussFilterManager.ApplyFilter(pin.Tex);
}

float4 main_Box( pixelIn pin ) : SV_TARGET
{
    return gIBoxFilterManager.ApplyFilter(pin.Tex);
}