#if defined(__INTELLISENSE__)
    #include "../Includes/Include_GenerateSAT.hlsli"
	#include "../Includes/Include_CommonConstantBuffer.hlsli"
#else
    #include "Graphics/Shaders/Includes/Include_GenerateSAT.hlsli"
    #include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#endif

struct pixelIn
{
    float4 PosH : SV_Position;
    float2 Tex : TEXCOORD0;
    float2 csPos : TEXCOORD1;
};

ISAT g_ISAT;

float4 main(pixelIn pin) : SV_TARGET
{
    return g_ISAT.CalcSAT(pin.Tex, gShadowMapDXY);
}

uint4 main_uint(pixelIn pin) : SV_Target
{
    return g_ISAT.CalcSAT_INT(pin.PosH.xy, gShadowMapDXY);
}