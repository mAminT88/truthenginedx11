#if defined(__INTELLISENSE__)

#include "../Includes/ps_Include_PostFX_FinalPass.hlsli"

#else

#include "Graphics/Shaders/Includes/ps_Include_PostFX_FinalPass.hlsli"

#endif


struct vertexOut
{
	float4 PosH : SV_Position;
	float2 Tex : TEXCOORD0;
	float2 csPos : TEXCOORD1;
};


float4 FinalPassPS(vertexOut In) : SV_TARGET
{
	// Get the color sample
	float3 hdr = HDRTex.Sample(gSamplerPoint, In.Tex).xyz;

	//float3 color = tColor.Sample(gSamplerPoint, In.Tex).xyz;

	//color *= hdr;

	//Bloom
	gIBloom.ApplyBloom(hdr/*color*/, In.Tex);

	// Tone mapping
	hdr = gIToneMapping.ToneMapping(hdr);

	// Output the LDR value
	return float4(hdr, 1.0);
}