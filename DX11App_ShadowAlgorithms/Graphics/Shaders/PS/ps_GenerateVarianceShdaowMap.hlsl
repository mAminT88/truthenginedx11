#if defined(__INTELLISENSE__)

#include "../Includes/ps_Include_GenerateVarianceShadowMap.hlsli"

#else

#include "Graphics/Shaders/Includes/ps_Include_GenerateVarianceShadowMap.hlsli"

#endif

IVSMGenerator g_IVSMGenerator;

float4 main(pixelIn pin) : SV_Target
{
    return g_IVSMGenerator.CalcMoments(pin.PosH, pin.LinearDepth);

}

int4 main_INT(pixelIn pin) : SV_Target
{
    return g_IVSMGenerator.CalcMoments_INT(pin.PosH, pin.LinearDepth);
}

