#if defined(__INTELLISENSE__)

#include "../Includes/ps_Include_DeferredShading.hlsli"

#else

#include "Graphics/Shaders/Includes/ps_Include_DeferredShading.hlsli"

#endif



float3 CalcWorldPos(float2 posH, float viewDepth)
{
	float4 posV;
	posV.xy = posH.xy * gPerspectiveValues.xy * viewDepth;
	posV.z = viewDepth;
	posV.w = 1.0f;

	float4 posW = mul(posV, gViewInv);
	return posW.xyz;
}

pixelOut main(vertexOut pin) : SV_Target
{
	GBufferData gbData = gIGBufferManager.GetData(pin.Tex);

	if (gbData.depth > 0.99999)
	{
		discard;
	}

	float viewDepth = ConvertToLinearDepth(gbData.depth, gPerspectiveValues.w, gPerspectiveValues.z);

	float3 PosW = CalcWorldPos(pin.csPos, viewDepth);

	float3 toEyeW = normalize(gEyePos - PosW);

	float4 diffuse = float4(0.0f, 0.0f, 0.0f, 1.0f), ambient = float4(0.0f, 0.0f, 0.0f, 1.0f), specular = float4(0.0f, 0.0f, 0.0f, 1.0f);


	for (int i = 0; i < gNumSpotLights; ++i)
	{

		cLight_SpotLight spotlight = gSpotLights[i];

		//Calculate Shadow Factor
		float ShadowFactor = gIShadowManager.CalcShadowFactor(spotlight
		, pin.Tex
		, spotlight.CalcShadowPos(PosW)
		, tShadowMapArray);

		float3 D, A, S;

		spotlight.Lit(D, A, S, gbData.NormalW, PosW, toEyeW, gbData.SpecPow);

		diffuse.xyz += D * ShadowFactor;
		ambient.xyz += A;
		specular.xyz += S * ShadowFactor;

		/*float3 lightVector = spotlight.Position - PosW;

		float distance = length(lightVector);

		if (distance < spotlight.Range)
		{

		lightVector /= distance;

		diffuse += spotlight.IlluminateDiffuse(lightVector, distance, gbData.NormalW) * ShadowFactor;
		ambient += spotlight.IlluminateAmbient();
		specular += spotlight.IlluminateSpecular(toEyeW, lightVector, gbData.NormalW, gbData.SpecPow) * ShadowFactor;

		}*/

		//[branch]
		//if (spotLight.IsCastShadow())
		//{
		//    float4 shadowPos = spotLight.CalcShadowPos(PosW);
		//    ShadowFactor = gIShadow_Base.CalcShadowFactor(shadowPos, spotLight.GetShadowMapID());
		//}

}

for (int i = 0; i < gNumDirectLights; ++i)
{

	cLight_DirectLight directlight = gDirectLights[i];

	//Calculate Shadow Factor
	float ShadowFactor = gIShadowManager.CalcShadowFactor(directlight
	, pin.Tex
	, directlight.CalcShadowPos(PosW)
	, tShadowMapArray);

	float3 D, A, S;

	directlight.Lit(D, A, S, gbData.NormalW, PosW, toEyeW, gbData.SpecPow);

	diffuse.xyz += D * ShadowFactor;
	ambient.xyz += A;
	specular.xyz += S * ShadowFactor;

	//diffuse += directlight.IlluminateDiffuse(float3(0.0f, 0.0f, 0.0f), 0.0f, float3(0.0f, 0.0f, 0.0f)) * ShadowFactor;
	//ambient += directlight.IlluminateAmbient();
	//specular += directlight.IlluminateSpecular(toEyeW) * ShadowFactor;

	//[branch]
	//if (spotLight.IsCastShadow())
	//{
	//    float4 shadowPos = spotLight.CalcShadowPos(PosW);
	//    ShadowFactor = gIShadow_Base.CalcShadowFactor(shadowPos, spotLight.GetShadowMapID());
	//}

}

float ssao = gISSAOManager.GetSSAO(pin.Tex, tSSAOMap);

//return float4(((float4(gbData.Color, 1.0) * (diffuse + ambient)) + specular).rgb, 1.0f);
//return float4(gbData.Color, 1.0f) * (diffuse + (ambient * ssao));

return gIOutputManager.GenerateOutput(gbData.Color, gbData.SpecIntensity, diffuse.xyz, ambient.xyz* ssao.xxx, specular.xyz);
}