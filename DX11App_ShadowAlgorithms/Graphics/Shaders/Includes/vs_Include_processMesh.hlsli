#if defined(__INTELLISENSE__)

#include "../../ShaderSlots_DeferredShading.h"


#define VERTEX_TYPE_SKINNED

#else

#include "Graphics/ShaderSlots_DeferredShading.h"

#endif

//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------

#ifdef VERTEX_TYPE_SKINNED

struct vertexIn
{
    float3 PosL             : POSITION;
    float3 NormalL          : NORMAL;
    float2 Tex              : TEXCOORD;
    float3 TangentU         : TANGENT;
    float3 BoneWeights      : BONEWEIGHT;
    int4 BoneIndex          : BONEINDEX;
};

cbuffer cb_boneTransforms : register(HLSL_SHADER_SLOTS_VS_CB_BONETRANSFORMS)
{
    row_major matrix gBoneTransformations[96];
};

#endif

#ifdef VERTEX_TYPE_BASIC32

struct vertexIn
{
    float3 PosL         : POSITION;
    float3 NormalL      : NORMAL;
    float2 Tex          : TEXCOORD;
};

#endif

struct vertexOut
{
    float4 PosH : SV_POSITION;
    float3 NormalW : NORMAL;
    float2 Tex : TEXCOORD0;
};



//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------




//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

