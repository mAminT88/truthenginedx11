#ifndef HLSL_INCLUDE_SAVSM
#define HLSL_INCLUDE_SAVSM

#if defined(__INTELLISENSE__)
    #include "Include_CommonConstantBuffer.hlsli"
    #include "Include_Samplers.hlsli"
    #include "Include_HelperFunc.hlsli"
    #include "ps_Include_GenerateVarianceShadowMap.hlsli"
#else
#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"
#include "Graphics/Shaders/Includes/ps_Include_GenerateVarianceShadowMap.hlsli"
#endif


//--------------------------------------------------------------------------------------
// Functions
//--------------------------------------------------------------------------------------


// Computes Chebyshev's Inequality
// Returns an upper bound given the first two moments and mean
float ChebyshevUpperBound( float2 Moments, float Mean, float MinVariance )
{
    // Standard shadow map comparison
    float p = (Mean <= Moments.x);
    
    // Compute variance
    float Variance = Moments.y - (Moments.x * Moments.x);
    Variance = max(Variance, MinVariance);
    
    // Compute probabilistic upper bound
    float d = Mean - Moments.x;
    float p_max = Variance / (Variance + d * d);
    
    return max(p, p_max);
}

/***********************Point Sampling**********************************/



// Point sample using two corners in the SAT
// Coords.xy is upper left, Coords.zw is lower right.
// Inclusive on upper left, not on lower right.
// Returns the sum (not average) - mostly an internal function
float4 SampleSATSum( Texture2DArray TextureArray, float TexArrayIndex, float4 Coords, int2 CoordsOffset )
{
    // Sample four SAT corners
    float4 nn = TextureArray.SampleLevel(gSamplerLinear, float3(Coords.xy, TexArrayIndex), 0, CoordsOffset);
    float4 np = TextureArray.SampleLevel(gSamplerLinear, float3(Coords.xw, TexArrayIndex), 0, CoordsOffset);
    float4 pn = TextureArray.SampleLevel(gSamplerLinear, float3(Coords.zy, TexArrayIndex), 0, CoordsOffset);
    float4 pp = TextureArray.SampleLevel(gSamplerLinear, float3(Coords.zw, TexArrayIndex), 0, CoordsOffset);
    
    // Clamping
    // Disable this for now... only really necessary when we want exact reproduction
    // around the x|y == 0 lines. Otherwise just wastes some time.
    /*
    bool4 InBounds = (Coords >= 0);
    bool4 Mask = InBounds.xxzz && InBounds.ywyw;
    nn *= Mask.x;
    np *= Mask.y;
    pn *= Mask.z;
    pp *= Mask.w;
    */
    
    // Return the sum of the area within
    return (pp - pn - np + nn);
}


// As above, but average instead of sum
float4 SampleSAT( Texture2DArray TextureArray, float TexArrayIndex, float4 Coords, float2 TexSize )
{
    // Work out normalized coordinates and area
    float4 RealCoords = (Coords - gShadowMapDXY.xyxy);
    float2 Dims = (Coords.zw - Coords.xy) * TexSize;
    
    // Sample sum and divide to get average
    return SampleSATSum(TextureArray, TexArrayIndex, RealCoords, int2(0, 0)) / (Dims.x * Dims.y);
}

// Sample and return a single fragment
float2 SampleMomentsPoint( Texture2DArray TextureArray, float TexArrayIndex, float4 Coords )
{
    float4 Value = SampleSAT(TextureArray, TexArrayIndex, Coords, gShadowMapSize);
    
    float2 V;
    V = Value.xy;

    // Unbias
    //V += GetMomentFPBias();

    return V;
}


// Sample and apply Chebyshev's inequality
float PointChebyshev( Texture2DArray TextureArray, float TexArrayIndex, float4 Coords, float Distance, float MinVariance )
{
    float2 Moments = SampleMomentsPoint(TextureArray, TexArrayIndex, Coords);
    return ChebyshevUpperBound(Moments, Distance, MinVariance);
}

/***********************Bilinear Sampling**********************************/





void SampleSATSumBilinear( Texture2DArray TextureArray, float TexArrayIndex, float4 Coords, float2 TexSize,
                          out float4 X, out float4 Y, out float4 Z, out float4 W )
{
    float4 RealCoords = (Coords - gShadowMapDXY.xyxy);
       
    // Sample the four rectangles
    X = SampleSATSum(TextureArray, TexArrayIndex, RealCoords, int2(1, 0));
    Y = SampleSATSum(TextureArray, TexArrayIndex, RealCoords, int2(0, 1));
    Z = SampleSATSum(TextureArray, TexArrayIndex, RealCoords, int2(1, 1));
    W = SampleSATSum(TextureArray, TexArrayIndex, RealCoords, int2(0, 0));
}


// As above, but average instead of sum
void SampleSATBilinear( Texture2DArray TextureArray, float TexArrayIndex, float4 Coords, float2 TexSize,
                       out float4 X, out float4 Y, out float4 Z, out float4 W )
{
    SampleSATSumBilinear(TextureArray, TexArrayIndex, Coords, TexSize, X, Y, Z, W);
    
    // Average
    float2 Dims = (Coords.zw - Coords.xy) * TexSize;
    float InvArea = 1 / float(Dims.x * Dims.y);
    X *= InvArea;
    Y *= InvArea;
    Z *= InvArea;
    W *= InvArea;
}


// Returns into the X, Y, Z, W coordinates the four rectangle averages
void SampleMomentsBilinear( Texture2DArray TextureArray, float TexArrayIndex, float4 Coords,
                           out float2 X, out float2 Y, out float2 Z, out float2 W )
{
    float4 X1, Y1, Z1, W1;
    SampleSATBilinear(TextureArray, TexArrayIndex, Coords, gShadowMapSize, X1, Y1, Z1, W1);
    
    X = X1.xy;
    Y = Y1.xy;
    Z = Z1.xy;
    W = W1.xy;
    
    // Unbias
    float2 FPBias = GetMomentFPBias();
    X += FPBias;
    Y += FPBias;
    Z += FPBias;
    W += FPBias;
}


// Sample a bilinear tile and compute a Chebyshev upper bound
float BilinearChebyshev( Texture2DArray TextureArray, float TexArrayIndex, float4 Coords, float4 BilWeights, float Distance,
                        float MinVariance )
{
    float2 X, Y, Z, W;
    SampleMomentsBilinear(TextureArray, TexArrayIndex, Coords, X, Y, Z, W);
    
    // Manual bilinear interpolation
    float2 I;
    I.x = dot(BilWeights, float4(X.x, Y.x, Z.x, W.x));
    I.y = dot(BilWeights, float4(X.y, Y.y, Z.y, W.y));
    
    return ChebyshevUpperBound(I, Distance, MinVariance);
        
    /*
    // Compute an upper bound for all four samples
    // This could be vectorized a bit more with rotated data, but again it would
    // be a waste on the G80, and barely a benefit on other architectures.
    float4 Factors;
    Factors.x = ChebyshevUpperBound(X, Distance, MinVariance);
    Factors.y = ChebyshevUpperBound(Y, Distance, MinVariance);
    Factors.z = ChebyshevUpperBound(Z, Distance, MinVariance);
    Factors.w = ChebyshevUpperBound(W, Distance, MinVariance);
    
    // Combine results using bilinear weights
    return dot(BilWeights, Factors);
    */
}

interface ISAVSM_Manager
{
    float CalcShadowFactor( Texture2DArray SATextureArray, int TexArrayIndex, float2 CoordsUL, float2 Size, float Distance, float MinVariance );
};

ISAVSM_Manager gISAVSM_Manager;



class CSAVSM_Manager_Bilinear : ISAVSM_Manager
{
    float CalcShadowFactor( Texture2DArray SATextureArray, int TexArrayIndex, float2 CoordsUL, float2 Size, float Distance, float MinVariance )
    {
        // Compute bilinear weights and coordinates
        float4 BilWeights;
        float2 BilCoordsUL = GetBilCoordsAndWeights(CoordsUL, gShadowMapSize, gShadowMapDXY, BilWeights);
        float4 Tile = BilCoordsUL.xyxy + float4(0, 0, Size.xy);

        float ShadowFactor = BilinearChebyshev(SATextureArray, TexArrayIndex, Tile, BilWeights, Distance, MinVariance);

        return ShadowFactor;
    }
};

class CSAVSM_Manager_Linear : ISAVSM_Manager
{
    float CalcShadowFactor( Texture2DArray SATextureArray, int TexArrayIndex, float2 CoordsUL, float2 Size, float Distance, float MinVariance )
    {
        // Use this instead if hardware bilinear is enabled for the SAT
        // We currently don't do this since it causes more precision problems...
        float4 Tile = CoordsUL.xyxy + float4(0, 0, Size.xy);
        float ShadowContrib = PointChebyshev(SATextureArray, TexArrayIndex, Tile, Distance, MinVariance);

        return ShadowContrib;
    }
};



float CalcSAVSMShadowFactor( Texture2DArray SATexArray
, float TexArrayIndex
, float2 tc
, float2 tc_dx
, float2 tc_dy
, float distance
, float MinFilterWidthInTexel
, float MaxFilterWidthInTexel
, float MinVariance )
{

    float2 TexelSize = gShadowMapDXY;

    // Compute the filter tile information
    // Don't clamp the filter area since we have constant-time filtering!
    float2 Size;
    float2 CoordsUL = GetFilterTile(tc, tc_dx, tc_dy, gShadowMapSize, gShadowMapDXY,
                                    MinFilterWidthInTexel, MaxFilterWidthInTexel, Size);

    
    float ShadowFactor = gISAVSM_Manager.CalcShadowFactor(SATexArray
    , TexArrayIndex
    , CoordsUL
    , Size
    , distance
    , MinVariance);



    return ShadowFactor;
}



#endif