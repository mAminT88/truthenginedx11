#if defined(__INTELLISENSE__)
	#include "../Includes/Include_Samplers.hlsli"
	#include "../Includes/Include_CommonConstantBuffer.hlsli"
#else
    #include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
	#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#endif


#if defined(__INTELLISENSE__)
#define SAMPLE_NUMBER 16
#endif

//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------

Texture2DArray gShadowMapArray : register(t0);
Texture2DArray<uint4> gShadowMapArray_INT : register(t1);



//--------------------------------------------------------------------------------------
// Constant Buffer
//--------------------------------------------------------------------------------------

cbuffer CB_PS_GENERATESAT : register(b6)
{
    float gIteration;
    float gShadowMapID;
    float2 pad_generateSAT0;
};

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface ISAT
{
    float4 CalcSAT( float2 tex, float2 texelSize );
    uint4 CalcSAT_INT( int2 texel, float2 texelSize );
};

//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CSAT_Horizontal : ISAT
{
    float4 CalcSAT( float2 tex, float2 texelSize )
    {
        float4 value = gShadowMapArray.SampleLevel(gSamplerPoint_BorderColor_0, float3(tex, gShadowMapID), 0.0f);
        float2 step = float2(pow(SAMPLE_NUMBER, gIteration) * texelSize.x, 0.0f);

        [unroll]
        for (int i = 1; i < SAMPLE_NUMBER; ++i)
        {

            float2 offset = step * i;

            value += gShadowMapArray.SampleLevel(gSamplerPoint_BorderColor_0, float3(tex - offset, gShadowMapID), 0.0f);

        }

        return value;

    }

   

    uint4 CalcSAT_INT( int2 texel, float2 texelSize )
    {

        uint4 value = gShadowMapArray_INT.Load(int4(texel, (int) gShadowMapID, 0));
        int2 step = int2(pow(SAMPLE_NUMBER, gIteration), 0.0f);

        [unroll]
        for (int i = 1; i < SAMPLE_NUMBER; ++i)
        {

            int2 offset = step * i;

            value += gShadowMapArray_INT.Load(int4(texel - offset, (int) gShadowMapID, 0) );

        }

        return value;
    }

};

class CSAT_Horizontal_0_UINT : ISAT
{
    float4 CalcSAT( float2 tex, float2 texelSize )
    {
        return float4(0.0f, 0.0f, 0.0f, 0.0f);

    }

    uint4 CalcSAT_INT( int2 texel, float2 texelSize )
    {
        float2 tex = float2(texel * texelSize);
        uint4 value = uint4(gShadowMapArray.SampleLevel(gSamplerPoint_BorderColor_0, float3(tex, gShadowMapID), 0.0f) * gFixedPrecision_Float32ToInt32.xxxx);
        //float2 step = float2(pow(SAMPLE_NUMBER, gIteration) * texelSize.x, 0.0f);
        float2 step = float2(texelSize.x, 0.0f);

        [unroll]
        for (int i = 1; i < SAMPLE_NUMBER; ++i)
        {

            float2 offset = step * i;

            value += uint4(gShadowMapArray.SampleLevel(gSamplerPoint_BorderColor_0, float3(tex - offset, gShadowMapID), 0.0f) * gFixedPrecision_Float32ToInt32.xxxx);

        }

        return value;
    }
};


class CSAT_Vertical : ISAT
{
    float4 CalcSAT( float2 tex, float2 texelSize )
    {
        float4 value = gShadowMapArray.SampleLevel(gSamplerPoint_BorderColor_0, float3(tex, gShadowMapID), 0.0f);
        float2 step = float2(0.0f, pow(SAMPLE_NUMBER, gIteration) * texelSize.y);

        [unroll]
        for (int i = 1; i < SAMPLE_NUMBER; ++i)
        {
            float2 offset = step * i;

            value += gShadowMapArray.SampleLevel(gSamplerPoint_BorderColor_0, float3(tex - offset, gShadowMapID), 0.0f);

        }

        return value;

    }

    uint4 CalcSAT_INT( int2 texel, float2 texelSize )
    {
        uint4 value = gShadowMapArray_INT.Load(int4(texel, (int) gShadowMapID, 0));
        int2 step = int2(0 , pow(SAMPLE_NUMBER, gIteration));

        [unroll]
        for (int i = 1; i < SAMPLE_NUMBER; ++i)
        {

            int2 offset = step * i;

            value += gShadowMapArray_INT.Load(int4(texel - offset, (int) gShadowMapID, 0));

        }

        return value;

    }

};

