#ifndef HLSL_MSM
#define HLSL_MSM

#if defined(__INTELLISENSE__)
	#include "../Includes/Include_Samplers.hlsli"
	#include "../../ShaderSlots_DeferredShading.h"
#else
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#include "Graphics/ShaderSlots_DeferredShading.h"
#endif


cbuffer cb_msm_parameters : register(HLSL_SHADER_SLOTS_PS_CB_MSM_PARAMETERS)
{
    float gMomentBias;
    float gMaxDepthBias;
    float2 cb_msm_parameters_pad0;

    row_major float3x2 gKernelSizeParameters;
    //float cb_msm_parameters_pad1 : packoffset(c2.w);

    row_major float3x2 gLightParameter;
    //float cb_msm_parameters_pad2 : packoffset(c5.z);

}


static const float3x2 KernelSizeParameters = { 0.00049f, 0.00049f, 0.0127f, 0.0127f , 78.76923f , 0.10f };

static const float3x2 LightParameter = { 0.51126f, 0.51075, 0.0f, 0.0f, 1.0f, 1.0f };

void GetBlockerSearchAreaSize( out float2 searchRadius, float lightSize, float linearDepth, float zNear )
{
	searchRadius = lightSize * (linearDepth - zNear) / linearDepth;
}

void GetBlockerSearchRectangle( out float2 leftTop, out float2 rightBottom, float2 center, float2 texelSize )
{
	//leftTop = saturate(center - searchRadius - 0.5f * texelSize);
	//rightBottom = saturate(center + searchRadius + 0.5f * texelSize);

	leftTop = saturate(center - gKernelSizeParameters._21_22 - 0.5f * texelSize);
	rightBottom = saturate(center + gKernelSizeParameters._21_22 + 0.5f * texelSize);
}

/*! Like ComputeRectangleAverage_uint4() but without interpolation. The result 
   refers to a modified version of the given rectangle where corner points have been 
   rounded to the pixel grid. This is substantially faster.
  \sa ComputeFixedPrecision() */
void ComputeIntegerRectangleAverage_uint4( out float4 OutAverageValue, float2 LeftTop, float2 RightBottom, Texture2DArray<uint4> SATTexture, int TexArrayIndex, float2 TextureSize, float2 FixedPrecision )
{
	int2 iLeftTop = int2(round(LeftTop * TextureSize.xy)) - 1;
	int2 iRightBottom = int2(round(RightBottom * TextureSize.xy)) - 1;

	uint4 sample0 = SATTexture.Load(int4(iLeftTop.x, iLeftTop.y, TexArrayIndex, 0));
	uint4 sample1 = SATTexture.Load(int4(iLeftTop.x, iRightBottom.y, TexArrayIndex, 0));
	uint4 sample2 = SATTexture.Load(int4(iRightBottom.x, iLeftTop.y, TexArrayIndex, 0));
	uint4 sample3 = SATTexture.Load(int4(iRightBottom.x, iRightBottom.y, TexArrayIndex, 0));

	uint4 Sample[2][2] =
	{
		{
			SATTexture.Load(int4(iLeftTop.x, iLeftTop.y, TexArrayIndex, 0)),
		 SATTexture.Load(int4(iLeftTop.x, iRightBottom.y, TexArrayIndex, 0))
		},
		{
			SATTexture.Load(int4(iRightBottom.x, iLeftTop.y, TexArrayIndex, 0)),
		 SATTexture.Load(int4(iRightBottom.x, iRightBottom.y, TexArrayIndex, 0))
		}
	};

	uint4 Integral = sample0 + sample3 - sample1 - sample2;
	uint2 Size = iRightBottom - iLeftTop;
	OutAverageValue = float4(Integral) * FixedPrecision.y / float(Size.x * Size.y);
}

/*! This function converts a vector of four moments from a representation that is 
   optimized for quantization to the canonical representation.*/
void Convert4MomentOptimizedToCanonical( out float4 OutBiased4Moments, float4 OptimizedMoments0 /*, float MomentBias 6.0e-7f*/ )
{
	OutBiased4Moments.xz = mul(OptimizedMoments0.xz - 0.5f, float2x2(-1.0f / 3.0f, -0.75f, sqrt(3.0f), 0.75f * sqrt(3.0f)));
	OutBiased4Moments.yw = mul(OptimizedMoments0.yw, float2x2(0.125f, -0.125f, 1.0f, 1.0f));
	OutBiased4Moments = lerp(OutBiased4Moments, float4(0.0f, 0.628f, 0.0f, 0.628f), gMomentBias);
}


/*! Solves the polynomial Coefficients[0]+Coefficients[1]*x+Coefficients[2]*x^2 for 
   x and returns a vector consisting of the two solutions. The returned solutions 
   are sorted. Behavior is undefined if the polynomial has no roots. A root with 
   multiplicity is returned twice.*/
float2 GetRoots( float3 Coefficients )
{
	float Scaling = 1.0f / Coefficients[2];
	float p = Coefficients[1] * Scaling;
	float q = Coefficients[0] * Scaling;
	float D = p * p * 0.25f - q;
	float r = sqrt(D);
	return float2(-0.5f * p - r, -0.5f * p + r);
}


/*! This function estimates the average depth of fragments in a filter region which 
   are less than FragmentDepth-DepthBias and outputs it. This is done using four 
   moments of the shadow map depth within this filter region.
  \param OutBlockerSearchShadowIntensity See ComputePCSSAverageBlockerDepth().*/
void Compute4MomentAverageBlockerDepth( out float OutAverageBlockerDepth, out float OutBlockerSearchShadowIntensity, float4 BlockerSearchBiased4Moments, float FragmentDepth )
{
	// Use short-hands for the many formulae to come
	float4 b = BlockerSearchBiased4Moments;
	float3 z;
	z[0] = FragmentDepth;

	// Compute a Cholesky factorization of the Hankel matrix B storing only non-
	// trivial entries or related products
	float L21D11 = mad(-b[0], b[1], b[2]);
	float D11 = mad(-b[0], b[0], b[1]);
	float SquaredDepthVariance = mad(-b[1], b[1], b[3]);
	float D22D11 = dot(float2(SquaredDepthVariance, -L21D11), float2(D11, L21D11));
	float InvD11 = 1.0f / D11;
	float L21 = L21D11 * InvD11;

	// Obtain a scaled inverse image of bz=(1,z[0],z[0]*z[0])^T
	float3 c = float3(1.0f, z[0], z[0] * z[0]);
	// Forward substitution to solve L*c1=bz
	c[1] -= b.x;
	c[2] -= b.y + L21 * c[1];
	// Scaling to solve D*c2=c1
	c[1] *= InvD11;
	c[2] *= D11 / D22D11;
	// Backward substitution to solve L^T*c3=c2
	c[1] -= L21 * c[2];
	c[0] -= dot(c.yz, b.xy);
	// Solve the quadratic equation c[0]+c[1]*z+c[2]*z^2 to obtain solutions 
	// z[1] and z[2]
	z.yz = GetRoots(c);
	// Compute weights of the Dirac-deltas at the roots
	float3 Weight;
	Weight[0] = (z[1] * z[2] - b[0] * (z[1] + z[2]) + b[1]) / ((z[0] - z[1]) * (z[0] - z[2]));
	Weight[1] = (z[0] * z[2] - b[0] * (z[0] + z[2]) + b[1]) / ((z[2] - z[1]) * (z[0] - z[1]));
	Weight[2] = 1.0f - Weight[0] - Weight[1];
	// Compute the shadow intensity and the unnormalized average depth of occluders
	float AverageBlockerDepthIntegral = ((z[1] < z[0]) ? (Weight[1] * z[1]) : 0.0f) + ((z[2] < z[0]) ? (Weight[2] * z[2]) : 0.0f);
	float BlockerSearchShadowIntensity = ((z[1] < z[0]) ? Weight[1] : 0.0f) + ((z[2] < z[0]) ? Weight[2] : 0.0f);
	const float FullyLitBias = 1.0e-3f;
	OutAverageBlockerDepth = clamp((FullyLitBias * z[0] + AverageBlockerDepthIntegral) / (FullyLitBias + BlockerSearchShadowIntensity), -1.0f, 1.0f);
	OutBlockerSearchShadowIntensity = (BlockerSearchShadowIntensity > 0.99f) ? 1.0f : (-1.0f);
}

/*! This function takes a sample from the four-moment optimized shadow map in the 
   given sampler using the given texture coordinate converts to biased moments and 
   outputs it.*/
float4 Sample4MomentOptimizedShadowMap( Texture2DArray MSMTextureArray, float3 ShadowMapTexCoord/*, float MomentBias = 6.0e-5f*/ )
{
	float4 OptimizedSample = MSMTextureArray.Sample(gSamplerPoint_BorderColor_1, ShadowMapTexCoord);

	OptimizedSample.xz = mul(OptimizedSample.xz - 0.5f, float2x2(-1.0f / 3.0f, -0.75f, sqrt(3.0f), 0.75f * sqrt(3.0f)));

	OptimizedSample.yw = mul(OptimizedSample.yw, float2x2(0.125f, -0.125f, 1.0f, 1.0f));

	OptimizedSample = lerp(OptimizedSample, float4(0.0f, 0.628f, 0.0f, 0.628f), gMomentBias);

	return OptimizedSample;
}

/*! Computes LightParameter as expected by EstimatePenumbraSize() for a directional 
   light.
  \param OutLightParameter The light parameters as expected by 
		 EstimatePenumbraSize().
  \param ViewToProjectionSpace The projection matrix used for the shadow map of the 
		 directional light.
  \param LightSourceAngle The angle spanned by the directional light across the 
		 hemisphere along the two directions corresponding to rows and columns in 
		 the shadow map.*/
void ComputeDirectionalLightSoftShadowParameters( out float3x2 OutLightParameter, float4x4 ViewToProjectionSpace, float2 LightSourceAngle )
{
	// Compute the bounding box dimensions of the view frustum of the shadow map
	float3 FrustumExtents = float3(
		2.0f * (ViewToProjectionSpace._11 * ViewToProjectionSpace._44 - ViewToProjectionSpace._14 * ViewToProjectionSpace._41) /
			 (ViewToProjectionSpace._11 * ViewToProjectionSpace._11 - ViewToProjectionSpace._41 * ViewToProjectionSpace._41),
		2.0f * (ViewToProjectionSpace._22 * ViewToProjectionSpace._44 - ViewToProjectionSpace._24 * ViewToProjectionSpace._42) /
			 (ViewToProjectionSpace._22 * ViewToProjectionSpace._22 - ViewToProjectionSpace._42 * ViewToProjectionSpace._42),
		dot(float4(ViewToProjectionSpace._43, -ViewToProjectionSpace._43, -ViewToProjectionSpace._33, ViewToProjectionSpace._33), float4(ViewToProjectionSpace._33, ViewToProjectionSpace._43, ViewToProjectionSpace._34, ViewToProjectionSpace._44)) /
			(ViewToProjectionSpace._33 * (ViewToProjectionSpace._33 - ViewToProjectionSpace._43))
	);
	// Compute a factor that turns a depth difference into a kernel size as texture 
	// coordinate
	OutLightParameter._11 = 0.5f * tan(0.5f * LightSourceAngle.x) * FrustumExtents.z / FrustumExtents.x;
	OutLightParameter._12 = 0.5f * tan(0.5f * LightSourceAngle.y) * FrustumExtents.z / FrustumExtents.y;
	// The denominator is constant one
	OutLightParameter._21_22 = float2(0.0f, 0.0f);
	OutLightParameter._31_32 = float2(1.0f, 1.0f);
}

/*! This function computes the adequate filter size for generating a contact 
   hardening shadow. It also adapts the depth bias to the filter size.
  \param OutKernelSize The size of the kernel as texture coordinate offset from its 
		 center to the right bottom.
  \param OutDepthBias The depth bias to be used when computing filtered shadows over 
		 the returned kernel. For a maximal kernel size it is MaxDepthBias and it is 
		 proportional to OutKernelSize but may be clamped below.
  \param OccluderDepth The (estimated) depth of the occluding geometry in shadow map 
		 coordintes.
  \param FragmentDepth The depth of the receiving fragment in shadow map 
		 coordinates.
  \param LightParameter Parameters defining the computation of the kernel size 
		 which depend solely on the light source and the shadow map projection. The 
		 first column relates to the horizontal kernel size, the second one to the 
		 vertical kernel size. Row 0 holds the factor to be multiplied onto the 
		 distance between occluder and receiver. Row 1 and 2 hold the gradient and 
		 intercept of the linear function in the denominator which depends on the 
		 occluder depth, respectively.
  \param KernelSizeParameter The first row provides the upper top texture coordinate 
		 offst from the center of the smallest possible filter kernel. The second 
		 row provides the same for the biggest possible kernel. The third row 
		 provides the reciprocal of the horizontal kernel offset (in texture 
		 coordinates) at which the depth bias becomes maximal and the minimal depth 
		 bias as multiple of MaxDepthBias.
  \param MaxDepthBias The depth bias that is to be used for a kernel of maximal 
		 size.*/
void EstimatePenumbraSize( out float2 OutKernelSize, out float OutDepthBias, float OccluderDepth, float FragmentDepth, float lightSize /*, float MaxDepthBias = 0.02f*/ )
{
	//float2 Numerator = lightSize * (FragmentDepth - OccluderDepth);
	//float2 Denominator = OccluderDepth;
	//OutKernelSize = abs(Numerator / Denominator);
	//OutDepthBias = gMaxDepthBias * clamp(OutKernelSize.x, 0.0f, 1.0f);

    float2 Numerator = /*gLightParameter._11_12*/ lightSize * (FragmentDepth - OccluderDepth);
    float2 Denominator = gLightParameter._21_22 * OccluderDepth + gLightParameter._31_32;
    OutKernelSize = max(gKernelSizeParameters._11_12, min(gKernelSizeParameters._21_22, Numerator / Denominator));
    OutDepthBias = gMaxDepthBias * clamp(OutKernelSize.x * gKernelSizeParameters._31, gKernelSizeParameters._32, 1.0f);
}

/*! This function computes the left top and right bottom of the rectangle over which 
   the shadow map should be filtered to get contact-hardening shadows.
  \param OutFilterRegionLeftTop The left top of the filter region as texture 
		 coordinate.
  \param OutFilterRegionRightBottom The right bottom of the filter region as texture 
		 coordinate.
  \param FilterRegionCenter The center of the filter region as texture coordinate.
  \param KernelSize See OutKernelSize in EstimatePenumbraSize().*/
void GetShadowFilterRectangle( out float2 OutFilterRegionLeftTop, out float2 OutFilterRegionRightBottom, float2 FilterRegionCenter, float2 KernelSize )
{
	OutFilterRegionLeftTop = saturate(FilterRegionCenter - KernelSize);
	OutFilterRegionRightBottom = saturate(FilterRegionCenter + KernelSize);
}


/*! This function computes the average value (component-wise) of a texture with 
   values from zero to one within a rectangle.
  \param OutAverageValue The average value as values in the range from zero to one.
  \param LeftTop,RightBottom The minimal and maximal texture coordinates which are 
		 still contained in the rectangle. Restrictions to the size of this 
		 rectangle may apply.
  \param SummedAreaTableSampler A texture and sampler for a summed area table which 
		 provides prefix sums of the texture of interest.
  \param TextureSize The width and height of the texture in x and y, reciprocals in 
		 z,w.
  \sa ComputeFixedPrecision() */
void ComputeRectangleAverage_uint4( out float4 OutAverageValue, float2 LeftTop, float2 RightBottom, Texture2DArray<uint4> SATTexture, int TexArrayIndex, float2 TextureSize, float2 TexelSize, float2 FixedPrecision )
{
	// The summed area table is essentially off by one because the top left texel 
	// already holds the integral over the top left texel. Compensate for that.
	LeftTop -= TexelSize;
	RightBottom -= TexelSize;
	// Round the texture coordinates to integer texels
	float2 LeftTopPixel = LeftTop * TextureSize;
	float2 RightBottomPixel = RightBottom * TextureSize;
	float2 LeftTopPixelFloor = floor(LeftTopPixel);
	float2 RightBottomPixelFloor = floor(RightBottomPixel);
	int2 iLeftTop = int2(LeftTopPixelFloor);
	int2 iRightBottom = int2(RightBottomPixelFloor);
	float2 LeftTopFactor = float2(1.0f, 1.0f) - (LeftTopPixel - LeftTopPixelFloor);
	float2 RightBottomFactor = RightBottomPixel - RightBottomPixelFloor;
	// Sample the summed area table at all relevant locations. The first two indices 
	// determine whether we are at the left top or right bottom of the rectangle, 
	// the latter two determine the pixel offset.
	uint4 Samples[2][2][2][2];
	[unroll]
	for (int x = 0; x != 2; ++x)
	{
		int TexelX = (x == 0) ? iLeftTop.x : iRightBottom.x;
		[unroll]
		for (int y = 0; y != 2; ++y)
		{
			int TexelY = (y == 0) ? iLeftTop.y : iRightBottom.y;
			[unroll]
			for (int z = 0; z != 2; ++z)
			{
				[unroll]
				for (int w = 0; w != 2; ++w)
				{
					Samples[x][y][z][w] = SATTexture.Load(int4(TexelX + z, TexelY + w, TexArrayIndex, 0));
				}
			}
		}
	}
	// Compute integrals for various rectangles
	float4 pCornerIntegral[2][2];
	[unroll]
	for (int x = 0; x != 2; ++x)
	{
		[unroll]
		for (int y = 0; y != 2; ++y)
		{
			pCornerIntegral[x][y] = float4(Samples[x][y][0][0] + Samples[x][y][1][1] - Samples[x][y][1][0] - Samples[x][y][0][1]);
		}
	}
	float4 pEdgeIntegral[4] =
	{
		// Right edge
		float4(Samples[1][0][0][1] + Samples[1][1][1][0] - Samples[1][0][1][1] - Samples[1][1][0][0]),
		// Top edge
		float4(Samples[0][0][1][0] + Samples[1][0][0][1] - Samples[0][0][1][1] - Samples[1][0][0][0]),
		// Left edge
		float4(Samples[0][0][0][1] + Samples[0][1][1][0] - Samples[0][0][1][1] - Samples[0][1][0][0]),
		// Bottom edge
		float4(Samples[0][1][1][0] + Samples[1][1][0][1] - Samples[0][1][1][1] - Samples[1][1][0][0])
	};
	float4 CenterIntegral = float4(Samples[0][0][1][1] + Samples[1][1][0][0] - Samples[0][1][1][0] - Samples[1][0][0][1]);
	// Compute the integral over the given rectangle
	float4 Integral = CenterIntegral;
	Integral += pCornerIntegral[0][0] * (LeftTopFactor.x * LeftTopFactor.y);
	Integral += pCornerIntegral[0][1] * (LeftTopFactor.x * RightBottomFactor.y);
	Integral += pCornerIntegral[1][0] * (RightBottomFactor.x * LeftTopFactor.y);
	Integral += pCornerIntegral[1][1] * (RightBottomFactor.x * RightBottomFactor.y);
	Integral += pEdgeIntegral[0] * RightBottomFactor.x;
	Integral += pEdgeIntegral[1] * LeftTopFactor.y;
	Integral += pEdgeIntegral[2] * LeftTopFactor.x;
	Integral += pEdgeIntegral[3] * RightBottomFactor.y;
	// Get from a non-normalized integral to moments
	float2 Size = RightBottomPixel - LeftTopPixel;
	OutAverageValue = Integral * FixedPrecision.y / (Size.x * Size.y);
}

/*! This function takes in a shadow intensity computed by the filtering step of an 
   algorithm for contact-hardening shadows and another shadow intensity computed by 
   the blocker search. The shadow intensity computed by the blocker search can be 
   negative to imply that results are inconclusive. Only in this case the shadow 
   intensity computed during filtering will be used. Otherwise the shadow intensity 
   computed during the blocker search is output and the compiler can branch away the 
   entire filtering step.
  \warning This method is heavily reliant on compiler optimizations so the produced 
		   assembly should be checked to see whether branches have been created as 
		   intended.*/
void PickShadowIntensity( out float OutShadowIntensity, float FilteredShadowIntensity, float BlockerSearchShadowIntensity )
{
	[branch]
	if (BlockerSearchShadowIntensity >= 0.0f)
	{
		OutShadowIntensity = BlockerSearchShadowIntensity;
	}
	else
	{
		OutShadowIntensity = FilteredShadowIntensity;
	}
}


/*! This function scales the given shadow intensity by a factor greater than 1.0 and 
   clamps the result back into the valid range. In addition it sets the shadow 
   intensity to 0.0, if the given shadow map texture coordinate or depth are outside 
   the valid range.*/
void ScaleShadowIntensity( out float OutShadowIntensity, float ShadowIntensity, float2 ShadowMapTexCoord, float FragmentDepth, float ShadowIntensityFactor = 1.02041f )
{
	OutShadowIntensity = saturate(ShadowIntensity * ShadowIntensityFactor);
	bool OutOfRange = ShadowMapTexCoord.x < 0.0f || ShadowMapTexCoord.x > 1.0f ||
					ShadowMapTexCoord.y < 0.0f || ShadowMapTexCoord.y > 1.0f ||
					FragmentDepth < -1.0f || FragmentDepth > 1.0f;
	OutShadowIntensity = OutOfRange ? 0.0f : OutShadowIntensity;
}

/*! Given a sampled value from a four-moment shadow map and a computed shadow map 
   depth for a point at the same location this function outputs 1.0, if the fragment 
   is in shadow 0.0f, if the fragment is lit and an intermediate value for partial 
   shadow. The returned value is an optimal lower bound.*/
void Compute4MomentShadowIntensity( out float OutShadowIntensity, float4 Biased4Moments, float FragmentDepth, float DepthBias )
{

	// Use short-hands for the many formulae to come
	float4 b = Biased4Moments;
	float3 z;
	z[0] = FragmentDepth - DepthBias;

	// Compute a Cholesky factorization of the Hankel matrix B storing only non-
	// trivial entries or related products
	float L21D11 = mad(-b[0], b[1], b[2]);
	float D11 = mad(-b[0], b[0], b[1]);
	float SquaredDepthVariance = mad(-b[1], b[1], b[3]);
	float D22D11 = dot(float2(SquaredDepthVariance, -L21D11), float2(D11, L21D11));
	float InvD11 = 1.0f / D11;
	float L21 = L21D11 * InvD11;

	// Obtain a scaled inverse image of bz=(1,z[0],z[0]*z[0])^T
	float3 c = float3(1.0f, z[0], z[0] * z[0]);
	// Forward substitution to solve L*c1=bz
	c[1] -= b.x;
	c[2] -= b.y + L21 * c[1];
	// Scaling to solve D*c2=c1
	c[1] *= InvD11;
	c[2] *= D11 / D22D11;
	// Backward substitution to solve L^T*c3=c2
	c[1] -= L21 * c[2];
	c[0] -= dot(c.yz, b.xy);
	// Solve the quadratic equation c[0]+c[1]*z+c[2]*z^2 to obtain solutions z[1] 
	// and z[2]
	float InvC2 = 1.0f / c[2];
	float p = c[1] * InvC2;
	float q = c[0] * InvC2;
	float D = ((p * p) / 4.0f) - q;
	float r = sqrt(D);
	z[1] = -(p / 2.0f) - r;
	z[2] = -(p / 2.0f) + r;

	// Use a solution made of four deltas if the solution with three deltas is 
	// invalid
	[branch]
	if (z[1] < -1.0f || z[2] > 1.0f)
	{
		float zFree = ((b[0] - b[2]) * z[0] + b[3] - b[1]) / (z[0] + b[2] - b[0] - b[1] * z[0]);
		float w1Factor = (z[0] > zFree) ? 1.0f : 0.0f;
		// Construct a polynomial taking value zero at z[0] and 1, value 1 at -1 and 
		// value w1Factor at zFree. Start with a linear part and then multiply by 
		// linear factors to get the roots.
		float2 Normalizers;
		Normalizers.x = w1Factor / ((zFree - z[0]) * mad(zFree, zFree, -1.0f));
		Normalizers.y = 0.5f / ((zFree + 1.0f) * (z[0] + 1.0f));
		float4 Polynomial;
		Polynomial[0] = mad(zFree, Normalizers.y, Normalizers.x);
		Polynomial[1] = Normalizers.x - Normalizers.y;
		// Multiply the polynomial by (z-z[0])
		Polynomial[2] = Polynomial[1];
		Polynomial[1] = mad(Polynomial[1], -z[0], Polynomial[0]);
		Polynomial[0] *= -z[0];
		// Multiply the polynomial by (z-1)
		Polynomial[3] = Polynomial[2];
		Polynomial.yz = Polynomial.xy - Polynomial.yz;
		Polynomial[0] *= -1.0f;
		// The shadow intensity is the dot product of the coefficients of this 
		// polynomial and the power moments for the respective powers
		OutShadowIntensity = dot(Polynomial, float4(1.0f, b.xyz));
	}
	// Use the solution with three deltas
	else
	{
		float4 Switch =
			(z[2] < z[0]) ? float4(z[1], z[0], 1.0f, 1.0f) : (
			(z[1] < z[0]) ? float4(z[0], z[1], 0.0f, 1.0f) :
			float4(0.0f, 0.0f, 0.0f, 0.0f));
		float Quotient = (Switch[0] * z[2] - b[0] * (Switch[0] + z[2]) + b[1]) / ((z[2] - Switch[1]) * (z[0] - z[1]));
		OutShadowIntensity = Switch[2] + Switch[3] * Quotient;
	}
	OutShadowIntensity = saturate(OutShadowIntensity);
}


/*! Given a sampled value from a four-moment shadow map and a computed shadow map 
   depth for a point at the same location this function outputs 1.0, if the fragment 
   is in shadow 0.0f, if the fragment is lit and an intermediate value for partial 
   shadow. The returned value is an optimal lower bound except for the fact that it 
   does not exploit the knowledge that the original distribution has support in 
   [0,1].*/
void Compute4MomentUnboundedShadowIntensity( out float OutShadowIntensity,
	float4 Biased4Moments, float FragmentDepth, float DepthBias )
{
	// Use short-hands for the many formulae to come
	float4 b = Biased4Moments;
	float3 z;
	z[0] = FragmentDepth - DepthBias;

	// Compute a Cholesky factorization of the Hankel matrix B storing only non-
	// trivial entries or related products
	float L21D11 = mad(-b[0], b[1], b[2]);
	float D11 = mad(-b[0], b[0], b[1]);
	float SquaredDepthVariance = mad(-b[1], b[1], b[3]);
	float D22D11 = dot(float2(SquaredDepthVariance, -L21D11), float2(D11, L21D11));
	float InvD11 = 1.0f / D11;
	float L21 = L21D11 * InvD11;
	float D22 = D22D11 * InvD11;
	float InvD22 = 1.0f / D22;

	// Obtain a scaled inverse image of bz=(1,z[0],z[0]*z[0])^T
	float3 c = float3(1.0f, z[0], z[0] * z[0]);
	// Forward substitution to solve L*c1=bz
	c[1] -= b.x;
	c[2] -= b.y + L21 * c[1];
	// Scaling to solve D*c2=c1
	c[1] *= InvD11;
	c[2] *= InvD22;
	// Backward substitution to solve L^T*c3=c2
	c[1] -= L21 * c[2];
	c[0] -= dot(c.yz, b.xy);
	// Solve the quadratic equation c[0]+c[1]*z+c[2]*z^2 to obtain solutions 
	// z[1] and z[2]
	float InvC2 = 1.0f / c[2];
	float p = c[1] * InvC2;
	float q = c[0] * InvC2;
	float D = (p * p * 0.25f) - q;
	float r = sqrt(D);
	z[1] = -p * 0.5f - r;
	z[2] = -p * 0.5f + r;
	// Compute the shadow intensity by summing the appropriate weights
	float4 Switch =
		(z[2] < z[0]) ? float4(z[1], z[0], 1.0f, 1.0f) : (
		(z[1] < z[0]) ? float4(z[0], z[1], 0.0f, 1.0f) :
		float4(0.0f, 0.0f, 0.0f, 0.0f));
	float Quotient = (Switch[0] * z[2] - b[0] * (Switch[0] + z[2]) + b[1]) / ((z[2] - Switch[1]) * (z[0] - z[1]));
	OutShadowIntensity = Switch[2] + Switch[3] * Quotient;
	OutShadowIntensity = saturate(OutShadowIntensity);
}

#endif