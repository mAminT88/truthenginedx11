#ifndef HLSL_GENERATEMOMENT
#define HLSL_GENERATEMOMENT

#if defined(__INTELLISENSE__)
    #include "../Includes/Include_CommonConstantBuffer.hlsli"
    #include "../Includes/Include_Lights.hlsli"
#else
    #include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
    #include "Graphics/Shaders/Includes/Include_Lights.hlsli"
#endif

ILight_Base iLight;

struct pixelIn
{
    float4 ShadowPos : SV_Position;
    float LinearDepth : TEXCOORD0;
};

float4 Generate4Moments(float depth)
{
    float Square = depth * depth;
    float4 Moments = float4(depth, Square, Square * depth, Square * Square);
    Moments.xz = mul(Moments.xz, float2x2(1.5f, sqrt(3.0f) * 0.5f, -2.0f, -sqrt(3.0f) * 2.0f / 9.0f)) + 0.5f;
    Moments.yw = mul(Moments.yw, float2x2(4.0f, 0.5f, -4.0f, 0.5f));

    return Moments;
}

#endif