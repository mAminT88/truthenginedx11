#if defined(__INTELLISENSE__)

#include "../Includes/Include_CommonConstantBuffer.hlsli"
#include "../Includes/Include_Samplers.hlsli"

#else

#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"

#endif

/////////////////////////////////////////////////////////////////////////////////
//Shader Resouces

Texture2D<float4> HDRTex		: register(t0);
Texture2D<float4> tColor			:register(t1);
StructuredBuffer<float> AvgLum	: register(t2);
Texture2D<float4> tBloom		:register(t3);

/////////////////////////////////////////////////////////////////////////////////
//Interfaces

interface IToneMapping
{
	float3 ToneMapping(float3 HDRColor);
};

interface IBloom
{
	void ApplyBloom(inout float3 Color, float2 UV);
};

/////////////////////////////////////////////////////////////////////////////////
//Classes

class ToneMapping_Reinhard : IToneMapping
{

	static const float3 LUM_FACTOR = float3(0.299, 0.587, 0.114);


	float3 ToneMapping(float3 HDRColor)
	{
		// Find the luminance scale for the current pixel
		float LScale = dot(HDRColor, LUM_FACTOR);
		LScale *= MiddleGrey / AvgLum[0];
		LScale = (LScale + LScale * LScale / LumWhiteSqr) / (1.0 + LScale);

		// Apply the luminance scale to the pixels color
		return HDRColor * LScale;
	}

};

class ToneMapping_ACES : IToneMapping
{
	// sRGB => XYZ => D65_2_D60 => AP1 => RRT_SAT
	static const float3x3 ACESInputMat =
	{
		{0.59719, 0.35458, 0.04823},
		{0.07600, 0.90834, 0.01566},
		{0.02840, 0.13383, 0.83777}
	};

	// ODT_SAT => XYZ => D60_2_D65 => sRGB
	static const float3x3 ACESOutputMat =
	{
		{ 1.60475, -0.53108, -0.07367},
		{-0.10208,  1.10813, -0.00605},
		{-0.00327, -0.07276,  1.07602}
	};

	float3 RRTAndODTFit(float3 v)
	{
		float3 a = v * (v + 0.0245786f) - 0.000090537f;
		float3 b = v * (0.983729f * v + 0.4329510f) + 0.238081f;
		return a / b;
	}

	float3 ToneMapping(float3 color)
	{
		color = mul(ACESInputMat, color);

		// Apply RRT and ODT
		color = RRTAndODTFit(color);

		color = mul(ACESOutputMat, color);

		// Clamp to [0, 1]
		color = saturate(color);

		return color;
	}
};


class Bloom_None : IBloom
{
	void ApplyBloom(inout float3 Color, float2 UV)
	{
		return;
	}
};

class Bloom_Default : IBloom
{
	void ApplyBloom(inout float3 Color, float2 UV)
	{
		Color += gBloomScale.xxx * tBloom.Sample(gSamplerLinear, UV).xyz;
	}
};

//////////////////////////////////////////////////////////////////////////////////
//Variables

IToneMapping gIToneMapping;
IBloom gIBloom;
