#if defined(__INTELLISENSE__)
	#include "../Includes/Include_CommonConstantBuffer.hlsli"
	#include "../Includes/Include_Lights.hlsli"
	#include "../Includes/Include_Samplers.hlsli"
    #include "../Includes/Include_HelperFunc.hlsli"
    #include "../Includes/Include_ShadowAlgorithms.hlsli"
    #include "../Includes/Include_SSAO.hlsli"
#else
	#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
	#include "Graphics/Shaders/Includes/Include_Lights.hlsli"
	#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
    #include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"
    #include "Graphics/Shaders/Includes/Include_ShadowAlgorithms.hlsli"
    #include "Graphics/Shaders/Includes/Include_SSAO.hlsli"
#endif


//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------
Texture2D gGBufferColor : register(t0);
Texture2D gGBufferNormalW : register(t1);
Texture2D gGBufferSpecPow : register(t2);
Texture2D gGBufferDepth : register(t3);

Texture2DArray tShadowMapArray : register(t4);

Texture2D<float> tSSAOMap : register(t5);
//Texture2D tRandVectors : register(t5);


//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------

struct vertexOut
{
	float4 PosH : SV_Position;
	float2 Tex : TEXCOORD0;
	float2 csPos : TEXCOORD1;
};

struct pixelOut
{
	float4 Color : SV_TARGET0;
	float4 Luminance : SV_TARGET1;
};

struct GBufferData
{
	float3 Color;
	float SpecIntensity;
	float3 NormalW;
	float SpecPow;
	float depth;
};

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface IGBufferManager
{
	GBufferData GetData(float2 texCoord); 
};

interface ISSAOManager
{
	float GetSSAO(float2 uv, Texture2D<float> tSSAOMap);
};

interface IOutputManager
{
	pixelOut GenerateOutput(float3 colorr, float specIntensity, float3 diffuse, float3 ambient, float3 specular);
};



//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CGBufferManager_Base : IGBufferManager
{
	GBufferData GetData(float2 texCoord)
	{
		GBufferData data;

		float4 ColorSpecIntensity = gGBufferColor.Sample(gSamplerPoint, texCoord);

		data.Color = ColorSpecIntensity.xyz;
		data.SpecIntensity = ColorSpecIntensity.w;

		float3 normal = gGBufferNormalW.Sample(gSamplerPoint, texCoord).xyz;

		data.NormalW = normalize(normal * 2.0f - 1.0f);

		data.SpecPow = gGBufferSpecPow.Sample(gSamplerPoint, texCoord).x;

		data.depth = gGBufferDepth.Sample(gSamplerPoint, texCoord).x;

		return data;
	}
};

class SSAOManager_NoSSAO : ISSAOManager
{
	float GetSSAO(float2 uv, Texture2D<float> tSSAOMap)
	{
		return 1.0f;
	}
};

class SSAOManager_Default : ISSAOManager
{
	float GetSSAO(float2 uv, Texture2D<float> tSSAOMap)
	{
		return tSSAOMap.Sample(gSamplerPoint, uv).x;
	}
};

class OutputManager_BackBuffer : IOutputManager
{
	pixelOut GenerateOutput(float3 color, float specIntensity, float3 diffuse, float3 ambient, float3 specular)
	{
		pixelOut pOut;

		pOut.Color = float4(color.xyz * (diffuse + ambient) + (specular * specIntensity.xxx) , 1.0f);
		pOut.Luminance = 0.0f;

		return pOut;
	}
};

class OutputManager_PreProcess : IOutputManager
{
	pixelOut GenerateOutput(float3 color, float specIntensity, float3 diffuse, float3 ambient, float3 specular)
	{
		pixelOut pOut;

		pOut.Color = float4(color , 1.0f);
		pOut.Luminance = float4(color * ( diffuse + ambient) + (specular * specIntensity.xxx), 1.0f);

		return pOut;

	}
};


//--------------------------------------------------------------------------------------
// Interface Instances
//--------------------------------------------------------------------------------------

IGBufferManager gIGBufferManager;

//ILightArray_Basic gILightsArray_Spot;
//ILightArray_Basic gILightsArray_Direct;

IShadowManager gIShadowManager;

ISSAOManager gISSAOManager;

IOutputManager gIOutputManager;


//CGBufferManager_Base g_CGBufferManager_Base;

//--------------------------------------------------------------------------------------
// Constant Buffers
//--------------------------------------------------------------------------------------

