#if defined(__INTELLISENSE__)
#include "../Includes/Include_CommonConstantBuffer.hlsli"
#include "../Includes/Include_Samplers.hlsli"
#include "../Includes/Include_HelperFunc.hlsli"
#include "../Includes/Include_GaussianWeights.hlsli"
#else
#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"
#include "Graphics/Shaders/Includes/Include_GaussianWeights.hlsli"
#endif

#if defined(__INTELLISENSE__)

#define BLUR_WIDTH 3.0f
#define BLUR_RADIUS 1.0f
#define GAUSSIANWEIGHTS Gaussian3
#define DEPTH_DIFF 20.0f

#endif

Texture2D tInput : register(t0);
Texture2D<float> tDepthMap : register(t1);
Texture2D<float3> tNormalMap : register(t2);

interface IApplyBlur
{
	float DoBlur(float2 uv);
};

class ApplyBlur_Horz : IApplyBlur
{

	float DoBlur(float2 uv)
	{

		float2 offset = float2(gScreenDXY.x, 0.0f);

		float weightSum = GAUSSIANWEIGHTS[0];
		float color = tInput.Sample(gSamplerPoint_Clamp, uv) * weightSum;

		float depth = tDepthMap.Sample(gSamplerPoint_Clamp, uv);
		depth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);
		float3 normal = tNormalMap.Sample(gSamplerPoint_Clamp, uv);
		normal = normalize(normal * 2.0f - 1.0f);

		[unroll]
		for (int i = -BLUR_RADIUS; i <= BLUR_RADIUS; ++i)
		{

			if (i == 0)
				continue;

			float2 tex = uv + (offset * i);

			float3 snormal = tNormalMap.Sample(gSamplerPoint_Clamp, tex);
			snormal = normalize(snormal * 2.0f - 1.0f);
			float sdepth = ConvertToLinearDepth(tDepthMap.Sample(gSamplerPoint_Clamp, tex), gPerspectiveValues.w, gPerspectiveValues.z);



			if (dot(normal, snormal) >= 0.8f &&
				abs(sdepth - depth) <= DEPTH_DIFF)
			{
				float weight = GAUSSIANWEIGHTS[abs(i)];

				color += tInput.Sample(gSamplerPoint_Clamp, tex) * weight;

				weightSum += weight;

			}

		}

		return color / weightSum;

	}

};

class ApplyBlur_Vert : IApplyBlur
{

	float DoBlur(float2 uv)
	{

		float2 offset = float2(0.0f, gScreenDXY.y);

		float weightSum = GAUSSIANWEIGHTS[0];
		float color = tInput.Sample(gSamplerPoint_Clamp, uv) * weightSum;

		float depth = tDepthMap.Sample(gSamplerPoint_Clamp, uv);
		depth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);
		float3 normal = tNormalMap.Sample(gSamplerPoint_Clamp, uv);
		normal = normalize(normal * 2.0f - 1.0f);

		[unroll]
		for (int i = -BLUR_RADIUS; i <= BLUR_RADIUS; ++i)
		{
			float2 tex = uv + (offset * i);

			float3 snormal = tNormalMap.Sample(gSamplerPoint_Clamp, tex);
			snormal = normalize(snormal * 2.0f - 1.0f);
			float sdepth = ConvertToLinearDepth(tDepthMap.Sample(gSamplerPoint_Clamp, tex), gPerspectiveValues.w, gPerspectiveValues.z);



			if (dot(normal, snormal) >= 0.8f &&
				abs(sdepth - depth) <= DEPTH_DIFF)
			{
				float weight = GAUSSIANWEIGHTS[abs(i)];

				color += tInput.Sample(gSamplerPoint_Clamp, tex) * weight;

				weightSum += weight;

			}

		}

		return color / weightSum;

	}

};



