#ifndef HLSL_SAMPLERS
#define HLSL_SAMPLERS

//--------------------------------------------------------------------------------------
// Samplers
//--------------------------------------------------------------------------------------

SamplerState gSamplerLinear : register(s0); //wrap
SamplerState gSamplerLinear_Clamp : register(s1);
SamplerState gSamplerLinear_Border_0 : register(s2);
SamplerState gSamplerLinear_Border_1 : register(s3);
SamplerState gSamplerPoint : register(s4); //wrap
SamplerState gSamplerPoint_Clamp : register(s5);
SamplerState gSamplerPoint_BorderColor_0 : register(s6);
SamplerState gSamplerPoint_BorderColor_1 : register(s7);
SamplerComparisonState gSamplerDepth : register(s8);
SamplerComparisonState gSamplerDepth_Greater : register(s9);

//SamplerState gSamplerDepthMap : register(s8)
//{
//	Filter = MIN_MAG_LINEAR_MIP_POINT;
//
//	// Set a very far depth value if sampling outside of the NormalDepth map
//	// so we do not get false occlusions.
//	AddressU = BORDER;
//	AddressV = BORDER;
//	BorderColor = float4(1.0f, 1.0f, 1.0f, 1.0f);
//};

#endif