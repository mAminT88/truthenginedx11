#ifndef HLSL_COMMONREGISTERSLOTS
#define HLSL_COMMONREGISTERSLOTS

#if defined(__INTELLISENSE__)
#include "../../ShaderSlots_DeferredShading.h"
#else
#include "Graphics/ShaderSlots_DeferredShading.h"
#endif


//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------

Texture2D gTex_DiffuseMap : register(HLSL_SHADER_SLOTS_PS_TEX_DIFFUSEMAP);
Texture2D gTex_NormalMap : register(HLSL_SHADER_SLOTS_PS_TEX_NORMALMAP);
Texture2D gTex_SpecularMap : register(HLSL_SHADER_SLOTS_PS_TEX_SPECULARMAP);


#endif