#if defined(__INTELLISENSE__)
	#include "../Includes/Include_CommonConstantBuffer.hlsli"
#else
#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#endif

//--------------------------------------------------------------------------------------
// Functions
//--------------------------------------------------------------------------------------

float2 GetMomentFPBias()
{

    return float2(0.5, 0.0);

}


//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------

struct pixelIn
{
    float4 PosH : SV_Position;
    float LinearDepth : TEXCOORD0;
};

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface IVSMGenerator
{
    //float4 CalcMoments( float viewDepth, float normalizedLinearDepth );
    float4 CalcMoments( float4 ShadowPos, float LinearDepth);
    int4 CalcMoments_INT( float4 ShadowPos, float LinearDepth );
};

ILight_Base iLight;

//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CVSMGenerator_LienarDepth : IVSMGenerator
{
    float4 CalcMoments( float4 ShadowPos, float LinearDepth )
    {

        float moment2 = LinearDepth * LinearDepth;

        //float dx_2 = ddx(viewDepth);
        //float dy_2 = ddy(viewDepth);

        //dx_2 *= dx_2;
        //dy_2 *= dy_2;

        //moment2 += 0.25f * (dx_2 + dy_2);

        return float4(LinearDepth, moment2, 0.0f, 0.0f);
    }

    int4 CalcMoments_INT( float4 ShadowPos, float LinearDepth )
    {

        float moment2 = LinearDepth * LinearDepth;

        //float dx_2 = ddx(viewDepth);
        //float dy_2 = ddy(viewDepth);

        //dx_2 *= dx_2;
        //dy_2 *= dy_2;

        //moment2 += 0.25f * (dx_2 + dy_2);

        return int4((int) LinearDepth, (int) moment2, 0.0f, 0.0f);
    }
};

class CVSMGenerator_NormalizedLienarDepth : IVSMGenerator
{
    float4 CalcMoments( float4 ShadowPos, float LinearDepth )
    {
        float normalizedLinearDepth = iLight.ConvertToNormalizedLinearDepth(ShadowPos);

        float d = normalizedLinearDepth;

        float moment2 = d * d;

        //float dx_2 = ddx(d);
        //float dy_2 = ddy(d);

        //dx_2 *= dx_2;
        //dy_2 *= dy_2;

        //moment2 += 0.25f * (dx_2 + dy_2);

        return float4(d, moment2, 0.0f, 0.0f);
    }

    int4 CalcMoments_INT( float4 ShadowPos, float LinearDepth )
    {
        return int4(1, 1, 1, 1);
    }
};