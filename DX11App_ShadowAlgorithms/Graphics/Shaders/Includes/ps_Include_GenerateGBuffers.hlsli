#if defined(__INTELLISENSE__)
#include "Include_Samplers.hlsli"

#define VERTEX_TYPE_SKINNED

#else
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#endif

//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------

Texture2D gTex_DiffuseMap : register(t0);
Texture2D gTex_NormalMap : register(t1);


//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------


struct pixelIn
{
    float4 PosH : SV_POSITION;
    float3 NormalW : NORMAL;
    float2 Tex : TEXCOORD0;
};

struct pixelOut
{
    float4 Color : SV_TARGET0;
    float4 NormalW_UNORM : SV_TARGET1;
    float SpecPow : SV_TARGET2;
};

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface IMaterialBase
{
    float4 GetAmbientColor( float2 texCoord );
    float4 GetDiffuseColor( float2 texCoord );
    float4 GetSpecularColor();
};

IMaterialBase g_IMaterialBase;


interface INormalManager
{
    float3 getNormal( pixelIn pin );
};

INormalManager g_INormalManager;

//--------------------------------------------------------------------------------------
// Constatnt Buffers
//--------------------------------------------------------------------------------------

cbuffer cbPerObject : register(b5)
{
    float4 diffuseColor : packoffset(c0);
    float4 ambientColor : packoffset(c1);
    float4 specularColor : packoffset(c2);
};


//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class cMaterialBase : IMaterialBase
{
    float4 GetAmbientColor( float2 texCoord )
    {
        return ambientColor;
    }

    float4 GetDiffuseColor( float2 texCoord )
    {
        return diffuseColor;
    }

    float4 GetSpecularColor()
    {
        return specularColor;
    }
};

class cMaterialTextured : IMaterialBase
{
    float4 GetAmbientColor( float2 texCoord )
    {
        return ambientColor * gTex_DiffuseMap.Sample(gSamplerLinear, texCoord);
    }

    float4 GetDiffuseColor( float2 texCoord )
    {
        return diffuseColor * gTex_DiffuseMap.Sample(gSamplerLinear, texCoord);
    }

    float4 GetSpecularColor()
    {
        return specularColor;
    }
};




class CNormalManager_NormalMap : INormalManager
{
    float3 getNormal( pixelIn pin )
    {
        return pin.NormalW;

    }
};

class CNormalManager_NormalVector : INormalManager
{
    float3 getNormal( pixelIn pin )
    {
        return pin.NormalW;
    }
};
