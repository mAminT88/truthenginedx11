#ifndef HLSL_CB_PERFRAME
#define HLSL_CB_PERFRAME

#if defined(__INTELLISENSE__)
	#include "../Includes/Include_Lights.hlsli"
#else
	#include "Graphics/Shaders/Includes/Include_Lights.hlsli"
#endif


cbuffer cb_ps_per_frame : register(b0)
{
	row_major matrix gView     : packoffset(c0);
    row_major matrix gViewInv  : packoffset(c4);
    row_major matrix gViewProj : packoffset(c8);


    float3 gEyePos : packoffset(c12.x);
    float gEyeZNear : packoffset(c12.w);

    float3 gEyeDir : packoffset(c13.x);
    float gEyeZFar : packoffset(c13.w);

	float gLuminanceAdaptation : packoffset(c14.x);
	float3 per_frame_pad0 : packoffset(c14.y);
};


cbuffer cb_ps_unfreq : register(b1)
{
	row_major matrix gProj;

	float4 gPerspectiveValues;

    float2 gFixedPrecision_Float32ToInt32;
    int gNumSpotLights;
    int gNumDirectLights;

    float2 gShadowMapSize;
    float2 gShadowMapDXY;

    float2 gScreenSize;
    float2 gScreenDXY;

    float gAspectRatio;
    float gFOV_Y;
	float MiddleGrey;
	float LumWhiteSqr;

	float gOcclusionRadius;
	float gOcclusionFadeStart;
	float gOcclusionFadeEnd;
	float gSurfaceEpsilon;

	float4 gOffsetVectors[14];
	
	//HDR Compute Shader
	uint2 gRes;   // Resulotion of the qurter size target: x - width, y - height
	uint gDomain; // Total pixel in the downscaled image
	uint gGroupSize; // Number of groups dispached on the first pass

	float gBloomThreshold;
	float gBloomScale;
	float2 cb_unfrequent_pad0;

};


cbuffer cb_ps_spotlights : register(b2)
{
    cLight_SpotLight gSpotLights[MAX_LIGHT_SPOTLIGHT];
};


cbuffer cb_ps_directlights : register(b3)
{
    cLight_DirectLight gDirectLights[MAX_LIGHT_DIRECTLIGHT];
};


#endif