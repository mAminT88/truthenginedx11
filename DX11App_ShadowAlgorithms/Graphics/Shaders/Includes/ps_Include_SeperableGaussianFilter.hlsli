#if defined(__INTELLISENSE__)

#include "../Includes/Include_GaussianWeights.hlsli"
#include "../Includes/Include_Samplers.hlsli"

#define FILTER_WIDTH 5.0f
#define GaussianWeight Gaussian5

#else

#include "Graphics/Shaders/Includes/Include_GaussianWeights.hlsli"
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"

#endif

#define FILTER_RADIUS ((FILTER_WIDTH - 1) / 2)

//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------
Texture2DArray tVarianceShadwoMapArray : register(t0);


//--------------------------------------------------------------------------------------
// CBuffer
//--------------------------------------------------------------------------------------

cbuffer cb_vsm_filter_perLight : register(b6)
{
    float2 gTexelSize;
    float gTexArrayIndex;
    float cb_vsm_filter_perLight_pad0;

    float2 gHorzDirection;
    float2 gVertDirection;
}


//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------


struct pixelIn
{
    float4 PosH : SV_Position;
    float2 Tex : TEXCOORD0;
    float2 csPos : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface IGaussFilterManager
{

    float4 ApplyFilter( float2 tex );
    float4 ApplyFilter_Radius_Dir( float2 tex, float Radius );

};


interface IBoxFilterManager
{
    float4 ApplyFilter( float2 tex );
};


//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CGaussFilterManager_Horizontally : IGaussFilterManager
{
    float4 ApplyFilter(float2 tex)
    {

        float3 location = float3(tex, gTexArrayIndex);

        float4 r;

        float weight_sum = 0.0f;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0) * GaussianWeight[0];
        weight_sum += GaussianWeight[0];

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        [unroll]
        for (int i = 1; i <= FILTER_RADIUS; ++i)
        {

            offset.x = gTexelSize.xy * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0) * GaussianWeight[i];
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0) * GaussianWeight[i];

            weight_sum += GaussianWeight[i] * 2.0f;

        }

        return r / weight_sum;

    }

    float4 ApplyFilter_Radius_Dir( float2 tex, float Radius )
    {
        float3 location = float3(tex, gTexArrayIndex);

        float4 r;

        float weight_sum = 0.0f;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0) * GaussianWeight[0];
        weight_sum += GaussianWeight[0];

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        float FilterRadius = FILTER_RADIUS;

        float2 step = (Radius / FilterRadius) * gHorzDirection.xy;

        [unroll]
        for (int i = 1; i <= FILTER_RADIUS; ++i)
        {

            offset.xy = step * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0) * GaussianWeight[i];
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0) * GaussianWeight[i];

            weight_sum += GaussianWeight[i] * 2.0f;

        }

        return r / weight_sum;

    }
};

class CGaussFilterManager_Vertically : IGaussFilterManager
{
    float4 ApplyFilter( float2 tex )
    {
        float3 location = float3(tex, 0);

        float4 r;

        float weight_sum = 0.0f;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0) * GaussianWeight[0];
        weight_sum += GaussianWeight[0];

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        [unroll]
        for (int i = 1; i <= FILTER_RADIUS; ++i)
        {

            offset.y = gTexelSize * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0) * GaussianWeight[i];
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0) * GaussianWeight[i];

            weight_sum += GaussianWeight[i] * 2.0f;

        }

        return r / weight_sum;

    }

    float4 ApplyFilter_Radius_Dir( float2 tex, float Radius )
    {
        float3 location = float3(tex, gTexArrayIndex);

        float4 r;

        float weight_sum = 0.0f;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0) * GaussianWeight[0];
        weight_sum += GaussianWeight[0];

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        float FilterRadius = FILTER_RADIUS;

        float2 step = (Radius / FilterRadius) * gVertDirection.xy;

        [unroll]
        for (int i = 1; i <= FILTER_RADIUS; ++i)
        {

            offset.xy = step * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0) * GaussianWeight[i];
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0) * GaussianWeight[i];

            weight_sum += GaussianWeight[i] * 2.0f;

        }

        return r / weight_sum;

    }
};

class CBoxFilterManager_Horizontally : IBoxFilterManager
{
    float4 ApplyFilter(float2 tex)
    {
        float FilterWidth = FILTER_WIDTH;

        float3 location = float3(tex, 0);

        float4 r;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0);

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        [unroll]
        for (int i = 1; i <= FILTER_RADIUS; ++i)
        {

            offset.x = gTexelSize * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0);
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0);

        }


        return r / FilterWidth;
    }
};

class CBoxFilterManager_Vertically : IBoxFilterManager
{
    float4 ApplyFilter( float2 tex )
    {
        float FilterWidth = FILTER_WIDTH;

        float3 location = float3(tex, 0);

        float4 r;

        r = tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0);

        float3 offset = float3(0.0f, 0.0f, 0.0f);

        [unroll]
        for (int i = 1; i <= FILTER_RADIUS; ++i)
        {

            offset.y = gTexelSize * i;

            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0);
            r += tVarianceShadwoMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0);

        }


        return r / FilterWidth;
    }
};
