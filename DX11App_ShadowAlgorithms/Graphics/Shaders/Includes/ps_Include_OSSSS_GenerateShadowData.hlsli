#ifndef HLSL_OSSSS_SHADOWDATA
#define HLSL_OSSSS_SHADOWDATA

#if defined(__INTELLISENSE__)
	#include "../Includes/Include_Samplers.hlsli"
	#include "../Includes/Include_GaussianWeights.hlsli"
	#include "../Includes/Include_CommonConstantBuffer.hlsli"
	#include "../Includes/Include_HelperFunc.hlsli"
	#include "../Includes/Include_PoissonDisks.hlsli"


#define BLOCKER_SEARCH_SAMPLE_COUNT         25
#define BLOCKER_SEARCH_POISSON              Poisson25

//#define BLOCKER_SEARCH_WIDTH_SAMPLE_COUNT    5
//#define GAUSSIAN_WEIGHTS_BLOCKER_SEARCH     Gaussian5
	
#define FILTER_WIDTH_HARDSHADOW             5
#define GAUSSIAN_WEIGHTS_HARDSHADOW         Gaussian5
	
#define BILATERAL_DEPTH                     0.003f

#else

#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#include "Graphics/Shaders/Includes/Include_GaussianWeights.hlsli"
#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"
#include "Graphics/Shaders/Includes/Include_PoissonDisks.hlsli"

#endif

//#define V1
#define V2
//#define CROSS_PRODUCT

#define FILTER_RADIUS_HARDSHADOW            (FILTER_WIDTH_HARDSHADOW - 1.0f) * 0.5f
#define FILTER_RADIUS_HARDSHADOW_INV        (1.0f / FILTER_RADIUS_HARDSHADOW)

//#define BLOCKER_SEARCH_RADIUS_SAMPLE_COUNT   (BLOCKER_SEARCH_WIDTH_SAMPLE_COUNT - 1.0f) * 0.5f

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------
ILight_Base iLight;

//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------
Texture2D tDepthMap : register(t0);
Texture2DArray tShadowMapArray : register(t1);
Texture2D tGBuffer_Normal : register(t2);
Texture2DArray tAvgBlocker : register(t3);
//Texture2D tShadowMap_Horz : register(t2);
Texture2DArray tHardShadow : register(t4);
Texture2D tPenumbraSize : register(t5);

//--------------------------------------------------------------------------------------
// Constant Buffer
//--------------------------------------------------------------------------------------



//--------------------------------------------------------------------------------------
// Strcuturs
//--------------------------------------------------------------------------------------


struct vertexOut_render2D
{
    float4 PosH : SV_Position;
    float2 Tex : TEXCOORD0;
    float2 csPos : TEXCOORD1;
};

struct pixelOut_0
{
    float HardShadowData : SV_TARGET0;
    float PenumbraSize : SV_TARGET1;
};

struct pixelOut_1
{
    float SoftShadows_Horz : SV_TARGET0;
};


//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface IGenerateShadowData_0
{
    pixelOut_0 CalcOutput(float4 shadowCoord);
};

interface IGenerateShadowData_1
{
    pixelOut_1 CalcOutput(float2 texCoord, float ref_depth, float4 shadowCoord, float3 posW, float3 lightPosW);
};




//--------------------------------------------------------------------------------------
// Functions
//--------------------------------------------------------------------------------------

// Computes depth offset (bias)
float BiasedZ(float z0, float2 dz_duv, float2 offset)
{
    return z0 + dot(dz_duv, offset);
}


bool IsBlocker(float depthSample, float ref_depth, float2 dz_duv, float2 offset)
{
    float biasedDepth = BiasedZ(ref_depth, dz_duv, offset);

    return depthSample < biasedDepth;
}

float ShadowMapSampling(float frag_shadow_depth, float3 location, float penumbraSize, float2 dz_duv, float2 Dir)
{

    float sampleCount = FILTER_RADIUS_HARDSHADOW;

    float2 step = Dir * (penumbraSize / sampleCount).xx;

    float3 offset = 0.0f;
    float z;


	//float shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location);

	//float shadowValue = shadowMapSample >= frag_shadow_depth;

	//[unroll]
	//for (int i = 1; i <= FILTER_RADIUS_HARDSHADOW; ++i)
	//{
	//    offset.xy = step * i.xx;
	//    float rand_num = rand2(offset.xy) * 2.0f - 1.0f;

	//    offset += rand_num * stepLength;

	//    z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy);

	//    shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location + offset);

	//    shadowValue += shadowMapSample >= z;

	//    offset.xy *= -1.0f;

	//    offset -= rand_num * stepLength;

	//    z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy);

	//    shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location + offset);

	//    shadowValue += shadowMapSample >= z;
	//}

	/********** Optimized Code **************/

    float shadowMapSample;

    float shadowValue = 0.0f;

	[unroll]
    for (int i = -FILTER_RADIUS_HARDSHADOW; i <= FILTER_RADIUS_HARDSHADOW; ++i)
    {
        offset.xy = step * i.xx;
		/*float rand_num = rand2(offset.xy) * 2.0f - 1.0f;

		offset.xy += rand_num * step;*/

        z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy);

        shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location + offset).x;

        shadowValue += shadowMapSample >= z;
    }

    return shadowValue / FILTER_WIDTH_HARDSHADOW;

}

float ShadowMapSampling_horz(float frag_shadow_depth, float3 location)
{
    float shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location);

    return shadowMapSample >= frag_shadow_depth;

}

float ShadowMapSampling_vertHorz(float penumbraSize, float frag_shadow_depth, float3 location, float2 dz_duv)
{
    float shadowMapSample = tShadowMapArray.Sample(gSamplerLinear_Border_1, location);

    float shadowValue = shadowMapSample >= frag_shadow_depth;

    if (penumbraSize < gShadowMapDXY.x)
    {
        return shadowValue;
    }

    float sampleCount = FILTER_RADIUS_HARDSHADOW;

    float3 step = float3(penumbraSize / sampleCount, 0.0f, 0.0f);

    float3 offset;
    float z;


	//[unroll]
	//for (int i = 1; i <= FILTER_RADIUS_HARDSHADOW; ++i)
	//{
	//    offset = step * i;

	//    shadowMapSample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, location + offset);

	//    z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy);

	//    shadowValue += shadowMapSample >= z;

	//    shadowMapSample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, location - offset);

	//    z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy * -1.0f);

	//    shadowValue += shadowMapSample >= z;
	//}


	/*********** Optimized Code ***************/

	[unroll]
    for (int i = 1; i <= FILTER_RADIUS_HARDSHADOW; ++i)
    {
        offset = step * i;

		/*float rand_num = rand2(offset.xy) * 2.0f - 1.0f;

		offset += rand_num * step;*/

        shadowMapSample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, location + offset);

        z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy);

        shadowValue += shadowMapSample >= z;
		
    }

	[unroll]
    for (int i = -FILTER_RADIUS_HARDSHADOW; i <= -1; ++i)
    {
        offset = step * i;

		/*float rand_num = rand2(offset.xy) * 2.0f - 1.0f;

		offset += rand_num * step;*/

        shadowMapSample = tShadowMapArray.Sample(gSamplerPoint_BorderColor_1, location + offset);

        z = BiasedZ(frag_shadow_depth, dz_duv, offset.xy);

        shadowValue += shadowMapSample >= z;
		
    }

    return shadowValue / FILTER_WIDTH_HARDSHADOW;

}

//void ApplyFilter_MeanBlocker(Texture2DArray shadowMapArray , out uint blockerCount, out float blockerAvg, float2 shadow_coord, float Radius, float ref_depth, float2 dz_duv, float2 Dir )
//{
//    float3 location = float3(shadow_coord, gShadowMapID);

//    blockerAvg = 0.0f;
//    blockerCount = 0;

//    float weight_sum = 0.0f;

//    float depthSample;

//    depthSample = shadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0);

//    if (depthSample < ref_depth)
//    {
//        blockerAvg += depthSample * GAUSSIAN_WEIGHTS_BLOCKER[0];
//        weight_sum += GAUSSIAN_WEIGHTS_BLOCKER[0];
//        blockerCount++;
//    }


//    float3 offset = float3(0.0f, 0.0f, 0.0f);

//    float2 step = (Radius * FILTER_RADIUS_BLOCKER_SEARCH_INV) * Dir;

//    [unroll]
//    for (int i = 1; i <= FILTER_RADIUS_BLOCKER_SEARCH; ++i)
//    {

//        offset.xy = step * i;

//        depthSample = shadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0);

//        if (IsBlocker(depthSample, ref_depth, dz_duv, offset.xy))
//        {
//            blockerAvg += depthSample * GAUSSIAN_WEIGHTS_BLOCKER[i];
//            weight_sum += GAUSSIAN_WEIGHTS_BLOCKER[i];
//            blockerCount++;
//        }


//        depthSample = shadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0);
//        if (IsBlocker(depthSample, ref_depth, dz_duv, offset.xy))
//        {
//            blockerAvg += depthSample * GAUSSIAN_WEIGHTS_BLOCKER[i];
//            weight_sum += GAUSSIAN_WEIGHTS_BLOCKER[i];
//            blockerCount++;
//        }

//    }

//    blockerAvg /= weight_sum;

//}


//void ApplyFilter_MeanBlocker( Texture2D shadowMapArray, out uint blockerCount, out float blockerAvg, float2 shadow_coord, float Radius, float ref_depth, float2 dz_duv, float2 Dir )
//{
//    float2 location = shadow_coord.xy;

//    blockerAvg = 0.0f;
//    blockerCount = 0;

//    float weight_sum = 0.0f;

//    float depthSample;

//    depthSample = shadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location, 0);

//    if (depthSample < ref_depth)
//    {
//        blockerAvg += depthSample * GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[0];
//        weight_sum += GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[0];
//        blockerCount++;
//    }


//    float2 offset = float2(0.0f, 0.0f);

//    float radiusSampleCount = BLOCKER_SEARCH_RADIUS_SAMPLE_COUNT;

//    float2 step = (Radius / radiusSampleCount) * Dir;

//    [unroll]
//    for (int i = 1; i <= BLOCKER_SEARCH_RADIUS_SAMPLE_COUNT; ++i)
//    {

//        offset.xy = step * i;

//        depthSample = shadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location + offset, 0);

//        //if (IsBlocker(depthSample, ref_depth, dz_duv, offset.xy))
//        if (depthSample < ref_depth)
//        {
//            blockerAvg += depthSample * GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[i];
//            weight_sum += GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[i];
//            blockerCount++;
//        }


//        depthSample = shadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, location - offset, 0);
//        //if (IsBlocker(depthSample, ref_depth, dz_duv, offset.xy))
//        if (depthSample < ref_depth)
//        {
//            blockerAvg += depthSample * GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[i];
//            weight_sum += GAUSSIAN_WEIGHTS_BLOCKER_SEARCH[i];
//            blockerCount++;
//        }

//    }

//    blockerAvg /= weight_sum;

//}

void ApplyFilter_FilterHardShadow(out float SoftShadow, float2 tex_coord, float ref_depth, float2 step /*, float Radius, float2 Dir, float LightDepth_Linear, float shadowCoordDerivation*/)
{
	

    float depthSample;
    float weight_sum = 0.0f;
	
	
    float2 offset = float2(0.0f, 0.0f);

    float2 uv;
   
    float shadowMapID = iLight.GetShadowMapID();

    SoftShadow = tHardShadow.Sample(gSamplerLinear_Border_1, float3(tex_coord, shadowMapID)) * GAUSSIAN_WEIGHTS_HARDSHADOW[0];

    weight_sum = GAUSSIAN_WEIGHTS_HARDSHADOW[0];



	/******************** Optimized Code ************************/

	[unroll]
    for (int i = -FILTER_RADIUS_HARDSHADOW; i <= FILTER_RADIUS_HARDSHADOW; ++i)
    {

        offset.xy = step * i;

		/*float rand_num = rand2(offset.xy) * 2.0f - 1.0f;

		offset += rand_num * step;*/

        uv = tex_coord + offset;


        depthSample = tDepthMap.Sample(gSamplerLinear_Border_1, uv);
        float linearDepth = ConvertToLinearDepth(depthSample, gPerspectiveValues.w, gPerspectiveValues.z);

        if (abs(ref_depth - linearDepth) < BILATERAL_DEPTH)
        {
            SoftShadow += tHardShadow.Sample(gSamplerLinear_Border_1, float3(uv, shadowMapID)) * GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];
            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

        }
		
    }

    SoftShadow /= weight_sum;

}

void ApplyFilter_FilterHardShadow_Horz(out float SoftShadow, float2 tex_coord, float4 shadowCoord, float ref_depth, float2 step, float PenumbraSize, float ScreenPenumbraSize, float2 dz_duv /*, float Radius, float2 Dir, float LightDepth_Linear, float shadowCoordDerivation*/)
{

    float weight_sum = 0.0f;

    float depthSample;

    float shadowMapID = iLight.GetShadowMapID();
	
    SoftShadow = tHardShadow.Sample(gSamplerLinear_Border_1, float3(tex_coord, shadowMapID));

    if (ScreenPenumbraSize < gScreenDXY.x && ScreenPenumbraSize < gScreenDXY.y)
    {
        return;
    }

    SoftShadow *= GAUSSIAN_WEIGHTS_HARDSHADOW[0];

    weight_sum = GAUSSIAN_WEIGHTS_HARDSHADOW[0];
	

    float2 offset = float2(0.0f, 0.0f);
   

    float2 uv;

    float sign = -1.0f;

	[unroll]
    for (int i = -FILTER_RADIUS_HARDSHADOW; i <= FILTER_RADIUS_HARDSHADOW; ++i)
    {

        if (i == 0)
            continue;

        offset.xy = step * i;

		/*float rand_num = rand2(offset.xy) * sign;

		sign *= -1.0f;

		offset += rand_num * step;*/


        uv = tex_coord + offset;


        depthSample = tDepthMap.Sample(gSamplerLinear_Border_1, uv);
        float linearDepth = ConvertToLinearDepth(depthSample, gPerspectiveValues.w, gPerspectiveValues.z);

#ifdef V2

        float2 sPosH = UVToPosH(uv);

        float3 sPosW = CalcWorldPos(sPosH, linearDepth, gPerspectiveValues, gViewInv);

        float4 sShadowPos = iLight.CalcShadowPos(sPosW);

        float Distance = distance(sShadowPos.xy, shadowCoord.xy);

        if (Distance < PenumbraSize)
        {

            SoftShadow += tHardShadow.Sample(gSamplerLinear_Border_1, float3(uv, shadowMapID)) * GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

        }
        else
        {
            float sampleCnt = FILTER_RADIUS_HARDSHADOW;
            float shadowMapStep = PenumbraSize / sampleCnt;

            float3 shadowLocation = float3(shadowCoord.xy, shadowMapID);

            float3 shadowMap_offset = float3(shadowMapStep * i, 0.0f, 0.0f);

            shadowLocation += shadowMap_offset;

            float z = BiasedZ(shadowCoord.z, dz_duv, shadowMap_offset.xy);

			//SoftShadow += ShadowMapSampling_horz(z, shadowLocation) * GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];
            SoftShadow += ShadowMapSampling(z, shadowLocation, PenumbraSize, dz_duv, float2(0.0f, 1.0f)) * GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];
        }

#endif

#ifdef V1

		if (abs(ref_depth - linearDepth) < BILATERAL_DEPTH)
		{

			SoftShadow += tHardShadow.Sample(gSamplerLinear_Border_1, float3(uv, shadowMapID)) * GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

			weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

		}
		else
		{
			float sampleCnt = FILTER_RADIUS_HARDSHADOW;
			float shadowMapStep = PenumbraSize / sampleCnt;

			float3 shadowLocation = float3(shadowCoord.xy, shadowMapID);

			float3 shadowMap_offset = float3(shadowMapStep * i, 0.0f, 0.0f);

			shadowLocation += shadowMap_offset;

			float z = BiasedZ(shadowCoord.z, dz_duv, shadowMap_offset.xy);

			SoftShadow += ShadowMapSampling(z, shadowLocation, PenumbraSize, dz_duv, float2(0.0f, 1.0f)) * GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

			weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];
		}

#endif

    }

    SoftShadow /= weight_sum;

}

void ApplyFilter_FilterHardShadow_Vert(out float SoftShadow, float2 tex_coord, float4 shadowCoord, float ref_depth, float2 step, float PenumbraSize, float ScreenPenumbraSize, float2 dz_duv /*, float Radius, float2 Dir, float LightDepth_Linear, float shadowCoordDerivation*/)
{

    float weight_sum = 0.0f;

    float depthSample;

    float shadowMapID = iLight.GetShadowMapID();
	
    SoftShadow = tHardShadow.Sample(gSamplerLinear_Border_1, float3(tex_coord, shadowMapID));

    if (ScreenPenumbraSize < gScreenDXY.x && ScreenPenumbraSize < gScreenDXY.y)
        return;

    SoftShadow *= GAUSSIAN_WEIGHTS_HARDSHADOW[0];

    weight_sum = GAUSSIAN_WEIGHTS_HARDSHADOW[0];
	
    float2 offset = float2(0.0f, 0.0f);

    float2 uv;



	/************************ Optimized Code *************************/

	[unroll]
    for (int i = 1; i <= FILTER_RADIUS_HARDSHADOW; ++i)
    {

        offset.xy = step * i;

		//float rand_num = rand2(offset.xy) * 2.0f - 1.0f;

		//offset += rand_num * step;

        uv = tex_coord + offset;


        depthSample = tDepthMap.Sample(gSamplerLinear_Border_1, uv);
        float linearDepth = ConvertToLinearDepth(depthSample, gPerspectiveValues.w, gPerspectiveValues.z);

        if (abs(ref_depth - linearDepth) < BILATERAL_DEPTH)
        {

            SoftShadow += tHardShadow.Sample(gSamplerLinear_Border_1, float3(uv, shadowMapID)) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];

        }
        else
        {
            float sampleCnt = FILTER_RADIUS_HARDSHADOW;
            float shadowMapStep = PenumbraSize / sampleCnt;

            float3 shadowLocation = float3(shadowCoord.xy, shadowMapID);

            float3 shadowMap_offset = float3(0.0f, shadowMapStep * -i, 0.0f);

            shadowLocation += shadowMap_offset;

            float z = BiasedZ(shadowCoord.z, dz_duv, shadowMap_offset.xy);

            SoftShadow += ShadowMapSampling_vertHorz(PenumbraSize, z, shadowLocation, dz_duv) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];

        }
    }

	[unroll]
    for (int i = -FILTER_RADIUS_HARDSHADOW; i <= -1; ++i)
    {

        offset.xy = step * i;

		/*float rand_num = rand2(offset.xy) * 2.0f - 1.0f;

		offset += rand_num * step;*/

        uv = tex_coord + offset;


        depthSample = tDepthMap.Sample(gSamplerLinear_Border_1, uv);
        float linearDepth = ConvertToLinearDepth(depthSample, gPerspectiveValues.w, gPerspectiveValues.z);

        if (abs(ref_depth - linearDepth) < BILATERAL_DEPTH)
        {

            SoftShadow += tHardShadow.Sample(gSamplerLinear_Border_1, float3(uv, shadowMapID)) * GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

        }
        else
        {
            float sampleCnt = FILTER_RADIUS_HARDSHADOW;
            float shadowMapStep = PenumbraSize / sampleCnt;

            float3 shadowLocation = float3(shadowCoord.xy, shadowMapID);

            float3 shadowMap_offset = float3(0.0f, shadowMapStep * -i, 0.0f);

            shadowLocation += shadowMap_offset;

            float z = BiasedZ(shadowCoord.z, dz_duv, shadowMap_offset.xy);

            SoftShadow += ShadowMapSampling_vertHorz(PenumbraSize, z, shadowLocation, dz_duv) * GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[abs(i)];

        }
    }

    SoftShadow /= weight_sum;

}


float3 CalcPosW(float3 csPos, float linearDepth)
{
    float3 posV = float3(0.0f, 0.0f, 0.0f);
    posV.xy = csPos.xy * gPerspectiveValues.xy * linearDepth.xx;
    posV.z = linearDepth;
	
    return mul(float4(posV, 1.0), gViewInv).xyz;
}


float EstimatePenumbraSize(float _lightSize, float _receiver, float _blocker)
{
    return _lightSize * (_receiver - _blocker) / _blocker;
}


// Derivatives of light-space depth with respect to texture coordinates
float2 DepthGradient(float2 uv, float z)
{
    float2 dz_duv = 0;

	// Packing derivatives of u,v, and distance to light source w.r.t. screen space x, and y
    float3 duvdist_dx = ddx(float3(uv, z));
    float3 duvdist_dy = ddy(float3(uv, z));

	// Top row of 2x2
    dz_duv.x = duvdist_dy.y * duvdist_dx.z;
    dz_duv.x -= duvdist_dx.y * duvdist_dy.z;
	
	// Bottom row of 2x2
    dz_duv.y = duvdist_dx.x * duvdist_dy.z;
    dz_duv.y -= duvdist_dy.x * duvdist_dx.z;

	// Multiply ddist/dx and ddist/dy by inverse transpose of Jacobian
    float det = (duvdist_dx.x * duvdist_dy.y) - (duvdist_dx.y * duvdist_dy.x);
    dz_duv /= det;

    return dz_duv;
}


// Using similar triangles from the surface point to the area light
float2 SearchRegionRadiusUV(float zLight, float zNear, float lightSize)
{
    return lightSize * (zLight - zNear) / zLight;
	//return lightSize * (zLight - gZNear) / (zLight - gZFar);
	//return lightSize *  (zLight - gZFar) / (zLight - gZNear);
	//return lightSize * zNear / zLight;
}


//Find Blockers By Brute_Force Sampling From Shadow Map with Calculates Hard Shadow for Corresponding Point
void FindBlocker(out float hardShadowBit,
				out float avgBlockerDepth,
				out float numBlockers,
				float2 uv,
				float z0,
				float2 dz_duv,
				float2 searchRegionRadiusUV,
				float shadowMapID)
{

    float blockerSum = 0.0f;
    numBlockers = 0.0f;

    float shadowDepth = tShadowMapArray.SampleLevel(gSamplerPoint, float3(uv, shadowMapID), 0);
    hardShadowBit = shadowDepth > z0;

	[unroll]
    for (int i = 0; i < BLOCKER_SEARCH_SAMPLE_COUNT; ++i)
    {
        float2 offset = BLOCKER_SEARCH_POISSON[i] * searchRegionRadiusUV;
        float shadowMapDepth = tShadowMapArray.SampleLevel(gSamplerPoint, float3(uv + offset, shadowMapID), 0);
        float z = BiasedZ(z0, dz_duv, offset);

		// averages depth values that are closer to the light than the receiving point {average blockers only}
        if (shadowMapDepth < z)
        {
            blockerSum += shadowMapDepth;
            numBlockers++;
        }
    }

    avgBlockerDepth = blockerSum / numBlockers;
}

//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CGenerateShadowData_0 : IGenerateShadowData_0
{

    pixelOut_0 CalcOutput(float4 shadowCoord)
    {

        pixelOut_0 result;

        float2 dz_duv = DepthGradient(shadowCoord.xy, shadowCoord.z);

        float LinearDepth = iLight.ConvertToLinearDepth(shadowCoord.z);

        float searchRadius = SearchRegionRadiusUV(LinearDepth, iLight.GetZNear(), iLight.GetLightSize());

        float blockerAvg;
        uint blockerCount;

        float shadowMapID = iLight.GetShadowMapID();

        FindBlocker(result.HardShadowData, blockerAvg, blockerCount, shadowCoord.xy, shadowCoord.z, dz_duv, searchRadius, shadowMapID);

        if (blockerCount == 0)
        {
            result.PenumbraSize = 0.0f;

            return result;
        }

        float blockerAvg_Linear = iLight.ConvertToLinearDepth(blockerAvg);

        result.PenumbraSize = EstimatePenumbraSize(iLight.GetLightSize(), LinearDepth, blockerAvg_Linear);

        result.HardShadowData = ShadowMapSampling(shadowCoord.z, float3(shadowCoord.xy, shadowMapID), result.PenumbraSize, dz_duv, float2(0.0f, 1.0f));

        return result;
    }

};

class CGenerateShadowData_0_UsePreFilterAvgBlocker : IGenerateShadowData_0
{

    pixelOut_0 CalcOutput(float4 shadowCoord)
    {

        pixelOut_0 result;

        float2 dz_duv = DepthGradient(shadowCoord.xy, shadowCoord.z);

        float LinearDepth = iLight.ConvertToLinearDepth(shadowCoord.z);

        float searchRadius = SearchRegionRadiusUV(LinearDepth, iLight.GetZNear(), iLight.GetLightSize());

        float blockerAvg = tAvgBlocker.Sample(gSamplerPoint, float3(shadowCoord.xy, 0.0f));

        if (blockerAvg > 0.99999f)
        {
            result.PenumbraSize = 0.0f;

            result.HardShadowData = 1.0f;

            return result;
        }

        float blockerAvg_Linear = iLight.ConvertToLinearDepth(blockerAvg);

        result.PenumbraSize = EstimatePenumbraSize(iLight.GetLightSize(), LinearDepth, blockerAvg_Linear);

        result.HardShadowData = ShadowMapSampling(shadowCoord.z, float3(shadowCoord.xy, iLight.GetShadowMapID()), result.PenumbraSize, dz_duv, float2(0.0f, 1.0f));

        return result;
    }

};

class CGenerateShadowData_1_Horz : IGenerateShadowData_1
{
    pixelOut_1 CalcOutput(float2 texCoord, float ref_depth, float4 shadowCoord, float3 posW, float3 lightPosW)
    {
        pixelOut_1 result;

        float3 LightVector = lightPosW - posW;
        float LinearDepth = length(LightVector);

        float DistanceMultiplier = LinearDepth / ref_depth;

        float3 normal = tGBuffer_Normal.Sample(gSamplerPoint, texCoord);

        normal = normalize(normal * 2.0f - 1.0f);

        LightVector = normalize(LightVector);

//#ifndef CROSS_PRODUCT

        float3 EyeVector = gEyePos - posW;

        EyeVector = normalize(EyeVector);

        float2 LightHorzDir = iLight.GetDirInEyeScreen_Horz();

        float DirMultiplier = abs(dot(LightHorzDir, float2(0.0f, 1.0f)));

        float EyeAngle = dot(normal, EyeVector);
        float LightAngle = dot(normal, LightVector);

        float AngleMultiplier = EyeAngle / LightAngle;
        AngleMultiplier = clamp(AngleMultiplier, 0.0f, 1.0f);
        AngleMultiplier = pow(AngleMultiplier, DirMultiplier);

        float PenumbraSize = tPenumbraSize.Sample(gSamplerPoint, texCoord);
		
        float ScreenPenumbraSize = PenumbraSize * DistanceMultiplier * (AngleMultiplier) * 0.70;

        float filter_radius_sample_count = FILTER_RADIUS_HARDSHADOW;

        float2 step = (ScreenPenumbraSize / filter_radius_sample_count).xx;

        step *= LightHorzDir;

//#else

//        float3 cross_product = cross(normal, LightVector);

//        cross_product = normalize(cross_product);

//        cross_product = posW + cross_product;

//        float4 p = mul(float4(cross_product, 1.0f), gViewProj);

//        p.xyz /= p.w;

//        float2 q = UVToPosH(texCoord);


//        float2 LightHorzDir = p.xy - q;

//        float PenumbraSize = tPenumbraSize.Sample(gSamplerPoint, texCoord);
		
//        float ScreenPenumbraSize = PenumbraSize * DistanceMultiplier;

//        float filter_radius_sample_count = FILTER_RADIUS_HARDSHADOW;

//        float2 step = (ScreenPenumbraSize / filter_radius_sample_count).xx;

//        step *= LightHorzDir;

//#endif


        float2 dz_duv = DepthGradient(shadowCoord.xy, shadowCoord.z);

        ApplyFilter_FilterHardShadow_Horz(result.SoftShadows_Horz, texCoord, shadowCoord, ref_depth, step, PenumbraSize, ScreenPenumbraSize, dz_duv);

        return result;

    }

};

class CGenerateShadowData_1_Vert : IGenerateShadowData_1
{
    pixelOut_1 CalcOutput(float2 texCoord, float ref_depth, float4 shadowCoord, float3 posW, float3 lightPosW)
    {
        pixelOut_1 result;

        float LinearDepth = iLight.ConvertToLinearDepth(shadowCoord.z);

        float DistanceMultiplier = LinearDepth / ref_depth;

        float3 normal = tGBuffer_Normal.Sample(gSamplerPoint, texCoord);

        normal = normalize(normal * 2.0f - 1.0f);

        float3 EyeVector = gEyePos - posW;
        float3 LightVector = lightPosW - posW;

        EyeVector = normalize(EyeVector);
        LightVector = normalize(LightVector);

        float EyeAngle = dot(normal, EyeVector);
        float LightAngle = dot(normal, LightVector);

        float AngleMultiplier = EyeAngle / LightAngle;

        float PenumbraSize = tPenumbraSize.Sample(gSamplerPoint, texCoord);
		
        float ScreenPenumbraSize = PenumbraSize * DistanceMultiplier * AngleMultiplier;

        float filter_radius_sample_count = FILTER_RADIUS_HARDSHADOW;

        float2 step = (ScreenPenumbraSize / filter_radius_sample_count);

        step *= iLight.GetDirInEyeScreen_Vert();

        float2 dz_duv = DepthGradient(shadowCoord.xy, shadowCoord.z);

        ApplyFilter_FilterHardShadow_Vert(result.SoftShadows_Horz, texCoord, shadowCoord, ref_depth, step, PenumbraSize, ScreenPenumbraSize, dz_duv);

        return result;

    }

};


//class CGenerateShadowData_1_Horz : IGenerateShadowData_1
//{
//    pixelOut_1 CalcOutput( float2 texCoord, float ref_depth, float4 shadowCoord )
//    {
//        pixelOut_1 result;

//        float PenumbraSize = tPenumbraSize.Sample(gSamplerPoint, texCoord);

//        float2 dxy_diff = gScreenDXY / gShadowMapDXY;

//        float2 shadowCoord_ddx = ddx(shadowCoord.xy) / dxy_diff;
//        float2 shadowCoord_ddy = ddy(shadowCoord.xy) / dxy_diff;

//        float ddx_contribution = abs(dot(float2(1.0f, 0.0f), gLight2DDirInEye_horz));
//        float ddy_contribution = abs(dot(float2(0.0f, 1.0f), gLight2DDirInEye_horz));

//        float shadowDerivation = ddx_contribution * length(shadowCoord_ddx) + ddy_contribution * length(shadowCoord_ddy);

//        float penumbraSize_multiplier = shadowCoord.w / ref_depth;

//        float filter_radius_sample_count =  FILTER_RADIUS_HARDSHADOW;

//        float2 step = (PenumbraSize / filter_radius_sample_count);

//        float multiplier = clamp(gShadowMapDXY / shadowDerivation, 0.0f, 1.0f);

//        step *= multiplier;
//        step *= gLight2DDirInEye_horz;

//        ApplyFilter_FilterHardShadow(result.SoftShadows_Horz, texCoord, ref_depth, step);

//        return result;

//    }

//};

//class CGenerateShadowData_1_Vert : IGenerateShadowData_1
//{
//    pixelOut_1 CalcOutput( float2 texCoord, float ref_depth, float4 shadowCoord )
//    {
//        pixelOut_1 result;

//        float PenumbraSize = tPenumbraSize.Sample(gSamplerPoint, texCoord);

//        float2 dxy_diff = gScreenDXY / gShadowMapDXY;

//        float2 shadowCoord_ddx = ddx(shadowCoord.xy) / dxy_diff;
//        float2 shadowCoord_ddy = ddy(shadowCoord.xy) / dxy_diff;

//        float ddx_contribution = abs(dot(float2(1.0f, 0.0f), gLight2DDirInEye_vert));
//        float ddy_contribution = abs(dot(float2(0.0f, -1.0f), gLight2DDirInEye_vert));

//        float shadowDerivation = ddx_contribution * length(shadowCoord_ddx) + ddy_contribution * length(shadowCoord_ddy);

//        float penumbraSize_multiplier = shadowCoord.w / ref_depth;

//        float filter_radius_sample_count = FILTER_RADIUS_HARDSHADOW;

//        float2 step = (PenumbraSize / filter_radius_sample_count);

//        float multiplier = gShadowMapDXY / shadowDerivation;

//        step *=  multiplier;
//        step *= gLight2DDirInEye_vert;

//        ApplyFilter_FilterHardShadow(result.SoftShadows_Horz, texCoord, ref_depth, step);

//        return result;

//    }

//};

#endif