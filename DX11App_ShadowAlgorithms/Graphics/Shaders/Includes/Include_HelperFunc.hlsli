#ifndef HLSL_HELPERFUNC
#define HLSL_HELPERFUNC 


float ConvertToLinearDepth( float depth, float w, float z )
{
    return w / (depth - z);
}

float ConvertToLinearDepth_Orthographic( float depth, float w, float z )
{
    return (depth - w) / z;
}

float linstep( float min, float max, float v )
{
    return clamp((v - min) / (max - min), 0, 1);
}

float3 CalcWorldPos(float2 posH, float viewDepth, float4 PerspectiveValues, row_major float4x4 ViewInv)
{
	float4 posV;
	posV.xy = posH.xy * PerspectiveValues.xy * viewDepth;
	posV.z = viewDepth;
	posV.w = 1.0f;

	float4 posW = mul(posV, ViewInv);
	return posW.xyz;
}

float3 CalcViewPos(float2 posH, float depth, float4 PerspectiveValues)
{

	float viewDepth = ConvertToLinearDepth(depth, PerspectiveValues.w, PerspectiveValues.z);

	float4 posV;
	posV.xy = posH.xy * PerspectiveValues.xy * viewDepth;
	posV.z = viewDepth;
	posV.w = 1.0f;

	return posV.xyz;
}

float2 UVToPosH(float2 uv)
{
	return float2((uv.x - 0.5f) * 2.0f, (uv.y - 0.5f) * -2.0f);
}

float2 PosHToUV(float2 posH)
{
	return float2( (posH.x * 0.5) + 0.5f, (posH.y * 0.5f) - 0.5f);
}

//Generate Random number
float rand( float2 co )
{
    return frac(sin(dot(co.xy, float2(12.9898, 78.233))) * 43758.5453);

}

float rand2(float2 uv)
{
	float2 noise = (frac(sin(dot(uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
	return abs(noise.x + noise.y) * 0.5;
}

struct randGenerator_XORShift
{
	uint rand_lcg(uint rng_state)
	{
		// LCG values from Numerical Recipes
		rng_state = 1664525 * rng_state + 1013904223;
		return rng_state;
	}

	uint rand_xorshift(uint rng_state)
	{
		// Xorshift algorithm from George Marsaglia's paper
		rng_state ^= (rng_state << 13);
		rng_state ^= (rng_state >> 17);
		rng_state ^= (rng_state << 5);
		return rng_state;
	}

	float Generate(uint rng_state)
	{
		float f0 = float(rand_xorshift(rng_state)) * (1.0 / 4294967296.0);
	}
};


// Compute the filter size in pixels
float2 GetFilterSize( float2 dx, float2 dy, float2 TexSize )
{
    return 2 * (abs(dx) + abs(dy)) * TexSize;
}




// Compute the upper left and size of the filter tile
// MinFilterWidth, MaxSizeDerivatives and TexSize given in texels
// Rest of the parameters and returns are in normalized coordinates
// NOTE: Can provide an upper bound for the size (in texels) computed via derivatives.
// This is necessary since GPU's finite differencing screws up in some cases,
// returning rediculous sizes here. For operations that loop on the filter area
// this is a big problem...
float2 GetFilterTile( float2 tc, float2 dx, float2 dy, float2 TexSize, float2 TexelSize,
                     float2 MinFilterWidth, float2 MaxSizeDerivatives,
                     out float2 Size )
{
    // Compute the filter size based on derivatives
    float2 SizeDerivatives = min(GetFilterSize(dx, dy, TexSize),
                                 MaxSizeDerivatives);
    
    // Force an integer tile size (in pixels) so that bilinear weights are consistent
    Size = round(max(SizeDerivatives, MinFilterWidth)) * TexelSize;
    
    // Compute upper left corner of the tile
    return (tc - 0.5 * (Size - TexelSize));
}


// Returns coordinates for the four pixels surround a given fragment.
// Given and returned Coords are normalized
// These are given by (in Fetch4 order) - where "R" is the returned value:
//   - R + (1, 0)
//   - R + (0, 1)
//   - R + (1, 1)
//   - R
// Also returns bilinear weights in the output parameter.
float2 GetBilCoordsAndWeights( float2 Coords, float2 TexSize, float2 TexelSize, out float4 Weights )
{
    float2 TexelCoords = Coords * TexSize;
    
    // Compute weights
    Weights.xy = frac(TexelCoords + 0.5);
    Weights.zw = 1 - Weights.xy;
    Weights = Weights.xzxz * Weights.wyyw;
    
    // Compute upper-left pixel coordinates
    // NOTE: D3D texel alignment...
    return (floor(TexelCoords - 0.5) + 0.5) * TexelSize;
}

#endif