#ifndef HLSL_LIGHTS
#define HLSL_LIGHTS

//--------------------------------------------------------------------------------------
// PreProcessors
//--------------------------------------------------------------------------------------
#define MAX_LIGHT_SPOTLIGHT   10
#define MAX_LIGHT_DIRECTLIGHT 5

#if defined(__INTELLISENSE__)

#include "Include_HelperFunc.hlsli"

#define GLOBAL_AMBIENT float4(0.1f, 0.2f, 0.3f, 1.0f);

#else

#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"

#endif




//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------
interface ILight_Base
{
	void Lit(out float3 diffuse, out float3 ambient, out float3 specular, float3 normalW, float3 posW, float3 toEye, float specPow);
	float4 IlluminateDiffuse();
	float4 IlluminateAmbient();
	float4 IlluminateSpecular(float3 toEyeW, float3 lightVector, float3 normal, float specPow);
	float4 CalcShadowPos(float3 posW);
	float ConvertToLinearDepth(float depth);
	float ConvertToNormalizedLinearDepth(float4 shadowPos);

	bool GetCastShadow();
	int GetShadowMapID();
	float GetZNear();
	float GetZFar();
	float GetLightSize();

	float4 GetPerspectiveValues();

	float3 GetPosition();

	row_major matrix GetViewMatrix();
	row_major matrix GetViewProjMatrix();

	float2 GetDirInEyeScreen_Horz();
	float2 GetDirInEyeScreen_Vert();

};

interface ILightArray_Basic
{
	float4 IlluminateDiffuse(float3 posW, float3 normalW, int arrayIndex);
	float4 IlluminateAmbient(int arrayIndex);
	float4 IlluminateSpecular(float3 toEyeW, int arrayIndex);
	float4 CalcShadowPos(float3 posW, int arrayIndex);
	bool IsCastShadow(int arrayIndex);
	int GetShadowMapID(int arrayIndex);
	float GetZNear(int arrayIndex);
	float GetZFar(int arrayIndex);
	float GetLightSize(int arrayIndex);
	float4 GetPerspectiveValues(int arrayIndex);
	float3 GetPosition(int arrayIndex);
	float ConvertToLinearDepth(int arrayIndex, float depth);
	row_major matrix GetViewMatrix(int arrayIndex);
	row_major matrix GetViewProjMatrix(int arrayIndex);

};

//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class cLight_DirectLight : ILight_Base
{

	row_major matrix View;
	row_major matrix ViewProj;
	row_major matrix ShadowTransform;

	float4 PerspectiveValues;

	float4 Diffuse;
	float4 Ambient;
	float4 Specular;

	float3 Direction;
	float LightSize;

	float3 Position;
	float zNear;

	float2 DirInEyeScreen_Horz;
	float2 DirInEyeScreen_Vert;

	float zFar;
	bool CastShadow;
	int ID;
	int ShadowMapID;

	void Lit(out float3 DiffuseColor, out float3 AmbientColor, out float3 SpecularColor, float3 NormalW, float3 PosW, float3 ToEye, float SpecPow)
	{
		DiffuseColor = float3(0.0f, 0.0f, 0.0f);
		AmbientColor = float3(0.0f, 0.0f, 0.0f);
		SpecularColor = float3(0.0f, 0.0f, 0.0f);

		float3 LightVector = -Direction;

		float DiffuseFactor = dot(NormalW, LightVector);

		if (DiffuseFactor > 0.0f)
		{
			DiffuseColor = Diffuse * DiffuseFactor;
			SpecularColor = IlluminateSpecular(ToEye, LightVector, NormalW, SpecPow);
		}

		AmbientColor = Ambient;

	}

	float4 IlluminateDiffuse()
	{
		return Diffuse;
	}

	float4 IlluminateAmbient()
	{
		return Ambient;
	}

	float4 IlluminateSpecular(float3 toEyeW, float3 nlightVector, float3 normal, float specPow)
	{
		float3 v = reflect(-nlightVector, normal);
		float specFactor = pow(max(dot(v, toEyeW), 0.0f), specPow);

		return float4(specFactor.xxx, 1.0f)* Diffuse;
	}

	float4 CalcShadowPos(float3 posW)
	{
		float4 sp = mul(float4(posW, 1.0f), ShadowTransform);
		return sp;
	}

	float ConvertToLinearDepth(float depth)
	{
		return (depth - PerspectiveValues.w) / PerspectiveValues.z;
	}

	float ConvertToNormalizedLinearDepth(float4 ShadowPos)
	{
		return ShadowPos.z;
	}

	bool GetCastShadow()
	{
		return CastShadow;
	}

	int GetShadowMapID()
	{
		return ShadowMapID;
	}

	float GetZNear()
	{
		return zNear;
	}

	float GetZFar()
	{
		return zFar;
	}

	float GetLightSize()
	{
		return LightSize;
	}

	float4 GetPerspectiveValues()
	{
		return PerspectiveValues;
	}

	float3 GetPosition()
	{
		return Position;
	}

	row_major matrix GetViewMatrix()
	{
		return View;
	}

	row_major matrix GetViewProjMatrix()
	{
		return ViewProj;
	}


	float2 GetDirInEyeScreen_Horz() {
		return DirInEyeScreen_Horz;
	}

	float2 GetDirInEyeScreen_Vert() {
		return DirInEyeScreen_Vert;
	}
};

class cLight_SpotLight : ILight_Base
{
	row_major matrix View;
	row_major matrix ViewProj;
	row_major matrix ShadowTransform;

	float4 PerspectiveValues;

	float4 Diffuse;
	float4 Ambient;
	float4 Specular;

	float3 Direction;
	float LightSize;

	float3 Position;
	float zNear;

	float2 DirInEyeScreen_Horz;
	float2 DirInEyeScreen_Vert;

	float zFar;
	bool CastShadow;
	int ID;
	int ShadowMapID;

	float3 Attenuation;
	float Range;

	float Spot;
	float3 pad_spotLight_1;

	void Lit(out float3 DiffuseColor, out float3 AmbientColor, out float3 SpecularColor, float3 NormalW, float3 PosW, float3 ToEye, float SpecPow)
	{
		DiffuseColor = float3(0.0f, 0.0f, 0.0f);
		AmbientColor = float3(0.0f, 0.0f, 0.0f);
		SpecularColor = float3(0.0f, 0.0f, 0.0f);

		float3 LightVector = Position - PosW;

		float Distance = length(LightVector);

		if (Distance < Range)
		{
			LightVector /= Distance;

			float DiffuseFactor = dot(NormalW, LightVector);

			if (DiffuseFactor > 0.0f)
			{
				DiffuseColor = Diffuse * DiffuseFactor;
				SpecularColor = IlluminateSpecular(ToEye, LightVector, NormalW, SpecPow);
			}

			float SpotFactor = pow(max(dot(-LightVector, Direction), 0.0f), Spot);

			AmbientColor = Ambient * SpotFactor;

			float AttenuationFactor = SpotFactor / dot(Attenuation, float3(1.0f, Distance, Distance * Distance));

			DiffuseColor *= AttenuationFactor;
			SpecularColor *= AttenuationFactor;
		}
	}

	float4 IlluminateDiffuse()
	{
		/*float4 D = float4(0.0f, 0.0f, 0.0f, 1.0f);

		[flatten]
		if (diffuseFactor > 0.0f)
		{
			D = diffuseFactor * Diffuse;

			float spot = pow(max(dot(-nlightVector, Direction), 0.0f), Spot);

			float attenuation = spot / dot(Attenuation, float3(1.0f, distance, distance * distance));

			D *= attenuation;

		}*/

		return Diffuse;
	}

	float4 IlluminateAmbient()
	{
		return Ambient;
	}

	float4 IlluminateSpecular(float3 toEyeW, float3 nlightVector, float3 normal, float specPow)
	{
		float3 v = reflect(-nlightVector, normal);
		float specFactor = pow(max(dot(v, toEyeW), 0.0f), specPow);

		return float4(specFactor.xxx, 1.0f)* Diffuse;
	}

	float4 CalcShadowPos(float3 posW)
	{
		float4 sp = mul(float4(posW, 1.0f), ShadowTransform);
		sp.xyz /= sp.w;
		return sp;
	}

	float ConvertToLinearDepth(float depth)
	{
		float w = PerspectiveValues.w;
		float z = PerspectiveValues.z;
		return w / (depth - z);
	}

	float ConvertToNormalizedLinearDepth(float4 ShadowPos)
	{
		return linstep(zNear, zFar, ShadowPos.w);
	}

	bool GetCastShadow()
	{
		return CastShadow;
	}

	int GetShadowMapID()
	{
		return ShadowMapID;
	}

	float GetZNear()
	{
		return zNear;
	}

	float GetZFar()
	{
		return zFar;
	}

	float GetLightSize()
	{
		return LightSize;
	}

	float4 GetPerspectiveValues()
	{
		return PerspectiveValues;
	}

	float3 GetPosition()
	{
		return Position;
	}

	row_major matrix GetViewMatrix()
	{
		return View;
	}

	row_major matrix GetViewProjMatrix()
	{
		return ViewProj;
	}

	float2 GetDirInEyeScreen_Horz() {
		return DirInEyeScreen_Horz;
	}

	float2 GetDirInEyeScreen_Vert() {
		return DirInEyeScreen_Vert;
	}

};

//--------------------------------------------------------------------------------------
// ArrayClasses
//--------------------------------------------------------------------------------------

class cLight_Basic_Data
{
	row_major matrix View;
	row_major matrix ViewProj;
	row_major matrix ShadowTransform;

	float4 PerspectiveValues;

	float4 Diffuse;
	float4 Ambient;
	float4 Specular;

	float3 Direction;
	float LightSize;

	float3 Position;
	float zNear;

	float zFar;
	bool CastShadow;
	int ID;
	int ShadowMapID;

};


class cLight_SpotLight_Data : cLight_Basic_Data
{
	float3 Attenuation;
	float Range;

	float Spot;
	float3 pad_spotLight_1;
};


class cLight_DirectLight_Data : cLight_Basic_Data
{

};


class ILightArray_SpotLight_Basic : ILightArray_Basic
{
	cLight_SpotLight_Data spotLights[MAX_LIGHT_SPOTLIGHT];

	float4 IlluminateDiffuse(float3 posW, float3 normalW, int arrayIndex)
	{
		float4 D;

		float3 lightVector = spotLights[arrayIndex].Position - posW;

		float distance = length(lightVector);

		if (distance > spotLights[arrayIndex].Range)
		{
			return float4(0.0f, 0.0f, 0.0f, 1.0f);
		}

		lightVector /= distance;

		float diffuseFactor = dot(normalW, lightVector);

		[flatten]
		if (diffuseFactor > 0.0f)
		{
			D = diffuseFactor * spotLights[arrayIndex].Diffuse;
		}

		float spot = pow(max(dot(-lightVector, spotLights[arrayIndex].Direction), 0.0f), spotLights[arrayIndex].Spot);

		float attenuation = spot / dot(spotLights[arrayIndex].Attenuation, float3(1.0f, distance, distance * distance));

		D *= attenuation;


		return D;
	}

	float4 IlluminateAmbient(int arrayIndex)
	{
		return float4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	float4 IlluminateSpecular(float3 toEyeW, int arrayIndex)
	{
		return float4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	float4 CalcShadowPos(float3 posW, int arrayIndex)
	{
		float4 sp = mul(float4(posW, 1.0f), spotLights[arrayIndex].ShadowTransform);
		sp.xyz /= sp.w;
		return sp;
	}

	bool IsCastShadow(int arrayIndex)
	{
		return spotLights[arrayIndex].CastShadow;

	}

	int GetShadowMapID(int arrayIndex)
	{
		return spotLights[arrayIndex].ShadowMapID;

	}

	float GetZNear(int arrayIndex)
	{
		return spotLights[arrayIndex].zNear;
	}

	float GetZFar(int arrayIndex)
	{
		return spotLights[arrayIndex].zFar;
	}

	float GetLightSize(int arrayIndex)
	{
		return spotLights[arrayIndex].LightSize;
	}

	float4 GetPerspectiveValues(int arrayIndex)
	{
		return spotLights[arrayIndex].PerspectiveValues;
	}

	float3 GetPosition(int arrayIndex)
	{
		return spotLights[arrayIndex].Position;
	}

	float ConvertToLinearDepth(int arrayIndex, float depth)
	{
		float4 pvalues = GetPerspectiveValues(arrayIndex);

		return pvalues.w / (depth - pvalues.z);

	}

	row_major matrix GetViewMatrix(int arrayIndex)
	{
		return spotLights[arrayIndex].View;
	}

	row_major matrix GetViewProjMatrix(int arrayIndex)
	{
		return spotLights[arrayIndex].ViewProj;
	}
};


class ILightArray_DirectLight_Basic : ILightArray_Basic
{
	cLight_DirectLight_Data directLights[MAX_LIGHT_DIRECTLIGHT];

	float4 IlluminateDiffuse(float3 posW, float3 normalW, int arrayIndex)
	{
		return directLights[arrayIndex].Diffuse;
	}

	float4 IlluminateAmbient(int arrayIndex)
	{
		return float4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	float4 IlluminateSpecular(float3 toEyeW, int arrayIndex)
	{
		return float4(0.0f, 0.0f, 0.0f, 1.0f);
	}

	float4 CalcShadowPos(float3 posW, int arrayIndex)
	{
		float4 sp = mul(float4(posW, 1.0f), directLights[arrayIndex].ShadowTransform);
		sp.xyz /= sp.w;
		return sp;
	}

	bool IsCastShadow(int arrayIndex)
	{

		return directLights[arrayIndex].CastShadow;

	}

	int GetShadowMapID(int arrayIndex)
	{

		return directLights[arrayIndex].ShadowMapID;

	}

	float GetZNear(int arrayIndex)
	{
		return directLights[arrayIndex].zNear;
	}

	float GetZFar(int arrayIndex)
	{
		return directLights[arrayIndex].zFar;
	}

	float GetLightSize(int arrayIndex)
	{
		return directLights[arrayIndex].LightSize;
	}

	float4 GetPerspectiveValues(int arrayIndex)
	{
		return directLights[arrayIndex].PerspectiveValues;
	}

	float3 GetPosition(int arrayIndex)
	{
		return directLights[arrayIndex].Position;
	}

	float ConvertToLinearDepth(int arrayIndex, float depth)
	{
		float4 pvalues = GetPerspectiveValues(arrayIndex);

		return (depth - pvalues.w) / pvalues.z;

	}

	row_major matrix GetViewMatrix(int arrayIndex)
	{
		return directLights[arrayIndex].View;
	}

	row_major matrix GetViewProjMatrix(int arrayIndex)
	{
		return directLights[arrayIndex].ViewProj;
	}
};

#endif