
#if defined(__INTELLISENSE__)

#include "../Includes/Include_CommonConstantBuffer.hlsli"
#include "../Includes/Include_Samplers.hlsli"
#include "../Includes/Include_HelperFunc.hlsli"

#else

#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"

#endif



static const row_major float4x4 gProjToTex = { 0.5f, 0.0f, 0.0f, 0.0f,
	0.0f, -0.5, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.5f, 0.5f, 0.0f, 1.0f };

Texture2D  tRandVector : register(t0);
Texture2D<float> tDepthMap : register(t1);
Texture2D<float3> tNormalMap : register(t2);


interface ISSAO
{
	float Calculate_SSAO(float2 uv, float2 csPos);
};

ISSAO gISSAO;


class SSAO_Default : ISSAO
{

	float3 CalcWorldPos(float2 posH, float viewDepth, out float4 posV)
	{
		posV.xy = posH.xy * gPerspectiveValues.xy * viewDepth;
		posV.z = viewDepth;
		posV.w = 1.0f;

		float4 posW = mul(posV, gViewInv);
		return posW.xyz;
	}

	// Determines how much the sample point q occludes the point p as a function
	// of distZ.
	float OcclusionFunction(float distZ)
	{
		//
		// If depth(q) is "behind" depth(p), then q cannot occlude p.  Moreover, if 
		// depth(q) and depth(p) are sufficiently close, then we also assume q cannot
		// occlude p because q needs to be in front of p by Epsilon to occlude p.
		//
		// We use the following function to determine the occlusion.  
		// 
		//
		//       1.0     -------------\
		//               |           |  \
		//               |           |    \
		//               |           |      \ 
		//               |           |        \
		//               |           |          \
		//               |           |            \
		//  ------|------|-----------|-------------|---------|--> zv
		//        0     Eps          z0            z1        
		//

		float occlusion = 0.0f;
		if (distZ > gSurfaceEpsilon)
		{
			float fadeLength = gOcclusionFadeEnd - gOcclusionFadeStart;

			// Linearly decrease occlusion from 1 to 0 as distZ goes 
			// from gOcclusionFadeStart to gOcclusionFadeEnd.	
			occlusion = saturate((gOcclusionFadeEnd - distZ) / fadeLength);
		}

		return occlusion;
	}


	float Calculate_SSAO(float2 uv, float2 csPos /*, float3 NormalW, float4 PosW, Texture2D tDepthMap, Texture2D tRandVector*/ )
	{

		float3 NormalW = tNormalMap.Sample(gSamplerPoint, uv).xyz;

		NormalW = normalize(NormalW * 2.0f - 1.0f);

		float depth = tDepthMap.Sample(gSamplerPoint, uv);

		float viewDepth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);

		float4 p; //= mul(PosW, gView);

		float3 PosW = CalcWorldPos(csPos, viewDepth, p);

		float3 n = mul(NormalW, (float3x3)gView);


		// Extract random vector and map from [0,1] --> [-1, +1].
		float3 randVec = 2.0f * tRandVector.SampleLevel(gSamplerPoint, 4.0f * uv, 0.0f).rgb - 1.0f;


		float occlusionSum = 0.0f;

		// Sample neighboring points about p in the hemisphere oriented by n.
		[unroll]
		for (int i = 0; i < 14; ++i)
		{
			// offset vectors are fixed and uniformly distributed (so that our offset vectors
			// do not clump in the same direction).  If we reflect them about a random vector
			// then we get a random uniform distribution of offset vectors.
			float3 offset = reflect(gOffsetVectors[i].xyz, randVec);

			// Flip offset vector if it is behind the plane defined by (p, n).
			float flip = sign(dot(offset, n));

			// Sample a point near p within the occlusion radius.
			float3 q = p + flip * gOcclusionRadius * offset;

			// Project q and generate projective tex-coords.  
			//float4 projCoord = mul(float4(q, 1.0f), gProj);
			//projCoord /= projCoord.w;
			//float4 projQ = mul(projCoord, gProjToTex);
			float4 projQ = mul(float4(q, 1.0f), gProj);
			projQ /= projQ.w;

			projQ = mul(projQ, gProjToTex);

			// Find the nearest depth value along the ray from the eye to q (this is not
			// the depth of q, as q is just an arbitrary point near p and might
			// occupy empty space).  To find the nearest depth we look it up in the depthmap.

			float rz = tDepthMap.SampleLevel(gSamplerPoint_BorderColor_1, projQ.xy, 0.0f).x;

			// Reconstruct full view space position r = (rx,ry,rz).  We know r
			// lies on the ray of q, so there exists a t such that r = t*q.
			// r.z = t*q.z ==> t = r.z / q.z

			float3 r = CalcViewPos(float2((projQ.x - 0.5f) * 2.0f, (projQ.y - 0.5f) * -2.0f), rz, gPerspectiveValues);

			//
			// Test whether r occludes p.
			//   * The product dot(n, normalize(r - p)) measures how much in front
			//     of the plane(p,n) the occluder point r is.  The more in front it is, the
			//     more occlusion weight we give it.  This also prevents self shadowing where 
			//     a point r on an angled plane (p,n) could give a false occlusion since they
			//     have different depth values with respect to the eye.
			//   * The weight of the occlusion is scaled based on how far the occluder is from
			//     the point we are computing the occlusion of.  If the occluder r is far away
			//     from p, then it does not occlude it.
			// 

			float distZ = p.z - r.z;
			float dp = max(dot(n, normalize(r - p)), 0.0f);
			float occlusion = dp * OcclusionFunction(distZ);

			occlusionSum += occlusion;
		}

		occlusionSum /= 14.0f;

		float access = 1.0f - occlusionSum;

		return saturate(pow(access, 4.0f));
	}
};