#ifndef HLSL_SHADOWALGORITHMS
#define HLSL_SHADOWALGORITHMS

#if defined(__INTELLISENSE__)
    #include "Include_Samplers.hlsli"
    #include "Include_Lights.hlsli"
    #include "Include_HelperFunc.hlsli"
    #include "Include_SAVSM.hlsli"
    #include "Include_MomentShadowMap.hlsli"
    #include "Include_PoissonDisks.hlsli"
#else
#include "Graphics/Shaders/Includes/Include_Lights.hlsli"
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli"
#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"
#include "Graphics/Shaders/Includes/Include_SAVSM.hlsli"
#include "Graphics/Shaders/Includes/Include_MomentShadowMap.hlsli"
#include "Graphics/Shaders/Includes/Include_PoissonDisks.hlsli"
#endif


//--------------------------------------------------------------------------------------
// Preprocessors
//--------------------------------------------------------------------------------------
#if defined(__INTELLISENSE__)

#define PCF_SAMPLE_COUNT 5
#define PCSS_SAMPLE_COUNT 25
#define PCSS_POISSONDISK Poisson25
#define BLOCKER_SEARCH_SAMPLE_COUNT 25
#define BLOCKER_SEARCH_POISSONDISK Poisson25

#endif


//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------
interface IShadowManager
{
    float CalcShadowFactor( ILight_Base iLight, float2 texCoord, float4 shadowPos, Texture2DArray tShadowMapArray );

};


//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CShadowManager_BasicShadowMap : IShadowManager
{

    float CalcShadowFactor( ILight_Base iLight, float2 texCoord, float4 shadowPos, Texture2DArray tShadowMapArray )
    {
        return step(shadowPos.z, tShadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, float3(shadowPos.xy, iLight.GetShadowMapID()), 0).x);
    }
    
};

//class CShadowManager_BasicCascadedShadowMap : IShadowManager
//{

//    float CalcShadowFactor(ILight_Base iLight, float2 texCoord, float4 shadowPos, Texture2DArray tShadowMapArray)
//    {


//        return step(shadowPos.z, tShadowMapArray.SampleLevel(gSamplerPoint_BorderColor_1, float3(shadowPos.xy, iLight.GetShadowMapID()), 0).x);
//    }
    
//};



class CShadowManager_PCF : IShadowManager
{

    float CalcShadowFactor( ILight_Base iLight, float2 texCoord, float4 shadowPos, Texture2DArray tShadowMapArray )
    {
        
        float sum = 0.0f;

        int radius = (PCF_SAMPLE_COUNT - 1) / 2;

        float2 offset;

        [unroll]
        for (int i = -radius; i <= radius; ++i)
        {
            [unroll]
            for (int j = -radius; j <= radius ; ++j)
            { 
                offset = gShadowMapDXY * float2(i, j);
                sum += tShadowMapArray.SampleCmpLevelZero(gSamplerDepth, float3(shadowPos.xy + offset, iLight.GetShadowMapID()), shadowPos.z).x;
            }
        }

        return (sum / (PCF_SAMPLE_COUNT * PCF_SAMPLE_COUNT));

    }
    

};

class CShadowManager_PCSS : IShadowManager
{

    float random( float2 p )
    {
        float2 K1 = float2(
        23.14069263277926, // e^pi (Gelfond's constant)
         2.665144142690225 // 2^sqrt(2) (Gelfond–Schneider constant)
    );
        return frac(cos(dot(p, K1)) * 12345.6789);
    }

    // returns random angle
    float randomAngle( float2 seed )
    {
        return abs(random(seed)) * 6.283285;
    }

    // Derivatives of light-space depth with respect to texture coordinates
    float2 DepthGradient( float2 uv, float z )
    {
        float2 dz_duv = 0;

	// Packing derivatives of u,v, and distance to light source w.r.t. screen space x, and y
        float3 duvdist_dx = ddx(float3(uv, z));
        float3 duvdist_dy = ddy(float3(uv, z));

	// Top row of 2x2
        dz_duv.x = duvdist_dy.y * duvdist_dx.z;
        dz_duv.x -= duvdist_dx.y * duvdist_dy.z;
	
	// Bottom row of 2x2
        dz_duv.y = duvdist_dx.x * duvdist_dy.z;
        dz_duv.y -= duvdist_dy.x * duvdist_dx.z;

	// Multiply ddist/dx and ddist/dy by inverse transpose of Jacobian
        float det = (duvdist_dx.x * duvdist_dy.y) - (duvdist_dx.y * duvdist_dy.x);
        dz_duv /= det;

        return dz_duv;

    }


    // Using similar triangles from the surface point to the area light
    float2 SearchRegionRadiusUV( float zLight, float zNear, float lightSize )
    {

        return lightSize * (zLight - zNear) / zLight;

    }


    // Computes depth offset (bias)
    float BiasedZ( float z0, float2 dz_duv, float2 offset )
    {

        return z0 + dot(dz_duv, offset);

    }


    float2 PenumbraRadiusUV( float zReceiver, float zBlocker, float lightSize )
    {

        return lightSize * (zReceiver - zBlocker) / zBlocker;
        
    }


    //void FindBlocker( out float avgBlockerDepth,
				//out float numBlockers,
				//float2 uv,
				//float z0,
				//float2 dz_duv,
				//float2 searchRegionRadiusUV,
    //            int shadowMapID,
    //            Texture2DArray tShadowMapArray )
    //{

    //    float blockerSum = 0;
    //    numBlockers = 0;

    //    float2 sample_dxy = searchRegionRadiusUV / BLOCKER_SEARCH_SAMPLE_COUNT;

    //    [unroll]
    //    for (int i = -(BLOCKER_SEARCH_SAMPLE_COUNT - 1) / 2; i <= (BLOCKER_SEARCH_SAMPLE_COUNT - 1) / 2; ++i)
    //    {

    //        [unroll]
    //        for (int j = -(BLOCKER_SEARCH_SAMPLE_COUNT - 1) / 2; j <= (BLOCKER_SEARCH_SAMPLE_COUNT - 1) / 2; ++j)
    //        {

    //            float2 offset = sample_dxy * float2(i, j);

    //            float shadowMapDepth = tShadowMapArray.SampleLevel(gSamplerPoint, float3(uv + offset, shadowMapID), 0);

    //            float z = BiasedZ(z0, dz_duv, offset);

		  //      // averages depth values that are closer to the light than the receiving point {average blockers only}
    //            if (shadowMapDepth < z)
    //            {
    //                blockerSum += shadowMapDepth;
    //                numBlockers++;
    //            }

    //        }

    //    }

    //    avgBlockerDepth = blockerSum / numBlockers;

    //}

    void FindBlocker( out float avgBlockerDepth,
				out float numBlockers,
				float2 uv,
				float z0,
				float2 dz_duv,
				float2 searchRegionRadiusUV,
                int shadowMapID,
                Texture2DArray tShadowMapArray )
    {

        float blockerSum = 0;
        numBlockers = 0;

        [unroll]
        for (int i = 0; i < BLOCKER_SEARCH_SAMPLE_COUNT; ++i)
        {

            float2 offset = searchRegionRadiusUV * BLOCKER_SEARCH_POISSONDISK[i];

            float shadowMapDepth = tShadowMapArray.SampleLevel(gSamplerPoint, float3(uv + offset, shadowMapID), 0);

            float z = BiasedZ(z0, dz_duv, offset);

		        // averages depth values that are closer to the light than the receiving point {average blockers only}
            if (shadowMapDepth < z)
            {
                blockerSum += shadowMapDepth;
                numBlockers++;
            }

        }

        avgBlockerDepth = blockerSum / numBlockers;

    }


    // Performs PCF filtering on the shadow map using multiple taps in the filter region.
    float PCF_Filter( float2 uv, float z0, float2 dz_duv, float2 filterRadiusUV, int shadowMapID, Texture2DArray tShadowMapArray )
    {
        float sum = 0;
	
        float2 sample_dxy = filterRadiusUV / PCSS_SAMPLE_COUNT;

        float angle = randomAngle(uv);
        float s = sin(angle);
        float c = cos(angle);

        [unroll]
        for (int i = 0; i < PCSS_SAMPLE_COUNT; ++i)
        {
            float2 offset = filterRadiusUV * PCSS_POISSONDISK[i];
            offset = float2(offset.x * c + offset.y * s, offset.x * -s + offset.y * c);
            float z = BiasedZ(z0, dz_duv, offset);
            sum += tShadowMapArray.SampleCmpLevelZero(gSamplerDepth, float3(uv + offset, shadowMapID), z);
        }
        return sum / PCSS_SAMPLE_COUNT;

    }


    float CalcShadowFactor( ILight_Base iLight, float2 texCoord, float4 shadowPos, Texture2DArray tShadowMapArray )
    {

        float avgBlockerDepth, blockerNum;

        float2 dz_duv = DepthGradient(shadowPos.xy, shadowPos.z);

        float linearDepth = iLight.ConvertToLinearDepth(shadowPos.z);

        float2 searchRadius = SearchRegionRadiusUV(linearDepth, iLight.GetZNear(), iLight.GetLightSize());

        FindBlocker(avgBlockerDepth, blockerNum, shadowPos.xy, shadowPos.z, dz_duv, searchRadius, iLight.GetShadowMapID(), tShadowMapArray);

        if (blockerNum == 0)
            return 1.0f;

        avgBlockerDepth = iLight.ConvertToLinearDepth(avgBlockerDepth); //ConvertToLinearDepth(avgBlockerDepth, lightPerspectiveValues.w, lightPerspectiveValues.z);

        float filterRadius = PenumbraRadiusUV(linearDepth, avgBlockerDepth, iLight.GetLightSize());

        return PCF_Filter(shadowPos.xy, shadowPos.z, dz_duv, filterRadius, iLight.GetShadowMapID(), tShadowMapArray);

    }
    
};

class CShadowManager_VSM : IShadowManager
{
    float CalcShadowFactor( ILight_Base iLight, float2 texCoord, float4 shadowPos, Texture2DArray tShadowMapArray )
    {

        float2 sample = tShadowMapArray.SampleLevel(gSamplerLinear_Border_1, float3(shadowPos.xy,iLight.GetShadowMapID()), 0.0f).xy;

        //float linearDepth = iLight.ConvertToLinearDepth(shadowPos.z);

        //float fragDepth = linearDepth;

        //float hshadow = smoothstep(fragDepth, fragDepth + 100, sample.x);

        float normalizedLinearFragDepth = iLight.ConvertToNormalizedLinearDepth(shadowPos);

        float E_x2 = sample.y;
        float Ex_2 = sample.x * sample.x;
        float variance = E_x2 - Ex_2;

        float md = normalizedLinearFragDepth - sample.x;

        float p = variance / (variance + md * md);

        return max(p, normalizedLinearFragDepth <= sample.x);

    }
};

class CShadowManager_SAVSM : IShadowManager
{

    float CalcShadowFactor( ILight_Base iLight, float2 texCoord, float4 shadowPos, Texture2DArray tShadowMapArray )
    {

        float3 tc = shadowPos.xyz;

        float2 tc_dx = ddx(tc.xy);
        float2 tc_dy = ddy(tc.xy);

        float NormalizedDistance = iLight.ConvertToNormalizedLinearDepth(shadowPos);

        float ShadowFactor = CalcSAVSMShadowFactor(tShadowMapArray, iLight.GetShadowMapID(), shadowPos.xy, tc_dx, tc_dy, NormalizedDistance, 5.0f, 21.0f, 0.0001f);

        return ShadowFactor;

    }

};

class CShadowManager_MomentShadowMap : IShadowManager
{
    Texture2DArray<uint4> SATTexture;

    float CalcShadowFactor( ILight_Base iLight, float2 texCoord, float4 shadowPos, Texture2DArray tShadowMapArray )
    {

        float3 tc = float3(shadowPos.xy, iLight.GetShadowMapID());

        //float NormalizedDistance = linstep(zNear, zFar, shadowPos.w);

        //float2 searchRadius;

        //GetBlockerSearchAreaSize(searchRadius, lightSize, shadowPos.w, zNear);

        float2 leftTop, bottomRight;

        GetBlockerSearchRectangle(leftTop, bottomRight, tc.xy, gShadowMapDXY);

        float4 AvgValue;

        ComputeIntegerRectangleAverage_uint4(AvgValue, leftTop, bottomRight, SATTexture, iLight.GetShadowMapID(), gShadowMapSize, gFixedPrecision_Float32ToInt32);

        float4 Biased4Moment;

        Convert4MomentOptimizedToCanonical(Biased4Moment, AvgValue);

        float AvgBlockerDepth, BlockerSearchShadowIntensity;

        //float FragDepth = shadowPos.z * 2.0f - 1.0f;
        float FragDepth = iLight.ConvertToNormalizedLinearDepth(shadowPos);

        FragDepth = FragDepth * 2.0f - 1.0;

        Compute4MomentAverageBlockerDepth(AvgBlockerDepth, BlockerSearchShadowIntensity, Biased4Moment, FragDepth);

        float2 kernelSize;
        float DepthBias;

        //float linearAvgBlockerDepth = lerp(iLight.GetZNear(), iLight.GetZFar(), AvgBlockerDepth); //iLight.ConvertToLinearDepth(AvgBlockerDepth); //ConvertToLinearDepth(AvgBlockerDepth, lightPerspectiveValues.w, lightPerspectiveValues.z);

        //float linearDepth = iLight.ConvertToLinearDepth(shadowPos.z);

        EstimatePenumbraSize(kernelSize, DepthBias, AvgBlockerDepth, shadowPos.z, iLight.GetLightSize());

        float2 FilterTopLeft, FilterBottomRight;

        GetShadowFilterRectangle(FilterTopLeft, FilterBottomRight, tc.xy, kernelSize);


        ComputeRectangleAverage_uint4(AvgValue, FilterTopLeft, FilterBottomRight, SATTexture, iLight.GetShadowMapID(), gShadowMapSize, gShadowMapDXY, gFixedPrecision_Float32ToInt32);

        Convert4MomentOptimizedToCanonical(Biased4Moment, AvgValue);

        float ShadowIntensity_0;

        Compute4MomentUnboundedShadowIntensity(ShadowIntensity_0, Biased4Moment, FragDepth, DepthBias);

        float ShadowIntensity_1;

        PickShadowIntensity(ShadowIntensity_1, ShadowIntensity_0, BlockerSearchShadowIntensity);

        float ShadowFactor;

        ScaleShadowIntensity(ShadowFactor, ShadowIntensity_1, tc.xy, FragDepth);

        return (1.0f - ShadowFactor);

    }
    
    //float CalcShadowFactor( float4 shadowPos, float4 lightPerspectiveValues, float ShadowMapID, float zNear, float zFar, float lightSize, Texture2DArray tMSMTextureArray )
    //{

    //    float3 tc = float3(shadowPos.xy, ShadowMapID);

    //    //float NormalizedDistance = linstep(zNear, zFar, shadowPos.w);

    //    float4 Biased4Moments = Sample4MomentOptimizedShadowMap(tMSMTextureArray, tc);

    //    float ShadowFactor;

    //    float FragDepth = shadowPos.z * 2.0f - 1.0f;

    //    Compute4MomentUnboundedShadowIntensity(ShadowFactor, Biased4Moments, FragDepth, 0.0006f);

    //    return (1.0f + ShadowFactor * -1.0f);

    //}

};

class CShadowManager_OSSSS : IShadowManager
{

    float CalcShadowFactor( ILight_Base iLight, float2 texCoord, float4 shadowPos, Texture2DArray tShadowMapArray )
    {
        return tShadowMapArray.SampleLevel(gSamplerLinear, float3(texCoord, iLight.GetShadowMapID()), 0.0f).x;
    }
    
};

#endif