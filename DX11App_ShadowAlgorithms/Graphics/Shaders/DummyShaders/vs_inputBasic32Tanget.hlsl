struct vertexIn
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
	float2 Tex     : TEXCOORD0;
	float3 Tangent : TEXCOORD1;
};

float4 main(vertexIn vin) : SV_POSITION
{
	return float4(vin.PosL, 1.0f);
}