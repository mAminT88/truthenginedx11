struct vertexIn
{
	float3 PosL : POSITION;
	float2 Size : SIZE;
};

float4 main( vertexIn vin ) : SV_POSITION
{
	return float4(vin.PosL, 1.0f);
}