
struct vertexIn
{
	float3 PosL : POSITION;
};

float4 main( vertexIn vin ) : SV_POSITION
{
	return float4(vin.PosL, 1.0f);
}