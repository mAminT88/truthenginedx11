struct vertexIn
{
	float3 InitialPos : InitialPosW;
	float3 initialVel : InitialVelW;
	float2 Size       : SizeW;
	float  Age        : Age;
	uint Type         : Type;
};

float4 main( vertexIn vin ) : SV_POSITION
{
	return float4(vin.InitialPos, 1.0f);
}