struct vertexIn
{
	float3 PosL    : POSITION;
	float3 NormalL : NORMAL;
	float2 Tex     : TEXCOORD;
};

float4 main( vertexIn vin ) : SV_POSITION
{
	return float4(vin.PosL, 1.0f);
}