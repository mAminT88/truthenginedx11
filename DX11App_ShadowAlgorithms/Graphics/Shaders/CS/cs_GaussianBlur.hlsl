#if defined(__INTELLISENSE__)

#include "Include_cs_GaussianBlur.hlsli"


#else

#include "Graphics/Shaders/CS/Include_cs_GaussianBlur.hlsli"


#endif


[numthreads(GROUP_SIZE_X, 1, 1)]
void GaussianBlur(uint3 Gid : SV_GroupID, uint GI : SV_GroupIndex)
{
	gIFilter.Apply1DFilter(Gid, GI);
}


