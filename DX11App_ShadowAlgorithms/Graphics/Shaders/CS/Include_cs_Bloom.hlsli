
#if defined(__INTELLISENSE__)

#include "../Includes/Include_CommonConstantBuffer.hlsli"

#define GroupSizeX 1024

#else

#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"


#endif

static const float4 LUM_FACTOR = float4(0.299, 0.587, 0.114, 0);

Texture2D<float4> tDownScaledHDR : register(t0);
StructuredBuffer<float> tAvgLum : register(t1);

RWTexture2D<float4> tBloom : register(u0);

