//cbuffer cb_cs_findblocker_perFrame : register(b0)
//{
//    row_major matrix gViewInv;
//
//    float4 gEyePerspectiveValues; gPerspectiveValues
//
//    float3 gEyePosW; gEyePos
//    float cb_cs_findblocker_perFrame_pad0;
//};
//
//cbuffer cb_cs_unfreq : register(b1)
//{
//    float4 gScreenSize_DXY : packoffset(c0); gScreenDXY
//
//    float4 gShadowMapSize_DXY : packoffset(c1); gShadowMapDXY
//
//    float gFixedPrecision_FloatToInt : packoffset(c2.x); gFixedPrecision_Float32ToInt32
//    float3 cb_cs_unfreq_pad0 : packoffset(c2.y);
//};