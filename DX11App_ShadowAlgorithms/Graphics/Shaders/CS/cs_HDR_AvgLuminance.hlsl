
#if defined(__INTELLISENSE__)

#include "Include_cs_HDR_AvgLuminance.hlsli"

#define FirstPass_ThreadGroupSize_X 1024
#define SecondPass_ThreadGroupSize_X 64
#define ThreadGroupSize_Y 1

#else

#include "Graphics/Shaders/CS/Include_cs_HDR_AvgLuminance.hlsli"


#endif

[numthreads(FirstPass_ThreadGroupSize_X, 1, 1)]
void DownScaleFirstPass(uint3 dispatchThreadId : SV_DispatchThreadID, uint3 groupThreadId : SV_GroupThreadID, uint3 groupId : SV_GroupID)
{
	uint2 CurPixel = uint2(dispatchThreadId.x % gRes.x, dispatchThreadId.x / gRes.x);

	// Reduce a group of 16 pixels to a single pixel and store in the shared memory
	float avgLum = DownScale4x4(CurPixel, groupThreadId.x);

	// Down scale from 1024 to 4
	avgLum = DownScale1024to4(dispatchThreadId.x, groupThreadId.x, avgLum);

	// Downscale from 4 to 1
	DownScale4to1(dispatchThreadId.x, groupThreadId.x, groupId.x, avgLum);
}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Second pass - convert the 1D average values into a single value
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//#define MAX_GROUPS 64

// Group shared memory to store the intermidiate results
groupshared float SharedAvgFinal[SecondPass_ThreadGroupSize_X];

[numthreads(SecondPass_ThreadGroupSize_X, 1, 1)]
void DownScaleSecondPass(uint3 groupId : SV_GroupID, uint3 groupThreadId : SV_GroupThreadID,
	uint3 dispatchThreadId : SV_DispatchThreadID, uint groupIndex : SV_GroupIndex)
{
	// Fill the shared memory with the 1D values
	float avgLum = 0.0;
	if (dispatchThreadId.x < gGroupSize)
	{
		avgLum = AverageValues1D[dispatchThreadId.x];
	}
	SharedAvgFinal[dispatchThreadId.x] = avgLum;

	GroupMemoryBarrierWithGroupSync(); // Sync before next step

	// Downscale from 64 to 16
	if (dispatchThreadId.x % 4 == 0)
	{
		// Calculate the luminance sum for this step
		float stepAvgLum = avgLum;
		stepAvgLum += dispatchThreadId.x + 1 < gGroupSize ? SharedAvgFinal[dispatchThreadId.x + 1] : avgLum;
		stepAvgLum += dispatchThreadId.x + 2 < gGroupSize ? SharedAvgFinal[dispatchThreadId.x + 2] : avgLum;
		stepAvgLum += dispatchThreadId.x + 3 < gGroupSize ? SharedAvgFinal[dispatchThreadId.x + 3] : avgLum;

		// Store the results
		avgLum = stepAvgLum;
		SharedAvgFinal[dispatchThreadId.x] = stepAvgLum;
	}

	GroupMemoryBarrierWithGroupSync(); // Sync before next step

	// Downscale from 16 to 4
	if (dispatchThreadId.x % 16 == 0)
	{
		// Calculate the luminance sum for this step
		float stepAvgLum = avgLum;
		stepAvgLum += dispatchThreadId.x + 4 < gGroupSize ? SharedAvgFinal[dispatchThreadId.x + 4] : avgLum;
		stepAvgLum += dispatchThreadId.x + 8 < gGroupSize ? SharedAvgFinal[dispatchThreadId.x + 8] : avgLum;
		stepAvgLum += dispatchThreadId.x + 12 < gGroupSize ? SharedAvgFinal[dispatchThreadId.x + 12] : avgLum;

		// Store the results
		avgLum = stepAvgLum;
		SharedAvgFinal[dispatchThreadId.x] = stepAvgLum;
	}

	GroupMemoryBarrierWithGroupSync(); // Sync before next step

	// Downscale from 4 to 1
	if (dispatchThreadId.x == 0)
	{
		// Calculate the average luminace
		float fFinalLumValue = avgLum;
		fFinalLumValue += dispatchThreadId.x + 16 < gGroupSize ? SharedAvgFinal[dispatchThreadId.x + 16] : avgLum;
		fFinalLumValue += dispatchThreadId.x + 32 < gGroupSize ? SharedAvgFinal[dispatchThreadId.x + 32] : avgLum;
		fFinalLumValue += dispatchThreadId.x + 48 < gGroupSize ? SharedAvgFinal[dispatchThreadId.x + 48] : avgLum;
		fFinalLumValue /= 64.0;

		// Store the final value
		//AverageLum[0] = max(fFinalLumValue, 0.0001);


		// Calculate the adaptive luminance
		float fAdaptedAverageLum = lerp(PrevAverageLum[0], fFinalLumValue,
			gLuminanceAdaptation);
		// Store the final value
		AverageLum[0] = max(fAdaptedAverageLum, 0.0001);

	}
}