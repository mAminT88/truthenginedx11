#if defined(__INTELLISENSE__)

   #include "../Includes/Include_CommonConstantBuffer.hlsli" 

#else

   #include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli" 

#endif

//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------

Texture2DArray<float4> tMomentsMapArray : register(t0);
Texture2DArray<uint4> tHorizontalSAT : register(t1);

RWTexture2DArray<uint4> tUAVOut : register(u1);

//--------------------------------------------------------------------------------------
// Constant Buffers
//--------------------------------------------------------------------------------------

cbuffer cb_cs_generateSAT : register(b5)
{
    int gShadowMapID;
    float3 cb_cs_generateSAT_pad0;
};

//--------------------------------------------------------------------------------------
// Functions
//--------------------------------------------------------------------------------------

void ApplyPrefixSumHorizontal_uint4( uint3 ThreadID )
{
    uint width = gShadowMapSize.x;

    uint4 PrefixSum = uint4(0, 0, 0, 0);
    for (uint i = 0; i < width; ++i)
    {
        PrefixSum += uint4(tMomentsMapArray.Load(uint4(i, ThreadID.y, gShadowMapID, 0)) * gFixedPrecision_Float32ToInt32.xxxx);
        tUAVOut[uint3(i, ThreadID.y, 0)] = PrefixSum;
    }
}

void ApplyPrefixSumVertical_uint4( uint3 ThreadID )
{
    uint Height = gShadowMapSize.y;
    uint4 PrefixSum = uint4(0, 0, 0, 0);
    for (uint i = 0; i < Height; ++i)
    {
        PrefixSum += tHorizontalSAT.Load(uint4(ThreadID.x, i, 0, 0));
        tUAVOut[uint3(ThreadID.x, i, gShadowMapID)] = PrefixSum;
    }
}

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface IApplyPrefixSum
{
    void ApplyPrefixSum( uint3 ThreadID );
};

//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CApplyPrefixSum_Horizontal : IApplyPrefixSum
{
    void ApplyPrefixSum( uint3 ThreadID )
    {

        ApplyPrefixSumHorizontal_uint4(ThreadID);

    }

};

class CApplyPrefixSum_Vertical : IApplyPrefixSum
{
    void ApplyPrefixSum( uint3 ThreadID )
    {

        ApplyPrefixSumVertical_uint4(ThreadID);

    }

};