
#if defined(__INTELLISENSE__)

#include "Include_cs_Bloom.hlsli"

#else

#include "Graphics/Shaders/CS/Include_cs_Bloom.hlsli"


#endif


[numthreads(GroupSizeX, 1, 1)]
void BloomReveal(uint3 dispatchThreadId : SV_DispatchThreadID)
{
	uint2 CurPixel = uint2(dispatchThreadId.x % gRes.x, dispatchThreadId.x / gRes.x);

	// Skip out of bound pixels
	if (CurPixel.y < gRes.y)
	{
		float4 color = tDownScaledHDR.Load(int3(CurPixel, 0));
		float Lum = dot(color, LUM_FACTOR);
		float avgLum = tAvgLum[0];

		// Find the color scale
		float colorScale = saturate(Lum - avgLum * gBloomThreshold);

		tBloom[CurPixel.xy] = color * colorScale;
	}
}
