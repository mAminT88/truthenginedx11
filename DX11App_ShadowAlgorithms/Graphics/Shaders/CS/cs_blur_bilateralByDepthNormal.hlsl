
#if defined(__INTELLISENSE__)

#include "Include_cs_blurBilateralByDepthNormal.hlsli"

#define ThreadGroupSize_X 256
#define ThreadGroupSize_Y 256

#else

#include "Graphics/Shaders/CS/Include_cs_blurBilateralByDepthNormal.hlsli"


#endif

Texture2D<float> tDepthMap : register(t0);
Texture2D<float> tNormalMap : register(t0);

RWTexture2D<float> tUAVOut : register(u1);

[numthreads(ThreadGroupSize_X, ThreadGroupSize_Y, 1)]
void main( uint3 DTid : SV_DispatchThreadID , uint3 GTid : SV_GroupThreadID)
{

}