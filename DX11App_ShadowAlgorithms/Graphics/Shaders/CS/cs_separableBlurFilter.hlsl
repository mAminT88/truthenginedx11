#if defined(__INTELLISENSE__)

    #include "Include_cs_separableBlurFilter.hlsli"

    #define ThreadGroupSize_X 256
    #define ThreadGroupSize_Y 256

#else

    #include "Graphics/Shaders/CS/Include_cs_separableBlurFilter.hlsli"


#endif

ISeparableBlur gISeparableBlue;

[numthreads(ThreadGroupSize_X, ThreadGroupSize_Y, 1)]
void main( uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GoupThreadID )
{
    int3 location = int3(DTid.xy, 0);

    float depth = tInput_Depth.Load(location);

    if(depth > 0.99999)
        return;

    
    float result = gISeparableBlue.ApplyBlur((int3)DTid, (int2)GTid.xy, depth);

    tUAVOut[DTid] = result;
}