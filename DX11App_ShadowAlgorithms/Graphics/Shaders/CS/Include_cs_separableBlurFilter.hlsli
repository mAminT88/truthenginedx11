#if defined(__INTELLISENSE__)

#include "Include_CommonConstantBuffer.hlsli" 
#include "../Includes/Include_Samplers.hlsli" 
#include "../Includes/Include_HelperFunc.hlsli"
#include "../Includes/Include_GaussianWeights.hlsli"


#define ThreadGroupSize_X 256
#define ThreadGroupSize_Y 256

#define FILTER_RADIUS_HARDSHADOW 5
#define GAUSSIAN_WEIGHTS_HARDSHADOW Gaussian11


#else

#include "Graphics/Shaders/CS/Include_CommonConstantBuffer.hlsli" 
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli" 
#include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"


#endif


//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------
Texture2D<float> tInput_Depth : register(t0);
Texture2D<float> tInput_Normal : register(t0);
Texture2D<float> tInput_ShadowBits : register(t1);
Texture2D<float> tInput_PenumbraSizes : register(t2);

RWTexture2DArray<float> tUAVOut : register(u1);


//--------------------------------------------------------------------------------------
// CBuffers
//--------------------------------------------------------------------------------------

cbuffer cb_cs_separableBlur_perLight : register(b1)
{
    row_major matrix gLightShadowTransform;
    row_major matrix gLightView;

    int gShadowMapID : packoffset(c1.x);
    float3 gLightPosW : packoffset(c1.y);

    float2 gLight2DDirInEye_Horz : packoffset(c2.x);
    float2 gLight2DDirInEye_Vert : packoffset(c2.z);
}


//--------------------------------------------------------------------------------------
// Functions
//--------------------------------------------------------------------------------------



float3 CalcPosW( float3 csPos, float linearDepth )
{
    float3 posV = float3(0.0f, 0.0f, 0.0f);
    posV.xy = csPos.xy * gPerspectiveValues.xy * linearDepth.xx;
    posV.z = linearDepth;
    
    return mul(float4(posV, 1.0), gViewInv).xyz;
}

void ApplyFilter_FilterHardShadow_Horz( out float SoftShadow, float2 tex_coord, float4 shadowCoord, float ref_depth, float2 step, float PenumbraSize, float ScreenPenumbraSize, float2 dz_duv /*, float Radius, float2 Dir, float LightDepth_Linear, float shadowCoordDerivation*/ )
{

    float weight_sum = 0.0f;

    float depthSample;
    
    float3 location = float3(tex_coord * gScreenSize.xy, 0);

    SoftShadow = tInput_ShadowBits.Load(location);

    if (ScreenPenumbraSize < gScreenDXY.x && ScreenPenumbraSize < gScreenDXY.y)
    {
        return;
    }

    SoftShadow *= GAUSSIAN_WEIGHTS_HARDSHADOW[0];

    weight_sum = GAUSSIAN_WEIGHTS_HARDSHADOW[0];
    
    float2 offset = float2(0.0f, 0.0f);

    step *= gScreenSize.xy;

    float2 uv;

    [unroll]
    for (int i = 1; i <= FILTER_RADIUS_HARDSHADOW; ++i)
    {

        offset.xy = step * i;

        location.xy += offset;

        //depthSample = tInput_Depth.Load(location);
        //float linearDepth = ConvertToLinearDepth(depthSample, gPerspectiveValues.w, gPerspectiveValues.z);

        //if (abs(ref_depth - linearDepth) < BILATERAL_DEPTH)
        //{

        SoftShadow += tInput_ShadowBits.Load(location) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

        weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];

        //}
        //else
        //{
        //    float sampleCnt = FILTER_RADIUS_HARDSHADOW;
        //    float shadowMapStep = PenumbraSize / sampleCnt;

        //    float3 shadowLocation = float3(shadowCoord.xy, gShadowMapID);

        //    float3 shadowMap_offset = (shadowMapStep * i, 0.0f, 0.0f);

        //    shadowLocation += shadowMap_offset;

        //    float z = BiasedZ(shadowCoord.z, dz_duv, shadowMap_offset.xy);

        //    SoftShadow += ShadowMapSampling_horz(z, shadowLocation) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

        //    weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];
        //}
        
        uv = tex_coord - offset;


        depthSample = tDepthMap.Sample(gSamplerLinear_Border_1, uv);
        linearDepth = ConvertToLinearDepth(depthSample, gPerspectiveValues.w, gPerspectiveValues.z);

        if (abs(ref_depth - linearDepth) < BILATERAL_DEPTH)
        {
            SoftShadow += tHardShadow.Sample(gSamplerLinear_Border_1, float3(uv, gShadowMapID)) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];
            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];
        }
        else
        {
            float sampleCnt = FILTER_RADIUS_HARDSHADOW;
            float shadowMapStep = PenumbraSize / sampleCnt;

            float3 shadowLocation = float3(shadowCoord.xy, gShadowMapID);

            float3 shadowMap_offset = (shadowMapStep * -i, 0.0f, 0.0f);

            shadowLocation += shadowMap_offset;

            float z = BiasedZ(shadowCoord.z, dz_duv, shadowMap_offset.xy);

            SoftShadow += ShadowMapSampling_horz(z, shadowLocation) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];
        }

    }

    SoftShadow /= weight_sum;

}


//--------------------------------------------------------------------------------------
// Interface
//--------------------------------------------------------------------------------------

interface ISeparableBlur
{
    float ApplyBlur( int3 DispatchThreadID, int2 ThreadGroupID, float depth );
};

//--------------------------------------------------------------------------------------
// Class
//--------------------------------------------------------------------------------------

class CSeparableBlur_Gauss_Horizontal : ISeparableBlur
{
    groupshared float sharedm[ThreadGroupSize_X];

    float ApplyBlur( int3 DispatchThreadID, int2 ThreadGroupID, float depth )
    {

        sharedm[ThreadGroupID.x] = tInput_ShadowBits.Load(DispatchThreadID);

        GroupMemoryBarrierWithGroupSync();

        float EyeLinearDepth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);

        float3 csPos = float3(DispatchThreadID.xy / gScreenSize.xy, depth);

        float3 PosW = CalcPosW(csPos, EyeLinearDepth);

        float3 PosLightView = mul(float4(PosW, 1.0f), gLightView).xyz;

        float4 shadowCoord = mul(float4(PosW, 1.0f), gLightShadowTransform);

        shadowCoord.xyz /= shadowCoord.w;

        float3 normal = tInput_Normal.Load(DispatchThreadID);

        normal = normalize(normal * 2.0f - 1.0f);

        float3 EyeVector = gEyePos - PosW;
        float3 LightVector = gLightPosW - PosW;

        float EyeAngle = dot(normal, EyeVector);
        float LightAngle = dot(normal, LightVector);

        float AngleMultiplier = EyeAngle / LightAngle;

        AngleMultiplier = clamp(AngleMultiplier, 0.0f, 1.0f);

        float DistanceMultiplier = PosLightView.z / EyeLinearDepth;

        float PenumbraSize = tInput_PenumbraSizes.Load(DispatchThreadID);

        float ScreenPenumbraSize = PenumbraSize * DistanceMultiplier * AngleMultiplier;

        float filter_radius_sample_count = FILTER_RADIUS_HARDSHADOW;

        float2 step = (ScreenPenumbraSize / filter_radius_sample_count);

        step *= gLight2DDirInEye_Horz * gScreenSize.xy;




        float weight_sum = 0.0f;

        float depthSample;
    
        float3 location = float3(tex_coord * gScreenSize.xy, 0);

        float SoftShadow = sharedm(location);

        if (ScreenPenumbraSize < gScreenDXY.x && ScreenPenumbraSize < gScreenDXY.y)
        {
            return;
        }

        SoftShadow *= GAUSSIAN_WEIGHTS_HARDSHADOW[0];

        weight_sum = GAUSSIAN_WEIGHTS_HARDSHADOW[0];
    
        float2 offset = float2(0.0f, 0.0f);

        step *= gScreenSize.xy;

        float2 uv;

    [unroll]
        for (int i = 1; i <= FILTER_RADIUS_HARDSHADOW; ++i)
        {

            offset.xy = step * i;

            location.xy += offset;
            

            SoftShadow += tInput_ShadowBits.Load(location) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

            weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];
            
        
            uv = tex_coord - offset;


            depthSample = tDepthMap.Sample(gSamplerLinear_Border_1, uv);
            linearDepth = ConvertToLinearDepth(depthSample, gPerspectiveValues.w, gPerspectiveValues.z);

            if (abs(ref_depth - linearDepth) < BILATERAL_DEPTH)
            {
                SoftShadow += tHardShadow.Sample(gSamplerLinear_Border_1, float3(uv, gShadowMapID)) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];
                weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];
            }
            else
            {
                float sampleCnt = FILTER_RADIUS_HARDSHADOW;
                float shadowMapStep = PenumbraSize / sampleCnt;

                float3 shadowLocation = float3(shadowCoord.xy, gShadowMapID);

                float3 shadowMap_offset = (shadowMapStep * -i, 0.0f, 0.0f);

                shadowLocation += shadowMap_offset;

                float z = BiasedZ(shadowCoord.z, dz_duv, shadowMap_offset.xy);

                SoftShadow += ShadowMapSampling_horz(z, shadowLocation) * GAUSSIAN_WEIGHTS_HARDSHADOW[i];

                weight_sum += GAUSSIAN_WEIGHTS_HARDSHADOW[i];
            }

        }

        SoftShadow /= weight_sum;




        return;
    }
};