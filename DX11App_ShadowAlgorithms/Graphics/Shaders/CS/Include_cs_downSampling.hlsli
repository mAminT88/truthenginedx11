#if defined(__INTELLISENSE__)

#include "../Includes/Include_CommonConstantBuffer.hlsli" 


#define GroupSize_X 1024

#else

#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli" 

#endif

//-----------------------------------------------------------------------------------------
// Compute shader
//-----------------------------------------------------------------------------------------
Texture2D tInput : register(t0);

RWTexture2D<float4> uOutPut : register(u0);


void DownScale4x4(uint2 CurPixel, uint groupThreadId)
{
	// Skip out of bound pixels
	if (CurPixel.y < gRes.y)
	{
		// Sum a group of 4x4 pixels
		int3 nFullResPos = int3(CurPixel * 4, 0);
		float4 downScaled = float4(0.0, 0.0, 0.0, 0.0);
		[unroll]
		for (int i = 0; i < 4; i++)
		{
			[unroll]
			for (int j = 0; j < 4; j++)
			{
				downScaled += tInput.Load(nFullResPos, int2(j, i));
			}
		}

		downScaled /= 16.0;
		uOutPut[CurPixel.xy] = downScaled; // Store the qurter resulotion image

	}
}