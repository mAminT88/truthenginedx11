#if defined(__INTELLISENSE__)

    #include "Include_cs_separableFindBlocker.hlsli"
    #include "../Includes/Include_HelperFunc.hlsli"

    #define ThreadGroupSize_X 256
    #define ThreadGroupSize_Y 256

#else

    #include "Graphics/Shaders/CS/Include_cs_separableFindBlocker.hlsli"
    #include "Graphics/Shaders/Includes/Include_HelperFunc.hlsli"


#endif

IFindBlocker gIFindBlocker;

[numthreads(ThreadGroupSize_X, ThreadGroupSize_Y, 1)]
void main( uint3 DTid : SV_DispatchThreadID )
{

    int3 location = int3(DTid.xy, 0);

    float depth = tDepthMap.Load(location);

    if (depth > 0.99999)
        return;

    float linearDepth = ConvertToLinearDepth(depth, gPerspectiveValues.w, gPerspectiveValues.z);

    float3 posW = CalcPosW(DTid.xy, linearDepth);

    float4 shadowCoord = mul(float4(posW, 1.0f), gShadowTransform);
    shadowCoord.xyz /= shadowCoord.w;

    if(shadowCoord.x < 0.0f || shadowCoord.y < 0.0f)
    {
        return;
    }

    float searchRaius = SearchRegionRadiusUV(shadowCoord.w, gLightZNear, gLightSize);

    float avgBlocker, blockerNum;

    gIFindBlocker.FindBlocker(avgBlocker, blockerNum, shadowCoord.xy, shadowCoord.z, searchRaius.xx, gShadowMapID);

    if(blockerNum == 0)
        return;

    float3 shadowXY = float3(floor(shadowCoord.xy * gShadowMapSize.xy), 0.0f);

    tUAVOutput[shadowXY] = avgBlocker;
}