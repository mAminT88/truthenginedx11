#if defined(__INTELLISENSE__)

    #include "../Includes/Include_CommonConstantBuffer.hlsli"
   #include  "../Includes/Include_Samplers.hlsli" 

#define ThreadGroupSize_X 256
#define ThreadGroupSize_Y 256

#define BLOCKER_SEARCH_SAMPLE_COUNT 5

#else


#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#include "Graphics/Shaders/Includes/Include_Samplers.hlsli" 

#endif



//--------------------------------------------------------------------------------------
// Textures
//--------------------------------------------------------------------------------------
Texture2D tDepthMap : register(t0);
Texture2DArray tShadowMapArray : register(t1);

RWTexture2DArray<float> tUAVOutput : register(u0);


//--------------------------------------------------------------------------------------
// CBuffers
//--------------------------------------------------------------------------------------


cbuffer cb_cs_perlight : register(b2)
{
    row_major matrix gShadowTransform;

    float gLightSize;
    float gLightZNear;
    float gLightZFar;
    float gShadowMapID;
};


//--------------------------------------------------------------------------------------
// Functions
//--------------------------------------------------------------------------------------

float3 CalcPosW( float2 xy, float linearDepth )
{
    float2 csPos = (2.0f * xy / gScreenSize.xy) - 1.0f;
    csPos.y *= -1.0f;
    

    float3 posV;
    posV.xy = csPos * gPerspectiveValues.xy * linearDepth.xx;
    posV.z = linearDepth;
    
    return mul(float4(posV, 1.0), gViewInv).xyz;
}


float2 SearchRegionRadiusUV( float zLight, float zNear, float lightSize )
{
    return lightSize * (zLight - zNear) / zLight;
}



//--------------------------------------------------------------------------------------
// Interface
//--------------------------------------------------------------------------------------

interface IFindBlocker
{
    void FindBlocker(
                out float avgBlockerDepth,
				out float numBlockers,
				float2 uv,
				float z0,
				float2 searchRegionRadiusUV,
                float shadowMapID );
};

//--------------------------------------------------------------------------------------
// Class
//--------------------------------------------------------------------------------------

class CFindBlocker_Horz : IFindBlocker
{
    void FindBlocker(
                out float avgBlockerDepth,
				out float numBlockers,
				float2 uv,
				float z0,
				float2 searchRegionRadiusUV,
                float shadowMapID )
    {

        float3 location = float3(uv, shadowMapID);

        float blockerSum = 0.0f;
        numBlockers = 0.0f;

        float shadowDepth = tShadowMapArray.SampleLevel(gSamplerPoint, location, 0);

        float BlockerSearchHalfSample = (BLOCKER_SEARCH_SAMPLE_COUNT - 1.0f) * 0.5f;

        float2 step = float2(1.0f, 0.0f) * searchRegionRadiusUV / BlockerSearchHalfSample;

        float3 offset = float3(0.0f, 0.0f, 0.0f);

    [unroll]
        for (int i = 1; i < (BLOCKER_SEARCH_SAMPLE_COUNT * 0.5f) - 1.0f; ++i)
        {
            offset.xy = step * i;
            float shadowMapDepth = tShadowMapArray.SampleLevel(gSamplerPoint, location + offset, 0);

		// averages depth values that are closer to the light than the receiving point {average blockers only}
            if (shadowMapDepth < z0)
            {
                blockerSum += shadowMapDepth;
                numBlockers++;
            }

            shadowMapDepth = tShadowMapArray.SampleLevel(gSamplerPoint, location - offset, 0);

        // averages depth values that are closer to the light than the receiving point {average blockers only}
            if (shadowMapDepth < z0)
            {
                blockerSum += shadowMapDepth;
                numBlockers++;
            }
        }

        avgBlockerDepth = blockerSum / numBlockers;
    }

};

class CFindBlocker_Vert : IFindBlocker
{
    void FindBlocker(
                out float avgBlockerDepth,
				out float numBlockers,
				float2 uv,
				float z0,
				float2 searchRegionRadiusUV,
                float shadowMapID )
    {

        float3 location = float3(uv, 0.0f);

        float blockerSum = 0.0f;
        numBlockers = 0.0f;

        float shadowDepth = tShadowMapArray.SampleLevel(gSamplerPoint, location, 0);

        float BlockerSearchHalfSample = (BLOCKER_SEARCH_SAMPLE_COUNT - 1.0f) * 0.5f;

        float2 step = float2(0.0f, 1.0f) * searchRegionRadiusUV / BlockerSearchHalfSample;

        float3 offset = float3(0.0f, 0.0f, 0.0f);

    [unroll]
        for (int i = 1; i < (BLOCKER_SEARCH_SAMPLE_COUNT / 2.0f) - 1.0f; ++i)
        {
            offset.xy = step * i;
            float shadowMapDepth = tShadowMapArray.SampleLevel(gSamplerPoint, location + offset, 0);

		// averages depth values that are closer to the light than the receiving point {average blockers only}
            if (shadowMapDepth < z0)
            {
                blockerSum += shadowMapDepth;
                numBlockers++;
            }

            shadowMapDepth = tShadowMapArray.SampleLevel(gSamplerPoint, location - offset, 0);

        // averages depth values that are closer to the light than the receiving point {average blockers only}
            if (shadowMapDepth < z0)
            {
                blockerSum += shadowMapDepth;
                numBlockers++;
            }
        }

        avgBlockerDepth = blockerSum / numBlockers;
    }

};