#if defined(__INTELLISENSE__)

#include "Include_cs_downSampling.hlsli"


#else

#include "Graphics/Shaders/CS/Include_cs_downSampling.hlsli"


#endif

[numthreads(GroupSize_X, 1, 1)]
void DownSampling4X4(uint3 dispatchThreadId : SV_DispatchThreadID, uint3 groupThreadId : SV_GroupThreadID, uint3 groupId : SV_GroupID)
{
	uint2 CurPixel = uint2(dispatchThreadId.x % gRes.x, dispatchThreadId.x / gRes.x);

	// Reduce a group of 16 pixels to a single pixel and store in the shared memory
	DownScale4x4(CurPixel, groupThreadId.x);

	return;

}