#if defined(__INTELLISENSE__)

#include "../Includes/Include_CommonConstantBuffer.hlsli"
#include "../Includes/Include_GaussianWeights.hlsli"

#define KERNEL_HALF 6
#define GAUSSIAN_WEIGHTS Gaussian13
#define GROUP_SIZE_X 128

#else

#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli"
#include "Graphics/Shaders/Includes/Include_GaussianWeights.hlsli"


#endif

Texture2D<float4> Input : register(t0);
RWTexture2D<float4> Output : register(u0);

groupshared float4 SharedInput[GROUP_SIZE_X];

//////////////////////////////////////////////////////////////////////////
//Interfaces
interface IFilter
{
	void Apply1DFilter(uint3 Gid, uint GI);
};

//////////////////////////////////////////////////////////////////////////
//Classes
class Filter_Vertical : IFilter
{
	void Apply1DFilter(uint3 Gid, uint GI)
	{
		int2 coord = int2(Gid.x, GI - KERNEL_HALF + (GROUP_SIZE_X - KERNEL_HALF * 2) * Gid.y);
		coord = clamp(coord, int2(0, 0), int2(gRes.x - 1, gRes.y - 1));
		SharedInput[GI] = Input.Load(int3(coord, 0));

		GroupMemoryBarrierWithGroupSync();

		// Vertical blur
		if (GI >= KERNEL_HALF && GI < (GROUP_SIZE_X - KERNEL_HALF) &&
			((GI - KERNEL_HALF + (GROUP_SIZE_X - KERNEL_HALF * 2) * Gid.y) < gRes.y))
		{
			float4 vOut = 0;

			[unroll]
			for (int i = -KERNEL_HALF; i <= KERNEL_HALF; ++i)
			{
				vOut += SharedInput[GI + i] * GAUSSIAN_WEIGHTS[abs(i)];
			}

			Output[coord] = float4(vOut.rgb, 1.0f);
		}
	}
};


class Filter_Horizontal : IFilter
{

	void Apply1DFilter(uint3 Gid, uint GI)
	{
		int2 coord = int2(GI - KERNEL_HALF + (GROUP_SIZE_X - KERNEL_HALF * 2) * Gid.x, Gid.y);
		coord = clamp(coord, int2(0, 0), int2(gRes.x - 1, gRes.y - 1));
		SharedInput[GI] = Input.Load(int3(coord, 0));

		GroupMemoryBarrierWithGroupSync();

		// Horizontal blur
		if (GI >= KERNEL_HALF && GI < (GROUP_SIZE_X - KERNEL_HALF) &&
			((Gid.x * (GROUP_SIZE_X - 2 * KERNEL_HALF) + GI - KERNEL_HALF) < gRes.x))
		{
			float4 vOut = 0;

			[unroll]
			for (int i = -KERNEL_HALF; i <= KERNEL_HALF; ++i)
				vOut += SharedInput[GI + i] * GAUSSIAN_WEIGHTS[abs(i)];

			Output[coord] = float4(vOut.rgb, 1.0f);
		}
	}

};


//////////////////////////////////////////////////////////////////////////
//Variables

IFilter gIFilter;