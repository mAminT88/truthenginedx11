#if defined(__INTELLISENSE__)

#include "../Includes/Include_CommonConstantBuffer.hlsli" 

#else

#include "Graphics/Shaders/Includes/Include_CommonConstantBuffer.hlsli" 

#endif

//-----------------------------------------------------------------------------------------
// Compute shader
//-----------------------------------------------------------------------------------------
Texture2D HDRTex : register(t0);
StructuredBuffer<float> AverageValues1D : register(t1);
StructuredBuffer<float> PrevAverageLum : register(t2);

RWStructuredBuffer<float> AverageLum : register(u1);
RWTexture2D<float4> HDRDownScale : register(u2);


// Group shared memory to store the intermidiate results
groupshared float SharedPositions[1024];

static const float4 LUM_FACTOR = float4(0.299, 0.587, 0.114, 0);

float DownScale4x4(uint2 CurPixel, uint groupThreadId)
{
	float avgLum = 0.0;

	// Skip out of bound pixels
	if (CurPixel.y < gRes.y)
	{
		// Sum a group of 4x4 pixels
		int3 nFullResPos = int3(CurPixel * 4, 0);
		float4 downScaled = float4(0.0, 0.0, 0.0, 0.0);
		[unroll]
		for (int i = 0; i < 4; i++)
		{
			[unroll]
			for (int j = 0; j < 4; j++)
			{
				downScaled += HDRTex.Load(nFullResPos, int2(j, i));
			}
		}

		downScaled /= 16.0;
		HDRDownScale[CurPixel.xy] = downScaled; // Store the qurter resulotion image

		// Calculate the lumenace value for this pixel
		avgLum = dot(downScaled, LUM_FACTOR);

	}
	// Write the result to the shared memory
	SharedPositions[groupThreadId] = avgLum;

	// Synchronize before next step
	GroupMemoryBarrierWithGroupSync();

	return avgLum;
}

float DownScale1024to4(uint dispatchThreadId, uint groupThreadId, float avgLum)
{
	// Expend the downscale code from a loop
	[unroll]
	for (uint groupSize = 4, step1 = 1, step2 = 2, step3 = 3; groupSize < 1024; groupSize *= 4, step1 *= 4, step2 *= 4, step3 *= 4)
	{
		// Skip out of bound pixels
		if (groupThreadId % groupSize == 0)
		{
			// Calculate the luminance sum for this step
			float stepAvgLum = avgLum;
			stepAvgLum += dispatchThreadId + step1 < gDomain ? SharedPositions[groupThreadId + step1] : avgLum;
			stepAvgLum += dispatchThreadId + step2 < gDomain ? SharedPositions[groupThreadId + step2] : avgLum;
			stepAvgLum += dispatchThreadId + step3 < gDomain ? SharedPositions[groupThreadId + step3] : avgLum;

			// Store the results
			avgLum = stepAvgLum;
			SharedPositions[groupThreadId] = stepAvgLum;
		}

		// Synchronize before next step
		GroupMemoryBarrierWithGroupSync();
	}

	return avgLum;
}

void DownScale4to1(uint dispatchThreadId, uint groupThreadId, uint groupId, float avgLum)
{
	if (groupThreadId == 0)
	{
		// Calculate the average lumenance for this thread group
		float fFinalAvgLum = avgLum;
		fFinalAvgLum += dispatchThreadId + 256 < gDomain ? SharedPositions[groupThreadId + 256] : avgLum;
		fFinalAvgLum += dispatchThreadId + 512 < gDomain ? SharedPositions[groupThreadId + 512] : avgLum;
		fFinalAvgLum += dispatchThreadId + 768 < gDomain ? SharedPositions[groupThreadId + 768] : avgLum;
		fFinalAvgLum /= 1024.0;

		// Write the final value into the 1D UAV which will be used on the next step
		AverageLum[groupId] = fFinalAvgLum;

	}
}

