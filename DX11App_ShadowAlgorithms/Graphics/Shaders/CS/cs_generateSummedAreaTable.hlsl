#if defined(__INTELLISENSE__)

   #include "Include_cs_generateSummedAreaTable.hlsli"

    #define ThreadGroupSize_X uint(256)
    #define ThreadGroupSize_Y uint(256)

    #define BLOCKER_SEARCH_SAMPLE_COUNT 5

#else

    #include "Graphics/Shaders/CS/Include_cs_generateSummedAreaTable.hlsli" 

#endif

IApplyPrefixSum gIApplyPrefixSum;

[numthreads(ThreadGroupSize_X, ThreadGroupSize_Y, 1)]
void main( uint3 DTid : SV_DispatchThreadID )
{
    gIApplyPrefixSum.ApplyPrefixSum(DTid);
}