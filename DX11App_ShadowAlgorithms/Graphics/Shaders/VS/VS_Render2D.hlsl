
#if defined(__INTELLISENSE__)

#include "../../ShaderSlots_DeferredShading.h"

#else

#include "Graphics/ShaderSlots_DeferredShading.h"

#endif

//--------------------------------------------------------------------------------------
// Constant Buffers
//--------------------------------------------------------------------------------------

cbuffer cb_vs_render2D : register(HLSL_SHADER_SLOTS_VS_CB_RENDER2D)
{
    row_major matrix gWVP;
};

//--------------------------------------------------------------------------------------
// Interfaces
//--------------------------------------------------------------------------------------

interface IPosition
{
    float4 CalcPositionH(float3 PosL);
};

IPosition g_IPosition;

//--------------------------------------------------------------------------------------
// Classes
//--------------------------------------------------------------------------------------

class CPosition_WVP : IPosition
{
    float4 CalcPositionH(float3 PosL)
    {
        return mul(float4(PosL, 1.0f), gWVP);
    }

};

class CPosition_noWVP : IPosition
{
    float4 CalcPositionH(float3 PosL)
    {
        return float4(PosL, 1.0f);
    }

};

CPosition_WVP g_CPosition_WVP;
CPosition_noWVP g_CPosition_noWVP;

//--------------------------------------------------------------------------------------
// Structs
//--------------------------------------------------------------------------------------

struct vertexIn
{
    float3 PosL : POSITION;
    float3 NormalL : NORMAL;
    float2 Tex : TEXCOORD;
};

struct vertexOut
{
    float4 PosH : SV_Position;
    float2 Tex : TEXCOORD0;
    float2 csPos : TEXCOORD1;
};

//--------------------------------------------------------------------------------------
// 
//--------------------------------------------------------------------------------------

vertexOut main(vertexIn vIn)
{
    vertexOut vOut;
    vOut.PosH = g_IPosition.CalcPositionH(vIn.PosL);
    vOut.Tex = vIn.Tex;
    vOut.csPos = vIn.PosL.xy;

    return vOut;
}