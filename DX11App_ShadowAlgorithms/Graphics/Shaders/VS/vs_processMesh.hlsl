
#include "../Includes/vs_Include_processMesh.hlsli"

cbuffer cbPerObjects : register(HLSL_SHADER_SLOTS_VS_CB_GENERATEGBUFFERS_PEROBJECT)
{

	row_major matrix gWVP ;
	row_major matrix gWorldInvTranspose;

};

cbuffer cbPerMesh : register(HLSL_SHADER_SLOTS_VS_CB_GENERATEGBUFFERS_PERMESH)
{
    row_major matrix gTexTransform;
}

#ifdef VERTEX_TYPE_BASIC32

vertexOut main(vertexIn vin)
{
	vertexOut vout;

	vout.PosH = mul(float4(vin.PosL, 1.0f), gWVP);
    vout.NormalW = mul(vin.NormalL, (float3x3) gWorldInvTranspose);
    vout.Tex = mul(float4(vin.Tex, 0.0f, 1.0f), gTexTransform).xy;

	return vout;
}

#endif


#ifdef VERTEX_TYPE_SKINNED

vertexOut main( vertexIn vin )
{
    vertexOut vout;

    float weightSum = vin.BoneWeights[0] + vin.BoneWeights[1] + vin.BoneWeights[2];

    float weight4 = 1.0 - weightSum;

    float4 pos = vin.BoneWeights[0] * mul(float4(vin.PosL, 1.0f), gBoneTransformations[vin.BoneIndex[0]]);
    pos += vin.BoneWeights[1] * mul(float4(vin.PosL, 1.0f), gBoneTransformations[vin.BoneIndex[1]]);
    pos += vin.BoneWeights[2] * mul(float4(vin.PosL, 1.0f), gBoneTransformations[vin.BoneIndex[2]]);
    pos += weight4 * mul(float4(vin.PosL, 1.0f), gBoneTransformations[vin.BoneIndex[3]]);
    pos.w = 1.0f;

    vout.PosH = mul(pos, gWVP);
    vout.NormalW = mul(vin.NormalL, (float3x3) gWorldInvTranspose);
    vout.Tex = mul(float4(vin.Tex, 0.0f, 1.0f), gTexTransform).xy;

    return vout;
}

#endif
