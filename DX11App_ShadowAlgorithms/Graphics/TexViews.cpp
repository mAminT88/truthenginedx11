#include "stdafx.h"
#include "TexViews.h"
#include "MathHelper.h"
#include "Globals.h"
#include "D3DUtil.h"
#include "Graphics/Colors.h"

using namespace Globals;


void TexViews::Init(UINT _bindFlag
	, UINT _height
	, UINT _width
	, DXGI_FORMAT _format
	, D3D11_USAGE _usageFlag
	, D3D11_CPU_ACCESS_FLAG _cpuAccessFlag
	, UINT _arraySize
	, UINT _mipLevels
	, UINT _sampleCount
	, UINT _sampleQuality
	, UINT _miscFlag
	, bool isTextureArray
	, const D3D11_SUBRESOURCE_DATA* initData)
{
	Release();


	mBindFlag = _bindFlag;
	mHeight = _height;
	mWidth = _width;
	mFormat = _format;
	mUsageFlags = _usageFlag;
	mCpuAccessFlag = _cpuAccessFlag;
	mArraySize = _arraySize;
	mMipLevels = _mipLevels;
	mSampleCount = _sampleCount;
	mSampleQuality = _sampleQuality;
	mMiscFlags = _miscFlag;

	if (mBindFlag & D3D11_BIND_DEPTH_STENCIL)
	{
		if (_format == DXGI_FORMAT_D32_FLOAT)
		{
			mFormat = DXGI_FORMAT_R32_TYPELESS;
		}
	}

	D3D11_TEXTURE2D_DESC texDesc;
	texDesc.Format = mFormat;
	texDesc.ArraySize = mArraySize;
	texDesc.BindFlags = mBindFlag;
	texDesc.CPUAccessFlags = mCpuAccessFlag;
	texDesc.Height = mHeight;
	texDesc.Width = mWidth;
	texDesc.MipLevels = mMipLevels;
	texDesc.MiscFlags = mMiscFlags;
	texDesc.SampleDesc.Count = mSampleCount;
	texDesc.SampleDesc.Quality = mSampleQuality;
	texDesc.Usage = mUsageFlags;

	//ComPtr<ID3D11Texture2D> tex;
	gDevice->CreateTexture2D(&texDesc, initData, mTex.ReleaseAndGetAddressOf());



	if (!isTextureArray)
	{

		if (mBindFlag & D3D11_BIND_SHADER_RESOURCE)
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC srvdesc;
			srvdesc.Format = _format == DXGI_FORMAT_D32_FLOAT ? DXGI_FORMAT_R32_FLOAT : _format;
			srvdesc.ViewDimension = mSampleCount == 1 ? D3D11_SRV_DIMENSION_TEXTURE2D : D3D11_SRV_DIMENSION_TEXTURE2DMS;
			srvdesc.Texture2D.MipLevels = mMipLevels;
			srvdesc.Texture2D.MostDetailedMip = 0;


			gDevice->CreateShaderResourceView(mTex.Get(), &srvdesc, mSRV.ReleaseAndGetAddressOf());
		}

		if (mBindFlag & D3D11_BIND_RENDER_TARGET)
		{
			D3D11_RENDER_TARGET_VIEW_DESC rtvdesc;
			rtvdesc.Format = _format;
			rtvdesc.ViewDimension = mSampleCount == 1 ? D3D11_RTV_DIMENSION_TEXTURE2D : D3D11_RTV_DIMENSION_TEXTURE2DMS;
			rtvdesc.Texture2D.MipSlice = 0;

			mRTV.emplace_back();

			gDevice->CreateRenderTargetView(mTex.Get(), &rtvdesc, mRTV.back().ReleaseAndGetAddressOf());
		}

		if (mBindFlag & D3D11_BIND_UNORDERED_ACCESS)
		{
			D3D11_UNORDERED_ACCESS_VIEW_DESC uavdesc;
			uavdesc.Format = _format;
			uavdesc.Texture2D.MipSlice = 0;
			uavdesc.ViewDimension = mSampleCount == 1 ? D3D11_UAV_DIMENSION_TEXTURE2D : D3D11_UAV_DIMENSION_TEXTURE2D;

			gDevice->CreateUnorderedAccessView(mTex.Get(), &uavdesc, mUAV.GetAddressOf());
		}

		if (mBindFlag & D3D11_BIND_DEPTH_STENCIL)
		{
			D3D11_DEPTH_STENCIL_VIEW_DESC dsvdesc;
			dsvdesc.Format = _format;
			dsvdesc.ViewDimension = mSampleCount == 1 ? D3D11_DSV_DIMENSION_TEXTURE2D : D3D11_DSV_DIMENSION_TEXTURE2DMS;
			dsvdesc.Texture2D.MipSlice = 0;
			dsvdesc.Flags = 0;

			mDSV.emplace_back();

			gDevice->CreateDepthStencilView(mTex.Get(), &dsvdesc, mDSV.back().ReleaseAndGetAddressOf());
		}
	}
	else
	{
		if (mBindFlag & D3D11_BIND_SHADER_RESOURCE)
		{
			D3D11_SHADER_RESOURCE_VIEW_DESC srvdesc;
			srvdesc.Format = _format == DXGI_FORMAT_D32_FLOAT ? DXGI_FORMAT_R32_FLOAT : _format;
			srvdesc.ViewDimension = mSampleCount == 1 ? D3D11_SRV_DIMENSION_TEXTURE2DARRAY : D3D11_SRV_DIMENSION_TEXTURE2DMSARRAY;
			srvdesc.Texture2DArray.ArraySize = mArraySize;
			srvdesc.Texture2DArray.FirstArraySlice = 0;
			srvdesc.Texture2DArray.MipLevels = mMipLevels;
			srvdesc.Texture2DArray.MostDetailedMip = 0;

			gDevice->CreateShaderResourceView(mTex.Get(), &srvdesc, mSRV.ReleaseAndGetAddressOf());
		}

		if (mBindFlag & D3D11_BIND_RENDER_TARGET)
		{
			D3D11_RENDER_TARGET_VIEW_DESC rtvdesc;
			rtvdesc.Format = texDesc.Format;
			rtvdesc.ViewDimension = mSampleCount == 1 ? D3D11_RTV_DIMENSION_TEXTURE2DARRAY : D3D11_RTV_DIMENSION_TEXTURE2DMSARRAY;
			rtvdesc.Texture2DArray.ArraySize = 1;
			rtvdesc.Texture2DArray.MipSlice = 0;

			for (int i = 0; i < mArraySize; ++i)
			{

				rtvdesc.Texture2DArray.FirstArraySlice = i;

				mRTV.emplace_back();

				gDevice->CreateRenderTargetView(mTex.Get(), &rtvdesc, mRTV.back().ReleaseAndGetAddressOf());
			}
		}

		if (mBindFlag & D3D11_BIND_UNORDERED_ACCESS)
		{
			D3D11_UNORDERED_ACCESS_VIEW_DESC uavdesc;
			uavdesc.Format = _format;
			uavdesc.ViewDimension = D3D11_UAV_DIMENSION_TEXTURE2DARRAY;
			uavdesc.Texture2DArray.MipSlice = 0;
			uavdesc.Texture2DArray.ArraySize = mArraySize;
			uavdesc.Texture2DArray.FirstArraySlice = 0;

			gDevice->CreateUnorderedAccessView(mTex.Get(), &uavdesc, mUAV.GetAddressOf());
		}

		if (mBindFlag & D3D11_BIND_DEPTH_STENCIL)
		{
			D3D11_DEPTH_STENCIL_VIEW_DESC dsvdesc;
			dsvdesc.Format = _format;
			dsvdesc.ViewDimension = mSampleCount == 1 ? D3D11_DSV_DIMENSION_TEXTURE2DARRAY : D3D11_DSV_DIMENSION_TEXTURE2DMSARRAY;
			dsvdesc.Texture2DArray.MipSlice = 0;
			dsvdesc.Texture2DArray.ArraySize = 1;
			dsvdesc.Flags = 0;

			for (int i = 0; i < mArraySize; ++i)
			{
				dsvdesc.Texture2DArray.FirstArraySlice = i;

				mDSV.emplace_back();

				gDevice->CreateDepthStencilView(mTex.Get(), &dsvdesc, mDSV.back().ReleaseAndGetAddressOf());

			}


		}
	}
}

void TexViews::BindSRV_PS(UINT _startSlot)const
{
	gContext->PSSetShaderResources(_startSlot, 1, mSRV.GetAddressOf());
}

ID3D11ShaderResourceView* TexViews::GetSRV()const
{
	return mSRV.Get();
}

ID3D11ShaderResourceView* const* TexViews::GetSRV_AddressOf() const
{
	return mSRV.GetAddressOf();
}

ID3D11RenderTargetView* TexViews::GetRTV(int index) const
{
	return mRTV[index].Get();
}

ID3D11UnorderedAccessView* TexViews::GetUAV() const
{
	return mUAV.Get();
}

ID3D11DepthStencilView* TexViews::GetDSV(int index) const
{
	return mDSV[index].Get();
}

void TexViews::ClearRTVs()
{
	for (auto m_rtv : mRTV)
	{
		gContext->ClearRenderTargetView(m_rtv.Get(), RGBA{ 0.0f, 0.0f, 0.0f, 1.0f });
	}
}

void TexViews::Release()
{
	mSRV.Reset();
	mUAV.Reset();
	/*for (auto m_rtv : mRTV)
	{
		m_rtv->Release();
		m_rtv = nullptr;
	}*/
	mRTV.clear();
	mDSV.clear();
}

ID3D11Texture2D* TexViews::GetTexture()const
{
	return mTex.Get();
}
