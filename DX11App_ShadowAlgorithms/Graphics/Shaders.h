#pragma once

#include "Globals.h"

using namespace Globals;

enum SHADER_TYPE
{
	SHADER_TYPE_VS,
	SHADER_TYPE_DS,
	SHADER_TYPE_HS,
	SHADER_TYPE_GS,
	SHADER_TYPE_PS
};

template<class T>
class ShaderObject
{
private:
	ComPtr<T> m_shader;
	SHADER_TYPE m_type;
	std::shared_ptr<void> pData = nullptr;
	size_t m_dataSize;
	LPCWSTR mHLSLFileName; LPCSTR mTarget; LPCSTR mEntryPoint; ID3DInclude* mInclude;  D3D_SHADER_MACRO* mDefines;
	LPCWSTR mCompiledFile;


	ComPtr<ID3D11ClassLinkage> pClassLinkage;


	ComPtr<ID3D11ShaderReflection> pReflector;


	//ComPtr<ID3DBlob> blob, errorBlob;

	HRESULT CompileFromFile(LPCWSTR _pFileName, LPCSTR _pTarget, LPCSTR _entryPoint = "main", ID3DInclude * _pInclude = nullptr, D3D_SHADER_MACRO * _pDefines = nullptr, UINT _flags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION);

	void CreateReflector(void* data, size_t datasize);
	

#pragma region Boost Serialization

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive& ar, const unsigned int version);
	

	template<class Archive>
	void save(Archive& ar, const unsigned int version) const;

	BOOST_SERIALIZATION_SPLIT_MEMBER();

#pragma endregion

public:
	

	ID3D11ClassLinkage* GetClassLinkage() const noexcept;
	

	ID3D11ShaderReflection* GetReflection() const noexcept;


	void Release() noexcept;


	T* GetShader() const noexcept;
	

	void SetShader(ID3D11DeviceContext* context, ID3D11ClassInstance** _classInstances = nullptr, int _classInstances_number = 0) const;

	int LoadShader(bool fromHLSLFile = false);
	void SaveShader(std::wstring _fileName) const;
	
	void SetVariables(LPCWSTR compileFile, LPCWSTR hlslFile, LPCSTR target, LPCSTR entryPoint, ID3DInclude* includes = nullptr, D3D_SHADER_MACRO* defines = nullptr);
};

template<class T>
void ShaderObject<T>::SetVariables(LPCWSTR compileFile, LPCWSTR hlslFile, LPCSTR target, LPCSTR entryPoint, ID3DInclude* includes, D3D_SHADER_MACRO* defines)
{
	mCompiledFile = compileFile;
	mHLSLFileName = hlslFile;
	mTarget = target;
	mEntryPoint = entryPoint;
	mInclude = includes;
	mDefines = defines;
}

template<class T>
ID3D11ClassLinkage* ShaderObject<T>::GetClassLinkage() const noexcept
{
	return pClassLinkage.Get();
}


template<class T>
ID3D11ShaderReflection* ShaderObject<T>::GetReflection() const noexcept
{
	return pReflector.Get();
}


template<class T>
void ShaderObject<T>::Release() noexcept
{
	m_shader.Reset();
	pReflector.Reset();
	pClassLinkage.Reset();
}


template<class T>
T* ShaderObject<T>::GetShader() const noexcept
{
	return m_shader.Get();
}

template<class T>
void ShaderObject<T>::CreateReflector(void* data, size_t datasize)
{

	D3DReflect(data, datasize, IID_ID3D11ShaderReflection, (void**)& pReflector);

}


template<class T>
int ShaderObject<T>::LoadShader(bool fromHLSLFile)
{

	std::ifstream bfin(mCompiledFile, std::ios::in | std::ios::binary);
	if (bfin.is_open() && !fromHLSLFile)
	{
		//pData = std::make_shared<byte>();

		boost::archive::binary_iarchive ai(bfin);

		ai&* this;

		return 0;
	}
	else {
		auto hresult = ShaderObject<T>::CompileFromFile(mHLSLFileName, mTarget, mEntryPoint, mInclude, mDefines, gD3DComplieShaderFlags);


		if (hresult == E_FAIL)
			return -1;
		else
		{
			SaveShader(mCompiledFile);
			return 1;
		}
	}

}

template<class T>
void ShaderObject<T>::SaveShader(std::wstring _fileName) const
{
	std::ofstream bfout = std::ofstream(_fileName, std::ios::out | std::ios::binary | std::ios::trunc);
	boost::archive::binary_oarchive ao(bfout);

	ao& (*this);
}



template<class T>
template<class Archive>
void ShaderObject<T>::save(Archive& ar, const unsigned int version) const
{

	std::wstring hlslFile(mHLSLFileName);
	std::wstring compiledfile(mCompiledFile);
	std::string targetstr(mTarget);
	std::string entrystr(mEntryPoint);

	ar& m_dataSize;
	ar& m_type;
	ar& hlslFile;
	ar& compiledfile;
	ar& targetstr;
	ar& entrystr;


	ar& boost::serialization::make_binary_object(pData.get(), m_dataSize);

}


template<>
template<class Archive>
void ShaderObject<ID3D11VertexShader>::load(Archive& ar, const unsigned int version)
{
	std::string targetstr, entrystr;
	std::wstring hlslFile, compiledfile;

	ar& m_dataSize;
	ar& m_type;
	ar& hlslFile;
	ar& compiledfile;
	ar& targetstr;
	ar& entrystr;

	mHLSLFileName = hlslFile.c_str();
	mHLSLFileName = hlslFile.c_str();
	mTarget = targetstr.c_str();
	mEntryPoint = entrystr.c_str();

	void* data = malloc(m_dataSize);

	ar& boost::serialization::make_binary_object(data, m_dataSize);

	pData.reset(data);

	gDevice->CreateClassLinkage(&pClassLinkage);
	gDevice->CreateVertexShader(pData.get(), m_dataSize, pClassLinkage.Get(), &m_shader);

	CreateReflector(pData.get(), m_dataSize);
}

template<>
template<class Archive>
void ShaderObject<ID3D11PixelShader>::load(Archive& ar, const unsigned int version)
{
	std::string targetstr, entrystr;
	std::wstring hlslFile, compiledfile;

	ar& m_dataSize;
	ar& m_type;
	ar& hlslFile;
	ar& compiledfile;
	ar& targetstr;
	ar& entrystr;

	mHLSLFileName = hlslFile.c_str();
	mHLSLFileName = hlslFile.c_str();
	mTarget = targetstr.c_str();
	mEntryPoint = entrystr.c_str();

	void* data = malloc(m_dataSize);

	ar& boost::serialization::make_binary_object(data, m_dataSize);

	pData.reset(data);

	gDevice->CreateClassLinkage(&pClassLinkage);
	gDevice->CreatePixelShader(pData.get(), m_dataSize, pClassLinkage.Get(), &m_shader);

	CreateReflector(pData.get(), m_dataSize);
}

template<>
template<class Archive>
void ShaderObject<ID3D11ComputeShader>::load(Archive& ar, const unsigned int version)
{

	std::string targetstr, entrystr;
	std::wstring hlslFile, compiledfile;

	ar& m_dataSize;
	ar& m_type;
	ar& hlslFile;
	ar& compiledfile;
	ar& targetstr;
	ar& entrystr;

	mHLSLFileName = hlslFile.c_str();
	mHLSLFileName = hlslFile.c_str();
	mTarget = targetstr.c_str();
	mEntryPoint = entrystr.c_str();

	void* data = malloc(m_dataSize);

	ar& boost::serialization::make_binary_object(data, m_dataSize);

	pData.reset(data);

	gDevice->CreateClassLinkage(&pClassLinkage);
	gDevice->CreateComputeShader(pData.get(), m_dataSize, pClassLinkage.Get(), &m_shader);

	CreateReflector(pData.get(), m_dataSize);
}

template<>
template<class Archive>
void ShaderObject<ID3D11HullShader>::load(Archive& ar, const unsigned int version)
{

	std::string targetstr, entrystr;
	std::wstring hlslFile, compiledfile;

	ar& m_dataSize;
	ar& m_type;
	ar& hlslFile;
	ar& compiledfile;
	ar& targetstr;
	ar& entrystr;

	mHLSLFileName = hlslFile.c_str();
	mHLSLFileName = hlslFile.c_str();
	mTarget = targetstr.c_str();
	mEntryPoint = entrystr.c_str();

	void* data = malloc(m_dataSize);

	ar& boost::serialization::make_binary_object(data, m_dataSize);

	pData.reset(data);

	gDevice->CreateClassLinkage(&pClassLinkage);
	gDevice->CreateHullShader(pData.get(), m_dataSize, pClassLinkage.Get(), &m_shader);

	CreateReflector(pData.get(), m_dataSize);
}


template<>
template<class Archive>
void ShaderObject<ID3D11DomainShader>::load(Archive& ar, const unsigned int version)
{

	std::string targetstr, entrystr;
	std::wstring hlslFile, compiledfile;

	ar& m_dataSize;
	ar& m_type;
	ar& hlslFile;
	ar& compiledfile;
	ar& targetstr;
	ar& entrystr;

	mHLSLFileName = hlslFile.c_str();
	mHLSLFileName = hlslFile.c_str();
	mTarget = targetstr.c_str();
	mEntryPoint = entrystr.c_str();

	void* data = malloc(m_dataSize);

	ar& boost::serialization::make_binary_object(data, m_dataSize);

	pData.reset(data);

	gDevice->CreateClassLinkage(&pClassLinkage);
	gDevice->CreateDomainShader(pData.get(), m_dataSize, pClassLinkage.Get(), &m_shader);

	CreateReflector(pData.get(), m_dataSize);
}

template<>
template<class Archive>
void ShaderObject<ID3D11GeometryShader>::load(Archive& ar, const unsigned int version)
{

	std::string targetstr, entrystr;
	std::wstring hlslFile, compiledfile;

	ar& m_dataSize;
	ar& m_type;
	ar& hlslFile;
	ar& compiledfile;
	ar& targetstr;
	ar& entrystr;

	mHLSLFileName = hlslFile.c_str();
	mHLSLFileName = hlslFile.c_str();
	mTarget = targetstr.c_str();
	mEntryPoint = entrystr.c_str();

	void* data = malloc(m_dataSize);

	ar& boost::serialization::make_binary_object(data, m_dataSize);

	pData.reset(data);

	gDevice->CreateClassLinkage(&pClassLinkage);
	gDevice->CreateGeometryShader(pData.get(), m_dataSize, pClassLinkage.Get(), &m_shader);

	CreateReflector(pData.get(), m_dataSize);
}


template<class T>
HRESULT ShaderObject<T>::CompileFromFile(LPCWSTR _pFileName, LPCSTR _pTarget, LPCSTR _entryPoint /*= "main"*/, ID3DInclude* _pInclude /*= nullptr*/, D3D_SHADER_MACRO* _pDefines /*= nullptr*/, UINT _flags /*= D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION*/);

template<class T>
void ShaderObject<T>::SetShader(ID3D11DeviceContext* context, ID3D11ClassInstance** _classInstances /*= nullptr*/, int _classInstances_number /*= 0*/) const;


