#include "stdafx.h"
#include "Globals.h"

using namespace Globals;

namespace StaticBuffers
{
	//ComPtr<ID3D11Buffer> gVertexBuffer_2D;
	//ComPtr<ID3D11Buffer> gIndexBuffer_2D;

	void InitStaticBuffers()
	{
		Vertex::Basic32 vertices[] =
		{
			{ XMFLOAT3(-1.0f, -1.0, 1.0f), XMFLOAT3(0.0f, 0.0, -1.0f), XMFLOAT2(0.0f, 1.0f) },
		{ XMFLOAT3(-1.0f, 1.0, 1.0f), XMFLOAT3(0.0f, 0.0, -1.0f), XMFLOAT2(0.0f, 0.0f) },
		{ XMFLOAT3(1.0f, 1.0, 1.0f), XMFLOAT3(0.0f, 0.0, -1.0f), XMFLOAT2(1.0f, 0.0f) },
		{ XMFLOAT3(1.0f, -1.0, 1.0f), XMFLOAT3(0.0f, 0.0, -1.0f), XMFLOAT2(1.0f, 1.0f) }
		};

		gVertecies_Basic32.emplace_back(); gVertecies_Basic32.back() = vertices[0];
		gVertecies_Basic32.emplace_back(); gVertecies_Basic32.back() = vertices[1];
		gVertecies_Basic32.emplace_back(); gVertecies_Basic32.back() = vertices[2];
		gVertecies_Basic32.emplace_back(); gVertecies_Basic32.back() = vertices[3];

		UINT indices[6] =
		{
			0,1,2,
			0,2,3
		};

		gIndecies.emplace_back(0);
		gIndecies.emplace_back(1);
		gIndecies.emplace_back(2);
		gIndecies.emplace_back(0);
		gIndecies.emplace_back(2);
		gIndecies.emplace_back(3);

		//D3D11_BUFFER_DESC bdesc;
		//bdesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		//bdesc.ByteWidth = 4 * sizeof(Vertex::Basic32);
		//bdesc.CPUAccessFlags = 0;
		//bdesc.MiscFlags = 0;
		//bdesc.StructureByteStride = 0;
		//bdesc.Usage = D3D11_USAGE_DEFAULT;

		//D3D11_SUBRESOURCE_DATA initvertex;
		//initvertex.pSysMem = &vertices[0];

		//gDevice->CreateBuffer(&bdesc, &initvertex, gVertexBuffer_2D.ReleaseAndGetAddressOf());

		/////////////////////////////////////////////////////////////////////////////////
		//bdesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		//bdesc.ByteWidth = 6 * sizeof(UINT32);
		//bdesc.CPUAccessFlags = 0;
		//bdesc.MiscFlags = 0;
		//bdesc.StructureByteStride = 0;
		//bdesc.Usage = D3D11_USAGE_DEFAULT;

		//D3D11_SUBRESOURCE_DATA initindex;
		//initindex.pSysMem = &indices[0];

		//gDevice->CreateBuffer(&bdesc, &initindex, gIndexBuffer_2D.ReleaseAndGetAddressOf());
	}
}