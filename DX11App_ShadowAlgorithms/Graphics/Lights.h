#pragma once

#include "stdafx.h"
#include "boost/serialization/serialization.hpp"
#include "MathHelper.h"
#include "ConstantBuffer.h"
#include "D3DUtil.h"
#include "WindowsComponents/LB_ItemList.h"
#include "WindowsComponents/ListBox.h"
#include "XMathSerialization.h"



class Lights;


enum LIGHT_TYPE
{
	LIGHT_TYPE_DIRECTIONAL,
	LIGHT_TYPE_SPOT,
	LIGHT_TYPE_POINT,
	LIGHT_TYPE_NONE
};




struct ILight
{
	ILight() : ShadowMapID(-1)
	{
		ZeroMemory(this, sizeof(ILight));
	}

	XMFLOAT4X4 View;
	XMFLOAT4X4 ViewProj;
	XMFLOAT4X4 ShadowTransform;

	XMFLOAT4 PerpectiveValues;

	XMFLOAT4 Diffuse;
	XMFLOAT4 Ambient;
	XMFLOAT4 Specular;

	XMFLOAT3 Direction;
	float	 LightSize;

	XMFLOAT3 Position;
	float	 zNear;

	float	 zFar;
	int32_t      CastShadow;
	int32_t      ID;
	int32_t      ShadowMapID;

};


struct DirectionalLight : public ILight
{

};


struct SpotLight : public ILight
{

	XMFLOAT3 Attenuation;
	float Range;

	float Spot;
	XMFLOAT3 pad;
};


class ILightObj
{

protected:
	
	int mIndex;


	ILight* ILightData = nullptr;


	static const XMFLOAT4X4 mProjToUV;


	XMFLOAT4X4 mProjMatrix;
	XMFLOAT4 mLightTarget;
	XMFLOAT4 mUpVector;


	bool mUpdated = false;
	bool mUpdated_View = false;
	bool mUpdated_Proj = false;


	float mFOVYAngle = XM_PI * 0.25f;
	float mAspectRatio = 1.0f;


	BoundingFrustum mBoundingFrustum;


	LIGHT_TYPE mLightType = LIGHT_TYPE_NONE;


	std::function<void(const float&)> mFunc_Update = [](const float& dt) {return; };


	//Serialization Functions

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive &ar, const unsigned int version)
	{
		ar& Name;
		ar& bDisabled;
		ar& bShadowDynamicObjects;
		ar& mLightTarget;
		ar& mUpVector;
		ar& mFOVYAngle;
		ar& mAspectRatio;
		ar& ILightData->Diffuse;
		ar& ILightData->Ambient;
		ar& ILightData->Specular;
		ar& ILightData->Direction;
		ar& ILightData->LightSize;
		ar& ILightData->Position;
		ar& ILightData->zNear;
		ar& ILightData->zFar;
		ar& ILightData->CastShadow;
		ar& ILightData->ID;
		ar& ILightData->ShadowMapID;
		
		UpdateViewMatrix();
		UpdateProjectionMatrix();
		UpdateViewProjMatrix();
		CreateBoundingFrustum();
	}

	template<class Archive>
	void save(Archive &ar, const unsigned int version) const
	{
		ar& Name;
		ar& bDisabled;
		ar& bShadowDynamicObjects;
		ar& mLightTarget;
		ar& mUpVector;
		ar& mFOVYAngle;
		ar& mAspectRatio;
		ar& ILightData->Diffuse;
		ar& ILightData->Ambient;
		ar& ILightData->Specular;
		ar& ILightData->Direction;
		ar& ILightData->LightSize;
		ar& ILightData->Position;
		ar& ILightData->zNear;
		ar& ILightData->zFar;
		ar& ILightData->CastShadow;
		ar& ILightData->ID;
		ar& ILightData->ShadowMapID;
	}

	BOOST_SERIALIZATION_SPLIT_MEMBER();

public:
	std::wstring Name;
	bool bDisabled;
	bool bShadowDynamicObjects = false;

	ILightObj();
	~ILightObj();


	virtual ILight& GetILightData() const;


	virtual void UpdateDiffuseColor(XMFLOAT4 _diffuseColor);
	virtual void UpdateAmbientColor(XMFLOAT4 _ambientColor);
	virtual void UpdateSpecularColor(XMFLOAT4 _specularColor);
	virtual void UpdateViewMatrix();
	virtual void UpdateViewMatrix(XMFLOAT4 _position, XMFLOAT4 _target, XMFLOAT4 _up);
	virtual void UpdateViewMatrix(XMFLOAT4X4 _view);
	virtual void UpdateShadowTransformMatrix();
	virtual void UpdateViewProjMatrix();
	virtual void UpdateViewProjMatrix(XMFLOAT4X4 _view, XMFLOAT4X4 _proj);
	virtual void SetDirection(XMFLOAT3 _direction);
	virtual void SetPosition(XMFLOAT3 _position);
	virtual void SetLightTarget(XMFLOAT3 _targetPosition);
	virtual void SetUpVector(XMFLOAT4 _up);
	virtual void SetCastShadow(bool _castshadow);
	//Create And Update Bounding Frustum
	virtual void CreateBoundingFrustum();
	//Return state of BoundingBox relative to Light Bounding Frustum
	virtual ContainmentType BoundingBoxContainment(BoundingBox _boundingBox);

	virtual bool UpdatedFromLastFrame();
	virtual void SetUpdated(bool _updated);
	virtual void Update(const float& dt);

	int GetIndex();

	LIGHT_TYPE GetLightType();


	/*
	*Virtual Functions
	*/

	virtual void UpdateProjectionMatrix() = 0;
	virtual void EditProperty(int _propIndex, float* value) = 0;
	virtual std::vector<std::tuple<std::string, int, std::vector<float>>> GetEditableProperties() = 0;

};


class DirectionalLightObj : public ILightObj
{

protected:

	float mFrustumRadius;

	DirectionalLight* LightData = nullptr;


	//Boost Serialization

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive &ar, const unsigned int version)
	{
		ar& mFrustumRadius;

		ar& boost::serialization::base_object<ILightObj>(*this);

	}


	template<class Archive>
	void save(Archive &ar, const unsigned int version) const
	{

		ar& mFrustumRadius;

		ar& boost::serialization::base_object<ILightObj>(*this);

	}


	BOOST_SERIALIZATION_SPLIT_MEMBER();


public:

	//DirectionalLightObj(FXMVECTOR _diffusecolor, FXMVECTOR _ambientColor, FXMVECTOR _specularColor, CXMVECTOR _target, XMFLOAT3 _direction, XMFLOAT3 _position, std::string _name, float _lightSize, float _znear, float _zfar, float _frustumRadius, int _castShadow, int _id, int _shadowMapID);
	DirectionalLightObj(DirectionalLight* lightData, XMFLOAT4 _diffusecolor, XMFLOAT4 _ambientColor, XMFLOAT4 _specularColor, XMFLOAT4 _target, XMFLOAT3 _direction, XMFLOAT3 _position, std::string _name, float _lightSize, float _znear, float _zfar, float _frustumRadius, int _castShadow, int index, int _id, int _shadowMapID);
	DirectionalLightObj(DirectionalLight* lightData);
	~DirectionalLightObj();

	//DirectionalLight& LightData = static_cast<DirectionalLight&>(ILightData);

	void SetLightData(DirectionalLight* directionalLightData);


	void UpdateProjectionMatrix() override;


	void EditProperty(int _propIndex, float* value) override;


	std::vector<std::tuple<std::string, int, std::vector<float>>> GetEditableProperties() override;


	void UpdateFrustum(float _znear, float _zfar, float _frustumRadius);


};


class DirectionalLightObj_Dynamic : public DirectionalLightObj
{
	bool updated = false;
};


class DirectionalLightObj_Static : public DirectionalLightObj
{

};


class SpotLightObj : public ILightObj
{

protected:

	SpotLight* LightData;

	//Boost Serialization

	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive &ar, const unsigned int version) const
	{
		ar& boost::serialization::base_object<ILightObj>(*this);
		
		ar& LightData->Attenuation;
		ar& LightData->Range;
		ar& LightData->Spot;
	}

	template<class Archive>
	void load(Archive &ar, const unsigned int version)
	{

		ar& boost::serialization::base_object<ILightObj>(*this);

		ar& LightData->Attenuation;
		ar& LightData->Range;
		ar& LightData->Spot;

		
	}


	BOOST_SERIALIZATION_SPLIT_MEMBER();


public:

	//SpotLightObj(FXMVECTOR _diffusecolor, FXMVECTOR _ambientColor, FXMVECTOR _specularColor, CXMVECTOR _target, XMFLOAT3 _direction, XMFLOAT3 _position, XMFLOAT3 _attenuation, std::string _name, float _lightSize, float _znear, float _zfar, float _range, float _spot, int _castShadow, int _id, int _shadowMapID, int _reversedZ);
	SpotLightObj(SpotLight* lightData, XMFLOAT4 _diffusecolor, XMFLOAT4 _ambientColor, XMFLOAT4 _specularColor, XMFLOAT4 _target, XMFLOAT3 _direction, XMFLOAT3 _position, XMFLOAT3 _attenuation, std::string _name, float _lightSize, float _znear, float _zfar, float _range, float _spot, int _castShadow, int index, int _id, int _shadowMapID, int _reversedZ);
	SpotLightObj(SpotLight* lightData);
	~SpotLightObj();

	void SetLightData(SpotLight* spotLightData);

	void UpdateFrustum(float _znear, float _zfar);
	void SetRange(float _range);
	void SetAttenuation(float _d1, float _d2, float _d3);
	void SetSpotSize(float _spotPow);

	void UpdateProjectionMatrix() override;

	void EditProperty(int _propIndex, float* value) override;

	std::vector<std::tuple<std::string, int, std::vector<float>>> GetEditableProperties() override;

};


class SpotLightObj_Dynamic : public SpotLightObj
{
	bool updated = false;
};


class SpotLightObj_Static : public SpotLightObj
{

};


//////////////////////////////////////////////////////////////////////////
//Lights Manager
//////////////////////////////////////////////////////////////////////////

struct CB_SPOTLIGHTS
{
	SpotLight gSpotLightArray[MAX_LIGHT_SPOTLIGHT];
};


struct CB_DIRECTIONALLIGHTS
{
	DirectionalLight gDirectionalLightArray[MAX_LIGHT_DIRECTIONALLIGHT];
};


class Lights
{
private:	

	CB_SPOTLIGHTS mData_CB_SpotLights;
	CB_DIRECTIONALLIGHTS mData_CB_DirectionalLight;

	UINT mDirectionalLightCount, mSpotLightCount;

	UINT mShadowMapCount = 0;


	/*ComPtr<ID3D11Buffer> mD3DDirectionalLightsB, mD3DSpotLightsB;
	D3D11_MAPPED_SUBRESOURCE mMapDirectionalLights, mMapSpotLights;
	ComPtr<ID3D11ShaderResourceView> mSRVDirectionalLights, mSRVSpotLights;*/

	ConstantBuffer<CB_SPOTLIGHTS> mCB_SpotLights;

	void InitBuffers();

	//Generate List Box Items Of Lights

	int mSelectedLightByListBox = -1;
	LB_ItemList m_itemList;
	void GenerateLBItems();


	//Boost Serialization

	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive & ar, const unsigned int version) const
	{
		auto directionalLightCount = mDirectionalLights.size();
		auto spotLightCount = mSpotLights.size();

		ar& directionalLightCount;
		ar& spotLightCount;

		for (int i = 0; i < mDirectionalLightCount; ++i)
		{
			ar& mDirectionalLights[i];
		}

		for (int i = 0; i < mSpotLightCount; ++i)
		{
			ar& mSpotLights[i];
		}

	}

	template<class Archive>
	void load(Archive & ar, const unsigned int version)
	{
		ar& mDirectionalLightCount;
		ar& mSpotLightCount;

		mDirectionalLights.reserve(mDirectionalLightCount);
		mSpotLights.reserve(mSpotLightCount);



		for (int i = 0; i < mDirectionalLightCount; ++i)
		{
			mDirectionalLights.emplace_back(DirectionalLightObj(&mData_CB_DirectionalLight.gDirectionalLightArray[i]));

			ar& mDirectionalLights.back();
		}

		for (int i = 0; i < mSpotLightCount; ++i)
		{
			mSpotLights.emplace_back(SpotLightObj(&mData_CB_SpotLights.gSpotLightArray[i]));

			ar& mSpotLights.back();
		}
	}

	BOOST_SERIALIZATION_SPLIT_MEMBER();

public:
	Lights();
	~Lights();

	void Destroy();

	void ResetShadowMapIDs();
	UINT GetShadowMapCount();


	std::vector<DirectionalLightObj> mDirectionalLights = {};
	std::vector<SpotLightObj> mSpotLights = {};


	std::vector<ILightObj*> mCollectionILights;


	void Update(const float& dt);
	void MoveLight(int lightIndex, XMFLOAT3 position, XMFLOAT3 direction);
	void LightSelected(int lightIndex);

	void ImportLights(std::wstring _lightFile, ID3D11DeviceContext *_context);
	void SaveLights(std::wstring _lightFile);
	void LoadLights(std::wstring _lightFile, ID3D11DeviceContext *_context);
	void UpdateLightBuffers(ID3D11DeviceContext *_context);
	void SetLightsBuffer_PS(ID3D11DeviceContext* _context, UINT spotLightRegisterSlot, UINT dirLightRegisterSlot);
	void SetLightsBuffer_VS(ID3D11DeviceContext* _context, UINT spotLightRegisterSlot, UINT dirLightRegisterSlot);

	void SetListBoxItems(ListBox* listBox);

};


