#pragma once



class AdapterData
{
public:
	AdapterData(ComPtr<IDXGIAdapter> pAdapter);
	ComPtr<IDXGIAdapter> pAdapter = nullptr;
	DXGI_ADAPTER_DESC description;
};

class AdapterReader
{
public:
	static std::vector<AdapterData> GetAdapters();
private:
	static std::vector<AdapterData> adapters;
};
