#include "stdafx.h"
#include "IRenderTechnique.h"
#include "Globals.h"

#include "Graphics/RenderPass/IRenderPass.h"

#include "Utility/GpuProfiler.h"

using namespace Globals;

void IRenderTechnique::AdvanceFrame()
{
	gLastFrame = gCurrentFrame;

	gCurrentFrame = (gCurrentFrame + 1) % 2;
}

void IRenderTechnique::InitGPUQueries()
{
	D3D11_QUERY_DESC queryDesc;
	queryDesc.Query = D3D11_QUERY_TIMESTAMP_DISJOINT;
	queryDesc.MiscFlags = 0;

	mQuery_Disjoint.emplace_back();
	mQuery_Disjoint.emplace_back();

	gDevice->CreateQuery(&queryDesc, mQuery_Disjoint[0].ReleaseAndGetAddressOf());
	gDevice->CreateQuery(&queryDesc, mQuery_Disjoint[1].ReleaseAndGetAddressOf());

	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
}

void IRenderTechnique::ReleaseGPUQueries()
{
	mQuery_Disjoint.clear();
	mGpuTimeStampDB->Destroy();
}

IRenderTechnique::IRenderTechnique() = default;

IRenderTechnique::~IRenderTechnique() = default;

IRenderTechnique::IRenderTechnique(IRenderTechnique&&) = default;

IRenderTechnique& IRenderTechnique::operator=(IRenderTechnique&&) = default;

void IRenderTechnique::Init()
{
	InitRenderPasses();
	InitSRVs();
}

void IRenderTechnique::Draw3D()
{
	gContext->Begin(mQuery_Disjoint[gCurrentFrame].Get());

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	for (auto rp : vRenderPasses)
	{
		rp->Draw3D();
	}

	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	gContext->End(mQuery_Disjoint[gCurrentFrame].Get());
}

void IRenderTechnique::DrawDeferred()
{
	for (auto rp : vRenderPasses)
	{
		rp->DrawDeferred();
	}
}

void IRenderTechnique::Draw2D()
{
	for (auto rp : vRenderPasses)
	{
		rp->Draw2D();
	}
}

void IRenderTechnique::DrawTxt()
{
	for (auto rp : vRenderPasses)
	{
		rp->DrawTxt();
	}
}

void IRenderTechnique::Update()
{
	AdvanceFrame();
}

void IRenderTechnique::Destroy()
{
}

void IRenderTechnique::InitGpuResources()
{
	for (auto rp : vRenderPasses)
	{
		rp->InitGPUResources();
	}

	InitGPUQueries();
}

void IRenderTechnique::DestroyGpuResources()
{
	for (auto rp : vRenderPasses)
	{
		rp->DestroyGPUResources();
	}

	ReleaseGPUQueries();
}

void IRenderTechnique::CaptureFrame()
{
}

void IRenderTechnique::RecompileShaders()
{
	for (auto rp : vRenderPasses)
	{
		rp->RecompileShaders();
	}
}

void IRenderTechnique::ShowUpContextMenu(int x, int y)
{
}

void IRenderTechnique::Catch_WCommand(WPARAM wparam, LPARAM lparam)
{
}

std::vector<std::pair<std::wstring, int>> IRenderTechnique::GetVersions()
{
	std::vector<std::pair<std::wstring, int>> v;
	return v;
}

void IRenderTechnique::RenderConfigurationUI()
{
}

std::string& IRenderTechnique::GetName()
{
	return mName;
}

std::string& IRenderTechnique::GetShadowTechniqueName()
{
	return mShadowTechniqueName;
}

void IRenderTechnique::ProfileGPUTime()
{
	static float onTime = 0.0f;
	static bool Validated = false;
	onTime += gDt;

	if (onTime > 0.5f)
	{
		Validated = true;
		onTime = 0.0f;
	}
	else {
		Validated = false;
	}

	while (gContext->GetData(mQuery_Disjoint[gLastFrame].Get(), &mQueryData_Disjoint, sizeof(mQueryData_Disjoint), 0) == S_FALSE)
	{
		Sleep(0.1);
	}

	if (mQueryData_Disjoint.Disjoint)
	{
		return;
	}


	const float GpuTime = 1000.0f / float(mQueryData_Disjoint.Frequency);


	if (Validated)
		mGpuTimeStampDB->WaitAndCalculateTime(gLastFrame, GpuTime, gContext.Get());

	ImGui::Begin("GPUTime");


	ImGui::Text(("Render Time : " + std::to_string(mGpuTimeStampDB->GetTimeDuration()) + " ms").c_str());

	for (auto rp : vRenderPasses)
	{

		if (Validated)
		{
			rp->ProfileGPUTime(GpuTime);

		}
		ImGui::Text(rp->GPUTime_String().c_str());
	}


	ImGui::End();
}
