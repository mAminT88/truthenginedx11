#include "stdafx.h"
#include "CCBuffers_DeferredShading.h"
#include "Globals.h"

using namespace Globals;

CCBuffers::CB_Unfrequent::CB_Unfrequent() : MiddleGrey(0.6048f/*0.0025f*/), LumWhiteSqr(5.0f), gOcclusionRadius(18.8f)
, gOcclusionFadeStart(2.8f), gOcclusionFadeEnd(14.9f), gSurfaceEpsilon(4.28f)
, gDownScaleHDRResolution_Height(gClientHeight / 4), gDownScaleHDRResolution_Width(gClientWidth / 4), gDomain(gClientWidth * gClientHeight / 16)
, gGroupSize(gDomain / 1024), gBloomThreshold(0.7f), gBloomScale(0.7f)
{
}
