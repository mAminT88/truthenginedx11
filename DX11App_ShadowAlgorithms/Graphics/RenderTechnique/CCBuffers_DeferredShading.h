#pragma once


#include "Graphics/ConstantBuffer.h"
#include "Graphics/ShaderSlots_DeferredShading.h"

namespace CCBuffers
{

	struct CB_Unfrequent
	{

		CB_Unfrequent();

		XMFLOAT4X4 gProj;

		XMFLOAT4 gPerspectiveValues;

		XMFLOAT2 gFixedPrecise_Float32ToInt32;
		int32_t gNumSpotLights;
		int32_t gNumDirectLights;

		XMFLOAT2 gShadowMapSize;
		XMFLOAT2 gShadowMapDXY;

		XMFLOAT2 gScreenSize;
		XMFLOAT2 gScreenDXY;

		float gAspectRatio;
		float gFOV_Y;
		float MiddleGrey;
		float LumWhiteSqr;

		float gOcclusionRadius;
		float gOcclusionFadeStart;
		float gOcclusionFadeEnd;
		float gSurfaceEpsilon;

		XMFLOAT4 gOffsetVectors[14];

		UINT gDownScaleHDRResolution_Width;
		UINT gDownScaleHDRResolution_Height;
		UINT gDomain;
		UINT gGroupSize;

		float gBloomThreshold;
		float gBloomScale;
		XMFLOAT2 pad0;

	};

	struct CB_PS_PER_FRAME
	{
		XMFLOAT4X4 gView;

		XMFLOAT4X4 gViewInv;

		XMFLOAT4X4 gViewProj;


		XMFLOAT3 gEyePos;
		float gEyeZNear;

		XMFLOAT3 gEyeDir;
		float gEyeZFar;

		float gLuminanceAdaptation = 1;
		XMFLOAT3 pad0;
	};

	extern ConstantBuffer<CB_Unfrequent> gCB_Unfrequent;
	extern CB_Unfrequent gData_CB_Unfrequent;

	extern ConstantBuffer<CB_PS_PER_FRAME> gCB_PS_Per_Frame;
	extern CB_PS_PER_FRAME gData_CB_PS_Per_Frame;

	inline void InitCommonCBs(ID3D11DeviceContext* context)
	{

		gCB_PS_Per_Frame.InitConstantBuffer(SHADER_SLOTS_PS_CB_PER_FRAME
			, CONSTANT_BUFFER_PS | CONSTANT_BUFFER_CS
			, &gData_CB_PS_Per_Frame
			, D3D11_CPU_ACCESS_WRITE
			, D3D11_USAGE_DYNAMIC);

		if(context != nullptr) gCB_PS_Per_Frame.SetConstantBuffer(context);

		gCB_Unfrequent.InitConstantBuffer(SHADER_SLOTS_PS_CB_UNFREQ
			, CONSTANT_BUFFER_PS | CONSTANT_BUFFER_CS
			, &gData_CB_Unfrequent
			, D3D11_CPU_ACCESS_WRITE
			, D3D11_USAGE_DYNAMIC);

		if(context != nullptr) gCB_Unfrequent.SetConstantBuffer(context);
	};

	inline void SetCommonCBs(ID3D11DeviceContext* context)
	{
		gCB_Unfrequent.SetConstantBuffer(context);
		gCB_PS_Per_Frame.SetConstantBuffer(context);
	}

	inline void ReleaseCBs()
	{
		gCB_Unfrequent.Release();
		gCB_PS_Per_Frame.Release();
	};

}