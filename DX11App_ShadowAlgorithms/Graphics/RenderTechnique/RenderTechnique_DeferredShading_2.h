#pragma once

#include "IRenderTechnique.h"


class RenderPass_GenerateGBuffers;
class RenderPass_DeferredShading;


class RenderTechnique_DeferredShading_2 : public IRenderTechnique
{
protected:

	enum class SHADOW_TECHNIQUE
	{
		BASICSHADOWMAPPING,
		PCF,
		VSM,
		SAVSM,
		PCSS,
		MSM,
		SSSM,
		OSSSS,
		OSSSS_SEPARABLEBLOCKERSEARCH
	};


	SHADOW_TECHNIQUE mCurrentShadowTechnique;

	std::unique_ptr<RenderPass_GenerateGBuffers> pRenderPass_GenerateGBUffers;
	std::unique_ptr<RenderPass_DeferredShading> pRenderPass_DeferredShading;

	void InitRenderPasses() override;
	void ReleaseRenderPasses() override;
	void SetTexViews() override;

	void UpdateCB_Unfreq();
	void UpdateCB_PerFrame();

	void ChangeShadowTechnique(SHADOW_TECHNIQUE shadowTechnique);

public:

	RenderTechnique_DeferredShading_2();
	~RenderTechnique_DeferredShading_2();

	RenderTechnique_DeferredShading_2(RenderTechnique_DeferredShading_2&&);
	RenderTechnique_DeferredShading_2& operator=(RenderTechnique_DeferredShading_2&&);

	void Init() override;
	void InitGpuResources() override;
	void DestroyGpuResources() override;
	void Draw3D() override;
	//void SetModels(std::vector<Model*>* BasicModels, std::vector<SkinnedModel*>* SkinnedModels) override;
	void Update() override;
	void ClearRenderPassVector();
	void ChangeRenderPass(int index, IRenderPass* renderpass);
	void AddRenderPass(int index, IRenderPass* renderpass);
	void AddRenderPass(IRenderPass* renderpass);
	void RenderConfigurationUI() override;
};
