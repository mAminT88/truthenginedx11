#pragma once

#include "IRenderTechnique.h"

#include "CCBuffers_DeferredShading.h"
#include "IDM_DeferredShading.h"

//#include "Graphics/RenderPass/RenderPass_GenerateGBuffers.h"
//#include "Graphics/RenderPass/RenderPass_DeferredShading.h"
//#include "Graphics/RenderPass/RenderPass_GenerateShadowMaps.h"
//#include "Graphics/RenderPass/RenderPass_GenerateVarianceShadowMap.h"
//#include "Graphics/RenderPass/RenderPass_GenerateSAT.h"
//#include "Graphics/RenderPass/RenderPass_GenerateMomentShadowMap.h"
//#include "Graphics/RenderPass/RenderPass_OSSSS_GenerateShadowData.h"
//#include "Graphics/RenderPass/RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker.h"
//#include "Graphics/RenderPass/RenderPass_SSSSM_GenerateShadowData.h"
//#include "Graphics/RenderPass/RenderPass_SSAO.h"
//#include "Graphics/RenderPass/RenderPass_HDR.h"

class IRenderPass;
class IRenderPass_SSAO;
class RenderPass_GenerateShadowMaps;
class RenderPass_GenerateVarianceShadowMap;
class RenderPass_GenerateGBuffers;
class RenderPass_DeferredShading;
class RenderPass_GenerateSAT;
class RenderPass_GenerateMomentShadowMap;
class RenderPass_OSSSS_GenerateShadowData;
class RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch;
class RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker;
class RenderPass_SSSSM_GenerateShadowData;
class RenderPass_HDR;
class RenderPass_SSAO;


class RenderTechnique_DeferredShading : public IRenderTechnique
{
protected:

	enum SHADOW_TECHNIQUE
	{
		SHADOWTECHNIQUE_BASICSHADOWMAPPING,
		SHADOWTECHNIQUE_PCF,
		SHADOWTECHNIQUE_VSM,
		SHADOWTECHNIQUE_SAVSM,
		SHADOWTECHNIQUE_PCSS,
		SHADOWTECHNIQUE_MSM,
		SHADOWTECHNIQUE_SSSM,
		SHADOWTECHNIQUE_OSSSS,
		SHADOWTECHNIQUE_OSSSS_SEPARABLEBLOCKERSEARCH
	};


	SHADOW_TECHNIQUE mCurrentShadowTechnique;

	static int mMSMKernelSampleCount;

	struct CB_PS_MSM_Parameters
	{
		CB_PS_MSM_Parameters();

		float gMomentBias;
		float gMaxDepthBias;
		XMFLOAT2 pad0;

		XMFLOAT4 gKernelSizeParameters_11_12;
		XMFLOAT4 gKernelSizeParameters_21_22;
		XMFLOAT4 gKernelSizeParameters_31_32;

		XMFLOAT4 gLightParameter_11_12;
		XMFLOAT4 gLightParameter_21_22;
		XMFLOAT4 gLightParameter_31_32;
	};

	CB_PS_MSM_Parameters mData_CB_PS_MSM_Parameters;
	ConstantBuffer<CB_PS_MSM_Parameters> mCB_PS_MSM_Parameters;
	   

	std::unique_ptr<RenderPass_GenerateShadowMaps> mRenderPass_GenerateShadowMap;
	std::unique_ptr<RenderPass_GenerateVarianceShadowMap> mRenderPass_GenerateVarianceShadowMap;
	std::unique_ptr<RenderPass_GenerateGBuffers> mRenderPass_GenerateGBUffers;
	std::unique_ptr<RenderPass_DeferredShading> mRenderPass_DeferredShading;
	std::unique_ptr<RenderPass_GenerateSAT> mRenderPass_SAVSM;
	std::unique_ptr<RenderPass_GenerateMomentShadowMap> mRenderPass_GenerateMomentShadowMap;
	std::unique_ptr<RenderPass_OSSSS_GenerateShadowData> mRenderPass_OSSSS_GenerateShadowData;
	std::unique_ptr<RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch> mRenderPass_OSSSS_GenerateShadowData__ScreenSpaceSeparableBlockerSearch;
	std::unique_ptr<RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker> mRenderPass_OSSSS_GenerateShadowData_LightViewPreFilteredBlockers;
	std::unique_ptr<RenderPass_SSSSM_GenerateShadowData> mRenderPass_SSSM_GenerateShadowData;
	std::unique_ptr<RenderPass_HDR> mRenderPass_HDR;
	std::unique_ptr<RenderPass_SSAO> mRenderPass_SSAO;

	IRenderPass_SSAO* pRenderPass_SSAO;

	IRenderPass* pRenderPass_HDR;


	void InitRenderPasses() override;
	void ReleaseRenderPasses() override;
	void SetTexViews() override;
	void SetLightBuffers() override;

	void UpdateCB_Unfreq();
	void UpdateCB_PerFrame();

	//void InitContextMenu() override;
	void ChangeShadowTechnique(SHADOW_TECHNIQUE shadowTechnique);
	void ChangeSSAOTechnique(IRenderPass_SSAO* rs_ssao);

public:

	RenderTechnique_DeferredShading();
	~RenderTechnique_DeferredShading();

	RenderTechnique_DeferredShading(RenderTechnique_DeferredShading&&);
	RenderTechnique_DeferredShading& operator=(RenderTechnique_DeferredShading&&);

	void Init() override;
	void InitGpuResources() override;
	void DestroyGpuResources() override;
	//void SetModels(std::vector<Model*>* BasicModels, std::vector<SkinnedModel*>* SkinnedModels) override;
	void Update() override;
	void ShowUpContextMenu(int x, int y) override;
	void Catch_WCommand(WPARAM wparam, LPARAM lparam) override;
	void ClearRenderPassVector();
	void ChangeRenderPass(int index, IRenderPass* renderpass);
	void AddRenderPass(int index, IRenderPass* renderpass);
	void AddRenderPass(IRenderPass* renderpass);
	void RenderConfigurationUI() override;
	void RecompileShaders() override;
};
