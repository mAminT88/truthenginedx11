#include "stdafx.h"
#include "Graphics/RenderTechnique/RenderTechnique_DeferredShading_2.h"
#include "Globals.h"
#include "MathHelper.h"

#include "CCBuffers_DeferredShading.h"
#include "IDM_DeferredShading.h"


//RenderPass Includes
#include "Graphics/RenderPass/RenderPass_GenerateGBuffers.h"
#include "Graphics/RenderPass/RenderPass_DeferredShading.h"

using namespace Globals;

void RenderTechnique_DeferredShading_2::InitRenderPasses()
{
	pRenderPass_GenerateGBUffers.reset(new RenderPass_GenerateGBuffers());
	pRenderPass_DeferredShading.reset(new RenderPass_DeferredShading());

	pRenderPass_GenerateGBUffers->Init();
	pRenderPass_DeferredShading->Init();

	SetTexViews();

	vRenderPasses.push_back(pRenderPass_GenerateGBUffers.get());
	vRenderPasses.push_back(pRenderPass_DeferredShading.get());
}

void RenderTechnique_DeferredShading_2::ReleaseRenderPasses()
{
	pRenderPass_GenerateGBUffers.reset();
	pRenderPass_DeferredShading.reset();
}

void RenderTechnique_DeferredShading_2::SetTexViews()
{
	pRenderPass_DeferredShading->pTexViews_ColorMap = pRenderPass_GenerateGBUffers->GetTexViewColorMap();
	pRenderPass_DeferredShading->pTexViews_NormalMap = pRenderPass_GenerateGBUffers->GetTexViewNormalMap();
	pRenderPass_DeferredShading->pTexViews_DepthMap = pRenderPass_GenerateGBUffers->GetTexViewDepthMap();
	pRenderPass_DeferredShading->pTexViews_SpecularMap = pRenderPass_GenerateGBUffers->GetTexViewSpecularMap();
	pRenderPass_DeferredShading->pTexViews_ShadowMapArray = gLights.mSpotLights[0].GetShadowMapTexView();
}

void RenderTechnique_DeferredShading_2::UpdateCB_Unfreq()
{
	// Start with 14 uniformly distributed vectors.  We choose the 8 corners of the cube
	// and the 6 center points along each cube face.  We always alternate the points on 
	// opposites sides of the cubes.  This way we still get the vectors spread out even
	// if we choose to use less than 14 samples.

	// 8 cube corners

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[0] = XMFLOAT4(+1.0f, +1.0f, +1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[1] = XMFLOAT4(-1.0f, -1.0f, -1.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[2] = XMFLOAT4(-1.0f, +1.0f, +1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[3] = XMFLOAT4(+1.0f, -1.0f, -1.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[4] = XMFLOAT4(+1.0f, +1.0f, -1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[5] = XMFLOAT4(-1.0f, -1.0f, +1.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[6] = XMFLOAT4(-1.0f, +1.0f, -1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[7] = XMFLOAT4(+1.0f, -1.0f, +1.0f, 0.0f);

	// 6 centers of cube faces
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[8] = XMFLOAT4(-1.0f, 0.0f, 0.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[9] = XMFLOAT4(+1.0f, 0.0f, 0.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[10] = XMFLOAT4(0.0f, -1.0f, 0.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[11] = XMFLOAT4(0.0f, +1.0f, 0.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[12] = XMFLOAT4(0.0f, 0.0f, -1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[13] = XMFLOAT4(0.0f, 0.0f, +1.0f, 0.0f);

	for (int i = 0; i < 14; ++i)
	{
		// Create random lengths in [0.25, 1.0].
		float s = MathHelper::RandF(0.25f, 1.0f);

		XMVECTOR v = s * XMVector4Normalize(XMLoadFloat4(&CCBuffers::gData_CB_Unfrequent.gOffsetVectors[i]));

		XMStoreFloat4(&CCBuffers::gData_CB_Unfrequent.gOffsetVectors[i], v);
	}


	XMStoreFloat4x4(&CCBuffers::gData_CB_Unfrequent.gProj, gCamera_main.Proj());

	XMStoreFloat4(&CCBuffers::gData_CB_Unfrequent.gPerspectiveValues, gCamera_main.GetPerspectiveValues());

	auto fixedprecise = (std::pow(2, 32) - 1.0f) / 784.0f;
	CCBuffers::gData_CB_Unfrequent.gFixedPrecise_Float32ToInt32.x = fixedprecise;
	CCBuffers::gData_CB_Unfrequent.gFixedPrecise_Float32ToInt32.y = 1 / fixedprecise;

	CCBuffers::gData_CB_Unfrequent.gNumSpotLights = Globals::gLights.mSpotLights.size();
	CCBuffers::gData_CB_Unfrequent.gNumDirectLights = Globals::gLights.mDirectionalLights.size();

	CCBuffers::gData_CB_Unfrequent.gShadowMapSize = XMFLOAT2(gShadowMapResolution, gShadowMapResolution);

	float shadowMapDxy = (1 / gShadowMapResolution);
	CCBuffers::gData_CB_Unfrequent.gShadowMapDXY = XMFLOAT2(shadowMapDxy, shadowMapDxy);

	CCBuffers::gData_CB_Unfrequent.gScreenSize = XMFLOAT2(gClientWidth, gClientHeight);
	CCBuffers::gData_CB_Unfrequent.gScreenDXY = XMFLOAT2(1.0f / gClientWidth, 1.0f / gClientHeight);


	CCBuffers::gCB_Unfrequent.Update(gContext.Get());
}

void RenderTechnique_DeferredShading_2::UpdateCB_PerFrame()
{
	auto& cb = CCBuffers::gData_CB_PS_Per_Frame;

	XMStoreFloat4x4(&cb.gView, gCamera_main.View());
	XMStoreFloat4x4(&cb.gViewInv, gCamera_main.ViewInv());
	XMStoreFloat4x4(&cb.gViewProj, gCamera_main.ViewProj());
	cb.gEyeDir = gCamera_main.GetLook();
	cb.gEyePos = gCamera_main.GetPosition();
	cb.gEyeZFar = gCamera_main.GetFarZ();
	cb.gEyeZNear = gCamera_main.GetNearZ();

	static float sumDt = 0.0f;
	sumDt += gDt;


	cb.gLuminanceAdaptation = std::max(gDt / (10), 1.0f);
	sumDt = 0.0f;

	CCBuffers::gCB_PS_Per_Frame.Update(gContext.Get());
}

void RenderTechnique_DeferredShading_2::ChangeShadowTechnique(SHADOW_TECHNIQUE shadowTechnique)
{

}

RenderTechnique_DeferredShading_2::RenderTechnique_DeferredShading_2() = default;

RenderTechnique_DeferredShading_2::~RenderTechnique_DeferredShading_2() = default;

RenderTechnique_DeferredShading_2::RenderTechnique_DeferredShading_2(RenderTechnique_DeferredShading_2&&) = default;

RenderTechnique_DeferredShading_2& RenderTechnique_DeferredShading_2::operator=(RenderTechnique_DeferredShading_2&&) = default;


void RenderTechnique_DeferredShading_2::Init()
{

	pRenderPass_GenerateGBUffers = std::make_unique<RenderPass_GenerateGBuffers>();
	pRenderPass_DeferredShading = std::make_unique<RenderPass_DeferredShading>();

	IRenderTechnique::Init();

	this->mName = "Deferred Shading 2";
}

void RenderTechnique_DeferredShading_2::InitGpuResources()
{

	CCBuffers::InitCommonCBs(nullptr);


	IRenderTechnique::InitGpuResources();



	UpdateCB_Unfreq();


	SetLightBuffers();	

}

void RenderTechnique_DeferredShading_2::DestroyGpuResources()
{
	IRenderTechnique::DestroyGpuResources();

	CCBuffers::ReleaseCBs();
}

void RenderTechnique_DeferredShading_2::Draw3D()
{

#pragma region Async Calls

	concurrency::task_group tasks;

	tasks.run([]() { gLights.PrepareAllShadowMapsCMDListAsyc(); });
	//gLights.PrepareAllShadowMapsCMDList();

	tasks.run([this]() {pRenderPass_GenerateGBUffers->DrawDeferred(); });

	tasks.run([this]() { pRenderPass_DeferredShading->DrawDeferred(); });

#pragma endregion


	//wait for finishing tasks
	tasks.wait();


#pragma region Immediate Rendering


	//gContext->Begin(mQuery_Disjoint[gCurrentFrame].Get());

	//mGpuTimeStampDB.SetStart(gCurrentFrame);

	//Execute CommandList on ImmediateContext
	gLights.GenerateAllShadowMaps();
	
	gContext->ExecuteCommandList(pRenderPass_GenerateGBUffers->GetCommandList(), false);

	gContext->ExecuteCommandList(pRenderPass_DeferredShading->GetCommandList(), false);

	//mGpuTimeStampDB.SetEnd(gCurrentFrame);

	//gContext->End(mQuery_Disjoint[gCurrentFrame].Get());

#pragma endregion

}

void RenderTechnique_DeferredShading_2::Update()
{
	for (auto& renderPasses : vRenderPasses)
	{
		renderPasses->Update();
	}

	UpdateCB_PerFrame();
}

void RenderTechnique_DeferredShading_2::ClearRenderPassVector()
{
	for (auto& r : vRenderPasses)
	{
		r->DestroyGPUResources();
	}

	vRenderPasses.clear();
}

void RenderTechnique_DeferredShading_2::ChangeRenderPass(int index, IRenderPass* renderpass)
{
	if (vRenderPasses[index] != renderpass)
	{
		vRenderPasses[index]->DestroyGPUResources();
		vRenderPasses[index] = renderpass;
		vRenderPasses[index]->InitGPUResources();
	}
}

void RenderTechnique_DeferredShading_2::AddRenderPass(int index, IRenderPass* renderpass)
{
	if (vRenderPasses[index] != renderpass)
	{
		renderpass->InitGPUResources();
		vRenderPasses.insert(vRenderPasses.begin() + index, renderpass);
	}
}

void RenderTechnique_DeferredShading_2::AddRenderPass(IRenderPass* renderpass)
{
	if (renderpass != nullptr)
	{
		renderpass->InitGPUResources();
		vRenderPasses.push_back(renderpass);
	}

}

void RenderTechnique_DeferredShading_2::RenderConfigurationUI()
{

}

