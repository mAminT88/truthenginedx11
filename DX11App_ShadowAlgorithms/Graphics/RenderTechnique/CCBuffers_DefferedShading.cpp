#include "stdafx.h"
#include "CCBuffers_DeferredShading.h"

#include "Graphics/ConstantBuffer.h"

namespace CCBuffers
{
	ConstantBuffer<CB_Unfrequent> gCB_Unfrequent;
	CB_Unfrequent gData_CB_Unfrequent;

	ConstantBuffer<CB_PS_PER_FRAME> gCB_PS_Per_Frame;
	CB_PS_PER_FRAME gData_CB_PS_Per_Frame;
}