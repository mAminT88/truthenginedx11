#include "stdafx.h"
#include "RenderTechnique_DeferredShading.h"
#include "Globals.h"
#include "MathHelper.h"

/////Render Passes
#include "Graphics/RenderPass/IRenderPass.h"
#include "Graphics/RenderPass/RenderPass_GenerateGBuffers.h"
#include "Graphics/RenderPass/RenderPass_DeferredShading.h"
#include "Graphics/RenderPass/RenderPass_GenerateShadowMaps.h"
#include "Graphics/RenderPass/RenderPass_GenerateVarianceShadowMap.h"
#include "Graphics/RenderPass/RenderPass_GenerateSAT.h"
#include "Graphics/RenderPass/RenderPass_GenerateMomentShadowMap.h"
#include "Graphics/RenderPass/RenderPass_OSSSS_GenerateShadowData.h"
#include "Graphics/RenderPass/RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker.h"
#include "Graphics/RenderPass/RenderPass_SSSSM_GenerateShadowData.h"
#include "Graphics/RenderPass/RenderPass_SSAO.h"
#include "Graphics/RenderPass/RenderPass_HDR.h"


/////Shaders
#include "Graphics/ShaderList/PS_PostFX_FinalPass.h"
#include <Graphics\ShaderList\PS_DeferredShading.h>


using namespace Globals;

int RenderTechnique_DeferredShading::mMSMKernelSampleCount = 27;

void RenderTechnique_DeferredShading::InitRenderPasses()
{
	mRenderPass_GenerateGBUffers.reset(new RenderPass_GenerateGBuffers());
	mRenderPass_DeferredShading.reset(new RenderPass_DeferredShading());
	mRenderPass_GenerateShadowMap.reset(new RenderPass_GenerateShadowMaps());
	mRenderPass_GenerateVarianceShadowMap.reset(new RenderPass_GenerateVarianceShadowMap());
	mRenderPass_SAVSM.reset(new RenderPass_GenerateSAT());
	mRenderPass_GenerateMomentShadowMap.reset(new RenderPass_GenerateMomentShadowMap());
	mRenderPass_OSSSS_GenerateShadowData.reset(new RenderPass_OSSSS_GenerateShadowData());
	mRenderPass_OSSSS_GenerateShadowData__ScreenSpaceSeparableBlockerSearch.reset(new RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch());
	mRenderPass_OSSSS_GenerateShadowData_LightViewPreFilteredBlockers.reset(new RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker());
	mRenderPass_SSSM_GenerateShadowData.reset(new RenderPass_SSSSM_GenerateShadowData());
	mRenderPass_SSAO.reset(new RenderPass_SSAO());
	mRenderPass_HDR.reset(new RenderPass_HDR());

	mRenderPass_GenerateGBUffers->Init();
	mRenderPass_DeferredShading->Init();
	mRenderPass_GenerateShadowMap->Init();
	mRenderPass_GenerateVarianceShadowMap->Init();
	mRenderPass_SAVSM->Init();
	mRenderPass_GenerateMomentShadowMap->Init();
	mRenderPass_OSSSS_GenerateShadowData->Init();
	mRenderPass_OSSSS_GenerateShadowData__ScreenSpaceSeparableBlockerSearch->Init();
	mRenderPass_OSSSS_GenerateShadowData_LightViewPreFilteredBlockers->Init();
	mRenderPass_SSSM_GenerateShadowData->Init();
	mRenderPass_SSAO->Init();
	mRenderPass_HDR->Init();

	
	SetTexViews();

	vRenderPasses.push_back(mRenderPass_GenerateShadowMap.get());
	vRenderPasses.push_back(mRenderPass_GenerateGBUffers.get());
	vRenderPasses.push_back(mRenderPass_DeferredShading.get());

}

void RenderTechnique_DeferredShading::ReleaseRenderPasses()
{
	mRenderPass_GenerateGBUffers.reset();
	mRenderPass_DeferredShading.reset();
	mRenderPass_GenerateShadowMap.reset();
	mRenderPass_GenerateVarianceShadowMap.reset();
	mRenderPass_SAVSM.reset();
	mRenderPass_GenerateMomentShadowMap.reset();
	mRenderPass_OSSSS_GenerateShadowData.reset();
	mRenderPass_OSSSS_GenerateShadowData__ScreenSpaceSeparableBlockerSearch.reset();
	mRenderPass_OSSSS_GenerateShadowData_LightViewPreFilteredBlockers.reset();
	mRenderPass_SSSM_GenerateShadowData.reset();
	mRenderPass_SSAO.reset();
	mRenderPass_HDR.reset();
}

void RenderTechnique_DeferredShading::SetTexViews()
{

	mRenderPass_DeferredShading->pTexViews_ColorMap = mRenderPass_GenerateGBUffers->GetTexViewColorMap();
	mRenderPass_DeferredShading->pTexViews_NormalMap = mRenderPass_GenerateGBUffers->GetTexViewNormalMap();
	mRenderPass_DeferredShading->pTexViews_DepthMap = mRenderPass_GenerateGBUffers->GetTexViewDepthMap();
	mRenderPass_DeferredShading->pTexViews_SpecularMap = mRenderPass_GenerateGBUffers->GetTexViewSpecularMap();
	mRenderPass_DeferredShading->pTexViews_SATUINT = mRenderPass_GenerateMomentShadowMap->GetTexViewSAT_UINT();
	mRenderPass_DeferredShading->pTexViews_ShadowMapArray = mRenderPass_GenerateShadowMap->GetTexViewShadowMap();
	mRenderPass_DeferredShading->pTexViews_SSAOMap = mRenderPass_SSAO->GetTexViewSSAO();

	mRenderPass_OSSSS_GenerateShadowData->pTexViewDepthMap = mRenderPass_GenerateGBUffers->GetTexViewDepthMap();
	mRenderPass_OSSSS_GenerateShadowData->pTexViewNormalMap = mRenderPass_GenerateGBUffers->GetTexViewNormalMap();
	mRenderPass_OSSSS_GenerateShadowData->pTexViewShadowMap = mRenderPass_GenerateShadowMap->GetTexViewShadowMap();

	mRenderPass_OSSSS_GenerateShadowData__ScreenSpaceSeparableBlockerSearch->pTexViewDepthMap = mRenderPass_GenerateGBUffers->GetTexViewDepthMap();
	mRenderPass_OSSSS_GenerateShadowData__ScreenSpaceSeparableBlockerSearch->pTexViewNormalMap = mRenderPass_GenerateGBUffers->GetTexViewNormalMap();
	mRenderPass_OSSSS_GenerateShadowData__ScreenSpaceSeparableBlockerSearch->pTexViewShadowMap = mRenderPass_GenerateShadowMap->GetTexViewShadowMap();

	mRenderPass_SSSM_GenerateShadowData->pTexViewDepthMap = mRenderPass_GenerateGBUffers->GetTexViewDepthMap();
	mRenderPass_SSSM_GenerateShadowData->pTexViewNormalMap = mRenderPass_GenerateGBUffers->GetTexViewNormalMap();
	mRenderPass_SSSM_GenerateShadowData->pTexViewShadowMap = mRenderPass_GenerateShadowMap->GetTexViewShadowMap();

	mRenderPass_SSAO->pTexViews_DepthMap = mRenderPass_GenerateGBUffers->GetTexViewDepthMap();
	mRenderPass_SSAO->pTexViews_NormalMap = mRenderPass_GenerateGBUffers->GetTexViewNormalMap();

	mRenderPass_HDR->pTexViews_LuminanceMap = mRenderPass_DeferredShading->GetLuminanceMap();
	mRenderPass_HDR->pTexViews_ColorMap = mRenderPass_DeferredShading->GetColorMap();


}

void RenderTechnique_DeferredShading::SetLightBuffers()
{
	gLights.SetLightsBuffer_PS(gContext.Get(), SHADER_SLOTS_PS_CB_SPOTLIGHTS, SHADER_SLOTS_PS_CB_DIRLIGHTS);
	gLights.SetLightsBuffer_VS(gContext.Get(), SHADER_SLOTS_VS_CB_SPOTLIGHTS, SHADER_SLOTS_VS_CB_DIRLIGHTS);
}


void RenderTechnique_DeferredShading::UpdateCB_Unfreq()
{

	// Start with 14 uniformly distributed vectors.  We choose the 8 corners of the cube
	// and the 6 center points along each cube face.  We always alternate the points on 
	// opposites sides of the cubes.  This way we still get the vectors spread out even
	// if we choose to use less than 14 samples.

	// 8 cube corners

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[0] = XMFLOAT4(+1.0f, +1.0f, +1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[1] = XMFLOAT4(-1.0f, -1.0f, -1.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[2] = XMFLOAT4(-1.0f, +1.0f, +1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[3] = XMFLOAT4(+1.0f, -1.0f, -1.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[4] = XMFLOAT4(+1.0f, +1.0f, -1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[5] = XMFLOAT4(-1.0f, -1.0f, +1.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[6] = XMFLOAT4(-1.0f, +1.0f, -1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[7] = XMFLOAT4(+1.0f, -1.0f, +1.0f, 0.0f);

	// 6 centers of cube faces
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[8] = XMFLOAT4(-1.0f, 0.0f, 0.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[9] = XMFLOAT4(+1.0f, 0.0f, 0.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[10] = XMFLOAT4(0.0f, -1.0f, 0.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[11] = XMFLOAT4(0.0f, +1.0f, 0.0f, 0.0f);

	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[12] = XMFLOAT4(0.0f, 0.0f, -1.0f, 0.0f);
	CCBuffers::gData_CB_Unfrequent.gOffsetVectors[13] = XMFLOAT4(0.0f, 0.0f, +1.0f, 0.0f);

	for (int i = 0; i < 14; ++i)
	{
		// Create random lengths in [0.25, 1.0].
		float s = MathHelper::RandF(0.25f, 1.0f);

		XMVECTOR v = s * XMVector4Normalize(XMLoadFloat4(&CCBuffers::gData_CB_Unfrequent.gOffsetVectors[i]));

		XMStoreFloat4(&CCBuffers::gData_CB_Unfrequent.gOffsetVectors[i], v);
	}


	XMStoreFloat4x4(&CCBuffers::gData_CB_Unfrequent.gProj, gCamera_main.Proj());

	XMStoreFloat4(&CCBuffers::gData_CB_Unfrequent.gPerspectiveValues, gCamera_main.GetPerspectiveValues());

	auto fixedprecise = (std::pow(2, 32) - 1.0f) / 784.0f;
	CCBuffers::gData_CB_Unfrequent.gFixedPrecise_Float32ToInt32.x = fixedprecise;
	CCBuffers::gData_CB_Unfrequent.gFixedPrecise_Float32ToInt32.y = 1 / fixedprecise;

	CCBuffers::gData_CB_Unfrequent.gNumSpotLights = Globals::gLights.mSpotLights.size();
	CCBuffers::gData_CB_Unfrequent.gNumDirectLights = Globals::gLights.mDirectionalLights.size();

	CCBuffers::gData_CB_Unfrequent.gShadowMapSize = XMFLOAT2(gShadowMapResolution, gShadowMapResolution);

	float shadowMapDxy = (1 / gShadowMapResolution);
	CCBuffers::gData_CB_Unfrequent.gShadowMapDXY = XMFLOAT2(shadowMapDxy, shadowMapDxy);

	CCBuffers::gData_CB_Unfrequent.gScreenSize = XMFLOAT2(gClientWidth, gClientHeight);
	CCBuffers::gData_CB_Unfrequent.gScreenDXY = XMFLOAT2(1.0f / gClientWidth, 1.0f / gClientHeight);


	CCBuffers::gCB_Unfrequent.Update(gContext.Get());
}

void RenderTechnique_DeferredShading::UpdateCB_PerFrame()
{
	auto& cb = CCBuffers::gData_CB_PS_Per_Frame;

	XMStoreFloat4x4(&cb.gView, gCamera_main.View());
	XMStoreFloat4x4(&cb.gViewInv, gCamera_main.ViewInv());
	XMStoreFloat4x4(&cb.gViewProj, gCamera_main.ViewProj());
	cb.gEyeDir = gCamera_main.GetLook();
	cb.gEyePos = gCamera_main.GetPosition();
	cb.gEyeZFar = gCamera_main.GetFarZ();
	cb.gEyeZNear = gCamera_main.GetNearZ();

	static float sumDt = 0.0f;
	sumDt += gDt;


	cb.gLuminanceAdaptation = std::max(gDt / (10), 1.0f);
	sumDt = 0.0f;

	CCBuffers::gCB_PS_Per_Frame.Update(gContext.Get());
}

//void RenderTechnique_DeferredShading::InitContextMenu()
//{
//	mContextMenu = CreatePopupMenu();
//
//	HMENU mShadowAlgorithmMenu = CreatePopupMenu();
//
//	AppendMenuW(mShadowAlgorithmMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_BASICSHADOWMAP, L"Basic Shadow Map");
//	AppendMenuW(mShadowAlgorithmMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_PCF, L"PCF");
//	AppendMenuW(mShadowAlgorithmMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_PCSS, L"PCSS");
//	AppendMenuW(mShadowAlgorithmMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_VSM, L"VSM");
//
//	//Generate Summed-Area Variance Shadow Map Sub-Menu
//	HMENU mSAVSMMenu = CreatePopupMenu();
//	AppendMenuW(mSAVSMMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_SAVSM_BiLinear, L"SAVSM_BiLinear");
//	AppendMenuW(mSAVSMMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_SAVSM_Linear, L"SAVSM_Linear");
//
//	AppendMenuW(mShadowAlgorithmMenu, MF_STRING | MF_POPUP, (UINT)mSAVSMMenu, L"SAVSM");
//
//	//Generate Moment Shadow Map Sub-Menu
//	HMENU mMSMMenu = CreatePopupMenu();
//
//	AppendMenuW(mMSMMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_MSM, L"Moment Shadow Map - Pixel Shader SAT");
//	AppendMenuW(mMSMMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_MSM_ComputeShaderSAT, L"Moment Shadow Map - Compute Shader SAT");
//
//	AppendMenuW(mShadowAlgorithmMenu, MF_STRING | MF_POPUP, (UINT)mMSMMenu, L"Moment Shadow Map");
//
//	//Generate Optimized Screen Space Soft Shadows Sub-Menu
//	HMENU mOSSSSMenu = CreatePopupMenu();
//
//	AppendMenuW(mOSSSSMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_OSSSS, L"OSSSS");
//	AppendMenuW(mOSSSSMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_OSSSS_PreFilterBlockers, L"OSSSS - PreFilter Blockers");
//	AppendMenuW(mOSSSSMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_OSSSS_SCREENSPACESEPARABLEBLOCKERSEARCH, L"OSSSS - Screen Space Separable Blocker Search");
//
//	AppendMenuW(mShadowAlgorithmMenu, MF_STRING | MF_POPUP, (UINT)mOSSSSMenu, L"Optimized Screen Space Soft Shadow");
//
//
//	//Separable Soft Shadow Map
//	AppendMenuW(mShadowAlgorithmMenu, MF_STRING, IDM_CONTEXT_SHADOWALGORITHM_SSSM, L"Separable Soft Shadow Map");
//
//
//	AppendMenuW(mContextMenu, MF_STRING | MF_POPUP, (UINT)mShadowAlgorithmMenu, L"Shadow Algorithms");
//}

void RenderTechnique_DeferredShading::ChangeShadowTechnique(SHADOW_TECHNIQUE shadowTechnique)
{

	mCurrentShadowTechnique = shadowTechnique;

	mCB_PS_MSM_Parameters.Release();

	switch (mCurrentShadowTechnique)
	{
	case SHADOWTECHNIQUE_BASICSHADOWMAPPING:
		mShadowTechniqueName = "BasicShadowMapping";
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_BasicShadowMap();

		mRenderPass_DeferredShading->pTexViews_ShadowMapArray = mRenderPass_GenerateShadowMap->GetTexViewShadowMap();

		ClearRenderPassVector();
		AddRenderPass(mRenderPass_GenerateShadowMap.get());
		AddRenderPass(mRenderPass_GenerateGBUffers.get());
		AddRenderPass(pRenderPass_SSAO);
		AddRenderPass(mRenderPass_DeferredShading.get());
		AddRenderPass(pRenderPass_HDR);


		break;

	case SHADOWTECHNIQUE_PCF:
		mShadowTechniqueName = "PCF";
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_PCF();

		mRenderPass_DeferredShading->pTexViews_ShadowMapArray = mRenderPass_GenerateShadowMap->GetTexViewShadowMap();

		ClearRenderPassVector();
		AddRenderPass(mRenderPass_GenerateShadowMap.get());
		AddRenderPass(mRenderPass_GenerateGBUffers.get());
		AddRenderPass(pRenderPass_SSAO);
		AddRenderPass(mRenderPass_DeferredShading.get());
		AddRenderPass(pRenderPass_HDR);

		break;

	case SHADOWTECHNIQUE_VSM:
		mShadowTechniqueName = "VSM";
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_VSM();

		mRenderPass_DeferredShading->pTexViews_ShadowMapArray = mRenderPass_GenerateVarianceShadowMap->GetTexViewVarianceShadowMap();

		ClearRenderPassVector();
		AddRenderPass(mRenderPass_GenerateVarianceShadowMap.get());
		AddRenderPass(mRenderPass_GenerateGBUffers.get());
		AddRenderPass(pRenderPass_SSAO);
		AddRenderPass(mRenderPass_DeferredShading.get());
		AddRenderPass(pRenderPass_HDR);

		break;

	case SHADOWTECHNIQUE_PCSS:
		mShadowTechniqueName = "PCSS";
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_PCSS();

		mRenderPass_DeferredShading->pTexViews_ShadowMapArray = mRenderPass_GenerateShadowMap->GetTexViewShadowMap();

		ClearRenderPassVector();
		AddRenderPass(mRenderPass_GenerateShadowMap.get());
		AddRenderPass(mRenderPass_GenerateGBUffers.get());
		AddRenderPass(pRenderPass_SSAO);
		AddRenderPass(mRenderPass_DeferredShading.get());
		AddRenderPass(pRenderPass_HDR);

		break;

	case SHADOWTECHNIQUE_SAVSM:
		mShadowTechniqueName = "SAVSM";
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_SAVSM();
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_SAVSM_Linear();

		mRenderPass_DeferredShading->pTexViews_ShadowMapArray = mRenderPass_SAVSM->GetTexViewVarianceShadowMap();

		ClearRenderPassVector();
		AddRenderPass(mRenderPass_SAVSM.get());
		AddRenderPass(mRenderPass_GenerateGBUffers.get());
		AddRenderPass(pRenderPass_SSAO);
		AddRenderPass(mRenderPass_DeferredShading.get());
		AddRenderPass(pRenderPass_HDR);


		break;

	case SHADOWTECHNIQUE_MSM:

		mCB_PS_MSM_Parameters.InitConstantBuffer(SHADER_SLOTS_PS_CB_MSM_PARAMETERS, CONSTANT_BUFFER_PS, &mData_CB_PS_MSM_Parameters, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
		mCB_PS_MSM_Parameters.SetConstantBuffer(gContext.Get());
		mCB_PS_MSM_Parameters.Update(gContext.Get());

		mShadowTechniqueName = "MSM";
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_MSM();
		mRenderPass_GenerateMomentShadowMap->ChangeSATVersion(true);


		ClearRenderPassVector();
		AddRenderPass(mRenderPass_GenerateMomentShadowMap.get());
		AddRenderPass(mRenderPass_GenerateGBUffers.get());
		AddRenderPass(pRenderPass_SSAO);
		AddRenderPass(mRenderPass_DeferredShading.get());
		AddRenderPass(pRenderPass_HDR);


		break;
	case SHADOWTECHNIQUE_SSSM:
		mShadowTechniqueName = "SSSM";
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_OSSSS();

		mRenderPass_DeferredShading->pTexViews_ShadowMapArray = mRenderPass_SSSM_GenerateShadowData->GetTexViewShadows();

		ClearRenderPassVector();
		AddRenderPass(mRenderPass_GenerateShadowMap.get());
		AddRenderPass(mRenderPass_GenerateGBUffers.get());
		AddRenderPass(mRenderPass_SSSM_GenerateShadowData.get());
		AddRenderPass(pRenderPass_SSAO);
		AddRenderPass(mRenderPass_DeferredShading.get());
		AddRenderPass(pRenderPass_HDR);


		break;

	case SHADOWTECHNIQUE_OSSSS:
		mShadowTechniqueName = "OSSSS";
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_OSSSS();

		mRenderPass_DeferredShading->pTexViews_ShadowMapArray = mRenderPass_OSSSS_GenerateShadowData->GetTexViewShadows();


		ClearRenderPassVector();
		AddRenderPass(mRenderPass_GenerateShadowMap.get());
		AddRenderPass(mRenderPass_GenerateGBUffers.get());
		AddRenderPass(mRenderPass_OSSSS_GenerateShadowData.get());
		AddRenderPass(pRenderPass_SSAO);
		AddRenderPass(mRenderPass_DeferredShading.get());
		AddRenderPass(pRenderPass_HDR);

		break;

	case SHADOWTECHNIQUE_OSSSS_SEPARABLEBLOCKERSEARCH:
		mShadowTechniqueName = "OSSSS_SeparableBlockerSearch";
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_OSSSS();

		mRenderPass_DeferredShading->pTexViews_ShadowMapArray = mRenderPass_OSSSS_GenerateShadowData__ScreenSpaceSeparableBlockerSearch->GetTexViewShadows();


		ClearRenderPassVector();
		AddRenderPass(mRenderPass_GenerateShadowMap.get());
		AddRenderPass(mRenderPass_GenerateGBUffers.get());
		AddRenderPass(mRenderPass_OSSSS_GenerateShadowData__ScreenSpaceSeparableBlockerSearch.get());
		AddRenderPass(pRenderPass_SSAO);
		AddRenderPass(mRenderPass_DeferredShading.get());
		AddRenderPass(pRenderPass_HDR);

		break;
	}

	//DrawDeferred();
}

void RenderTechnique_DeferredShading::ChangeSSAOTechnique(IRenderPass_SSAO * rs_ssao)
{
	if (pRenderPass_SSAO == rs_ssao)
		return;

	pRenderPass_SSAO = rs_ssao;

	ChangeShadowTechnique(mCurrentShadowTechnique);


	if (rs_ssao != nullptr) {

		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_SSAOManager_Default();
	}

	else {
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_SSAOManager_NoSSAO();
	}



}

RenderTechnique_DeferredShading::RenderTechnique_DeferredShading() : mCurrentShadowTechnique(SHADOWTECHNIQUE_BASICSHADOWMAPPING), pRenderPass_SSAO(nullptr), pRenderPass_HDR(nullptr)
{
}

RenderTechnique_DeferredShading::~RenderTechnique_DeferredShading() = default;

RenderTechnique_DeferredShading::RenderTechnique_DeferredShading(RenderTechnique_DeferredShading&&) = default;

RenderTechnique_DeferredShading& RenderTechnique_DeferredShading::operator=(RenderTechnique_DeferredShading&&) = default;

void RenderTechnique_DeferredShading::Init()
{
	IRenderTechnique::Init();

	this->mName = "Deferred Shading";

	/*mData_CB_PS_MSM_Parameters.gMomentBias = 6.0e-7f;
	mData_CB_PS_MSM_Parameters.gMaxDepthBias = 0.02f;
	mData_CB_PS_MSM_Parameters.gKernelSizeParameters_11_12 = XMFLOAT2(gShadowMapDXY, gShadowMapDXY);
	mData_CB_PS_MSM_Parameters.gKernelSizeParameters_21_22 = XMFLOAT2(0.0127f, 0.0127f);
	mData_CB_PS_MSM_Parameters.gKernelSizeParameters_31_32 = XMFLOAT2(78.76923f, 0.10f);
	mData_CB_PS_MSM_Parameters.gLightParameter_11_12 = XMFLOAT2(0.51124f, 0.51075f);
	mData_CB_PS_MSM_Parameters.gLightParameter_21_22 = XMFLOAT2(0.0f, 0.0f);
	mData_CB_PS_MSM_Parameters.gLightParameter_31_32 = XMFLOAT2(1.0f, 1.0f);*/


	//pRenderPass_HDR = &mRenderPass_HDR;

	//ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_Bloom_Default();

	//ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_ToneMapping_Reinhard();

}

void RenderTechnique_DeferredShading::InitGpuResources()
{

	CCBuffers::InitCommonCBs(gContext.Get());

	IRenderTechnique::InitGpuResources();


	ChangeShadowTechnique(SHADOWTECHNIQUE_BASICSHADOWMAPPING);

	UpdateCB_Unfreq();

	SetLightBuffers();

}

void RenderTechnique_DeferredShading::DestroyGpuResources()
{
	IRenderTechnique::DestroyGpuResources();

	CCBuffers::ReleaseCBs();
}

void RenderTechnique_DeferredShading::Update()
{

	for (auto& renderPasses : vRenderPasses)
	{
		renderPasses->Update();
	}

	UpdateCB_PerFrame();
}

void RenderTechnique_DeferredShading::ShowUpContextMenu(int x, int y)
{
	TrackPopupMenuEx(mContextMenu
		, TPM_LEFTALIGN | TPM_RIGHTBUTTON
		, x, y
		, gHWND_App
		, NULL);
}

void RenderTechnique_DeferredShading::Catch_WCommand(WPARAM wparam, LPARAM lparam)
{
	int wmid = LOWORD(wparam);

	switch (wmid)
	{
		

	}
}

void RenderTechnique_DeferredShading::ClearRenderPassVector()
{
	for (auto& r : vRenderPasses)
	{
		r->DestroyGPUResources();
	}

	vRenderPasses.clear();
}

void RenderTechnique_DeferredShading::ChangeRenderPass(int index, IRenderPass * renderpass)
{

	if (vRenderPasses[index] != renderpass)
	{
		vRenderPasses[index]->DestroyGPUResources();
		vRenderPasses[index] = renderpass;
		vRenderPasses[index]->InitGPUResources();
	}

}

void RenderTechnique_DeferredShading::AddRenderPass(int index, IRenderPass * renderpass)
{
	if (vRenderPasses[index] != renderpass)
	{
		renderpass->InitGPUResources();
		vRenderPasses.insert(vRenderPasses.begin() + index, renderpass);
	}
}

void RenderTechnique_DeferredShading::AddRenderPass(IRenderPass * renderpass)
{
	if (renderpass != nullptr)
	{
		renderpass->InitGPUResources();
		vRenderPasses.push_back(renderpass);
	}

}

void RenderTechnique_DeferredShading::RenderConfigurationUI()
{

	static bool EnableHDR = false;

	if (ImGui::Checkbox("HDR", &EnableHDR))
	{
		mRenderPass_DeferredShading->SetHDR(EnableHDR);

		pRenderPass_HDR = EnableHDR ? mRenderPass_HDR.get() : nullptr;

		ChangeShadowTechnique(mCurrentShadowTechnique);
	}


	if (EnableHDR)
	{
		ImGui::SameLine();

		auto& EnableBloom = mRenderPass_HDR->Enable_Bloom;

		if (ImGui::Checkbox("Bloom", &EnableBloom))
		{
			if (EnableBloom)
				ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_Bloom_Default();
			else
				ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_Bloom_None();
		}

		static std::string ToneMappingTechnique = "Reinhard";

		if (ImGui::BeginCombo("HDR Tone Mapping", ToneMappingTechnique.c_str()))
		{
			if (ImGui::Selectable("Reinhard", ToneMappingTechnique == "Reinhard"))
			{
				ToneMappingTechnique = "Reinhard";
				ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_ToneMapping_Reinhard();
			}

			if (ImGui::Selectable("ACES", ToneMappingTechnique == "ACES"))
			{
				ToneMappingTechnique = "ACES";
				ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_ToneMapping_ACES();
			}


			ImGui::EndCombo();

		}


		auto u1 = ImGui::SliderFloat("HDR: Middle Grey", &CCBuffers::gData_CB_Unfrequent.MiddleGrey, 0.001, 1.0f, "%.4f");
		u1 |= ImGui::SliderFloat("HDR: Luminance White", &CCBuffers::gData_CB_Unfrequent.LumWhiteSqr, 0.1, 10.0f, "%.1f");


		if (EnableBloom)
		{
			u1 |= ImGui::DragFloat("Bloom Threshold", &CCBuffers::gData_CB_Unfrequent.gBloomThreshold, 0.1f, .1f, 10.0f);
			u1 |= ImGui::DragFloat("Bloom Scale", &CCBuffers::gData_CB_Unfrequent.gBloomScale, 0.1f, .1f, 10.0f);
		}

		if (u1)
			UpdateCB_Unfreq();
	}



	if (ImGui::BeginCombo("Shadow Techniques", mShadowTechniqueName.c_str()))
	{

		if (ImGui::Selectable("Basic Shadow Mapping", mShadowTechniqueName == "BasicShadowMapping"))
		{
			ChangeShadowTechnique(SHADOWTECHNIQUE_BASICSHADOWMAPPING);
		}
		if (ImGui::Selectable("Percentage Closer Filtering(PCF)", mShadowTechniqueName == "PCF"))
		{
			ChangeShadowTechnique(SHADOWTECHNIQUE_PCF);
		}
		if (ImGui::Selectable("Variance Shadow Mapping(VSM)", mShadowTechniqueName == "VSM"))
		{
			ChangeShadowTechnique(SHADOWTECHNIQUE_VSM);
		}
		if (ImGui::Selectable("Percentage Closer Soft Shadows(PCSS)", mShadowTechniqueName == "PCSS"))
		{
			ChangeShadowTechnique(SHADOWTECHNIQUE_PCSS);
		}
		if (ImGui::Selectable("Summed-Area Variance Soft Shadow Mapping(SAVSM)", mShadowTechniqueName == "SAVSM"))
		{
			ChangeShadowTechnique(SHADOWTECHNIQUE_SAVSM);
		}
		if (ImGui::Selectable("Moment Shadow Mapping(MSM)", mShadowTechniqueName == "MSM"))
		{
			ChangeShadowTechnique(SHADOWTECHNIQUE_MSM);
		}
		if (ImGui::Selectable("Separable Soft Shadow Mapping(SSSM)", mShadowTechniqueName == "SSSM"))
		{
			ChangeShadowTechnique(SHADOWTECHNIQUE_SSSM);
		}
		if (ImGui::Selectable("Optimized Screen Space Soft Shadow Mapping(OSSSS)", mShadowTechniqueName == "OSSSS"))
		{
			ChangeShadowTechnique(SHADOWTECHNIQUE_OSSSS);
		}
		if (ImGui::Selectable("OSSSS - Separable Blocker Search", mShadowTechniqueName == "OSSSS_SeparableBlockerSearch"))
		{
			ChangeShadowTechnique(SHADOWTECHNIQUE_OSSSS_SEPARABLEBLOCKERSEARCH);
		}

		ImGui::EndCombo();
	}

	ImGui::Begin("Settings: SSAO");

	{
		static std::string mCurrentSSAOTechnique = "No SSAO";
		if (ImGui::BeginCombo("SSAO Technique", mCurrentSSAOTechnique.c_str()))
		{
			if (ImGui::Selectable("No SSAO", mCurrentSSAOTechnique == "No SSAO"))
			{
				mCurrentSSAOTechnique = "No SSAO";

				ChangeSSAOTechnique(nullptr);

			}
			if (ImGui::Selectable("Default SSAO", mCurrentSSAOTechnique == "Default SSAO"))
			{
				mCurrentSSAOTechnique = "Default SSAO";


				ChangeSSAOTechnique(mRenderPass_SSAO.get());
			}

			ImGui::EndCombo();
		}

		auto b1 = ImGui::DragFloat("Occlusion Radius", &CCBuffers::gData_CB_Unfrequent.gOcclusionRadius, 0.1f, 0.1f, 1000.0f);
		b1 |= ImGui::DragFloat("Occlusion Fade Start", &CCBuffers::gData_CB_Unfrequent.gOcclusionFadeStart, 0.1f, 0.1f, 1000.0f);
		b1 |= ImGui::DragFloat("Occlusion Fade End", &CCBuffers::gData_CB_Unfrequent.gOcclusionFadeEnd, 0.1f, 0.1f, 1000.0f);
		b1 |= ImGui::DragFloat("Occlusion Surface Epsilon", &CCBuffers::gData_CB_Unfrequent.gSurfaceEpsilon, 0.01f, 0.01f, 1000.0f);

		if (b1)
			UpdateCB_Unfreq();
	}

	ImGui::End();

	if (mShadowTechniqueName == "MSM")
	{
		ImGui::Begin("MSM Parameters");

		auto u1 = ImGui::DragFloat("Moment Bias", &mData_CB_PS_MSM_Parameters.gMomentBias, 0.0000001, 0.0000001, 1.0f, "%.7f");
		auto u2 = ImGui::DragFloat("Max Depth Bias", &mData_CB_PS_MSM_Parameters.gMaxDepthBias, 0.0000001, 0.0000001, 1.0f, "%.7f");

		auto u3 = ImGui::DragFloat2("Kernel Size Parameters 1*", &mData_CB_PS_MSM_Parameters.gKernelSizeParameters_11_12.x, 0.00001f, 0.00001f, 1000.0f, "%.7f");
		//auto u4 = ImGui::DragFloat2("Kernel Size Parameters 2*", &mData_CB_PS_MSM_Parameters.gKernelSizeParameters_21_22.x, 0.00001f, 0.00001f, 1000.0f, "%.7f");
		//auto u5 = ImGui::DragFloat2("Kernel Size Parameters 3*", &mData_CB_PS_MSM_Parameters.gKernelSizeParameters_31_32.x, 0.00001f, 0.00001f, 1000.0f, "%.7f");

		auto u6 = ImGui::DragFloat2("Light Parameters 1*", &mData_CB_PS_MSM_Parameters.gLightParameter_11_12.x, 0.00001f, 0.00001f, 1000.0f, "%.7f");
		auto u7 = ImGui::DragFloat2("Light Parameters 2*", &mData_CB_PS_MSM_Parameters.gLightParameter_21_22.x, 0.00001f, 0.00001f, 1000.0f, "%.7f");
		auto u8 = ImGui::DragFloat2("Light Parameters 3*", &mData_CB_PS_MSM_Parameters.gLightParameter_31_32.x, 0.00001f, 0.00001f, 1000.0f, "%.7f");

		static int MSMKernelSampleCount = 27;

		auto u9 = ImGui::DragInt("Kernel Sample Count", &MSMKernelSampleCount, 1.0f, 1, 100);

		if (u1 || u2 || u3 || u6 || u7 || u8 || u9)
		{



			mCB_PS_MSM_Parameters.Update(gContext.Get());
		}

		ImGui::End();
	}

	if (mShadowTechniqueName == "OSSSS")
	{
		ImGui::Begin("Settings: Optimized Screen Space Soft Shadows");



		static bool downSampling = false;

		if (ImGui::Checkbox("Down Sampling", &downSampling))
		{
			mRenderPass_OSSSS_GenerateShadowData->SetDownSampling(downSampling);

			ChangeShadowTechnique(SHADOWTECHNIQUE_OSSSS);
		}

		ImGui::End();
	}



	for (auto& RenderPass : vRenderPasses)
	{
		RenderPass->RenderConfigurationUI();
	}
}

//void RenderTechnique_DeferredShading::RenderConfigurationUI()
//{
//	if (ImGui::BeginCombo("Shadow Techniques", mShadowTechniqueName.c_str()))
//	{
//
//		if (ImGui::Selectable("Basic Shadow Mapping", mShadowTechniqueName == "BasicShadowMapping"))
//		{
//			ChangeShadowTechnique(SHADOWTECHNIQUE_BASICSHADOWMAPPING);
//		}
//		if (ImGui::Selectable("Percentage Closer Filtering(PCF)", mShadowTechniqueName == "PCF"))
//		{
//			ChangeShadowTechnique(SHADOWTECHNIQUE_PCF);
//		}
//		if (ImGui::Selectable("Variance Shadow Mapping(VSM)", mShadowTechniqueName == "VSM"))
//		{
//			ChangeShadowTechnique(SHADOWTECHNIQUE_VSM);
//		}
//		if (ImGui::Selectable("Percentage Closer Soft Shadows(PCSS)", mShadowTechniqueName == "PCSS"))
//		{
//			ChangeShadowTechnique(SHADOWTECHNIQUE_PCSS);
//		}
//		if (ImGui::Selectable("Summed-Area Variance Soft Shadow Mapping(SAVSM)", mShadowTechniqueName == "SAVSM"))
//		{
//			ChangeShadowTechnique(SHADOWTECHNIQUE_SAVSM);
//		}
//		if (ImGui::Selectable("Moment Shadow Mapping(MSM)", mShadowTechniqueName == "MSM"))
//		{
//			ChangeShadowTechnique(SHADOWTECHNIQUE_MSM);
//		}
//		if (ImGui::Selectable("Separable Soft Shadow Mapping(SSSM)", mShadowTechniqueName == "SSSM"))
//		{
//			ChangeShadowTechnique(SHADOWTECHNIQUE_SSSM);
//		}
//		if (ImGui::Selectable("Optimized Screen Space Soft Shadow Mapping(OSSSS)", mShadowTechniqueName == "OSSSS"))
//		{
//			ChangeShadowTechnique(SHADOWTECHNIQUE_OSSSS);
//		}
//		if (ImGui::Selectable("OSSSS - Separable Blocker Search", mShadowTechniqueName == "OSSSS_SeparableBlockerSearch"))
//		{
//			ChangeShadowTechnique(SHADOWTECHNIQUE_OSSSS_SEPARABLEBLOCKERSEARCH);
//		}
//
//		ImGui::EndCombo();
//	}
//}

void RenderTechnique_DeferredShading::RecompileShaders()
{

	for (auto rp : vRenderPasses)
	{
		rp->RecompileShaders();
	}

	ChangeSSAOTechnique(pRenderPass_SSAO);

}

RenderTechnique_DeferredShading::CB_PS_MSM_Parameters::CB_PS_MSM_Parameters() : gMomentBias(6.0e-7f), gMaxDepthBias(0.02f)
, gKernelSizeParameters_11_12(XMFLOAT4(gShadowMapDXY / 0.5f, gShadowMapDXY / 0.5f, 0.0f, 0.0f))
, gKernelSizeParameters_21_22 (XMFLOAT4(std::max(0.5f, mMSMKernelSampleCount * 0.5f - 0.5f) / gShadowMapResolution, std::max(0.5f, mMSMKernelSampleCount * 0.5f - 0.5f) / gShadowMapResolution, 0.0f, 0.0f))
, gKernelSizeParameters_31_32(XMFLOAT4(1.0f / gKernelSizeParameters_21_22.x, 0.1f, 0.0f, 0.0f))
, gLightParameter_11_12(XMFLOAT4(0.51124f, 0.51075f, 0.0f, 0.0f))
, gLightParameter_21_22(XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f))
, gLightParameter_31_32(XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f))
{

}
