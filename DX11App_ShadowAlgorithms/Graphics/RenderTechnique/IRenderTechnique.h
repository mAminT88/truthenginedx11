#pragma once

//#include "Graphics/RenderPass/IRenderPass.h"

class IRenderPass;
class GPUTimeStamp_DualBuffer;

class IRenderTechnique
{


protected:
	std::string mName;
	std::string mShadowTechniqueName;

	std::vector<IRenderPass*> vRenderPasses;
	
	std::unique_ptr<GPUTimeStamp_DualBuffer> mGpuTimeStampDB;

	std::vector<ComPtr<ID3D11Query>> mQuery_Disjoint;
	D3D11_QUERY_DATA_TIMESTAMP_DISJOINT mQueryData_Disjoint;

	HMENU mContextMenu;

	virtual void InitRenderPasses() {};
	virtual void ReleaseRenderPasses() {};
	virtual void SetSRVs() {};
	virtual void SetTexViews() {};
	virtual void InitSRVs() {};
	virtual void SetLightBuffers() {};
	virtual void AdvanceFrame();
	
	virtual void InitGPUQueries();
	
	virtual void ReleaseGPUQueries();
	

public:
	IRenderTechnique();
	virtual ~IRenderTechnique();

	IRenderTechnique(IRenderTechnique&&);
	IRenderTechnique& operator=(IRenderTechnique&&);

	virtual void Init();
	
	virtual void Draw3D();
	
	virtual void DrawDeferred();
	
	virtual void Draw2D();
	
	virtual void DrawTxt();
	
	virtual void Update();
	

	virtual void Destroy();

	virtual void InitGpuResources();
	

	virtual void DestroyGpuResources();
	

	virtual void CaptureFrame();

	virtual void RecompileShaders();
	

	virtual void ShowUpContextMenu(int x, int y);

	virtual void Catch_WCommand(WPARAM wparam, LPARAM lparam);

	virtual std::vector<std::pair<std::wstring, int>> GetVersions();
	

	virtual void RenderConfigurationUI();

	virtual std::string& GetName();
	

	virtual std::string& GetShadowTechniqueName();
	

	virtual void ProfileGPUTime();
	
};

