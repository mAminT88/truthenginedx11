#pragma once

enum MSAA_MODE : int;

class SwapChain
{

private:

	ComPtr<IDXGISwapChain> pSwapChain;

	ComPtr<ID3D11Texture2D> pTexBackBuffer;
	ComPtr<ID3D11RenderTargetView> pRTV;
	ComPtr<IDXGISurface> pIDXGISurface;


public:
	void Init(MSAA_MODE MSAAMode, UINT MSAAQualityLevel);

	void Present(UINT syncInterval = 0, UINT flags = 0)const;

	IDXGISurface* GetIDXGISurface()const;
	ID3D11RenderTargetView* GetBackBufferRTV()const;

};
