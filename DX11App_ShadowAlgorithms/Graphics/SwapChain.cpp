#include "stdafx.h"
#include "SwapChain.h"
#include "enums.h"
#include "Globals.h"

using namespace Globals;

void SwapChain::Init(MSAA_MODE MSAAMode, UINT MSAAQualityLevel)
{
	DXGI_SWAP_CHAIN_DESC swapdesc;
	ZeroMemory(&swapdesc, sizeof(DXGI_SWAP_CHAIN_DESC));

	swapdesc.BufferCount = 1;
	swapdesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapdesc.Windowed = TRUE;
	swapdesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	swapdesc.OutputWindow = gHWND_Rendering;
	swapdesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
	swapdesc.BufferDesc.Height = gClientHeight;
	swapdesc.BufferDesc.Width = gClientWidth;
	swapdesc.BufferDesc.RefreshRate.Numerator = 60;
	swapdesc.BufferDesc.RefreshRate.Denominator = 1;
	swapdesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapdesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapdesc.Flags = 0;
	swapdesc.SampleDesc.Count = MSAAMode;
	swapdesc.SampleDesc.Quality = MSAAQualityLevel;


	ComPtr<IDXGIDevice> idxgi_device;
	gDevice.As(&idxgi_device);

	ComPtr<IDXGIAdapter> idxgi_adapter;
	idxgi_device->GetParent(IID_PPV_ARGS(&idxgi_adapter));

	ComPtr<IDXGIFactory> idxgi_factory;
	idxgi_adapter->GetParent(IID_PPV_ARGS(&idxgi_factory));

	idxgi_factory->CreateSwapChain(gDevice.Get(), &swapdesc, pSwapChain.ReleaseAndGetAddressOf());


	pSwapChain->GetBuffer(0, IID_PPV_ARGS(pTexBackBuffer.ReleaseAndGetAddressOf()));
	pSwapChain->GetBuffer(0, IID_PPV_ARGS(pIDXGISurface.ReleaseAndGetAddressOf()));

	gDevice->CreateRenderTargetView(pTexBackBuffer.Get(), nullptr, pRTV.ReleaseAndGetAddressOf());
}

IDXGISurface * SwapChain::GetIDXGISurface() const
{
	return pIDXGISurface.Get();
}

ID3D11RenderTargetView * SwapChain::GetBackBufferRTV() const
{
	return pRTV.Get();
}

void SwapChain::Present(UINT syncInterval, UINT flags) const
{
	pSwapChain->Present(syncInterval, flags);
}
