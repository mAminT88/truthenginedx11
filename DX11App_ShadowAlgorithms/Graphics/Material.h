#pragma once


#include "XMathSerialization.h"
#include "ShadingColor.h"
#include "Graphics\ShaderList\IPixelShader.h"


class IMesh;
class Texture;

class Material
{


public:

	Material();

	std::string mName = "";

	int ID;

	std::string mDiffuseTextureKey, mNormalTextureKey, mDisplacementTextureKey;

	bool IsTransparent;

	ShadingColor mShadingColor;

	void SetDiffueMap(Texture* diffusem, std::string& DiffuseTextureKey);
	void SetNormalMap(Texture* normalm, std::string& NormalTextureIndex);
	void SetDisplacementMap(Texture* displacementm, std::string& DisplacementTextureIndex);

	void SetShaderClass_Material(SURFACE_SHADER_CLASSINSTANCES_MATERIAL shaderClass);
	void SetShaderClass_Normal(SURFACE_SHADER_CLASSINSTANCES_NORMAL shaderClass);


	void SetShaderResources_PS(UINT SRVSlot_diffuse_normal_displacement, ID3D11DeviceContext* context);

	virtual SURFACE_SHADER_CLASSINSTANCES_MATERIAL GetSurfaceShaderClassInstance_Material();
	virtual SURFACE_SHADER_CLASSINSTANCES_NORMAL GetSurfaceShaderClassInstance_Normal();

	virtual void AddMesh(IMesh* mesh);
	virtual const std::vector<IMesh*>& GetMeshes();

private:

	Texture* p_diffusemap;
	Texture* p_normalmap;
	Texture* p_displacementmap;

	SURFACE_SHADER_CLASSINSTANCES_MATERIAL mShaderClassInstance_material;
	SURFACE_SHADER_CLASSINSTANCES_NORMAL mShaderClassInstance_normal;

	std::vector<ID3D11ShaderResourceView*> vSRVs;

	std::vector<IMesh*> mMeshes;

#pragma region Serialization

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive& ar, const unsigned int version)
	{
		ar& ID;
		ar& mName;
		ar& mDiffuseTextureKey;
		ar& mNormalTextureKey;
		ar& mDisplacementTextureKey;
		ar& IsTransparent;
		ar& mShadingColor.diffuseColor;
		ar& mShadingColor.ambientColor;
		ar& mShadingColor.specularColor;
		ar& mShaderClassInstance_material;
		ar& mShaderClassInstance_normal;
	}



	template<class Archive>
	void save(Archive& ar, const unsigned int version) const
	{
		ar& ID;
		ar& mName;
		ar& mDiffuseTextureKey;
		ar& mNormalTextureKey;
		ar& mDisplacementTextureKey;
		ar& IsTransparent;
		ar& mShadingColor.diffuseColor;
		ar& mShadingColor.ambientColor;
		ar& mShadingColor.specularColor;
		ar& mShaderClassInstance_material;
		ar& mShaderClassInstance_normal;
	}

	BOOST_SERIALIZATION_SPLIT_MEMBER();

#pragma endregion Serialization




};