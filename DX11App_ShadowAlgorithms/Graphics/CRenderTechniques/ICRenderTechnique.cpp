#include "stdafx.h"
#include "ICRenderTechnique.h"


ICRenderTechnique::ICRenderTechnique() = default;

ICRenderTechnique::~ICRenderTechnique() = default;

ICRenderTechnique::ICRenderTechnique(ICRenderTechnique&&) = default;

ICRenderTechnique& ICRenderTechnique::operator=(ICRenderTechnique&&) = default;


void ICRenderTechnique::AdvanceFrame()
{
}

void ICRenderTechnique::Init()
{
}

void ICRenderTechnique::Destroy()
{
}

void ICRenderTechnique::Draw3D()
{
}

void ICRenderTechnique::Update()
{
}

void ICRenderTechnique::InitGpuResources()
{
}

void ICRenderTechnique::DestroyGpuResources()
{
}

void ICRenderTechnique::RecompileShaders()
{
}

void ICRenderTechnique::RenderConfigurationUI()
{
}

std::string& ICRenderTechnique::GetName()
{
	return mName;
}

