#pragma once

class ICRenderTechnique
{
protected:
	std::string mName;

	virtual void AdvanceFrame();

public:

	ICRenderTechnique();
	virtual ~ICRenderTechnique();

	ICRenderTechnique(ICRenderTechnique&&);
	ICRenderTechnique& operator=(ICRenderTechnique&&);

	virtual void Init();

	virtual void Destroy();

	virtual void Draw3D();

	virtual void Update();

	virtual void InitGpuResources();

	virtual void DestroyGpuResources();

	virtual void RecompileShaders();

	virtual void RenderConfigurationUI();

	virtual std::string& GetName();

};
