#include "stdafx.h"
#include "RenderStates.h"
#include "MathHelper.h"
#include "Globals.h"

using namespace Globals;

ComPtr<ID3D11RasterizerState>  RenderStates::RSCounterClockwise = nullptr;
ComPtr<ID3D11RasterizerState>  RenderStates::RSCounterClockwiseWireFrame = nullptr;
ComPtr<ID3D11RasterizerState>  RenderStates::RSWireframe = nullptr;
ComPtr<ID3D11RasterizerState>  RenderStates::RSNoCulling = nullptr;
ComPtr<ID3D11RasterizerState>  RenderStates::RSShadowMap = nullptr;
ComPtr<ID3D11RasterizerState>  RenderStates::RSShadowMap_ReversedZ = nullptr;

ComPtr<ID3D11DepthStencilState> RenderStates::DSNoDepth = nullptr;
ComPtr<ID3D11DepthStencilState> RenderStates::DSReversedZ = nullptr;

ComPtr<ID3D11SamplerState> RenderStates::SSLinearWrap = nullptr;
ComPtr<ID3D11SamplerState> RenderStates::SSLinearClamp = nullptr;
ComPtr<ID3D11SamplerState> RenderStates::SSLinearBorder0 = nullptr;
ComPtr<ID3D11SamplerState> RenderStates::SSLinearBorder1 = nullptr;
ComPtr<ID3D11SamplerState> RenderStates::SSComparisonState_Less = nullptr;
ComPtr<ID3D11SamplerState> RenderStates::SSComparisonState_Greater = nullptr;
ComPtr<ID3D11SamplerState> RenderStates::SSPointBorderColor0 = nullptr;
ComPtr<ID3D11SamplerState> RenderStates::SSPointBorderColor1 = nullptr;
ComPtr<ID3D11SamplerState> RenderStates::SSPointWrap = nullptr;
ComPtr<ID3D11SamplerState> RenderStates::SSPointClamp = nullptr;


ComPtr<ID3D11BlendState> RenderStates::BSUseTransparency = nullptr;
ComPtr<ID3D11BlendState> RenderStates::BSNoWriteToBackBuffer = nullptr;

RENDER_STATE_RS RenderStates::mCurrentRSState = RENDER_STATE_RS_DEFAULT;

RENDER_STATE_DS RenderStates::mCurrentDSState = RENDER_STATE_DS_DEFAULT;

RENDER_STATE_BS RenderStates::mCurrentBSState = RENDER_STATE_BS_DEFAULT;

ComPtr<ID3D11DepthStencilState> RenderStates::DSSetStencil = nullptr;

float RenderStates::DepthBias = 50.0f;

float RenderStates::SlopeScaledDepthBias = 25.0f;

float RenderStates::DepthBiasClamp = 0.0f;

void RenderStates::InitAll()
{
	D3D11_RASTERIZER_DESC RSCCDesc;
	ZeroMemory(&RSCCDesc, sizeof(D3D11_RASTERIZER_DESC));
	RSCCDesc.CullMode = D3D11_CULL_BACK;
	RSCCDesc.DepthClipEnable = true;
	RSCCDesc.FillMode = D3D11_FILL_SOLID;
	RSCCDesc.FrontCounterClockwise = true;

	gDevice->CreateRasterizerState(&RSCCDesc, &RSCounterClockwise);

	D3D11_RASTERIZER_DESC RSCCWireFrameDesc;
	ZeroMemory(&RSCCWireFrameDesc, sizeof(D3D11_RASTERIZER_DESC));
	RSCCWireFrameDesc.CullMode = D3D11_CULL_BACK;
	RSCCWireFrameDesc.DepthClipEnable = true;
	RSCCWireFrameDesc.FillMode = D3D11_FILL_WIREFRAME;
	RSCCWireFrameDesc.FrontCounterClockwise = true;

	gDevice->CreateRasterizerState(&RSCCWireFrameDesc, &RSCounterClockwiseWireFrame);

	RSCCDesc.FrontCounterClockwise = false;
	RSCCDesc.FillMode = D3D11_FILL_WIREFRAME;

	gDevice->CreateRasterizerState(&RSCCDesc, &RSWireframe);

	D3D11_DEPTH_STENCIL_DESC dsdesc;
	ZeroMemory(&dsdesc, sizeof(D3D11_DEPTH_STENCIL_DESC));
	dsdesc.DepthEnable = false;
	dsdesc.DepthFunc = D3D11_COMPARISON_ALWAYS;
	dsdesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsdesc.StencilEnable = false;
	dsdesc.StencilReadMask = 0;
	dsdesc.StencilWriteMask = 0;

	gDevice->CreateDepthStencilState(&dsdesc, &DSNoDepth);

	//FLOAT borderColor[4] = {0.0f, 0.0f, 0.0f, 1.0f};

	/*Samplers*/

	D3D11_SAMPLER_DESC sdesc;
	ZeroMemory(&sdesc, sizeof(D3D11_SAMPLER_DESC));
	sdesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sdesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sdesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	sdesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	sdesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	sdesc.MaxAnisotropy = 1;

	gDevice->CreateSamplerState(&sdesc, &SSLinearWrap);

	sdesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sdesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sdesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;

	gDevice->CreateSamplerState(&sdesc, &SSLinearClamp);

	sdesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	sdesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	sdesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	sdesc.BorderColor[0] = 0.0f;
	sdesc.BorderColor[1] = 0.0f;
	sdesc.BorderColor[2] = 0.0f;
	sdesc.BorderColor[3] = 0.0f;

	gDevice->CreateSamplerState(&sdesc, &SSLinearBorder0);

	sdesc.BorderColor[0] = 1.0f;
	sdesc.BorderColor[1] = 1.0f;
	sdesc.BorderColor[2] = 1.0f;
	sdesc.BorderColor[3] = 1.0f;

	gDevice->CreateSamplerState(&sdesc, &SSLinearBorder1);

	sdesc.Filter = D3D11_FILTER_MIN_MAG_MIP_POINT;
	sdesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	sdesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	sdesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;

	gDevice->CreateSamplerState(&sdesc, &SSPointWrap);

	sdesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	sdesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	sdesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;

	gDevice->CreateSamplerState(&sdesc, &SSPointClamp);


	sdesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	sdesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	sdesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	sdesc.BorderColor[0] = 0.0f;
	sdesc.BorderColor[1] = 0.0f;
	sdesc.BorderColor[2] = 0.0f;
	sdesc.BorderColor[3] = 0.0f;

	gDevice->CreateSamplerState(&sdesc, &SSPointBorderColor0);

	sdesc.BorderColor[0] = 1.0f;
	sdesc.BorderColor[1] = 1.0f;
	sdesc.BorderColor[2] = 1.0f;
	sdesc.BorderColor[3] = 1.0f;

	gDevice->CreateSamplerState(&sdesc, &SSPointBorderColor1);

	sdesc.AddressU = D3D11_TEXTURE_ADDRESS_BORDER;
	sdesc.AddressV = D3D11_TEXTURE_ADDRESS_BORDER;
	sdesc.AddressW = D3D11_TEXTURE_ADDRESS_BORDER;
	sdesc.ComparisonFunc = D3D11_COMPARISON_LESS;
	sdesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
	sdesc.MaxAnisotropy = 1;

	gDevice->CreateSamplerState(&sdesc, &SSComparisonState_Less);

	sdesc.ComparisonFunc = D3D11_COMPARISON_GREATER;

	gDevice->CreateSamplerState(&sdesc, &SSComparisonState_Greater);


	/*Rasterizer States*/

	D3D11_RASTERIZER_DESC rsdesc;
	rsdesc.AntialiasedLineEnable = false;
	rsdesc.CullMode = D3D11_CULL_NONE;
	rsdesc.DepthBias = DepthBias;
	rsdesc.DepthBiasClamp = DepthBiasClamp;
	rsdesc.DepthClipEnable = true;
	rsdesc.FillMode = D3D11_FILL_SOLID;
	rsdesc.FrontCounterClockwise = false;
	rsdesc.MultisampleEnable = false;
	rsdesc.ScissorEnable = false;
	rsdesc.SlopeScaledDepthBias = SlopeScaledDepthBias;

	gDevice->CreateRasterizerState(&rsdesc, &RSShadowMap);

	rsdesc.DepthBias *= -1;
	rsdesc.DepthBiasClamp *= -1;
	rsdesc.SlopeScaledDepthBias *= -1;

	gDevice->CreateRasterizerState(&rsdesc, &RSShadowMap_ReversedZ);

	rsdesc.CullMode = D3D11_CULL_NONE;
	rsdesc.DepthBias = 0;

	gDevice->CreateRasterizerState(&rsdesc, &RSNoCulling);

	D3D11_BLEND_DESC bsdesc;
	bsdesc.AlphaToCoverageEnable = false;
	bsdesc.IndependentBlendEnable = false;
	bsdesc.RenderTarget[0].BlendEnable = true;
	bsdesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	bsdesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	bsdesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	bsdesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	bsdesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	bsdesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	bsdesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

	gDevice->CreateBlendState(&bsdesc, &BSUseTransparency);

	bsdesc.RenderTarget[0].BlendEnable = false;
	bsdesc.RenderTarget[0].RenderTargetWriteMask = 0;

	gDevice->CreateBlendState(&bsdesc, &BSNoWriteToBackBuffer);

	dsdesc.DepthEnable = true;
	dsdesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	dsdesc.DepthFunc = D3D11_COMPARISON_LESS;
	dsdesc.StencilEnable = true;
	dsdesc.StencilReadMask = 0xff;
	dsdesc.StencilWriteMask = 0xff;
	dsdesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsdesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsdesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	dsdesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_REPLACE;
	dsdesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	dsdesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
	dsdesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	dsdesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_REPLACE;

	gDevice->CreateDepthStencilState(&dsdesc, &DSSetStencil);

	dsdesc.DepthEnable = true;
	dsdesc.DepthFunc = D3D11_COMPARISON_GREATER_EQUAL;
	dsdesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	dsdesc.StencilEnable = false;

	gDevice->CreateDepthStencilState(&dsdesc, &DSReversedZ);
}

void RenderStates::DestroyAll()
{

	DSNoDepth.Reset();
	DSSetStencil.Reset();
	DSReversedZ.Reset();

	SSLinearWrap.Reset();
	SSLinearClamp.Reset();
	SSLinearBorder0.Reset();
	SSLinearBorder1.Reset();
	SSPointWrap.Reset();
	SSPointClamp.Reset();
	SSPointBorderColor0.Reset();
	SSPointBorderColor1.Reset();
	SSComparisonState_Greater.Reset();
	SSComparisonState_Less.Reset();

	RSCounterClockwise.Reset();
	RSCounterClockwiseWireFrame.Reset();
	RSShadowMap.Reset();
	RSShadowMap_ReversedZ.Reset();
	RSNoCulling.Reset();
	RSWireframe.Reset();


	BSUseTransparency.Reset();
	BSNoWriteToBackBuffer.Reset();
}

void RenderStates::SetSamplerStates(ID3D11DeviceContext* _context)
{
	_context->PSSetSamplers((int)SAMPLER_SLOTS::BILINEAR_WRAP, 1, SSLinearWrap.GetAddressOf());
	_context->PSSetSamplers((int)SAMPLER_SLOTS::BILINEAR_CLAMP, 1, SSLinearClamp.GetAddressOf());
	_context->PSSetSamplers((int)SAMPLER_SLOTS::BILINEAR_BORDER_0, 1, SSLinearBorder0.GetAddressOf());
	_context->PSSetSamplers((int)SAMPLER_SLOTS::BILINEAR_BORDER_1, 1, SSLinearBorder1.GetAddressOf());
	_context->PSSetSamplers((int)SAMPLER_SLOTS::POINT_WRAP, 1, SSPointWrap.GetAddressOf());
	_context->PSSetSamplers((int)SAMPLER_SLOTS::POINT_CLAMP, 1, SSPointClamp.GetAddressOf());
	_context->PSSetSamplers((int)SAMPLER_SLOTS::POINT_BORDER_0, 1, SSPointBorderColor0.GetAddressOf());
	_context->PSSetSamplers((int)SAMPLER_SLOTS::POINT_BORDER_1, 1, SSPointBorderColor1.GetAddressOf());
	_context->PSSetSamplers((int)SAMPLER_SLOTS::COMPARISON_LESS, 1, SSComparisonState_Less.GetAddressOf());
	_context->PSSetSamplers((int)SAMPLER_SLOTS::COMPARISON_GREATER, 1, SSComparisonState_Greater.GetAddressOf());

	_context->CSSetSamplers((int)SAMPLER_SLOTS::BILINEAR_WRAP, 1, SSLinearWrap.GetAddressOf());
	_context->CSSetSamplers((int)SAMPLER_SLOTS::BILINEAR_CLAMP, 1, SSLinearClamp.GetAddressOf());
	_context->CSSetSamplers((int)SAMPLER_SLOTS::BILINEAR_BORDER_0, 1, SSLinearBorder0.GetAddressOf());
	_context->CSSetSamplers((int)SAMPLER_SLOTS::BILINEAR_BORDER_1, 1, SSLinearBorder1.GetAddressOf());
	_context->CSSetSamplers((int)SAMPLER_SLOTS::POINT_WRAP, 1, SSPointWrap.GetAddressOf());
	_context->CSSetSamplers((int)SAMPLER_SLOTS::POINT_CLAMP, 1, SSPointClamp.GetAddressOf());
	_context->CSSetSamplers((int)SAMPLER_SLOTS::POINT_BORDER_0, 1, SSPointBorderColor0.GetAddressOf());
	_context->CSSetSamplers((int)SAMPLER_SLOTS::POINT_BORDER_1, 1, SSPointBorderColor1.GetAddressOf());
	_context->CSSetSamplers((int)SAMPLER_SLOTS::COMPARISON_LESS, 1, SSComparisonState_Less.GetAddressOf());
	_context->CSSetSamplers((int)SAMPLER_SLOTS::COMPARISON_GREATER, 1, SSComparisonState_Greater.GetAddressOf());
}


void RenderStates::SetBlendState(RENDER_STATE_BS _bsType, ID3D11DeviceContext* _context)
{
	/*if (_context.Get() == gContext.Get() && _bsType == mCurrentBSState)
		return;

	mCurrentBSState = _bsType;*/

	float blendfactor[] ={ 0.0f, 0.0f, 0.0f, 0.0f };
	switch (_bsType)
	{
	case RENDER_STATE_BS_NONE:
		break;
	case RENDER_STATE_BS_DEFAULT:
		_context->OMSetBlendState(nullptr, blendfactor, 0xffffffff);
		break;
	case RENDER_STATE_BS_NOWRITE_BACKBUFFER:
		_context->OMSetBlendState(BSNoWriteToBackBuffer.Get(), blendfactor, 0xffffffff);
		break;
	case RENDER_STATE_BS_TRANSPARENCY:
		_context->OMSetBlendState(BSUseTransparency.Get(), blendfactor, 0xffffffff);
		break;
	}
	return;
}

void RenderStates::SetDepthStencilState(RENDER_STATE_DS _dsType, ID3D11DeviceContext* _context)
{

	/*if (_context.Get() == gContext.Get() && mCurrentDSState == _dsType)
		return;

	mCurrentDSState = _dsType;*/

	switch (_dsType)
	{
	case RENDER_STATE_DS_NONE:
		break;
	case RENDER_STATE_DS_DEFAULT:
		_context->OMSetDepthStencilState(nullptr, 0);
		break;
	case RENDER_STATE_DS_NODEPTH:
		_context->OMSetDepthStencilState(DSNoDepth.Get(), 0);
		break;
	case RENDER_STATE_DS_STENCIL_ENABLE:
		_context->OMSetDepthStencilState(DSSetStencil.Get(), 0);
		break;
	case RENDER_STATE_DS_REVERSEDZ:
		_context->OMSetDepthStencilState(DSReversedZ.Get(), 0);
		break;
	}
	return;
}

void RenderStates::SetRasterizerState(RENDER_STATE_RS _rsType, ID3D11DeviceContext* _context)
{

	/*if (_context.Get() == gContext.Get() && mCurrentRSState == _rsType)
		return;

	mCurrentRSState = _rsType;*/

	switch (_rsType)
	{
	case RENDER_STATE_RS_NONE:
		break;
	case RENDER_STATE_RS_DEFAULT:
		_context->RSSetState(nullptr);
		break;
	case RENDER_STATE_RS_NOCULLING:
		_context->RSSetState(RSNoCulling.Get());
		break;
	case RENDER_STATE_RS_SHADOWMAP:
		_context->RSSetState(RSShadowMap.Get());
		break;
	case RENDER_STATE_RS_SHADOWMAP_ReversedZ:
		_context->RSSetState(RSShadowMap_ReversedZ.Get());
		break;
	case RENDER_STATE_RS_COUNTERCLOCKWISE:
		_context->RSSetState(RSCounterClockwise.Get());
		break;
	case RENDER_STATE_RS_COUNTERCLOCKWISE_WIREFRAME:
		_context->RSSetState(RSCounterClockwiseWireFrame.Get());
		break;
	case RENDER_STATE_RS_WIREFRAME:
		_context->RSSetState(RSWireframe.Get());
		break;
	}
	return;
}
