#include "stdafx.h"
#include "Texture.h"
#include "MathHelper.h"
#include "Globals.h"
#include "ConstantBuffer.h"

#include "ErrorLogger.h"

#ifdef USE_DIRECTXTEX


#else

#include "HelperComponent/WICTextureLoader.h"
#include "HelperComponent/DDSTextureLoader.h"

#endif

using namespace Globals;

#ifdef USE_DIRECTXTEX

Texture::Texture() : mFilePath(L""), mScratchImage(new ScratchImage()), mSRV(nullptr), ID(-1)
{
}



Texture::Texture(const std::wstring& _filePath, TEXTURE_FORMAT _textureFormat /*= TEXTURE_FORMAT_WIC*/) : mScratchImage(new ScratchImage())
{
	InitTextureFromFile(_filePath, _textureFormat);
}



Texture::Texture(const uint8_t* data, int width, int height, TEXTURE_FORMAT _textureFormat /*= TEXTURE_FORMAT_WIC*/) : mScratchImage(new ScratchImage())
{
	size_t dataSize;
	if (height == 0)
		dataSize = width;
	else
		dataSize = width * height;

	InitTextureFromMemory(data, dataSize, _textureFormat);
}


Texture::Texture(Texture&& texture)
{
	mFilePath = std::move(texture.mFilePath);
	mSRV = std::move(texture.mSRV);
	mScratchImage = std::move(texture.mScratchImage);
}


Texture::Texture(const Texture& texture)
{

	mFilePath = texture.mFilePath;
	mSRV = texture.mSRV;
	mScratchImage = texture.mScratchImage;
}


void Texture::InitTextureFromFile(const std::wstring& _filePath, TEXTURE_FORMAT _textureFormat /*= TEXTURE_FORMAT_WIC*/)
{
	switch (_textureFormat)
	{
	case TEXTURE_FORMAT_DDS:
		InitTextureFromDDSFile(_filePath);
		break;
	case TEXTURE_FORMAT_WIC:
		InitTextureFromWICFile(_filePath);
		break;
	default:
		break;
	}
}


void Texture::InitTextureFromMemory(const uint8_t* data, size_t dataSize, TEXTURE_FORMAT _textureFormat /*= TEXTURE_FORMAT_WIC*/)
{
	switch (_textureFormat)
	{
	case TEXTURE_FORMAT_DDS:
		InitTextureFromDDSMemory(data, dataSize);
		break;
	case TEXTURE_FORMAT_WIC:
		InitTextureFromWICMemory(data, dataSize);
		break;
	default:
		break;
	}
}



void Texture::InitTextureFromWICFile(const std::wstring& _filePath)
{
	mFilePath = _filePath;

	if (_filePath != L"")
	{

		/*ScratchImage sImage;*/

		DirectX::LoadFromWICFile(_filePath.c_str(), WIC_FLAGS_NONE, nullptr, *mScratchImage/*sImage*/);

		//DirectX::Compress(gDevice.Get(), *sImage.GetImage(0, 0, 0), DXGI_FORMAT_BC7_UNORM, TEX_COMPRESS_PARALLEL, 0, *mScratchImage);

		DirectX::CreateShaderResourceViewEx(gDevice.Get(), mScratchImage->GetImage(0, 0, 0), 1, mScratchImage->GetMetadata(), D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, true, mSRV.ReleaseAndGetAddressOf());
	}
}


void Texture::InitTextureFromWICMemory(const uint8_t * data, size_t dataSize)
{
	if (data != nullptr)
	{
		ScratchImage sImage;
		DirectX::LoadFromWICMemory(data, dataSize, WIC_FLAGS_FORCE_SRGB, nullptr, sImage);
		DirectX::Compress(gDevice.Get(), *sImage.GetImage(0, 0, 0), DXGI_FORMAT_BC7_UNORM_SRGB, TEX_COMPRESS_PARALLEL, 0, *mScratchImage);
		DirectX::CreateShaderResourceViewEx(gDevice.Get(), mScratchImage->GetImage(0, 0, 0), 1, mScratchImage->GetMetadata(), D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, true, mSRV.ReleaseAndGetAddressOf());
	}
}

void Texture::InitTextureFromDDSFile(const std::wstring& _filePath)
{
	mFilePath = _filePath;

	if (_filePath != L"")
	{

		DirectX::LoadFromDDSFile(_filePath.c_str(), WIC_FLAGS_FORCE_SRGB, nullptr, *mScratchImage);

		DirectX::CreateShaderResourceViewEx(gDevice.Get(), mScratchImage->GetImage(0, 0, 0), 1, mScratchImage->GetMetadata(), D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, true, mSRV.ReleaseAndGetAddressOf());
	}
}

void Texture::InitTextureFromDDSMemory(const uint8_t * data, size_t dataSize)
{
	if (data != nullptr)
	{
		DirectX::LoadFromWICMemory(data, dataSize, WIC_FLAGS_FORCE_SRGB, nullptr, *mScratchImage);
		DirectX::CreateShaderResourceViewEx(gDevice.Get(), mScratchImage->GetImage(0, 0, 0), 1, mScratchImage->GetMetadata(), D3D11_USAGE_DEFAULT, D3D11_BIND_SHADER_RESOURCE, 0, 0, true, mSRV.ReleaseAndGetAddressOf());
	}
}


void Texture::SetSRV(const UINT _registerSlot) const noexcept
{
	if (mSRV.Get() != nullptr)
		gContext->PSSetShaderResources(_registerSlot, 1, mSRV.GetAddressOf());
	return;
}

void Texture::Release() noexcept
{
	mSRV.Reset();
}

ID3D11ShaderResourceView* Texture::GetSRV() const noexcept
{
	return mSRV.Get();
}

const std::wstring& Texture::GetFilePath() const noexcept
{
	return mFilePath;
}

#else

Texture::Texture() : mFilePath(L""), mSRV(nullptr), ID(-1)
{
}


Texture::Texture(std::wstring _filePath, Texture_TYPE _textureType, TEXTURE_FORMAT _textureFormat /*= TEXTURE_FORMAT_WIC*/)
{
	InitTextureFromFile(_filePath, _textureType, _textureFormat);
}

Texture::Texture(const uint8_t* data, int width, int height, Texture_TYPE _textureType, TEXTURE_FORMAT _textureFormat /*= TEXTURE_FORMAT_WIC*/)
{

	size_t dataSize;
	if (height == 0)
		dataSize = width;
	else
		dataSize = width * height;


	InitTextureFromMemory(data, dataSize, TEXTURE_2D, TEXTURE_FORMAT_WIC);
}

void Texture::InitTextureFromFile(std::wstring _filePath, Texture_TYPE _textureType, TEXTURE_FORMAT _textureFormat)
{
	if (_filePath != L"")
	{
		mFilePath = _filePath;

		std::ifstream fin(_filePath, std::ios::binary);
		fin.seekg(0, std::ios::end);
		size_t dataSize = fin.tellg();
		uint8_t* data = new uint8_t[dataSize];
		fin.seekg(0, std::ios::beg);
		fin.read((char*)data, dataSize);

		switch (_textureType)
		{
		case TEXTURE_2D:
		{
			if (_textureFormat == TEXTURE_FORMAT_WIC)
			{
			
				InitTextureFromMemory(data, dataSize, TEXTURE_2D, TEXTURE_FORMAT_WIC);
				break;
			}
			if (_textureFormat == TEXTURE_FORMAT_DDS)
			{

				InitTextureFromMemory(data, dataSize, TEXTURE_2D, TEXTURE_FORMAT_DDS);
				break;
			}
		}
		case TEXTURE_2D_ARRAY:
		{
			InitTextureFromMemory(data, dataSize, TEXTURE_2D_ARRAY, TEXTURE_FORMAT_DDS);

			break;
		}
		default:
			break;
		}

		std::unique_ptr<ScratchImage> sImage = std::make_unique<ScratchImage>();
		std::unique_ptr<TexMetadata> metaData = std::make_unique<TexMetadata>();

		DirectX::LoadFromWICFile(_filePath.c_str(), WIC_FLAGS_FORCE_SRGB, metaData.get(), *sImage);

		ScratchImage csImage;

		DirectX::Compress(gDevice.Get(), *sImage->GetImage(0, 0, 0), DXGI_FORMAT_BC7_UNORM_SRGB, TEX_COMPRESS_PARALLEL, 0, csImage);

		ComPtr<ID3D11ShaderResourceView> srv;


		DirectX::CreateShaderResourceView(gDevice.Get(), csImage.GetImage(0, 0, 0), 1, csImage.GetMetadata(), srv.ReleaseAndGetAddressOf());

		return;
	}
}

void Texture::InitTextureFromMemory(const uint8_t * data, size_t dataSize, Texture_TYPE _textureType, TEXTURE_FORMAT _textureFormat /*= TEXTURE_FORMAT_WIC*/)
{
	if (data != nullptr)
	{
		mType = _textureType;
		mFormat = _textureFormat;

		ComPtr<ID3D11Resource> tex;

		mData.reset(const_cast<uint8_t*>(data));
		mDataSize = dataSize;

		switch (_textureType)
		{
		case TEXTURE_2D:
		{

			if (_textureFormat == TEXTURE_FORMAT_DDS)
			{
				auto hr = CreateDDSTextureFromMemoryEx(gDevice.Get()
					, gContext.Get()
					, data
					, dataSize
					, 0
					, D3D11_USAGE_DEFAULT
					, D3D11_BIND_SHADER_RESOURCE
					, 0
					, 0
					, WIC_LOADER_FORCE_SRGB
					, tex.ReleaseAndGetAddressOf()
					, mSRV.ReleaseAndGetAddressOf());
			}
			else if (_textureFormat == TEXTURE_FORMAT_WIC)
			{


				auto hr = CreateWICTextureFromMemoryEx(gDevice.Get()
					, gContext.Get()
					, data
					, dataSize
					, 0
					, D3D11_USAGE_DEFAULT
					, D3D11_BIND_SHADER_RESOURCE
					, 0
					, 0
					, WIC_LOADER_FORCE_SRGB
					, tex.ReleaseAndGetAddressOf()
					, mSRV.ReleaseAndGetAddressOf()
				);
			}

			break;
		}
		case TEXTURE_2D_ARRAY:
		{
			auto hr = CreateDDSTextureFromMemoryEx(gDevice.Get()
				, gContext.Get()
				, data
				, dataSize
				, 0
				, D3D11_USAGE_DEFAULT
				, D3D11_BIND_SHADER_RESOURCE
				, 0
				, 0
				, WIC_LOADER_FORCE_SRGB
				, tex.ReleaseAndGetAddressOf()
				, mSRV.ReleaseAndGetAddressOf());

			break;
		}


		default:
			break;
		}
	}
}


void Texture::SetSRV(const UINT _registerSlot) const noexcept
{
	if (mSRV.Get() != nullptr)
		gContext->PSSetShaderResources(_registerSlot, 1, mSRV.GetAddressOf());
	return;
}

void Texture::Release() noexcept
{
	mSRV.Reset();
}

ID3D11ShaderResourceView* Texture::GetSRV() const noexcept
{
	return mSRV.Get();
}

const std::wstring& Texture::GetFilePath() const noexcept
{
	return mFilePath;
}

#endif 
