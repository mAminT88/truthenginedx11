#pragma once




using namespace DirectX;

enum VERTEX_TYPE : int
{
	VERTEX_TYPE_NONE,
	VERTEX_TYPE_SIMPLE,
	VERTEX_TYPE_POINTBILLBOARD,
	VERTEX_TYPE_BASIC32,
	VERTEX_TYPE_BASIC32_TANGET,
	VERTEX_TYPE_PARTICLE,
	VERTEX_TYPE_SKINNED
};

namespace Vertex {

	struct Basic32
	{
		XMFLOAT3 Pos;
		XMFLOAT3 Normal;
		XMFLOAT2 Tex;
	};
	
	struct PointBillboard
	{
		XMFLOAT3 Pos;
		XMFLOAT2 Size;
	};

	struct Simple {
		XMFLOAT3 Pos;
	};

	struct PosNormalTexTan {
		XMFLOAT3 Pos;
		XMFLOAT3 Normal;
		XMFLOAT2 Tex;
		XMFLOAT3 TangentU;
	};

	struct Particle
	{
		XMFLOAT3 InitialPos;
		XMFLOAT3 initialVel;
		XMFLOAT2 Size;
		float	 Age;
		unsigned int Type;
	};

	struct Skinned
	{
		XMFLOAT3 Pos;
		XMFLOAT3 Normal;
		XMFLOAT2 Tex;
		XMFLOAT3 TangentU;
		XMFLOAT3 BoneWeights;
		byte     BoneIndex[4];
	};

	extern ComPtr<ID3D11InputLayout> IL_Simple;
	extern ComPtr<ID3D11InputLayout> IL_PointBillboard;
	extern ComPtr<ID3D11InputLayout> IL_Basic32;
	extern ComPtr<ID3D11InputLayout> IL_Basic32_Tanget;
	extern ComPtr<ID3D11InputLayout> IL_Particle;
	extern ComPtr<ID3D11InputLayout> IL_Skinned;

	extern UINT VertexStride_Simple;
	extern UINT VertexStride_Basic32;
	extern UINT VertexStride_Basic32_Tangent;
	extern UINT VertexStride_PointBillboard;
	extern UINT VertexStride_Particle;
	extern UINT VertexStride_Skinned;

	void InitInputLayouts();
	ComPtr<ID3D11InputLayout> GetInputLayout(VERTEX_TYPE _vertexType);
	void GetVertexStride(VERTEX_TYPE _vertexType, UINT &_stride);
	void ReleaseMemory();

}