#include "Pipeline_CS_GenerateSAT.h"

ShaderList::ComputeShaders::CS_GenerateSAT_Horz PipelineList::Pipeline_CS_GenerateSAT::mCS_Horz = ShaderList::ComputeShaders::CS_GenerateSAT_Horz();

ShaderList::ComputeShaders::CS_GenerateSAT_Vert PipelineList::Pipeline_CS_GenerateSAT::mCS_Vert = ShaderList::ComputeShaders::CS_GenerateSAT_Vert();

void PipelineList::Pipeline_CS_GenerateSAT::Apply_Preliminiary_Horz()
{
	mCS_Horz.SetShader();
}

void PipelineList::Pipeline_CS_GenerateSAT::Apply_Preliminiary_Vert()
{
	mCS_Vert.SetShader();
}

