#pragma once

#include "IPipeline.h"

#include "Graphics/ShaderList/VS_Render2D.h"
#include "Graphics/ShaderList/PS_DeferredShading.h"

enum SHADER_CLASSINSTANCES_POSITION
{
	SHADER_CLASSINSTANCES_POSITION_NOWVP,
	SHADER_CLASSINSTANCES_POSITION_WVP
};

enum SHADER_CLASSINSTANCES_GBUFFERMANAGER
{
	SHADER_CLASSINSTANCES_GBUFFERMANAGER_BASIC
};

enum SHADER_CLASSINSTANCES_SPOTLIGHTSARRAY
{
	SHADER_CLASSINSTANCES_SPOTLIGHTSARRAY_DEFAULT
};

enum SHADER_CLASSINSTANCES_SHADOWMANAGER
{
	SHADER_CLASSINSTANCES_SHADOWMANAGER_BASICSHADOWMAP,
	SHADER_CLASSINSTANCES_SHADOWMANAGER_PCF
};

namespace PipelineList {


	class Pipeline_DeferredShading : public IPipeline
	{
		static UINT mVertexStride, mVertexOffset;

		static ComPtr<ID3D11InputLayout> pInputLayout;

		static D3D11_PRIMITIVE_TOPOLOGY mPrimitiveTopology;


		RENDER_STATE_RS mRenderState_Rasterizer;
		RENDER_STATE_BS mRenderState_Blend;
		RENDER_STATE_DS mRenderState_Depth;


		static ShaderList::VertexShaders::VS_Render2D	mVertexShader;
		static ShaderList::PixelShaders::PS_DeferredShading		mPixelShader;


	public:


		static void Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset = 0);
		static void Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology);
		void ChangeRenderStates(RENDER_STATE_RS rs = RENDER_STATE_RS_NONE, RENDER_STATE_BS bs = RENDER_STATE_BS_NONE, RENDER_STATE_DS ds = RENDER_STATE_DS_NONE);

		void Init();

		void Apply_Preliminiary(
			/*ID3D11Buffer* vertexBuffer
			, ID3D11Buffer* indexBuffer,*/
			/*SHADER_CLASSINSTANCES_POSITION classInstance_position
			, SHADER_CLASSINSTANCES_GBUFFERMANAGER classInstance_gbufferManager
			, SHADER_CLASSINSTANCES_SPOTLIGHTSARRAY classInstance_spotLightArray
			, SHADER_CLASSINSTANCES_SHADOWMANAGER classInstance_shadowManager*/
			ID3D11DeviceContext* context = gContext.Get());
	};

}
