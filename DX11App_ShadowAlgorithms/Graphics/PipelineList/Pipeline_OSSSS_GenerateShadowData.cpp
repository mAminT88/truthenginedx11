#include "Pipeline_OSSSS_GenerateShadowData.h"

ShaderList::VertexShaders::VS_Render2D PipelineList::Pipeline_OSSSS_GenerateShadowData::mVS = ShaderList::VertexShaders::VS_Render2D();

ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0 PipelineList::Pipeline_OSSSS_GenerateShadowData::mPS_0 = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0();

ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1 PipelineList::Pipeline_OSSSS_GenerateShadowData::mPS_1 = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1();

UINT PipelineList::Pipeline_OSSSS_GenerateShadowData::mVertexStride = 0;

UINT PipelineList::Pipeline_OSSSS_GenerateShadowData::mVertexOffset = 0;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_OSSSS_GenerateShadowData::pInputLayout = nullptr;

RENDER_STATE_RS PipelineList::Pipeline_OSSSS_GenerateShadowData::mRenderState_Rasterizer = RENDER_STATE_RS_DEFAULT;

RENDER_STATE_BS PipelineList::Pipeline_OSSSS_GenerateShadowData::mRenderState_Blend = RENDER_STATE_BS_DEFAULT;

RENDER_STATE_DS PipelineList::Pipeline_OSSSS_GenerateShadowData::mRenderState_Depth = RENDER_STATE_DS_NODEPTH;

void PipelineList::Pipeline_OSSSS_GenerateShadowData::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset /*= 0*/)
{
	pInputLayout = Vertex::GetInputLayout(vertexType);
	Vertex::GetVertexStride(vertexType, mVertexStride);

	mVertexOffset = vertexOffset;
}


void PipelineList::Pipeline_OSSSS_GenerateShadowData::ChangeRenderStates(RENDER_STATE_RS rs /*= RENDER_STATE_RS_NONE*/, RENDER_STATE_BS bs /*= RENDER_STATE_BS_NONE*/, RENDER_STATE_DS ds /*= RENDER_STATE_DS_NONE*/)
{
	mRenderState_Rasterizer = rs;
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
}

void PipelineList::Pipeline_OSSSS_GenerateShadowData::Init()
{
	Set_VertexType(VERTEX_TYPE_BASIC32);
}

void PipelineList::Pipeline_OSSSS_GenerateShadowData::Apply_Preliminiary_Pass0(ID3D11DeviceContext* context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, gVertexBuffer_2D.GetAddressOf(), &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(gIndexBuffer_2D.Get(), DXGI_FORMAT_R32_UINT, 0);


	RenderStates::SetRasterizerState(mRenderState_Rasterizer, context);
	RenderStates::SetBlendState(mRenderState_Blend, context);
	RenderStates::SetDepthStencilState(mRenderState_Depth, context);

	mVS.SetShader(context);
	mPS_0.SetShader(context);
}

void PipelineList::Pipeline_OSSSS_GenerateShadowData::Apply_Preliminiary_Pass1(ID3D11DeviceContext* context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, gVertexBuffer_2D.GetAddressOf(), &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(gIndexBuffer_2D.Get(), DXGI_FORMAT_R32_UINT, 0);


	RenderStates::SetRasterizerState(mRenderState_Rasterizer, context);
	RenderStates::SetBlendState(mRenderState_Blend, context);
	RenderStates::SetDepthStencilState(mRenderState_Depth, context);

	mVS.SetShader(context);
	mPS_1.SetShader(context);
}



//
// Screen Space Separable Blocker Search
//


ShaderList::VertexShaders::VS_Render2D PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mVS = ShaderList::VertexShaders::VS_Render2D();

ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0 PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mPS_0 = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0();

ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1 PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mPS_1 = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1();

ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2 PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mPS_2 = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2();

UINT PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mVertexStride = 0;

UINT PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mVertexOffset = 0;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::pInputLayout = nullptr;

D3D11_PRIMITIVE_TOPOLOGY PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

RENDER_STATE_RS PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mRenderState_Rasterizer = RENDER_STATE_RS_DEFAULT;

RENDER_STATE_BS PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mRenderState_Blend = RENDER_STATE_BS_DEFAULT;

RENDER_STATE_DS PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::mRenderState_Depth = RENDER_STATE_DS_NODEPTH;

void PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset /*= 0*/)
{
	pInputLayout = Vertex::GetInputLayout(vertexType);
	Vertex::GetVertexStride(vertexType, mVertexStride);

	mVertexOffset = vertexOffset;
}

void PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
{
	mPrimitiveTopology = primitiveTopology;
}

void PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::ChangeRenderStates(RENDER_STATE_RS rs /*= RENDER_STATE_RS_NONE*/, RENDER_STATE_BS bs /*= RENDER_STATE_BS_NONE*/, RENDER_STATE_DS ds /*= RENDER_STATE_DS_NONE*/)
{
	mRenderState_Rasterizer = rs;
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
}

void PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Init()
{
	Set_VertexType(VERTEX_TYPE_BASIC32);
}

void PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Apply_Preliminiary_Pass0(ID3D11DeviceContext* context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer_2D.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer_2D.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(mPrimitiveTopology);
	context->IASetVertexBuffers(0, 1, gVertexBuffer_2D.GetAddressOf(), &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(gIndexBuffer_2D.Get(), DXGI_FORMAT_R32_UINT, 0);


	RenderStates::SetRasterizerState(mRenderState_Rasterizer, context);
	RenderStates::SetBlendState(mRenderState_Blend, context);
	RenderStates::SetDepthStencilState(mRenderState_Depth, context);

	mVS.SetShader(context);
	mPS_0.SetShader(context);
}

void PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Apply_Preliminiary_Pass1(ID3D11DeviceContext* context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer_2D.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer_2D.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(mPrimitiveTopology);
	context->IASetVertexBuffers(0, 1, gVertexBuffer_2D.GetAddressOf(), &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(gIndexBuffer_2D.Get(), DXGI_FORMAT_R32_UINT, 0);


	RenderStates::SetRasterizerState(mRenderState_Rasterizer, context);
	RenderStates::SetBlendState(mRenderState_Blend, context);
	RenderStates::SetDepthStencilState(mRenderState_Depth, context);

	mVS.SetShader(context);
	mPS_1.SetShader(context);
}

void PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Apply_Preliminiary_Pass2(ID3D11DeviceContext* context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer_2D.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer_2D.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(mPrimitiveTopology);
	context->IASetVertexBuffers(0, 1, gVertexBuffer_2D.GetAddressOf(), &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(gIndexBuffer_2D.Get(), DXGI_FORMAT_R32_UINT, 0);


	RenderStates::SetRasterizerState(mRenderState_Rasterizer, context);
	RenderStates::SetBlendState(mRenderState_Blend, context);
	RenderStates::SetDepthStencilState(mRenderState_Depth, context);

	mVS.SetShader(context);
	mPS_2.SetShader(context);
}
