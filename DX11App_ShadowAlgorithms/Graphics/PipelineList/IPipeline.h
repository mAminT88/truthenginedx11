#pragma once

#include "Graphics/RenderStates.h"
#include "Graphics/Vertex.h"
#include "Graphics/StaticBuffers.h"
#include "Graphics/ShaderList/IPixelShader.h"




namespace PipelineList
{	

	interface IPipeline
	{
	protected:


	public:


	};

	interface IPipeline_Surface : public IPipeline
	{
	protected:
		RENDER_STATE_RS mRenderState_Rasterizer;
		RENDER_STATE_BS mRenderState_Blend;
		RENDER_STATE_DS mRenderState_Depth;


		

	public:


		inline void ChangeRenderStates(RENDER_STATE_RS rs = RENDER_STATE_RS_NONE, RENDER_STATE_BS bs = RENDER_STATE_BS_NONE, RENDER_STATE_DS ds = RENDER_STATE_DS_NONE)
		{
			mRenderState_Rasterizer = rs;
			mRenderState_Depth = ds;
			mRenderState_Blend = bs;
		};

		virtual void Init() {};

		virtual void Apply_Preliminiary(
			SURFACE_SHADER_CLASSINSTANCES_NORMAL classInstace_Normal
			, SURFACE_SHADER_CLASSINSTANCES_MATERIAL classInstance_Material
			, VERTEX_TYPE vertexType
			, ID3D11DeviceContext* context = gContext.Get()) {};

		

	};

	interface IPipeline_GenerateShadowMap : IPipeline
	{

	protected:

	public:
		virtual void Apply_Preliminiary(VERTEX_TYPE vertexType
			, ID3D11DeviceContext* _context = gContext.Get()) {};

	};

}