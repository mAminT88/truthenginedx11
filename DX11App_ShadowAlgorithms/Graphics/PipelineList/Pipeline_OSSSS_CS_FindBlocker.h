#pragma once

#include "IPipeline.h"

#include "Graphics/ShaderList/CS_FindBlocker.h"

namespace PipelineList
{
	class Pipeline_OSSSS_CS_FindBlocker : public IPipeline
	{
	protected:
		static ShaderList::ComputeShaders::CS_FindBlocker mCS;

	public:
		void Apply_Preliminiary();
	};
}
