#include "Pipeline_GenerateShadowMap.h"

UINT PipelineList::Pipeline_GenerateShadowMap::mVertexStride = 0;
UINT PipelineList::Pipeline_GenerateShadowMap::mVertexOffset = 0;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_GenerateShadowMap::pInputLayout = nullptr;

VERTEX_TYPE PipelineList::Pipeline_GenerateShadowMap::mVertexType = VERTEX_TYPE_NONE;

RENDER_STATE_RS PipelineList::Pipeline_GenerateShadowMap::mRenderState_Rasterizer = RENDER_STATE_RS_DEFAULT;

RENDER_STATE_BS PipelineList::Pipeline_GenerateShadowMap::mRenderState_Blend = RENDER_STATE_BS_DEFAULT;

RENDER_STATE_DS PipelineList::Pipeline_GenerateShadowMap::mRenderState_Depth = RENDER_STATE_DS_DEFAULT;

ShaderList::VertexShaders::VS_BuildShadowMap PipelineList::Pipeline_GenerateShadowMap::mVertexShader = ShaderList::VertexShaders::VS_BuildShadowMap();


void PipelineList::Pipeline_GenerateShadowMap::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset)
{
	if (mVertexType == vertexType)
		return;

	mVertexType = vertexType;

	pInputLayout = Vertex::GetInputLayout(vertexType);
	Vertex::GetVertexStride(vertexType, mVertexStride);

	mVertexOffset = vertexOffset;
}

void PipelineList::Pipeline_GenerateShadowMap::Init()
{

	Set_VertexType(VERTEX_TYPE_BASIC32, 0);
	ChangeRenderStates(RENDER_STATE_RS_SHADOWMAP, RENDER_STATE_BS_DEFAULT, RENDER_STATE_DS_DEFAULT);
}

void PipelineList::Pipeline_GenerateShadowMap::Apply_Preliminiary(VERTEX_TYPE vertexType
	, ID3D11DeviceContext*  context)
{
	//Set_VertexType(vertexType);

	//context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	/*context->IASetVertexBuffers(0, 1, &vertexBuffer, &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);*/

	RenderStates::SetBlendState(mRenderState_Blend);
	RenderStates::SetDepthStencilState(mRenderState_Depth);
	RenderStates::SetRasterizerState(mRenderState_Rasterizer);

	switch (vertexType)
	{
	case VERTEX_TYPE_NONE:
		break;
	case VERTEX_TYPE_SIMPLE:
		break;
	case VERTEX_TYPE_POINTBILLBOARD:
		break;
	case VERTEX_TYPE_BASIC32:
		mVertexShader.SetShader_Basic32(context);
		break;
	case VERTEX_TYPE_BASIC32_TANGET:
		break;
	case VERTEX_TYPE_PARTICLE:
		break;
	case VERTEX_TYPE_SKINNED:
		mVertexShader.SetShader_Skinned(context);
		break;
	default:
		break;
	}

	context->PSSetShader(nullptr, nullptr, 0);
}

void PipelineList::Pipeline_GenerateShadowMap::ChangeRenderStates(RENDER_STATE_RS rs, RENDER_STATE_BS bs, RENDER_STATE_DS ds)
{
	mRenderState_Rasterizer = rs;
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
}
