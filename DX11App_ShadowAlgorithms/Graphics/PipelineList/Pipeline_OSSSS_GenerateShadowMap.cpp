#include "Pipeline_OSSSS_GenerateShadowMap.h"

ShaderList::VertexShaders::VS_OSSSS_GenerateShadowMap PipelineList::Pipeline_OSSSS_GenerateShadowMap::mVS = ShaderList::VertexShaders::VS_OSSSS_GenerateShadowMap();

ShaderList::PixelShaders::PS_OSSSS_GenerateShadowMap PipelineList::Pipeline_OSSSS_GenerateShadowMap::mPS = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowMap();

UINT PipelineList::Pipeline_OSSSS_GenerateShadowMap::mVertexStride = 0;

UINT PipelineList::Pipeline_OSSSS_GenerateShadowMap::mVertexOffset = 0;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_OSSSS_GenerateShadowMap::pInputLayout = nullptr;

D3D11_PRIMITIVE_TOPOLOGY PipelineList::Pipeline_OSSSS_GenerateShadowMap::mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

RENDER_STATE_RS PipelineList::Pipeline_OSSSS_GenerateShadowMap::mRenderState_Rasterizer = RENDER_STATE_RS_SHADOWMAP;

RENDER_STATE_BS PipelineList::Pipeline_OSSSS_GenerateShadowMap::mRenderState_Blend = RENDER_STATE_BS_DEFAULT;

RENDER_STATE_DS PipelineList::Pipeline_OSSSS_GenerateShadowMap::mRenderState_Depth = RENDER_STATE_DS_DEFAULT;

void PipelineList::Pipeline_OSSSS_GenerateShadowMap::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset /*= 0*/)
{
	pInputLayout = Vertex::GetInputLayout(vertexType);
	Vertex::GetVertexStride(vertexType, mVertexStride);

	mVertexOffset = vertexOffset;
}

void PipelineList::Pipeline_OSSSS_GenerateShadowMap::Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
{
	mPrimitiveTopology = primitiveTopology;
}

void PipelineList::Pipeline_OSSSS_GenerateShadowMap::ChangeRenderStates(RENDER_STATE_RS rs /*= RENDER_STATE_RS_NONE*/, RENDER_STATE_BS bs /*= RENDER_STATE_BS_NONE*/, RENDER_STATE_DS ds /*= RENDER_STATE_DS_NONE*/)
{
	mRenderState_Rasterizer = rs;
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
}

void PipelineList::Pipeline_OSSSS_GenerateShadowMap::Init()
{
	Set_VertexType(VERTEX_TYPE_BASIC32, 0);
}

void PipelineList::Pipeline_OSSSS_GenerateShadowMap::Apply_Preliminiary(ID3D11DeviceContext *context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(mPrimitiveTopology);
	context->IASetVertexBuffers(0, 1, &vertexBuffer, &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	RenderStates::SetBlendState(mRenderState_Blend);
	RenderStates::SetDepthStencilState(mRenderState_Depth);
	RenderStates::SetRasterizerState(mRenderState_Rasterizer);

	mVS.SetShader(context);
	mPS.SetShader(context);

}
