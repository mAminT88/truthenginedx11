#pragma once

#include "IPipeline.h"

#include "Graphics/ShaderList/VS_Render2D.h"
#include "Graphics/ShaderList/PS_SeperableBlurFilter.h"

namespace PipelineList
{
	class Pipeline_SeperableGaussianFilter : public IPipeline
	{
		static UINT mVertexStride;
		static UINT mVertexOffset;

		static ComPtr<ID3D11InputLayout> pInputLayout;

		static RENDER_STATE_RS mRenderState_Rasterizer;
		static RENDER_STATE_BS mRenderState_Blend;
		static RENDER_STATE_DS mRenderState_Depth;


		static ShaderList::VertexShaders::VS_Render2D mVertexShader;
		static ShaderList::PixelShaders::PS_SeperableBlurFilter mPixelShader;

	public:

		static void Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset = 0);
		static void ChangeRenderStates(RENDER_STATE_RS rs = RENDER_STATE_RS_NONE, RENDER_STATE_BS bs = RENDER_STATE_BS_NONE, RENDER_STATE_DS ds = RENDER_STATE_DS_NONE);

		static void Init();

		static void Apply_Preliminiary(ID3D11DeviceContext* _context = gContext.Get()
			, ID3D11Buffer* vertexBuffer = StaticBuffers::gVertexBuffer_2D.Get()
			, ID3D11Buffer* indexBuffer = StaticBuffers::gIndexBuffer_2D.Get());
	};
}
