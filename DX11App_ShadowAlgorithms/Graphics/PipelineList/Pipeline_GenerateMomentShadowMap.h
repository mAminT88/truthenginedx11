#pragma once

#include "IPipeline.h"

#include "Graphics/ShaderList/VS_BuildShadowMap.h"
#include "Graphics/ShaderList/PS_GenerateMomentShadowMap.h"

namespace PipelineList
{
	class Pipeline_GenerateMomentShadowMap : public IPipeline
	{
	private:
		static ShaderList::VertexShaders::VS_BuildShadowMap mVS;
		static ShaderList::PixelShaders::PS_GenerateMomentShadowMap mPS;

		static UINT mVertexStride, mVertexOffset;

		static ComPtr<ID3D11InputLayout> pInputLayout;

		//static D3D11_PRIMITIVE_TOPOLOGY mPrimitiveTopology;

		static VERTEX_TYPE mVertexType;

		static RENDER_STATE_RS mRenderState_Rasterizer;
		static RENDER_STATE_BS mRenderState_Blend;
		static RENDER_STATE_DS mRenderState_Depth;

	public:
		//static void Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset = 0);
		//static void Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology);
		static void ChangeRenderStates(RENDER_STATE_RS rs = RENDER_STATE_RS_NONE, RENDER_STATE_BS bs = RENDER_STATE_BS_NONE, RENDER_STATE_DS ds = RENDER_STATE_DS_NONE);

		void Init();

		void Apply_Preliminiary(VERTEX_TYPE vertexType
			, ID3D11DeviceContext* context = gContext.Get());

	};

}