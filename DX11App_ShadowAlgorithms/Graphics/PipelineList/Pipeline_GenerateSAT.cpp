#include "Pipeline_GenerateSAT.h"

using namespace StaticBuffers;


UINT PipelineList::Pipeline_GenerateSAT::mVertexStride = 0;

UINT PipelineList::Pipeline_GenerateSAT::mVertexOffset = 0;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_GenerateSAT::pInputLayout = nullptr;

D3D11_PRIMITIVE_TOPOLOGY PipelineList::Pipeline_GenerateSAT::mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

RENDER_STATE_RS PipelineList::Pipeline_GenerateSAT::mRenderState_Rasterizer = RENDER_STATE_RS_DEFAULT;

RENDER_STATE_BS PipelineList::Pipeline_GenerateSAT::mRenderState_Blend = RENDER_STATE_BS_DEFAULT;

RENDER_STATE_DS PipelineList::Pipeline_GenerateSAT::mRenderState_Depth = RENDER_STATE_DS_NODEPTH;

ShaderList::VertexShaders::VS_Render2D PipelineList::Pipeline_GenerateSAT::mVertexShader = ShaderList::VertexShaders::VS_Render2D();

ShaderList::PixelShaders::PS_GenerateSAT PipelineList::Pipeline_GenerateSAT::mPixelShader = ShaderList::PixelShaders::PS_GenerateSAT();

void PipelineList::Pipeline_GenerateSAT::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset /*= 0*/)
{
	pInputLayout = Vertex::GetInputLayout(vertexType);

	Vertex::GetVertexStride(vertexType, mVertexStride);

	mVertexOffset = vertexOffset;
}

void PipelineList::Pipeline_GenerateSAT::Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
{
	mPrimitiveTopology = primitiveTopology;
}

void PipelineList::Pipeline_GenerateSAT::ChangeRenderStates(RENDER_STATE_RS rs /*= RENDER_STATE_RS_NONE*/, RENDER_STATE_BS bs /*= RENDER_STATE_BS_NONE*/, RENDER_STATE_DS ds /*= RENDER_STATE_DS_NONE*/)
{
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
	mRenderState_Rasterizer = rs;
}

void PipelineList::Pipeline_GenerateSAT::Init()
{
	Set_VertexType(VERTEX_TYPE_BASIC32, 0);
}

void PipelineList::Pipeline_GenerateSAT::Apply_Preliminiary(ID3D11DeviceContext* context /*= gContext.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(mPrimitiveTopology);
	context->IASetVertexBuffers(0, 1, gVertexBuffer_2D.GetAddressOf(), &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(gIndexBuffer_2D.Get(), DXGI_FORMAT_R32_UINT, 0);
	

	RenderStates::SetRasterizerState(mRenderState_Rasterizer, context);
	RenderStates::SetBlendState(mRenderState_Blend, context);
	RenderStates::SetDepthStencilState(mRenderState_Depth, context);

	mVertexShader.SetShader(context);
	mPixelShader.SetShader(context);
}

void PipelineList::Pipeline_GenerateSAT::Apply_Preliminiary_UINT(ID3D11DeviceContext* context /*= gContext.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(mPrimitiveTopology);
	context->IASetVertexBuffers(0, 1, gVertexBuffer_2D.GetAddressOf(), &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(gIndexBuffer_2D.Get(), DXGI_FORMAT_R32_UINT, 0);


	RenderStates::SetRasterizerState(mRenderState_Rasterizer, context);
	RenderStates::SetBlendState(mRenderState_Blend, context);
	RenderStates::SetDepthStencilState(mRenderState_Depth, context);

	mVertexShader.SetShader(context);
	mPixelShader_UINT.SetShader(context);
}

