#include "Pipeline_SSSM_GenerateShadowData.h"

UINT PipelineList::Pipeline_SSSM_GenerateShadowData::mVertexStride = 0;

UINT PipelineList::Pipeline_SSSM_GenerateShadowData::mVertexOffset = 0;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_SSSM_GenerateShadowData::pInputLayout=  nullptr;


RENDER_STATE_RS PipelineList::Pipeline_SSSM_GenerateShadowData::mRenderState_Rasterizer = RENDER_STATE_RS_DEFAULT;

RENDER_STATE_BS PipelineList::Pipeline_SSSM_GenerateShadowData::mRenderState_Blend = RENDER_STATE_BS_DEFAULT;

RENDER_STATE_DS PipelineList::Pipeline_SSSM_GenerateShadowData::mRenderState_Depth = RENDER_STATE_DS_NODEPTH;

ShaderList::VertexShaders::VS_Render2D PipelineList::Pipeline_SSSM_GenerateShadowData::mVertexShader = ShaderList::VertexShaders::VS_Render2D();

ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0 PipelineList::Pipeline_SSSM_GenerateShadowData::mPixelShader_0 = ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0();
																																										  
ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1 PipelineList::Pipeline_SSSM_GenerateShadowData::mPixelShader_1 = ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1();
																																											  
ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2 PipelineList::Pipeline_SSSM_GenerateShadowData::mPixelShader_2 = ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2();
																																											  
void PipelineList::Pipeline_SSSM_GenerateShadowData::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset /*= 0*/)
{
	pInputLayout = Vertex::GetInputLayout(vertexType);
	Vertex::GetVertexStride(vertexType, mVertexStride);

	mVertexOffset = vertexOffset;
}

void PipelineList::Pipeline_SSSM_GenerateShadowData::ChangeRenderStates(RENDER_STATE_RS rs /*= RENDER_STATE_RS_NONE*/, RENDER_STATE_BS bs /*= RENDER_STATE_BS_NONE*/, RENDER_STATE_DS ds /*= RENDER_STATE_DS_NONE*/)
{
	mRenderState_Rasterizer = rs;
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
}

void PipelineList::Pipeline_SSSM_GenerateShadowData::Init()
{
	Set_VertexType(VERTEX_TYPE_BASIC32, 0);
}

void PipelineList::Pipeline_SSSM_GenerateShadowData::Apply_Preliminiary_0(ID3D11DeviceContext* context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer_2D.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer_2D.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, &vertexBuffer, &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	RenderStates::SetBlendState(mRenderState_Blend);
	RenderStates::SetDepthStencilState(mRenderState_Depth);
	RenderStates::SetRasterizerState(mRenderState_Rasterizer);

	mVertexShader.SetShader(context);
	
	mPixelShader_0.SetShader(context);
}

void PipelineList::Pipeline_SSSM_GenerateShadowData::Apply_Preliminiary_1(ID3D11DeviceContext* context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer_2D.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer_2D.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, &vertexBuffer, &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	RenderStates::SetBlendState(mRenderState_Blend);
	RenderStates::SetDepthStencilState(mRenderState_Depth);
	RenderStates::SetRasterizerState(mRenderState_Rasterizer);

	mVertexShader.SetShader(context);

	mPixelShader_1.SetShader(context);
}

void PipelineList::Pipeline_SSSM_GenerateShadowData::Apply_Preliminiary_2(ID3D11DeviceContext* context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer_2D.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer_2D.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, &vertexBuffer, &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	RenderStates::SetBlendState(mRenderState_Blend);
	RenderStates::SetDepthStencilState(mRenderState_Depth);
	RenderStates::SetRasterizerState(mRenderState_Rasterizer);

	mVertexShader.SetShader(context);

	mPixelShader_2.SetShader(context);
}

