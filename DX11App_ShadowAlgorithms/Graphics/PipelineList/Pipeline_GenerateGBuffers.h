#pragma once

#include "IPipeline.h"

#include "Graphics/ShaderList/VS_ProcessMesh.h"
#include "Graphics/ShaderList/PS_GenerateGBuffers.h"

namespace PipelineList
{
	class Pipeline_GenerateGBuffers : public IPipeline_Surface
	{


		static UINT mVertexStride, mVertexOffset;


		static VERTEX_TYPE mVertexType;

		static ComPtr<ID3D11InputLayout> pInputLayout;

		static ShaderList::VertexShaders::VS_ProcessMesh	mVertexShader;
		static ShaderList::PixelShaders::PS_GenerateGBuffers		mPixelShader;


	public:
		

		//static void Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset = 0);


		void Init() override;


		void Apply_Preliminiary(SURFACE_SHADER_CLASSINSTANCES_NORMAL classInstace_Normal
			, SURFACE_SHADER_CLASSINSTANCES_MATERIAL classInstance_Material
			, VERTEX_TYPE vertexType
			, ID3D11DeviceContext* context = gContext.Get()) override;


	};

}
