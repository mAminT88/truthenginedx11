#include "Pipeline_GenerateGBuffers.h"

UINT PipelineList::Pipeline_GenerateGBuffers::mVertexStride = 0;
UINT PipelineList::Pipeline_GenerateGBuffers::mVertexOffset = 0;

VERTEX_TYPE PipelineList::Pipeline_GenerateGBuffers::mVertexType = VERTEX_TYPE_NONE;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_GenerateGBuffers::pInputLayout = nullptr;

ShaderList::VertexShaders::VS_ProcessMesh	PipelineList::Pipeline_GenerateGBuffers::mVertexShader = ShaderList::VertexShaders::VS_ProcessMesh();
ShaderList::PixelShaders::PS_GenerateGBuffers		PipelineList::Pipeline_GenerateGBuffers::mPixelShader = ShaderList::PixelShaders::PS_GenerateGBuffers();

//void PipelineList::Pipeline_GenerateGBuffers::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset)
//{
//	if (vertexType == mVertexType)
//		return;
//
//	mVertexType = vertexType;
//	pInputLayout = Vertex::GetInputLayout(vertexType);
//	Vertex::GetVertexStride(vertexType, mVertexStride);
//
//	mVertexOffset = 0;
//}

void PipelineList::Pipeline_GenerateGBuffers::Init()
{

	//Set_VertexType(VERTEX_TYPE_BASIC32);
	//Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ChangeRenderStates(RENDER_STATE_RS_DEFAULT, RENDER_STATE_BS_DEFAULT, RENDER_STATE_DS_DEFAULT);
}

void PipelineList::Pipeline_GenerateGBuffers::Apply_Preliminiary(
	SURFACE_SHADER_CLASSINSTANCES_NORMAL classInstace_Normal
	, SURFACE_SHADER_CLASSINSTANCES_MATERIAL classInstance_Material
	, VERTEX_TYPE vertexType
	, ID3D11DeviceContext* context)
{
	/*Set_VertexType(vertexType);*/

	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	/*context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);
	context->IASetVertexBuffers(0, 1, &vertexBuffer, &mVertexStride, &mVertexOffset);*/

	RenderStates::SetBlendState(mRenderState_Blend);
	RenderStates::SetDepthStencilState(mRenderState_Depth);
	RenderStates::SetRasterizerState(mRenderState_Rasterizer);

	switch (classInstace_Normal)
	{
	case SURFACE_SHADER_CLASSINSTANCES_NORMAL_VECTOR:
		mPixelShader.SetClassInstace_NormalManager_NormalVector();
		break;
	case SURFACE_SHADER_CLASSINSTANCES_NORMAL_NORMALMAP:
		mPixelShader.SetClassInstace_NormalManager_NormalMap();
		break;
	}

	switch (classInstance_Material)
	{
	case SURFACE_SHADER_CLASSINSTANCES_MATERIAL_BASIC:
		mPixelShader.SetClassInstace_MaterialBase();
		break;
	case SURFACE_SHADER_CLASSINSTANCES_MATERIAL_TEXTURED:
		mPixelShader.SetClassInstace_MaterialTextured();
		break;
	}

	switch (vertexType)
	{
	case VERTEX_TYPE_NONE:
		break;
	case VERTEX_TYPE_SIMPLE:
		break;
	case VERTEX_TYPE_POINTBILLBOARD:
		break;
	case VERTEX_TYPE_BASIC32:
		mVertexShader.SetShader_Basic32(context);
		break;
	case VERTEX_TYPE_BASIC32_TANGET:
		break;
	case VERTEX_TYPE_PARTICLE:
		break;
	case VERTEX_TYPE_SKINNED:
		mVertexShader.SetShader_Skinned(context);
		break;
	default:
		break;
	}

	mPixelShader.SetShader(context);
}
