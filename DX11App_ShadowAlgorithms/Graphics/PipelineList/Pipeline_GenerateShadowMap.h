#pragma once

#include "IPipeline.h"

#include "Graphics/ShaderList/VS_BuildShadowMap.h"

namespace PipelineList
{
	class Pipeline_GenerateShadowMap : public IPipeline_GenerateShadowMap
	{
		static UINT mVertexStride, mVertexOffset;

		static ComPtr<ID3D11InputLayout> pInputLayout;

		static VERTEX_TYPE mVertexType;

		static RENDER_STATE_RS mRenderState_Rasterizer;
		static RENDER_STATE_BS mRenderState_Blend;
		static RENDER_STATE_DS mRenderState_Depth;


		static ShaderList::VertexShaders::VS_BuildShadowMap mVertexShader;


	public:

		static void Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset = 0);
		static void ChangeRenderStates(RENDER_STATE_RS rs = RENDER_STATE_RS_NONE, RENDER_STATE_BS bs = RENDER_STATE_BS_NONE, RENDER_STATE_DS ds = RENDER_STATE_DS_NONE);

		static void Init();

		void Apply_Preliminiary(VERTEX_TYPE vertexType
			, ID3D11DeviceContext* _context = gContext.Get()) override;

	};

}
