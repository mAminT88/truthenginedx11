#pragma once

#include "IPipeline.h"

#include "Graphics/ShaderList/PS_GenerateSAT.h"
#include "Graphics/ShaderList/PS_GenerateSAT_UINT.h"
#include "Graphics/ShaderList/VS_Render2D.h"

namespace PipelineList
{
	class Pipeline_GenerateSAT : public IPipeline
	{
		static UINT mVertexStride;
		static UINT mVertexOffset;

		static ComPtr<ID3D11InputLayout> pInputLayout;

		static D3D11_PRIMITIVE_TOPOLOGY mPrimitiveTopology;


		static RENDER_STATE_RS mRenderState_Rasterizer;
		static RENDER_STATE_BS mRenderState_Blend;
		static RENDER_STATE_DS mRenderState_Depth;



		static ShaderList::VertexShaders::VS_Render2D       mVertexShader;
		static ShaderList::PixelShaders::PS_GenerateSAT		mPixelShader;
		static ShaderList::PixelShaders::PS_GenerateSAT_UINT mPixelShader_UINT;


	public:


		static void Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset = 0);
		static void Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology);
		static void ChangeRenderStates(RENDER_STATE_RS rs = RENDER_STATE_RS_NONE, RENDER_STATE_BS bs = RENDER_STATE_BS_NONE, RENDER_STATE_DS ds = RENDER_STATE_DS_NONE);

		static void Init();


		void Apply_Preliminiary(
			ID3D11DeviceContext* context = gContext.Get());
		void Apply_Preliminiary_UINT(
			ID3D11DeviceContext* context = gContext.Get());
	};
}