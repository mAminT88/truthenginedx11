#include "Pipeline_GenerateMomentShadowMap.h"

ShaderList::VertexShaders::VS_BuildShadowMap PipelineList::Pipeline_GenerateMomentShadowMap::mVS = ShaderList::VertexShaders::VS_BuildShadowMap();

ShaderList::PixelShaders::PS_GenerateMomentShadowMap PipelineList::Pipeline_GenerateMomentShadowMap::mPS = ShaderList::PixelShaders::PS_GenerateMomentShadowMap();

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_GenerateMomentShadowMap::pInputLayout = nullptr;

VERTEX_TYPE PipelineList::Pipeline_GenerateMomentShadowMap::mVertexType = VERTEX_TYPE_NONE;

RENDER_STATE_RS PipelineList::Pipeline_GenerateMomentShadowMap::mRenderState_Rasterizer = RENDER_STATE_RS_DEFAULT;

RENDER_STATE_BS PipelineList::Pipeline_GenerateMomentShadowMap::mRenderState_Blend = RENDER_STATE_BS_DEFAULT;

RENDER_STATE_DS PipelineList::Pipeline_GenerateMomentShadowMap::mRenderState_Depth = RENDER_STATE_DS_DEFAULT;

UINT PipelineList::Pipeline_GenerateMomentShadowMap::mVertexStride = 0;
UINT PipelineList::Pipeline_GenerateMomentShadowMap::mVertexOffset = 0;

//void PipelineList::Pipeline_GenerateMomentShadowMap::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset /*= 0*/)
//{
//	if (mVertexType == vertexType)
//		return;
//
//	mVertexType = vertexType;
//
//	pInputLayout = Vertex::GetInputLayout(vertexType);
//	Vertex::GetVertexStride(vertexType, mVertexStride);
//
//	mVertexOffset = vertexOffset;
//}

//void PipelineList::Pipeline_GenerateMomentShadowMap::Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
//{
//	mPrimitiveTopology = primitiveTopology;
//}

void PipelineList::Pipeline_GenerateMomentShadowMap::ChangeRenderStates(RENDER_STATE_RS rs /*= RENDER_STATE_RS_NONE*/, RENDER_STATE_BS bs /*= RENDER_STATE_BS_NONE*/, RENDER_STATE_DS ds /*= RENDER_STATE_DS_NONE*/)
{
	mRenderState_Rasterizer = rs;
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
}

void PipelineList::Pipeline_GenerateMomentShadowMap::Init()
{
	//Set_VertexType(VERTEX_TYPE_BASIC32, 0);
}

void PipelineList::Pipeline_GenerateMomentShadowMap::Apply_Preliminiary(VERTEX_TYPE vertexType
	, ID3D11DeviceContext* context /*= gContext.Get() */)
{

	//Set_VertexType(vertexType);

	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	/*context->IASetVertexBuffers(0, 1, &vertexBuffer, &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);*/

	RenderStates::SetBlendState(mRenderState_Blend);
	RenderStates::SetDepthStencilState(mRenderState_Depth);
	RenderStates::SetRasterizerState(mRenderState_Rasterizer);

	switch (vertexType)
	{
	case VERTEX_TYPE_NONE:
		break;
	case VERTEX_TYPE_SIMPLE:
		break;
	case VERTEX_TYPE_POINTBILLBOARD:
		break;
	case VERTEX_TYPE_BASIC32:
		mVS.SetShader_Basic32(context);
		break;
	case VERTEX_TYPE_BASIC32_TANGET:
		break;
	case VERTEX_TYPE_PARTICLE:
		break;
	case VERTEX_TYPE_SKINNED:
		mVS.SetShader_Skinned(context);
		break;
	default:
		break;
	}

	mPS.SetShader(context);
}
