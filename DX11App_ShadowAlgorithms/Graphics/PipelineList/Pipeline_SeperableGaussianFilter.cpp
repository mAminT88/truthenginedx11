#include "Pipeline_SeperableGaussianFilter.h"

UINT PipelineList::Pipeline_SeperableGaussianFilter::mVertexStride = 0;

UINT PipelineList::Pipeline_SeperableGaussianFilter::mVertexOffset = 0;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_SeperableGaussianFilter::pInputLayout = nullptr;

//D3D11_PRIMITIVE_TOPOLOGY PipelineList::Pipeline_SeperableGaussianFilter::mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

RENDER_STATE_RS PipelineList::Pipeline_SeperableGaussianFilter::mRenderState_Rasterizer = RENDER_STATE_RS_DEFAULT;

RENDER_STATE_BS PipelineList::Pipeline_SeperableGaussianFilter::mRenderState_Blend = RENDER_STATE_BS_DEFAULT;

RENDER_STATE_DS PipelineList::Pipeline_SeperableGaussianFilter::mRenderState_Depth = RENDER_STATE_DS_NODEPTH;

ShaderList::VertexShaders::VS_Render2D PipelineList::Pipeline_SeperableGaussianFilter::mVertexShader = ShaderList::VertexShaders::VS_Render2D();

ShaderList::PixelShaders::PS_SeperableBlurFilter PipelineList::Pipeline_SeperableGaussianFilter::mPixelShader = ShaderList::PixelShaders::PS_SeperableBlurFilter();

void PipelineList::Pipeline_SeperableGaussianFilter::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset /*= 0*/)
{
	pInputLayout = Vertex::GetInputLayout(vertexType);

	Vertex::GetVertexStride(vertexType, mVertexStride);

	mVertexOffset = vertexOffset;
}

//void PipelineList::Pipeline_SeperableGaussianFilter::Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
//{
//	mPrimitiveTopology = primitiveTopology;
//}

void PipelineList::Pipeline_SeperableGaussianFilter::ChangeRenderStates(RENDER_STATE_RS rs /*= RENDER_STATE_RS_NONE*/, RENDER_STATE_BS bs /*= RENDER_STATE_BS_NONE*/, RENDER_STATE_DS ds /*= RENDER_STATE_DS_NONE*/)
{
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
	mRenderState_Rasterizer = rs;
}

void PipelineList::Pipeline_SeperableGaussianFilter::Init()
{
	Set_VertexType(VERTEX_TYPE_BASIC32, 0);
}

void PipelineList::Pipeline_SeperableGaussianFilter::Apply_Preliminiary(ID3D11DeviceContext* context /*= gContext.Get() */, ID3D11Buffer* vertexBuffer /*= gVertexBuffer_2D.Get() */, ID3D11Buffer* indexBuffer /*= gIndexBuffer_2D.Get()*/)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, &vertexBuffer, &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	RenderStates::SetBlendState(mRenderState_Blend);
	RenderStates::SetDepthStencilState(mRenderState_Depth);
	RenderStates::SetRasterizerState(mRenderState_Rasterizer);

	mVertexShader.SetClassInstace_Position_noWVP();

	mVertexShader.SetShader(context);
	mPixelShader.SetShader_GaussFilter(context);

}
