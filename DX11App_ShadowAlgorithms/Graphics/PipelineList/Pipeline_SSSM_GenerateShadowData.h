#pragma once

#include "IPipeline.h"

#include "Graphics/ShaderList/VS_Render2D.h"
#include "Graphics/ShaderList/PS_SSSM_GenerateShadowData.h"

using namespace StaticBuffers;

namespace PipelineList
{

	class Pipeline_SSSM_GenerateShadowData : public IPipeline
	{
		static UINT mVertexStride;
		static UINT mVertexOffset;

		static ComPtr<ID3D11InputLayout> pInputLayout;

		static RENDER_STATE_RS mRenderState_Rasterizer;
		static RENDER_STATE_BS mRenderState_Blend;
		static RENDER_STATE_DS mRenderState_Depth;


		static ShaderList::VertexShaders::VS_Render2D mVertexShader;
		static ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0 mPixelShader_0;
		static ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1 mPixelShader_1;
		static ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2 mPixelShader_2;

	public:

		static void Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset = 0);
		static void ChangeRenderStates(RENDER_STATE_RS rs = RENDER_STATE_RS_NONE, RENDER_STATE_BS bs = RENDER_STATE_BS_NONE, RENDER_STATE_DS ds = RENDER_STATE_DS_NONE);

		static void Init();

		static void Apply_Preliminiary_0(ID3D11DeviceContext* context = gContext.Get()
			, ID3D11Buffer* vertexBuffer = gVertexBuffer_2D.Get()
			, ID3D11Buffer* indexBuffer = gIndexBuffer_2D.Get());

		static void Apply_Preliminiary_1(ID3D11DeviceContext* context = gContext.Get()
			, ID3D11Buffer* vertexBuffer = gVertexBuffer_2D.Get()
			, ID3D11Buffer* indexBuffer = gIndexBuffer_2D.Get());

		static void Apply_Preliminiary_2(ID3D11DeviceContext* context = gContext.Get()
			, ID3D11Buffer* vertexBuffer = gVertexBuffer_2D.Get()
			, ID3D11Buffer* indexBuffer = gIndexBuffer_2D.Get());
	};

}
