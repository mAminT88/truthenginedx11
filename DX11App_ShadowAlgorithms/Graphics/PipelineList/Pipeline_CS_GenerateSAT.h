#pragma once

#include "IPipeline.h"

#include "Graphics/ShaderList/CS_GenerateSAT.h"

namespace PipelineList
{

	class Pipeline_CS_GenerateSAT : public IPipeline
	{
	protected:
		static ShaderList::ComputeShaders::CS_GenerateSAT_Vert mCS_Vert;
		static ShaderList::ComputeShaders::CS_GenerateSAT_Horz mCS_Horz;

	public:
		void Apply_Preliminiary_Horz();
		void Apply_Preliminiary_Vert();
	};

}
