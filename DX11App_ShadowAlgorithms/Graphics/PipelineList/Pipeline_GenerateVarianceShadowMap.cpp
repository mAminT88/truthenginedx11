#include "Pipeline_GenerateVarianceShadowMap.h"

UINT PipelineList::Pipeline_GenerateVarianceShadowMap::mVertexStride = 0;

UINT PipelineList::Pipeline_GenerateVarianceShadowMap::mVertexOffset = 0;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_GenerateVarianceShadowMap::pInputLayout = nullptr;

VERTEX_TYPE PipelineList::Pipeline_GenerateVarianceShadowMap::mVertexType = VERTEX_TYPE_NONE;

RENDER_STATE_RS PipelineList::Pipeline_GenerateVarianceShadowMap::mRenderState_Rasterizer = RENDER_STATE_RS_SHADOWMAP;

RENDER_STATE_BS PipelineList::Pipeline_GenerateVarianceShadowMap::mRenderState_Blend = RENDER_STATE_BS_DEFAULT;

RENDER_STATE_DS PipelineList::Pipeline_GenerateVarianceShadowMap::mRenderState_Depth = RENDER_STATE_DS_DEFAULT;

ShaderList::VertexShaders::VS_BuildShadowMap PipelineList::Pipeline_GenerateVarianceShadowMap::mVertexShader = ShaderList::VertexShaders::VS_BuildShadowMap();

ShaderList::PixelShaders::PS_GenerateVarianceShadowMap PipelineList::Pipeline_GenerateVarianceShadowMap::mPixelShader = ShaderList::PixelShaders::PS_GenerateVarianceShadowMap();

void PipelineList::Pipeline_GenerateVarianceShadowMap::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset /*= 0*/)
{
	if (mVertexType == vertexType)
		return;

	mVertexType = vertexType;

	pInputLayout = Vertex::GetInputLayout(vertexType);
	Vertex::GetVertexStride(vertexType, mVertexStride);

	mVertexOffset = vertexOffset;
}
void PipelineList::Pipeline_GenerateVarianceShadowMap::ChangeRenderStates(RENDER_STATE_RS rs /*= RENDER_STATE_RS_NONE*/, RENDER_STATE_BS bs /*= RENDER_STATE_BS_NONE*/, RENDER_STATE_DS ds /*= RENDER_STATE_DS_NONE*/)
{
	mRenderState_Rasterizer = rs;
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
}

void PipelineList::Pipeline_GenerateVarianceShadowMap::Init()
{
	Set_VertexType(VERTEX_TYPE_BASIC32, 0);
	ChangeRenderStates(RENDER_STATE_RS_DEFAULT, RENDER_STATE_BS_DEFAULT, RENDER_STATE_DS_DEFAULT);
}

void PipelineList::Pipeline_GenerateVarianceShadowMap::Apply_Preliminiary(VERTEX_TYPE vertexType
	, ID3D11DeviceContext* context /*= gContext.Get() */)
{

	//Set_VertexType(vertexType);

	//context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	/*context->IASetVertexBuffers(0, 1, &vertexBuffer, &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(indexBuffer, DXGI_FORMAT_R32_UINT, 0);*/

	RenderStates::SetBlendState(mRenderState_Blend);
	RenderStates::SetDepthStencilState(mRenderState_Depth);
	RenderStates::SetRasterizerState(mRenderState_Rasterizer);

	switch (vertexType)
	{
	case VERTEX_TYPE_NONE:
		break;
	case VERTEX_TYPE_SIMPLE:
		break;
	case VERTEX_TYPE_POINTBILLBOARD:
		break;
	case VERTEX_TYPE_BASIC32:
		mVertexShader.SetShader_Basic32(context);
		break;
	case VERTEX_TYPE_BASIC32_TANGET:
		break;
	case VERTEX_TYPE_PARTICLE:
		break;
	case VERTEX_TYPE_SKINNED:
		mVertexShader.SetShader_Skinned(context);
		break;
	default:
		break;
	}

	mPixelShader.SetShader(context);
}

