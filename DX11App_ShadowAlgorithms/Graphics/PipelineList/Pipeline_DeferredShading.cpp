#include "Pipeline_DeferredShading.h"


using namespace StaticBuffers;

UINT PipelineList::Pipeline_DeferredShading::mVertexOffset = 0;
UINT PipelineList::Pipeline_DeferredShading::mVertexStride = 0;

ComPtr<ID3D11InputLayout> PipelineList::Pipeline_DeferredShading::pInputLayout = ComPtr<ID3D11InputLayout>(nullptr);

D3D11_PRIMITIVE_TOPOLOGY PipelineList::Pipeline_DeferredShading::mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

ShaderList::VertexShaders::VS_Render2D	PipelineList::Pipeline_DeferredShading::mVertexShader = ShaderList::VertexShaders::VS_Render2D();
ShaderList::PixelShaders::PS_DeferredShading PipelineList::Pipeline_DeferredShading::mPixelShader = ShaderList::PixelShaders::PS_DeferredShading();

void PipelineList::Pipeline_DeferredShading::Set_VertexType(VERTEX_TYPE vertexType, UINT vertexOffset)
{
	pInputLayout = Vertex::GetInputLayout(vertexType);
	Vertex::GetVertexStride(vertexType, mVertexStride);

	mVertexOffset = vertexOffset;
}

void PipelineList::Pipeline_DeferredShading::Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY primitiveTopology)
{
	mPrimitiveTopology = primitiveTopology;
}

void PipelineList::Pipeline_DeferredShading::ChangeRenderStates(RENDER_STATE_RS rs, RENDER_STATE_BS bs, RENDER_STATE_DS ds)
{
	mRenderState_Rasterizer = rs;
	mRenderState_Blend = bs;
	mRenderState_Depth = ds;
}

void PipelineList::Pipeline_DeferredShading::Init()
{
	/*mVertexShader_BuildShadowMap.Init();
	mPixelShader.Init();*/

	Set_VertexType(VERTEX_TYPE_BASIC32);
	Set_PrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	ChangeRenderStates(RENDER_STATE_RS_DEFAULT, RENDER_STATE_BS_DEFAULT, RENDER_STATE_DS_NODEPTH);
}

void PipelineList::Pipeline_DeferredShading::Apply_Preliminiary(
	/*ID3D11Buffer* vertexBuffer
	, ID3D11Buffer* indexBuffer,*/
	/*SHADER_CLASSINSTANCES_POSITION classInstance_position
	, SHADER_CLASSINSTANCES_GBUFFERMANAGER classInstance_gbufferManager
	, SHADER_CLASSINSTANCES_SPOTLIGHTSARRAY classInstance_spotLightArray
	, SHADER_CLASSINSTANCES_SHADOWMANAGER classInstance_shadowManager*/
	ID3D11DeviceContext* context)
{
	context->IASetInputLayout(pInputLayout.Get());
	context->IASetPrimitiveTopology(mPrimitiveTopology);
	context->IASetVertexBuffers(0, 1, gVertexBuffer_2D.GetAddressOf(), &mVertexStride, &mVertexOffset);
	context->IASetIndexBuffer(gIndexBuffer_2D.Get(), DXGI_FORMAT_R32_UINT, 0);

	/*switch (classInstance_position)
	{
	case SHADER_CLASSINSTANCES_POSITION_NOWVP:
		mVertexShader_BuildShadowMap.SetClassInstace_Position_noWVP();
		break;
	case SHADER_CLASSINSTANCES_POSITION_WVP:
		mVertexShader_BuildShadowMap.SetClassInstace_Position_WVP();
		break;
	}

	switch (classInstance_gbufferManager)
	{
	case SHADER_CLASSINSTANCES_GBUFFERMANAGER_BASIC:
		mPixelShader.SetClassInstace_GBufferManager_Basic();
		break;
	}

	switch (classInstance_spotLightArray)
	{
	case SHADER_CLASSINSTANCES_SPOTLIGHTSARRAY_DEFAULT:
		mPixelShader.SetClassInstace_SpotLightArray_Basic();
		break;
	}

	switch (classInstance_shadowManager)
	{
	case SHADER_CLASSINSTANCES_SHADOWMANAGER_BASICSHADOWMAP:
		mPixelShader.SetClassInstace_ShadowManager_BasicShadowMap();
		break;
	case SHADER_CLASSINSTANCES_SHADOWMANAGER_PCF:
		mPixelShader.SetClassInstace_ShadowManager_PCF();
		break;
	}*/

	RenderStates::SetRasterizerState(mRenderState_Rasterizer, context);
	RenderStates::SetBlendState(mRenderState_Blend, context);
	RenderStates::SetDepthStencilState(mRenderState_Depth, context);

	mVertexShader.SetShader(context);
	mPixelShader.SetShader(context);
}
