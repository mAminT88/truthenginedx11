#pragma once



class Engine;
class Model;
class IRenderTechnique;
class RenderTechnique_DeferredShading;
class RenderTechnique_DeferredShading_2;

class RenderEngine
{

private:

	Engine* mEngine;
	Model* mSelectedModel;

	std::unique_ptr<RenderTechnique_DeferredShading> mRenderTechnique_DeferredShading;
	std::unique_ptr<RenderTechnique_DeferredShading_2> mRenderTechnique_DeferredShading_2;

	IRenderTechnique* pCurrentTechnique;

	//std::vector<Model*>* mModels;

	void InitCamera();
	void InitViewPort();
	void InitStatics();
	void InitRenderTechniques();
	void InitSwapChain();
	void LoadLights(std::wstring lightsFilePath);

	void ImportOBJ(std::wstring ObjFilePath);

	void ChangeRenderTechnique(IRenderTechnique* pRTechnique);

	/*void LoadLevel(std::wstring _levelFilePath);
	void LoadLight(std::wstring _lightFilePath);*/

	//Input Events
	void OnMouseDown(WPARAM btnState, int x, int y);
	void OnMouseUp(WPARAM btnState, int x, int y);
	void OnMouseMove(WPARAM btnState, int x, int y);
	void ProcessKeyInputs();

	void FilterDirectxMessages();

	void RenderUI();
	void RenderUI_ModelModification();
	void UpdateBuffers();

public:

	RenderEngine();
	~RenderEngine();

	void Init(Engine* engine, int clientWidth, int clientHeight);


	void InitDXDevice();
	void Draw();
	void Update();
	void Catch_WCOMMAND(UINT msg, WPARAM wparam, LPARAM lparam);

	void RecompileShaders();

	//void SendModels(std::vector<Model*>* BasicModels, std::vector<SkinnedModel*>* SkinnedModels);
};
