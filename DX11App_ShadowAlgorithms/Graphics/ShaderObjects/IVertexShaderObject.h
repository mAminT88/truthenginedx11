#pragma once
#include "IShaderObject.h"

template<class T> class ShaderObject;

class IVertexShaderObject : public IShaderObject
{	
public:
	IVertexShaderObject();
	virtual ~IVertexShaderObject();
		
};
