#include "stdafx.h"
#include "PSO_GenerateGBuffers.h"

#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

namespace ShaderObjects
{
	namespace PixelShaders
	{
		ShaderObject<ID3D11PixelShader> PSO_GenerateGBuffers::mPS;

		UINT PSO_GenerateGBuffers::mNumInterfaces;
		UINT PSO_GenerateGBuffers::mISlot_MaterialBase;
		UINT PSO_GenerateGBuffers::mISlot_NormalManager;

		ComPtr<ID3D11ClassInstance> PSO_GenerateGBuffers::pSClass_MaterialBase;
		ComPtr<ID3D11ClassInstance> PSO_GenerateGBuffers::pSClass_MaterialTextured;
		ComPtr<ID3D11ClassInstance> PSO_GenerateGBuffers::pSClass_NormalManager_NormalVector;
		ComPtr<ID3D11ClassInstance> PSO_GenerateGBuffers::pSClass_NormalManager_NormalMap;
		
		int PSO_GenerateGBuffers::Init()
		{
			pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

			SetClassInstace_MaterialBase();
			SetClassInstace_NormalManager_NormalVector();
			
			return 0;
		}

		void PSO_GenerateGBuffers::InitStatics(bool fromHLSLFile)
		{
			std::string target = "ps_";
			target.append(SHADER_TARGET_VERSION);

			mPS.SetVariables(L"Data/Shaders/PS_GenerateGBuffers.tes", SHADER_FILE_PS_GENERATEGBUFFERS, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, nullptr);

			mPS.LoadShader(fromHLSLFile);

			auto pReflector = mPS.GetReflection();

			mNumInterfaces = pReflector->GetNumInterfaceSlots();

			ID3D11ShaderReflectionVariable* IMaterial = pReflector->GetVariableByName("g_IMaterialBase");
			ID3D11ShaderReflectionVariable* INormalManager = pReflector->GetVariableByName("g_INormalManager");

			mISlot_MaterialBase = IMaterial->GetInterfaceSlot(0);
			mISlot_NormalManager = INormalManager->GetInterfaceSlot(0);

			auto classLinkage = mPS.GetClassLinkage();


			classLinkage->CreateClassInstance("cMaterialBase", 4, 0, 0, 0, pSClass_MaterialBase.ReleaseAndGetAddressOf());
			classLinkage->CreateClassInstance("cMaterialTextured", 4, 0, 0, 0, pSClass_MaterialTextured.ReleaseAndGetAddressOf());
			classLinkage->CreateClassInstance("CNormalManager_NormalMap", 4, 0, 0, 0, pSClass_NormalManager_NormalMap.ReleaseAndGetAddressOf());
			classLinkage->CreateClassInstance("CNormalManager_NormalVector", 4, 0, 0, 0, pSClass_NormalManager_NormalVector.ReleaseAndGetAddressOf());

			
		}
		concurrency::task<void> PSO_GenerateGBuffers::CompileAsync()
		{
			return concurrency::task<void>([]() { InitStatics(); });
		}
		void PSO_GenerateGBuffers::SetClassInstace_MaterialBase()
		{
			if (mISlot_MaterialBase < 100)
				pDynamicClassLinkage[mISlot_MaterialBase] = pSClass_MaterialBase.Get();
		}
		void PSO_GenerateGBuffers::SetClassInstace_MaterialTextured()
		{
			if (mISlot_MaterialBase < 100)
				pDynamicClassLinkage[mISlot_MaterialBase] = pSClass_MaterialTextured.Get();
		}
		void PSO_GenerateGBuffers::SetClassInstace_NormalManager_NormalVector()
		{
			if (mISlot_NormalManager < 100)
				pDynamicClassLinkage[mISlot_NormalManager] = pSClass_NormalManager_NormalVector.Get();
		}
		void PSO_GenerateGBuffers::SetClassInstace_NormalManager_NormalMap()
		{
			if (mISlot_NormalManager < 100)
				pDynamicClassLinkage[mISlot_NormalManager] = pSClass_NormalManager_NormalMap.Get();
		}
		void PSO_GenerateGBuffers::SetShader(ID3D11DeviceContext* context)
		{
			mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
		}
		void PSO_GenerateGBuffers::RecompileShader()
		{
			InitStatics(true);
		}
	}
}
