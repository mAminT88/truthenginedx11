#pragma once
#include "IPixelShaderObject.h"

template<class T> class ShaderObject;

namespace ShaderObjects
{
	namespace PixelShaders
	{

		class PSO_GenerateGBuffers : public IPixelShaderObject
		{
		protected:
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_MaterialBase;
			static UINT mISlot_NormalManager;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_MaterialBase;
			static ComPtr<ID3D11ClassInstance> pSClass_MaterialTextured;
			static ComPtr<ID3D11ClassInstance> pSClass_NormalManager_NormalVector;
			static ComPtr<ID3D11ClassInstance> pSClass_NormalManager_NormalMap;

			//ClassInstaceArray
			ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			int Init() override;
			
			static void InitStatics(bool fromHLSLFile = false);
			static concurrency::task<void> CompileAsync();

			void SetClassInstace_MaterialBase();
			void SetClassInstace_MaterialTextured();
			void SetClassInstace_NormalManager_NormalVector();
			void SetClassInstace_NormalManager_NormalMap();

			void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();
		};
		
	}
}
