#include "stdafx.h"
#include "IVertexShaderObject.h"

IVertexShaderObject::IVertexShaderObject() = default;

IVertexShaderObject::~IVertexShaderObject() = default;
