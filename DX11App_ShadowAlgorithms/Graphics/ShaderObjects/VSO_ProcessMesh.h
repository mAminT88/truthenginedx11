#pragma once
#include "IVertexShaderObject.h"

template<class T> class ShaderObject;

namespace ShaderObjects
{
	namespace VertexShaders
	{

		class VSO_ProcessMesh : public IVertexShaderObject
		{
		protected:
			static ShaderObject<ID3D11VertexShader> mVS_Basic32;
			static ShaderObject<ID3D11VertexShader> mVS_Skinned;


		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();
			//void Init();

			static void SetShader_Basic32(ID3D11DeviceContext* context);
			static void SetShader_Skinned(ID3D11DeviceContext* context);
			static void RecompileShader();
		};
		
	}
}
