#pragma once
#include "IVertexShaderObject.h"

namespace ShaderObjects
{
	namespace VertexShaders
	{

		class VSO_BuildBasicShadowMap : public IVertexShaderObject
		{
		protected:
			static ShaderObject<ID3D11VertexShader> mVS_Basic32;
			static ShaderObject<ID3D11VertexShader> mVS_Skinned;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader_Basic32(ID3D11DeviceContext* context);
			static void SetShader_Skinned(ID3D11DeviceContext* context);
			static void RecompileShader();
		};

	}
}