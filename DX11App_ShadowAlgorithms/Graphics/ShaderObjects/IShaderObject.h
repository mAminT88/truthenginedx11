#pragma once


interface IShaderObject
{
public:
	IShaderObject();
	virtual ~IShaderObject();
	
	virtual int Init();
};