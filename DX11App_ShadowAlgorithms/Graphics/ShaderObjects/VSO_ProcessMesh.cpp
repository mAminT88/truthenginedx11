#include "stdafx.h"
#include "VSO_ProcessMesh.h"

#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11VertexShader> ShaderObjects::VertexShaders::VSO_ProcessMesh::mVS_Basic32;
ShaderObject<ID3D11VertexShader> ShaderObjects::VertexShaders::VSO_ProcessMesh::mVS_Skinned;

void ShaderObjects::VertexShaders::VSO_ProcessMesh::InitStatics(bool fromHLSLFile)
{
	std::string target = "vs_";
	target.append(SHADER_TARGET_VERSION);

	D3D_SHADER_MACRO macro[] = {
		{"VERTEX_TYPE_BASIC32", ""},
	{NULL, NULL}
	};

	mVS_Basic32.SetVariables(L"Data/Shaders/VS_ProcessMesh_Basic32.tes", SHADER_FILE_VS_PROCESSMESH, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macro);

	mVS_Basic32.LoadShader(fromHLSLFile);

	macro[0].Name = "VERTEX_TYPE_SKINNED";

	mVS_Skinned.SetVariables(L"Data/Shaders/VS_ProcessMesh_Skinned.tes", SHADER_FILE_VS_PROCESSMESH, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macro);

	mVS_Skinned.LoadShader(fromHLSLFile);
}

concurrency::task<void> ShaderObjects::VertexShaders::VSO_ProcessMesh::CompileAsync()
{
	return concurrency::task<void>([]() {InitStatics(); });
}

void ShaderObjects::VertexShaders::VSO_ProcessMesh::SetShader_Basic32(ID3D11DeviceContext* context)
{
	mVS_Basic32.SetShader(context, nullptr, 0);
}

void ShaderObjects::VertexShaders::VSO_ProcessMesh::SetShader_Skinned(ID3D11DeviceContext* context)
{
	mVS_Skinned.SetShader(context, nullptr, 0);
}

void ShaderObjects::VertexShaders::VSO_ProcessMesh::RecompileShader()
{
	InitStatics(true);
}
