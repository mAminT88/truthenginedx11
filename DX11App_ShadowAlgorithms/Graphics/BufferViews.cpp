#include "stdafx.h"
#include "BufferViews.h"

#include "Globals.h"

void BufferViews::Init(UINT _bindFlag
	, UINT _structureByteStride
	, UINT _byteWidth
	, UINT _numElement
	, D3D11_USAGE _usageFlag /*= D3D11_USAGE_DEFAULT */
	, D3D11_CPU_ACCESS_FLAG _cpuAccessFlag /*= static_cast<D3D11_CPU_ACCESS_FLAG>(0) */
	, UINT _miscFlag /*= 0 */
	, const D3D11_SUBRESOURCE_DATA* initData /*= nullptr*/)
{
	Release();

	mBindFlag = _bindFlag;
	mStructureByteStride - _structureByteStride;
	mUsageFlags = _usageFlag;
	mCpuAccessFlag = _cpuAccessFlag;
	mMiscFlags = _miscFlag;

	D3D11_BUFFER_DESC bdesc;
	bdesc.BindFlags = _bindFlag;
	bdesc.ByteWidth = _byteWidth;
	bdesc.CPUAccessFlags = _cpuAccessFlag;
	bdesc.MiscFlags = mMiscFlags;
	bdesc.StructureByteStride = _structureByteStride;
	bdesc.Usage = _usageFlag;

	Globals::gDevice->CreateBuffer(&bdesc, initData, mBuffer.GetAddressOf());

	if (_bindFlag & D3D11_BIND_SHADER_RESOURCE)
	{
		D3D11_SHADER_RESOURCE_VIEW_DESC sdesc = {};


		sdesc.Buffer.NumElements = _numElement;
		sdesc.Format = DXGI_FORMAT_UNKNOWN;
		sdesc.ViewDimension = D3D11_SRV_DIMENSION_BUFFER;

		Globals::gDevice->CreateShaderResourceView(mBuffer.Get(), &sdesc, mSRV.GetAddressOf());
	}

	if (_bindFlag & D3D11_BIND_UNORDERED_ACCESS)
	{

		D3D11_UNORDERED_ACCESS_VIEW_DESC udesc = {};
		udesc.Buffer.NumElements = _numElement;
		udesc.Format = DXGI_FORMAT_UNKNOWN;
		udesc.ViewDimension = D3D11_UAV_DIMENSION_BUFFER;

		Globals::gDevice->CreateUnorderedAccessView(mBuffer.Get(), &udesc, mUAV.GetAddressOf());

	}

}

ID3D11ShaderResourceView* BufferViews::GetSRV() const
{
	return mSRV.Get();
}

ID3D11ShaderResourceView* const* BufferViews::GetSRV_AddressOf() const
{
	return mSRV.GetAddressOf();
}

ID3D11UnorderedAccessView* BufferViews::GetUAV() const
{
	return mUAV.Get();
}

ID3D11UnorderedAccessView*const* BufferViews::GetUAV_AddressOf() const
{
	return mUAV.GetAddressOf();
}

ID3D11Buffer* BufferViews::GetBuffer() const
{
	return mBuffer.Get();
}

void BufferViews::Release()
{
	mBuffer.Reset();
	mSRV.Reset();
	mUAV.Reset();
}
