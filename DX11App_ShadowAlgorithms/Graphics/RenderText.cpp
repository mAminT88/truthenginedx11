#include "stdafx.h"
#include "RenderText.h"

RenderText::RenderText(HWND hwnd, ComPtr<IDXGISurface> dxgiSurface)
{
	mhwnd = hwnd;
	mDXGISurface = dxgiSurface;
	CreateDeviceIndependentResource();
	CreateDeviceResource();
}




//void RenderText::Init(HWND hwnd, IDXGISurface *dxgiSurfaces)
//{
//	mhwnd = hwnd;
//	mDXGISurface = dxgiSurfaces;
//	CreateDeviceIndependentResource();
//	CreateDeviceResource();
//
//}

//void RenderText::Destroy()
//{
//	DestroyDeviceResource();
//
//	mDWriteFactory->Release();
//	mTextFormat->Release();
//	mD2DFactory->Release();
//}

void RenderText::CreateDeviceIndependentResource()
{
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, mD2DFactory.ReleaseAndGetAddressOf());
	DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory), reinterpret_cast<IUnknown**>(mDWriteFactory.ReleaseAndGetAddressOf()));


	mDWriteFactory->CreateTextFormat(L"Arial",
		NULL,
		DWRITE_FONT_WEIGHT_REGULAR,
		DWRITE_FONT_STYLE_NORMAL,
		DWRITE_FONT_STRETCH_NORMAL,
		ConvertPointSizeToDIP(10.0f),
		L"en-us",
		&mTextFormat);

	mTextFormat->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
	mTextFormat->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
}

void RenderText::CreateDeviceResource()
{
	GetClientRect(mhwnd, &rc);

	D2D1_SIZE_U size = D2D1::SizeU(rc.right - rc.left, rc.bottom - rc.top);
	/*FLOAT dpiX, dpiY;
	mD2DFactory->GetDesktopDpi(&dpiX, &dpiY);*/

	const FLOAT dpi = static_cast<FLOAT>(GetDpiForWindow(mhwnd));

	/*if (!mRT)
	{
		mD2DFactory->CreateHwndRenderTarget(D2D1::RenderTargetProperties(), D2D1::HwndRenderTargetProperties(mhwnd, size), &mRT);
		mRT->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &mSolidColorBrush);
	}*/

	if (!mDXGIRT)
	{
		D2D1_RENDER_TARGET_PROPERTIES props = D2D1::RenderTargetProperties(
			D2D1_RENDER_TARGET_TYPE_DEFAULT,
			D2D1::PixelFormat(DXGI_FORMAT_UNKNOWN, D2D1_ALPHA_MODE_IGNORE),
			dpi,
			dpi
		);
		HRESULT r = mD2DFactory->CreateDxgiSurfaceRenderTarget(mDXGISurface.Get(), &props, &mDXGIRT);
		mDXGIRT->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &mSolidColorBrush);
	}
}

void RenderText::DestroyDeviceResource()
{
	mRT.Reset();
	mDXGIRT.Reset();
	mSolidColorBrush.Reset();
}

FLOAT RenderText::ConvertPointSizeToDIP(FLOAT points)
{
	return (points / 72.0f)*96.0f;
}

void RenderText::Render(const wchar_t *text)
{

	wsztext = text;
	mTextLength = static_cast<UINT32>(wcslen(wsztext));

	const D2D1_POINT_2F origin = D2D1::Point2F(originX, originY);
	mDWriteFactory->CreateTextLayout(wsztext, mTextLength, mTextFormat.Get(), static_cast<float>(rc.right - rc.left), static_cast<float>(rc.bottom - rc.top), &mTextLayout);

	//mRT->BeginDraw();
	//mRT->SetTransform(D2D1::IdentityMatrix());
	//mRT->Clear(D2D1::ColorF(D2D1::ColorF::Wheat));

	D2D1_RECT_F layoutRect = D2D1::RectF(static_cast<float>(rc.left), static_cast<float>(rc.top), static_cast<float>(rc.right - rc.left), static_cast<float>(rc.bottom - rc.top));

	//mRT->DrawTextLayout(origin, mTextLayout, mSolidColorBrush);

	//mRT->EndDraw();

	mDXGIRT->BeginDraw();
	mDXGIRT->SetTransform(D2D1::IdentityMatrix());
	//mDXGIRT->Clear(D2D1::ColorF(D2D1::ColorF::Azure));
	mDXGIRT->DrawTextLayout(origin, mTextLayout.Get(), mSolidColorBrush.Get());
	HRESULT r = mDXGIRT->EndDraw();
	originY += 30.0f;
}

void RenderText::ResetTextPosition()
{
	originX = 10.0f;
	originY = 10.0f;
}

void RenderText::ChangeColor(D2D1::ColorF _color)
{

	mDXGIRT->CreateSolidColorBrush(_color, &mSolidColorBrush);

}
