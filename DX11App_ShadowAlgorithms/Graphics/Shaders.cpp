#include "stdafx.h"
#include "Shaders.h"
#include "Globals.h"

using namespace Globals;


template<>
  HRESULT ShaderObject<ID3D11VertexShader>::CompileFromFile(LPCWSTR _pFileName, LPCSTR _pTarget, LPCSTR _entryPoint, ID3DInclude* _pInclude, D3D_SHADER_MACRO* _pDefines, UINT _flags)
{

	gDevice->CreateClassLinkage(pClassLinkage.ReleaseAndGetAddressOf());

	if (mHLSLFileName == L"")
	{
		m_shader.Reset();
		return 0;
	}

	ID3DBlob* blob;
	ComPtr<ID3DBlob> errorBlob;

	auto hresult = D3DCompileFromFile(_pFileName
		, _pDefines
		, _pInclude
		, _entryPoint
		, _pTarget
		, _flags
		, 0
		, &blob
		, &errorBlob);

	pData.reset(blob->GetBufferPointer());
	m_dataSize = blob->GetBufferSize();

	if (FAILED(hresult))
	{
		OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}

	m_dataSize = blob->GetBufferSize();

	CreateReflector(blob->GetBufferPointer(), blob->GetBufferSize());

	gDevice->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), pClassLinkage.Get(), m_shader.ReleaseAndGetAddressOf());

	return hresult;
}

template<>
  HRESULT ShaderObject<ID3D11PixelShader>::CompileFromFile(LPCWSTR _pFileName, LPCSTR _pTarget, LPCSTR _entryPoint, ID3DInclude* _pInclude, D3D_SHADER_MACRO* _pDefines, UINT _flags)
{
	gDevice->CreateClassLinkage(pClassLinkage.ReleaseAndGetAddressOf());


	if (mHLSLFileName == L"")
	{
		m_shader.Reset();
		return 0;
	}

	ID3DBlob* blob;
	ComPtr<ID3DBlob> errorBlob;

	auto hresult = D3DCompileFromFile(_pFileName
		, _pDefines
		, _pInclude
		, _entryPoint
		, _pTarget
		, _flags
		, 0
		, &blob
		, &errorBlob);

	if (FAILED(hresult))
	{
		OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}

	pData.reset(blob->GetBufferPointer());
	m_dataSize = blob->GetBufferSize();


	m_dataSize = blob->GetBufferSize();

	CreateReflector(blob->GetBufferPointer(), blob->GetBufferSize());

	gDevice->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), pClassLinkage.Get(), m_shader.ReleaseAndGetAddressOf());

	return hresult;
}

template<>
  HRESULT ShaderObject<ID3D11ComputeShader>::CompileFromFile(LPCWSTR _pFileName, LPCSTR _pTarget, LPCSTR _entryPoint, ID3DInclude* _pInclude, D3D_SHADER_MACRO* _pDefines, UINT _flags)
{
	gDevice->CreateClassLinkage(pClassLinkage.ReleaseAndGetAddressOf());


	if (mHLSLFileName == L"")
	{
		m_shader.Reset();
		return 0;
	}

	ID3DBlob* blob;
	ComPtr<ID3DBlob> errorBlob;

	auto hresult = D3DCompileFromFile(_pFileName
		, _pDefines
		, _pInclude
		, _entryPoint
		, _pTarget
		, _flags
		, 0
		, &blob
		, &errorBlob);

	pData.reset(blob->GetBufferPointer());
	m_dataSize = blob->GetBufferSize();

	if (FAILED(hresult))
	{
		OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}

	m_dataSize = blob->GetBufferSize();

	CreateReflector(blob->GetBufferPointer(), blob->GetBufferSize());

	gDevice->CreateComputeShader(blob->GetBufferPointer(), blob->GetBufferSize(), pClassLinkage.Get(), m_shader.ReleaseAndGetAddressOf());

	return hresult;
}

template<>
  HRESULT ShaderObject<ID3D11HullShader>::CompileFromFile(LPCWSTR _pFileName, LPCSTR _pTarget, LPCSTR _entryPoint, ID3DInclude* _pInclude, D3D_SHADER_MACRO* _pDefines, UINT _flags)
{

	gDevice->CreateClassLinkage(pClassLinkage.ReleaseAndGetAddressOf());


	if (mHLSLFileName == L"")
	{
		m_shader.Reset();
		return 0;
	}

	ID3DBlob* blob;
	ComPtr<ID3DBlob> errorBlob;

	auto hresult = D3DCompileFromFile(_pFileName
		, _pDefines
		, _pInclude
		, _entryPoint
		, _pTarget
		, _flags
		, 0
		, &blob
		, &errorBlob);

	pData.reset(blob->GetBufferPointer());
	m_dataSize = blob->GetBufferSize();

	if (FAILED(hresult))
	{
		OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}

	m_dataSize = blob->GetBufferSize();

	CreateReflector(blob->GetBufferPointer(), blob->GetBufferSize());

	gDevice->CreateHullShader(blob->GetBufferPointer(), blob->GetBufferSize(), pClassLinkage.Get(), m_shader.ReleaseAndGetAddressOf());

	return hresult;
}

template<>
  HRESULT ShaderObject<ID3D11DomainShader>::CompileFromFile(LPCWSTR _pFileName, LPCSTR _pTarget, LPCSTR _entryPoint, ID3DInclude* _pInclude, D3D_SHADER_MACRO* _pDefines, UINT _flags)
{

	gDevice->CreateClassLinkage(pClassLinkage.ReleaseAndGetAddressOf());


	if (mHLSLFileName == L"")
	{
		m_shader.Reset();
		return 0;
	}

	ID3DBlob* blob;
	ComPtr<ID3DBlob> errorBlob;

	auto hresult = D3DCompileFromFile(_pFileName
		, _pDefines
		, _pInclude
		, _entryPoint
		, _pTarget
		, _flags
		, 0
		, &blob
		, &errorBlob);

	pData.reset(blob->GetBufferPointer());
	m_dataSize = blob->GetBufferSize();

	if (FAILED(hresult))
	{
		OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}

	m_dataSize = blob->GetBufferSize();

	CreateReflector(blob->GetBufferPointer(), blob->GetBufferSize());

	gDevice->CreateDomainShader(blob->GetBufferPointer(), blob->GetBufferSize(), pClassLinkage.Get(), m_shader.ReleaseAndGetAddressOf());

	return hresult;
}

template<>
  HRESULT ShaderObject<ID3D11GeometryShader>::CompileFromFile(LPCWSTR _pFileName, LPCSTR _pTarget, LPCSTR _entryPoint, ID3DInclude* _pInclude, D3D_SHADER_MACRO* _pDefines, UINT _flags)
{

	gDevice->CreateClassLinkage(pClassLinkage.ReleaseAndGetAddressOf());


	if (mHLSLFileName == L"")
	{
		m_shader.Reset();
		return 0;
	}

	ID3DBlob* blob;
	ComPtr<ID3DBlob> errorBlob;

	auto hresult = D3DCompileFromFile(_pFileName
		, _pDefines
		, _pInclude
		, _entryPoint
		, _pTarget
		, _flags
		, 0
		, &blob
		, &errorBlob);

	pData.reset(blob->GetBufferPointer());
	m_dataSize = blob->GetBufferSize();

	if (FAILED(hresult))
	{
		OutputDebugStringA((char*)errorBlob->GetBufferPointer());
	}

	m_dataSize = blob->GetBufferSize();

	CreateReflector(blob->GetBufferPointer(), blob->GetBufferSize());

	gDevice->CreateGeometryShader(blob->GetBufferPointer(), blob->GetBufferSize(), pClassLinkage.Get(), m_shader.ReleaseAndGetAddressOf());

	return hresult;
}



template<>
  void ShaderObject<ID3D11VertexShader>::SetShader(ID3D11DeviceContext* context, ID3D11ClassInstance** _classInstances, int _classInstances_number) const
{
	context->VSSetShader(m_shader.Get(), _classInstances, _classInstances_number);
}

template<>
  void ShaderObject<ID3D11PixelShader>::SetShader(ID3D11DeviceContext* context, ID3D11ClassInstance** _classInstances, int _classInstances_number) const
{
	context->PSSetShader(m_shader.Get(), _classInstances, _classInstances_number);
}

template<>
  void ShaderObject<ID3D11ComputeShader>::SetShader(ID3D11DeviceContext* context, ID3D11ClassInstance** _classInstances, int _classInstances_number) const
{
	context->CSSetShader(m_shader.Get(), _classInstances, _classInstances_number);
}

template<>
  void ShaderObject<ID3D11HullShader>::SetShader(ID3D11DeviceContext* context, ID3D11ClassInstance** _classInstances, int _classInstances_number) const
{
	context->HSSetShader(m_shader.Get(), _classInstances, _classInstances_number);
}

template<>
  void ShaderObject<ID3D11DomainShader>::SetShader(ID3D11DeviceContext* context, ID3D11ClassInstance** _classInstances, int _classInstances_number) const
{
	context->DSSetShader(m_shader.Get(), _classInstances, _classInstances_number);
}

template<>
  void ShaderObject<ID3D11GeometryShader>::SetShader(ID3D11DeviceContext* context, ID3D11ClassInstance** _classInstances, int _classInstances_number) const
{
	context->GSSetShader(m_shader.Get(), _classInstances, _classInstances_number);
}










