#pragma once





namespace ShaderList
{
	
	void InitAllStatics(bool fromHLSLFile = false);


			/*std::vector<std::future<void>> futures;

			futures.push_back(VertexShaders::VS_Render2D::CompileAsync());
			futures.push_back(VertexShaders::VS_ProcessMesh::CompileAsync());
			futures.push_back(VertexShaders::VS_BuildShadowMap::CompileAsync());

			futures.push_back(PixelShaders::PS_DeferredShading::CompileAsync());

			futures.push_back(PixelShaders::PS_GenerateGBuffers::CompileAsync());
			futures.push_back(PixelShaders::PS_GenerateVarianceShadowMap::CompileAsync());
			futures.push_back(PixelShaders::PS_SeperableBlurFilter::CompileAsync());
			futures.push_back(PixelShaders::PS_DownUpSampling::CompileAsync());
			futures.push_back(PixelShaders::PS_GenerateSAT::CompileAsync());
			futures.push_back(PixelShaders::PS_GenerateSAT_UINT::CompileAsync());
			futures.push_back(PixelShaders::PS_GenerateMomentShadowMap::CompileAsync());
			futures.push_back(PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::CompileAsync());
			futures.push_back(PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::CompileAsync());
			futures.push_back(PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::CompileAsync());
			futures.push_back(PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::CompileAsync());
			futures.push_back(PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::CompileAsync());
			futures.push_back(PixelShaders::PS_SSSM_GenerateShadowData_0::CompileAsync());
			futures.push_back(PixelShaders::PS_SSSM_GenerateShadowData_1::CompileAsync());
			futures.push_back(PixelShaders::PS_SSSM_GenerateShadowData_2::CompileAsync());
			futures.push_back(PixelShaders::PS_BlurBilateralByDepthNormal::CompileAsync());
			futures.push_back(PixelShaders::PS_SSAO::CompileAsync());
			futures.push_back(PixelShaders::PS_PostFX_FinalPass::CompileAsync());
			
			futures.push_back(ComputeShaders::CS_FindBlocker::CompileAsync());
			futures.push_back(ComputeShaders::CS_GenerateSAT_Horz::CompileAsync());
			futures.push_back(ComputeShaders::CS_GenerateSAT_Vert::CompileAsync());
			futures.push_back(ComputeShaders::CS_HDR_AvgLuminance::CompileAsync());
			futures.push_back(ComputeShaders::CS_Bloom::CompileAsync());
			futures.push_back(ComputeShaders::CS_GaussianBlur::CompileAsync());
			futures.push_back(ComputeShaders::CS_DownSampling::CompileAsync());


			for ( auto& f : futures)
			{
				f.get();
			}*/

			/*concurrency::task_group tasks;

			tasks.run([]() {VertexShaders::VS_Render2D::InitStatics(); });
			tasks.run([]() {VertexShaders::VS_ProcessMesh::InitStatics(); });
			tasks.run([]() {VertexShaders::VS_BuildShadowMap::InitStatics(); });

			tasks.run([]() {PixelShaders::PS_DeferredShading::InitStatics(); });

			tasks.run([]() {PixelShaders::PS_GenerateGBuffers::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_GenerateVarianceShadowMap::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_SeperableBlurFilter::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_DownUpSampling::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_GenerateSAT::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_GenerateSAT_UINT::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_GenerateMomentShadowMap::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_SSSM_GenerateShadowData_0::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_SSSM_GenerateShadowData_1::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_SSSM_GenerateShadowData_2::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_BlurBilateralByDepthNormal::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_SSAO::InitStatics(); });
			tasks.run([]() {PixelShaders::PS_PostFX_FinalPass::InitStatics(); });
			
			tasks.run([]() {ComputeShaders::CS_FindBlocker::InitStatics(); });
			tasks.run([]() {ComputeShaders::CS_GenerateSAT_Horz::InitStatics(); });
			tasks.run([]() {ComputeShaders::CS_GenerateSAT_Vert::InitStatics(); });
			tasks.run([]() {ComputeShaders::CS_HDR_AvgLuminance::InitStatics(); });
			tasks.run([]() {ComputeShaders::CS_Bloom::InitStatics(); });
			tasks.run([]() {ComputeShaders::CS_GaussianBlur::InitStatics(); });
			tasks.run([]() {ComputeShaders::CS_DownSampling::InitStatics(); });

			tasks.wait();*/

}

