#include "stdafx.h"
#include "AdapterReader.h"
#include "ErrorLogger.h"
#include "COMException.h"

std::vector<AdapterData> AdapterReader::adapters;

std::vector<AdapterData> AdapterReader::GetAdapters()
{
	try {
		if (!adapters.empty())
			return adapters;

		ComPtr<IDXGIFactory> pFactory;

		HRESULT hr = CreateDXGIFactory(IID_PPV_ARGS(pFactory.ReleaseAndGetAddressOf()));
		COM_ERROR_IF_FAILED(hr, "Failed to create DXGIFactory for enumrating adapters.");

		ComPtr<IDXGIAdapter> pAdapter;
		UINT index = 0;
		while (SUCCEEDED(pFactory->EnumAdapters(index, &pAdapter)))
		{
			adapters.emplace_back(AdapterData(pAdapter));
			index += 1;
		}

		return adapters;
	}
	catch (COMException& ex)
	{
		ErrorLogger::Log(ex);
		exit(-1);
	}
}

AdapterData::AdapterData(ComPtr<IDXGIAdapter> pAdapter)
{
	try {
		this->pAdapter = pAdapter;
		HRESULT hr = pAdapter->GetDesc(&this->description);
		COM_ERROR_IF_FAILED(hr, "Failed to get description for IDXGIAdapter.");
	}
	catch (COMException& ex)
	{
		ErrorLogger::Log(ex);
		exit(-1);
	}

}
