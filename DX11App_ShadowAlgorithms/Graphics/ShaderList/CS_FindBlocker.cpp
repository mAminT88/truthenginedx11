#include "stdafx.h"
#include "CS_FindBlocker.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11ComputeShader> ShaderList::ComputeShaders::CS_FindBlocker::mCS = ShaderObject<ID3D11ComputeShader>();

UINT ShaderList::ComputeShaders::CS_FindBlocker::mNumInterfaces = 0;

UINT ShaderList::ComputeShaders::CS_FindBlocker::mISlot_FindBlocker = 0;

UINT ShaderList::ComputeShaders::CS_FindBlocker::mThreadGroupSizeX = 256;

UINT ShaderList::ComputeShaders::CS_FindBlocker::mThreadGroupSizeY = 1;

UINT ShaderList::ComputeShaders::CS_FindBlocker::mBlockerSearchSampleCount = 25;

ComPtr<ID3D11ClassInstance> ShaderList::ComputeShaders::CS_FindBlocker::pSClass_FindBlocker_Horz = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::ComputeShaders::CS_FindBlocker::pSClass_FindBlocker_Vert = nullptr;

ID3D11ClassInstance** ShaderList::ComputeShaders::CS_FindBlocker::pDynamicClassLinkage = nullptr;

void ShaderList::ComputeShaders::CS_FindBlocker::InitStatics(bool fromHLSLFile)
{
	std::string target = "cs_";
	target.append(SHADER_TARGET_VERSION);

	auto value_1 = std::to_string(mThreadGroupSizeX);
	auto value_2 = std::to_string(mThreadGroupSizeY);
	auto value_3 = std::to_string(mBlockerSearchSampleCount);

	D3D_SHADER_MACRO macros[] =
	{
		{ "ThreadGroupSize_X", value_1.c_str() },
		{ "ThreadGroupSize_Y", value_2.c_str() },
		{ "BLOCKER_SEARCH_SAMPLE_COUNT", value_3.c_str() },
	{ NULL,NULL }
	};


	mCS.SetVariables(L"Data/Shaders/CS_FindBlocker.tes", SHADER_FILE_CS_OSSSS_SEPARABLEFindBlocker, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mCS.LoadShader(fromHLSLFile);

	auto pReflector = mCS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();


	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);


	ID3D11ShaderReflectionVariable* IFindBlocker = pReflector->GetVariableByName("gIFindBlocker");

	mISlot_FindBlocker = IFindBlocker->GetInterfaceSlot(0);

	auto classLinkage = mCS.GetClassLinkage();

	classLinkage->CreateClassInstance("CFindBlocker_Horz", 4, 0, 0, 0, pSClass_FindBlocker_Horz.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CFindBlocker_Vert", 4, 0, 0, 0, pSClass_FindBlocker_Vert.ReleaseAndGetAddressOf());

	SetClassInstace_FindBlocker_Horz();
}

concurrency::task<void> ShaderList::ComputeShaders::CS_FindBlocker::CompileAsync()
{
	return concurrency::task<void>([]() {InitStatics(); });
}

void ShaderList::ComputeShaders::CS_FindBlocker::SetClassInstace_FindBlocker_Horz()
{
	pDynamicClassLinkage[mISlot_FindBlocker] = pSClass_FindBlocker_Horz.Get();
}

void ShaderList::ComputeShaders::CS_FindBlocker::SetClassInstace_FindBlocker_Vert()
{
	pDynamicClassLinkage[mISlot_FindBlocker] = pSClass_FindBlocker_Vert.Get();
}

void ShaderList::ComputeShaders::CS_FindBlocker::SetShader(ID3D11DeviceContext* context)
{
	mCS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::ComputeShaders::CS_FindBlocker::RecompileShader()
{
	InitStatics(true);
}

UINT ShaderList::ComputeShaders::CS_FindBlocker::GetThreadGroupSizeX()
{
	return mThreadGroupSizeX;
}

UINT ShaderList::ComputeShaders::CS_FindBlocker::GetThreadGroupSizeY()
{
	return mThreadGroupSizeY;
}

void ShaderList::ComputeShaders::CS_FindBlocker::SetStaticValue(UINT ThreadGroupSizeX, UINT ThreadGroupSizeY, UINT BlockerSearchSampleCount)
{
	mThreadGroupSizeX = ThreadGroupSizeX;
	mThreadGroupSizeY = ThreadGroupSizeY;
	mBlockerSearchSampleCount = BlockerSearchSampleCount;
}
