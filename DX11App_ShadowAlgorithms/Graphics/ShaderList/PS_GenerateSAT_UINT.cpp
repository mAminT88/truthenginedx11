#include "stdafx.h"
#include "PS_GenerateSAT_UINT.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_GenerateSAT_UINT::mPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_GenerateSAT_UINT::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_GenerateSAT_UINT::mISlot_SAT = 0;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateSAT_UINT::pSClass_SAT_UINT_Horizontal = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateSAT_UINT::pSClass_SAT_UINT_Horizontal_0 = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateSAT_UINT::pSClass_SAT_UINT_Vertical = nullptr;

ID3D11ClassInstance** ShaderList::PixelShaders::PS_GenerateSAT_UINT::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_GenerateSAT_UINT::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	D3D_SHADER_MACRO macros[] =
	{
		{ "SAMPLE_NUMBER", "8" },
		{ NULL,NULL }
	};

	mPS.SetVariables(L"Data/Shaders/PS_GenerateSAT_UINT.tes", SHADER_FILE_PS_GENERATESAT, target.c_str(), "main_uint", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);

	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();


	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);


	ID3D11ShaderReflectionVariable* ISAT = pReflector->GetVariableByName("g_ISAT");

	mISlot_SAT = ISAT->GetInterfaceSlot(0);

	auto classLinkage = mPS.GetClassLinkage();

	classLinkage->CreateClassInstance("CSAT_Horizontal", 4, 0, 0, 0, pSClass_SAT_UINT_Horizontal.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CSAT_Horizontal_0_UINT", 4, 0, 0, 0, pSClass_SAT_UINT_Horizontal_0.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CSAT_Vertical", 4, 0, 0, 0, pSClass_SAT_UINT_Vertical.ReleaseAndGetAddressOf());

	SetClassInstace_SAT_UINT_Horizontal();
}

concurrency::task<void> ShaderList::PixelShaders::PS_GenerateSAT_UINT::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetClassInstace_SAT_UINT_Horizontal()
{
	pDynamicClassLinkage[mISlot_SAT] = pSClass_SAT_UINT_Horizontal.Get();
}

void ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetClassInstace_SAT_UINT_Horizontal_0()
{
	pDynamicClassLinkage[mISlot_SAT] = pSClass_SAT_UINT_Horizontal_0.Get();
}

void ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetClassInstace_SAT_UINT_Vertical()
{
	pDynamicClassLinkage[mISlot_SAT] = pSClass_SAT_UINT_Vertical.Get();
}

void ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetShader(ID3D11DeviceContext* context)
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_GenerateSAT_UINT::RecompileShader()
{
	InitStatics(true);
}
