#pragma once

#include "IPixelShader.h"
#include "Macros.h"

template<class T> class ShaderObject;



namespace ShaderList {

	namespace PixelShaders {

		class PS_OSSSS_GenerateShadowData_Pass1 : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_GenerateShadowData_1;
			static UINT mISlot_ILight;


			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_GenerateShadowData_1_Horz;
			static ComPtr<ID3D11ClassInstance> pSClass_GenerateShadowData_1_Vert;
			static ComPtr<ID3D11ClassInstance> pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
			static ComPtr<ID3D11ClassInstance> pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];



			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:

			enum class PS_OSSSS_GenerateShadowData_Pass1_Version
			{
				normal,
				cross_product
			};

			static PS_OSSSS_GenerateShadowData_Pass1_Version PSVersion;

			static float mBilateralDepth, mFilterWidth, mBlockerSearchSampleCount;

			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);

			static void RecompileShader();

			static void Set_ClassInstance_HorizontalStep();
			static void Set_ClassInstance_VerticalStep();

			static void Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex);
			static void Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex);
		};

	};

}
