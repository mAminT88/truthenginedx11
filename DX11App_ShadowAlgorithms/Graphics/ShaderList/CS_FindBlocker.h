#pragma once

#include "IComputeShader.h"

template<class T> class ShaderObject;

namespace ShaderList
{
	namespace ComputeShaders
	{
		class CS_FindBlocker : public IComputeShader
		{
		protected:
			static ShaderObject<ID3D11ComputeShader> mCS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_FindBlocker;

			static UINT mThreadGroupSizeX;
			static UINT mThreadGroupSizeY;
			static UINT mBlockerSearchSampleCount;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_FindBlocker_Horz;
			static ComPtr<ID3D11ClassInstance> pSClass_FindBlocker_Vert;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetClassInstace_FindBlocker_Horz();
			static void SetClassInstace_FindBlocker_Vert();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

			static UINT GetThreadGroupSizeX();
			static UINT GetThreadGroupSizeY();

			static void SetStaticValue(UINT ThreadGroupSizeX, UINT ThreadGroupSizeY, UINT BlockerSearchSampleCount);
		};

	};
}
