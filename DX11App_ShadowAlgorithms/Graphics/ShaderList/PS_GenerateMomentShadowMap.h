#pragma once

#include "IPixelShader.h"
#include "Macros.h"

template<class T> class ShaderObject;




namespace ShaderList
{
	namespace PixelShaders
	{

		class PS_GenerateMomentShadowMap : public IPixelShader
		{
		private:
			static ShaderObject<ID3D11PixelShader> mPS;

			static int mNumInterfaces;
			static int mISlot_ILight;

			static ComPtr<ID3D11ClassInstance> pPSClass_Light_Spots[MAX_LIGHT_SPOTLIGHT];
			static ComPtr<ID3D11ClassInstance> pPSClass_Light_Directs[MAX_LIGHT_DIRECTIONALLIGHT];

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);
			static concurrency::task<void> CompileAsync();


			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

			static void SetClassInstance_ILight_Spot(int Index);
			static void SetClassInstance_ILight_Direct(int Index);
		};

	};
}