#pragma once

#include "IPixelShader.h"

template<class T> class ShaderObject;



namespace ShaderList
{
	namespace PixelShaders
	{

		class PS_SeperableBlurFilter : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> pPS_GaussFilter;
			static ShaderObject<ID3D11PixelShader> pPS_BoxFilter;

			//Interfaces Slot
			static UINT mNumInterfaces_Gauss;
			static UINT mNumInterfaces_Box;

			static UINT mISlot_GaussFilterManager;
			static UINT mISlot_BoxFilterManager;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_GaussFilterManager_Horizontally;
			static ComPtr<ID3D11ClassInstance> pSClass_GaussFilterManager_Vertically;

			static ComPtr<ID3D11ClassInstance> pSClass_BoxFilterManager_Horizontally;
			static ComPtr<ID3D11ClassInstance> pSClass_BoxFilterManager_Vertically;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage_Gauss;
			static ID3D11ClassInstance** pDynamicClassLinkage_Box;

		public:

			static float FilterWidth;

			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetClassInstace_GaussFilterManager_Horizontally();
			static void SetClassInstace_GaussFilterManager_Vertically();

			static void SetClassInstace_BoxFilterManager_Horizontally();
			static void SetClassInstace_BoxFilterManager_Vertically();

			static void SetShader_GaussFilter(ID3D11DeviceContext* context);
			static void SetShader_BoxFilter(ID3D11DeviceContext* context);
			static void RecompileShader();
		};

	}
	
}

