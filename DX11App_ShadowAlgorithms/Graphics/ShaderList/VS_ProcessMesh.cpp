#include "stdafx.h"
#include "VS_ProcessMesh.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"


ShaderObject<ID3D11VertexShader> ShaderList::VertexShaders::VS_ProcessMesh::pVS_Basic32 = ShaderObject<ID3D11VertexShader>();
ShaderObject<ID3D11VertexShader> ShaderList::VertexShaders::VS_ProcessMesh::pVS_Skinned = ShaderObject<ID3D11VertexShader>();


void ShaderList::VertexShaders::VS_ProcessMesh::InitStatics(bool fromHLSLFile)
{
	std::string target = "vs_";
	target.append(SHADER_TARGET_VERSION);

	D3D_SHADER_MACRO macro[] = {
		{"VERTEX_TYPE_BASIC32", ""},
	{NULL, NULL}
	};

	pVS_Basic32.SetVariables(L"Data/Shaders/VS_ProcessMesh_Basic32.tes", SHADER_FILE_VS_PROCESSMESH, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macro);

	pVS_Basic32.LoadShader(fromHLSLFile);

	macro[0].Name = "VERTEX_TYPE_SKINNED";

	pVS_Skinned.SetVariables(L"Data/Shaders/VS_ProcessMesh_Skinned.tes", SHADER_FILE_VS_PROCESSMESH, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macro);

	pVS_Skinned.LoadShader(fromHLSLFile);

}

concurrency::task<void> ShaderList::VertexShaders::VS_ProcessMesh::CompileAsync()
{
	return concurrency::task<void>([]() {InitStatics(); });
}

//void ShaderList::VertexShaders::VS_Default_Basic32Vertex::Init()
//{
//	
//}

void ShaderList::VertexShaders::VS_ProcessMesh::SetShader_Basic32(ID3D11DeviceContext* context)
{
	pVS_Basic32.SetShader(context, nullptr, 0);
}

void ShaderList::VertexShaders::VS_ProcessMesh::SetShader_Skinned(ID3D11DeviceContext* context)
{
	pVS_Skinned.SetShader(context, nullptr, 0);
}

void ShaderList::VertexShaders::VS_ProcessMesh::RecompileShader()
{
	InitStatics(true);
	//Init();
}
