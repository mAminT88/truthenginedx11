#include "stdafx.h"
#include "PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

//
// Pass 0
//

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mISlot_GenerateShadowData_0 = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mISlot_ILight = 0;

int ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mBlockerSearchWidthSampleCount = 25;

int ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mHardShadowFilterSampleCount = 25;

float ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mBilateralDepth = 20.0f;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::pSClass_GenerateShadowData_0 = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];


ID3D11ClassInstance** ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	auto blockerSearchSampleCountS = std::to_string(mBlockerSearchWidthSampleCount);
	auto blockerSearchGaussian = std::string("Gaussian") + blockerSearchSampleCountS;
	auto hardShadowFilterSampleCountS = std::to_string(mHardShadowFilterSampleCount);
	auto hardShadowFilterGaussian = std::string("Gaussian") + hardShadowFilterSampleCountS;
	auto bilateralDepthS = std::to_string(mBilateralDepth);


	D3D_SHADER_MACRO macros[] = {
		{ "BLOCKER_SEARCH_WIDTH_SAMPLE_COUNT", blockerSearchSampleCountS.c_str() },
	{ "GAUSSIAN_WEIGHTS_BLOCKER_SEARCH", blockerSearchGaussian.c_str() },
	{ "FILTER_WIDTH_HARDSHADOW", hardShadowFilterSampleCountS.c_str() },
	{ "GAUSSIAN_WEIGHTS_HARDSHADOW", hardShadowFilterGaussian.c_str() },
	{ "BILATERAL_DEPTH", bilateralDepthS.c_str() },
	{ NULL, NULL }
	};


	mPS.SetVariables(L"Data/Shaders/PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_0.tes", SHADER_FILE_PS_OSSSS_GENERATESHADOWDATA_SCREENSPACESEPARABLEBLOCKERSEARCH, target.c_str(), "main_0", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);

	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IGenerateShadowData_0 = pReflector->GetVariableByName("g_IGenerateShadowData_0");
	ID3D11ShaderReflectionVariable* ILight = pReflector->GetVariableByName("iLight");


	mISlot_GenerateShadowData_0 = IGenerateShadowData_0->GetInterfaceSlot(0);
	mISlot_ILight = ILight->GetInterfaceSlot(0);

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);


	auto classLinkage = mPS.GetClassLinkage();


	classLinkage->CreateClassInstance("CGenerateShadowData_0", 0, 0, 0, 0, pSClass_GenerateShadowData_0.ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_SPOTLIGHT; ++i)
		classLinkage->GetClassInstance("gSpotLights", 0, pSClass_Light_SpotLights[i].ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_DIRECTIONALLIGHT; ++i)
		classLinkage->GetClassInstance("gDirectLights", 0, pSClass_Light_DirectLights[i].ReleaseAndGetAddressOf());

	if (mISlot_GenerateShadowData_0 < 100)
		pDynamicClassLinkage[mISlot_GenerateShadowData_0] = pSClass_GenerateShadowData_0.Get();

	
}

concurrency::task<void> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::SetShader(ID3D11DeviceContext* context)
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_SpotLights[SpotLightIndex].Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_DirectLights[DirectLightIndex].Get();
}

//
// Pass 1
//

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mISlot_GenerateShadowData_1 = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mISlot_ILight = 0;

int ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mBlockerSearchWidthSampleCount = 5;

int ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mHardShadowFilterSampleCount = 11;

float ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mBilateralDepth = 20.0f;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::pSClass_GenerateShadowData_1 = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];

ID3D11ClassInstance** ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	auto blockerSearchSampleCountS = std::to_string(mBlockerSearchWidthSampleCount);
	auto blockerSearchGaussian = std::string("Gaussian") + blockerSearchSampleCountS;
	auto hardShadowFilterSampleCountS = std::to_string(mHardShadowFilterSampleCount);
	auto hardShadowFilterGaussian = std::string("Gaussian") + hardShadowFilterSampleCountS;
	auto bilateralDepthS = std::to_string(mBilateralDepth);


	D3D_SHADER_MACRO macros[] = {
		{ "BLOCKER_SEARCH_WIDTH_SAMPLE_COUNT", blockerSearchSampleCountS.c_str() },
	{ "GAUSSIAN_WEIGHTS_BLOCKER_SEARCH", blockerSearchGaussian.c_str() },
	{ "FILTER_WIDTH_HARDSHADOW", hardShadowFilterSampleCountS.c_str() },
	{ "GAUSSIAN_WEIGHTS_HARDSHADOW", hardShadowFilterGaussian.c_str() },
	{ "BILATERAL_DEPTH", bilateralDepthS.c_str() },
	{ NULL, NULL }
	};


	mPS.SetVariables(L"Data/Shaders/PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_1.tes", SHADER_FILE_PS_OSSSS_GENERATESHADOWDATA_SCREENSPACESEPARABLEBLOCKERSEARCH, target.c_str(), "main_1", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);

	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IGenerateShadowData_1 = pReflector->GetVariableByName("g_IGenerateShadowData_1");
	ID3D11ShaderReflectionVariable* ILight = pReflector->GetVariableByName("iLight");


	mISlot_GenerateShadowData_1 = IGenerateShadowData_1->GetInterfaceSlot(0);
	mISlot_ILight = ILight->GetInterfaceSlot(0);

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);


	auto classLinkage = mPS.GetClassLinkage();


	classLinkage->CreateClassInstance("CGenerateShadowData_1", 0, 0, 0, 0, pSClass_GenerateShadowData_1.ReleaseAndGetAddressOf());


	for (int i = 0; i < MAX_LIGHT_SPOTLIGHT; ++i)
		classLinkage->GetClassInstance("gSpotLights", 0, pSClass_Light_SpotLights[i].ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_DIRECTIONALLIGHT; ++i)
		classLinkage->GetClassInstance("gDirectLights", 0, pSClass_Light_DirectLights[i].ReleaseAndGetAddressOf());


	Set_ClassInstance_GenerateShadowData1();
}

concurrency::task<void> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::SetShader(ID3D11DeviceContext* context)
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::Set_ClassInstance_GenerateShadowData1()
{
	if (mISlot_GenerateShadowData_1 < 100)
		pDynamicClassLinkage[mISlot_GenerateShadowData_1] = pSClass_GenerateShadowData_1.Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_SpotLights[SpotLightIndex].Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_DirectLights[DirectLightIndex].Get();
}

//
// Pass 2
//

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mISlot_GenerateShadowData_2 = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mISlot_ILight = 0;

int ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mBlockerSearchWidthSampleCount = 25;

int ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mHardShadowFilterSampleCount = 25;

float ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mBilateralDepth = 20.0f;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::pSClass_GenerateShadowData_2 = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];

ID3D11ClassInstance** ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	auto blockerSearchSampleCountS = std::to_string(mBlockerSearchWidthSampleCount);
	auto blockerSearchGaussian = std::string("Gaussian") + blockerSearchSampleCountS;
	auto hardShadowFilterSampleCountS = std::to_string(mHardShadowFilterSampleCount);
	auto hardShadowFilterGaussian = std::string("Gaussian") + hardShadowFilterSampleCountS;
	auto bilateralDepthS = std::to_string(mBilateralDepth);


	D3D_SHADER_MACRO macros[] = {
		{ "BLOCKER_SEARCH_WIDTH_SAMPLE_COUNT", blockerSearchSampleCountS.c_str() },
	{ "GAUSSIAN_WEIGHTS_BLOCKER_SEARCH", blockerSearchGaussian.c_str() },
	{ "FILTER_WIDTH_HARDSHADOW", hardShadowFilterSampleCountS.c_str() },
	{ "GAUSSIAN_WEIGHTS_HARDSHADOW", hardShadowFilterGaussian.c_str() },
	{ "BILATERAL_DEPTH", bilateralDepthS.c_str() },
	{ NULL, NULL }
	};


	mPS.SetVariables(L"Data/Shaders/PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_2.tes", SHADER_FILE_PS_OSSSS_GENERATESHADOWDATA_SCREENSPACESEPARABLEBLOCKERSEARCH, target.c_str(), "main_2", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);


	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IGenerateShadowData_2 = pReflector->GetVariableByName("g_IGenerateShadowData_2");
	ID3D11ShaderReflectionVariable* ILight = pReflector->GetVariableByName("iLight");


	mISlot_GenerateShadowData_2 = IGenerateShadowData_2->GetInterfaceSlot(0);
	mISlot_ILight = ILight->GetInterfaceSlot(0);

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);


	auto classLinkage = mPS.GetClassLinkage();


	classLinkage->CreateClassInstance("CGenerateShadowData_2", 0, 0, 0, 0, pSClass_GenerateShadowData_2.ReleaseAndGetAddressOf());


	for (int i = 0; i < MAX_LIGHT_SPOTLIGHT; ++i)
		classLinkage->GetClassInstance("gSpotLights", 0, pSClass_Light_SpotLights[i].ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_DIRECTIONALLIGHT; ++i)
		classLinkage->GetClassInstance("gDirectLights", 0, pSClass_Light_DirectLights[i].ReleaseAndGetAddressOf());


	Set_ClassInstance_GenerateShadowData2();
}

concurrency::task<void> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::SetShader(ID3D11DeviceContext* context )
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::Set_ClassInstance_GenerateShadowData2()
{
	if (mISlot_GenerateShadowData_2 < 100)
		pDynamicClassLinkage[mISlot_GenerateShadowData_2] = pSClass_GenerateShadowData_2.Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_SpotLights[SpotLightIndex].Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_DirectLights[DirectLightIndex].Get();
}
