#include "stdafx.h"
#include "PS_GenerateGBuffers.h"
#include "StringConverter.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"



ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_GenerateGBuffers::pPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_GenerateGBuffers::mNumInterfaces = 0;
UINT ShaderList::PixelShaders::PS_GenerateGBuffers::mISlot_MaterialBase = 0;
UINT ShaderList::PixelShaders::PS_GenerateGBuffers::mISlot_NormalManager = 0;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateGBuffers::pSClass_MaterialBase = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateGBuffers::pSClass_MaterialTextured = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateGBuffers::pSClass_NormalManager_NormalVector = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateGBuffers::pSClass_NormalManager_NormalMap = nullptr;




ID3D11ClassInstance** ShaderList::PixelShaders::PS_GenerateGBuffers::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_GenerateGBuffers::SaveShader()
{
	pPS.SaveShader(StringConverter::StringToWide("Data/Shaders/PS_GenerateGBuffers.tes"));
}

void ShaderList::PixelShaders::PS_GenerateGBuffers::LoadShader()
{
	//pPS.LoadShader(StringConverter::StringToWide("Data/Shaders/PS_GenerateGBuffers.tes"));
}

void ShaderList::PixelShaders::PS_GenerateGBuffers::InitStatics(bool fromHLSLFile)
{

	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	pPS.SetVariables(L"Data/Shaders/PS_GenerateGBuffers.tes", SHADER_FILE_PS_GENERATEGBUFFERS, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, nullptr);

	pPS.LoadShader(fromHLSLFile);

	auto pReflector = pPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IMaterial = pReflector->GetVariableByName("g_IMaterialBase");
	ID3D11ShaderReflectionVariable* INormalManager = pReflector->GetVariableByName("g_INormalManager");

	mISlot_MaterialBase = IMaterial->GetInterfaceSlot(0);
	mISlot_NormalManager = INormalManager->GetInterfaceSlot(0);

	auto classLinkage = pPS.GetClassLinkage();

	/*classLinkage->GetClassInstance("g_cMaterialBase", 0, pSClass_MaterialBase.ReleaseAndGetAddressOf());
	classLinkage->GetClassInstance("g_cMaterialTextured", 0, pSClass_MaterialTextured.ReleaseAndGetAddressOf());
	classLinkage->GetClassInstance("g_CNormalManager_NormalMap", 0, pSClass_NormalManager_NormalMap.ReleaseAndGetAddressOf());
	classLinkage->GetClassInstance("g_CNormalManager_NormalVector", 0, pSClass_NormalManager_NormalVector.ReleaseAndGetAddressOf());*/


	classLinkage->CreateClassInstance("cMaterialBase", 4, 0, 0, 0, pSClass_MaterialBase.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("cMaterialTextured", 4, 0, 0, 0, pSClass_MaterialTextured.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CNormalManager_NormalMap", 4, 0, 0, 0, pSClass_NormalManager_NormalMap.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CNormalManager_NormalVector", 4, 0, 0, 0, pSClass_NormalManager_NormalVector.ReleaseAndGetAddressOf());

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	SetClassInstace_MaterialBase();
	SetClassInstace_NormalManager_NormalVector();
}

concurrency::task<void> ShaderList::PixelShaders::PS_GenerateGBuffers::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstace_MaterialBase()
{
	if (mISlot_MaterialBase < 100)
		pDynamicClassLinkage[mISlot_MaterialBase] = pSClass_MaterialBase.Get();
}

void ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstace_MaterialTextured()
{
	if (mISlot_MaterialBase < 100)
		pDynamicClassLinkage[mISlot_MaterialBase] = pSClass_MaterialTextured.Get();
}

void ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstace_NormalManager_NormalVector()
{
	if (mISlot_NormalManager < 100)
		pDynamicClassLinkage[mISlot_NormalManager] = pSClass_NormalManager_NormalVector.Get();
}

void ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstace_NormalManager_NormalMap()
{
	if (mISlot_NormalManager < 100)
		pDynamicClassLinkage[mISlot_NormalManager] = pSClass_NormalManager_NormalMap.Get();
}

void ShaderList::PixelShaders::PS_GenerateGBuffers::SetShader(ID3D11DeviceContext* context)
{
	pPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_GenerateGBuffers::RecompileShader()
{
	InitStatics(true);
}
