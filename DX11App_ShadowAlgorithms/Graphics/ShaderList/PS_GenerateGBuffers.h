#pragma once

#include "IPixelShader.h"

template<class T> class ShaderObject;



namespace ShaderList
{
	namespace PixelShaders
	{

		class PS_GenerateGBuffers : public IPixelShader_Surface<PS_GenerateGBuffers>
		{

		protected:
			static ShaderObject<ID3D11PixelShader> pPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_MaterialBase;
			static UINT mISlot_NormalManager;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_MaterialBase;
			static ComPtr<ID3D11ClassInstance> pSClass_MaterialTextured;
			static ComPtr<ID3D11ClassInstance> pSClass_NormalManager_NormalVector;
			static ComPtr<ID3D11ClassInstance> pSClass_NormalManager_NormalMap;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void SaveShader();
			static void LoadShader();
			static void InitStatics(bool fromHLSLFile = false);
			static concurrency::task<void> CompileAsync();

			static void SetClassInstace_MaterialBase();
			static void SetClassInstace_MaterialTextured();
			static void SetClassInstace_NormalManager_NormalVector();
			static void SetClassInstace_NormalManager_NormalMap();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

		};

	};
}
