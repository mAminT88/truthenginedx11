#pragma once

#include "IVertexShader.h"

template<class T> class ShaderObject;



namespace ShaderList
{
	namespace VertexShaders {

		class VS_ProcessMesh : public IVertexShader_3DMesh
		{
		protected:
			static ShaderObject<ID3D11VertexShader> pVS_Basic32;
			static ShaderObject<ID3D11VertexShader> pVS_Skinned;


		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();
			//void Init();

			static void SetShader_Basic32(ID3D11DeviceContext* context);
			static void SetShader_Skinned(ID3D11DeviceContext* context);
			static void RecompileShader();
		};


	};
}
