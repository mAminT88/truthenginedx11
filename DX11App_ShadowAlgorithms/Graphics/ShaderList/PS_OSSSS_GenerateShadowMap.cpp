#include "stdafx.h"
#include "PS_OSSSS_GenerateShadowMap.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"
#include "Globals.h"

using namespace Globals;

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowMap::mPS = ShaderObject<ID3D11PixelShader>();

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowMap::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);
	
	mPS.SetVariables(L"Data/Shaders/PS_OSSSS_GenerateShadowMap.tes", SHADER_FILE_PS_OSSSS_GENERATESHADOWMAP, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, nullptr);

	mPS.LoadShader(fromHLSLFile);
}

concurrency::task<void> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowMap::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowMap::SetShader(ID3D11DeviceContext* context )
{
	mPS.SetShader(gContext.Get());
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowMap::RecompileShader()
{
	InitStatics(true);
}
