#include "stdafx.h"
#include "PS_GenerateVarianceShadowMap.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::mPS = ShaderObject<ID3D11PixelShader>();

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::pPSClass_VSMGenerator_LinearDepth = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::pPSClass_VSMGenerator_NormalizedLinearDepth = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::pPSClass_Light_Spots[MAX_LIGHT_SPOTLIGHT];

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::pPSClass_Light_Directs[MAX_LIGHT_DIRECTIONALLIGHT];

int ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::mISlot_VSMGenerator = 0;

UINT ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::mISlot_ILight = 0;

ID3D11ClassInstance** ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);
	
	mPS.SetVariables(L"Data/Shaders/PS_GenerateVarianceShadowMap.tes", SHADER_FILE_PS_GENERATEVARIANCESHADOWMAP, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, nullptr);

	mPS.LoadShader();

	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* vsm_generator = pReflector->GetVariableByName("g_IVSMGenerator");
	ID3D11ShaderReflectionVariable* iLight = pReflector->GetVariableByName("iLight");

	mISlot_VSMGenerator = vsm_generator->GetInterfaceSlot(0);
	mISlot_ILight = iLight->GetInterfaceSlot(0);

	auto classLinkage = mPS.GetClassLinkage();

	classLinkage->CreateClassInstance("CVSMGenerator_LienarDepth", 0, 0, 0, 0, pPSClass_VSMGenerator_LinearDepth.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CVSMGenerator_NormalizedLienarDepth", 0, 0, 0, 0, pPSClass_VSMGenerator_NormalizedLinearDepth.ReleaseAndGetAddressOf());

	for (int Index = 0; Index < MAX_LIGHT_DIRECTIONALLIGHT ; Index++)
	{
		classLinkage->GetClassInstance("gDirectLights", Index, pPSClass_Light_Directs[Index].ReleaseAndGetAddressOf());
	}

	for (int Index = 0; Index < MAX_LIGHT_SPOTLIGHT; Index++)
	{
		classLinkage->GetClassInstance("gSpotLights", Index, pPSClass_Light_Spots[Index].ReleaseAndGetAddressOf());
	}

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	SetClassInstance_VSMGenerator_LinearDepth();
}

concurrency::task<void> ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_VSMGenerator_LinearDepth()
{
	pDynamicClassLinkage[mISlot_VSMGenerator] = pPSClass_VSMGenerator_LinearDepth.Get();
}

void ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_VSMGenerator_NormalizedLinearDepth()
{
	pDynamicClassLinkage[mISlot_VSMGenerator] = pPSClass_VSMGenerator_NormalizedLinearDepth.Get();
}

void ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_Light_Spot(int Index)
{
	pDynamicClassLinkage[mISlot_ILight] = pPSClass_Light_Spots[Index].Get();
}

void ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_Light_Direct(int Index)
{
	pDynamicClassLinkage[mISlot_ILight] = pPSClass_Light_Directs[Index].Get();
}


void ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetShader(ID3D11DeviceContext*  context)
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::RecompileShader()
{
	InitStatics(true);
}
