#include "stdafx.h"
#include "PS_DeferredShading.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"


ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_DeferredShading::pPS = ShaderObject<ID3D11PixelShader>();

int ShaderList::PixelShaders::PS_DeferredShading::mPCFSamplesCount = 5;

int ShaderList::PixelShaders::PS_DeferredShading::mPCSSSamplesCount = 16;

int ShaderList::PixelShaders::PS_DeferredShading::mPCSSBlockerSearchSampleCount = 9;

float ShaderList::PixelShaders::PS_DeferredShading::mGlobalAmbient = 0.1f;

UINT ShaderList::PixelShaders::PS_DeferredShading::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_DeferredShading::mISlot_GBufferManager = 0;

UINT ShaderList::PixelShaders::PS_DeferredShading::mISlot_ShadowManager = 0;

UINT ShaderList::PixelShaders::PS_DeferredShading::mISlot_SAVSM_Manager = 0;

UINT ShaderList::PixelShaders::PS_DeferredShading::mISlot_SSAOManager = 0;

UINT ShaderList::PixelShaders::PS_DeferredShading::mISlot_OutputManager;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_GBufferManager_Base = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_ShadowManager_BasicShadowMap = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_ShadowManager_PCF = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_ShadowManager_PCSS = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_ShadowManager_VSM = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_ShadowManager_SAVSM = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_ShadowManager_MSM = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_ShadowManager_OSSSS = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_SAVSM_Manager_Bilinear = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_SAVSM_Manager_Linear = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_SSAOManager_NoSSAO = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_SSAOManager_Default = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_OutputManager_Backbuffer;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_DeferredShading::pSClass_OutputManager_PreProcess;

ID3D11ClassInstance** ShaderList::PixelShaders::PS_DeferredShading::pDynamicClassLinkage = nullptr;



void ShaderList::PixelShaders::PS_DeferredShading::InitStatics(bool fromHLSLFile)
{

	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	auto globalAmbient = std::to_string(mGlobalAmbient).c_str();

	auto PCFSampleCntS = std::to_string(mPCFSamplesCount);
	auto PCSSSampleCntS = std::to_string(mPCSSSamplesCount);
	auto PCSSPoissonDiskS = std::string("Poisson") + std::to_string(mPCSSSamplesCount);
	auto PCSSBlockerSearchSampleCntS = std::to_string(mPCSSBlockerSearchSampleCount);
	auto PCSSBlockerSearchPoissonDiskS = std::string("Poisson") + std::to_string(mPCSSBlockerSearchSampleCount);

	D3D_SHADER_MACRO macros[] = {
	{"PCF_SAMPLE_COUNT", PCFSampleCntS.c_str()},
	{"PCSS_SAMPLE_COUNT", PCSSSampleCntS.c_str()},
	{"PCSS_POISSONDISK", PCSSPoissonDiskS.c_str()},
	{"BLOCKER_SEARCH_SAMPLE_COUNT", PCSSBlockerSearchSampleCntS.c_str()},
	{"BLOCKER_SEARCH_POISSONDISK", PCSSBlockerSearchPoissonDiskS.c_str()},
	{"GLOBAL_AMBIENT", "float4(0.0f, 0.0f, 0.0f, 0.0f)"},
	{NULL, NULL}
	};


	pPS.SetVariables(L"Data/Shaders/PS_DeferredShading.tes", SHADER_FILE_PS_DEFERREDSHADING, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	pPS.LoadShader(fromHLSLFile);

	auto pReflector = pPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IGBufferManager = pReflector->GetVariableByName("gIGBufferManager");
	ID3D11ShaderReflectionVariable* ILightsArray_Spot = pReflector->GetVariableByName("gILightsArray_Spot");
	ID3D11ShaderReflectionVariable* ILightsArray_Direct = pReflector->GetVariableByName("gILightsArray_Direct");
	ID3D11ShaderReflectionVariable* IShadowManager = pReflector->GetVariableByName("gIShadowManager");

	ID3D11ShaderReflectionVariable* ISAVSMManager = pReflector->GetVariableByName("gISAVSM_Manager");

	ID3D11ShaderReflectionVariable* ISSAOManager = pReflector->GetVariableByName("gISSAOManager");

	ID3D11ShaderReflectionVariable* IOutputManager = pReflector->GetVariableByName("gIOutputManager");


	mISlot_GBufferManager = IGBufferManager->GetInterfaceSlot(0);
	mISlot_ShadowManager = IShadowManager->GetInterfaceSlot(0);

	mISlot_SAVSM_Manager = ISAVSMManager->GetInterfaceSlot(0);

	mISlot_SSAOManager = ISSAOManager->GetInterfaceSlot(0);

	mISlot_OutputManager = IOutputManager->GetInterfaceSlot(0);

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);


	auto classLinkage = pPS.GetClassLinkage();


	classLinkage->CreateClassInstance("CGBufferManager_Base", 0, 0, 0, 0, pSClass_GBufferManager_Base.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CShadowManager_BasicShadowMap", 0, 0, 0, 0, pSClass_ShadowManager_BasicShadowMap.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CShadowManager_PCF", 0, 0, 0, 0, pSClass_ShadowManager_PCF.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CShadowManager_PCSS", 0, 0, 0, 0, pSClass_ShadowManager_PCSS.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CShadowManager_VSM", 0, 0, 0, 0, pSClass_ShadowManager_VSM.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CShadowManager_SAVSM", 0, 0, 0, 0, pSClass_ShadowManager_SAVSM.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CShadowManager_MomentShadowMap", 0, 0, 6, 0, pSClass_ShadowManager_MSM.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CShadowManager_OSSSS", 0, 0, 0, 0, pSClass_ShadowManager_OSSSS.ReleaseAndGetAddressOf());

	classLinkage->CreateClassInstance("CSAVSM_Manager_Bilinear", 0, 0, 0, 0, pSClass_SAVSM_Manager_Bilinear.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CSAVSM_Manager_Linear", 0, 0, 0, 0, pSClass_SAVSM_Manager_Linear.ReleaseAndGetAddressOf());


	classLinkage->CreateClassInstance("SSAOManager_NoSSAO", 0, 0, 0, 0, pSClass_SSAOManager_NoSSAO.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("SSAOManager_Default", 0, 0, 0, 0, pSClass_SSAOManager_Default.ReleaseAndGetAddressOf());

	classLinkage->CreateClassInstance("OutputManager_BackBuffer", 0, 0, 0, 0, pSClass_OutputManager_Backbuffer.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("OutputManager_PreProcess", 0, 0, 0, 0, pSClass_OutputManager_PreProcess.ReleaseAndGetAddressOf());


	SetClassInstace_GBufferManager_Basic();
 	SetClassInstace_ShadowManager_BasicShadowMap();
 	SetClassInstance_SAVSM_Bilinear();
	SetClassInstance_SSAOManager_NoSSAO();
	SetClassInstance_OutputManager_Backbuffer();
}

concurrency::task<void> ShaderList::PixelShaders::PS_DeferredShading::CompileAsync()
{
	return concurrency::task<void>([]() {InitStatics(true); });
}

//void ShaderList::PixelShaders::PS_DeferredShading::Init()
//{
//	
//}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_GBufferManager_Basic()
{
	if (mISlot_GBufferManager < 100) 
		pDynamicClassLinkage[mISlot_GBufferManager] = pSClass_GBufferManager_Base.Get();
}


void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_BasicShadowMap()
{
	if (mISlot_ShadowManager < 100) 
		pDynamicClassLinkage[mISlot_ShadowManager] = pSClass_ShadowManager_BasicShadowMap.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_PCF()
{
	if (mISlot_ShadowManager < 100) 
		pDynamicClassLinkage[mISlot_ShadowManager] = pSClass_ShadowManager_PCF.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_PCSS()
{
	if (mISlot_ShadowManager < 100) 
		pDynamicClassLinkage[mISlot_ShadowManager] = pSClass_ShadowManager_PCSS.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_VSM()
{
	if (mISlot_ShadowManager < 100)
		pDynamicClassLinkage[mISlot_ShadowManager] = pSClass_ShadowManager_VSM.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_SAVSM()
{
	if (mISlot_ShadowManager < 100)
		pDynamicClassLinkage[mISlot_ShadowManager] = pSClass_ShadowManager_SAVSM.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_MSM()
{
	if (mISlot_ShadowManager < 100)
		pDynamicClassLinkage[mISlot_ShadowManager] = pSClass_ShadowManager_MSM.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstace_ShadowManager_OSSSS()
{
	if (mISlot_ShadowManager < 100)
		pDynamicClassLinkage[mISlot_ShadowManager] = pSClass_ShadowManager_OSSSS.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_SAVSM_Bilinear()
{
	if (mISlot_SAVSM_Manager < 100)
		pDynamicClassLinkage[mISlot_SAVSM_Manager] = pSClass_SAVSM_Manager_Bilinear.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_SAVSM_Linear()
{
	if (mISlot_SAVSM_Manager < 100)
		pDynamicClassLinkage[mISlot_SAVSM_Manager] = pSClass_SAVSM_Manager_Linear.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_SSAOManager_NoSSAO()
{
	if (mISlot_SSAOManager < 100)
		pDynamicClassLinkage[mISlot_SSAOManager] = pSClass_SSAOManager_NoSSAO.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_SSAOManager_Default()
{
	if (mISlot_SSAOManager < 100)
		pDynamicClassLinkage[mISlot_SSAOManager] = pSClass_SSAOManager_Default.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_OutputManager_Backbuffer()
{
	if (mISlot_OutputManager < 100)
		pDynamicClassLinkage[mISlot_OutputManager] = pSClass_OutputManager_Backbuffer.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_OutputManager_PreProcess()
{
	if (mISlot_OutputManager < 100)
		pDynamicClassLinkage[mISlot_OutputManager] = pSClass_OutputManager_PreProcess.Get();
}

void ShaderList::PixelShaders::PS_DeferredShading::SetShader(ID3D11DeviceContext* context)
{
	pPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_DeferredShading::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_DeferredShading::Destroy()
{
}

void ShaderList::PixelShaders::PS_DeferredShading::SetGlobalAmbient(float globalAmbient)
{
	mGlobalAmbient = globalAmbient;
}

