#pragma once


enum SURFACE_SHADER_CLASSINSTANCES_NORMAL
{
	SURFACE_SHADER_CLASSINSTANCES_NORMAL_VECTOR,
	SURFACE_SHADER_CLASSINSTANCES_NORMAL_NORMALMAP
};

enum SURFACE_SHADER_CLASSINSTANCES_MATERIAL
{
	SURFACE_SHADER_CLASSINSTANCES_MATERIAL_BASIC,
	SURFACE_SHADER_CLASSINSTANCES_MATERIAL_TEXTURED
};

namespace ShaderList
{

	namespace PixelShaders
	{
		interface IPixelShader
		{
		protected:

		public:
		};

		template <class T>
		interface IPixelShader_Surface : IPixelShader
		{
		protected:

		public:
			static void SetClassInstace_Material(SURFACE_SHADER_CLASSINSTANCES_MATERIAL ClassInstanceType)
			{
				switch (ClassInstanceType)
				{
				case SURFACE_SHADER_CLASSINSTANCES_MATERIAL_BASIC:
					T::SetClassInstace_MaterialBase();
					break;
				case SURFACE_SHADER_CLASSINSTANCES_MATERIAL_TEXTURED:
					T::SetClassInstace_MaterialTextured();
					break;
				}
			}

			static void SetClassInstance_Normal(SURFACE_SHADER_CLASSINSTANCES_NORMAL ClassInstanceType)
			{
				switch (ClassInstanceType)
				{
				case SURFACE_SHADER_CLASSINSTANCES_NORMAL_VECTOR:
					T::SetClassInstace_NormalManager_NormalVector();
					break;
				case SURFACE_SHADER_CLASSINSTANCES_NORMAL_NORMALMAP:
					T::SetClassInstace_NormalManager_NormalMap();
					break;
				}
			}


			 /*static void SetClassInstace_MaterialBase() {};
			static  void SetClassInstace_MaterialTextured() {};
			static void SetClassInstace_NormalManager_NormalVector() {};
			static void SetClassInstace_NormalManager_NormalMap() {};*/
		};
	}

}