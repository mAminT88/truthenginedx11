#pragma once

#include "IPixelShader.h"

template<class T> class ShaderObject;



namespace ShaderList
{
	namespace PixelShaders
	{

		class PS_PostFX_FinalPass : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;

			static UINT mISlot_IToneMapping;
			static UINT mISlot_IBloom;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_ToneMapping_Reinhard;
			static ComPtr<ID3D11ClassInstance> pSClass_ToneMapping_ACES;

			static ComPtr<ID3D11ClassInstance> pSClass_Bloom_None;
			static ComPtr<ID3D11ClassInstance> pSClass_Bloom_Default;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetClassInstance_ToneMapping_Reinhard();
			static void SetClassInstance_ToneMapping_ACES();

			static void SetClassInstance_Bloom_Default();
			static void SetClassInstance_Bloom_None();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

		};

	}
}

