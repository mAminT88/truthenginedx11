#include "stdafx.h"
#include "PS_DownUpSampling.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_DownUpSampling::mPS;

void ShaderList::PixelShaders::PS_DownUpSampling::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	D3D_SHADER_MACRO macros[] =
	{
		{NULL,NULL}
	};

	mPS.SetVariables(L"Data/Shaders/PS_DownUpSampling.tes", SHADER_FILE_PS_DOWNUPSAMPLING, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);

}

concurrency::task<void> ShaderList::PixelShaders::PS_DownUpSampling::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_DownUpSampling::SetShader(ID3D11DeviceContext* context)
{
	mPS.SetShader(context, nullptr, 0);
}

void ShaderList::PixelShaders::PS_DownUpSampling::RecompileShader()
{
	InitStatics(true);
}
