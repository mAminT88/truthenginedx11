#pragma once

#include "IVertexShader.h"

template<class T> class ShaderObject;

namespace ShaderList
{
	namespace VertexShaders
	{
		class VS_Render2D : public IVertexShader
		{
		protected:
			static ShaderObject<ID3D11VertexShader> pVS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_IPosition;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_Position_WVP;
			static ComPtr<ID3D11ClassInstance> pSClass_Position_noWVP;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;


		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();
			//void Init();

			static void SetClassInstace_Position_WVP();
			static void SetClassInstace_Position_noWVP();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();
		};
	};
}
