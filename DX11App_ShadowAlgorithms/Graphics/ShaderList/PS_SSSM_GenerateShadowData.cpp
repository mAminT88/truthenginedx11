#include "stdafx.h"
#include "PS_SSSM_GenerateShadowData.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"




//////////////////////////////////////////////////////////////////////////
// Separable Soft Shadow Map - Pass 0
//////////////////////////////////////////////////////////////////////////

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::mPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::mISlot_ILight = 0;

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::ALPHA = "0.01";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::BETA = "0.02";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::BLOCKER_SEARCH_SAMPLE_COUNT = "121.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::BLOCKER_SEARCH_HALF_SAMPLE_COUNT = "12.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT = "11.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT = "5.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::HARDSHADOW_FILTER_GAUSSIAN_WEIGHT = "Gaussian11";

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];

ID3D11ClassInstance** ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	D3D_SHADER_MACRO macros[] = {
		{ "ALPHA", ALPHA.c_str() },
		{ "BETA", BETA.c_str() },
		{ "BLOCKER_SEARCH_SAMPLE_COUNT", BLOCKER_SEARCH_SAMPLE_COUNT.c_str() },
		{ "BLOCKER_SEARCH_HALF_SAMPLE_COUNT", BLOCKER_SEARCH_HALF_SAMPLE_COUNT.c_str() },
		{ "HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT", HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT.c_str() },
		{ "HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT", HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT.c_str() },
		{ "HARDSHADOW_FILTER_GAUSSIAN_WEIGHT", HARDSHADOW_FILTER_GAUSSIAN_WEIGHT.c_str() },
	{ NULL, NULL }
	};


	mPS.SetVariables(L"Data/Shaders/PS_SSSM_GenerateShadowData_0.tes", SHADER_FILE_PS_SSSM_GENERATESHADOWDATA, target.c_str(), "main_0", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);

	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* ILight = pReflector->GetVariableByName("iLight");

	mISlot_ILight = ILight->GetInterfaceSlot(0);

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	auto classLinkage = mPS.GetClassLinkage();

	for (int i = 0; i < MAX_LIGHT_SPOTLIGHT; ++i)
		classLinkage->GetClassInstance("gSpotLights", 0, pSClass_Light_SpotLights[i].ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_DIRECTIONALLIGHT; ++i)
		classLinkage->GetClassInstance("gDirectLights", 0, pSClass_Light_DirectLights[i].ReleaseAndGetAddressOf());

}

concurrency::task<void> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::SetShader(ID3D11DeviceContext* context )
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::SetStaticValues(std::string BlockerSearchSampleCount, std::string BlockerSearchHalfSampleCount)
{
	BLOCKER_SEARCH_SAMPLE_COUNT = BlockerSearchSampleCount;
	BLOCKER_SEARCH_HALF_SAMPLE_COUNT = BlockerSearchHalfSampleCount;
}


void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_SpotLights[SpotLightIndex].Get();
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_DirectLights[DirectLightIndex].Get();
}

//////////////////////////////////////////////////////////////////////////
// Separable Soft Shadow Map - Pass 1
//////////////////////////////////////////////////////////////////////////


ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::mPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::mISlot_ILight = 0;

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::ALPHA = "0.01";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::BETA = "0.02";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::BLOCKER_SEARCH_SAMPLE_COUNT = "9.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::BLOCKER_SEARCH_HALF_SAMPLE_COUNT = "4.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT = "11.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT = "5.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::HARDSHADOW_FILTER_GAUSSIAN_WEIGHT = "Gaussian11";

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];

ID3D11ClassInstance** ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	D3D_SHADER_MACRO macros[] = {
		{ "ALPHA", ALPHA.c_str() },
		{ "BETA", BETA.c_str() },
		{ "BLOCKER_SEARCH_SAMPLE_COUNT", BLOCKER_SEARCH_SAMPLE_COUNT.c_str() },
		{ "BLOCKER_SEARCH_HALF_SAMPLE_COUNT", BLOCKER_SEARCH_HALF_SAMPLE_COUNT.c_str() },
		{ "HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT", HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT.c_str() },
		{ "HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT", HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT.c_str() },
		{ "HARDSHADOW_FILTER_GAUSSIAN_WEIGHT", HARDSHADOW_FILTER_GAUSSIAN_WEIGHT.c_str() },
	{ NULL, NULL }
	};


	mPS.SetVariables(L"Data/Shaders/PS_SSSM_GenerateShadowData_1.tes", SHADER_FILE_PS_SSSM_GENERATESHADOWDATA, target.c_str(), "main_1", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);

	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* ILight = pReflector->GetVariableByName("iLight");

	mISlot_ILight = ILight->GetInterfaceSlot(0);

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	auto classLinkage = mPS.GetClassLinkage();

	for (int i = 0; i < MAX_LIGHT_SPOTLIGHT; ++i)
		classLinkage->GetClassInstance("gSpotLights", 0, pSClass_Light_SpotLights[i].ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_DIRECTIONALLIGHT; ++i)
		classLinkage->GetClassInstance("gDirectLights", 0, pSClass_Light_DirectLights[i].ReleaseAndGetAddressOf());
}

concurrency::task<void> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::SetShader(ID3D11DeviceContext* context )
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::SetStaticValues(std::string BlockerSearchSampleCount
	, std::string BlockerSearchHalfSampleCount
	, std::string HardShadowFilterWidthSampleCount
	, std::string HardShadowFilterRadiusSampleCount
	, std::string HardShadowFilterGaussianWeights)
{
	BLOCKER_SEARCH_SAMPLE_COUNT = BlockerSearchSampleCount;
	BLOCKER_SEARCH_HALF_SAMPLE_COUNT = BlockerSearchHalfSampleCount;
	HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT = HardShadowFilterWidthSampleCount;
	HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT = HardShadowFilterRadiusSampleCount;
	HARDSHADOW_FILTER_GAUSSIAN_WEIGHT = HardShadowFilterGaussianWeights;
}



void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_SpotLights[SpotLightIndex].Get();
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_DirectLights[DirectLightIndex].Get();
}

//////////////////////////////////////////////////////////////////////////
// Separable Soft Shadow Map - Pass 2
//////////////////////////////////////////////////////////////////////////


ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::mPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::mISlot_ILight = 0;

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::ALPHA = "0.01";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::BETA = "0.02";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::BLOCKER_SEARCH_SAMPLE_COUNT = "9.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::BLOCKER_SEARCH_HALF_SAMPLE_COUNT = "4.0";

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT = "11.0";;

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT = "5.0";;

std::string ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::HARDSHADOW_FILTER_GAUSSIAN_WEIGHT = "Gaussian11";

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];

ID3D11ClassInstance** ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	D3D_SHADER_MACRO macros[] = {
		{ "ALPHA", ALPHA.c_str() },
		{ "BETA", BETA.c_str() },
		{ "BLOCKER_SEARCH_SAMPLE_COUNT", BLOCKER_SEARCH_SAMPLE_COUNT.c_str() },
		{ "BLOCKER_SEARCH_HALF_SAMPLE_COUNT", BLOCKER_SEARCH_HALF_SAMPLE_COUNT.c_str() },
		{ "HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT", HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT.c_str() },
		{ "HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT", HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT.c_str() },
		{ "HARDSHADOW_FILTER_GAUSSIAN_WEIGHT", HARDSHADOW_FILTER_GAUSSIAN_WEIGHT.c_str() },
	{ NULL, NULL }
	};


	mPS.SetVariables(L"Data/Shaders/PS_SSSM_GenerateShadowData_2.tes", SHADER_FILE_PS_SSSM_GENERATESHADOWDATA, target.c_str(), "main_2", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);

	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* ILight = pReflector->GetVariableByName("iLight");

	mISlot_ILight = ILight->GetInterfaceSlot(0);

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	auto classLinkage = mPS.GetClassLinkage();

	for (int i = 0; i < MAX_LIGHT_SPOTLIGHT; ++i)
		classLinkage->GetClassInstance("gSpotLights", 0, pSClass_Light_SpotLights[i].ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_DIRECTIONALLIGHT; ++i)
		classLinkage->GetClassInstance("gDirectLights", 0, pSClass_Light_DirectLights[i].ReleaseAndGetAddressOf());
}

concurrency::task<void> ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::SetShader(ID3D11DeviceContext* context )
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::SetStaticValues(std::string HardShadowFilterWidthSampleCount, std::string HardShadowFilterRadiusSampleCount, std::string HardShadowFilterGaussianWeights)
{
	HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT = HardShadowFilterWidthSampleCount;
	HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT = HardShadowFilterRadiusSampleCount;
	HARDSHADOW_FILTER_GAUSSIAN_WEIGHT = HardShadowFilterGaussianWeights;
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_SpotLights[SpotLightIndex].Get();
}

void ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_DirectLights[DirectLightIndex].Get();
}
