#include "stdafx.h"
#include "PS_SSAO.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_SSAO::pPS;

UINT ShaderList::PixelShaders::PS_SSAO::mNumInterfaces;

UINT ShaderList::PixelShaders::PS_SSAO::mISlot_SSAO;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SSAO::pSClass_SSAO_Default = nullptr;

ID3D11ClassInstance** ShaderList::PixelShaders::PS_SSAO::pDynamicClassLinkage;

void ShaderList::PixelShaders::PS_SSAO::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);


	pPS.SetVariables(L"Data/Shaders/PS_SSAO.tes", SHADER_FILE_PS_SSAO, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, nullptr);

	pPS.LoadShader(fromHLSLFile);

	auto pReflector = pPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* ISSAO = pReflector->GetVariableByName("gISSAO");

	mISlot_SSAO = ISSAO->GetInterfaceSlot(0);

	auto classLinkage = pPS.GetClassLinkage();

	classLinkage->CreateClassInstance("SSAO_Default", 4, 0, 0, 0, pSClass_SSAO_Default.ReleaseAndGetAddressOf());

	pDynamicClassLinkage = (ID3D11ClassInstance * *)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	SetClassInstance_SSAOManager_Default();
}

concurrency::task<void> ShaderList::PixelShaders::PS_SSAO::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_SSAO::SetClassInstance_SSAOManager_Default()
{
	pDynamicClassLinkage[mISlot_SSAO] = pSClass_SSAO_Default.Get();
}

void ShaderList::PixelShaders::PS_SSAO::SetShader(ID3D11DeviceContext* context )
{
	pPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_SSAO::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_SSAO::Destroy()
{
	pPS.Release();
}
