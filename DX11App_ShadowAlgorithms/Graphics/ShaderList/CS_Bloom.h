#pragma once

#include "IComputeShader.h"

template<class T> class ShaderObject;

namespace ShaderList
{
	namespace ComputeShaders
	{
		class CS_Bloom : public IComputeShader
		{
		protected:
			static ShaderObject<ID3D11ComputeShader> mCS_Bloom;

			//Interfaces Slot
			//static UINT mNumInterfaces;

			static UINT mThreadGroupSizeX;

			//Shader Class Instances
			//static ComPtr<ID3D11ClassInstance> pSClass_ApplyPrefixSum_Horz;

			//ClassInstaceArray
			//static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

			static UINT GetThreadGroupSizeX() noexcept;

			static void SetStaticValue(UINT ThreadGroupSizeX) noexcept;

		};
	}
}
