#include "stdafx.h"
#include "CS_GaussianBlur.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11ComputeShader> ShaderList::ComputeShaders::CS_GaussianBlur::mCS_Blur;

UINT ShaderList::ComputeShaders::CS_GaussianBlur::mNumInterfaces;

UINT ShaderList::ComputeShaders::CS_GaussianBlur::mISlot_Filter;

UINT ShaderList::ComputeShaders::CS_GaussianBlur::mThreadGroupSizeX = 128;

UINT ShaderList::ComputeShaders::CS_GaussianBlur::mKernelHalf = 6;

ComPtr<ID3D11ClassInstance> ShaderList::ComputeShaders::CS_GaussianBlur::pSClass_ApplyFilter_Horz;

ComPtr<ID3D11ClassInstance> ShaderList::ComputeShaders::CS_GaussianBlur::pSClass_ApplyFilter_Vert;

ID3D11ClassInstance** ShaderList::ComputeShaders::CS_GaussianBlur::pDynamicClassLinkage;

void ShaderList::ComputeShaders::CS_GaussianBlur::InitStatics(bool fromHLSLFile)
{
	std::string target = "cs_";
	target.append(SHADER_TARGET_VERSION);

	auto value_1 = std::to_string(mThreadGroupSizeX);
	auto value_2 = std::to_string(mKernelHalf);
	auto value_3 = "Gaussian"+ std::to_string((UINT)(mKernelHalf*2.0 + 1));

	D3D_SHADER_MACRO macros[] =
	{
		{ "GROUP_SIZE_X", value_1.c_str() },
		{ "KERNEL_HALF", value_2.c_str() },
		{ "GAUSSIAN_WEIGHTS", value_3.c_str() },
	{ NULL,NULL }
	};


	mCS_Blur.SetVariables(L"Data/Shaders/CS_GaussainBlur.tes", SHADER_FILE_CS_GAUSSIANBLUR, target.c_str(), "GaussianBlur", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mCS_Blur.LoadShader(fromHLSLFile);

	auto pReflector = mCS_Blur.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IBlur = pReflector->GetVariableByName("gIFilter");

	mISlot_Filter = IBlur->GetInterfaceSlot(0);

	auto classLinkage = mCS_Blur.GetClassLinkage();

	classLinkage->CreateClassInstance("Filter_Horizontal", 4, 0, 0, 0, pSClass_ApplyFilter_Horz.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("Filter_Vertical", 4, 0, 0, 0, pSClass_ApplyFilter_Vert.ReleaseAndGetAddressOf());

	pDynamicClassLinkage = (ID3D11ClassInstance * *)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	SetClassInstance_Filter_Horz();
}

concurrency::task<void> ShaderList::ComputeShaders::CS_GaussianBlur::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::ComputeShaders::CS_GaussianBlur::SetShader(ID3D11DeviceContext* context)
{
	mCS_Blur.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::ComputeShaders::CS_GaussianBlur::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::ComputeShaders::CS_GaussianBlur::SetClassInstance_Filter_Horz()
{
	pDynamicClassLinkage[mISlot_Filter] = pSClass_ApplyFilter_Horz.Get();
}

void ShaderList::ComputeShaders::CS_GaussianBlur::SetClassInstance_Filter_Vert()
{
	pDynamicClassLinkage[mISlot_Filter] = pSClass_ApplyFilter_Vert.Get();

}

UINT ShaderList::ComputeShaders::CS_GaussianBlur::GetThreadGroupSizeX() noexcept
{
	return mThreadGroupSizeX;
}

UINT ShaderList::ComputeShaders::CS_GaussianBlur::GetHalfKernel() noexcept
{
	return mKernelHalf;
}

void ShaderList::ComputeShaders::CS_GaussianBlur::SetStaticValue(UINT ThreadGroupSizeX, UINT HalfKernel) noexcept
{
	mThreadGroupSizeX = ThreadGroupSizeX;
	mKernelHalf = HalfKernel;
}
