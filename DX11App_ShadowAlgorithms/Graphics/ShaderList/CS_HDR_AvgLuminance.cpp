#include "stdafx.h"
#include "CS_HDR_AvgLuminance.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11ComputeShader> ShaderList::ComputeShaders::CS_HDR_AvgLuminance::mCS_FirstPass;

ShaderObject<ID3D11ComputeShader> ShaderList::ComputeShaders::CS_HDR_AvgLuminance::mCS_SecondPass;

UINT ShaderList::ComputeShaders::CS_HDR_AvgLuminance::mThreadGroupSizeX_FirstPass = 1024;

UINT ShaderList::ComputeShaders::CS_HDR_AvgLuminance::mThreadGroupSizeX_SecondPass = 64;

UINT ShaderList::ComputeShaders::CS_HDR_AvgLuminance::mThreadGroupSizeY = 1;

void ShaderList::ComputeShaders::CS_HDR_AvgLuminance::InitStatics(bool fromHLSLFile)
{
	std::string target = "cs_";
	target.append(SHADER_TARGET_VERSION);

	auto value_1 = std::to_string(mThreadGroupSizeX_FirstPass);
	auto value_2 = std::to_string(mThreadGroupSizeX_SecondPass);
	auto value_3 = std::to_string(mThreadGroupSizeY);

	D3D_SHADER_MACRO macros[] =
	{
		{ "FirstPass_ThreadGroupSize_X", value_1.c_str() },
		{ "SecondPass_ThreadGroupSize_X", value_2.c_str() },
	{ "ThreadGroupSize_Y", value_3.c_str() },
	{ NULL,NULL }
	};


	mCS_FirstPass.SetVariables(L"Data/Shaders/CS_FirstPass.tes", SHADER_FILE_CS_HDR_AvgLuminance, target.c_str(), "DownScaleFirstPass", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mCS_FirstPass.LoadShader(fromHLSLFile);

	mCS_SecondPass.SetVariables(L"Data/Shaders/CS_SecondPass.tes", SHADER_FILE_CS_HDR_AvgLuminance, target.c_str(), "DownScaleSecondPass", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mCS_SecondPass.LoadShader(fromHLSLFile);
}

concurrency::task<void> ShaderList::ComputeShaders::CS_HDR_AvgLuminance::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::ComputeShaders::CS_HDR_AvgLuminance::SetShader_FirstPass(ID3D11DeviceContext* context)
{
	mCS_FirstPass.SetShader(context, nullptr, 0);
}

void ShaderList::ComputeShaders::CS_HDR_AvgLuminance::SetShader_SecondPass(ID3D11DeviceContext* context)
{
	mCS_SecondPass.SetShader(context, nullptr, 0);
}

void ShaderList::ComputeShaders::CS_HDR_AvgLuminance::RecompileShader()
{
	InitStatics(true);
}

UINT ShaderList::ComputeShaders::CS_HDR_AvgLuminance::GetThreadGroupSizeX_FirstPass() noexcept
{
	return mThreadGroupSizeX_FirstPass;
}

UINT ShaderList::ComputeShaders::CS_HDR_AvgLuminance::GetThreadGroupSizeX_SecondPass() noexcept
{
	return mThreadGroupSizeX_SecondPass;
}

UINT ShaderList::ComputeShaders::CS_HDR_AvgLuminance::GetThreadGroupSizeY() noexcept
{
	return mThreadGroupSizeY;
}

void ShaderList::ComputeShaders::CS_HDR_AvgLuminance::SetStaticValue(UINT ThreadGroupSizeX_FirstPass, UINT ThreadGroupSizeX_SecondPass, UINT ThreadGroupSizeY) noexcept
{
	mThreadGroupSizeX_FirstPass = ThreadGroupSizeX_FirstPass;
	mThreadGroupSizeX_SecondPass = ThreadGroupSizeX_SecondPass;
	mThreadGroupSizeY = ThreadGroupSizeY;
}
