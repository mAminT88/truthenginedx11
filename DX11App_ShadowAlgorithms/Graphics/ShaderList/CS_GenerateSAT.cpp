#include "stdafx.h"
#include "CS_GenerateSAT.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

//
// Generate Summed Area Table Horizontal Step
//

ShaderObject<ID3D11ComputeShader> ShaderList::ComputeShaders::CS_GenerateSAT_Horz::mCS = ShaderObject<ID3D11ComputeShader>();

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Horz::mNumInterfaces = 0;

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Horz::mISlot_ApplyPrefixSum = 0;

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Horz::mThreadGroupSizeX = 1;

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Horz::mThreadGroupSizeY = 64;

ComPtr<ID3D11ClassInstance> ShaderList::ComputeShaders::CS_GenerateSAT_Horz::pSClass_ApplyPrefixSum_Horz = nullptr;

ID3D11ClassInstance** ShaderList::ComputeShaders::CS_GenerateSAT_Horz::pDynamicClassLinkage = nullptr;

void ShaderList::ComputeShaders::CS_GenerateSAT_Horz::InitStatics(bool fromHLSLFile)
{
	std::string target = "cs_";
	target.append(SHADER_TARGET_VERSION);

	auto value_1 = std::to_string(mThreadGroupSizeX);
	auto value_2 = std::to_string(mThreadGroupSizeY);

	D3D_SHADER_MACRO macros[] =
	{
		{ "ThreadGroupSize_X", value_1.c_str() },
	{ "ThreadGroupSize_Y", value_2.c_str() },
	{ NULL,NULL }
	};
	
	mCS.SetVariables(L"Data/Shaders/CS_GenerateSAT_Horz.tes", SHADER_FILE_CS_OSSSS_GENERATESAT, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mCS.LoadShader(fromHLSLFile);

	auto pReflector = mCS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();
	
	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);
	
	ID3D11ShaderReflectionVariable* IApplyPrefixSum = pReflector->GetVariableByName("gIApplyPrefixSum");

	mISlot_ApplyPrefixSum = IApplyPrefixSum->GetInterfaceSlot(0);

	auto classLinkage = mCS.GetClassLinkage();

	classLinkage->CreateClassInstance("CApplyPrefixSum_Horizontal", 4, 0, 0, 0, pSClass_ApplyPrefixSum_Horz.ReleaseAndGetAddressOf());

	SetClassInstace_ApplyPrefixSum_Horz();
}

concurrency::task<void> ShaderList::ComputeShaders::CS_GenerateSAT_Horz::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::ComputeShaders::CS_GenerateSAT_Horz::SetClassInstace_ApplyPrefixSum_Horz()
{
	pDynamicClassLinkage[mISlot_ApplyPrefixSum] = pSClass_ApplyPrefixSum_Horz.Get();
}


void ShaderList::ComputeShaders::CS_GenerateSAT_Horz::SetShader(ID3D11DeviceContext* context)
{
	mCS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::ComputeShaders::CS_GenerateSAT_Horz::RecompileShader()
{
	InitStatics(true);
}

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Horz::GetThreadGroupSizeX()
{
	return mThreadGroupSizeX;
}

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Horz::GetThreadGroupSizeY()
{
	return mThreadGroupSizeY;
}

void ShaderList::ComputeShaders::CS_GenerateSAT_Horz::SetStaticValue(UINT ThreadGroupSizeX, UINT ThreadGroupSizeY)
{
	mThreadGroupSizeX = ThreadGroupSizeX;
	mThreadGroupSizeY = ThreadGroupSizeY;
}



//
// Generate Summed Area Table Vertical Step
//

ShaderObject<ID3D11ComputeShader> ShaderList::ComputeShaders::CS_GenerateSAT_Vert::mCS = ShaderObject<ID3D11ComputeShader>();

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Vert::mNumInterfaces = 0;

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Vert::mISlot_ApplyPrefixSum = 0;

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Vert::mThreadGroupSizeX = 64;

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Vert::mThreadGroupSizeY = 1;

ComPtr<ID3D11ClassInstance> ShaderList::ComputeShaders::CS_GenerateSAT_Vert::pSClass_ApplyPrefixSum_Vert = nullptr;

ID3D11ClassInstance** ShaderList::ComputeShaders::CS_GenerateSAT_Vert::pDynamicClassLinkage = nullptr;

void ShaderList::ComputeShaders::CS_GenerateSAT_Vert::InitStatics(bool fromHLSLFile)
{
	std::string target = "cs_";
	target.append(SHADER_TARGET_VERSION);

	auto value_1 = std::to_string(mThreadGroupSizeX);
	auto value_2 = std::to_string(mThreadGroupSizeY);

	D3D_SHADER_MACRO macros[] =
	{
		{ "ThreadGroupSize_X", value_1.c_str() },
	{ "ThreadGroupSize_Y", value_2.c_str() },
	{ NULL,NULL }
	};

	mCS.SetVariables(L"Data/Shaders/CS_GenerateSAT_Vert.tes", SHADER_FILE_CS_OSSSS_GENERATESAT, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mCS.LoadShader(fromHLSLFile);

	auto pReflector = mCS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();


	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);


	ID3D11ShaderReflectionVariable* IApplyPrefixSum = pReflector->GetVariableByName("gIApplyPrefixSum");

	mISlot_ApplyPrefixSum = IApplyPrefixSum->GetInterfaceSlot(0);

	auto classLinkage = mCS.GetClassLinkage();

	classLinkage->CreateClassInstance("CApplyPrefixSum_Vertical", 4, 0, 0, 0, pSClass_ApplyPrefixSum_Vert.ReleaseAndGetAddressOf());

	SetClassInstace_ApplyPrefixSum_Vert();
}

concurrency::task<void> ShaderList::ComputeShaders::CS_GenerateSAT_Vert::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::ComputeShaders::CS_GenerateSAT_Vert::SetClassInstace_ApplyPrefixSum_Vert()
{
	pDynamicClassLinkage[mISlot_ApplyPrefixSum] = pSClass_ApplyPrefixSum_Vert.Get();

}


void ShaderList::ComputeShaders::CS_GenerateSAT_Vert::SetShader(ID3D11DeviceContext* context)
{
	mCS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::ComputeShaders::CS_GenerateSAT_Vert::RecompileShader()
{
	InitStatics();
}

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Vert::GetThreadGroupSizeX()
{
	return mThreadGroupSizeX;
}

UINT ShaderList::ComputeShaders::CS_GenerateSAT_Vert::GetThreadGroupSizeY()
{
	return mThreadGroupSizeY;
}

void ShaderList::ComputeShaders::CS_GenerateSAT_Vert::SetStaticValue(UINT ThreadGroupSizeX, UINT ThreadGroupSizeY)
{
	mThreadGroupSizeX = ThreadGroupSizeX;
	mThreadGroupSizeY = ThreadGroupSizeY;
}

