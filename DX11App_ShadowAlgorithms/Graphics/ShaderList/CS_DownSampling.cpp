#include "stdafx.h"
#include "CS_DownSampling.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11ComputeShader> ShaderList::ComputeShaders::CS_DownSampling::mCS;

UINT ShaderList::ComputeShaders::CS_DownSampling::mThreadGroupSizeX = 1024;

void ShaderList::ComputeShaders::CS_DownSampling::InitStatics(bool fromHLSLFile)
{
	std::string target = "cs_";
	target.append(SHADER_TARGET_VERSION);

	auto value_1 = std::to_string(mThreadGroupSizeX);

	D3D_SHADER_MACRO macros[] =
	{
		{ "GroupSize_X", value_1.c_str() },
	{ NULL,NULL }
	};


	mCS.SetVariables(L"Data/Shaders/CS_DownSampling.tes", SHADER_FILE_CS_DOWNSAMPLING, target.c_str(), "DownSampling4X4", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mCS.LoadShader(fromHLSLFile);

}

concurrency::task<void> ShaderList::ComputeShaders::CS_DownSampling::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::ComputeShaders::CS_DownSampling::SetShader(ID3D11DeviceContext* context)
{
	mCS.SetShader(context, nullptr, 0);
}

void ShaderList::ComputeShaders::CS_DownSampling::RecompileShader()
{
	InitStatics(true);
}

UINT ShaderList::ComputeShaders::CS_DownSampling::GetThreadGroupSizeX() noexcept
{
	return mThreadGroupSizeX;
}

void ShaderList::ComputeShaders::CS_DownSampling::SetStaticValue(UINT ThreadGroupSizeX) noexcept
{
	mThreadGroupSizeX = ThreadGroupSizeX;
}
