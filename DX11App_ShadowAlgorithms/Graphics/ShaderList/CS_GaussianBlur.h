#pragma once

#include "IComputeShader.h"

template<class T> class ShaderObject;

namespace ShaderList
{
	namespace ComputeShaders
	{
		class CS_GaussianBlur : public IComputeShader
		{
		protected:
			static ShaderObject<ID3D11ComputeShader> mCS_Blur;

			//Interfaces Slot
			static UINT mNumInterfaces;

			static UINT mISlot_Filter;

			static UINT mThreadGroupSizeX;
			static UINT mKernelHalf;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_ApplyFilter_Horz;
			static ComPtr<ID3D11ClassInstance> pSClass_ApplyFilter_Vert;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

			static void SetClassInstance_Filter_Horz();
			static void SetClassInstance_Filter_Vert();

			static UINT GetThreadGroupSizeX() noexcept;
			static UINT GetHalfKernel() noexcept;

			static void SetStaticValue(UINT ThreadGroupSizeX, UINT HalfKernel) noexcept;
		};
	}
}
