#include "stdafx.h"
#include "PS_OSSSS_GenerateShadowData_Pass0.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mISlot_GenerateShadowData_0 = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mISlot_ILight = 0;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::pSClass_GenerateShadowData_0 = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::pSClass_GenerateShadowData_0_UsePreFilteredAvgBlockers = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];

ID3D11ClassInstance** ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::pDynamicClassLinkage = nullptr;

float ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mBilateralDepth = 20.0f;
float ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mFilterWidth = 11.0f;
float ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mBlockerSearchSampleCount= 9.0f;


void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	std::string blockerSearchSampleCntS = std::to_string(mBlockerSearchSampleCount);
	std::string blockerSearchPoissonDiskS = std::string("Poisson") + std::to_string((int)mBlockerSearchSampleCount);
	std::string bilateralDepthS = std::to_string(mBilateralDepth);
	std::string filterWidthS = std::to_string(mFilterWidth);
	std::string GaussianS = std::string("Gaussian") + std::to_string((int)mFilterWidth);

	D3D_SHADER_MACRO macros[] = {
		{ "BLOCKER_SEARCH_SAMPLE_COUNT", blockerSearchSampleCntS.c_str() },
	{ "BLOCKER_SEARCH_POISSON", blockerSearchPoissonDiskS.c_str() },
	{ "FILTER_WIDTH_HARDSHADOW", filterWidthS.c_str() },
	{ "GAUSSIAN_WEIGHTS_HARDSHADOW", GaussianS.c_str() },
	{ "BILATERAL_DEPTH", bilateralDepthS.c_str() },
	{ NULL, NULL }
	};


	mPS.SetVariables(L"Data/Shaders/PS_OSSSS_GenerateShadowData_0.tes", SHADER_FILE_PS_OSSSS_GENERATESHADOWDATA, target.c_str(), "main_0", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);

	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* ILight = pReflector->GetVariableByName("iLight");
	ID3D11ShaderReflectionVariable* IGenerateShadowData_0 = pReflector->GetVariableByName("g_IGenerateShadowData_0");

	mISlot_GenerateShadowData_0 = IGenerateShadowData_0->GetInterfaceSlot(0);
	mISlot_ILight = ILight->GetInterfaceSlot(0);

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);


	auto classLinkage = mPS.GetClassLinkage();


	classLinkage->CreateClassInstance("CGenerateShadowData_0", 0, 0, 0, 0, pSClass_GenerateShadowData_0.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CGenerateShadowData_0_UsePreFilterAvgBlocker", 0, 0, 0, 0, pSClass_GenerateShadowData_0_UsePreFilteredAvgBlockers.ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_SPOTLIGHT; ++i)
		classLinkage->GetClassInstance("gSpotLights", i, pSClass_Light_SpotLights[i].ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_DIRECTIONALLIGHT; ++i)
		classLinkage->GetClassInstance("gDirectLights", i, pSClass_Light_DirectLights[i].ReleaseAndGetAddressOf());


	pDynamicClassLinkage[mISlot_GenerateShadowData_0] = pSClass_GenerateShadowData_0.Get();
}

concurrency::task<void> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::SetShader(ID3D11DeviceContext* context)
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_GenerateShadowData_0_Normal()
{
	pDynamicClassLinkage[mISlot_GenerateShadowData_0] = pSClass_GenerateShadowData_0.Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_GenerateShadowData_0_UsePreFilteredAvgBlockers()
{
	pDynamicClassLinkage[mISlot_GenerateShadowData_0] = pSClass_GenerateShadowData_0_UsePreFilteredAvgBlockers.Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_SpotLights[SpotLightIndex].Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_DirectLights[DirectLightIndex].Get();
}
