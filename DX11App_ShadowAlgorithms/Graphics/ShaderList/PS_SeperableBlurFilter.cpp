#include "stdafx.h"
#include "PS_SeperableBlurFilter.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_SeperableBlurFilter::pPS_GaussFilter = ShaderObject<ID3D11PixelShader>();
ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_SeperableBlurFilter::pPS_BoxFilter = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_SeperableBlurFilter::mNumInterfaces_Gauss = 0;
UINT ShaderList::PixelShaders::PS_SeperableBlurFilter::mNumInterfaces_Box = 0;

UINT ShaderList::PixelShaders::PS_SeperableBlurFilter::mISlot_GaussFilterManager = 0;
UINT ShaderList::PixelShaders::PS_SeperableBlurFilter::mISlot_BoxFilterManager = 0;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SeperableBlurFilter::pSClass_GaussFilterManager_Horizontally = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SeperableBlurFilter::pSClass_GaussFilterManager_Vertically = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SeperableBlurFilter::pSClass_BoxFilterManager_Horizontally = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_SeperableBlurFilter::pSClass_BoxFilterManager_Vertically = nullptr;

ID3D11ClassInstance** ShaderList::PixelShaders::PS_SeperableBlurFilter::pDynamicClassLinkage_Gauss = nullptr;

ID3D11ClassInstance** ShaderList::PixelShaders::PS_SeperableBlurFilter::pDynamicClassLinkage_Box = nullptr;

float ShaderList::PixelShaders::PS_SeperableBlurFilter::FilterWidth = 11.0f;

void ShaderList::PixelShaders::PS_SeperableBlurFilter::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	auto fw = std::to_string(FilterWidth);
	int gaussWeights = FilterWidth;
	auto gw = std::string("Gaussian") + std::to_string(gaussWeights);

	D3D_SHADER_MACRO macros[] = {
		{ "FILTER_WIDTH", fw.c_str() },
	{ "GaussianWeight", gw.c_str() },
	{ NULL, NULL }
	};


	pPS_GaussFilter.SetVariables(L"Data/Shaders/PS_SeparableGaussianFilter.tes", SHADER_FILE_PS_SEPERABLEGAUSSIANFILTER, target.c_str(), "main_Gauss", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);
	pPS_GaussFilter.LoadShader(fromHLSLFile);

	pPS_BoxFilter.SetVariables(L"Data/Shaders/PS_SeparableBoxFilter.tes", SHADER_FILE_PS_SEPERABLEGAUSSIANFILTER, target.c_str(), "main_Box", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);
	pPS_BoxFilter.LoadShader(fromHLSLFile);

	auto pReflector_Gauss = pPS_GaussFilter.GetReflection();
	auto pReflector_Box = pPS_BoxFilter.GetReflection();

	mNumInterfaces_Gauss = pReflector_Gauss->GetNumInterfaceSlots();
	mNumInterfaces_Box = pReflector_Box->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IGaussFilterManger = pReflector_Gauss->GetVariableByName("gIGaussFilterManager");
	ID3D11ShaderReflectionVariable* IBoxFilterManger = pReflector_Box->GetVariableByName("gIBoxFilterManager");

	mISlot_GaussFilterManager = IGaussFilterManger->GetInterfaceSlot(0);

	mISlot_BoxFilterManager = IBoxFilterManger->GetInterfaceSlot(0);

	pDynamicClassLinkage_Gauss = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces_Gauss);
	pDynamicClassLinkage_Box = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces_Box);


	auto classLinkage_Gauss = pPS_GaussFilter.GetClassLinkage();
	auto classLinkage_Box = pPS_BoxFilter.GetClassLinkage();


	classLinkage_Gauss->CreateClassInstance("CGaussFilterManager_Horizontally", 0, 0, 0, 0, pSClass_GaussFilterManager_Horizontally.ReleaseAndGetAddressOf());
	classLinkage_Gauss->CreateClassInstance("CGaussFilterManager_Vertically", 0, 0, 0, 0, pSClass_GaussFilterManager_Vertically.ReleaseAndGetAddressOf());

	classLinkage_Box->CreateClassInstance("CBoxFilterManager_Horizontally", 0, 0, 0, 0, pSClass_BoxFilterManager_Horizontally.ReleaseAndGetAddressOf());
	classLinkage_Box->CreateClassInstance("CBoxFilterManager_Vertically", 0, 0, 0, 0, pSClass_BoxFilterManager_Vertically.ReleaseAndGetAddressOf());

}


concurrency::task<void> ShaderList::PixelShaders::PS_SeperableBlurFilter::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}


void ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_GaussFilterManager_Horizontally()
{
	pDynamicClassLinkage_Gauss[mISlot_GaussFilterManager] = pSClass_GaussFilterManager_Horizontally.Get();
}

void ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_GaussFilterManager_Vertically()
{
	pDynamicClassLinkage_Gauss[mISlot_GaussFilterManager] = pSClass_GaussFilterManager_Vertically.Get();
}

void ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_BoxFilterManager_Horizontally()
{
	pDynamicClassLinkage_Box[mISlot_BoxFilterManager] = pSClass_BoxFilterManager_Horizontally.Get();
}

void ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_BoxFilterManager_Vertically()
{
	pDynamicClassLinkage_Box[mISlot_BoxFilterManager] = pSClass_BoxFilterManager_Vertically.Get();
}

void ShaderList::PixelShaders::PS_SeperableBlurFilter::SetShader_GaussFilter(ID3D11DeviceContext* context )
{
	pPS_GaussFilter.SetShader(context, pDynamicClassLinkage_Gauss, mNumInterfaces_Gauss);
}

void ShaderList::PixelShaders::PS_SeperableBlurFilter::SetShader_BoxFilter(ID3D11DeviceContext* context )
{
	pPS_BoxFilter.SetShader(context, pDynamicClassLinkage_Box, mNumInterfaces_Box);
}

void ShaderList::PixelShaders::PS_SeperableBlurFilter::RecompileShader()
{
	InitStatics(true);
}
