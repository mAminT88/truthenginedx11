#include "stdafx.h"
#include "CS_Bloom.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"


ShaderObject<ID3D11ComputeShader> ShaderList::ComputeShaders::CS_Bloom::mCS_Bloom;

UINT ShaderList::ComputeShaders::CS_Bloom::mThreadGroupSizeX = 1024;

void ShaderList::ComputeShaders::CS_Bloom::InitStatics(bool fromHLSLFile)
{
	std::string target = "cs_";
	target.append(SHADER_TARGET_VERSION);

	auto value_1 = std::to_string(mThreadGroupSizeX);

	D3D_SHADER_MACRO macros[] =
	{
		{ "GroupSizeX", value_1.c_str() },
	{ NULL,NULL }
	};


	mCS_Bloom.SetVariables(L"Data/Shaders/CS_Bloom.tes", SHADER_FILE_CS_BLOOM, target.c_str(), "BloomReveal", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mCS_Bloom.LoadShader(fromHLSLFile);

}

concurrency::task<void> ShaderList::ComputeShaders::CS_Bloom::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::ComputeShaders::CS_Bloom::SetShader(ID3D11DeviceContext* context)
{
	mCS_Bloom.SetShader(context, nullptr, 0);
}

void ShaderList::ComputeShaders::CS_Bloom::RecompileShader()
{
	InitStatics(true);
}

UINT ShaderList::ComputeShaders::CS_Bloom::GetThreadGroupSizeX() noexcept
{
	return mThreadGroupSizeX;
}

void ShaderList::ComputeShaders::CS_Bloom::SetStaticValue(UINT ThreadGroupSizeX) noexcept
{
	mThreadGroupSizeX = ThreadGroupSizeX;
}
