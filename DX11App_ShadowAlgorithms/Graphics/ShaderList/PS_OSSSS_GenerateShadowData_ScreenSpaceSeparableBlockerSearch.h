#pragma once


#include "IPixelShader.h"
#include "Macros.h"

template<class T> class ShaderObject;



namespace ShaderList
{

	namespace PixelShaders
	{


		class PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0 : public IPixelShader
		{
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_GenerateShadowData_0;
			static UINT mISlot_ILight;



			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_GenerateShadowData_0;
			static ComPtr<ID3D11ClassInstance> pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
			static ComPtr<ID3D11ClassInstance> pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];



			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:


			static int mBlockerSearchWidthSampleCount;
			static int mHardShadowFilterSampleCount;
			static float mBilateralDepth;

			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);

			static void RecompileShader();


			static void Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex);
			static void Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex);

		};


		class PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1 : public IPixelShader
		{
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_GenerateShadowData_1;
			static UINT mISlot_ILight;


			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_GenerateShadowData_1;
			static ComPtr<ID3D11ClassInstance> pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
			static ComPtr<ID3D11ClassInstance> pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];



			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:


			static int mBlockerSearchWidthSampleCount;
			static int mHardShadowFilterSampleCount;
			static float mBilateralDepth;

			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);

			static void RecompileShader();

			static void Set_ClassInstance_GenerateShadowData1();
			

			static void Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex);
			static void Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex);

		};


		class PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2 : public IPixelShader
		{
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_GenerateShadowData_2;
			static UINT mISlot_ILight;


			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_GenerateShadowData_2;
			static ComPtr<ID3D11ClassInstance> pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
			static ComPtr<ID3D11ClassInstance> pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];


			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			
			static int mBlockerSearchWidthSampleCount;
			static int mHardShadowFilterSampleCount;
			static float mBilateralDepth;

			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);

			static void RecompileShader();

			static void Set_ClassInstance_GenerateShadowData2();
			

			static void Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex);
			static void Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex);
		};


	};

}