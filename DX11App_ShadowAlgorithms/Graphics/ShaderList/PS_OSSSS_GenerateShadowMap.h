#pragma once

#include "IPixelShader.h"

template<class T> class ShaderObject;



namespace ShaderList
{
	namespace PixelShaders
	{

		class PS_OSSSS_GenerateShadowMap : public IPixelShader
		{
		private:
			static ShaderObject<ID3D11PixelShader> mPS;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();


			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();
		};

	};
}
