#pragma once

#include "IPixelShader.h"

template<class T> class ShaderObject;



namespace ShaderList
{

	namespace PixelShaders
	{
		
		class PS_BlurBilateralByDepthNormal : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> pPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_ApplyBlur;

			//Shader Class Instances

			//SSAO Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_ApplyBlur_Horz;
			static ComPtr<ID3D11ClassInstance> pSClass_ApplyBlur_Vert;


			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:

			//Configuration Variables
			static float BlurWidth;
			static float DepthDifference;

			static void InitStatics(bool fromHLSLFile = false);
			static concurrency::task<void> CompileAsync();
			//void Init();

			static void SetClassInstance_ApplyBlur_Horz();
			static void SetClassInstance_ApplyBlur_Vert();

			static void SetShader(ID3D11DeviceContext* context);

			static void RecompileShader();

			static void Destroy();
		};

	}

}