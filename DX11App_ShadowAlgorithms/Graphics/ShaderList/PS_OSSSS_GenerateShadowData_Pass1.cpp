#include "stdafx.h"
#include "PS_OSSSS_GenerateShadowData_Pass1.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mPS = ShaderObject<ID3D11PixelShader>();

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mNumInterfaces = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mISlot_GenerateShadowData_1 = 0;

UINT ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mISlot_ILight = 0;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::pSClass_GenerateShadowData_1_Horz = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::pSClass_GenerateShadowData_1_Vert = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];

ID3D11ClassInstance** ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::pDynamicClassLinkage = nullptr;

float ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mBilateralDepth = 20.0f;
float ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mFilterWidth = 11.0f;
float ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mBlockerSearchSampleCount = 9.0f;


ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::PS_OSSSS_GenerateShadowData_Pass1_Version ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::PSVersion = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::PS_OSSSS_GenerateShadowData_Pass1_Version::normal;

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	std::string blockerSearchSampleCntS = std::to_string(mBlockerSearchSampleCount);
	std::string blockerSearchPoissonDiskS = std::string("Poisson") + std::to_string((int)mBlockerSearchSampleCount);
	std::string bilateralDepthS = std::to_string(mBilateralDepth);
	std::string filterWidthS = std::to_string(mFilterWidth);
	std::string GaussianS = std::string("Gaussian") + std::to_string((int)mFilterWidth);

	D3D_SHADER_MACRO macros[] = {
		{ "BLOCKER_SEARCH_SAMPLE_COUNT", blockerSearchSampleCntS.c_str() },
	{ "BLOCKER_SEARCH_POISSON", blockerSearchPoissonDiskS.c_str() },
	{ "FILTER_WIDTH_HARDSHADOW", filterWidthS.c_str() },
	{ "GAUSSIAN_WEIGHTS_HARDSHADOW", GaussianS.c_str() },
	{ "BILATERAL_DEPTH", bilateralDepthS.c_str() },
	{ NULL, NULL }
	};

	mPS.SetVariables(L"Data/Shaders/PS_OSSSS_GenerateShadowData_1.tes", SHADER_FILE_PS_OSSSS_GENERATESHADOWDATA, target.c_str(), "main_1", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	mPS.LoadShader(fromHLSLFile);

	auto pReflector = mPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IGenerateShadowData_1 = pReflector->GetVariableByName("g_IGenerateShadowData_1");
	ID3D11ShaderReflectionVariable* ILight = pReflector->GetVariableByName("iLight");


	mISlot_GenerateShadowData_1 = IGenerateShadowData_1->GetInterfaceSlot(0);
	mISlot_ILight = ILight->GetInterfaceSlot(0);

	pDynamicClassLinkage = (ID3D11ClassInstance * *)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);


	auto classLinkage = mPS.GetClassLinkage();


	classLinkage->CreateClassInstance("CGenerateShadowData_1_Horz", 0, 0, 0, 0, pSClass_GenerateShadowData_1_Horz.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CGenerateShadowData_1_Vert", 0, 0, 0, 0, pSClass_GenerateShadowData_1_Vert.ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_SPOTLIGHT; ++i)
		classLinkage->GetClassInstance("gSpotLights", 0, pSClass_Light_SpotLights[i].ReleaseAndGetAddressOf());

	for (int i = 0; i < MAX_LIGHT_DIRECTIONALLIGHT; ++i)
		classLinkage->GetClassInstance("gDirectLights", 0, pSClass_Light_DirectLights[i].ReleaseAndGetAddressOf());


	Set_ClassInstance_HorizontalStep();

}

concurrency::task<void> ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader(ID3D11DeviceContext * context)
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_HorizontalStep()
{
	if (mISlot_GenerateShadowData_1 < 100)
		pDynamicClassLinkage[mISlot_GenerateShadowData_1] = pSClass_GenerateShadowData_1_Horz.Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_VerticalStep()
{
	if (mISlot_GenerateShadowData_1 < 100)
		pDynamicClassLinkage[mISlot_GenerateShadowData_1] = pSClass_GenerateShadowData_1_Vert.Get();
}


void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_SpotLights[SpotLightIndex].Get();
}

void ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex)
{
	pDynamicClassLinkage[mISlot_ILight] = pSClass_Light_DirectLights[DirectLightIndex].Get();
}
