#pragma once

namespace ShaderList
{

	namespace VertexShaders
	{

		interface IVertexShader
		{
		protected:

		public:
		};

		interface IVertexShader_3DMesh : IVertexShader
		{
		public:
		};

	};

}
