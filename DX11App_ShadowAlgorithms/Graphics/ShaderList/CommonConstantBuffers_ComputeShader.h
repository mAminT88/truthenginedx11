#pragma once

template<class T> class ConstantBuffer;


namespace CommonConstantBuffers
{

	class ComputeShader
	{
	private:

		struct CB_Per_Frame
		{
			XMFLOAT4X4 gViewInv;

			XMFLOAT4 gEyePerspectiveValues;

			XMFLOAT3 gEyePos;
			float pad0;
		};

		struct CB_Unfrequent
		{
			XMFLOAT4 gScreenSize;

			XMFLOAT4 gShadowMapSize;

			float gFixedPrecision_FloatToInt;
			XMFLOAT3 pad0;
		};

		static ConstantBuffer<CB_Per_Frame> mCBPerFrame;
		static ConstantBuffer<CB_Unfrequent> mCBUnfrequent;

	public:

		static CB_Per_Frame mDataPerFrame;
		static CB_Unfrequent mDataUnfrequent;

		static void InitBuffers(ID3D11DeviceContext* context);
		static void UpdateBuffer_PerFrame(ID3D11DeviceContext* context);
		static void UpdateBuffer_UnFrequent(ID3D11DeviceContext* context);

	};

}
