#pragma once

#include "IPixelShader.h"

template<class T> class ShaderObject;



namespace ShaderList
{
	namespace PixelShaders
	{

		class PS_GenerateSAT_UINT : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_SAT;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_SAT_UINT_Horizontal;
			static ComPtr<ID3D11ClassInstance> pSClass_SAT_UINT_Horizontal_0;
			static ComPtr<ID3D11ClassInstance> pSClass_SAT_UINT_Vertical;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);
			static concurrency::task<void> CompileAsync();

			static void SetClassInstace_SAT_UINT_Horizontal();
			static void SetClassInstace_SAT_UINT_Horizontal_0();
			static void SetClassInstace_SAT_UINT_Vertical();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();
		};

	};
}
