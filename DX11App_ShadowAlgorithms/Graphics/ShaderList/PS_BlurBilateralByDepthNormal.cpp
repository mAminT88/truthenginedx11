#include "stdafx.h"
#include "PS_BlurBilateralByDepthNormal.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::pPS;

UINT ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::mNumInterfaces;

UINT ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::mISlot_ApplyBlur;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::pSClass_ApplyBlur_Horz;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::pSClass_ApplyBlur_Vert;

ID3D11ClassInstance** ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::pDynamicClassLinkage;

float ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::BlurWidth = 11;

float ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::DepthDifference = 50.0f;

void ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);


	std::string blurWidth = std::to_string(BlurWidth);
	std::string blurRadius = std::to_string((BlurWidth - 1.0) / 2.0);
	std::string gaussianWeights = "Gaussian" + std::to_string((int)BlurWidth);
	std::string depthDifference = std::to_string(DepthDifference);


	D3D_SHADER_MACRO macros[] =
	{
		{ "BLUR_WIDTH" , blurWidth.c_str() },
		{ "BLUR_RADIUS" , blurRadius.c_str() },
		{ "GAUSSIANWEIGHTS" , gaussianWeights.c_str() },
		{ "DEPTH_DIFF" , depthDifference.c_str() },
		{NULL,NULL}
	};


	pPS.SetVariables(L"Data/Shaders/PS_BlurBilateralByDepthNormal.tes", SHADER_FILE_PS_BLURBILATERALBYDEPTHNORMAL, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macros);

	pPS.LoadShader(fromHLSLFile);

	auto pReflector = pPS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IApplyBlur = pReflector->GetVariableByName("gIApplyBlur");

	mISlot_ApplyBlur = IApplyBlur->GetInterfaceSlot(0);

	auto classLinkage = pPS.GetClassLinkage();
	   
	classLinkage->CreateClassInstance("ApplyBlur_Horz", 0, 0, 0, 0, pSClass_ApplyBlur_Horz.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("ApplyBlur_Vert", 0, 0, 0, 0, pSClass_ApplyBlur_Vert.ReleaseAndGetAddressOf());

	pDynamicClassLinkage = (ID3D11ClassInstance * *)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	SetClassInstance_ApplyBlur_Horz();
}

concurrency::task<void> ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::CompileAsync()
{
	return concurrency::task<void>([]() {InitStatics(true); });
}

void ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::SetClassInstance_ApplyBlur_Horz()
{
	pDynamicClassLinkage[mISlot_ApplyBlur] = pSClass_ApplyBlur_Horz.Get();
}

void ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::SetClassInstance_ApplyBlur_Vert()
{
	pDynamicClassLinkage[mISlot_ApplyBlur] = pSClass_ApplyBlur_Vert.Get();
}

void ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::SetShader(ID3D11DeviceContext* context)
{
	pPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);

}

void ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::Destroy()
{
	pPS.Release();
}
