#include "stdafx.h"
#include "VS_OSSSS_GenerateShadowMap.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"
#include "Globals.h"

using namespace Globals;

ShaderObject<ID3D11VertexShader> ShaderList::VertexShaders::VS_OSSSS_GenerateShadowMap::mVS_Basic32 = ShaderObject<ID3D11VertexShader>();
ShaderObject<ID3D11VertexShader> ShaderList::VertexShaders::VS_OSSSS_GenerateShadowMap::mVS_Skinned = ShaderObject<ID3D11VertexShader>();


void ShaderList::VertexShaders::VS_OSSSS_GenerateShadowMap::InitStatics(bool fromHLSLFile)
{
	std::string target = "vs_";
	target.append(SHADER_TARGET_VERSION);

	D3D_SHADER_MACRO macro[] =
	{
		{"VERTEX_TYPE_BASIC32", ""}, {NULL, NULL}
	};
	
	mVS_Basic32.SetVariables(L"Data/Shaders/VS_OSSSS_Basic32.tes",
		SHADER_FILE_VS_OSSSS_BUILDSHADOWMAP
		, target.c_str()
		, "main"
		, D3D_COMPILE_STANDARD_FILE_INCLUDE
		, macro);

	mVS_Basic32.LoadShader(fromHLSLFile);

	macro[0].Name = "VERTEX_TYPE_SKINNED";


	mVS_Skinned.SetVariables(L"Data/Shaders/VS_OSSSS_Skinned.tes",
		SHADER_FILE_VS_OSSSS_BUILDSHADOWMAP
		, target.c_str()
		, "main"
		, D3D_COMPILE_STANDARD_FILE_INCLUDE
		, macro
		);

	mVS_Skinned.LoadShader(fromHLSLFile);
}

concurrency::task<void> ShaderList::VertexShaders::VS_OSSSS_GenerateShadowMap::CompileAsync()
{
	return concurrency::task<void>([]() {InitStatics(); });
}

void ShaderList::VertexShaders::VS_OSSSS_GenerateShadowMap::SetShader_Basic32(ID3D11DeviceContext* context )
{
	mVS_Basic32.SetShader(gContext.Get());
}

void ShaderList::VertexShaders::VS_OSSSS_GenerateShadowMap::SetShader_Skinned(ID3D11DeviceContext* context )
{
	mVS_Skinned.SetShader(gContext.Get());
}

void ShaderList::VertexShaders::VS_OSSSS_GenerateShadowMap::RecompileShader()
{
	InitStatics(true);
}
