#include "stdafx.h"
#include "VS_BuildShadowMap.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11VertexShader> ShaderList::VertexShaders::VS_BuildShadowMap::mVS_Basic32 = ShaderObject<ID3D11VertexShader>();
ShaderObject<ID3D11VertexShader> ShaderList::VertexShaders::VS_BuildShadowMap::mVS_Skinned = ShaderObject<ID3D11VertexShader>();


void ShaderList::VertexShaders::VS_BuildShadowMap::InitStatics(bool fromHLSLFile)
{

	std::string target = "vs_";
	target.append(SHADER_TARGET_VERSION);

	D3D_SHADER_MACRO macro[] =
	{
		{ "VERTEX_TYPE_BASIC32", "" },
	    {NULL, NULL}
	};

	mVS_Basic32.SetVariables(L"Data/Shaders/VS_Basic32.tes", SHADER_FILE_VS_SHADOWMAP_BASIC32VERTEX, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macro);

	mVS_Basic32.LoadShader(fromHLSLFile);


	macro[0].Name = "VERTEX_TYPE_SKINNED";

	mVS_Skinned.SetVariables(L"Data/Shaders/VS_Skinned.tes", SHADER_FILE_VS_SHADOWMAP_BASIC32VERTEX, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, macro);

	mVS_Skinned.LoadShader(fromHLSLFile);

}

concurrency::task<void> ShaderList::VertexShaders::VS_BuildShadowMap::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(ID3D11DeviceContext* context)
{
	mVS_Basic32.SetShader(context, nullptr, 0);
}

void ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(ID3D11DeviceContext* context)
{
	mVS_Skinned.SetShader(context, nullptr, 0);
}

void ShaderList::VertexShaders::VS_BuildShadowMap::RecompileShader()
{
	InitStatics(true);
}
