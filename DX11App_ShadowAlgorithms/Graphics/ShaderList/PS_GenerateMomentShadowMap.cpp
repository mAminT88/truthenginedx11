#include "stdafx.h"
#include "PS_GenerateMomentShadowMap.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_GenerateMomentShadowMap::mPS = ShaderObject<ID3D11PixelShader>();

int ShaderList::PixelShaders::PS_GenerateMomentShadowMap::mNumInterfaces = 0;

int ShaderList::PixelShaders::PS_GenerateMomentShadowMap::mISlot_ILight = 0;

ID3D11ClassInstance** ShaderList::PixelShaders::PS_GenerateMomentShadowMap::pDynamicClassLinkage = nullptr;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateMomentShadowMap::pPSClass_Light_Directs[MAX_LIGHT_DIRECTIONALLIGHT];

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_GenerateMomentShadowMap::pPSClass_Light_Spots[MAX_LIGHT_SPOTLIGHT];

void ShaderList::PixelShaders::PS_GenerateMomentShadowMap::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);
	   
	mPS.SetVariables(L"Data/Shaders/PS_GenerateMomentShadowMap.tes", SHADER_FILE_PS_GENERATEMOMENTSHADOWMAP, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, nullptr);

	mPS.LoadShader(fromHLSLFile);

	auto Reflector = mPS.GetReflection();

	mNumInterfaces = Reflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* iLight = Reflector->GetVariableByName("iLight");

	mISlot_ILight = iLight->GetInterfaceSlot(0);

	auto classLinkage = mPS.GetClassLinkage();

	for (int Index = 0; Index < MAX_LIGHT_DIRECTIONALLIGHT; Index++)
	{
		classLinkage->GetClassInstance("gDirectLights", Index, pPSClass_Light_Directs[Index].ReleaseAndGetAddressOf());
	}

	for (int Index = 0; Index < MAX_LIGHT_SPOTLIGHT; Index++)
	{
		classLinkage->GetClassInstance("gSpotLights", Index, pPSClass_Light_Spots[Index].ReleaseAndGetAddressOf());
	}

	pDynamicClassLinkage = (ID3D11ClassInstance * *)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);
}

concurrency::task<void> ShaderList::PixelShaders::PS_GenerateMomentShadowMap::CompileAsync()
{
	return concurrency::task<void>([]() {InitStatics(); });
}

void ShaderList::PixelShaders::PS_GenerateMomentShadowMap::SetShader(ID3D11DeviceContext* context)
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_GenerateMomentShadowMap::RecompileShader()
{
	InitStatics(true);
}

void ShaderList::PixelShaders::PS_GenerateMomentShadowMap::SetClassInstance_ILight_Spot(int Index)
{
	pDynamicClassLinkage[mISlot_ILight] = pPSClass_Light_Spots[Index].Get();
}

void ShaderList::PixelShaders::PS_GenerateMomentShadowMap::SetClassInstance_ILight_Direct(int Index)
{
	pDynamicClassLinkage[mISlot_ILight] = pPSClass_Light_Directs[Index].Get();
}
