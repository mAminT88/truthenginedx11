#pragma once

#include "IComputeShader.h"

template<class T> class ShaderObject;




namespace ShaderList {

	namespace ComputeShaders
	{

		class CS_HDR_AvgLuminance : public IComputeShader
		{
		protected:
			static ShaderObject<ID3D11ComputeShader> mCS_FirstPass;
			static ShaderObject<ID3D11ComputeShader> mCS_SecondPass;

			//Interfaces Slot
			//static UINT mNumInterfaces;

			static UINT mThreadGroupSizeX_FirstPass;
			static UINT mThreadGroupSizeX_SecondPass;
			static UINT mThreadGroupSizeY;

			//Shader Class Instances
			//static ComPtr<ID3D11ClassInstance> pSClass_ApplyPrefixSum_Horz;

			//ClassInstaceArray
			//static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader_FirstPass(ID3D11DeviceContext* context);
			static void SetShader_SecondPass(ID3D11DeviceContext* context);
			static void RecompileShader();

			static UINT GetThreadGroupSizeX_FirstPass() noexcept;
			static UINT GetThreadGroupSizeX_SecondPass() noexcept;
			static UINT GetThreadGroupSizeY() noexcept;

			static void SetStaticValue(UINT ThreadGroupSizeX_FirstPass, UINT ThreadGroupSizeX_SecondPass, UINT ThreadGroupSizeY) noexcept;
		};

	}

}