#include "stdafx.h"
#include "VS_Render2D.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"


ShaderObject<ID3D11VertexShader> ShaderList::VertexShaders::VS_Render2D::pVS = ShaderObject<ID3D11VertexShader>();

UINT ShaderList::VertexShaders::VS_Render2D::mNumInterfaces = 0;
UINT ShaderList::VertexShaders::VS_Render2D::mISlot_IPosition = 0;

ComPtr<ID3D11ClassInstance> ShaderList::VertexShaders::VS_Render2D::pSClass_Position_WVP = nullptr;
ComPtr<ID3D11ClassInstance> ShaderList::VertexShaders::VS_Render2D::pSClass_Position_noWVP = nullptr;

ID3D11ClassInstance** ShaderList::VertexShaders::VS_Render2D::pDynamicClassLinkage = nullptr;


void ShaderList::VertexShaders::VS_Render2D::InitStatics(bool fromHLSLFile)
{
	std::string target = "vs_";
	target.append(SHADER_TARGET_VERSION);

	pVS.SetVariables(L"Data/Shaders/VS_Render2D.tes", SHADER_FILE_VS_RENDER2D, target.c_str(), "main", D3D_COMPILE_STANDARD_FILE_INCLUDE, nullptr);

	pVS.LoadShader(fromHLSLFile);

	auto pReflector = pVS.GetReflection();

	mNumInterfaces = pReflector->GetNumInterfaceSlots();

	ID3D11ShaderReflectionVariable* IPosition = pReflector->GetVariableByName("g_IPosition");

	mISlot_IPosition = IPosition->GetInterfaceSlot(0);

	auto classLinkage = pVS.GetClassLinkage();

	/*classLinkage->GetClassInstance("g_CPosition_WVP", 0, pSClass_Position_WVP.ReleaseAndGetAddressOf());
	classLinkage->GetClassInstance("g_CPosition_noWVP", 0, pSClass_Position_noWVP.ReleaseAndGetAddressOf());*/

	classLinkage->CreateClassInstance("CPosition_WVP", 0, 0, 0, 0, pSClass_Position_WVP.ReleaseAndGetAddressOf());
	classLinkage->CreateClassInstance("CPosition_noWVP", 0, 0, 0, 0, pSClass_Position_noWVP.ReleaseAndGetAddressOf());

	pDynamicClassLinkage = (ID3D11ClassInstance**)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	SetClassInstace_Position_noWVP();
}

concurrency::task<void> ShaderList::VertexShaders::VS_Render2D::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

//void ShaderList::VertexShaders::VS_Render2D::Init()
//{
//	
//}

void ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_WVP()
{
	pDynamicClassLinkage[mISlot_IPosition] = pSClass_Position_WVP.Get();
}

void ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP()
{
	pDynamicClassLinkage[mISlot_IPosition] = pSClass_Position_noWVP.Get();
}

void ShaderList::VertexShaders::VS_Render2D::SetShader(ID3D11DeviceContext*  context)
{
	pVS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::VertexShaders::VS_Render2D::RecompileShader()
{
	InitStatics(true);
	//Init();
}
