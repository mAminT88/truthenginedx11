#pragma once

#include "IPixelShader.h"

template<class T> class ShaderObject;



namespace ShaderList
{
	namespace PixelShaders
	{

		class PS_GenerateSAT : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_SAT;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_SAT_Horizontal;
			static ComPtr<ID3D11ClassInstance> pSClass_SAT_Vertical;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);
			static concurrency::task<void> CompileAsync();

			static void SetClassInstace_SAT_Horizontal();
			static void SetClassInstace_SAT_Vertical();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();
		};

	}
}