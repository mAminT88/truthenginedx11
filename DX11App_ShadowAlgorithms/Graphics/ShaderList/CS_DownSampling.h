#pragma once

#include "IComputeShader.h"

template<class T> class ShaderObject;

namespace ShaderList
{
	namespace ComputeShaders
	{
		class CS_DownSampling : IComputeShader
		{

		protected:
			static ShaderObject<ID3D11ComputeShader> mCS;

			//Interfaces Slot
			//static UINT mNumInterfaces;

			static UINT mThreadGroupSizeX;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

			static UINT GetThreadGroupSizeX() noexcept;

			static void SetStaticValue(UINT ThreadGroupSizeX) noexcept;

		};
	}
}
