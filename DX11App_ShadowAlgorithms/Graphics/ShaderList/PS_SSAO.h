#pragma once

#include "IPixelShader.h"

template<class T> class ShaderObject;



namespace ShaderList
{
	namespace PixelShaders
	{
		class PS_SSAO : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> pPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_SSAO;

			//SSAO Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_SSAO_Default;


			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;


		public:


			static void InitStatics(bool fromHLSLFile = false);
			static concurrency::task<void> CompileAsync();

			static void SetClassInstance_SSAOManager_Default();

			static void SetShader(ID3D11DeviceContext* context);

			static void RecompileShader();

			static void Destroy();
		};
	}
}
