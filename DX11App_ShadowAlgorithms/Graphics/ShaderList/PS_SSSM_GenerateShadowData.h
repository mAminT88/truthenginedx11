#pragma once

#include "IPixelShader.h"
#include "Macros.h"

template<class T> class ShaderObject;



namespace ShaderList
{
	namespace PixelShaders
	{

		class PS_SSSM_GenerateShadowData_0 : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_ILight;

			static std::string ALPHA;
			static std::string BETA;
			static std::string BLOCKER_SEARCH_SAMPLE_COUNT;
			static std::string BLOCKER_SEARCH_HALF_SAMPLE_COUNT;
			static std::string HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT;
			static std::string HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT;
			static std::string HARDSHADOW_FILTER_GAUSSIAN_WEIGHT;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
			static ComPtr<ID3D11ClassInstance> pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);

			static void RecompileShader();

			static void SetStaticValues(std::string BlockerSearchSampleCount, std::string BlockerSearchHalfSampleCount);

			static void Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex);
			static void Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex);

		};



		class PS_SSSM_GenerateShadowData_1 : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_ILight;

			static std::string ALPHA;
			static std::string BETA;
			static std::string BLOCKER_SEARCH_SAMPLE_COUNT;
			static std::string BLOCKER_SEARCH_HALF_SAMPLE_COUNT;
			static std::string HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT;
			static std::string HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT;
			static std::string HARDSHADOW_FILTER_GAUSSIAN_WEIGHT;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
			static ComPtr<ID3D11ClassInstance> pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);

			static void RecompileShader();

			static void SetStaticValues(std::string BlockerSearchSampleCount
				, std::string BlockerSearchHalfSampleCount
				, std::string HardShadowFilterWidthSampleCount
				, std::string HardShadowFilterRadiusSampleCount
				, std::string HardShadowFilterGaussianWeights
			);

			static void Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex);
			static void Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex);
		};



		class PS_SSSM_GenerateShadowData_2 : public IPixelShader
		{
		protected:
			static ShaderObject<ID3D11PixelShader> mPS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_ILight;

			static std::string ALPHA;
			static std::string BETA;
			static std::string BLOCKER_SEARCH_SAMPLE_COUNT;
			static std::string BLOCKER_SEARCH_HALF_SAMPLE_COUNT;
			static std::string HARDSHADOW_FILTER_WIDTH_SAMPLE_COUNT;
			static std::string HARDSHADOW_FILTER_RADIUS_SAMPLE_COUNT;
			static std::string HARDSHADOW_FILTER_GAUSSIAN_WEIGHT;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_Light_SpotLights[MAX_LIGHT_SPOTLIGHT];
			static ComPtr<ID3D11ClassInstance> pSClass_Light_DirectLights[MAX_LIGHT_DIRECTIONALLIGHT];


			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetShader(ID3D11DeviceContext* context);

			static void RecompileShader();

			static void SetStaticValues(std::string HardShadowFilterWidthSampleCount
				, std::string HardShadowFilterRadiusSampleCount
				, std::string HardShadowFilterGaussianWeights
			);

			static void Set_ClassInstance_ActiveLight_SpotLight(int SpotLightIndex);
			static void Set_ClassInstance_ActiveLight_DirectLight(int DirectLightIndex);
		};

	};
}
