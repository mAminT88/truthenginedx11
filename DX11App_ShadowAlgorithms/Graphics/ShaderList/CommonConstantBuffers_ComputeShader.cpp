#include "stdafx.h"
#include "CommonConstantBuffers_ComputeShader.h"
#include "Globals.h"
#include "Graphics/ShaderSlots_DeferredShading.h"
#include "Graphics/ConstantBuffer.h"

using namespace Globals;

ConstantBuffer<CommonConstantBuffers::ComputeShader::CB_Per_Frame> CommonConstantBuffers::ComputeShader::mCBPerFrame;

ConstantBuffer<CommonConstantBuffers::ComputeShader::CB_Unfrequent> CommonConstantBuffers::ComputeShader::mCBUnfrequent;

CommonConstantBuffers::ComputeShader::CB_Per_Frame CommonConstantBuffers::ComputeShader::mDataPerFrame;

CommonConstantBuffers::ComputeShader::CB_Unfrequent CommonConstantBuffers::ComputeShader::mDataUnfrequent;

void CommonConstantBuffers::ComputeShader::InitBuffers(ID3D11DeviceContext* context)
{
	mCBPerFrame.InitConstantBuffer(SHADER_SLOTS_CS_CB_PERFRAME
		, CONSTANT_BUFFER_CS
		, &mDataPerFrame
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);
	mCBPerFrame.SetConstantBuffer(context);

	mCBUnfrequent.InitConstantBuffer(SHADER_SLOTS_CS_CB_UNFREQUENT
		, CONSTANT_BUFFER_CS
		, &mDataUnfrequent
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);
	mCBUnfrequent.SetConstantBuffer(context);

	mDataUnfrequent.gFixedPrecision_FloatToInt = (std::pow(2, 32) - 1.0f) / 784.0f;
	mDataUnfrequent.gScreenSize = XMFLOAT4(gClientWidth, gClientHeight, 1 /gClientWidth, 1/ gClientHeight);
	mDataUnfrequent.gShadowMapSize = XMFLOAT4(gShadowMapResolution, gShadowMapResolution, gShadowMapDXY, gShadowMapDXY);

	mCBUnfrequent.Update(context);
	
}

void CommonConstantBuffers::ComputeShader::UpdateBuffer_PerFrame(ID3D11DeviceContext* context)
{
	mCBPerFrame.Update(context);
}

void CommonConstantBuffers::ComputeShader::UpdateBuffer_UnFrequent(ID3D11DeviceContext* context)
{
	mCBUnfrequent.Update(context);
}
