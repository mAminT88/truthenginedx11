#pragma once

#include "IComputeShader.h"

template<class T> class ShaderObject;

namespace ShaderList {

	namespace ComputeShaders
	{
		
		class CS_GenerateSAT_Horz : public IComputeShader
		{

		protected:
			static ShaderObject<ID3D11ComputeShader> mCS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_ApplyPrefixSum;

			static UINT mThreadGroupSizeX;
			static UINT mThreadGroupSizeY;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_ApplyPrefixSum_Horz;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetClassInstace_ApplyPrefixSum_Horz();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

			static UINT GetThreadGroupSizeX();
			static UINT GetThreadGroupSizeY();

			static void SetStaticValue(UINT ThreadGroupSizeX, UINT ThreadGroupSizeY);

		
		};

		class CS_GenerateSAT_Vert : public IComputeShader
		{

		protected:
			static ShaderObject<ID3D11ComputeShader> mCS;

			//Interfaces Slot
			static UINT mNumInterfaces;
			static UINT mISlot_ApplyPrefixSum;

			static UINT mThreadGroupSizeX;
			static UINT mThreadGroupSizeY;

			//Shader Class Instances
			static ComPtr<ID3D11ClassInstance> pSClass_ApplyPrefixSum_Vert;

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);

			static concurrency::task<void> CompileAsync();

			static void SetClassInstace_ApplyPrefixSum_Vert();

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

			static UINT GetThreadGroupSizeX();
			static UINT GetThreadGroupSizeY();

			static void SetStaticValue(UINT ThreadGroupSizeX, UINT ThreadGroupSizeY);


		};

	};

}
