#pragma once

#include "IPixelShader.h"
#include "Macros.h"

template<class T> class ShaderObject;

namespace ShaderList
{
	namespace PixelShaders
	{

		class PS_GenerateVarianceShadowMap : IPixelShader
		{
		protected:

			static int mNumInterfaces;
			static UINT mISlot_VSMGenerator;
			static UINT mISlot_ILight;

			static ShaderObject<ID3D11PixelShader> mPS;


			static ComPtr<ID3D11ClassInstance> pPSClass_VSMGenerator_LinearDepth;
			static ComPtr<ID3D11ClassInstance> pPSClass_VSMGenerator_NormalizedLinearDepth;

			static ComPtr<ID3D11ClassInstance> pPSClass_Light_Spots[MAX_LIGHT_SPOTLIGHT];
			static ComPtr<ID3D11ClassInstance> pPSClass_Light_Directs[MAX_LIGHT_DIRECTIONALLIGHT];

			//ClassInstaceArray
			static ID3D11ClassInstance** pDynamicClassLinkage;

		public:
			static void InitStatics(bool fromHLSLFile = false);
			static concurrency::task<void> CompileAsync();

			static void SetClassInstance_VSMGenerator_LinearDepth();
			static void SetClassInstance_VSMGenerator_NormalizedLinearDepth();

			static void SetClassInstance_Light_Spot(int Index);
			static void SetClassInstance_Light_Direct(int Index);

			static void SetShader(ID3D11DeviceContext* context);
			static void RecompileShader();

		};

	};
}
