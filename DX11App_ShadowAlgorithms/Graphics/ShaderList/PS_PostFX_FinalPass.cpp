#include "stdafx.h"
#include "PS_PostFX_FinalPass.h"
#include "Graphics/Shaders.h"
#include "Graphics/ShaderFiles.h"

ShaderObject<ID3D11PixelShader> ShaderList::PixelShaders::PS_PostFX_FinalPass::mPS;

UINT ShaderList::PixelShaders::PS_PostFX_FinalPass::mNumInterfaces;

UINT ShaderList::PixelShaders::PS_PostFX_FinalPass::mISlot_IToneMapping;

UINT ShaderList::PixelShaders::PS_PostFX_FinalPass::mISlot_IBloom;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_PostFX_FinalPass::pSClass_ToneMapping_Reinhard;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_PostFX_FinalPass::pSClass_ToneMapping_ACES;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_PostFX_FinalPass::pSClass_Bloom_None;

ComPtr<ID3D11ClassInstance> ShaderList::PixelShaders::PS_PostFX_FinalPass::pSClass_Bloom_Default;

ID3D11ClassInstance** ShaderList::PixelShaders::PS_PostFX_FinalPass::pDynamicClassLinkage = nullptr;

void ShaderList::PixelShaders::PS_PostFX_FinalPass::InitStatics(bool fromHLSLFile)
{
	std::string target = "ps_";
	target.append(SHADER_TARGET_VERSION);

	mPS.SetVariables(L"Data/Shaders/PS_OSSSS_PostFX_FinalPass.tes", SHADER_FILE_PS_POSTFX_FINALPASS, target.c_str(), "FinalPassPS", D3D_COMPILE_STANDARD_FILE_INCLUDE, nullptr);
	mPS.LoadShader(fromHLSLFile);

	auto pRelection = mPS.GetReflection();

	mNumInterfaces = pRelection->GetNumInterfaceSlots();
	pDynamicClassLinkage = (ID3D11ClassInstance * *)malloc(sizeof(ID3D11ClassInstance*) * mNumInterfaces);

	ID3D11ShaderReflectionVariable* IToneMapping = pRelection->GetVariableByName("gIToneMapping");
	ID3D11ShaderReflectionVariable* IBloom = pRelection->GetVariableByName("gIBloom");

	mISlot_IToneMapping = IToneMapping->GetInterfaceSlot(0);
	mISlot_IBloom = IBloom->GetInterfaceSlot(0);

	auto ClassLinckage = mPS.GetClassLinkage();

	ClassLinckage->CreateClassInstance("ToneMapping_Reinhard", 0, 0, 0, 0, pSClass_ToneMapping_Reinhard.ReleaseAndGetAddressOf());
	ClassLinckage->CreateClassInstance("ToneMapping_ACES", 0, 0, 0, 0, pSClass_ToneMapping_ACES.ReleaseAndGetAddressOf());

	ClassLinckage->CreateClassInstance("Bloom_None", 0, 0, 0, 0, pSClass_Bloom_None.ReleaseAndGetAddressOf());
	ClassLinckage->CreateClassInstance("Bloom_Default", 0, 0, 0, 0, pSClass_Bloom_Default.ReleaseAndGetAddressOf());

	SetClassInstance_ToneMapping_Reinhard();
	SetClassInstance_Bloom_None();
}

concurrency::task<void> ShaderList::PixelShaders::PS_PostFX_FinalPass::CompileAsync()
{
	return concurrency::task<void>([]() { InitStatics(); });
}

void ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_ToneMapping_Reinhard()
{
	pDynamicClassLinkage[mISlot_IToneMapping] = pSClass_ToneMapping_Reinhard.Get();
}

void ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_ToneMapping_ACES()
{
	pDynamicClassLinkage[mISlot_IToneMapping] = pSClass_ToneMapping_ACES.Get();
}

void ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_Bloom_Default()
{
	pDynamicClassLinkage[mISlot_IBloom] = pSClass_Bloom_Default.Get();
}

void ShaderList::PixelShaders::PS_PostFX_FinalPass::SetClassInstance_Bloom_None()
{
	pDynamicClassLinkage[mISlot_IBloom] = pSClass_Bloom_None.Get();
}

void ShaderList::PixelShaders::PS_PostFX_FinalPass::SetShader(ID3D11DeviceContext* context )
{
	mPS.SetShader(context, pDynamicClassLinkage, mNumInterfaces);
}

void ShaderList::PixelShaders::PS_PostFX_FinalPass::RecompileShader()
{
	InitStatics(true);
}
