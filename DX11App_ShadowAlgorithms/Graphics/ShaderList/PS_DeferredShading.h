#pragma once

#include "IPixelShader.h"

template<class T> class ShaderObject;



namespace ShaderList {

	namespace PixelShaders {


	class PS_DeferredShading : public IPixelShader {

	protected:
		static ShaderObject<ID3D11PixelShader> pPS;

		

		//Interfaces Slot
		static UINT mNumInterfaces;
		static UINT mISlot_GBufferManager;
		static UINT mISlot_ShadowManager;
		static UINT mISlot_SAVSM_Manager;
		static UINT mISlot_SSAOManager;
		static UINT mISlot_OutputManager;

		//Shader Class Instances
		static ComPtr<ID3D11ClassInstance> pSClass_GBufferManager_Base;
		static ComPtr<ID3D11ClassInstance> pSClass_ShadowManager_BasicShadowMap;
		static ComPtr<ID3D11ClassInstance> pSClass_ShadowManager_PCF;
		static ComPtr<ID3D11ClassInstance> pSClass_ShadowManager_PCSS;
		static ComPtr<ID3D11ClassInstance> pSClass_ShadowManager_VSM;
		static ComPtr<ID3D11ClassInstance> pSClass_ShadowManager_SAVSM;
		static ComPtr<ID3D11ClassInstance> pSClass_ShadowManager_MSM;
		static ComPtr<ID3D11ClassInstance> pSClass_ShadowManager_OSSSS;

		//SAVSM Class Instance
		static ComPtr<ID3D11ClassInstance> pSClass_SAVSM_Manager_Bilinear;
		static ComPtr<ID3D11ClassInstance> pSClass_SAVSM_Manager_Linear;

		//SSAO Class Instances
		static ComPtr<ID3D11ClassInstance> pSClass_SSAOManager_NoSSAO;
		static ComPtr<ID3D11ClassInstance> pSClass_SSAOManager_Default;

		//Output Class Instances
		static ComPtr<ID3D11ClassInstance> pSClass_OutputManager_Backbuffer;
		static ComPtr<ID3D11ClassInstance> pSClass_OutputManager_PreProcess;


		//ClassInstaceArray
		static ID3D11ClassInstance** pDynamicClassLinkage;


	public:

		//Configuration Variables
		static int mPCFSamplesCount;
		static int mPCSSSamplesCount;
		static int mPCSSBlockerSearchSampleCount;
		static float mGlobalAmbient;

		static void InitStatics(bool fromHLSLFile = false);
		static concurrency::task<void> CompileAsync();
		//void Init();

		static void SetClassInstace_GBufferManager_Basic();
		static void SetClassInstace_ShadowManager_BasicShadowMap();
		static void SetClassInstace_ShadowManager_PCF();
		static void SetClassInstace_ShadowManager_PCSS();
		static void SetClassInstace_ShadowManager_VSM();
		static void SetClassInstace_ShadowManager_SAVSM();
		static void SetClassInstace_ShadowManager_MSM();
		static void SetClassInstace_ShadowManager_OSSSS();
		
		static void SetClassInstance_SAVSM_Bilinear();
		static void SetClassInstance_SAVSM_Linear();

		static void SetClassInstance_SSAOManager_NoSSAO();
		static void SetClassInstance_SSAOManager_Default();

		static void SetClassInstance_OutputManager_Backbuffer();
		static void SetClassInstance_OutputManager_PreProcess();

		static void SetGlobalAmbient(float globalAmbient);

		static void SetShader(ID3D11DeviceContext* context);

		static void RecompileShader();

		static void Destroy();

	};

	};

}