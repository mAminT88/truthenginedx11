#include "stdafx.h"
#include "Pipeline.h"
#include "Globals.h"

using namespace Globals;

void Pipeline::Init()
{

}


void Pipeline::Destroy()
{
	pInputLayout.Reset();
}


void Pipeline::Apply_Preliminiary(ID3D11DeviceContext* _context)
{
	mVSObject.SetShader(nullptr);
	mPSObject.SetShader(nullptr);
	mDSObject.SetShader(nullptr);
	mHSObject.SetShader(nullptr);
	mGSObject.SetShader(nullptr);

	RenderStates::SetRasterizerState(mRasterizerStateType, _context);
	RenderStates::SetBlendState(mBlendStateType, _context);
	RenderStates::SetDepthStencilState(mDepthStateType, _context);
}


void Pipeline::Apply_Preliminiary(ComPtr<ID3D11Buffer> _vertexBuffer, ComPtr<ID3D11Buffer> _indexBuffer, ID3D11DeviceContext* _context)
{
	gContext->IASetInputLayout(pInputLayout.Get());
	gContext->IASetPrimitiveTopology(mPrimitiveTopology);
	gContext->IASetVertexBuffers(0, (_vertexBuffer != nullptr ? 1 : 0), _vertexBuffer.GetAddressOf(), &mVertexStride, &mVertexOffset);
	gContext->IASetIndexBuffer(_indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);


	mVSObject.SetShader(nullptr);
	mPSObject.SetShader(nullptr);
	mDSObject.SetShader(nullptr);
	mHSObject.SetShader(nullptr);
	mGSObject.SetShader(nullptr);

	RenderStates::SetRasterizerState(mRasterizerStateType, _context);
	RenderStates::SetBlendState(mBlendStateType, _context);
	RenderStates::SetDepthStencilState(mDepthStateType, _context);
}


void Pipeline::ChangeRenderStates(RENDER_STATE_RS _rs, RENDER_STATE_BS _bs, RENDER_STATE_DS _ds)
{
	mRasterizerStateType = _rs;
	mBlendStateType = _bs;
	mDepthStateType = _ds;
}


