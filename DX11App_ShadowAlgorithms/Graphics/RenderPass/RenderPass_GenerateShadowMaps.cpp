#include "stdafx.h"
#include "RenderPass_GenerateShadowMaps.h"
#include "Globals.h"

#include "Graphics/TexViews.h"
#include "Graphics/RenderStates.h"
#include "Graphics/ConstantBuffer.h"
#include "Graphics/ShaderSlots_DeferredShading.h"

#include "Objects/Mesh.h"

#include "Utility/GpuProfiler.h"
#include <Graphics\ShaderList\VS_BuildShadowMap.h>

using namespace Globals;

//void RenderPass_GenerateShadowMaps::InitPipelines()
//{
//	mPiplineList_NormalShadowMap.Init();
//}

void RenderPass_GenerateShadowMaps::InitViews()
{
	mViewport.Height = gShadowMapResolution;
	mViewport.Width = gShadowMapResolution;
	mViewport.MaxDepth = 1.0f;
	mViewport.MinDepth = 0.0f;
	mViewport.TopLeftX = 0.0f;
	mViewport.TopLeftY = 0.0f;

	int shadowLightNum = gLights.GetShadowMapCount();

	

	mTexView_ShadowMap_DSV->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_DEPTH_STENCIL
		, gShadowMapResolution, gShadowMapResolution
		, DXGI_FORMAT_D32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, shadowLightNum
		, 1, 1, 0, 0, true);


}

void RenderPass_GenerateShadowMaps::ReleaseViews()
{

	mTexView_ShadowMap_DSV->Release();


}

void RenderPass_GenerateShadowMaps::InitBuffers()
{

	mCBPerObject.reset(new ConstantBuffer<CB_PER_OBJECT>());
	mCBperLight.reset(new ConstantBuffer<CB_PER_LIGHT>());
	mCBBoneTransforms.reset(new ConstantBuffer<CB_VS_BONETRANSFORMS>());


	mCBPerObject->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PEROBJECT, CONSTANT_BUFFER_VS, &mDataCBPerObject, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
	mCBPerObject->SetConstantBuffer(gContext.Get());

	mCBperLight->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PERLIGHT, CONSTANT_BUFFER_VS, &mDataCBPerLight, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
	mCBperLight->SetConstantBuffer(gContext.Get());

	mCBBoneTransforms->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_BONETRANSFORMS, CONSTANT_BUFFER_VS, &mDataCBBoneTransforms, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
	mCBBoneTransforms->SetConstantBuffer(gContext.Get());

	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
}

void RenderPass_GenerateShadowMaps::ReleaseBuffers()
{

	mCBperLight->Release();
	mCBPerObject->Release();
	mCBBoneTransforms->Release();

	mCBperLight.reset();
	mCBPerObject.reset();
	mCBBoneTransforms.reset();

}

void RenderPass_GenerateShadowMaps::UpdateCBuffer()
{



}

RenderPass_GenerateShadowMaps::RenderPass_GenerateShadowMaps() {
	mTexView_ShadowMap_DSV.reset(new TexViews());
}

RenderPass_GenerateShadowMaps::~RenderPass_GenerateShadowMaps() = default;

RenderPass_GenerateShadowMaps::RenderPass_GenerateShadowMaps(RenderPass_GenerateShadowMaps&&) = default;

RenderPass_GenerateShadowMaps& RenderPass_GenerateShadowMaps::operator=(RenderPass_GenerateShadowMaps&&) = default;

void RenderPass_GenerateShadowMaps::Init()
{
}

void RenderPass_GenerateShadowMaps::Draw3D()
{

	//gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_GenerateShadowMap");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());


	gContext->RSSetViewports(1, &mViewport);


	gContext->PSSetShader(nullptr, nullptr, 0);


	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_DEFAULT, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_SHADOWMAP, gContext.Get());


	auto Models_Basic32 = gModelManager.GetBasicModels();

	auto Models_Skinned = gModelManager.GetSkinnedModels();


	for (const auto iLight : gLights.mCollectionILights)
	{

		if (iLight->GetILightData().CastShadow)
		{

			auto dsv = mTexView_ShadowMap_DSV->GetDSV(iLight->GetILightData().ShadowMapID);
			gContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);

			gContext->OMSetRenderTargets(0, nullptr, dsv);

			mDataCBPerLight.gLightVP = iLight->GetILightData().ViewProj;
			mCBperLight->Update(gContext.Get(), &mDataCBPerLight, sizeof(XMFLOAT4X4));

			gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			if (Models_Basic32->size() > 0)
			{
				gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
				gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);


				ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(gContext.Get());

				for (auto model : *Models_Basic32)
				{

					/*if (iLight->BoundingBoxContainment(model->pMesh->AABB) == ContainmentType::DISJOINT)
					{
						continue;
					}*/



					mDataCBPerObject.gWorld = model->mWorldMatrix;
					mCBPerObject->Update(gContext.Get());

					for (auto mesh : model->mMeshes)
					{
						//mPiplineList_NormalShadowMap.Apply_Preliminiary(mesh->mVertexType);

						mesh->Draw(gContext.Get());
					}


				}

			}

			if (Models_Skinned->size() > 0)
			{
				gContext->IASetInputLayout(Vertex::IL_Skinned.Get());
				gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);


				ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(gContext.Get());

				for (auto skModel : *Models_Skinned)
				{

					mCBBoneTransforms->Update(gContext.Get(), skModel->GetBoneTransforms()->data(), skModel->GetBoneTransforms()->size() * sizeof(XMFLOAT4X4));

					mDataCBPerObject.gWorld = skModel->mWorldMatrix;
					mCBPerObject->Update(gContext.Get());

					for (auto mesh : skModel->mMeshes)
					{

						//mPiplineList_NormalShadowMap.Apply_Preliminiary(mesh->mVertexType);

						mesh->Draw(gContext.Get());
					}

				}
			}

		}
	}

	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	//gD3DUserDefinedAnnotation->EndEvent();

}


void RenderPass_GenerateShadowMaps::DrawTxt()
{

}



void RenderPass_GenerateShadowMaps::Destroy()
{

}

void RenderPass_GenerateShadowMaps::DestroyGPUResources()
{
	ReleaseBuffers();
	ReleaseViews();

	GPUResourceInitialized = false;
}

void RenderPass_GenerateShadowMaps::InitGPUResources()
{
	if (!GPUResourceInitialized)
	{

		InitBuffers();
		InitViews();

		GPUResourceInitialized = true;

	}
}

void RenderPass_GenerateShadowMaps::RecompileShaders()
{

}

std::string RenderPass_GenerateShadowMaps::GPUTime_String()
{
	return "Render Shadow Map Time : " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}

ID3D11ShaderResourceView* RenderPass_GenerateShadowMaps::GetShadowMapsDepth_SRV() const
{
	return mTexView_ShadowMap_DSV->GetSRV();
}

const TexViews* RenderPass_GenerateShadowMaps::GetTexViewShadowMap() const noexcept
{
	return mTexView_ShadowMap_DSV.get();
}

//void RenderPass_GenerateShadowMaps::SetObjects(std::vector<Model*>* Models)
//{
//	mModels = Models;
//}

