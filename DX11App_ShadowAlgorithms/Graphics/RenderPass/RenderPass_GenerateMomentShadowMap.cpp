#include "stdafx.h"

#include "RenderPass_GenerateMomentShadowMap.h"
#include "Globals.h"

#include "Graphics//RenderStates.h"
#include "Graphics/TexViews.h"
#include "Graphics/Colors.h"
#include "Graphics/ConstantBuffer.h"
#include "Graphics/ShaderSlots_DeferredShading.h"

#include "Objects/Mesh.h"

#include "Utility/GpuProfiler.h"

/////Shaders
#include "Graphics\ShaderList\CS_GenerateSAT.h"
#include "Graphics\ShaderList\PS_GenerateMomentShadowMap.h"
#include "Graphics\ShaderList\VS_BuildShadowMap.h"
#include "Graphics\ShaderList\VS_Render2D.h"
#include <Graphics\ShaderList\PS_GenerateSAT_UINT.h>


using namespace Globals;

//void RenderPass_GenerateMomentShadowMap::InitPipelines()
//{
//	mPipeline_GenerateMomentShadowMap.Init();
//	//mPipeline_SeperableGaussianFilter.Init();
//	mPipeline_GenerateSAT.Init();mTexView_DSV
//}

void RenderPass_GenerateMomentShadowMap::InitViews()
{
	mViewport.Height = gShadowMapResolution;
	mViewport.Width = gShadowMapResolution;
	mViewport.MaxDepth = 1.0f;
	mViewport.MinDepth = 0.0f;
	mViewport.TopLeftX = 0.0f;
	mViewport.TopLeftY = 0.0f;



	mTexView_DSV->Init(D3D11_BIND_DEPTH_STENCIL
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_D32_FLOAT
		, D3D11_USAGE_DEFAULT);

	auto shadowLightNum = gLights.GetShadowMapCount();

	mTexView_MomentShadowMap->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32G32B32A32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, shadowLightNum
		, 1
		, 1
		, 0
		, 0
		, true);

	mTexView_SAT_Temp->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32G32B32A32_UINT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, 1
		, 1
		, 1
		, 0
		, 0
		, true);

	mTexView_SAT_UINT->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32G32B32A32_UINT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, shadowLightNum
		, 1
		, 1
		, 0
		, 0
		, true);

	mTexView_SAT_Temp_UAV->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32G32B32A32_UINT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, 1
		, 1
		, 1
		, 0
		, 0
		, true);

	mTexView_SAT_UINT_UAV->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32G32B32A32_UINT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, shadowLightNum
		, 1
		, 1
		, 0
		, 0
		, true);
}

void RenderPass_GenerateMomentShadowMap::ReleaseViews()
{
	mTexView_DSV->Release();
	mTexView_MomentShadowMap->Release();
	//mTexView_TempFilteredMomentShadowMap.Release();
	mTexView_SAT_Temp->Release();
	mTexView_SAT_UINT->Release();
	mTexView_SAT_Temp_UAV->Release();
	mTexView_SAT_UINT_UAV->Release();

}

void RenderPass_GenerateMomentShadowMap::InitBuffers()
{

	mCB_VS_perLight.reset(new ConstantBuffer<CB_VS_PER_LIGHT>());
	mCB_VS_PerObject.reset(new ConstantBuffer<CB_VS_PER_OBJECT>());
	mCB_PS_GenerateSAT.reset(new ConstantBuffer<CB_PS_GENERATESAT>());
	mCB_CS_GenerateSAT.reset(new ConstantBuffer<CB_CS_GENERATESAT>());


	mCB_VS_perLight->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PERLIGHT
		, CONSTANT_BUFFER_VS
		, &mDataCBPerLight
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_VS_perLight->SetConstantBuffer(gContext.Get());


	mCB_VS_PerObject->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PEROBJECT
		, CONSTANT_BUFFER_VS
		, &mDataCBPerObject
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_VS_PerObject->SetConstantBuffer(gContext.Get());

	/*mCB_PS_GaussianFilter_PerLight.InitConstantBuffer(SHADER_SLOTS_PS_CB_VSM_FILTERSHADOWMAP
		, CONSTANT_BUFFER_PS
		, &mData_CB_GaussianFilter_PerLight
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_PS_GaussianFilter_PerLight.SetConstantBuffer(gContext.Get());*/

	mCB_PS_GenerateSAT->InitConstantBuffer(SHADER_SLOTS_PS_CB_GENERATESAT
		, CONSTANT_BUFFER_PS
		, &mData_CB_GenerateSAT
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_PS_GenerateSAT->SetConstantBuffer(gContext.Get());

	mCB_CS_GenerateSAT->InitConstantBuffer(SHADER_SLOTS_CS_CB_GENERATESAT_PERLIGHT
		, CONSTANT_BUFFER_CS
		, &mData_CB_GenerateSAT_CS
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_CS_GenerateSAT->SetConstantBuffer(gContext.Get());

	//mData_CB_GenerateSAT_CS.gFixedPrecision_FloatToInt = (std::pow(2, 32) - 1.0f) / 784.0f;

	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));

}

void RenderPass_GenerateMomentShadowMap::ReleaseBuffers()
{
	mCB_VS_PerObject->Release();
	mCB_VS_perLight->Release();
	mCB_PS_GenerateSAT->Release();
	mCB_CS_GenerateSAT->Release();

	mCB_VS_PerObject.reset();
	mCB_VS_perLight.reset();
	mCB_PS_GenerateSAT.reset();
	mCB_CS_GenerateSAT.reset();

	mGpuTimeStampDB->Destroy();
}

ID3D11RenderTargetView* RenderPass_GenerateMomentShadowMap::GetSATRTV(int i, int rtv_index)
{
	switch (i % 2)
	{
	case 0:
		return mTexView_SAT_Temp->GetRTV();
		break;
	case 1:
		return mTexView_SAT_UINT->GetRTV(rtv_index);
		break;
	}
}

ID3D11ShaderResourceView* RenderPass_GenerateMomentShadowMap::GetSATSRV(int i)
{
	switch (i % 2)
	{
	case 0:
		return mTexView_SAT_UINT->GetSRV();
		break;
	case 1:
		return mTexView_SAT_Temp->GetSRV();
		break;
	}
}

RenderPass_GenerateMomentShadowMap::RenderPass_GenerateMomentShadowMap()
{
	mTexView_DSV.reset(new TexViews());
	mTexView_MomentShadowMap.reset(new TexViews());
	mTexView_SAT_Temp.reset(new TexViews());
	mTexView_SAT_UINT.reset(new TexViews());
	mTexView_SAT_Temp_UAV.reset(new TexViews());
	mTexView_SAT_UINT_UAV.reset(new TexViews());
}

RenderPass_GenerateMomentShadowMap::~RenderPass_GenerateMomentShadowMap() = default;

RenderPass_GenerateMomentShadowMap::RenderPass_GenerateMomentShadowMap(RenderPass_GenerateMomentShadowMap&&) = default;

RenderPass_GenerateMomentShadowMap& RenderPass_GenerateMomentShadowMap::operator=(RenderPass_GenerateMomentShadowMap&&) = default;

void RenderPass_GenerateMomentShadowMap::Init()
{
	//InitPipelines();

	float a = std::log2f(gShadowMapResolution);
	float b = std::log2f(SampleCountPerSATPass);

	SATPassCount = ceil(a / b);

	SAT_ThreadGroupCountX = ceil(gShadowMapResolution / ShaderList::ComputeShaders::CS_GenerateSAT_Vert::GetThreadGroupSizeX());
	SAT_ThreadGroupCountY = ceil(gShadowMapResolution / ShaderList::ComputeShaders::CS_GenerateSAT_Horz::GetThreadGroupSizeY());
}

void RenderPass_GenerateMomentShadowMap::Draw3D()
{

	gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_GenerateMomentShadowMap");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_DEFAULT, gContext.Get());

	gContext->RSSetViewports(1, &mViewport);

	auto dsv = mTexView_DSV->GetDSV();

	ID3D11RenderTargetView* rtv = nullptr;

	gContext->PSSetShader(nullptr, nullptr, 0);

	for (auto& directlight : gLights.mDirectionalLights)
	{

		ShaderList::PixelShaders::PS_GenerateMomentShadowMap::SetClassInstance_ILight_Direct(directlight.GetIndex());
		ShaderList::PixelShaders::PS_GenerateMomentShadowMap::SetShader(gContext.Get());

		int shadowMapID = directlight.GetILightData().ShadowMapID;

		rtv = mTexView_MomentShadowMap->GetRTV(shadowMapID);

		gContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
		gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f , 1.0f , 1.0f });

		gContext->OMSetRenderTargets(1, &rtv, dsv);

		mDataCBPerLight.gLightVP = directlight.GetILightData().ViewProj;
		mDataCBPerLight.gZNearFar = XMFLOAT2(directlight.GetILightData().zNear, directlight.GetILightData().zFar);
		mCB_VS_perLight->Update(gContext.Get());


		auto Models_Basic32 = gModelManager.GetBasicModels();


		if (Models_Basic32->size() > 0)
		{

			gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
			gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(gContext.Get());

			for (auto model : *Models_Basic32)
			{
				/*if (directlight.BoundingBoxContainment(obj.pMesh->AABB) == ContainmentType::DISJOINT)
				{
					continue;
				}*/

				mDataCBPerObject.gWorld = model->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : model->mMeshes)
				{
					/*mPipeline_GenerateMomentShadowMap.Apply_Preliminiary(mesh->mVertexType);*/

					mesh->Draw(gContext.Get());
				}

			}
		}

		auto SkinnedModels = gModelManager.GetSkinnedModels();

		if (SkinnedModels->size() > 0)
		{

			gContext->IASetInputLayout(Vertex::IL_Skinned.Get());
			gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(gContext.Get());

			for (auto skModel : *SkinnedModels)
			{

				mDataCBPerObject.gWorld = skModel->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : skModel->mMeshes)
				{

					//mPipeline_GenerateMomentShadowMap.Apply_Preliminiary(mesh->mVertexType);

					mesh->Draw(gContext.Get());
				}

			}
		}

	}

	for (auto& spotlight : gLights.mSpotLights)
	{

		ShaderList::PixelShaders::PS_GenerateMomentShadowMap::SetClassInstance_ILight_Spot(spotlight.GetIndex());
		ShaderList::PixelShaders::PS_GenerateMomentShadowMap::SetShader(gContext.Get());

		int shadowMapID = spotlight.GetILightData().ShadowMapID;

		rtv = mTexView_MomentShadowMap->GetRTV(shadowMapID);

		gContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
		gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f , 1.0f , 1.0f });

		gContext->OMSetRenderTargets(1, &rtv, dsv);

		mDataCBPerLight.gLightVP = spotlight.GetILightData().ViewProj;
		mDataCBPerLight.gZNearFar = XMFLOAT2(spotlight.GetILightData().zNear, spotlight.GetILightData().zFar);
		mCB_VS_perLight->Update(gContext.Get());


		auto Models_Basic32 = gModelManager.GetBasicModels();

		if (Models_Basic32->size() > 0)
		{

			gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
			gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
			gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(gContext.Get());

			for (auto model : *Models_Basic32)
			{
				/*if (spotlight.BoundingBoxContainment(obj.pMesh->AABB) == ContainmentType::DISJOINT)
				{
					continue;
				}*/

				mDataCBPerObject.gWorld = model->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : model->mMeshes)
				{
					/*mPipeline_GenerateMomentShadowMap.Apply_Preliminiary(mesh->mVertexType);*/

					mesh->Draw(gContext.Get());
				}

			}
		}

		auto SkinnedModels = gModelManager.GetSkinnedModels();

		if (SkinnedModels->size() > 0)
		{

			gContext->IASetInputLayout(Vertex::IL_Skinned.Get());
			gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(gContext.Get());

			for (auto skModel : *SkinnedModels)
			{

				mDataCBPerObject.gWorld = skModel->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : skModel->mMeshes)
				{

					//mPipeline_GenerateMomentShadowMap.Apply_Preliminiary(mesh->mVertexType);

					mesh->Draw(gContext.Get());
				}

			}
		}

	}


	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());

	if (!mUseComputeShaderForSAT)
	{
		ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
		ShaderList::VertexShaders::VS_Render2D::SetShader(gContext.Get());


		for (int i = 0; i < gLights.GetShadowMapCount(); ++i)
		{
			ID3D11ShaderResourceView* srv;
			//
			//Run the first horizontal pass separately, because we need to sample from float4 moments texture and write on UINT4 RTV,
			//on other iteration on generating SAT, we sample from UINT4 Texture and write on UINT4 RTV
			//
			ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetClassInstace_SAT_UINT_Horizontal_0();
			ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetShader(gContext.Get());

			rtv = GetSATRTV(0, i);

			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			mTexView_MomentShadowMap->BindSRV_PS(0);

			mData_CB_GenerateSAT.gShadowMapID = i;

			gContext->DrawIndexed(6, 0, 0);

			ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetClassInstace_SAT_UINT_Horizontal();
			ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetShader(gContext.Get());

			for (int j = 1; j < SATPassCount; ++j)
			{

				mData_CB_GenerateSAT.gIteration = j;
				mCB_PS_GenerateSAT->Update(gContext.Get());

				rtv = GetSATRTV(j, i);

				gContext->OMSetRenderTargets(1, &rtv, nullptr);

				srv = GetSATSRV(j);

				gContext->PSSetShaderResources(1, 1, &srv);

				gContext->DrawIndexed(6, 0, 0);

			}

			ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetClassInstace_SAT_UINT_Vertical();
			ShaderList::PixelShaders::PS_GenerateSAT_UINT::SetShader(gContext.Get());

			for (int j = 0; j < SATPassCount; ++j)
			{

				mData_CB_GenerateSAT.gIteration = j;
				mCB_PS_GenerateSAT->Update(gContext.Get());

				rtv = GetSATRTV(j, i);

				gContext->OMSetRenderTargets(1, &rtv, nullptr);

				srv = GetSATSRV(j);

				gContext->PSSetShaderResources(1, 1, &srv);

				gContext->DrawIndexed(6, 0, 0);

			}
		}
	}
	else
	{

		ID3D11RenderTargetView* rtvArray[1] = { nullptr };
		gContext->OMSetRenderTargets(1, rtvArray, nullptr);

		UINT d = 0;
		ID3D11UnorderedAccessView* uav;

		for (int i = 0; i < gLights.GetShadowMapCount(); ++i)
		{

			mData_CB_GenerateSAT_CS.gShadowMapID = i;

			mCB_CS_GenerateSAT->Update(gContext.Get());

			ShaderList::ComputeShaders::CS_GenerateSAT_Horz::SetShader(gContext.Get());
			//mPipeline_CS_GenerateSAT.Apply_Preliminiary_Horz();

			uav = mTexView_SAT_Temp_UAV->GetUAV();


			gContext->CSSetUnorderedAccessViews(1, 1, &uav, &d);

			gContext->CSSetShaderResources(0, 1, mTexView_MomentShadowMap->GetSRV_AddressOf());

			gContext->Dispatch(1, SAT_ThreadGroupCountY, 1);

			ShaderList::ComputeShaders::CS_GenerateSAT_Vert::SetShader(gContext.Get());
			//mPipeline_CS_GenerateSAT.Apply_Preliminiary_Vert();

			uav = mTexView_SAT_UINT_UAV->GetUAV();

			gContext->CSSetUnorderedAccessViews(1, 1, &uav, &d);

			gContext->CSSetShaderResources(1, 1, mTexView_SAT_Temp_UAV->GetSRV_AddressOf());

			gContext->Dispatch(SAT_ThreadGroupCountX, 1, 1);

		}

		uav = nullptr;

		gContext->CSSetUnorderedAccessViews(1, 1, &uav, &d);

	}

	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	gD3DUserDefinedAnnotation->EndEvent();

}

void RenderPass_GenerateMomentShadowMap::Draw2D()
{

}

void RenderPass_GenerateMomentShadowMap::DrawTxt()
{

}

void RenderPass_GenerateMomentShadowMap::Update()
{

}

void RenderPass_GenerateMomentShadowMap::Destroy()
{

}

void RenderPass_GenerateMomentShadowMap::DestroyGPUResources()
{
	ReleaseBuffers();
	ReleaseViews();

	GPUResourceInitialized = false;
}

void RenderPass_GenerateMomentShadowMap::InitGPUResources()
{
	if (!GPUResourceInitialized)
	{
		InitBuffers();
		InitViews();

		GPUResourceInitialized = true;
	}
}

void RenderPass_GenerateMomentShadowMap::RecompileShaders()
{

}

//void RenderPass_GenerateMomentShadowMap::SetObjects(std::vector<Model*>* Models)
//{
//	mModels = Models;
//}

ID3D11ShaderResourceView* RenderPass_GenerateMomentShadowMap::GetMomentShadowMap_SRV() const
{
	return mTexView_MomentShadowMap->GetSRV();
}

ID3D11ShaderResourceView* RenderPass_GenerateMomentShadowMap::GetSAT4Moments_UINT_SRV() const
{
	if (mUseComputeShaderForSAT)
	{
		return mTexView_SAT_UINT_UAV->GetSRV();
	}
	else
	{
		return mTexView_SAT_UINT->GetSRV();
	}
}

void RenderPass_GenerateMomentShadowMap::ChangeSATVersion(bool useComputeVersion)
{
	mUseComputeShaderForSAT = useComputeVersion;
}


const TexViews* RenderPass_GenerateMomentShadowMap::GetTexViewSAT_UINT() const noexcept
{
	return mTexView_SAT_UINT_UAV.get();
}

std::string RenderPass_GenerateMomentShadowMap::GPUTime_String()
{
	return "Generate Moment Shadow Map's Render Time : " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}
