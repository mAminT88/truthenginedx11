#pragma once

#include "IRenderPass.h"

class TexViews;
class GPUTimeStamp_DualBuffer;
class SRVCollector;
class RTVCollector;
class ILight;

template<class T> class ConstantBuffer;

class RenderPass_OSSSS_GenerateShadowData : public IRenderPass
{

public:

	RenderPass_OSSSS_GenerateShadowData();
	~RenderPass_OSSSS_GenerateShadowData();

	RenderPass_OSSSS_GenerateShadowData(RenderPass_OSSSS_GenerateShadowData&&);
	RenderPass_OSSSS_GenerateShadowData& operator=(RenderPass_OSSSS_GenerateShadowData&&);

	const TexViews* pTexViewDepthMap;
	const TexViews* pTexViewShadowMap;
	const TexViews* pTexViewNormalMap;

	virtual void Init() override;
	virtual void InitGPUResources() override;
	virtual void DestroyGPUResources() override;
	virtual void Draw3D() override;
	virtual void DrawDeferred() override;
	virtual void Draw2D() override;
	virtual void DrawTxt() override;
	virtual void Update() override;
	virtual void Destroy() override;
	virtual void CaptureFrame() override;
	virtual void RecompileShaders() override;
	
	virtual void SetSRVs();

	virtual void ProfileGPUTime(float GPUTime) override;

	virtual std::string GPUTime_String() override;

	ID3D11ShaderResourceView* GetSRV_Shadows() const;


	virtual void RenderConfigurationUI() override;

	virtual void SetDownSamplingScale(int sfactor);

	virtual void SetDownSampling(bool active);

	const TexViews* GetTexViewShadows() const noexcept;


protected:	

	D3D11_VIEWPORT mViewPort_DownSampling;

	std::unique_ptr<GPUTimeStamp_DualBuffer> mGpuTimeStampDB_1;
	std::unique_ptr<GPUTimeStamp_DualBuffer> mGpuTimeStampDB_2;

	
	ComPtr<ID3D11CommandList> mCommandList;


	std::unique_ptr<TexViews> mTexView_Shadows;
	std::unique_ptr<TexViews> mTexView_PenumbraSize;
	std::unique_ptr<TexViews> mTexView_Shadows_Temp;
	std::unique_ptr<TexViews> mTexView_Shadows_DownSampled;
	std::unique_ptr<TexViews> mTexView_Shadows_DownSampled_Temp;



	std::unique_ptr<SRVCollector> mSRVCollector_0;
	std::unique_ptr<SRVCollector> mSRVCollector_1;
	
	std::unique_ptr<RTVCollector> mRTVCollector_0;
	std::unique_ptr<RTVCollector> mRTVCollector_1;

	UINT mVertexOffset = 0;

	int mDownSamplingScale = 4;
	UINT mDownSamplingGroupSizeX;

	bool mDownSampling = false;


	virtual void InitViews() override;


	virtual void ReleaseViews() override;


	virtual void InitBuffers() override;


	virtual void ReleaseBuffers() override;

	//Calculate The direction of the passed light in XZ plane of Eye view sapce
	//void CalcLightDirInViewSpace_XZ(XMFLOAT2& LightDir_Horz, XMFLOAT2& LightDir_Vert, const ILight& light);

};




//////////////////////////////////////////////////////////////////////////
//Screen Space Separable Blocker Search
//////////////////////////////////////////////////////////////////////////



class RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch : public IRenderPass
{

public:

	RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch();
	~RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch();

	RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch(RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch&&);
	RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch& operator=(RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch&&);

	const TexViews* pTexViewDepthMap;
	const TexViews* pTexViewShadowMap;
	const TexViews* pTexViewNormalMap;

	virtual void Init() override;
	virtual void InitGPUResources() override;
	virtual void DestroyGPUResources() override;
	virtual void Draw3D() override;
	virtual void Draw2D() override;
	virtual void DrawTxt() override;
	virtual void Update() override;
	virtual void Destroy() override;
	virtual void CaptureFrame() override;
	virtual void RecompileShaders() override;

	/*virtual void SetSRV(
		ID3D11ShaderResourceView* const ppDepthMap
		, ID3D11ShaderResourceView* const ppShadowMapArray
		, ID3D11ShaderResourceView* const ppGBuffer_Normal);*/

	virtual void SetSRVs();

	virtual void RenderConfigurationUI() override;

	virtual std::string GPUTime_String();

	ID3D11ShaderResourceView* GetSRV_Shadows() const;

	const TexViews* GetTexViewShadows() const noexcept;




protected:


	struct CB_PS_PER_LIGHT
	{
		float gShadowMapID;
		XMFLOAT3 pad;

		XMFLOAT2 gLight2DDirInEye_Horz;
		XMFLOAT2 gLight2DDirInEye_Vert;
	};

	std::unique_ptr<ConstantBuffer<CB_PS_PER_LIGHT>> mCB_PerLight;
	CB_PS_PER_LIGHT mData_CB_PerLight;



	std::unique_ptr<TexViews> mTexView_Shadows;
	std::unique_ptr<TexViews> mTexView_PenumbraSize;
	std::unique_ptr<TexViews> mTexView_Shadows_Temp;
	std::unique_ptr<TexViews> mTexView_AvgBlocker_Temp;



	std::unique_ptr<SRVCollector> mSRVCollector_0;
	std::unique_ptr<SRVCollector> mSRVCollector_1;
	std::unique_ptr<SRVCollector> mSRVCollector_2;

	std::unique_ptr<RTVCollector> mRTVCollector_0;
	std::unique_ptr<RTVCollector> mRTVCollector_1;
	std::unique_ptr<RTVCollector> mRTVCollector_2;

	//PipelineList::Pipeline_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch mPipeline;
	UINT mVertexOffset = 0;

	XMFLOAT4X4 mMatixRotationY_90;


	//virtual void InitPipelines() override;


	virtual void InitViews() override;


	virtual void ReleaseViews() override;


	virtual void InitBuffers() override;


	virtual void ReleaseBuffers() override;

	//Calculate The direction of the passed light in XZ plane of Eye view sapce
	void CalcLightDirInViewSpace_XZ(XMFLOAT2& LightDir_Horz, XMFLOAT2& LightDir_Vert, const ILight& light);


};