#include "stdafx.h"
#include "RenderPass_SSAO.h"
#include "Globals.h"
#include "MathHelper.h"

#include "Graphics/RenderStates.h"
#include "Graphics/TexViews.h"
#include "Graphics/ResourceViewCollectors.h"
#include "Graphics/Colors.h"

#include "Utility/GpuProfiler.h"

/////Shaders
#include "Graphics/ShaderList/PS_BlurBilateralByDepthNormal.h"
#include "Graphics/ShaderList/VS_Render2D.h"
#include "Graphics/ShaderList/PS_SSAO.h"


using namespace Globals;

void RenderPass_SSAO::InitViews()
{
	/*Init Random Vectors On Immutable Texture*/
	D3D11_SUBRESOURCE_DATA initData = { 0 };
	initData.SysMemPitch = 256 * sizeof(DirectX::PackedVector::XMCOLOR);

	DirectX::PackedVector::XMCOLOR color[256 * 256];
	for (int i = 0; i < 256; ++i)
	{
		for (int j = 0; j < 256; ++j)
		{
			XMFLOAT3 v(MathHelper::RandF(), MathHelper::RandF(), MathHelper::RandF());

			color[i * 256 + j] = DirectX::PackedVector::XMCOLOR(v.x, v.y, v.z, 0.0f);
		}
	}

	initData.pSysMem = color;

	mTexView_RandomVectorMap->Init(D3D11_BIND_SHADER_RESOURCE, 256, 256, DXGI_FORMAT_R8G8B8A8_UNORM, D3D11_USAGE_IMMUTABLE, (D3D11_CPU_ACCESS_FLAG)0, 1, 1, 1, 0, 0, false, &initData);

	/*Initialize SSAO Map Texture*/

	mTexView_SSAOMap->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R32_FLOAT);

	mTexView_SSAOMap_Temp->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R32_FLOAT);



}

void RenderPass_SSAO::ReleaseViews()
{
	/*Release Textures*/
	mTexView_SSAOMap->Release();
	mTexView_RandomVectorMap->Release();
	mTexView_SSAOMap_Temp->Release();
}

void RenderPass_SSAO::InitBuffers()
{
	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
}

void RenderPass_SSAO::ReleaseBuffers()
{
	mGpuTimeStampDB->Destroy();
	mGpuTimeStampDB.reset();
}

RenderPass_SSAO::RenderPass_SSAO() {
	mTexView_RandomVectorMap.reset(new TexViews());
	mTexView_SSAOMap.reset(new TexViews());
	mTexView_SSAOMap_Temp.reset(new TexViews());
}

RenderPass_SSAO::~RenderPass_SSAO() = default;

RenderPass_SSAO::RenderPass_SSAO(RenderPass_SSAO&&) = default;

RenderPass_SSAO& RenderPass_SSAO::operator=(RenderPass_SSAO&&) = default;

ID3D11ShaderResourceView* RenderPass_SSAO::GetSSAOMap()
{
	return mTexView_SSAOMap->GetSRV();
}

const TexViews* RenderPass_SSAO::GetTexViewSSAO() const noexcept
{
	return mTexView_SSAOMap.get();
}

void RenderPass_SSAO::Draw3D()
{
	gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_SSAO");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());

	gContext->RSSetViewports(1, &gViewPort);


	gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
	gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

	auto rtv = mTexView_SSAOMap->GetRTV();

	gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	gContext->OMSetRenderTargets(1, &rtv, nullptr);

	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	ShaderList::VertexShaders::VS_Render2D::SetShader(gContext.Get());


	ShaderList::PixelShaders::PS_SSAO::SetShader(gContext.Get());


	mSRVCollector->BindSRVs_PS(gContext, 0);

	gContext->DrawIndexed(6, 0, 0);


	ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::SetClassInstance_ApplyBlur_Horz();
	ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::SetShader(gContext.Get());


	rtv = mTexView_SSAOMap_Temp->GetRTV();
	gContext->ClearRenderTargetView(rtv, Colors::White);
	gContext->OMSetRenderTargets(1, &rtv, nullptr);

	gContext->PSSetShaderResources(0, 1, mTexView_SSAOMap->GetSRV_AddressOf());

	gContext->DrawIndexed(6, 0, 0);

	ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::SetClassInstance_ApplyBlur_Vert();
	ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::SetShader(gContext.Get());

	rtv = mTexView_SSAOMap->GetRTV();
	gContext->ClearRenderTargetView(rtv, Colors::White);
	gContext->OMSetRenderTargets(1, &rtv, nullptr);

	gContext->PSSetShaderResources(0, 1, mTexView_SSAOMap_Temp->GetSRV_AddressOf());

	gContext->DrawIndexed(6, 0, 0);

	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	gD3DUserDefinedAnnotation->EndEvent();
}

void RenderPass_SSAO::DestroyGPUResources()
{
	ReleaseViews();
	ReleaseBuffers();

	GPUResourceInitialized = false;
}

void RenderPass_SSAO::InitGPUResources()
{
	if (!GPUResourceInitialized)
	{
		InitViews();
		InitBuffers();

		GPUResourceInitialized = true;
	}

	SetSRVs();
}

void RenderPass_SSAO::RecompileShaders()
{
	ShaderList::PixelShaders::PS_SSAO::RecompileShader();
	ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::RecompileShader();
}

std::string RenderPass_SSAO::GPUTime_String()
{
	return "SSAO Render Time: " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}

void RenderPass_SSAO::RenderConfigurationUI()
{
	ImGui::Begin("Settings: SSAO");

	ImGui::DragFloat("Blur Width", &ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::BlurWidth, 1.0f, 3.0f, 200.0f);
	ImGui::DragFloat("Depth Difference", &ShaderList::PixelShaders::PS_BlurBilateralByDepthNormal::DepthDifference, 1.0f, 0.01f);

	ImGui::End();
}

//void RenderPass_SSAO::SetSRVs(ID3D11ShaderResourceView* const depthMap, ID3D11ShaderResourceView* const normalMap)
//{
//	mSRVCollector->ClearAndAddSRV(mTexView_RandomVectorMap->GetSRV(), 3);
//	mSRVCollector->AddSRV(depthMap);
//	mSRVCollector->AddSRV(normalMap);
//}

void RenderPass_SSAO::SetSRVs()
{
	mSRVCollector.reset(new SRVCollector());

	mSRVCollector->ClearAndAddSRV(mTexView_RandomVectorMap->GetSRV(), 3);
	mSRVCollector->AddSRV(pTexViews_DepthMap->GetSRV());
	mSRVCollector->AddSRV(pTexViews_NormalMap->GetSRV());
}

