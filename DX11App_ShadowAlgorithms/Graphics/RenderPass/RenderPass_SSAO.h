#pragma once

#include "IRenderPass.h"

class SRVCollector;
class TexViews;

class RenderPass_SSAO : public IRenderPass_SSAO
{
	std::unique_ptr<TexViews> mTexView_SSAOMap;
	std::unique_ptr<TexViews> mTexView_SSAOMap_Temp;
	std::unique_ptr<TexViews> mTexView_RandomVectorMap;


	std::unique_ptr<SRVCollector> mSRVCollector;

	UINT mVertexOffset = 0;



	//void InitPipelines() override;
	void InitViews() override;
	void ReleaseViews() override;

	void InitBuffers() override;
	void ReleaseBuffers() override;

public:

	RenderPass_SSAO();
	~RenderPass_SSAO();

	RenderPass_SSAO(RenderPass_SSAO&&);
	RenderPass_SSAO& operator=(RenderPass_SSAO&&);

	const TexViews* pTexViews_DepthMap;
	const TexViews* pTexViews_NormalMap;


	virtual void Draw3D() override;
	virtual void DestroyGPUResources() override;
	virtual void InitGPUResources() override;
	virtual void RecompileShaders() override;
	virtual std::string GPUTime_String() override;
	virtual void RenderConfigurationUI() override;

	virtual void SetSRVs();

	virtual ID3D11ShaderResourceView* GetSSAOMap() override;

	const TexViews* GetTexViewSSAO() const noexcept;

};
