﻿#include "stdafx.h"
#include "RenderPass_GenerateCascadedShadowMap.h"
#include "Globals.h"

#include "Graphics/ConstantBuffer.h"
#include "Graphics/TexViews.h"
#include "Graphics/ShaderSlots_DeferredShading.h"
#include "Graphics/RenderStates.h"

#include "Utility/GpuProfiler.h"

/////Shaders
#include "Graphics/ShaderList/VS_BuildShadowMap.h"


using namespace Globals;


void RenderPass_GenerateCascadedShadowMap::InitViews()
{
	mViewport.Height = 512.0f;
	mViewport.Width = 512.0f;
	mViewport.MaxDepth = 1.0f;
	mViewport.MinDepth = 0.0f;
	mViewport.TopLeftX = 0.0f;
	mViewport.TopLeftY = 0.0f;

	const int shadowLightNum = gLights.GetShadowMapCount();


	mTexView_ShadowMap_DSV->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_DEPTH_STENCIL
		, 2048, 2048
		, DXGI_FORMAT_D32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, shadowLightNum
		, 1, 1, 0, 0, true);
}

void RenderPass_GenerateCascadedShadowMap::ReleaseViews()
{
	mTexView_ShadowMap_DSV->Release();
}

void RenderPass_GenerateCascadedShadowMap::InitBuffers()
{
	mCBPerObject.reset(new ConstantBuffer<CB_PER_OBJECT>());
	mCBperLight.reset(new ConstantBuffer<CB_PER_LIGHT>());


	mCBPerObject->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PEROBJECT, CONSTANT_BUFFER_VS, &mDataCBPerObject, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
	mCBPerObject->SetConstantBuffer(gContext.Get());

	mCBperLight->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PERLIGHT, CONSTANT_BUFFER_VS, &mDataCBPerLight, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
	mCBperLight->SetConstantBuffer(gContext.Get());

	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
}

void RenderPass_GenerateCascadedShadowMap::ReleaseBuffers()
{
	mCBPerObject->Release();
	mCBperLight->Release();
}

RenderPass_GenerateCascadedShadowMap::RenderPass_GenerateCascadedShadowMap()
{
	mTexView_ShadowMap_DSV.reset(new TexViews());
}

RenderPass_GenerateCascadedShadowMap::~RenderPass_GenerateCascadedShadowMap() = default;

RenderPass_GenerateCascadedShadowMap::RenderPass_GenerateCascadedShadowMap(
	RenderPass_GenerateCascadedShadowMap&&) = default;

RenderPass_GenerateCascadedShadowMap& RenderPass_GenerateCascadedShadowMap::operator=(
	RenderPass_GenerateCascadedShadowMap&&) = default;

void RenderPass_GenerateCascadedShadowMap::Init()
{
}

void RenderPass_GenerateCascadedShadowMap::Draw3D()
{
	//gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_GenerateShadowMap");

	//mGpuTimeStampDB.SetStart(gCurrentFrame);


	gContext->RSSetViewports(1, &mViewport);


	gContext->PSSetShader(nullptr, nullptr, 0);


	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_DEFAULT, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_SHADOWMAP, gContext.Get());


	auto Models_Basic32 = gModelManager.GetBasicModels();

	auto Models_Skinned = gModelManager.GetSkinnedModels();


	for (const auto iLight : gLights.mCollectionILights)
	{

		if (iLight->GetILightData().CastShadow)
		{

			auto dsv = mTexView_ShadowMap_DSV->GetDSV(iLight->GetILightData().ShadowMapID);
			gContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);

			gContext->OMSetRenderTargets(0, nullptr, dsv);

			mDataCBPerLight.gLightVP = iLight->GetILightData().ViewProj;
			mCBperLight->Update(gContext.Get(), &mDataCBPerLight, sizeof(XMFLOAT4X4));

			gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			if (Models_Basic32->size() > 0)
			{
				gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
				gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);


				ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(gContext.Get());

				for (auto model : *Models_Basic32)
				{

					/*if (iLight->BoundingBoxContainment(model->pMesh->AABB) == ContainmentType::DISJOINT)
					{
						continue;
					}*/



					mDataCBPerObject.gWorld = model->mWorldMatrix;
					mCBPerObject->Update(gContext.Get());

					for (auto mesh : model->mMeshes)
					{

						mesh->Draw(gContext.Get());
					}


				}

			}

			if (Models_Skinned->size() > 0)
			{
				gContext->IASetInputLayout(Vertex::IL_Skinned.Get());
				gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);


				ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(gContext.Get());

				for (auto skModel : *Models_Skinned)
				{

					mDataCBPerObject.gWorld = skModel->mWorldMatrix;
					mCBPerObject->Update(gContext.Get());

					for (auto mesh : skModel->mMeshes)
					{
						mesh->Draw(gContext.Get());
					}

				}
			}

		}
	}

	//mGpuTimeStampDB.SetEnd(gCurrentFrame);

	//gD3DUserDefinedAnnotation->EndEvent();
}

void RenderPass_GenerateCascadedShadowMap::DestroyGPUResources()
{
	ReleaseBuffers();
	ReleaseViews();

	GPUResourceInitialized = false;
}

void RenderPass_GenerateCascadedShadowMap::InitGPUResources()
{
	if (!GPUResourceInitialized)
	{

		InitBuffers();
		InitViews();

		GPUResourceInitialized = true;

	}
}

void RenderPass_GenerateCascadedShadowMap::RecompileShaders()
{
}

std::string RenderPass_GenerateCascadedShadowMap::GPUTime_String()
{
	return "";
}

ID3D11ShaderResourceView* RenderPass_GenerateCascadedShadowMap::GetShadowMapsDepth_SRV() const
{
	return mTexView_ShadowMap_DSV->GetSRV();
}

const TexViews* RenderPass_GenerateCascadedShadowMap::GetTexViewShadowMap() const noexcept
{
	return mTexView_ShadowMap_DSV.get();
}
