#include "stdafx.h"
#include "RenderPass_GenerateGBuffers.h"
#include "D3D.h"
#include "Globals.h"

#include "Graphics/TexViews.h"
#include "Graphics/ResourceViewCollectors.h"
#include "Graphics/RenderStates.h"
#include "Graphics/ConstantBuffer.h"
#include "Graphics/ShaderSlots_DeferredShading.h"
#include "Graphics/material.h"

#include "Utility/GpuProfiler.h"

#include "Objects/Mesh.h"

#include "Graphics\ShaderList\VS_ProcessMesh.h"
#include "Graphics\ShaderList\PS_GenerateGBuffers.h"

#define SRVSLOT_DIFFUSEMAP 0

using namespace Globals;

void RenderPass_GenerateGBuffers::InitViews()
{
	

	mTexView_ColorMap->Init(D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE, gClientHeight, gClientWidth, DXGI_FORMAT_R8G8B8A8_UNORM);
	mRTVCollector->ClearAndAddRTV(mTexView_ColorMap->GetRTV(0), 3);


	mTexView_NormalMap->Init(D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE, gClientHeight, gClientWidth, DXGI_FORMAT_R11G11B10_FLOAT);
	mRTVCollector->AddRTV(mTexView_NormalMap->GetRTV(0));


	mTexView_SpecularMap->Init(D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE, gClientHeight, gClientWidth, DXGI_FORMAT_R16_FLOAT);
	mRTVCollector->AddRTV(mTexView_SpecularMap->GetRTV(0));


	mTexView_DepthMap->Init(D3D11_BIND_DEPTH_STENCIL | D3D11_BIND_SHADER_RESOURCE, gClientHeight, gClientWidth, DXGI_FORMAT_D32_FLOAT);


}

void RenderPass_GenerateGBuffers::InitBuffers()
{
	mCB_VS_Per_Object.reset(new ConstantBuffer<CB_VS_PER_OBJECT>());
	mCB_VS_Per_Mesh.reset(new ConstantBuffer<CB_VS_PER_MESH>());
	mCB_PS_Per_Object.reset(new ConstantBuffer<CB_PS_PER_OBJECT>());
	mCB_VS_BoneTransforms.reset(new ConstantBuffer<CB_VS_BONETRANSFORMS>());


	mCB_VS_Per_Object->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATEGBUFFERS_PEROBJECT
		, CONSTANT_BUFFER_VS
		, &mData_CB_VS_Per_Object
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);
	mCB_VS_Per_Object->SetConstantBuffer(gContext.Get());
	mCB_VS_Per_Object->SetConstantBuffer(mDeferredContext.Get());

	mCB_VS_Per_Mesh->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATEGBUFFERS_PERMESH
		, CONSTANT_BUFFER_VS
		, &mData_CB_VS_Per_Mesh
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);
	mCB_VS_Per_Mesh->SetConstantBuffer(gContext.Get());
	mCB_VS_Per_Mesh->SetConstantBuffer(mDeferredContext.Get());

	mCB_PS_Per_Object->InitConstantBuffer(SHADER_SLOTS_PS_CB_GENERATEGBUFFERS_PEROBJECT
		, CONSTANT_BUFFER_PS
		, &mData_CB_PS_Per_Object
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);
	mCB_PS_Per_Object->SetConstantBuffer(gContext.Get());
	mCB_PS_Per_Object->SetConstantBuffer(mDeferredContext.Get());

	mCB_VS_BoneTransforms->InitConstantBuffer(SHADER_SLOTS_VS_CB_BONETRANSFORMS
		, CONSTANT_BUFFER_VS
		, &mData_VS_BoneTransforms
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);
	mCB_VS_BoneTransforms->SetConstantBuffer(gContext.Get());
	mCB_VS_BoneTransforms->SetConstantBuffer(mDeferredContext.Get());

	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));

}

void RenderPass_GenerateGBuffers::ReleaseBuffers()
{
	mCB_PS_Per_Object->Release();
	mCB_VS_Per_Object->Release();
	mCB_VS_Per_Mesh->Release();

	mCB_VS_Per_Object.reset();
	mCB_VS_Per_Mesh.reset();
	mCB_PS_Per_Object.reset();
	mCB_VS_BoneTransforms.reset();

	mGpuTimeStampDB->Destroy();
}

void RenderPass_GenerateGBuffers::ReleaseViews()
{
	mTexView_ColorMap->Release();
	mTexView_DepthMap->Release();
	mTexView_NormalMap->Release();
	mTexView_SpecularMap->Release();
}

RenderPass_GenerateGBuffers::RenderPass_GenerateGBuffers()
{
	mRTVCollector.reset(new RTVCollector());
	mTexView_ColorMap.reset(new TexViews());
	mTexView_DepthMap.reset(new TexViews());
	mTexView_NormalMap.reset(new TexViews());
	mTexView_SpecularMap.reset(new TexViews());
}

RenderPass_GenerateGBuffers::~RenderPass_GenerateGBuffers() = default;

RenderPass_GenerateGBuffers::RenderPass_GenerateGBuffers(RenderPass_GenerateGBuffers&&) = default;

RenderPass_GenerateGBuffers& RenderPass_GenerateGBuffers::operator=(RenderPass_GenerateGBuffers&&) = default;

ID3D11ShaderResourceView* RenderPass_GenerateGBuffers::GetGBufferColor()
{
	return mTexView_ColorMap->GetSRV();
}

ID3D11ShaderResourceView* RenderPass_GenerateGBuffers::GetGBufferNormal()
{
	return mTexView_NormalMap->GetSRV();
}

ID3D11ShaderResourceView* RenderPass_GenerateGBuffers::GetGBufferDepth()
{
	return mTexView_DepthMap->GetSRV();
}

ID3D11ShaderResourceView* RenderPass_GenerateGBuffers::GetGBufferSpecular()
{
	return mTexView_SpecularMap->GetSRV();
}


void RenderPass_GenerateGBuffers::Init()
{
}

void RenderPass_GenerateGBuffers::Draw3D()
{


	static float c = 1.0f, s = -1.0f;

	if (gSelectedMesh != nullptr)
	{

		c += s * (0.000006f / gDt);
		if (c < 0.0 || c > 1.0f)
		{
			c = c < 0.0 ? 0.0f : 1.0f;
			s *= -1.0f;
		}

	}

	//gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_GenerateGBuffers");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_DEFAULT, gContext.Get());

	mRTVCollector->ClearRTVs(gContext.Get());
	gContext->ClearDepthStencilView(mTexView_DepthMap->GetDSV(), D3D11_CLEAR_DEPTH, 1.0f, 0);
	gContext->OMSetRenderTargets(3, mRTVCollector->GetRTVs(), mTexView_DepthMap->GetDSV());
	gContext->RSSetViewports(1, &gViewPort);

	auto pVector_BasicModels = gModelManager.GetBasicModels();

	if (pVector_BasicModels->size() > 0)
	{

		gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
		gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);


		ShaderList::VertexShaders::VS_ProcessMesh::SetShader_Basic32(gContext.Get());


		for (const auto* model : *pVector_BasicModels)
		{
			// Multiply Object's World Matrix by Camera's ViewProjection Matrix
			XMStoreFloat4x4(&mData_CB_VS_Per_Object.gWVP, XMMatrixMultiply(XMLoadFloat4x4A(&model->mWorldMatrix), gCamera_main.ViewProj()));

			//Store Object's WorldInvTranspose Matrix
			mData_CB_VS_Per_Object.gWorldInvTranspose = model->mWorldInvTransposeMatrix;

			mCB_VS_Per_Object->Update(gContext.Get());

			for (const auto* mesh : model->mMeshes)
			{

				auto material = gModelManager.gMaterials[mesh->GetMatrialIndex()].get();

				if (gCamera_main.BoundingBoxContainment(mesh->GetBoundingBox()) == ContainmentType::DISJOINT)
					continue;


				XMFLOAT4 selectedColor = mesh == gSelectedMesh ? XMFLOAT4(1.0f, c, c, 1.0f) : material->mShadingColor.diffuseColor;

				//Update Material Info of Object
				mData_CB_PS_Per_Object.diffuseColor = selectedColor;
				mData_CB_PS_Per_Object.ambientColor = material->mShadingColor.ambientColor;
				mData_CB_PS_Per_Object.specularColor = material->mShadingColor.specularColor;

				mData_CB_VS_Per_Mesh.gTexTransform = mesh->mMatrix_TexTransform;


				mCB_VS_Per_Mesh->Update(gContext.Get());
				mCB_PS_Per_Object->Update(gContext.Get());

				ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstace_Material(material->GetSurfaceShaderClassInstance_Material());
				ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstance_Normal(material->GetSurfaceShaderClassInstance_Normal());
				ShaderList::PixelShaders::PS_GenerateGBuffers::SetShader(gContext.Get());

				material->SetShaderResources_PS(0, gContext.Get());

				mesh->Draw(gContext.Get());

			}
		}

	}

	auto pVector_SkinnedModels = gModelManager.GetSkinnedModels();

	if (pVector_SkinnedModels->size() > 0)
	{

		gContext->IASetInputLayout(Vertex::IL_Skinned.Get());
		gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);

		ShaderList::VertexShaders::VS_ProcessMesh::SetShader_Skinned(gContext.Get());

		for (const auto* skinnedmodel : *pVector_SkinnedModels)
		{

			mCB_VS_BoneTransforms->Update(gContext.Get(), skinnedmodel->GetBoneTransforms()->data(), skinnedmodel->GetBoneTransforms()->size() * sizeof(XMFLOAT4X4));

			// Multiply Object's World Matrix by Camera's ViewProjection Matrix
			XMStoreFloat4x4(&mData_CB_VS_Per_Object.gWVP, XMMatrixMultiply(XMLoadFloat4x4A(&skinnedmodel->mWorldMatrix), gCamera_main.ViewProj()));

			//Store Object's WorldInvTranspose Matrix
			mData_CB_VS_Per_Object.gWorldInvTranspose = skinnedmodel->mWorldInvTransposeMatrix;

			mCB_VS_Per_Object->Update(gContext.Get());

			for (const auto* mesh : skinnedmodel->mMeshes)
			{

				//Update Material Info of Object
				mData_CB_PS_Per_Object.diffuseColor = mesh->GetMaterial()->mShadingColor.diffuseColor;
				mData_CB_PS_Per_Object.ambientColor = mesh->GetMaterial()->mShadingColor.ambientColor;
				mData_CB_PS_Per_Object.specularColor = mesh->GetMaterial()->mShadingColor.specularColor;

				mData_CB_VS_Per_Mesh.gTexTransform = mesh->mMatrix_TexTransform;


				mCB_VS_Per_Mesh->Update(gContext.Get());
				mCB_PS_Per_Object->Update(gContext.Get());

				ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstace_Material(mesh->GetMaterial()->GetSurfaceShaderClassInstance_Material());
				ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstance_Normal(mesh->GetMaterial()->GetSurfaceShaderClassInstance_Normal());
				ShaderList::PixelShaders::PS_GenerateGBuffers::SetShader(gContext.Get());

				mesh->GetMaterial()->SetShaderResources_PS(0, gContext.Get());

				mesh->Draw(gContext.Get());

			}
		}
	}

	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	//gD3DUserDefinedAnnotation->EndEvent();
}

void RenderPass_GenerateGBuffers::DrawDeferred()
{
	static float c = 1.0f, s = -1.0f;

	if (gSelectedMesh != nullptr)
	{

		c += s * (0.000006f / gDt);
		if (c < 0.0 || c > 1.0f)
		{
			c = c < 0.0 ? 0.0f : 1.0f;
			s *= -1.0f;
		}

	}

	//gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_GenerateGBuffers");

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, mDeferredContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, mDeferredContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_DEFAULT, mDeferredContext.Get());

	mRTVCollector->ClearRTVs(mDeferredContext.Get());
	mDeferredContext->ClearDepthStencilView(mTexView_DepthMap->GetDSV(), D3D11_CLEAR_DEPTH, 1.0f, 0);
	mDeferredContext->OMSetRenderTargets(3, mRTVCollector->GetRTVs(), mTexView_DepthMap->GetDSV());
	mDeferredContext->RSSetViewports(1, &gViewPort);

	auto pVector_BasicModels = gModelManager.GetBasicModels();

	if (pVector_BasicModels->size() > 0)
	{

		mDeferredContext->IASetInputLayout(Vertex::IL_Basic32.Get());
		mDeferredContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		mDeferredContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);


		ShaderList::VertexShaders::VS_ProcessMesh::SetShader_Basic32(mDeferredContext.Get());


		for (const Model* model : *pVector_BasicModels)
		{
			// Multiply Object's World Matrix by Camera's ViewProjection Matrix
			XMStoreFloat4x4(&mData_CB_VS_Per_Object.gWVP, XMMatrixMultiply(XMLoadFloat4x4A(&model->mWorldMatrix), gCamera_main.ViewProj()));

			//Store Object's WorldInvTranspose Matrix
			mData_CB_VS_Per_Object.gWorldInvTranspose = model->mWorldInvTransposeMatrix;

			mCB_VS_Per_Object->Update(mDeferredContext.Get());

			for (const IMesh* mesh : model->mMeshes)
			{

				auto material = gModelManager.gMaterials[mesh->GetMatrialIndex()].get();

				if (gCamera_main.BoundingBoxContainment(mesh->GetBoundingBox()) == ContainmentType::DISJOINT)
					continue;


				XMFLOAT4 selectedColor = mesh == gSelectedMesh ? XMFLOAT4(1.0f, c, c, 1.0f) : material->mShadingColor.diffuseColor;

				//Update Material Info of Object
				mData_CB_PS_Per_Object.diffuseColor = selectedColor;
				mData_CB_PS_Per_Object.ambientColor = material->mShadingColor.ambientColor;
				mData_CB_PS_Per_Object.specularColor = material->mShadingColor.specularColor;

				mData_CB_VS_Per_Mesh.gTexTransform = mesh->mMatrix_TexTransform;


				mCB_VS_Per_Mesh->Update(mDeferredContext.Get());
				mCB_PS_Per_Object->Update(mDeferredContext.Get());

				ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstace_Material(material->GetSurfaceShaderClassInstance_Material());
				ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstance_Normal(material->GetSurfaceShaderClassInstance_Normal());
				ShaderList::PixelShaders::PS_GenerateGBuffers::SetShader(mDeferredContext.Get());

				material->SetShaderResources_PS(0, mDeferredContext.Get());

				mesh->Draw(mDeferredContext.Get());

			}
		}

	}

	auto pVector_SkinnedModels = gModelManager.GetSkinnedModels();

	if (pVector_SkinnedModels->size() > 0)
	{

		mDeferredContext->IASetInputLayout(Vertex::IL_Skinned.Get());
		mDeferredContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		mDeferredContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);

		ShaderList::VertexShaders::VS_ProcessMesh::SetShader_Skinned(mDeferredContext.Get());

		for (const SkinnedModel* skinnedmodel : *pVector_SkinnedModels)
		{

			mCB_VS_BoneTransforms->Update(mDeferredContext.Get(), skinnedmodel->GetBoneTransforms()->data(), skinnedmodel->GetBoneTransforms()->size() * sizeof(XMFLOAT4X4));

			// Multiply Object's World Matrix by Camera's ViewProjection Matrix
			XMStoreFloat4x4(&mData_CB_VS_Per_Object.gWVP, XMMatrixMultiply(XMLoadFloat4x4A(&skinnedmodel->mWorldMatrix), gCamera_main.ViewProj()));

			//Store Object's WorldInvTranspose Matrix
			mData_CB_VS_Per_Object.gWorldInvTranspose = skinnedmodel->mWorldInvTransposeMatrix;

			mCB_VS_Per_Object->Update(mDeferredContext.Get());

			for (const IMesh* mesh : skinnedmodel->mMeshes)
			{

				//Update Material Info of Object
				mData_CB_PS_Per_Object.diffuseColor = mesh->GetMaterial()->mShadingColor.diffuseColor;
				mData_CB_PS_Per_Object.ambientColor = mesh->GetMaterial()->mShadingColor.ambientColor;
				mData_CB_PS_Per_Object.specularColor = mesh->GetMaterial()->mShadingColor.specularColor;

				mData_CB_VS_Per_Mesh.gTexTransform = mesh->mMatrix_TexTransform;


				mCB_VS_Per_Mesh->Update(mDeferredContext.Get());
				mCB_PS_Per_Object->Update(mDeferredContext.Get());

				ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstace_Material(mesh->GetMaterial()->GetSurfaceShaderClassInstance_Material());
				ShaderList::PixelShaders::PS_GenerateGBuffers::SetClassInstance_Normal(mesh->GetMaterial()->GetSurfaceShaderClassInstance_Normal());
				ShaderList::PixelShaders::PS_GenerateGBuffers::SetShader(mDeferredContext.Get());

				mesh->GetMaterial()->SetShaderResources_PS(0, mDeferredContext.Get());

				mesh->Draw(mDeferredContext.Get());

			}
		}
	}

	//gD3DUserDefinedAnnotation->EndEvent();


	mDeferredContext->FinishCommandList(true, mCommandList.ReleaseAndGetAddressOf());
}

void RenderPass_GenerateGBuffers::Draw2D()
{
}

void RenderPass_GenerateGBuffers::DrawTxt()
{
}

void RenderPass_GenerateGBuffers::Update()
{

}

void RenderPass_GenerateGBuffers::Destroy()
{

}

void RenderPass_GenerateGBuffers::DestroyGPUResources()
{
	ReleaseViews();

	ReleaseBuffers();

	mDeferredContext.Reset();

	mCommandList.Reset();

	GPUResourceInitialized = false;

}

void RenderPass_GenerateGBuffers::InitGPUResources()
{
	if (!GPUResourceInitialized)
	{
		gDevice->CreateDeferredContext(0, mDeferredContext.ReleaseAndGetAddressOf());

		D3D::SetGlobalContextStates(mDeferredContext.Get(), true, false, false);

		InitBuffers();
		InitViews();


		GPUResourceInitialized = true;
	}
}

void RenderPass_GenerateGBuffers::CaptureFrame()
{
}

void RenderPass_GenerateGBuffers::RecompileShaders()
{
}

std::string RenderPass_GenerateGBuffers::GPUTime_String()
{
	return "Generate GBuffers' Render Time : " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}

const TexViews* RenderPass_GenerateGBuffers::GetTexViewColorMap() const noexcept
{
	return mTexView_ColorMap.get();
}

const TexViews* RenderPass_GenerateGBuffers::GetTexViewNormalMap() const noexcept
{
	return mTexView_NormalMap.get();
}

const TexViews* RenderPass_GenerateGBuffers::GetTexViewDepthMap() const noexcept
{
	return mTexView_DepthMap.get();
}

const TexViews* RenderPass_GenerateGBuffers::GetTexViewSpecularMap() const noexcept
{
	return mTexView_SpecularMap.get();
}
