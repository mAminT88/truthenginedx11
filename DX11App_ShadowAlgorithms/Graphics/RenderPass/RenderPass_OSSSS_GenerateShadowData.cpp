#include "stdafx.h"
#include "RenderPass_OSSSS_GenerateShadowData.h"
#include "Globals.h"

#include "Graphics/RenderStates.h"
#include "Graphics/TexViews.h"
#include "Graphics/ResourceViewCollectors.h"
#include "Graphics/Colors.h"
#include "Graphics/ConstantBuffer.h"
#include "Graphics/ShaderSlots_DeferredShading.h"


#include "Utility/GpuProfiler.h"

#include "Graphics/ShaderList/CS_DownSampling.h"
#include <Graphics/ShaderList/VS_Render2D.h>
#include <Graphics/ShaderList/PS_OSSSS_GenerateShadowData_Pass0.h>
#include <Graphics/ShaderList/PS_OSSSS_GenerateShadowData_Pass1.h>
#include <Graphics/ShaderList/PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch.h>


using namespace Globals;

void RenderPass_OSSSS_GenerateShadowData::InitViews()
{

	auto lightCount = gLights.GetShadowMapCount();

	UINT height = mDownSampling ? gClientHeight / mDownSamplingScale : gClientHeight;
	UINT width = mDownSampling ? gClientWidth / mDownSamplingScale : gClientWidth;

	mTexView_Shadows->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, height
		, width
		, DXGI_FORMAT_R32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, lightCount
		, 1
		, 1
		, 0
		, 0
		, true
	);

	mTexView_PenumbraSize->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight	 //(mDownSampling ? gClientHeight / mDownSamplingScale : gClientHeight)
		, gClientWidth	//(mDownSampling ? gClientWidth / mDownSamplingScale : gClientWidth)
		, DXGI_FORMAT_R32_FLOAT
	);


	mTexView_Shadows_Temp->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight //(mDownSampling ? gClientHeight / mDownSamplingScale : gClientHeight)
		, gClientWidth //(mDownSampling ? gClientWidth / mDownSamplingScale : gClientWidth)
		, DXGI_FORMAT_R32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, 1
		, 1
		, 1
		, 0
		, 0
		, true
	);

	if (mDownSampling)
	{

		mTexView_Shadows_DownSampled->Init(D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE
			, height
			, width
			, DXGI_FORMAT_R32_FLOAT
			, D3D11_USAGE_DEFAULT
			, (D3D11_CPU_ACCESS_FLAG)0
			, 1
			, 1
			, 1
			, 0
			, 0
			, false
		);

		mTexView_Shadows_DownSampled_Temp->Init(D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
			, height
			, width
			, DXGI_FORMAT_R32_FLOAT
			, D3D11_USAGE_DEFAULT
			, (D3D11_CPU_ACCESS_FLAG)0
			, 1
			, 1
			, 1
			, 0
			, 0
			, false
		);

	}

	mRTVCollector_0->ClearAndAddRTV(mTexView_Shadows_Temp->GetRTV());
	mRTVCollector_0->AddRTV(mTexView_PenumbraSize->GetRTV());

	mRTVCollector_1->ClearAndAddRTV(mTexView_Shadows->GetRTV());

	mSRVCollector_1->ClearAndAddSRV(mDownSampling ? mTexView_Shadows_DownSampled->GetSRV() : mTexView_Shadows_Temp->GetSRV());
	mSRVCollector_1->AddSRV(mTexView_PenumbraSize->GetSRV());

}

void RenderPass_OSSSS_GenerateShadowData::ReleaseViews()
{
	mTexView_PenumbraSize->Release();
	mTexView_Shadows->Release();
	mTexView_Shadows_Temp->Release();
	mTexView_Shadows_DownSampled->Release();
}

void RenderPass_OSSSS_GenerateShadowData::InitBuffers()
{
	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
	mGpuTimeStampDB_1.reset(new GPUTimeStamp_DualBuffer(2));
}

void RenderPass_OSSSS_GenerateShadowData::ReleaseBuffers()
{
	//mCB_PerLight->Release();

	mGpuTimeStampDB->Destroy();
	mGpuTimeStampDB_1->Destroy();

	mGpuTimeStampDB.reset();
	mGpuTimeStampDB_1.reset();

}

RenderPass_OSSSS_GenerateShadowData::RenderPass_OSSSS_GenerateShadowData() {
	mTexView_Shadows.reset(new TexViews());
	mTexView_PenumbraSize.reset(new TexViews());
	mTexView_Shadows_Temp.reset(new TexViews());
	mTexView_Shadows_DownSampled.reset(new TexViews());
	mTexView_Shadows_DownSampled_Temp.reset(new TexViews());



	mSRVCollector_0.reset(new SRVCollector());
	mSRVCollector_1.reset(new SRVCollector());
	mRTVCollector_0.reset(new RTVCollector());
	mRTVCollector_1.reset(new RTVCollector());
}

RenderPass_OSSSS_GenerateShadowData::~RenderPass_OSSSS_GenerateShadowData() = default;

RenderPass_OSSSS_GenerateShadowData::RenderPass_OSSSS_GenerateShadowData(RenderPass_OSSSS_GenerateShadowData&&) = default;

RenderPass_OSSSS_GenerateShadowData& RenderPass_OSSSS_GenerateShadowData::operator=(RenderPass_OSSSS_GenerateShadowData&&) = default;

void RenderPass_OSSSS_GenerateShadowData::Init()
{
	/*auto mRotationAngle_radian = XMConvertToRadians(90.0f);
	auto m = XMMatrixRotationY(mRotationAngle_radian);

	XMStoreFloat4x4(&mMatixRotationY_90, m);*/

}

void RenderPass_OSSSS_GenerateShadowData::InitGPUResources()
{

	mDownSamplingGroupSizeX = (UINT)ceil((float)(gClientWidth * gClientHeight / 16) / 1024.0f);

	mViewPort_DownSampling.Height = gClientHeight / mDownSamplingScale;
	mViewPort_DownSampling.Width = gClientWidth / mDownSamplingScale;
	mViewPort_DownSampling.MaxDepth = 1.0f;
	mViewPort_DownSampling.MinDepth = 0.0f;
	mViewPort_DownSampling.TopLeftX = 0.0f;
	mViewPort_DownSampling.TopLeftY = 0.0f;

	if (!GPUResourceInitialized)
	{


		InitViews();
		InitBuffers();

		GPUResourceInitialized = true;
	}

	SetSRVs();

}

void RenderPass_OSSSS_GenerateShadowData::DestroyGPUResources()
{
	ReleaseBuffers();
	ReleaseViews();

	GPUResourceInitialized = false;
}

void RenderPass_OSSSS_GenerateShadowData::Draw3D()
{

	gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_OSSSS_GenerateShadowData");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
	gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);


	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH, gContext.Get());

	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	ShaderList::VertexShaders::VS_Render2D::SetShader(gContext.Get());


	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_GenerateShadowData_0_Normal();
	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_HorizontalStep();

	for (int i = 0; i < gLights.mSpotLights.size(); ++i)
	{

		gContext->RSSetViewports(1, &gViewPort);

		gContext->OMSetRenderTargets(0, nullptr, nullptr);

		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_ActiveLight_SpotLight(i);
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::SetShader(gContext.Get());


		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });


		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);


		mSRVCollector_0->BindSRVs_PS(gContext, 0);


		gContext->DrawIndexed(6, 0, 0);


		ID3D11RenderTargetView* rtv;

		if (mDownSampling)
		{

			//DownSampling

			SetNullRTVDSV(gContext.Get(), 2);

			ID3D11UnorderedAccessView* uav[1];

			gContext->RSSetViewports(1, &mViewPort_DownSampling);

			ShaderList::ComputeShaders::CS_DownSampling::SetShader(gContext.Get());

			uav[0] = mTexView_Shadows_DownSampled->GetUAV();
			gContext->CSSetUnorderedAccessViews(0, 1, uav, nullptr);

			gContext->CSSetShaderResources(0, 1, mTexView_Shadows_Temp->GetSRV_AddressOf());

			gContext->Dispatch(mDownSamplingGroupSizeX, 1, 1);

			uav[0] = nullptr;
			gContext->CSSetUnorderedAccessViews(0, 1, uav, nullptr);

			//Blur

			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_ActiveLight_SpotLight(i);
			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader(gContext.Get());

			auto rtv = mTexView_Shadows->GetRTV(i);
			gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			mSRVCollector_1->BindSRVs_PS(gContext, 4);

			gContext->DrawIndexed(6, 0, 0);

			//UpScaling

			/*gContext->RSSetViewports(1, &gViewPort);

			ShaderList::PixelShaders::PS_DownUpSampling::SetShader(gContext.Get());

			rtv = mTexView_Shadows->GetRTV();
			gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f , 1.0f , 1.0f });
			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			gContext->PSSetShaderResources(0, 1, mTexView_Shadows_DownSampled_Temp->GetSRV_AddressOf());

			gContext->DrawIndexed(6, 0, 0);*/

		}
		else {

			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_ActiveLight_SpotLight(i);
			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader(gContext.Get());

			gContext->RSSetViewports(1, &gViewPort);

			auto rtv = mTexView_Shadows->GetRTV(i);
			gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			mSRVCollector_1->BindSRVs_PS(gContext, 4);


			gContext->DrawIndexed(6, 0, 0);
		}
	}

	for (int i = 0; i < gLights.mDirectionalLights.size(); ++i)
	{
		gContext->RSSetViewports(1, mDownSampling ? &mViewPort_DownSampling : &gViewPort);

		gContext->OMSetRenderTargets(0, nullptr, nullptr);


		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_ActiveLight_DirectLight(i);
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::SetShader(gContext.Get());


		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });


		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);


		mSRVCollector_0->BindSRVs_PS(gContext, 0);


		//CalcLightDirInViewSpace_XZ(mData_CB_PerLight.gLight2DDirInEye_Horz, mData_CB_PerLight.gLight2DDirInEye_Vert, gLights.mDirectionalLights[i].GetILightData());

		//mCB_PerLight->Update(gContext.Get());


		gContext->DrawIndexed(6, 0, 0);

		mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

		mGpuTimeStampDB_1->SetStart(gCurrentFrame, gContext.Get());




		if (mDownSampling)
		{

			//DownSampling


			SetNullRTVDSV(gContext.Get(), 2);


			ShaderList::ComputeShaders::CS_DownSampling::SetShader(gContext.Get());

			ID3D11UnorderedAccessView* uav[1];
			uav[0] = mTexView_Shadows_DownSampled->GetUAV();
			gContext->CSSetUnorderedAccessViews(0, 1, uav, nullptr);

			gContext->CSSetShaderResources(0, 1, mTexView_Shadows_Temp->GetSRV_AddressOf());

			gContext->Dispatch(mDownSamplingGroupSizeX, 1, 1);

			//Blur

			uav[0] = nullptr;
			gContext->CSSetUnorderedAccessViews(0, 1, uav, nullptr);


			gContext->RSSetViewports(1, &mViewPort_DownSampling);

			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_ActiveLight_DirectLight(i);
			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader(gContext.Get());

			auto rtv = mTexView_Shadows->GetRTV(i);
			gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			mSRVCollector_1->BindSRVs_PS(gContext, 4);

			gContext->DrawIndexed(6, 0, 0);

			//UpScaling

			/*gContext->RSSetViewports(1, &gViewPort);

			ShaderList::PixelShaders::PS_DownUpSampling::SetShader(gContext.Get());

			rtv = mTexView_Shadows->GetRTV();
			gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f , 1.0f , 1.0f });
			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			gContext->PSSetShaderResources(0, 1, mTexView_Shadows_DownSampled_Temp->GetSRV_AddressOf());

			gContext->DrawIndexed(6, 0, 0);*/

		}
		else {

			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_ActiveLight_DirectLight(i);
			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader(gContext.Get());

			gContext->RSSetViewports(1, &gViewPort);

			auto rtv = mTexView_Shadows->GetRTV(i);
			gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			mSRVCollector_1->BindSRVs_PS(gContext, 4);


			gContext->DrawIndexed(6, 0, 0);
		}

		mGpuTimeStampDB_1->SetEnd(gCurrentFrame, gContext.Get());

	}

	gD3DUserDefinedAnnotation->EndEvent();


	//	gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_OSSSS_GenerateShadowData");
	//
	//	mGpuTimeStampDB->SetStart(gCurrentFrame);
	//
	//	gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
	//	gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	//	gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);
	//
	//	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT);
	//	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT);
	//	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH);
	//
	//	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	//	ShaderList::VertexShaders::VS_Render2D::SetShader();
	//
	//	ID3D11UnorderedAccessView* uav;
	//
	//	gContext->RSSetViewports(1, &gViewPort);
	//
	//
	//	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_GenerateShadowData_0_Normal();
	//	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_HorizontalStep();
	//
	//	for (int i = 0; i < gLights.mSpotLights.size(); ++i)
	//	{
	//		gContext->OMSetRenderTargets(0, nullptr, nullptr);
	//
	//
	//		//mPipeline.Apply_Preliminiary_Pass0();
	//		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_ActiveLight_SpotLight(i);
	//		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::SetShader();
	//
	//
	//		//mRTVCollector_0->ReplaceRTV(mTexView_Shadows->GetRTV(i), 0);
	//
	//		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
	//		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	//
	//
	//		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);
	//
	//
	//		mSRVCollector_0->BindSRVs_PS(gContext, 0);
	//
	//
	//		//CalcLightDirInViewSpace_XZ(mData_CB_PerLight.gLight2DDirInEye_Horz, mData_CB_PerLight.gLight2DDirInEye_Vert, gLights.mSpotLights[i].GetILightData());
	//		//mData_CB_PerLight.gShadowMapID = i;
	//
	//		//mCB_PerLight->Update(gContext.Get());
	//
	//
	//		gContext->DrawIndexed(6, 0, 0);
	//
	//
	//		//mPipeline.Apply_Preliminiary_Pass1();
	//		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_ActiveLight_SpotLight(i);
	//		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader();
	//
	//
	//		auto rtv = mTexView_Shadows->GetRTV(i);
	//		gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	//		gContext->OMSetRenderTargets(1, &rtv, nullptr);
	//
	//		mSRVCollector_1->BindSRVs_PS(gContext, 4);
	//
	//
	//		gContext->DrawIndexed(6, 0, 0);
	//	}
	//
	//	for (int i = 0; i < gLights.mDirectionalLights.size(); ++i)
	//	{
	//		gContext->OMSetRenderTargets(0, nullptr, nullptr);
	//
	//
	//		//mPipeline.Apply_Preliminiary_Pass0();
	//		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_ActiveLight_DirectLight(i);
	//		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::SetShader();
	//
	//
	//		//mRTVCollector_0->ReplaceRTV(mTexView_Shadows->GetRTV(i), 0);
	//
	//		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
	//		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	//
	//
	//		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);
	//
	//
	//		mSRVCollector_0->BindSRVs_PS(gContext, 0);
	//
	//
	//		//CalcLightDirInViewSpace_XZ(mData_CB_PerLight.gLight2DDirInEye_Horz, mData_CB_PerLight.gLight2DDirInEye_Vert, gLights.mDirectionalLights[i].GetILightData());
	//		//mData_CB_PerLight.gShadowMapID = i;
	//
	//		//mCB_PerLight->Update(gContext.Get());
	//
	//
	//		gContext->DrawIndexed(6, 0, 0);
	//
	//
	//		//mPipeline.Apply_Preliminiary_Pass1();
	//		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_ActiveLight_DirectLight(i);
	//		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader();
	//
	//
	//		auto rtv = mTexView_Shadows->GetRTV(i);
	//		gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	//		gContext->OMSetRenderTargets(1, &rtv, nullptr);
	//
	//		mSRVCollector_1->BindSRVs_PS(gContext, 4);
	//
	//
	//		gContext->DrawIndexed(6, 0, 0);
	//	}
	//
	//// 	for (int i = 0; i < gLights.GetShadowMapCount(); ++i)
	//// 	{
	//// 
	//// 		gContext->OMSetRenderTargets(0, nullptr, nullptr);
	//// 
	//// 
	//// 		//mPipeline.Apply_Preliminiary_Pass0();
	//// 		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::SetShader();
	//// 
	//// 
	//// 		//mRTVCollector_0->ReplaceRTV(mTexView_Shadows->GetRTV(i), 0);
	//// 
	//// 		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
	//// 		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	//// 
	//// 
	//// 		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);
	//// 
	//// 
	//// 		mSRVCollector_0->BindSRVs_PS(gContext, 0);
	//// 
	//// 
	//// 		CalcLightDirInViewSpace_XZ(mData_CB_PerLight.gLight2DDirInEye_Horz, mData_CB_PerLight.gLight2DDirInEye_Vert, gLights.mCollectionILights[i]->GetILightData());
	//// 		mData_CB_PerLight.gShadowMapID = i;
	//// 
	//// 		mCB_PerLight->Update(gContext.Get());
	//// 
	//// 
	//// 		gContext->DrawIndexed(6, 0, 0);
	//// 
	//// 
	//// 		//mPipeline.Apply_Preliminiary_Pass1();
	//// 		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader();
	//// 
	//// 
	//// 		auto rtv = mTexView_Shadows->GetRTV(i);
	//// 		gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	//// 		gContext->OMSetRenderTargets(1, &rtv, nullptr);
	//// 
	//// 		mSRVCollector_1->BindSRVs_PS(gContext, 4);
	//// 
	//// 
	//// 		gContext->DrawIndexed(6, 0, 0);
	//// 
	//// 		/*ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_VerticalStep();
	//// 		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader();
	//// 
	//// 
	//// 		gContext->ClearRenderTargetView(mTexView_Shadows->GetRTV(i), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	//// 		gContext->OMSetRenderTargets(1, &rtv, nullptr);
	//// 
	//// 
	//// 		gContext->DrawIndexed(6, 0, 0);*/
	//// 
	//// 	}
	//
	//	mGpuTimeStampDB->SetEnd(gCurrentFrame);
	//
	//	gD3DUserDefinedAnnotation->EndEvent();

	//gContext->ExecuteCommandList(mCommandList.Get(), true);

}

void RenderPass_OSSSS_GenerateShadowData::DrawDeferred()
{

}

void RenderPass_OSSSS_GenerateShadowData::Draw2D()
{

}

void RenderPass_OSSSS_GenerateShadowData::DrawTxt()
{

}

void RenderPass_OSSSS_GenerateShadowData::Update()
{

}

void RenderPass_OSSSS_GenerateShadowData::Destroy()
{

}

void RenderPass_OSSSS_GenerateShadowData::CaptureFrame()
{

}

void RenderPass_OSSSS_GenerateShadowData::RecompileShaders()
{
	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::InitStatics();
	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::InitStatics();
}

void RenderPass_OSSSS_GenerateShadowData::SetSRVs()
{
	mSRVCollector_0->ClearAndAddSRV(pTexViewDepthMap->GetSRV());
	mSRVCollector_0->AddSRV(pTexViewShadowMap->GetSRV());
	mSRVCollector_0->AddSRV(pTexViewNormalMap->GetSRV());
}

ID3D11ShaderResourceView* RenderPass_OSSSS_GenerateShadowData::GetSRV_Shadows() const
{
	return mTexView_Shadows->GetSRV();
}

void RenderPass_OSSSS_GenerateShadowData::RenderConfigurationUI()
{
	ImGui::Begin("Settings: Optimized Screen Space Soft Shadows");

	if (ImGui::DragFloat("Blocker Search Sample Count", &ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mBlockerSearchSampleCount, 1.0f, 1.0f, 51.0f))
	{
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mBlockerSearchSampleCount = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mBlockerSearchSampleCount;
	}

	if (ImGui::DragFloat("Gaussian Filter Width", &ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mFilterWidth, 1.0f, 1.0f, 51.0f))
	{
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mFilterWidth = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mFilterWidth;
	}

	if (ImGui::DragFloat("Bilateral Depth Reference", &ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mBilateralDepth, 0.1f, 0.1f, 1000.0f))
	{
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::mBilateralDepth = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::mBilateralDepth;
	}


	ImGui::DragInt("Down Sampling Scale Factor", &mDownSamplingScale, 1.0f, 1, 10);

	static bool cross_product_version;

	if (ImGui::Checkbox("Cross Product version", &cross_product_version))
	{
		if (cross_product_version)
			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::PSVersion = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::PS_OSSSS_GenerateShadowData_Pass1_Version::cross_product;
		else
			ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::PSVersion = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::PS_OSSSS_GenerateShadowData_Pass1_Version::normal;
	}

	ImGui::End();
}

void RenderPass_OSSSS_GenerateShadowData::SetDownSamplingScale(int sfactor)
{
	mDownSamplingScale = sfactor;
}

void RenderPass_OSSSS_GenerateShadowData::SetDownSampling(bool active)
{
	if (mDownSampling == active)
		return;

	mDownSampling = active;

	/*DestroyGPUResources();
	InitGPUResources();

	DeferredDraw();*/
}

const TexViews* RenderPass_OSSSS_GenerateShadowData::GetTexViewShadows() const noexcept
{
	return mTexView_Shadows.get();
}

void RenderPass_OSSSS_GenerateShadowData::ProfileGPUTime(float GPUTime)
{
	mGpuTimeStampDB->WaitAndCalculateTime(gLastFrame, GPUTime, gContext.Get());
	mGpuTimeStampDB_1->WaitAndCalculateTime(gLastFrame, GPUTime, gContext.Get());
}

std::string RenderPass_OSSSS_GenerateShadowData::GPUTime_String()
{
	return "OSSSS Generate Shadow Data's Render Time : First Stage Time = " + std::to_string(mGpuTimeStampDB->GetTimeDuration()) + "; Second Stage Time : " + std::to_string(mGpuTimeStampDB_1->GetTimeDuration());
}




//////////////////////////////////////////////////////////////////////////
// Screen Space Separable Blocker Search


void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::InitViews()
{

	auto lightCount = gLights.GetShadowMapCount();


	mTexView_Shadows->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, lightCount
		, 1
		, 1
		, 0
		, 0
		, true
	);

	mTexView_PenumbraSize->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R32_FLOAT
	);


	mTexView_Shadows_Temp->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R32_FLOAT
	);

	mTexView_AvgBlocker_Temp->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R32_FLOAT
	);


	mRTVCollector_0->ClearAndAddRTV(mTexView_AvgBlocker_Temp->GetRTV());

	mRTVCollector_1->ClearAndAddRTV(mTexView_Shadows_Temp->GetRTV());
	mRTVCollector_1->AddRTV(mTexView_PenumbraSize->GetRTV());

	mRTVCollector_2->ClearAndAddRTV(mTexView_Shadows->GetRTV());

	//Set Shader Resource Views

	mSRVCollector_1->ClearAndAddSRV(mTexView_AvgBlocker_Temp->GetSRV());


	mSRVCollector_2->ClearAndAddSRV(mTexView_Shadows_Temp->GetSRV());
	mSRVCollector_2->AddSRV(mTexView_PenumbraSize->GetSRV());

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::ReleaseViews()
{
	mTexView_PenumbraSize->Release();
	mTexView_Shadows->Release();
	mTexView_Shadows_Temp->Release();
	mTexView_AvgBlocker_Temp->Release();

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::InitBuffers()
{
	mCB_PerLight.reset(new ConstantBuffer<CB_PS_PER_LIGHT>());

	mCB_PerLight->InitConstantBuffer(SHADER_SLOTS_PS_CB_OSSSS_GENERATESHADOWDATA
		, CONSTANT_BUFFER_PS
		, &mData_CB_PerLight
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_PerLight->SetConstantBuffer(gContext.Get());


	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::ReleaseBuffers()
{
	mCB_PerLight->Release();

	mCB_PerLight.reset();

	mGpuTimeStampDB->Destroy();
	mGpuTimeStampDB.reset();

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::CalcLightDirInViewSpace_XZ(XMFLOAT2& LightDir_Horz, XMFLOAT2& LightDir_Vert, const ILight& light)
{
	XMMATRIX view_xz = XMMatrixLookToLH(gCamera_main.GetPositionXM()
		, XMVector4Normalize(XMVectorSet(gCamera_main.GetLook().x, 0.0f, gCamera_main.GetLook().z, 0.0f))
		, XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f));

	auto lightDirW_XZ = light.Direction;
	lightDirW_XZ.y = 0.0f;

	auto lightDir = XMVector4Normalize(XMVector4Transform(XMLoadFloat3(&lightDirW_XZ), view_xz));

	XMFLOAT3 dir_temp;

	XMStoreFloat3(&dir_temp, lightDir);

	LightDir_Vert.x = dir_temp.x;
	LightDir_Vert.y = dir_temp.z * -1.0f;

	lightDir = XMVector4Transform(lightDir, XMLoadFloat4x4(&mMatixRotationY_90));

	XMStoreFloat3(&dir_temp, lightDir);

	LightDir_Horz.x = dir_temp.x;
	LightDir_Horz.y = dir_temp.z * -1.0f;


}

RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch() {

	mSRVCollector_0.reset(new SRVCollector());
	mSRVCollector_1.reset(new SRVCollector());
	mSRVCollector_2.reset(new SRVCollector());

	mRTVCollector_0.reset(new RTVCollector());
	mRTVCollector_1.reset(new RTVCollector());
	mRTVCollector_2.reset(new RTVCollector());

	mTexView_Shadows.reset(new TexViews());
	mTexView_PenumbraSize.reset(new TexViews());
	mTexView_Shadows_Temp.reset(new TexViews());
	mTexView_AvgBlocker_Temp.reset(new TexViews());
}


RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::~RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch() = default;

RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch(RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch&&) = default;

RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch& RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::operator=(RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch&&) = default;

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Init()
{
	//InitPipelines();

	auto mRotationAngle_radian = XMConvertToRadians(90.0f);
	auto m = XMMatrixRotationY(mRotationAngle_radian);

	XMStoreFloat4x4(&mMatixRotationY_90, m);


}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::InitGPUResources()
{

	if (!GPUResourceInitialized)
	{

		InitViews();
		InitBuffers();

		GPUResourceInitialized = true;
	}

	SetSRVs();

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::DestroyGPUResources()
{
	ReleaseBuffers();
	ReleaseViews();

	GPUResourceInitialized = false;
}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Draw3D()
{

	gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_OSSSS_GenerateShadowData_SeparableBlockerSearch");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
	gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());

	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	ShaderList::VertexShaders::VS_Render2D::SetShader(gContext.Get());


	ID3D11UnorderedAccessView* uav;

	gContext->RSSetViewports(1, &gViewPort);


	for (int i = 0; i < gLights.mDirectionalLights.size(); ++i)
	{
		//Clear All Render Target Views

		gContext->ClearRenderTargetView(mTexView_AvgBlocker_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
		gContext->ClearRenderTargetView(mTexView_Shadows->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });

		//
		// Search Blocker Verticall On Shadow Map
		//

		//mPipeline.Apply_Preliminiary_Pass0();
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::Set_ClassInstance_ActiveLight_DirectLight(i);
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::SetShader(gContext.Get());


		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);


		mSRVCollector_0->BindSRVs_PS(gContext, 0);


		CalcLightDirInViewSpace_XZ(mData_CB_PerLight.gLight2DDirInEye_Horz, mData_CB_PerLight.gLight2DDirInEye_Vert, gLights.mDirectionalLights[i].GetILightData());
		mData_CB_PerLight.gShadowMapID = i;


		mCB_PerLight->Update(gContext.Get());


		gContext->DrawIndexed(6, 0, 0);


		//
		// Search Blocker Horizontally On Screen Space And Calculate Penumbra Size And Filter hard Shadows Vertically on Shadow Map
		//

		//mPipeline.Apply_Preliminiary_Pass1();
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::Set_ClassInstance_ActiveLight_DirectLight(i);
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::SetShader(gContext.Get());



		gContext->OMSetRenderTargets(mRTVCollector_1->GetSize(), mRTVCollector_1->GetRTVs(), nullptr);


		mSRVCollector_1->BindSRVs_PS(gContext, 3);


		gContext->DrawIndexed(6, 0, 0);


		//
		// Apply Filter on Vertically Filtered Shadows On Screen Space Horizontally
		//

		//mPipeline.Apply_Preliminiary_Pass2();
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::Set_ClassInstance_ActiveLight_DirectLight(i);
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::SetShader(gContext.Get());



		gContext->OMSetRenderTargets(mRTVCollector_2->GetSize(), mRTVCollector_2->GetRTVs(), nullptr);


		mSRVCollector_2->BindSRVs_PS(gContext, 4);


		gContext->DrawIndexed(6, 0, 0);
	}


	for (int i = 0; i < gLights.mSpotLights.size(); ++i)
	{
		//Clear All Render Target Views

		gContext->ClearRenderTargetView(mTexView_AvgBlocker_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
		gContext->ClearRenderTargetView(mTexView_Shadows->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });

		//
		// Search Blocker Verticall On Shadow Map
		//

		//mPipeline.Apply_Preliminiary_Pass0();
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::Set_ClassInstance_ActiveLight_SpotLight(i);
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::SetShader(gContext.Get());


		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);


		mSRVCollector_0->BindSRVs_PS(gContext, 0);


		CalcLightDirInViewSpace_XZ(mData_CB_PerLight.gLight2DDirInEye_Horz, mData_CB_PerLight.gLight2DDirInEye_Vert, gLights.mSpotLights[i].GetILightData());
		mData_CB_PerLight.gShadowMapID = i;


		mCB_PerLight->Update(gContext.Get());


		gContext->DrawIndexed(6, 0, 0);


		//
		// Search Blocker Horizontally On Screen Space And Calculate Penumbra Size And Filter hard Shadows Vertically on Shadow Map
		//

		//mPipeline.Apply_Preliminiary_Pass1();
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::Set_ClassInstance_ActiveLight_SpotLight(i);
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::SetShader(gContext.Get());



		gContext->OMSetRenderTargets(mRTVCollector_1->GetSize(), mRTVCollector_1->GetRTVs(), nullptr);


		mSRVCollector_1->BindSRVs_PS(gContext, 3);


		gContext->DrawIndexed(6, 0, 0);


		//
		// Apply Filter on Vertically Filtered Shadows On Screen Space Horizontally
		//

		//mPipeline.Apply_Preliminiary_Pass2();
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::Set_ClassInstance_ActiveLight_SpotLight(i);
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::SetShader(gContext.Get());


		gContext->OMSetRenderTargets(mRTVCollector_2->GetSize(), mRTVCollector_2->GetRTVs(), nullptr);


		mSRVCollector_2->BindSRVs_PS(gContext, 4);


		gContext->DrawIndexed(6, 0, 0);
	}

	// 	for (int i = 0; i < gLights.GetShadowMapCount(); ++i)
	// 	{
	// 
	// 		//Clear All Render Target Views
	// 
	// 		gContext->ClearRenderTargetView(mTexView_AvgBlocker_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	// 		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
	// 		gContext->ClearRenderTargetView(mTexView_Shadows->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	// 		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	// 
	// 		//
	// 		// Search Blocker Verticall On Shadow Map
	// 		//
	// 
	// 		//mPipeline.Apply_Preliminiary_Pass0();
	// 		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::SetShader();
	// 
	// 
	// 		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);
	// 
	// 
	// 		mSRVCollector_0->BindSRVs_PS(gContext, 0);
	// 
	// 
	// 		CalcLightDirInViewSpace_XZ(mData_CB_PerLight.gLight2DDirInEye_Horz, mData_CB_PerLight.gLight2DDirInEye_Vert, gLights.mCollectionILights[i]->GetILightData());
	// 		mData_CB_PerLight.gShadowMapID = i;
	// 
	// 
	// 		mCB_PerLight->Update(gContext.Get());
	// 
	// 
	// 		gContext->DrawIndexed(6, 0, 0);
	// 
	// 
	// 		//
	// 		// Search Blocker Horizontally On Screen Space And Calculate Penumbra Size And Filter hard Shadows Vertically on Shadow Map
	// 		//
	// 
	// 		//mPipeline.Apply_Preliminiary_Pass1();
	// 		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::SetShader();
	// 
	// 
	// 
	// 		gContext->OMSetRenderTargets(mRTVCollector_1->GetSize(), mRTVCollector_1->GetRTVs(), nullptr);
	// 
	// 
	// 		mSRVCollector_1->BindSRVs_PS(gContext, 3);
	// 
	// 
	// 		gContext->DrawIndexed(6, 0, 0);
	// 
	// 
	// 		//
	// 		// Apply Filter on Vertically Filtered Shadows On Screen Space Horizontally
	// 		//
	// 
	// 		//mPipeline.Apply_Preliminiary_Pass2();
	// 		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::SetShader();
	// 
	// 
	// 		gContext->OMSetRenderTargets(mRTVCollector_2->GetSize(), mRTVCollector_2->GetRTVs(), nullptr);
	// 
	// 
	// 		mSRVCollector_2->BindSRVs_PS(gContext, 4);
	// 
	// 
	// 		gContext->DrawIndexed(6, 0, 0);
	// 	}

	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	gD3DUserDefinedAnnotation->EndEvent();
}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Draw2D()
{

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::DrawTxt()
{

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Update()
{

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::Destroy()
{

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::CaptureFrame()
{

}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::RecompileShaders()
{
	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::InitStatics();
	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::InitStatics();
	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::InitStatics();
}

void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::SetSRVs()
{
	mSRVCollector_0->ClearAndAddSRV(pTexViewDepthMap->GetSRV());
	mSRVCollector_0->AddSRV(pTexViewShadowMap->GetSRV());
	mSRVCollector_0->AddSRV(pTexViewNormalMap->GetSRV());
}

//void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::SetSRV(ID3D11ShaderResourceView * const ppDepthMap, ID3D11ShaderResourceView * const ppShadowMapArray, ID3D11ShaderResourceView * const ppGBuffer_Normal)
//{
//	mSRVCollector_0->ClearAndAddSRV(ppDepthMap);
//	mSRVCollector_0->AddSRV(ppShadowMapArray);
//	mSRVCollector_0->AddSRV(ppGBuffer_Normal);
//
//}


void RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::RenderConfigurationUI()
{
	ImGui::Begin("Optimized Screen Space Soft Shadows");

	if (ImGui::DragInt("Blocker Search Sample Count", &ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mBlockerSearchWidthSampleCount, 1.0f, 1, 100))
	{
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mBlockerSearchWidthSampleCount = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mBlockerSearchWidthSampleCount;
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mBlockerSearchWidthSampleCount = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mBlockerSearchWidthSampleCount;
	}

	if (ImGui::DragInt("Hard Shadow Filter Width Sample Count", &ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mHardShadowFilterSampleCount, 1.0f, 1, 100))
	{
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mHardShadowFilterSampleCount = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mHardShadowFilterSampleCount;
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mHardShadowFilterSampleCount = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mHardShadowFilterSampleCount;
	}

	if (ImGui::DragFloat("Bilateral Depth", &ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mBilateralDepth, 1.0f, 1.0f, 10000.0f))
	{
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass1::mBilateralDepth = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mBilateralDepth;
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass2::mBilateralDepth = ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch_Pass0::mBilateralDepth;
	}


	ImGui::End();
}

std::string RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::GPUTime_String()
{
	return "OSSSS Screen Space Separable Blocker Search Render Time : " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}

ID3D11ShaderResourceView* RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::GetSRV_Shadows() const
{
	return mTexView_Shadows->GetSRV();
}

const TexViews* RenderPass_OSSSS_GenerateShadowData_ScreenSpaceSeparableBlockerSearch::GetTexViewShadows() const noexcept
{
	return mTexView_Shadows.get();
}
