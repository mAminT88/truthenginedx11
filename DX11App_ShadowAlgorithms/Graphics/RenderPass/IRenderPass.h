#pragma once

//#include "Graphics/ConstantBuffer.h"
//#include "Graphics/Objects.h"
//#include "Graphics/TexViews.h"
//#include "Graphics/BufferViews.h"
//#include "Graphics/ResourceViewCollectors.h"
//#include "Graphics/StaticBuffers.h"
//
//#include "Objects/Model.h"
//
//#include "Graphics/ShaderSlots_DeferredShading.h"
//
//#include "Utility/GpuProfiler.h"

class GPUTimeStamp_DualBuffer;

class IRenderPass
{
	//
	// Inherited Render Classes
	//
protected:

	bool GPUResourceInitialized = false;

	std::unique_ptr<GPUTimeStamp_DualBuffer> mGpuTimeStampDB;

	ComPtr<ID3D11DeviceContext> mDeferredContext;
	ComPtr<ID3D11CommandList> mCommandList;

	//virtual void InitPipelines() {};
	virtual void ReleasePipelines();
	virtual void InitViews();
	virtual void ReleaseViews();
	virtual void InitBuffers();
	virtual void ReleaseBuffers();
	virtual void UpdateCBuffer();

public:
	IRenderPass();
	virtual ~IRenderPass();

	IRenderPass(IRenderPass&&);
	IRenderPass& operator = (IRenderPass&&);

	virtual void Init();
	virtual void Draw3D();
	virtual void Draw2D();
	virtual void DrawTxt();
	virtual void Update();
	virtual void Destroy();
	virtual void DestroyGPUResources();
	virtual void InitGPUResources();
	virtual void CaptureFrame();
	virtual void RecompileShaders();
	virtual void ProfileGPUTime(float GPUTime);
	virtual std::string GPUTime_String();
	virtual void RenderConfigurationUI();
	virtual void DrawDeferred();
	virtual ID3D11CommandList* GetCommandList();
};



class IRenderPass_SSAO : public IRenderPass
{
public:
	IRenderPass_SSAO();
	virtual ~IRenderPass_SSAO();

	IRenderPass_SSAO(IRenderPass_SSAO&&);
	IRenderPass_SSAO& operator=(IRenderPass_SSAO&&);

	virtual ID3D11ShaderResourceView* GetSSAOMap();
	virtual void SetSRVs(ID3D11ShaderResourceView* const depthMap, ID3D11ShaderResourceView* const normalMap);
};