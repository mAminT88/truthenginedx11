#pragma once

#include "IRenderPass.h"

template<class T> class ConstantBuffer;

class TexViews;


class RenderPass_GenerateShadowMaps : public IRenderPass
{
protected:

	struct CB_VS_BONETRANSFORMS
	{
		XMFLOAT4X4 gBoneTransforms[96];
	};

	struct CB_PER_OBJECT
	{
		XMFLOAT4X4 gWorld;
	};

	struct CB_PER_LIGHT
	{
		XMFLOAT4X4 gLightVP;
		XMFLOAT2 gZNearFar;
		XMFLOAT2 pad;
		XMFLOAT3 gLightPosition;
		float pad2;
	};

	
	CB_PER_OBJECT mDataCBPerObject;
	CB_PER_LIGHT  mDataCBPerLight;
	CB_VS_BONETRANSFORMS mDataCBBoneTransforms;


	std::unique_ptr<ConstantBuffer<CB_PER_OBJECT>> mCBPerObject;
	std::unique_ptr<ConstantBuffer<CB_PER_LIGHT>> mCBperLight;
	std::unique_ptr<ConstantBuffer<CB_VS_BONETRANSFORMS>> mCBBoneTransforms;


	D3D11_VIEWPORT mViewport;

	//PipelineList::Pipeline_GenerateShadowMap mPiplineList_NormalShadowMap;
	UINT mVertexOffset = 0;

	std::unique_ptr<TexViews> mTexView_ShadowMap_DSV;


	virtual void InitViews() override;
	virtual void ReleaseViews() override;
	virtual void InitBuffers() override;
	virtual void ReleaseBuffers() override;
	virtual void UpdateCBuffer() override;

public:

	RenderPass_GenerateShadowMaps();
	~RenderPass_GenerateShadowMaps();

	RenderPass_GenerateShadowMaps(RenderPass_GenerateShadowMaps&&);
	RenderPass_GenerateShadowMaps& operator=(RenderPass_GenerateShadowMaps&&);

	virtual void Init() override;
	virtual void Draw3D() override;
	virtual void DrawTxt() override;
	virtual void Destroy() override;
	virtual void DestroyGPUResources() override;
	virtual void InitGPUResources() override;
	virtual void RecompileShaders() override;
	virtual std::string GPUTime_String() override;

	ID3D11ShaderResourceView* GetShadowMapsDepth_SRV()const;

	const TexViews* GetTexViewShadowMap() const noexcept;
	//void SetObjects(std::vector<Model*>* Models);

};
