﻿#pragma once
#include "IRenderPass.h"

template<class T> class ConstantBuffer;
class TexViews;

class RenderPass_GenerateCascadedShadowMap : IRenderPass
{
	
	struct CB_PER_OBJECT
	{
		XMFLOAT4X4 gWorld;
	};

	struct CB_PER_LIGHT
	{
		XMFLOAT4X4 gLightVP;
		XMFLOAT2 gZNearFar;
		XMFLOAT2 pad;
		XMFLOAT3 gLightPosition;
		float pad2;
	};


	CB_PER_OBJECT mDataCBPerObject;
	CB_PER_LIGHT  mDataCBPerLight;


	std::unique_ptr<ConstantBuffer<CB_PER_OBJECT>> mCBPerObject;
	std::unique_ptr<ConstantBuffer<CB_PER_LIGHT>> mCBperLight;


	D3D11_VIEWPORT mViewport;
	
	UINT mVertexOffset = 0;

	std::unique_ptr<TexViews> mTexView_ShadowMap_DSV;


	virtual void InitViews() override;
	virtual void ReleaseViews() override;
	virtual void InitBuffers() override;
	virtual void ReleaseBuffers() override;

public:

	RenderPass_GenerateCascadedShadowMap();
	~RenderPass_GenerateCascadedShadowMap();

	RenderPass_GenerateCascadedShadowMap(RenderPass_GenerateCascadedShadowMap&&);
	RenderPass_GenerateCascadedShadowMap& operator=(RenderPass_GenerateCascadedShadowMap&&);

	virtual void Init() override;
	virtual void Draw3D() override;
	virtual void DestroyGPUResources() override;
	virtual void InitGPUResources() override;
	virtual void RecompileShaders() override;
	virtual std::string GPUTime_String() override;

	ID3D11ShaderResourceView* GetShadowMapsDepth_SRV()const;

	const TexViews* GetTexViewShadowMap() const noexcept;

	
};
