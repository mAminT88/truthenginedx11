#pragma once

#include "IRenderPass.h"


class TexViews;
class SRVCollector;
class RTVCollector;
template<typename T> class ConstantBuffer;

class RenderPass_SSSSM_GenerateShadowData : public IRenderPass
{
public:

	RenderPass_SSSSM_GenerateShadowData();
	~RenderPass_SSSSM_GenerateShadowData();

	RenderPass_SSSSM_GenerateShadowData(RenderPass_SSSSM_GenerateShadowData&&);
	RenderPass_SSSSM_GenerateShadowData& operator=(RenderPass_SSSSM_GenerateShadowData&&);

	const TexViews* pTexViewDepthMap;
	const TexViews* pTexViewShadowMap;
	const TexViews* pTexViewNormalMap;

	virtual void Init() override;
	virtual void InitGPUResources() override;
	virtual void DestroyGPUResources() override;
	virtual void Draw3D() override;
	virtual void Draw2D() override;
	virtual void DrawTxt() override;
	virtual void Update() override;
	virtual void Destroy() override;
	virtual void CaptureFrame() override;
	virtual void RecompileShaders() override;


	virtual void SetSRVs();
	virtual std::string GPUTime_String() override;

	ID3D11ShaderResourceView* GetSRV_Shadows() const;

	const TexViews* GetTexViewShadows() const noexcept;

protected:

	struct CB_PS_PER_FRAME
	{
		XMFLOAT4X4 gViewInvT;
	};

	struct CB_PS_PER_LIGHT
	{
		XMFLOAT4X4 gLightViewInv;
	};

	std::unique_ptr<ConstantBuffer<CB_PS_PER_LIGHT>> mCB_PerLight;
	CB_PS_PER_LIGHT mData_CB_PerLight;

	std::unique_ptr<ConstantBuffer<CB_PS_PER_FRAME>> mCB_PerFrame;
	CB_PS_PER_FRAME mData_CB_PerFrame;


	std::unique_ptr<TexViews> mTexView_AvgBlocker_Temp;
	std::unique_ptr<TexViews> mTexView_Shadows;
	std::unique_ptr<TexViews> mTexView_PenumbraSize;
	std::unique_ptr<TexViews> mTexView_Shadows_Temp;



	std::unique_ptr<SRVCollector> mSRVCollector_0;
	std::unique_ptr<SRVCollector> mSRVCollector_1;
	std::unique_ptr<SRVCollector> mSRVCollector_2;
	
	std::unique_ptr<RTVCollector> mRTVCollector_0;
	std::unique_ptr<RTVCollector> mRTVCollector_1;
	std::unique_ptr<RTVCollector> mRTVCollector_2;


	//PipelineList::Pipeline_SSSM_GenerateShadowData mPipeline;
	UINT mVertexOffset = 0;



	virtual void InitViews() override;


	virtual void ReleaseViews() override;


	virtual void InitBuffers() override;


	virtual void ReleaseBuffers() override;

};
