#pragma once

#include "IRenderPass.h"

class TexViews;
class BufferViews;

class RenderPass_Bloom : public IRenderPass
{
	
	ID3D11ShaderResourceView* SRVs[2];
	ID3D11UnorderedAccessView* UAVs[1];

	//PipelineList::Pipeline_DeferredShading mPipeline;
	UINT mVertexOffset = 0;
	UINT mDownScaleGroups;

	std::unique_ptr<TexViews> mTexView_Bloom;
		
	virtual void InitViews() override;
	virtual void ReleaseViews() override;
	void InitBuffers() override;
	void ReleaseBuffers() override;

public:

	RenderPass_Bloom();
	~RenderPass_Bloom();

	RenderPass_Bloom(RenderPass_Bloom&&);
	RenderPass_Bloom& operator =(RenderPass_Bloom&&);

	const TexViews* pTexViews_DownScaledHDR;
	const BufferViews* pBufferView_AvgLum;

	/*virtual void Init() override;*/
	virtual void Draw3D() override;
	//virtual void DrawDeferred() override;
	virtual void RecompileShaders() override;
	virtual void InitGPUResources() override;
	virtual void DestroyGPUResources() override;
	virtual std::string GPUTime_String() override;


	//void RenderConfigurationUI() override;
};
