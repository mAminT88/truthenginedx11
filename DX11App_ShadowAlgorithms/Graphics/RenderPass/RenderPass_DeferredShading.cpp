#include "stdafx.h"
#include "RenderPass_DeferredShading.h"
#include "Globals.h"
#include "D3D.h"

#include "Graphics/RenderTechnique/CCBuffers_DeferredShading.h"
#include "Graphics/TexViews.h"
#include "Graphics/ResourceViewCollectors.h"
#include "Graphics/RenderStates.h"
#include "Graphics/Colors.h"

#include "Utility/GpuProfiler.h"

//////Shaders
#include "Graphics\ShaderList\VS_Render2D.h"
#include "Graphics\ShaderList\PS_DeferredShading.h"


using namespace Globals;

#define SRV_SLOT_COLORMAP 0

void RenderPass_DeferredShading::InitViews()
{
	

	if (mEnableHDR)
	{

		mTexView_HDR->Init(D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
			, gClientHeight
			, gClientWidth
			, DXGI_FORMAT_R16G16B16A16_FLOAT);

	}
		mTexView_Color->Init(D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
			, gClientHeight
			, gClientWidth
			, DXGI_FORMAT_R8G8B8A8_UNORM);


	pRTV[0] = mEnableHDR ? mTexView_Color->GetRTV() : gSwapChain.GetBackBufferRTV();
	pRTV[1] = mEnableHDR ? mTexView_HDR->GetRTV() : nullptr;
}

void RenderPass_DeferredShading::ReleaseViews()
{
	mTexView_Color->Release();
	mTexView_HDR->Release();
}


void RenderPass_DeferredShading::InitBuffers()
{
	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
}

void RenderPass_DeferredShading::ReleaseBuffers()
{
	mGpuTimeStampDB->Destroy();
}

RenderPass_DeferredShading::RenderPass_DeferredShading()
{
	mTexView_Color.reset(new TexViews());
	mTexView_HDR.reset(new TexViews());
	mSRVCollector.reset(new SRVCollector());
}

RenderPass_DeferredShading::~RenderPass_DeferredShading()
{
}

RenderPass_DeferredShading::RenderPass_DeferredShading(RenderPass_DeferredShading&&)
{
}

RenderPass_DeferredShading& RenderPass_DeferredShading::operator=(RenderPass_DeferredShading&&)
{
	return std::move(*this);
}

void RenderPass_DeferredShading::Init()
{

	SetRTV(gSwapChain.GetBackBufferRTV());
}

void RenderPass_DeferredShading::Draw3D()
{

	//gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_DeferredShading");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());

	gContext->RSSetViewports(1, &gViewPort);


	gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
	gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);
	//gContext->IASetVertexBuffers(0, 1, StaticBuffers::gVertexBuffer_2D.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

	

	gContext->ClearRenderTargetView(pRTV[0], RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	gContext->OMSetRenderTargets(2, pRTV, nullptr);


	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	ShaderList::VertexShaders::VS_Render2D::SetShader(gContext.Get());

	if (mEnableHDR)
	{

		gContext->ClearRenderTargetView(pRTV[1], RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_OutputManager_PreProcess();
	}
	else {
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_OutputManager_Backbuffer();
	}


	ShaderList::PixelShaders::PS_DeferredShading::SetShader(gContext.Get());


	mSRVCollector->BindSRVs_PS(gContext, SRV_SLOT_COLORMAP);


	gContext->DrawIndexed(6, 0, 0);


	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	//gD3DUserDefinedAnnotation->EndEvent();
}

void RenderPass_DeferredShading::DrawDeferred()
{
	//gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_DeferredShading");

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, mDeferredContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH, mDeferredContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, mDeferredContext.Get());

	mDeferredContext->RSSetViewports(1, &gViewPort);

	mDeferredContext->IASetInputLayout(Vertex::IL_Basic32.Get());
	mDeferredContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	mDeferredContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);
	//mDeferredContext->IASetVertexBuffers(0, 1, StaticBuffers::gVertexBuffer_2D.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);



	mDeferredContext->ClearRenderTargetView(pRTV[0], RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	mDeferredContext->OMSetRenderTargets(2, pRTV, nullptr);


	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	ShaderList::VertexShaders::VS_Render2D::SetShader(mDeferredContext.Get());

	if (mEnableHDR)
	{

		mDeferredContext->ClearRenderTargetView(pRTV[1], RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_OutputManager_PreProcess();
	}
	else {
		ShaderList::PixelShaders::PS_DeferredShading::SetClassInstance_OutputManager_Backbuffer();
	}


	ShaderList::PixelShaders::PS_DeferredShading::SetShader(mDeferredContext.Get());


	mSRVCollector->BindSRVs_PS(mDeferredContext, SRV_SLOT_COLORMAP);


	mDeferredContext->DrawIndexed(6, 0, 0);

	//gD3DUserDefinedAnnotation->EndEvent();

	mDeferredContext->FinishCommandList(true, mCommandList.ReleaseAndGetAddressOf());
}

void RenderPass_DeferredShading::Draw2D()
{
}

void RenderPass_DeferredShading::DrawTxt()
{
}

void RenderPass_DeferredShading::Update()
{
}

void RenderPass_DeferredShading::Destroy()
{
}

void RenderPass_DeferredShading::CaptureFrame()
{
}

void RenderPass_DeferredShading::RecompileShaders()
{
	ShaderList::PixelShaders::PS_DeferredShading::InitStatics();
}

void RenderPass_DeferredShading::InitGPUResources()
{
	if (!GPUResourceInitialized)
	{
		gDevice->CreateDeferredContext(0, mDeferredContext.ReleaseAndGetAddressOf());

		D3D::SetGlobalContextStates(mDeferredContext.Get(), true, true, false);

		CCBuffers::SetCommonCBs(mDeferredContext.Get());

		InitBuffers();
		InitViews();


		GPUResourceInitialized = true;

	}

	SetSRVs();
}

void RenderPass_DeferredShading::DestroyGPUResources()
{
	ReleaseBuffers();
	ReleaseViews();

	mCommandList.Reset();
	mDeferredContext.Reset();

	GPUResourceInitialized = false;
}

std::string RenderPass_DeferredShading::GPUTime_String()
{
	return "Deferred Shading Render Time: " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}

//void RenderPass_DeferredShading::SetSRV(ID3D11ShaderResourceView* const ppColorMap,
//	ID3D11ShaderResourceView* const ppNormalMap,
//	ID3D11ShaderResourceView* const ppSpecularMap,
//	ID3D11ShaderResourceView* const ppDepthMap,
//	ID3D11ShaderResourceView* const ppShadowMapArray,
//	ID3D11ShaderResourceView* const ppSSAOMap,
//	ID3D11ShaderResourceView* const ppSATUINT)
//{
//	mSRVCollector->ClearAndAddSRV(ppColorMap, 7);
//	mSRVCollector->AddSRV(ppNormalMap);
//	mSRVCollector->AddSRV(ppSpecularMap);
//	mSRVCollector->AddSRV(ppDepthMap);
//	mSRVCollector->AddSRV(ppShadowMapArray);
//	mSRVCollector->AddSRV(ppSSAOMap);
//	mSRVCollector->AddSRV(ppSATUINT);
//}

void RenderPass_DeferredShading::SetSRVs()
{
	

	if(pTexViews_ColorMap) mSRVCollector->ClearAndAddSRV(pTexViews_ColorMap->GetSRV(), 7);
	if(pTexViews_NormalMap) mSRVCollector->AddSRV(pTexViews_NormalMap->GetSRV());
	if(pTexViews_SpecularMap) mSRVCollector->AddSRV(pTexViews_SpecularMap->GetSRV());
	if(pTexViews_DepthMap) mSRVCollector->AddSRV(pTexViews_DepthMap->GetSRV());
	if(pTexViews_ShadowMapArray) mSRVCollector->AddSRV(pTexViews_ShadowMapArray->GetSRV());
	if(pTexViews_SSAOMap) mSRVCollector->AddSRV(pTexViews_SSAOMap->GetSRV());
	if(pTexViews_SATUINT) mSRVCollector->AddSRV(pTexViews_SATUINT->GetSRV());
}

void RenderPass_DeferredShading::SetSRV_SSAO(ID3D11ShaderResourceView* ssaoMap)
{
	mSRVCollector->ReplaceSRV(ssaoMap, 5);
}

void RenderPass_DeferredShading::SetRTV(ID3D11RenderTargetView* rtv)
{
	pRTV[0] = rtv;
}

const TexViews* RenderPass_DeferredShading::GetLuminanceMap() const noexcept
{
	return mTexView_HDR.get();
}

const TexViews* RenderPass_DeferredShading::GetColorMap() const noexcept
{
	return mTexView_Color.get();
}

void RenderPass_DeferredShading::SetHDR(bool _enable)
{
	mEnableHDR = _enable;
}

void RenderPass_DeferredShading::RenderConfigurationUI()
{
	ImGui::Begin("Settings: Deferred Shading");

	ImGui::DragInt("PCF Sample Count", &ShaderList::PixelShaders::PS_DeferredShading::mPCFSamplesCount, 1.0f, 1, 100);

	ImGui::DragInt("PCSS Sample Count", &ShaderList::PixelShaders::PS_DeferredShading::mPCSSSamplesCount, 1.0f, 1, 100);

	ImGui::DragInt("PCSS Blocker Search Sample Count", &ShaderList::PixelShaders::PS_DeferredShading::mPCSSBlockerSearchSampleCount, 1.0f, 1, 100);

	ImGui::End();
}
