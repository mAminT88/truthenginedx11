#include "stdafx.h"
#include "RenderPass_SSSSM_GenerateShadowData.h"
#include "Globals.h"

#include "Graphics/RenderStates.h"
#include "Graphics/TexViews.h"
#include "Graphics/ResourceViewCollectors.h"
#include "Graphics/ConstantBuffer.h"
#include "Graphics/Colors.h"
#include "Graphics/ShaderSlots_DeferredShading.h"

#include "Utility/GpuProfiler.h"


/////Shaders
#include <Graphics\ShaderList\VS_Render2D.h>
#include <Graphics\ShaderList\PS_SSSM_GenerateShadowData.h>


using namespace Globals;

RenderPass_SSSSM_GenerateShadowData::RenderPass_SSSSM_GenerateShadowData() {
	mTexView_Shadows.reset(new TexViews());
	mTexView_PenumbraSize.reset(new TexViews());
	mTexView_Shadows_Temp.reset(new TexViews());
	mTexView_AvgBlocker_Temp.reset(new TexViews());
	mRTVCollector_0.reset(new RTVCollector());
	mRTVCollector_1.reset(new RTVCollector());
	mRTVCollector_2.reset(new RTVCollector());
	mSRVCollector_0.reset(new SRVCollector());
	mSRVCollector_1.reset(new SRVCollector());
	mSRVCollector_2.reset(new SRVCollector());
}
RenderPass_SSSSM_GenerateShadowData::~RenderPass_SSSSM_GenerateShadowData() = default;

RenderPass_SSSSM_GenerateShadowData::RenderPass_SSSSM_GenerateShadowData(RenderPass_SSSSM_GenerateShadowData&&) = default;

RenderPass_SSSSM_GenerateShadowData& RenderPass_SSSSM_GenerateShadowData::operator=(RenderPass_SSSSM_GenerateShadowData&&) = default;

void RenderPass_SSSSM_GenerateShadowData::Init()
{
	//InitPipelines();
}

void RenderPass_SSSSM_GenerateShadowData::InitGPUResources()
{
	if (!GPUResourceInitialized)
	{
		InitBuffers();
		InitViews();

		GPUResourceInitialized = true;
	}

	SetSRVs();

}

void RenderPass_SSSSM_GenerateShadowData::DestroyGPUResources()
{
	ReleaseViews();
	ReleaseBuffers();

	GPUResourceInitialized = false;
}

void RenderPass_SSSSM_GenerateShadowData::Draw3D()
{

	gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_SSSM_GenerateShadowData");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	XMStoreFloat4x4(&mData_CB_PerFrame.gViewInvT, XMMatrixTranspose(gCamera_main.ViewInv()));

	mCB_PerFrame->Update(gContext.Get());

	gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
	gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());

	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	ShaderList::VertexShaders::VS_Render2D::SetShader(gContext.Get());

	ID3D11UnorderedAccessView* uav;

	gContext->RSSetViewports(1, &gViewPort);


	for (int i = 0; i < gLights.mDirectionalLights.size(); ++i)
	{

		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::Set_ClassInstance_ActiveLight_DirectLight(i);
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::Set_ClassInstance_ActiveLight_DirectLight(i);
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::Set_ClassInstance_ActiveLight_DirectLight(i);

		auto& dlight = gLights.mDirectionalLights[i];

		auto viewInv = XMMatrixInverse(nullptr, XMLoadFloat4x4(&dlight.GetILightData().View));

		int ShadowMapID = dlight.GetILightData().ShadowMapID;

		XMStoreFloat4x4(&mData_CB_PerLight.gLightViewInv, viewInv);

		mCB_PerLight->Update(gContext.Get());


		mRTVCollector_0->ReplaceRTV(mTexView_Shadows->GetRTV(ShadowMapID), 0);
		mRTVCollector_2->ReplaceRTV(mTexView_Shadows->GetRTV(ShadowMapID), 0);

		//Pass 0 : Generate Hard Shadows And Find Blockers On horizontal Direction

		//mPipeline.Apply_Preliminiary_0();
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::SetShader(gContext.Get());

		gContext->ClearRenderTargetView(mTexView_Shadows->GetRTV(ShadowMapID), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		gContext->ClearRenderTargetView(mTexView_AvgBlocker_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });


		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);


		mSRVCollector_0->BindSRVs_PS(gContext, 0);


		gContext->DrawIndexed(6, 0, 0);

		//Pass 1 : Calculate penumbra size and Apply Horizontal Gaussian Filter on Hard Shadows

		//mPipeline.Apply_Preliminiary_1();
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::SetShader(gContext.Get());


		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });


		gContext->OMSetRenderTargets(mRTVCollector_1->GetSize(), mRTVCollector_1->GetRTVs(), nullptr);


		mSRVCollector_1->BindSRVs_PS(gContext, 3);


		gContext->DrawIndexed(6, 0, 0);

		//Pass 2 : Apply Vertical Gaussian Filter on Horizontally Filtered Shadows

		//mPipeline.Apply_Preliminiary_2();
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::SetShader(gContext.Get());


		gContext->OMSetRenderTargets(mRTVCollector_2->GetSize(), mRTVCollector_2->GetRTVs(), nullptr);


		mSRVCollector_2->BindSRVs_PS(gContext, 4);


		gContext->DrawIndexed(6, 0, 0);
	}

	for (int i = 0; i < gLights.mSpotLights.size(); ++i)
	{

		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::Set_ClassInstance_ActiveLight_SpotLight(i);
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::Set_ClassInstance_ActiveLight_SpotLight(i);
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::Set_ClassInstance_ActiveLight_SpotLight(i);

		auto& slight = gLights.mSpotLights[i];

		auto viewInv = XMMatrixInverse(nullptr, XMLoadFloat4x4(&slight.GetILightData().View));

		int ShadowMapID = slight.GetILightData().ShadowMapID;

		XMStoreFloat4x4(&mData_CB_PerLight.gLightViewInv, viewInv);

		mCB_PerLight->Update(gContext.Get());


		mRTVCollector_0->ReplaceRTV(mTexView_Shadows->GetRTV(ShadowMapID), 0);
		mRTVCollector_2->ReplaceRTV(mTexView_Shadows->GetRTV(ShadowMapID), 0);

		//Pass 0 : Generate Hard Shadows And Find Blockers On horizontal Direction

		//mPipeline.Apply_Preliminiary_0();
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_0::SetShader(gContext.Get());

		gContext->ClearRenderTargetView(mTexView_Shadows->GetRTV(ShadowMapID), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		gContext->ClearRenderTargetView(mTexView_AvgBlocker_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });


		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);


		mSRVCollector_0->BindSRVs_PS(gContext, 0);


		gContext->DrawIndexed(6, 0, 0);

		//Pass 1 : Calculate penumbra size and Apply Horizontal Gaussian Filter on Hard Shadows

		//mPipeline.Apply_Preliminiary_1();
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_1::SetShader(gContext.Get());


		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });


		gContext->OMSetRenderTargets(mRTVCollector_1->GetSize(), mRTVCollector_1->GetRTVs(), nullptr);


		mSRVCollector_1->BindSRVs_PS(gContext, 3);


		gContext->DrawIndexed(6, 0, 0);

		//Pass 2 : Apply Vertical Gaussian Filter on Horizontally Filtered Shadows

		//mPipeline.Apply_Preliminiary_2();
		ShaderList::PixelShaders::PS_SSSM_GenerateShadowData_2::SetShader(gContext.Get());


		gContext->OMSetRenderTargets(mRTVCollector_2->GetSize(), mRTVCollector_2->GetRTVs(), nullptr);


		mSRVCollector_2->BindSRVs_PS(gContext, 4);


		gContext->DrawIndexed(6, 0, 0);
	}

	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	gD3DUserDefinedAnnotation->EndEvent();

}

void RenderPass_SSSSM_GenerateShadowData::Draw2D()
{

}

void RenderPass_SSSSM_GenerateShadowData::DrawTxt()
{

}

void RenderPass_SSSSM_GenerateShadowData::Update()
{

}

void RenderPass_SSSSM_GenerateShadowData::Destroy()
{

}

void RenderPass_SSSSM_GenerateShadowData::CaptureFrame()
{

}

void RenderPass_SSSSM_GenerateShadowData::RecompileShaders()
{

}


void RenderPass_SSSSM_GenerateShadowData::SetSRVs()
{
	mSRVCollector_0->ClearAndAddSRV(pTexViewShadowMap->GetSRV());
	mSRVCollector_0->AddSRV(pTexViewDepthMap->GetSRV());
	mSRVCollector_0->AddSRV(pTexViewNormalMap->GetSRV());
}

ID3D11ShaderResourceView* RenderPass_SSSSM_GenerateShadowData::GetSRV_Shadows() const
{
	return mTexView_Shadows->GetSRV();
}


const TexViews* RenderPass_SSSSM_GenerateShadowData::GetTexViewShadows() const noexcept
{
	return mTexView_Shadows.get();
}

std::string RenderPass_SSSSM_GenerateShadowData::GPUTime_String()
{
	return "SSSM Generate Shadow Data's Render Time : " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}

//void RenderPass_SSSSM_GenerateShadowData::InitPipelines()
//{
//	mPipeline.Init();
//}

void RenderPass_SSSSM_GenerateShadowData::InitViews()
{
	auto lightCount = gLights.GetShadowMapCount();

	

	mTexView_Shadows->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, lightCount
		, 1
		, 1
		, 0
		, 0
		, true
	);

	mTexView_PenumbraSize->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R32_FLOAT
	);


	mTexView_Shadows_Temp->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, 1
		, 1
		, 1
		, 0
		, 0
		, true
	);

	mTexView_AvgBlocker_Temp->Init(
		D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight
		, gClientWidth
		, DXGI_FORMAT_R16G16_FLOAT
	);

	//Set RTVs

	

	mRTVCollector_0->ClearAndAddRTV(mTexView_Shadows->GetRTV());
	mRTVCollector_0->AddRTV(mTexView_AvgBlocker_Temp->GetRTV());



	mRTVCollector_1->ClearAndAddRTV(mTexView_PenumbraSize->GetRTV());
	mRTVCollector_1->AddRTV(mTexView_Shadows_Temp->GetRTV());



	mRTVCollector_2->ClearAndAddRTV(mTexView_Shadows->GetRTV());

	//Set SRVs



	mSRVCollector_1->ClearAndAddSRV(mTexView_AvgBlocker_Temp->GetSRV());
	mSRVCollector_1->AddSRV(mTexView_Shadows->GetSRV());



	mSRVCollector_2->ClearAndAddSRV(mTexView_Shadows_Temp->GetSRV());
	mSRVCollector_2->AddSRV(mTexView_PenumbraSize->GetSRV());
}

void RenderPass_SSSSM_GenerateShadowData::ReleaseViews()
{
	mTexView_AvgBlocker_Temp->Release();
	mTexView_PenumbraSize->Release();
	mTexView_Shadows->Release();
	mTexView_Shadows_Temp->Release();
}

void RenderPass_SSSSM_GenerateShadowData::InitBuffers()
{
	mCB_PerLight.reset(new ConstantBuffer<CB_PS_PER_LIGHT>());
	mCB_PerFrame.reset(new ConstantBuffer<CB_PS_PER_FRAME>());

	mCB_PerLight->InitConstantBuffer(SHADER_SLOTS_PS_CB_SSSM_GENERATESHADOWDATA
		, CONSTANT_BUFFER_PS
		, &mData_CB_PerLight
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_PerLight->SetConstantBuffer(gContext.Get());


	mCB_PerFrame->InitConstantBuffer(SHADER_SLOTS_PS_CB_SSSM_PERFRAME
		, CONSTANT_BUFFER_PS
		, &mData_CB_PerFrame
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_PerFrame->SetConstantBuffer(gContext.Get());

	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
}

void RenderPass_SSSSM_GenerateShadowData::ReleaseBuffers()
{
	mCB_PerLight->Release();

	mCB_PerFrame->Release();

	mGpuTimeStampDB->Destroy();

	mCB_PerLight.reset();
	mCB_PerFrame.reset();
	mGpuTimeStampDB.reset();
}
