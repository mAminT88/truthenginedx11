#pragma once

#include "IRenderPass.h"

class TexViews;
template<class T> class ConstantBuffer;

class RenderPass_GenerateMomentShadowMap : public IRenderPass
{
private:

	int SampleCountPerSATPass = 8;
	int SATPassCount;


	UINT SAT_ThreadGroupCountX = 0, SAT_ThreadGroupCountY = 0, SAT_ThreadGroupCountZ = 1;

	bool mUseComputeShaderForSAT = false;

	struct CB_PS_GAUSSIANFILTER_PERLIGHT
	{
		XMFLOAT2 gTexelSize;
		int gTexArrayIndex;
		float pad;
	};

	struct CB_VS_PER_OBJECT
	{
		XMFLOAT4X4 gWorld;
	};

	struct CB_VS_PER_LIGHT
	{
		XMFLOAT4X4 gLightVP;
		XMFLOAT2 gZNearFar;
		XMFLOAT2 pad;
		XMFLOAT3 gLightPosition;
		float pad2;
	};

	struct CB_PS_GENERATESAT
	{
		float gIteration;
		float gShadowMapID;
		XMFLOAT2 pad;
	};

	struct CB_CS_GENERATESAT
	{
		int32_t gShadowMapID;
		XMFLOAT3 pad0;
	};


	CB_VS_PER_OBJECT mDataCBPerObject;
	CB_VS_PER_LIGHT  mDataCBPerLight;
	//CB_PS_GAUSSIANFILTER_PERLIGHT mData_CB_GaussianFilter_PerLight;
	CB_PS_GENERATESAT mData_CB_GenerateSAT;
	CB_CS_GENERATESAT mData_CB_GenerateSAT_CS;


	std::unique_ptr<ConstantBuffer<CB_VS_PER_OBJECT>> mCB_VS_PerObject;
	std::unique_ptr<ConstantBuffer<CB_VS_PER_LIGHT>> mCB_VS_perLight;
	//std::unique_ptr<ConstantBuffer<CB_PS_GAUSSIANFILTER_PERLIGHT>> mCB_PS_GaussianFilter_PerLight;
	std::unique_ptr<ConstantBuffer<CB_PS_GENERATESAT>> mCB_PS_GenerateSAT;
	std::unique_ptr<ConstantBuffer<CB_CS_GENERATESAT>> mCB_CS_GenerateSAT;


	D3D11_VIEWPORT mViewport;

	UINT mVertexOffset = 0;

	std::unique_ptr<TexViews> mTexView_DSV;
	std::unique_ptr<TexViews> mTexView_MomentShadowMap;
	std::unique_ptr<TexViews> mTexView_SAT_UINT;
	std::unique_ptr<TexViews> mTexView_SAT_Temp;
	std::unique_ptr<TexViews> mTexView_SAT_UINT_UAV;
	std::unique_ptr<TexViews> mTexView_SAT_Temp_UAV;


	//ShaderList::VertexShaders::VS_BuildShadowMap mVertexShader_BuildShadowMap;
	//ShaderList::VertexShaders::VS_Render2D       mVertexShader_Render2D;

	//ShaderList::PixelShaders::PS_GenerateMomentShadowMap mPixelShader_GenerateMomentShadowMap;

	//ShaderList::ComputeShaders::CS_GenerateSAT_Horz mComputeShaderHorz;
	//ShaderList::ComputeShaders::CS_GenerateSAT_Vert mComputeShaderVert;


	virtual void InitViews() override;


	virtual void ReleaseViews() override;


	virtual void InitBuffers() override;


	virtual void ReleaseBuffers() override;

	ID3D11RenderTargetView* GetSATRTV(int i, int rtv_index);
	ID3D11ShaderResourceView* GetSATSRV(int i);

public:

	RenderPass_GenerateMomentShadowMap();
	~RenderPass_GenerateMomentShadowMap();

	RenderPass_GenerateMomentShadowMap(RenderPass_GenerateMomentShadowMap&&);
	RenderPass_GenerateMomentShadowMap& operator=(RenderPass_GenerateMomentShadowMap&&);

	virtual void Init() override;


	virtual void Draw3D() override;


	virtual void Draw2D() override;


	virtual void DrawTxt() override;


	virtual void Update() override;


	virtual void Destroy() override;


	virtual void DestroyGPUResources() override;


	virtual void InitGPUResources() override;


	virtual void RecompileShaders() override;


	virtual std::string GPUTime_String() override;

	//void SetObjects(std::vector<Model*>* Models);

	ID3D11ShaderResourceView* GetMomentShadowMap_SRV()const;
	ID3D11ShaderResourceView* GetSAT4Moments_UINT_SRV()const;

	void ChangeSATVersion(bool useComputeVersion);

	const TexViews* GetTexViewSAT_UINT() const noexcept;


};

