#pragma once

#include "IRenderPass.h"


class TexViews;
template<class T> class ConstantBuffer;

class RenderPass_GenerateSAT : public IRenderPass
{
protected:

	int SampleCountPerSATPass = 8;
	int SATPassCount;

	struct CB_VS_PER_OBJECT
	{
		XMFLOAT4X4 gWorld;
	};


	struct CB_VS_PER_LIGHT
	{
		XMFLOAT4X4 gLightVP;
		XMFLOAT2 gZNearFar;
		XMFLOAT2 pad;
		XMFLOAT3 gLightPosition;
		float pad2;
	};



	struct CB_PS_GENERATESAT
	{
		float gIteration;
		float gShadowMapID;
		XMFLOAT2 pad;
	};


	CB_VS_PER_OBJECT mData_CB_PerObject;
	CB_VS_PER_LIGHT  mData_CB_PerLight;
	CB_PS_GENERATESAT mData_CB_GenerateSAT;


	std::unique_ptr<ConstantBuffer<CB_VS_PER_OBJECT>> mCB_VS_PerObject;
	std::unique_ptr<ConstantBuffer<CB_VS_PER_LIGHT>> mCB_VS_perLight;
	std::unique_ptr<ConstantBuffer<CB_PS_GENERATESAT>> mCB_PS_GenerateSAT;


	D3D11_VIEWPORT mViewport;


	//PipelineList::Pipeline_GenerateVarianceShadowMap mPipeline_GenerateVarianceShadowMap;
	//PipelineList::Pipeline_GenerateSAT mPipeline_GenerateSAT;


	std::unique_ptr<TexViews> mTexView_DSV;
	std::unique_ptr<TexViews> mTexView_SATVarianceShadowMap;
	std::unique_ptr<TexViews> mTexView_Temp_SAT;


	UINT mVertexOffset = 0;


	//std::vector<Model*>* mModels;


	//virtual void InitPipelines() override;


	virtual void InitViews() override;


	virtual void ReleaseViews() override;


	virtual void InitBuffers() override;


	virtual void ReleaseBuffers() override;


public:

	RenderPass_GenerateSAT();
	~RenderPass_GenerateSAT();

	RenderPass_GenerateSAT(RenderPass_GenerateSAT&&);
	RenderPass_GenerateSAT& operator=(RenderPass_GenerateSAT&&);


	virtual void Init() override;


	virtual void Draw3D() override;


	virtual void Draw2D() override;


	virtual void DrawTxt() override;


	virtual void Update() override;


	virtual void Destroy() override;


	virtual void DestroyGPUResources() override;


	virtual void InitGPUResources() override;


	virtual void RecompileShaders() override;

	//void SetObjects(std::vector<Model*>* Models);

	virtual std::string GPUTime_String() override;

	ID3D11ShaderResourceView* GetSAVarianceShadowMap_SRV()const;

	const TexViews* GetTexViewVarianceShadowMap() const noexcept;

};
