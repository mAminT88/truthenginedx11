#include "stdafx.h"
#include "RenderPass_Bloom.h"
#include "Globals.h"
#include "D3DUtil.h"

#include "Graphics/ShaderList/CS_Bloom.h"
#include "Graphics/TexViews.h"
#include "Graphics/BufferViews.h"

#include "Utility/GpuProfiler.h"

using namespace Globals;

void RenderPass_Bloom::InitViews()
{
	

	mTexView_Bloom->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS
		, gClientHeight / 4
		, gClientWidth / 4
		, DXGI_FORMAT_R16G16B16A16_FLOAT);
}

void RenderPass_Bloom::ReleaseViews()
{
	mTexView_Bloom->Release();
}

void RenderPass_Bloom::InitBuffers()
{
	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
}

void RenderPass_Bloom::ReleaseBuffers()
{
	mGpuTimeStampDB->Destroy();
}

RenderPass_Bloom::RenderPass_Bloom()
{
	mTexView_Bloom.reset(new TexViews());
}

RenderPass_Bloom::~RenderPass_Bloom() = default;

RenderPass_Bloom::RenderPass_Bloom(RenderPass_Bloom&&) = default;
RenderPass_Bloom& RenderPass_Bloom::operator=(RenderPass_Bloom&&) = default;

void RenderPass_Bloom::Draw3D()
{

	SetNullRTVDSV(gContext.Get());

}

void RenderPass_Bloom::RecompileShaders()
{

}

void RenderPass_Bloom::InitGPUResources()
{
	if (!GPUResourceInitialized)
	{
		InitViews();
		InitBuffers();

		GPUResourceInitialized = true;
	}

	SRVs[0] = pTexViews_DownScaledHDR->GetSRV();
	SRVs[1] = pBufferView_AvgLum->GetSRV();

	UAVs[0] = mTexView_Bloom->GetUAV();

}

void RenderPass_Bloom::DestroyGPUResources()
{
	ReleaseViews();
	ReleaseBuffers();

	GPUResourceInitialized = false;
}

std::string RenderPass_Bloom::GPUTime_String()
{
	return "";
}

