#include "stdafx.h"
#include "IRenderPass.h"
#include "Globals.h"

#include "Utility/GpuProfiler.h"

using namespace Globals;

void IRenderPass::ProfileGPUTime(float GPUTime)
{
		mGpuTimeStampDB->WaitAndCalculateTime(gLastFrame, GPUTime, gContext.Get());
}

std::string IRenderPass::GPUTime_String()
{
	return "";
}

void IRenderPass::RenderConfigurationUI()
{
}

void IRenderPass::DrawDeferred()
{
}

ID3D11CommandList* IRenderPass::GetCommandList()
{
	return mCommandList.Get();
}

void IRenderPass::ReleasePipelines()
{
}

void IRenderPass::InitViews()
{
}

void IRenderPass::ReleaseViews()
{
}

void IRenderPass::InitBuffers()
{
}

void IRenderPass::ReleaseBuffers()
{
}

void IRenderPass::UpdateCBuffer()
{
}

IRenderPass::IRenderPass()
{
}

IRenderPass::~IRenderPass()
{
}

IRenderPass::IRenderPass(IRenderPass&&)
{
}

IRenderPass& IRenderPass::operator=(IRenderPass&&)
{
	return std::move(*this);
}

void IRenderPass::Init()
{
}

void IRenderPass::Draw3D()
{
}

void IRenderPass::Draw2D()
{
}

void IRenderPass::DrawTxt()
{
}

void IRenderPass::Update()
{
}

void IRenderPass::Destroy()
{
}

void IRenderPass::DestroyGPUResources()
{
}

void IRenderPass::InitGPUResources()
{
}

void IRenderPass::CaptureFrame()
{
}

void IRenderPass::RecompileShaders()
{
}

IRenderPass_SSAO::IRenderPass_SSAO() = default;

IRenderPass_SSAO::~IRenderPass_SSAO() = default;

IRenderPass_SSAO::IRenderPass_SSAO(IRenderPass_SSAO&&) = default;

IRenderPass_SSAO& IRenderPass_SSAO::operator=(IRenderPass_SSAO&&) = default;

ID3D11ShaderResourceView* IRenderPass_SSAO::GetSSAOMap()
{
	return nullptr;
}

void IRenderPass_SSAO::SetSRVs(ID3D11ShaderResourceView* const depthMap, ID3D11ShaderResourceView* const normalMap)
{
}
