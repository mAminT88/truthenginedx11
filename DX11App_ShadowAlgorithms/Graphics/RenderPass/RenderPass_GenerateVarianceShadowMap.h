#pragma once

#include "IRenderPass.h"

class TexViews;
template<class T> class ConstantBuffer;

class RenderPass_GenerateVarianceShadowMap : public IRenderPass
{

protected:


	struct CB_PS_VSM_PERLIGHT 
	{
		XMFLOAT2 gTexelSize;
		int gTexArrayIndex;
		float pad;
	};

	struct CB_VS_PER_OBJECT
	{
		XMFLOAT4X4 gWorld;
	};

	struct CB_VS_PER_LIGHT
	{
		XMFLOAT4X4 gLightVP;
		XMFLOAT2 gZNearFar;
		XMFLOAT2 pad;
		XMFLOAT3 gLightPosition;
		float pad2;
	};


	CB_VS_PER_OBJECT mDataCBPerObject;
	CB_VS_PER_LIGHT  mDataCBPerLight;
	CB_PS_VSM_PERLIGHT mData_CB_VSM_PerLight;


	std::unique_ptr<ConstantBuffer<CB_VS_PER_OBJECT>> mCB_VS_PerObject;
	std::unique_ptr<ConstantBuffer<CB_VS_PER_LIGHT>> mCB_VS_perLight;
	std::unique_ptr<ConstantBuffer<CB_PS_VSM_PERLIGHT>> mCB_PS_VSM_PerLight;


	D3D11_VIEWPORT mViewport;

	//ShaderList::VertexShaders::VS_BuildShadowMap mVertexShader_BuildShadowMap;
	//ShaderList::VertexShaders::VS_Render2D mVertexShader_Render2D;
	//
	//
	//ShaderList::PixelShaders::PS_GenerateVarianceShadowMap mPixelShader_GenerateVSM;


	std::unique_ptr<TexViews> mTexView_DSV;
	std::unique_ptr<TexViews> mTexView_VarianceShadowMap;
	std::unique_ptr<TexViews> mTexView_TempFilteredVarianceShadowMap;


	UINT mVertexOffset = 0;

	std::function<void(ID3D11DeviceContext* context)> SetShader;
	std::function<void()> SetFilter_ClassInstance_Horizontally;
	std::function<void()> SetFilter_ClassInstance_Vertically;

	//std::vector<Model*>* mModels;

	//virtual void InitPipelines() override;


	virtual void InitViews() override;


	virtual void ReleaseViews() override;


	virtual void InitBuffers() override;


	virtual void ReleaseBuffers() override;


public:

	RenderPass_GenerateVarianceShadowMap();
	~RenderPass_GenerateVarianceShadowMap();

	RenderPass_GenerateVarianceShadowMap(RenderPass_GenerateVarianceShadowMap&&);
	RenderPass_GenerateVarianceShadowMap& operator=(RenderPass_GenerateVarianceShadowMap&&);

	virtual void Init() override;


	virtual void Draw3D() override;


	virtual void Draw2D() override;


	virtual void DrawTxt() override;


	virtual void Update() override;


	virtual void Destroy() override;


	virtual void DestroyGPUResources() override;


	virtual void InitGPUResources() override;


	virtual void RecompileShaders() override;

	//void SetObjects(std::vector<Model*>* Models);


	virtual std::string GPUTime_String() override;

	virtual void RenderConfigurationUI() override;

	ID3D11ShaderResourceView* GetVarianceShadowMap_SRV()const;

	const TexViews* GetTexViewVarianceShadowMap() const noexcept;

};
