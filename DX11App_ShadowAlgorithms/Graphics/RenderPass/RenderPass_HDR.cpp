#include "stdafx.h"
#include "RenderPass_HDR.h"
#include "Globals.h"

#include "Graphics/RenderStates.h"
#include "Graphics/TexViews.h"
#include "Graphics/BufferViews.h"
#include "Graphics/ResourceViewCollectors.h"
#include "Graphics/Colors.h"

#include "Utility/GpuProfiler.h"

#include "Graphics/ShaderList/VS_Render2D.h"
#include "Graphics/ShaderList/CS_HDR_AvgLuminance.h"
#include "Graphics/ShaderList/PS_PostFX_FinalPass.h"
#include "Graphics/ShaderList/CS_Bloom.h"
#include "Graphics/ShaderList/CS_GaussianBlur.h"

using namespace Globals;

void RenderPass_HDR::InitViews()
{


	mTexView_DownScaledHDR->Init(D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE
		, gClientHeight / 4
		, gClientWidth / 4
		, DXGI_FORMAT_R16G16B16A16_FLOAT
	);

	mTexView_Bloom->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS
		, gClientHeight / 4
		, gClientWidth / 4
		, DXGI_FORMAT_R16G16B16A16_FLOAT);

	mTexView_Bloom_Temp->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_UNORDERED_ACCESS
		, gClientHeight / 4
		, gClientWidth / 4
		, DXGI_FORMAT_R16G16B16A16_FLOAT);
}

void RenderPass_HDR::ReleaseViews()
{
	mTexView_DownScaledHDR->Release();
	mTexView_Bloom->Release();
	mTexView_Bloom_Temp->Release();
}

void RenderPass_HDR::InitBuffers()
{

	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer{ 2 });

	mBuffer_DownScale1D.reset(new BufferViews());
	mBuffer_AvgLum_0.reset(new BufferViews());
	mBuffer_AvgLum_1.reset(new BufferViews());

	float iData = 1.0f;

	D3D11_SUBRESOURCE_DATA initData;
	initData.pSysMem = &iData;
	initData.SysMemPitch = 0;
	initData.SysMemSlicePitch = 0;

	mBuffer_DownScale1D->Init(D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE
		, sizeof(float)
		, mDownScaleGroups * sizeof(float)
		, mDownScaleGroups
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, D3D11_RESOURCE_MISC_BUFFER_STRUCTURED);

	mBuffer_AvgLum_0->Init(D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE
		, sizeof(float)
		, sizeof(float)
		, 1
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, D3D11_RESOURCE_MISC_BUFFER_STRUCTURED
		, &initData);

	mBuffer_AvgLum_1->Init(D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE
		, sizeof(float)
		, sizeof(float)
		, 1
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, D3D11_RESOURCE_MISC_BUFFER_STRUCTURED
		, &initData);
}

void RenderPass_HDR::ReleaseBuffers()
{
	mGpuTimeStampDB->Destroy();

	mBuffer_DownScale1D->Release();
	mBuffer_AvgLum_0->Release();
	mBuffer_AvgLum_1->Release();

	mBuffer_DownScale1D.reset();
	mBuffer_AvgLum_0.reset();
	mBuffer_AvgLum_1.reset();
}

RenderPass_HDR::RenderPass_HDR() : mDownScaleGroups(0), frame(0), Enable_Bloom(false)
{
	mTexView_DownScaledHDR.reset(new TexViews());
	mTexView_Bloom.reset(new TexViews());
	mTexView_Bloom_Temp.reset(new TexViews());
}

RenderPass_HDR::~RenderPass_HDR() = default;

RenderPass_HDR::RenderPass_HDR(RenderPass_HDR&&) = default;

RenderPass_HDR& RenderPass_HDR::operator=(RenderPass_HDR&&) = default;


void RenderPass_HDR::Init()
{

}

void RenderPass_HDR::Draw3D()
{
	//gContext->ExecuteCommandList(mCommandList.Get(), true);

	FLOAT clearValue[] = { 0.0f, 0.0f, 0.0f, 0.0f };

	/*gContext->ClearUnorderedAccessViewFloat(mBuffer_DownScale1D->GetUAV(), clearValue);
	gContext->ClearUnorderedAccessViewFloat(mBuffer_AvgLum_0->GetUAV(), clearValue);
	gContext->ClearUnorderedAccessViewFloat(mBuffer_AvgLum_1->GetUAV(), clearValue);*/

	gD3DUserDefinedAnnotation->BeginEvent(L"HDR Pass");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	//////////////////////////////////////////////////////////////////////////
	//First Pass

	ID3D11RenderTargetView* nullrtv[] = { nullptr };
	ID3D11ShaderResourceView* srvnullptr[] = { nullptr, nullptr };

	gContext->OMSetRenderTargets(1, nullrtv, nullptr);

	gContext->CSSetUnorderedAccessViews(1, 2, UAVs, 0);
	/*gContext->ClearUnorderedAccessViewFloat(UAVs[0], clearValue);
	gContext->ClearUnorderedAccessViewFloat(UAVs[1], clearValue);*/

	gContext->CSSetShaderResources(0, 1, pTexViews_LuminanceMap->GetSRV_AddressOf());

	ShaderList::ComputeShaders::CS_HDR_AvgLuminance::SetShader_FirstPass(gContext.Get());

	gContext->Dispatch(mDownScaleGroups, 1, 1);


	//////////////////////////////////////////////////////////////////////////
	//Second Pass

	static ID3D11ShaderResourceView* SRVs[2];
	SRVs[0] = mBuffer_DownScale1D->GetSRV();
	SRVs[1] = AvgLum_SRV[prevframe];


	gContext->CSSetUnorderedAccessViews(1, 1, &AvgLum_UAV[frame], 0);
	//gContext->ClearUnorderedAccessViewFloat(AvgLum_UAV[frame], clearValue);

	gContext->CSSetShaderResources(1, 2, SRVs);

	ShaderList::ComputeShaders::CS_HDR_AvgLuminance::SetShader_SecondPass(gContext.Get());

	gContext->Dispatch(1, 1, 1);

	gContext->CSSetShaderResources(0, 2, srvnullptr);


	//////////////////////////////////////////////////////////////////////////
	//Bloom
	if (Enable_Bloom)
	{
		Bloom();
	}

	//////////////////////////////////////////////////////////////////////////
	//Final Pass

	SetNullUAV_CS(gContext.Get(), 2, 0);

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());

	gContext->RSSetViewports(1, &gViewPort);

	gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
	gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	ShaderList::VertexShaders::VS_Render2D::SetShader(gContext.Get());

	ShaderList::PixelShaders::PS_PostFX_FinalPass::SetShader(gContext.Get());

	auto rtv = gSwapChain.GetBackBufferRTV();

	gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
	gContext->OMSetRenderTargets(1, &rtv, nullptr);

	mSRVCollector->BindSRVs_PS(gContext, 0);


	gContext->DrawIndexed(6, 0, 0);

	//////////////////////////////////////////////////////////////////////////
	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	//gContext->FinishCommandList(true, mCommandList.ReleaseAndGetAddressOf());

	gD3DUserDefinedAnnotation->EndEvent();
}

void RenderPass_HDR::DrawDeferred()
{

}

void RenderPass_HDR::RecompileShaders()
{
	ShaderList::ComputeShaders::CS_HDR_AvgLuminance::RecompileShader();
}

void RenderPass_HDR::InitGPUResources()
{
	mDownScaleGroups = (UINT)ceil((float)(gClientWidth * gClientHeight / 16) / 1024.0f);

	if (!GPUResourceInitialized)
	{
		InitBuffers();
		InitViews();

		GPUResourceInitialized = true;
	}

	UAVs[0] = mBuffer_DownScale1D->GetUAV();
	UAVs[1] = mTexView_DownScaledHDR->GetUAV();


	AvgLum_SRV[0] = mBuffer_AvgLum_0->GetSRV();
	AvgLum_SRV[1] = mBuffer_AvgLum_1->GetSRV();

	AvgLum_UAV[0] = mBuffer_AvgLum_0->GetUAV();
	AvgLum_UAV[1] = mBuffer_AvgLum_1->GetUAV();

	UAVs_Bloom[0] = mTexView_Bloom->GetUAV();

	SetSRVs();
}

void RenderPass_HDR::DestroyGPUResources()
{
	ReleaseBuffers();
	ReleaseViews();

	GPUResourceInitialized = false;
}

std::string RenderPass_HDR::GPUTime_String()
{
	return "HDR Render Time: " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}


void RenderPass_HDR::Update()
{
	prevframe = frame;
	frame = (frame + 1) % 2;

	SRVs_Bloom[1] = AvgLum_SRV[frame];
}

void RenderPass_HDR::RenderConfigurationUI()
{

}

void RenderPass_HDR::SetSRVs()
{
	mSRVCollector.reset(new SRVCollector());

	mSRVCollector->ClearAndAddSRV(pTexViews_LuminanceMap->GetSRV());
	mSRVCollector->AddSRV(pTexViews_ColorMap->GetSRV());
	mSRVCollector->AddSRV(mBuffer_AvgLum_0->GetSRV());
	mSRVCollector->AddSRV(mTexView_Bloom->GetSRV());

	SRVs_Bloom[0] = mTexView_DownScaledHDR->GetSRV();
}

void RenderPass_HDR::Bloom()
{
	SetNullUAV_CS(gContext.Get(), 3, 0);

	ShaderList::ComputeShaders::CS_Bloom::SetShader(gContext.Get());

	FLOAT clearValue[] = { 0.0f, 0.0f, 0.0f, 0.0f };

	gContext->CSSetUnorderedAccessViews(0, (UINT)1, UAVs_Bloom, nullptr);

	gContext->ClearUnorderedAccessViewFloat(UAVs_Bloom[0], clearValue);

	gContext->CSSetShaderResources(0, (UINT)2, SRVs_Bloom);

	gContext->Dispatch(mDownScaleGroups, 1, 1);

	//////////////////////////////////////////////////////////////////////////
	//Apply Gaussian Blur on Bloom Texture

	ShaderList::ComputeShaders::CS_GaussianBlur::SetClassInstance_Filter_Horz();
	ShaderList::ComputeShaders::CS_GaussianBlur::SetShader(gContext.Get());

	SetNullUAV_CS(gContext.Get(), 3, 0);

	ID3D11UnorderedAccessView* uav = mTexView_Bloom_Temp->GetUAV();

	gContext->CSSetUnorderedAccessViews(0, 1, &uav, nullptr);

	gContext->ClearUnorderedAccessViewFloat(uav, clearValue);

	gContext->CSSetShaderResources(0, 1, mTexView_Bloom->GetSRV_AddressOf());

	gContext->Dispatch((UINT)ceil((gClientWidth / 4.0f) / (128.0f - 12.0f)), (UINT)ceil(gClientHeight / 4.0f), 1);

	//Vertical Pass

	ShaderList::ComputeShaders::CS_GaussianBlur::SetClassInstance_Filter_Vert();
	ShaderList::ComputeShaders::CS_GaussianBlur::SetShader(gContext.Get());

	uav = mTexView_Bloom->GetUAV();

	gContext->CSSetUnorderedAccessViews(0, 1, &uav, nullptr);

	gContext->ClearUnorderedAccessViewFloat(uav, clearValue);

	gContext->CSSetShaderResources(0, 1, mTexView_Bloom_Temp->GetSRV_AddressOf());

	gContext->Dispatch((UINT)ceil(gClientWidth / 4.0f), (UINT)ceil((gClientHeight / 4.0f) / (128.0f - 12.0f)), 1);

}
