#include "stdafx.h"
#include "RenderPass_GenerateSAT.h"
#include "Globals.h"

#include "Graphics/TexViews.h"
#include "Graphics/RenderStates.h"
#include "Graphics/Colors.h"
#include "Graphics/ConstantBuffer.h"
#include "Graphics/ShaderSlots_DeferredShading.h"

#include "Objects/Mesh.h"


#include "Utility/GpuProfiler.h"
#include <Graphics\ShaderList\PS_GenerateVarianceShadowMap.h>
#include <Graphics\ShaderList\VS_BuildShadowMap.h>
#include <Graphics\ShaderList\VS_Render2D.h>
#include <Graphics\ShaderList\PS_GenerateSAT.h>

using namespace Globals;

void RenderPass_GenerateSAT::InitViews()
{
	mViewport.Height = gShadowMapResolution;
	mViewport.Width = gShadowMapResolution;
	mViewport.MaxDepth = 1.0f;
	mViewport.MinDepth = 0.0f;
	mViewport.TopLeftX = 0.0f;
	mViewport.TopLeftY = 0.0f;


	mTexView_DSV->Init(D3D11_BIND_DEPTH_STENCIL
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_D32_FLOAT
		, D3D11_USAGE_DEFAULT);

	auto shadowLightNum = gLights.GetShadowMapCount();

	mTexView_SATVarianceShadowMap->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32G32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, shadowLightNum
		, 1
		, 1
		, 0
		, 0
		, true);

	mTexView_Temp_SAT->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32G32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, shadowLightNum
		, 1
		, 1
		, 0
		, 0
		, true);
}

void RenderPass_GenerateSAT::ReleaseViews()
{
	mTexView_DSV->Release();
	mTexView_Temp_SAT->Release();
	mTexView_SATVarianceShadowMap->Release();

}

void RenderPass_GenerateSAT::InitBuffers()
{

	mCB_VS_PerObject.reset(new ConstantBuffer<CB_VS_PER_OBJECT>());
	mCB_VS_perLight.reset(new ConstantBuffer<CB_VS_PER_LIGHT>());
	mCB_PS_GenerateSAT.reset(new ConstantBuffer<CB_PS_GENERATESAT>());


	mCB_VS_PerObject->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PEROBJECT
		, CONSTANT_BUFFER_VS
		, &mData_CB_PerObject
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_VS_PerObject->SetConstantBuffer(gContext.Get());

	mCB_VS_perLight->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PERLIGHT
		, CONSTANT_BUFFER_VS
		, &mData_CB_PerLight
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_VS_perLight->SetConstantBuffer(gContext.Get());

	mCB_PS_GenerateSAT->InitConstantBuffer(SHADER_SLOTS_PS_CB_GENERATESAT
		, CONSTANT_BUFFER_PS
		, &mData_CB_GenerateSAT
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_PS_GenerateSAT->SetConstantBuffer(gContext.Get());





	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
}

void RenderPass_GenerateSAT::ReleaseBuffers()
{
	mCB_PS_GenerateSAT->Release();
	mCB_VS_perLight->Release();
	mCB_VS_PerObject->Release();

	mCB_PS_GenerateSAT.reset();
	mCB_VS_perLight.reset();
	mCB_VS_PerObject.reset();

	mGpuTimeStampDB->Destroy();
}

RenderPass_GenerateSAT::RenderPass_GenerateSAT() {
	mTexView_DSV.reset(new TexViews());
	mTexView_Temp_SAT.reset(new TexViews());
	mTexView_SATVarianceShadowMap.reset(new TexViews());
}

RenderPass_GenerateSAT::~RenderPass_GenerateSAT() = default;

RenderPass_GenerateSAT::RenderPass_GenerateSAT(RenderPass_GenerateSAT&&) = default;

RenderPass_GenerateSAT& RenderPass_GenerateSAT::operator=(RenderPass_GenerateSAT&&) = default;

void RenderPass_GenerateSAT::Init()
{

	float a = std::log2f(gShadowMapResolution);
	float b = std::log2f(SampleCountPerSATPass);

	SATPassCount = ceil(a / b);
}

void RenderPass_GenerateSAT::Draw3D()
{

	gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_GenerateSAT");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_DEFAULT, gContext.Get());

	gContext->RSSetViewports(1, &mViewport);

	auto dsv = mTexView_DSV->GetDSV();

	ID3D11RenderTargetView* rtv = nullptr;

	ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_VSMGenerator_NormalizedLinearDepth();

	for (auto& directlight : gLights.mDirectionalLights)
	{
		int shadowMapID = directlight.GetILightData().ShadowMapID;

		rtv = mTexView_SATVarianceShadowMap->GetRTV(shadowMapID);

		gContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
		gContext->ClearRenderTargetView(rtv, RGBA{ 0.0f, 0.0f , 0.0f , 0.0f });

		gContext->OMSetRenderTargets(1, &rtv, dsv);

		mData_CB_PerLight.gLightVP = directlight.GetILightData().ViewProj;
		mData_CB_PerLight.gZNearFar = XMFLOAT2(directlight.GetILightData().zNear, directlight.GetILightData().zFar);
		mData_CB_PerLight.gLightPosition = directlight.GetILightData().Position;
		mCB_VS_perLight->Update(gContext.Get());

		ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_Light_Direct(directlight.GetIndex());
		ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetShader(gContext.Get());


		auto SKModels = gModelManager.GetSkinnedModels();

		if (SKModels->size() > 0)
		{

			gContext->IASetInputLayout(Vertex::IL_Skinned.Get());
			gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(gContext.Get());

			for (auto skmodel : *SKModels)
			{
				/*if (directlight.BoundingBoxContainment(obj.pMesh->AABB) == ContainmentType::DISJOINT)
				{
					continue;
				}*/

				mData_CB_PerObject.gWorld = skmodel->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : skmodel->mMeshes)
				{
					mesh->Draw(gContext.Get());
				}

			}
		}


		auto Models = gModelManager.GetBasicModels();

		gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
		gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

		if (Models->size() > 0)
		{

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(gContext.Get());


			for (auto model : *Models)
			{
				/*if (directlight.BoundingBoxContainment(obj.pMesh->AABB) == ContainmentType::DISJOINT)
				{
					continue;
				}*/

				mData_CB_PerObject.gWorld = model->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : model->mMeshes)
				{
					mesh->Draw(gContext.Get());
				}

			}
		}

	}

	for (auto& spotlight : gLights.mSpotLights)
	{
		int shadowMapID = spotlight.GetILightData().ShadowMapID;

		rtv = mTexView_SATVarianceShadowMap->GetRTV(shadowMapID);

		gContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
		gContext->ClearRenderTargetView(rtv, RGBA{ 0.0f, 0.0f , 0.0f , 0.0f });

		gContext->OMSetRenderTargets(1, &rtv, dsv);

		mData_CB_PerLight.gLightVP = spotlight.GetILightData().ViewProj;
		mData_CB_PerLight.gZNearFar = XMFLOAT2(spotlight.GetILightData().zNear, spotlight.GetILightData().zFar);
		mData_CB_PerLight.gLightPosition = spotlight.GetILightData().Position;
		mCB_VS_perLight->Update(gContext.Get());

		ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_Light_Spot(spotlight.GetIndex());
		ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetShader(gContext.Get());


		auto SKModels = gModelManager.GetSkinnedModels();

		if (SKModels->size() > 0)
		{

			gContext->IASetInputLayout(Vertex::IL_Skinned.Get());
			gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(gContext.Get());

			for (auto skmodel : *SKModels)
			{
				/*if (spotlight.BoundingBoxContainment(obj.pMesh->AABB) == ContainmentType::DISJOINT)
				{
					continue;
				}*/

				mData_CB_PerObject.gWorld = skmodel->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : skmodel->mMeshes)
				{
					mesh->Draw(gContext.Get());
				}

			}
		}


		auto Models = gModelManager.GetBasicModels();

		gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
		gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

		if (Models->size() > 0)
		{

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(gContext.Get());


			for (auto model : *Models)
			{
				/*if (spotlight.BoundingBoxContainment(obj.pMesh->AABB) == ContainmentType::DISJOINT)
				{
					continue;
				}*/

				mData_CB_PerObject.gWorld = model->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : model->mMeshes)
				{
					mesh->Draw(gContext.Get());
				}

			}
		}

	}


	//mPipeline_GenerateSAT.Apply_Preliminiary(gContext.Get());

	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH, gContext.Get());

	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	ShaderList::VertexShaders::VS_Render2D::SetShader(gContext.Get());

	for (int i = 0; i < gLights.GetShadowMapCount(); ++i)
	{

		/***Generate SAT Based on Variance Shadow Map, Horizontal Pass*/

		ShaderList::PixelShaders::PS_GenerateSAT::SetClassInstace_SAT_Horizontal();
		ShaderList::PixelShaders::PS_GenerateSAT::SetShader(gContext.Get());

		mData_CB_GenerateSAT.gShadowMapID = i;

		for (int j = 0; j < SATPassCount; ++j)
		{

			mData_CB_GenerateSAT.gIteration = j;
			mCB_PS_GenerateSAT->Update(gContext.Get());

			rtv = mTexView_Temp_SAT->GetRTV(i);

			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			mTexView_SATVarianceShadowMap->BindSRV_PS(0);

			gContext->DrawIndexed(6, 0, 0);

			mData_CB_GenerateSAT.gIteration = ++j;
			mCB_PS_GenerateSAT->Update(gContext.Get());

			rtv = mTexView_SATVarianceShadowMap->GetRTV(i);

			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			mTexView_Temp_SAT->BindSRV_PS(0);

			gContext->DrawIndexed(6, 0, 0);

		}

		ShaderList::PixelShaders::PS_GenerateSAT::SetClassInstace_SAT_Vertical();
		ShaderList::PixelShaders::PS_GenerateSAT::SetShader(gContext.Get());

		for (int j = 0; j < SATPassCount; ++j)
		{

			mData_CB_GenerateSAT.gIteration = j;
			mCB_PS_GenerateSAT->Update(gContext.Get());

			rtv = mTexView_Temp_SAT->GetRTV(i);

			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			mTexView_SATVarianceShadowMap->BindSRV_PS(0);

			gContext->DrawIndexed(6, 0, 0);

			mData_CB_GenerateSAT.gIteration = ++j;
			mCB_PS_GenerateSAT->Update(gContext.Get());

			rtv = mTexView_SATVarianceShadowMap->GetRTV(i);

			gContext->OMSetRenderTargets(1, &rtv, nullptr);

			mTexView_Temp_SAT->BindSRV_PS(0);

			gContext->DrawIndexed(6, 0, 0);

		}


	}

	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	gD3DUserDefinedAnnotation->EndEvent();

}

void RenderPass_GenerateSAT::Draw2D()
{

}

void RenderPass_GenerateSAT::DrawTxt()
{

}

void RenderPass_GenerateSAT::Update()
{

}

void RenderPass_GenerateSAT::Destroy()
{

}

void RenderPass_GenerateSAT::DestroyGPUResources()
{
	ReleaseViews();
	ReleaseBuffers();

	GPUResourceInitialized = false;
}

void RenderPass_GenerateSAT::InitGPUResources()
{
	if (!GPUResourceInitialized)
	{
		InitViews();
		InitBuffers();

		GPUResourceInitialized = true;
	}
}

void RenderPass_GenerateSAT::RecompileShaders()
{

}

//void RenderPass_GenerateSAT::SetObjects(std::vector<Model*>* Models)
//{
//	mModels = Models;
//}

ID3D11ShaderResourceView* RenderPass_GenerateSAT::GetSAVarianceShadowMap_SRV() const
{
	return mTexView_SATVarianceShadowMap->GetSRV();
}


const TexViews* RenderPass_GenerateSAT::GetTexViewVarianceShadowMap() const noexcept
{
	return mTexView_SATVarianceShadowMap.get();
}

std::string RenderPass_GenerateSAT::GPUTime_String()
{
	return "Generate SAT's Render Time : " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}

