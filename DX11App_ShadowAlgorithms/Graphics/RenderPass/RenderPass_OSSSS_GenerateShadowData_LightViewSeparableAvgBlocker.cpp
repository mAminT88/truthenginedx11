#include "stdafx.h"
#include "RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker.h"
#include "Globals.h"

#include "Graphics/TexViews.h"
#include "Graphics/ResourceViewCollectors.h"
#include "Graphics/Colors.h"
#include "Graphics/ConstantBuffer.h"

#include "Utility/GpuProfiler.h"
#include <Graphics\ShaderList\CS_FindBlocker.h>
#include <Graphics\ShaderList\PS_OSSSS_GenerateShadowData_Pass0.h>
#include <Graphics\ShaderList\PS_OSSSS_GenerateShadowData_Pass1.h>

using namespace Globals;


RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker() {
	mTexView_UAV_FindBlocker_0.reset(new TexViews());
	mTexView_UAV_FindBlocker_1.reset(new TexViews());
}

RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::~RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker() = default;

RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker(RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker&&) = default;

RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker& RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::operator=(RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker&&) = default;

void RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::Init()
{
	RenderPass_OSSSS_GenerateShadowData::Init();

	auto x = ShaderList::ComputeShaders::CS_FindBlocker::GetThreadGroupSizeX();
	auto y = ShaderList::ComputeShaders::CS_FindBlocker::GetThreadGroupSizeY();

	mThreadGroupCountX = gClientWidth / x;
	mThreadGroupCountY = gClientHeight / y;
}

void RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::Draw3D()
{

	gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker");

	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());

	ID3D11UnorderedAccessView* uav;
	ID3D11UnorderedAccessView* uav_array[1] = { nullptr };


	gContext->RSSetViewports(1, &gViewPort);


	XMStoreFloat4x4(&mData_CB_CS_SeparableFindBlocker_PerFrame.gViewInv, gCamera_main.ViewInv());
	XMStoreFloat4(&mData_CB_CS_SeparableFindBlocker_PerFrame.gEyePerspectiveValues, gCamera_main.GetPerspectiveValues());

	mCB_CS_SeparableFindBlocker_PerFrame->Update(gContext.Get());

	ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass0::Set_ClassInstance_GenerateShadowData_0_UsePreFilteredAvgBlockers();


	for (int i = 0; i < gLights.GetShadowMapCount(); ++i)
	{

		gContext->OMSetRenderTargets(0, nullptr, nullptr);

		ShaderList::ComputeShaders::CS_FindBlocker::SetClassInstace_FindBlocker_Vert();
		ShaderList::ComputeShaders::CS_FindBlocker::SetShader(gContext.Get());

		mData_CB_CS_SeparableFindBlocker_PerLight.gLightSize = gLights.mCollectionILights[i]->GetILightData().LightSize;
		mData_CB_CS_SeparableFindBlocker_PerLight.gLightZFar = gLights.mCollectionILights[i]->GetILightData().zFar;
		mData_CB_CS_SeparableFindBlocker_PerLight.gLightZNear = gLights.mCollectionILights[i]->GetILightData().zNear;
		mData_CB_CS_SeparableFindBlocker_PerLight.gShadowMapID = gLights.mCollectionILights[i]->GetILightData().ShadowMapID;
		mData_CB_CS_SeparableFindBlocker_PerLight.gShadowTransform = gLights.mCollectionILights[i]->GetILightData().ShadowTransform;

		mCB_CS_SeparableFindBlocker_PerLight->Update(gContext.Get());

		mSRVCollector_CS_FindBlocker_Horz->BindSRVs_CS(gContext, 0);

		gContext->ClearUnorderedAccessViewFloat(mTexView_UAV_FindBlocker_0->GetUAV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		gContext->ClearUnorderedAccessViewFloat(mTexView_UAV_FindBlocker_1->GetUAV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });

		uav = mTexView_UAV_FindBlocker_0->GetUAV();

		UINT c = 0;

		gContext->CSSetUnorderedAccessViews(0, 1, &uav, &c);

		gContext->Dispatch(mThreadGroupCountX, mThreadGroupCountY, mThreadGroupCountZ);

		ShaderList::ComputeShaders::CS_FindBlocker::SetClassInstace_FindBlocker_Horz();
		ShaderList::ComputeShaders::CS_FindBlocker::SetShader(gContext.Get());

		uav = mTexView_UAV_FindBlocker_1->GetUAV();

		gContext->CSSetUnorderedAccessViews(0, 1, &uav, &c);


		mSRVCollector_CS_FindBlocker_Vert->BindSRVs_CS(gContext, 0);


		gContext->Dispatch(mThreadGroupCountX, mThreadGroupCountY, mThreadGroupCountZ);


		gContext->CSSetUnorderedAccessViews(0, 1, uav_array, &c);


		//mPipeline.Apply_Preliminiary_Pass0();


		gContext->ClearRenderTargetView(mTexView_PenumbraSize->GetRTV(), RGBA{ 0.0f, 0.0f, 0.0f, 0.0f });
		gContext->ClearRenderTargetView(mTexView_Shadows_Temp->GetRTV(), RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });


		gContext->OMSetRenderTargets(mRTVCollector_0->GetSize(), mRTVCollector_0->GetRTVs(), nullptr);


		mSRVCollector_0->BindSRVs_PS(gContext, 0);


		/*CalcLightDirInViewSpace_XZ(mData_CB_PerLight.gLight2DDirInEye_Horz, mData_CB_PerLight.gLight2DDirInEye_Vert, gLights.mCollectionILights[i]->GetILightData());
		mData_CB_PerLight.gShadowMapID = i;


		mCB_PerLight.Update(gContext.Get());*/


		gContext->DrawIndexed(6, 0, 0);


		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::Set_ClassInstance_HorizontalStep();
		ShaderList::PixelShaders::PS_OSSSS_GenerateShadowData_Pass1::SetShader(gContext.Get());



		auto rtv = mTexView_Shadows->GetRTV(i);
		gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		gContext->OMSetRenderTargets(1, &rtv, nullptr);


		mSRVCollector_1->BindSRVs_PS(gContext, 4);


		gContext->DrawIndexed(6, 0, 0);
	}


	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	gD3DUserDefinedAnnotation->EndEvent();
}

void RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::SetSRV(ID3D11ShaderResourceView* const ppDepthMap, ID3D11ShaderResourceView* const ppShadowMapArray, ID3D11ShaderResourceView* const ppGBuffer_Normal)
{
	mSRVCollector_0.reset(new SRVCollector());
	mSRVCollector_1.reset(new SRVCollector());
	mSRVCollector_CS_FindBlocker_Horz.reset(new SRVCollector());
	mSRVCollector_CS_FindBlocker_Vert.reset(new SRVCollector());

	mSRVCollector_0->ClearAndAddSRV(ppDepthMap);
	mSRVCollector_0->AddSRV(ppShadowMapArray);
	mSRVCollector_0->AddSRV(ppGBuffer_Normal);
	mSRVCollector_0->AddSRV(mTexView_UAV_FindBlocker_1->GetSRV());


	mSRVCollector_CS_FindBlocker_Horz->ClearAndAddSRV(ppDepthMap);
	mSRVCollector_CS_FindBlocker_Horz->AddSRV(ppShadowMapArray);

	mSRVCollector_CS_FindBlocker_Vert->ClearAndAddSRV(ppDepthMap);
	mSRVCollector_CS_FindBlocker_Vert->AddSRV(mTexView_UAV_FindBlocker_0->GetSRV());
}


std::string RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::GPUTime_String()
{
	return "OSSSS Light View Blocker Search Generate Shadow Data's Time : " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}

void RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::InitViews()
{
	RenderPass_OSSSS_GenerateShadowData::InitViews();


	mTexView_UAV_FindBlocker_0->Init(
		D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, 1
		, 1
		, 1
		, 0
		, 0
		, true
	);

	mTexView_UAV_FindBlocker_1->Init(
		D3D11_BIND_UNORDERED_ACCESS | D3D11_BIND_SHADER_RESOURCE
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, 1
		, 1
		, 1
		, 0
		, 0
		, true
	);
}

void RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::ReleaseViews()
{

	RenderPass_OSSSS_GenerateShadowData::ReleaseViews();

	mTexView_UAV_FindBlocker_0->Release();
	mTexView_UAV_FindBlocker_1->Release();
}

void RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::InitBuffers()
{
	RenderPass_OSSSS_GenerateShadowData::InitBuffers();

	mCB_CS_SeparableFindBlocker_PerFrame.reset(new ConstantBuffer<CB_CS_SEPARABLEFINDBLOCKER_PERFRAME>());
	mCB_CS_SeparableFindBlocker_PerLight.reset(new ConstantBuffer<CB_CS_SEPARABLEFINDBLOCKER_PERLIGHT>());
	mCB_CS_SeparableFindBlocker_Unfreq.reset(new ConstantBuffer<CB_CS_SEPARABLEFINDBLOCKER_UNFREQ>());


	mCB_CS_SeparableFindBlocker_PerFrame->InitConstantBuffer(0
		, CONSTANT_BUFFER_CS
		, &mData_CB_CS_SeparableFindBlocker_PerFrame
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_CS_SeparableFindBlocker_PerFrame->SetConstantBuffer(gContext.Get());


	mCB_CS_SeparableFindBlocker_PerLight->InitConstantBuffer(2
		, CONSTANT_BUFFER_CS
		, &mData_CB_CS_SeparableFindBlocker_PerLight
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_CS_SeparableFindBlocker_PerLight->SetConstantBuffer(gContext.Get());


	mCB_CS_SeparableFindBlocker_Unfreq->InitConstantBuffer(1
		, CONSTANT_BUFFER_CS
		, &mData_CB_CS_SeparableFindBlocker_Unfreq
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_CS_SeparableFindBlocker_Unfreq->SetConstantBuffer(gContext.Get());

	mData_CB_CS_SeparableFindBlocker_Unfreq.gScreenSize = XMFLOAT2(gClientWidth, gClientHeight);
	mData_CB_CS_SeparableFindBlocker_Unfreq.gShadowMapSize = XMFLOAT2(gShadowMapResolution, gShadowMapResolution);

	mCB_CS_SeparableFindBlocker_Unfreq->Update(gContext.Get());


	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));

}

void RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker::ReleaseBuffers()
{
	RenderPass_OSSSS_GenerateShadowData::ReleaseBuffers();

	mCB_CS_SeparableFindBlocker_PerFrame->Release();
	mCB_CS_SeparableFindBlocker_PerLight->Release();
	mCB_CS_SeparableFindBlocker_Unfreq->Release();

	mCB_CS_SeparableFindBlocker_PerFrame.reset();
	mCB_CS_SeparableFindBlocker_PerLight.reset();
	mCB_CS_SeparableFindBlocker_Unfreq.reset();

	mGpuTimeStampDB->Destroy();
	mGpuTimeStampDB.reset();

}
