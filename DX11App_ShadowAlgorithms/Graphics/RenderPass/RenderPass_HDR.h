#pragma once

#include "IRenderPass.h"

class TexViews;
class BufferViews;
class SRVCollector;

class RenderPass_HDR : public IRenderPass
{

   int frame, prevframe;


	std::unique_ptr<SRVCollector> mSRVCollector;

	ID3D11ShaderResourceView* AvgLum_SRV[2];
	ID3D11UnorderedAccessView* AvgLum_UAV[2];

	ID3D11UnorderedAccessView* UAVs[2];

	ID3D11ShaderResourceView* SRVs_Bloom[2];
	ID3D11UnorderedAccessView* UAVs_Bloom[1];

	//PipelineList::Pipeline_DeferredShading mPipeline;
	UINT mVertexOffset = 0;
	UINT mDownScaleGroups;

	std::unique_ptr<BufferViews> mBuffer_DownScale1D;
	std::unique_ptr<BufferViews> mBuffer_AvgLum_0;
	std::unique_ptr<BufferViews> mBuffer_AvgLum_1;
	
	std::unique_ptr<TexViews> mTexView_DownScaledHDR;
	std::unique_ptr<TexViews> mTexView_Bloom;
	std::unique_ptr<TexViews> mTexView_Bloom_Temp;

	ComPtr<ID3D11CommandList> mCommandList;

	//virtual void InitPipelines() override;
	virtual void InitViews() override;
	virtual void ReleaseViews() override;
	virtual void InitBuffers() override;
	virtual void ReleaseBuffers() override;

	void SetSRVs();

	void Bloom();
public:

	RenderPass_HDR();
	~RenderPass_HDR();

	RenderPass_HDR(RenderPass_HDR&&);
	RenderPass_HDR& operator=(RenderPass_HDR&&);

	bool Enable_Bloom;



	const TexViews* pTexViews_LuminanceMap;
	const TexViews* pTexViews_ColorMap;

	virtual void Init() override;
	virtual void Draw3D() override;
	virtual void DrawDeferred() override;
	virtual void RecompileShaders() override;
	virtual void InitGPUResources() override;
	virtual void DestroyGPUResources() override;
	virtual std::string GPUTime_String() override;
	void Update() override;
	

	void RenderConfigurationUI() override;
};
