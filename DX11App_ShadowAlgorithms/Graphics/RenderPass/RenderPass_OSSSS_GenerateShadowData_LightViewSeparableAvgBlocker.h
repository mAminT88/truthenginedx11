#pragma once

#include "RenderPass_OSSSS_GenerateShadowData.h"


class TexViews;
class SRVCollector;
template<typename T> class ConstantBuffer;

class RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker : public RenderPass_OSSSS_GenerateShadowData
{

public:
	RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker();
	~RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker();

	RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker(RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker&&);
	RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker& operator=(RenderPass_OSSSS_GenerateShadowData_LightViewSeparableAvgBlocker&&);

	virtual void Init() override;
	virtual void Draw3D() override;

	virtual void SetSRV(
		ID3D11ShaderResourceView* const ppDepthMap
		, ID3D11ShaderResourceView* const ppShadowMapArray
		, ID3D11ShaderResourceView* const ppGBuffer_Normal);

	virtual std::string GPUTime_String() override;

protected:

	struct CB_CS_SEPARABLEFINDBLOCKER_PERFRAME
	{
		XMFLOAT4X4 gViewInv;

		XMFLOAT4 gEyePerspectiveValues;
	};

	struct CB_CS_SEPARABLEFINDBLOCKER_PERLIGHT
	{
		XMFLOAT4X4 gShadowTransform;

		float gLightSize;
		float gLightZNear;
		float gLightZFar;
		float gShadowMapID;
	};

	struct CB_CS_SEPARABLEFINDBLOCKER_UNFREQ
	{
		XMFLOAT2 gScreenSize;
		XMFLOAT2 gShadowMapSize;
	};

	std::unique_ptr<ConstantBuffer<CB_CS_SEPARABLEFINDBLOCKER_PERFRAME>> mCB_CS_SeparableFindBlocker_PerFrame;
	CB_CS_SEPARABLEFINDBLOCKER_PERFRAME mData_CB_CS_SeparableFindBlocker_PerFrame;

	std::unique_ptr<ConstantBuffer<CB_CS_SEPARABLEFINDBLOCKER_PERLIGHT>> mCB_CS_SeparableFindBlocker_PerLight;
	CB_CS_SEPARABLEFINDBLOCKER_PERLIGHT mData_CB_CS_SeparableFindBlocker_PerLight;

	std::unique_ptr<ConstantBuffer<CB_CS_SEPARABLEFINDBLOCKER_UNFREQ>> mCB_CS_SeparableFindBlocker_Unfreq;
	CB_CS_SEPARABLEFINDBLOCKER_UNFREQ mData_CB_CS_SeparableFindBlocker_Unfreq;

	std::unique_ptr<TexViews> mTexView_UAV_FindBlocker_0;
	std::unique_ptr<TexViews> mTexView_UAV_FindBlocker_1;


	std::unique_ptr<SRVCollector> mSRVCollector_CS_FindBlocker_Horz;
	std::unique_ptr<SRVCollector> mSRVCollector_CS_FindBlocker_Vert;
	

	UINT mThreadGroupCountX, mThreadGroupCountY, mThreadGroupCountZ = 1;


	virtual void InitViews() override;


	virtual void ReleaseViews() override;


	virtual void InitBuffers() override;


	virtual void ReleaseBuffers() override;
};
