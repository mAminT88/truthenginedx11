#include "stdafx.h"
#include "RenderPass_GenerateVarianceShadowMap.h"
#include "Globals.h"

#include "Graphics/RenderStates.h"
#include "Graphics/TexViews.h"
#include "graphics/Colors.h"
#include "Graphics/ConstantBuffer.h"
#include "Graphics/ShaderSlots_DeferredShading.h"

#include "Objects/Mesh.h"

#include "Utility/GpuProfiler.h"

////Shaders
#include "Graphics/ShaderList/PS_SeperableBlurFilter.h"
#include "Graphics/ShaderList/PS_GenerateVarianceShadowMap.h"
#include "Graphics/ShaderList/VS_BuildShadowMap.h"
#include "Graphics/ShaderList/VS_Render2D.h"

using namespace Globals;

//void RenderPass_GenerateVarianceShadowMap::InitPipelines()
//{
//
//	mPipeline_SeperableGaussianFilter.Init();
//	mPipeline_GenerateVarianceShadowMap.Init();
//
//}


void RenderPass_GenerateVarianceShadowMap::InitViews()
{
	mViewport.Height = gShadowMapResolution;
	mViewport.Width = gShadowMapResolution;
	mViewport.MaxDepth = 1.0f;
	mViewport.MinDepth = 0.0f;
	mViewport.TopLeftX = 0.0f;
	mViewport.TopLeftY = 0.0f;


	mTexView_DSV->Init(D3D11_BIND_DEPTH_STENCIL
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_D32_FLOAT
		, D3D11_USAGE_DEFAULT);

	auto shadowLightNum = gLights.GetShadowMapCount();

	mTexView_TempFilteredVarianceShadowMap->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32G32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, 1
		, 1
		, 1
		, 0
		, 0
		, true);

	mTexView_VarianceShadowMap->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_RENDER_TARGET
		, gShadowMapResolution
		, gShadowMapResolution
		, DXGI_FORMAT_R32G32_FLOAT
		, D3D11_USAGE_DEFAULT
		, (D3D11_CPU_ACCESS_FLAG)0
		, shadowLightNum
		, 1
		, 1
		, 0
		, 0
		, true);
}

void RenderPass_GenerateVarianceShadowMap::ReleaseViews()
{
	mTexView_VarianceShadowMap->Release();
	mTexView_TempFilteredVarianceShadowMap->Release();
	mTexView_DSV->Release();
}

void RenderPass_GenerateVarianceShadowMap::InitBuffers()
{
	mCB_PS_VSM_PerLight.reset(new ConstantBuffer<CB_PS_VSM_PERLIGHT>());
	mCB_VS_perLight.reset(new ConstantBuffer<CB_VS_PER_LIGHT>());
	mCB_VS_PerObject.reset(new ConstantBuffer<CB_VS_PER_OBJECT>());

	mCB_PS_VSM_PerLight->InitConstantBuffer(SHADER_SLOTS_PS_CB_VSM_FILTERSHADOWMAP
		, CONSTANT_BUFFER_PS
		, &mData_CB_VSM_PerLight
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_PS_VSM_PerLight->SetConstantBuffer(gContext.Get());


	mCB_VS_perLight->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PERLIGHT
		, CONSTANT_BUFFER_VS
		, &mDataCBPerLight
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_VS_perLight->SetConstantBuffer(gContext.Get());


	mCB_VS_PerObject->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PEROBJECT
		, CONSTANT_BUFFER_VS
		, &mDataCBPerObject
		, D3D11_CPU_ACCESS_WRITE
		, D3D11_USAGE_DYNAMIC);

	mCB_VS_PerObject->SetConstantBuffer(gContext.Get());

	



	mGpuTimeStampDB.reset(new GPUTimeStamp_DualBuffer(2));
}

void RenderPass_GenerateVarianceShadowMap::ReleaseBuffers()
{
	mCB_PS_VSM_PerLight->Release();
	mCB_VS_perLight->Release();
	mCB_VS_PerObject->Release();

	mCB_PS_VSM_PerLight.reset();
	mCB_VS_perLight.reset();
	mCB_VS_PerObject.reset();

	mGpuTimeStampDB->Destroy();
}

RenderPass_GenerateVarianceShadowMap::RenderPass_GenerateVarianceShadowMap() {
	mTexView_DSV.reset(new TexViews());
	mTexView_TempFilteredVarianceShadowMap.reset(new TexViews());
	mTexView_VarianceShadowMap.reset(new TexViews());
}

RenderPass_GenerateVarianceShadowMap::~RenderPass_GenerateVarianceShadowMap() = default;

RenderPass_GenerateVarianceShadowMap::RenderPass_GenerateVarianceShadowMap(RenderPass_GenerateVarianceShadowMap&&) = default;

RenderPass_GenerateVarianceShadowMap& RenderPass_GenerateVarianceShadowMap::operator=(RenderPass_GenerateVarianceShadowMap&&) = default;


void RenderPass_GenerateVarianceShadowMap::Init()
{
	//InitPipelines();
	SetShader = ShaderList::PixelShaders::PS_SeperableBlurFilter::SetShader_GaussFilter;
	SetFilter_ClassInstance_Horizontally = ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_GaussFilterManager_Horizontally;
	SetFilter_ClassInstance_Vertically = ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_GaussFilterManager_Vertically;
}

void RenderPass_GenerateVarianceShadowMap::Draw3D()
{


	gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_GenerateVarianceShadowMap");


	mGpuTimeStampDB->SetStart(gCurrentFrame, gContext.Get());


	gContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);


	RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, gContext.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_DEFAULT, gContext.Get());
	RenderStates::SetRasterizerState(RENDER_STATE_RS_DEFAULT, gContext.Get());


	gContext->RSSetViewports(1, &mViewport);


	auto dsv = mTexView_DSV->GetDSV();


	ID3D11RenderTargetView* rtv = nullptr;

	ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_VSMGenerator_NormalizedLinearDepth();

	for (auto& spotlight : gLights.mSpotLights)
	{

		ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_Light_Spot(spotlight.GetIndex());
		ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetShader(gContext.Get());

		int shadowMapID = spotlight.GetILightData().ShadowMapID;

		rtv = mTexView_VarianceShadowMap->GetRTV(shadowMapID);

		gContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
		gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f , 1.0f , 1.0f });

		gContext->OMSetRenderTargets(1, &rtv, dsv);

		mDataCBPerLight.gLightVP = spotlight.GetILightData().ViewProj;
		mDataCBPerLight.gZNearFar = XMFLOAT2(spotlight.GetILightData().zNear, spotlight.GetILightData().zFar);
		mDataCBPerLight.gLightPosition = spotlight.GetILightData().Position;
		mCB_VS_perLight->Update(gContext.Get());

		auto Models_Skinned = gModelManager.GetSkinnedModels();

		if (Models_Skinned->size() > 0)
		{
			gContext->IASetInputLayout(Vertex::IL_Skinned.Get());
			gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(gContext.Get());

			for (auto skModel : *Models_Skinned)
			{

				mDataCBPerObject.gWorld = skModel->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : skModel->mMeshes)
				{

					//mPipeline_GenerateVarianceShadowMap.Apply_Preliminiary(mesh->mVertexType);

					mesh->Draw(gContext.Get());
				}

			}
		}


		auto Models_Basic32 = gModelManager.GetBasicModels();

		gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
		gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

		if (Models_Basic32->size() > 0)
		{

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(gContext.Get());

			for (auto model : *Models_Basic32)
			{
				/*if (spotlight.BoundingBoxContainment(obj.pMesh->AABB) == ContainmentType::DISJOINT)
				{
					continue;
				}*/

				mDataCBPerObject.gWorld = model->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : model->mMeshes)
				{
					//mPipeline_GenerateVarianceShadowMap.Apply_Preliminiary(mesh->mVertexType);

					mesh->Draw(gContext.Get());
				}

			}
		}

	}


	for (auto& directlight : gLights.mDirectionalLights)
	{

		ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetClassInstance_Light_Direct(directlight.GetIndex());
		ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::SetShader(gContext.Get());

		int shadowMapID = directlight.GetILightData().ShadowMapID;

		rtv = mTexView_VarianceShadowMap->GetRTV(shadowMapID);

		gContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
		gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f , 1.0f , 1.0f });

		gContext->OMSetRenderTargets(1, &rtv, dsv);

		mDataCBPerLight.gLightVP = directlight.GetILightData().ViewProj;
		mDataCBPerLight.gZNearFar = XMFLOAT2(directlight.GetILightData().zNear, directlight.GetILightData().zFar);
		mDataCBPerLight.gLightPosition = directlight.GetILightData().Position;
		mCB_VS_perLight->Update(gContext.Get());

		auto Models_Skinned = gModelManager.GetSkinnedModels();

		if (Models_Skinned->size() > 0)
		{
			gContext->IASetInputLayout(Vertex::IL_Skinned.Get());
			gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &mVertexOffset);

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(gContext.Get());

			for (auto skModel : *Models_Skinned)
			{

				mDataCBPerObject.gWorld = skModel->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : skModel->mMeshes)
				{

					//mPipeline_GenerateVarianceShadowMap.Apply_Preliminiary(mesh->mVertexType);

					mesh->Draw(gContext.Get());
				}

			}
		}


		auto Models_Basic32 = gModelManager.GetBasicModels();

		gContext->IASetInputLayout(Vertex::IL_Basic32.Get());
		gContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &mVertexOffset);

		if (Models_Basic32->size() > 0)
		{

			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(gContext.Get());

			for (auto model : *Models_Basic32)
			{
				/*if (directlight.BoundingBoxContainment(obj.pMesh->AABB) == ContainmentType::DISJOINT)
				{
					continue;
				}*/

				mDataCBPerObject.gWorld = model->mWorldMatrix;
				mCB_VS_PerObject->Update(gContext.Get());

				for (auto mesh : model->mMeshes)
				{
					//mPipeline_GenerateVarianceShadowMap.Apply_Preliminiary(mesh->mVertexType);

					mesh->Draw(gContext.Get());
				}

			}
		}

	}


	//mPipeline_SeperableGaussianFilter.Apply_Preliminiary(gContext.Get(), gVertexBuffer_2D.Get(), gIndexBuffer_2D.Get());
	RenderStates::SetDepthStencilState(RENDER_STATE_DS_NODEPTH, gContext.Get());


	ShaderList::VertexShaders::VS_Render2D::SetClassInstace_Position_noWVP();
	ShaderList::VertexShaders::VS_Render2D::SetShader(gContext.Get());


	rtv = mTexView_TempFilteredVarianceShadowMap->GetRTV();


	for (int i = 0; i < gLights.GetShadowMapCount(); ++i)
	{

		/***Filter Variance Shadow Map Horizontally*/

		SetFilter_ClassInstance_Horizontally();
		SetShader(gContext.Get());

		//gContext->ClearRenderTargetView(rtv, RGBA{ 1.0f, 1.0f, 1.0f, 1.0f });
		gContext->OMSetRenderTargets(1, &rtv, nullptr);

		mTexView_VarianceShadowMap->BindSRV_PS(0);

		mData_CB_VSM_PerLight.gTexArrayIndex = i;
		mData_CB_VSM_PerLight.gTexelSize.x = gShadowMapDXY;
		mData_CB_VSM_PerLight.gTexelSize.y = gShadowMapDXY;
		mCB_PS_VSM_PerLight->Update(gContext.Get());

		gContext->DrawIndexed(6, 0, 0);

		/***Filter Variance Shadow Map Vertically and overwrite result on the first variance shadow map*/

		SetFilter_ClassInstance_Vertically();
		SetShader(gContext.Get());

		auto vsm_rtv = mTexView_VarianceShadowMap->GetRTV(i);

		gContext->OMSetRenderTargets(1, &vsm_rtv, nullptr);

		mTexView_TempFilteredVarianceShadowMap->BindSRV_PS(0);

		gContext->DrawIndexed(6, 0, 0);

	}

	mGpuTimeStampDB->SetEnd(gCurrentFrame, gContext.Get());

	gD3DUserDefinedAnnotation->EndEvent();

}

void RenderPass_GenerateVarianceShadowMap::Draw2D()
{

}

void RenderPass_GenerateVarianceShadowMap::DrawTxt()
{

}

void RenderPass_GenerateVarianceShadowMap::Update()
{

}

void RenderPass_GenerateVarianceShadowMap::Destroy()
{

}

void RenderPass_GenerateVarianceShadowMap::DestroyGPUResources()
{
	ReleaseViews();
	ReleaseBuffers();

	GPUResourceInitialized = false;
}

void RenderPass_GenerateVarianceShadowMap::InitGPUResources()
{

	if (!GPUResourceInitialized)
	{

		InitViews();
		InitBuffers();


		GPUResourceInitialized = true;
	}
}

void RenderPass_GenerateVarianceShadowMap::RecompileShaders()
{
	ShaderList::VertexShaders::VS_BuildShadowMap::InitStatics();
	ShaderList::VertexShaders::VS_Render2D::InitStatics();

	ShaderList::PixelShaders::PS_SeperableBlurFilter::InitStatics();
	ShaderList::PixelShaders::PS_GenerateVarianceShadowMap::InitStatics();
}

//void RenderPass_GenerateVarianceShadowMap::SetObjects(std::vector<Model*>* Models)
//{
//	mModels = Models;
//}

ID3D11ShaderResourceView* RenderPass_GenerateVarianceShadowMap::GetVarianceShadowMap_SRV() const
{
	return mTexView_VarianceShadowMap->GetSRV();
}


const TexViews* RenderPass_GenerateVarianceShadowMap::GetTexViewVarianceShadowMap() const noexcept
{
	return mTexView_VarianceShadowMap.get();
}

std::string RenderPass_GenerateVarianceShadowMap::GPUTime_String()
{
	return "Generate Variance Shadow Map's Render Time : " + std::to_string(mGpuTimeStampDB->GetTimeDuration());
}

void RenderPass_GenerateVarianceShadowMap::RenderConfigurationUI()
{
	ImGui::Begin("Settings: Variance Shadow Map");

	static float filterWidth = 11.0f;

	if (ImGui::DragFloat("Blur Filter Width", &filterWidth, 1.0f, 1.0f, 50.0f))
	{
		ShaderList::PixelShaders::PS_SeperableBlurFilter::FilterWidth = filterWidth;
	}

	static std::string selectedFilterType = "Gaussian Filter";

	if (ImGui::BeginCombo("Blur Filter Type", selectedFilterType.c_str()))
	{
		if (ImGui::Selectable("Gaussian Filter"))
		{
			SetShader = ShaderList::PixelShaders::PS_SeperableBlurFilter::SetShader_GaussFilter;
			SetFilter_ClassInstance_Horizontally = ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_GaussFilterManager_Horizontally;
			SetFilter_ClassInstance_Vertically = ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_GaussFilterManager_Vertically;

		}
		if (ImGui::Selectable("Box Filter"))
		{
			SetShader = ShaderList::PixelShaders::PS_SeperableBlurFilter::SetShader_BoxFilter;
			SetFilter_ClassInstance_Horizontally = ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_BoxFilterManager_Horizontally;
			SetFilter_ClassInstance_Vertically = ShaderList::PixelShaders::PS_SeperableBlurFilter::SetClassInstace_BoxFilterManager_Vertically;
		}

		ImGui::EndCombo();
	}

	ImGui::End();
}
