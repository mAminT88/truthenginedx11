#pragma once

#include "IRenderPass.h"


class SRVCollector;
class TexViews;

class RenderPass_DeferredShading : public IRenderPass
{
protected:

	ID3D11RenderTargetView * pRTV[2];
	
	std::unique_ptr<TexViews> mTexView_HDR;
	std::unique_ptr<TexViews> mTexView_Color;

	std::unique_ptr<SRVCollector> mSRVCollector;

	//PipelineList::Pipeline_DeferredShading mPipeline;
	UINT mVertexOffset = 0;
	
	bool mEnableHDR;

	//virtual void InitPipelines() override;
	virtual void InitViews() override;
	virtual void ReleaseViews() override;
	virtual void InitBuffers() override;
	virtual void ReleaseBuffers() override;
 
public:

	RenderPass_DeferredShading();
	~RenderPass_DeferredShading();

	RenderPass_DeferredShading(RenderPass_DeferredShading&&);
	RenderPass_DeferredShading& operator = (RenderPass_DeferredShading&&);

	const TexViews* pTexViews_ColorMap;
	const TexViews* pTexViews_NormalMap;
	const TexViews* pTexViews_SpecularMap;
	const TexViews* pTexViews_DepthMap;
	const TexViews* pTexViews_ShadowMapArray;
	const TexViews* pTexViews_SSAOMap;
	const TexViews* pTexViews_SATUINT;

	virtual void Init() override;
	virtual void Draw3D() override;
	virtual void DrawDeferred() override;
	virtual void Draw2D() override;
	virtual void DrawTxt() override;
	virtual void Update() override;
	virtual void Destroy() override;
	virtual void CaptureFrame() override;
	virtual void RecompileShaders() override;
	virtual void InitGPUResources() override;
	virtual void DestroyGPUResources() override;
	virtual std::string GPUTime_String() override;

	/*void SetSRV(ID3D11ShaderResourceView*const ppColorMap,
		ID3D11ShaderResourceView* const ppNormalMap,
		ID3D11ShaderResourceView* const ppSpecularMap,
		ID3D11ShaderResourceView* const ppDepthMap,
		ID3D11ShaderResourceView* const ppShadowMapArray,
		ID3D11ShaderResourceView* const ppSSAOMap = nullptr,
		ID3D11ShaderResourceView* const ppSATUINT = nullptr);*/

	void SetSRVs();

	void SetSRV_SSAO(ID3D11ShaderResourceView* ssaoMap);

	void SetRTV(ID3D11RenderTargetView* rtv /*= gSwapChain.GetBackBufferRTV()*/);

	const TexViews* GetLuminanceMap() const noexcept;
	const TexViews* GetColorMap() const noexcept;

	void SetHDR(bool _enable);

	void RenderConfigurationUI() override;
};
