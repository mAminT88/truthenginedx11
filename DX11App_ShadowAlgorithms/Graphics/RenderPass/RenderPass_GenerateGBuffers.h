#pragma once

#include "IRenderPass.h"

class RTVCollector;
class TexViews;

template<class T> class ConstantBuffer;

class RenderPass_GenerateGBuffers : public IRenderPass
{
protected:

	struct CB_VS_BONETRANSFORMS
	{
		XMFLOAT4X4 gBoneTransforms[96];
	};

	struct CB_VS_PER_OBJECT
	{
		XMFLOAT4X4 gWVP;
		XMFLOAT4X4 gWorldInvTranspose;
	};

	struct CB_VS_PER_MESH
	{
		XMFLOAT4X4 gTexTransform;
	};


	struct CB_PS_PER_OBJECT
	{
		XMFLOAT4 diffuseColor;
		XMFLOAT4 ambientColor;
		XMFLOAT4 specularColor;
	};


	CB_VS_PER_OBJECT mData_CB_VS_Per_Object;
	CB_VS_PER_MESH mData_CB_VS_Per_Mesh;
	CB_PS_PER_OBJECT mData_CB_PS_Per_Object;
	CB_VS_BONETRANSFORMS mData_VS_BoneTransforms;


	std::unique_ptr<ConstantBuffer<CB_VS_PER_OBJECT>> mCB_VS_Per_Object;
	std::unique_ptr<ConstantBuffer<CB_VS_PER_MESH>> mCB_VS_Per_Mesh;
	std::unique_ptr<ConstantBuffer<CB_VS_BONETRANSFORMS>> mCB_VS_BoneTransforms;
	std::unique_ptr<ConstantBuffer<CB_PS_PER_OBJECT>> mCB_PS_Per_Object;


	//PipelineList::Pipeline_GenerateGBuffers mPipeline;


	std::unique_ptr<TexViews> mTexView_ColorMap;
	std::unique_ptr<TexViews> mTexView_NormalMap;
	std::unique_ptr<TexViews> mTexView_DepthMap;
	std::unique_ptr<TexViews> mTexView_SpecularMap;

	ID3D11ShaderResourceView** mSRV_ColorMap;
	ID3D11ShaderResourceView** mSRV_NormalMap;
	ID3D11ShaderResourceView** mSRV_DepthMap;
	ID3D11ShaderResourceView** mSRV_SpecularMap;


	std::unique_ptr<RTVCollector> mRTVCollector;


	UINT mVertexOffset = 0;

	/*std::vector<Model*>* pVector_Models;
	std::vector<SkinnedModel*>* pVector_SkinnedModels;*/


	//void InitPipelines() override;
	void InitViews() override;
	void InitBuffers() override;
	void ReleaseBuffers() override;
	void ReleaseViews() override;

public:

	RenderPass_GenerateGBuffers();
	~RenderPass_GenerateGBuffers();

	RenderPass_GenerateGBuffers(RenderPass_GenerateGBuffers&&);
	RenderPass_GenerateGBuffers& operator = (RenderPass_GenerateGBuffers&&);

	ID3D11ShaderResourceView* GetGBufferColor();
	ID3D11ShaderResourceView* GetGBufferNormal();
	ID3D11ShaderResourceView* GetGBufferDepth();
	ID3D11ShaderResourceView* GetGBufferSpecular();

	//void SetObjects(std::vector<Model*>* models, std::vector<SkinnedModel*>* skinnedModels = nullptr);

	virtual void Init() override;
	virtual void Draw3D() override;
	virtual void DrawDeferred() override;
	virtual void Draw2D() override;
	virtual void DrawTxt() override;
	virtual void Update() override;
	virtual void Destroy() override;
	virtual void DestroyGPUResources() override;
	virtual void InitGPUResources() override;
	virtual void CaptureFrame() override;
	virtual void RecompileShaders() override;
	virtual std::string GPUTime_String() override;

	const TexViews* GetTexViewColorMap() const noexcept;
	const TexViews* GetTexViewNormalMap()const noexcept;
	const TexViews* GetTexViewDepthMap()const noexcept;
	const TexViews* GetTexViewSpecularMap()const noexcept;
};
