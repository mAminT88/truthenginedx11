#include "stdafx.h"
#include "MenuItem.h"

void MenuItem::Init()
{
	m_menu = CreatePopupMenu();
}

void MenuItem::AddItem(std::wstring text, int id)
{
	AppendMenu(m_menu, MF_STRING, id, text.c_str());
}

void MenuItem::SetParent(HMENU & menu)
{
	
}
