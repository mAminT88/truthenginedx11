#include "stdafx.h"
#include "D3D.h"


#include "Globals.h"
#include "Graphics/RenderStates.h"
#include "Graphics/ShaderSlots_DeferredShading.h"

using namespace Globals;

void D3D::SetGlobalContextStates(ID3D11DeviceContext* context, bool Samplers, bool lights_ps, bool lights_vs)
{
		context->IASetIndexBuffer(gIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

		if (Samplers)
			RenderStates::SetSamplerStates(context);

		if (lights_ps)
			gLights.SetLightsBuffer_PS(context, SHADER_SLOTS_PS_CB_SPOTLIGHTS, SHADER_SLOTS_PS_CB_DIRLIGHTS);

		if (lights_vs)
			gLights.SetLightsBuffer_VS(context, SHADER_SLOTS_VS_CB_SPOTLIGHTS, SHADER_SLOTS_VS_CB_DIRLIGHTS);
}

void XM_CALLCONV D3D::CalcFrustumSplits(FXMMATRIX ProjMAtrix)
{
	BoundingFrustum bf;
	BoundingFrustum::CreateFromMatrix(bf, ProjMAtrix);
	XMFLOAT3 corners[8];
	bf.GetCorners(corners);
	
}
