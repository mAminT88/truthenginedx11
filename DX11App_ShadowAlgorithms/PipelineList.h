#pragma once

#include "stdafx.h"
#include "Pipeline.h"

namespace PipelineList
{
	class Pipeline_DeferredShading_Surface_Default : public Pipeline
	{
		Pipeline_DeferredShading_Surface_Default()
		{
			mPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

			pInputLayout = Vertex::GetInputLayout(VERTEX_TYPE_BASIC32);

			Vertex::GetVertexStride(VERTEX_TYPE_BASIC32, mVertexStride);

			mShaderProfile = "5_0";

			mRasterizerStateType = RENDER_STATE_RS_DEFAULT;
			mBlendStateType = RENDER_STATE_BS_DEFAULT;
			mDepthStateType = RENDER_STATE_DS_DEFAULT;

		};
	};
}
