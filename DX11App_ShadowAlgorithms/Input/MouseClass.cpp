#include "stdafx.h"
#include "MouseClass.h"

void MouseClass::OnLeftPressed(int x, int y)
{
	b_leftIsDown = true;
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::LPress, x, y));
}

void MouseClass::OnLeftReleased(int x, int y)
{
	b_leftIsDown = false;
	
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::LRelease, x, y));
}

void MouseClass::OnRightPressed(int x, int y)
{
	b_rightIsDown = true;
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::RPress, x, y));
}

void MouseClass::OnRightReleased(int x, int y)
{
	b_rightIsDown = false;
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::RRelease, x, y));
}

void MouseClass::OnMiddlePressed(int x, int y)
{
	b_middleIsDown = true;
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::MPress, x, y));
}

void MouseClass::OnMiddleReleased(int x, int y)
{
	b_middleIsDown = false;
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::MRelease, x, y));
}

void MouseClass::OnWheelUp(int x, int y)
{
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::WheelUp, x, y));
}

void MouseClass::OnWheelDown(int x, int y)
{
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::WheelDown, x, y));
}

void MouseClass::OnMouseMove(int x, int y)
{
	mLastX = mX;
	mLastY = mY;
	mX = x;
	mY = y;
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::Move, x, y));
}

void MouseClass::OnMouseMoveRaw(int x, int y)
{
	mX = x;
	mY = y;
	m_eventBuffer.push(MouseEvent(MouseEvent::EventType::Raw_Move, x, y));
}

bool MouseClass::IsLeftDown() const
{
	return b_leftIsDown;
}

bool MouseClass::IsRightDown() const
{
	return b_rightIsDown;
}

bool MouseClass::IsMiddleDown() const
{
	return b_middleIsDown;
}

int MouseClass::GetPosX() const
{
	return mX;
}

int MouseClass::GetPosY() const
{
	return mY;
}

int MouseClass::GetDX() const
{
	return mX - mLastX;
}

int MouseClass::GetDY() const
{
	return mY - mLastY;
}

MousePoint MouseClass::GetPos() const
{
	return { mX, mY };
}

bool MouseClass::IsEventBufferEmpty() const
{
	return m_eventBuffer.empty();
}

MouseEvent MouseClass::PopEvent()
{
	if (m_eventBuffer.empty())
		return MouseEvent();

	auto e = m_eventBuffer.front();
	m_eventBuffer.pop();
	return e;
}

MouseEvent* MouseClass::ReadEvent()
{
	if (m_eventBuffer.empty())
		return nullptr;

	auto& e = m_eventBuffer.front();
	return &e;
}
