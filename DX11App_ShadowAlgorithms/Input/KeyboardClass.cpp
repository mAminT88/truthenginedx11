#include "stdafx.h"
#include "KeyboardClass.h"
#include <queue>

KeyboardClass::KeyboardClass() : b_autoRepeatKeys(true), b_autoRepeatChars(false)
{
	for (bool & b_keyState : b_keyStates)
	{
		b_keyState = false;
	}
}

bool KeyboardClass::KeyIsPressed(const unsigned char keycode) const
{
	return b_keyStates[keycode];
}

bool KeyboardClass::KeyBufferIsEmpty() const
{
	return m_keyBuffer.empty();
}

bool KeyboardClass::CharBufferIsEmpty() const
{
	return m_charBuffer.empty();
}

KeyboardEvent KeyboardClass::PopKey()
{
	if (m_keyBuffer.empty())
		return KeyboardEvent();

	KeyboardEvent e = m_keyBuffer.front();
	m_keyBuffer.pop();
	return e;
}

KeyboardEvent* KeyboardClass::ReadKey()
{
	if (m_keyBuffer.empty())
		return nullptr;

	KeyboardEvent e = m_keyBuffer.front();
	return &e;
}

unsigned char KeyboardClass::PopChar()
{
	if (m_charBuffer.empty())
		return 0u;

	unsigned char c = m_charBuffer.front();
	m_charBuffer.pop();
	return c;
}

unsigned char* KeyboardClass::ReadChar()
{
	if (m_charBuffer.empty())
		return nullptr;

	unsigned char c = m_charBuffer.front();
	return &c;
}

void KeyboardClass::OnKeyPressed(const unsigned char key)
{
	b_keyStates[key] = true;
	m_keyBuffer.push(KeyboardEvent(KeyboardEvent::EventType::Press, key));
}

void KeyboardClass::OnKeyReleased(const unsigned char key)
{
	b_keyStates[key] = false;
	m_keyBuffer.push(KeyboardEvent(KeyboardEvent::EventType::Release, key));
}

void KeyboardClass::OnChar(const unsigned char key)
{
	m_charBuffer.push(key);
}

void KeyboardClass::EnableAutoRepeatKeys()
{
	b_autoRepeatKeys = true;
}

void KeyboardClass::DisableAutoRepeatKeys()
{
	b_autoRepeatKeys = false;
}

void KeyboardClass::EnableAutoRepeatChars()
{
	b_autoRepeatChars = true;
}

void KeyboardClass::DisableAutoRepeatChars()
{
	b_autoRepeatChars = false;
}

bool KeyboardClass::IsKeysAutoRepeat() const
{
	return b_autoRepeatKeys;
}

bool KeyboardClass::IsCharsAutoRepeat() const
{
	return b_autoRepeatChars;
}
