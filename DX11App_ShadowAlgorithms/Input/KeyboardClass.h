#pragma once

#include "KeyboardEvent.h"
#include <queue>

class KeyboardClass
{
public:
	KeyboardClass();
	bool KeyIsPressed(const unsigned char keycode) const;
	bool KeyBufferIsEmpty() const;
	bool CharBufferIsEmpty() const;
	KeyboardEvent PopKey();
	KeyboardEvent* ReadKey();
	unsigned char PopChar();
	unsigned char* ReadChar();
	void OnKeyPressed(const unsigned char key);
	void OnKeyReleased(const unsigned char key);
	void OnChar(const unsigned char key);
	void EnableAutoRepeatKeys();
	void DisableAutoRepeatKeys();
	void EnableAutoRepeatChars();
	void DisableAutoRepeatChars();
	bool IsKeysAutoRepeat() const;
	bool IsCharsAutoRepeat() const;
private:
	bool b_autoRepeatKeys;
	bool b_autoRepeatChars;
	bool b_keyStates[256];

	std::queue<KeyboardEvent> m_keyBuffer;
	std::queue<unsigned char> m_charBuffer;
};
