#include "stdafx.h"
#include <comdef.h>
#include "ErrorLogger.h"
#include "COMException.h"

void ErrorLogger::Log(std::string msg)
{
	std::string errorMsg = "Error: " + msg;
	MessageBoxA(NULL, errorMsg.c_str(), "Error", MB_ICONERROR);
}

void ErrorLogger::Log(HRESULT hr, std::string msg)
{
	_com_error error(hr);
	std::wstring errorMsg = L"Error: " + StringConverter::StringToWide(msg) + L"\n" + error.ErrorMessage();
	MessageBoxW(NULL, errorMsg.c_str(), L"Error", MB_ICONERROR);
}

void ErrorLogger::Log(COMException& exception)
{
	std::wstring error_message = exception.what();
	MessageBoxW(NULL, error_message.c_str(), L"Error", MB_ICONERROR);
}
