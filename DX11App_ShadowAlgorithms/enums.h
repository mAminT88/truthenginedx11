#pragma once



enum MSAA_MODE : int
{
	MSAA_1X = 1,
	MSAA_2X = 2,
	MSAA_4X = 4,
	MSAA_8X = 8,
	MSAA_16X = 16
};
