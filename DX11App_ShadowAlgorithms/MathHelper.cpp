#include "stdafx.h"
#include "MathHelper.h"

const XMFLOAT4X4A MathHelper::IdentityMatrix = XMFLOAT4X4A(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f);