// Win32-Directx11App-Template.cpp : Defines the entry point for the application.
//


#include "stdafx.h"
#include "Win32-Directx11App-Template.h"
#include "GameTimer.h"
#include "Engine.h"
#include "Globals.h"


using namespace Globals;

#define MAX_LOADSTRING 100

// Global Variables:                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

HWND h_MainWindow;
HMENU h_MainMenu;


// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

//App Variabels

//RendererEngine mDXEngine;
Engine mEngine;
GameTimer     mTimer;

//App Functions
void InitControls();



int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: Place code here.

	CoInitialize(NULL);


	mTimer.Reset();
	mEngine.Initialize(hInstance, "AminDirectX11App", "DX11AppMainWindow", gClientWidth, gClientHeight);


	// Main message loop:


	while (mEngine.ProcessMessages() == true)
	{
		mTimer.Tick();
		gDt = mTimer.DeltaTime();
		mEngine.Update();
		mEngine.Run();
	}

	return 0;

}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WIN32DIRECTX11APPTEMPLATE));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_WIN32DIRECTX11APPTEMPLATE);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	gHInstance = hInstance; // Store instance handle in our global variable

	RECT wr = { 0, 0, gClientWidth, gClientHeight };
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, TRUE);

	gHWND_App = CreateWindowW(szWindowClass, szTitle, WS_MAXIMIZE|WS_OVERLAPPEDWINDOW^WS_THICKFRAME,
		CW_USEDEFAULT, 0, 1600, 1200, nullptr, nullptr, hInstance, nullptr);

	if (!gHWND_App)
	{
		return FALSE;
	}

	gHWND_Rendering = CreateWindowW(szWindowClass, szTitle, WS_CHILD | WS_VISIBLE | WS_BORDER,
		CW_USEDEFAULT, 0, wr.right - wr.left, wr.bottom - wr.top, gHWND_App, nullptr, hInstance, nullptr);


	ShowWindow(gHWND_App, nCmdShow);
	UpdateWindow(gHWND_App);

	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	//mDXEngine.Catch_WCOMMAND(message, wParam, lParam);

	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		switch (wmId)
		{
		/*case IDC_LISTBOX_RIGHTSIDED:
			gLB_RightSided.ProcCommand(HIWORD(wParam));
			break;*/

		case IDM_ABOUT:
			DialogBox(gHInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code that uses hdc here...
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}


	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

void InitControls()
{
	std::shared_ptr<RECT> pRect = std::make_shared<RECT>();
	GetWindowRect(gHWND_App, pRect.get());

	auto mainWNDWidth = pRect->right - pRect->left;
	auto mainWNDHeight = pRect->bottom - pRect->top;

	float lbWidth = 200; float lbHeight = mainWNDHeight;

	//gLB_RightSided.Init(gHInstance, gHWND_App, mainWNDWidth - lbWidth, 0, lbWidth, lbHeight, IDC_LISTBOX_RIGHTSIDED);
}
