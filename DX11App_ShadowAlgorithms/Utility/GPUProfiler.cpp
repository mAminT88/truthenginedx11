#include "stdafx.h"
#include "GPUProfiler.h"

GPUTimeStamp::GPUTimeStamp(UINT queryNum, ID3D11Device* device)
{
	


	D3D11_QUERY_DESC desc;
	desc.MiscFlags = 0;
	desc.Query = D3D11_QUERY_TIMESTAMP;

	device->CreateQuery(&desc, m_query_end.ReleaseAndGetAddressOf());

	if (queryNum == 2)
	{
		device->CreateQuery(&desc, m_query_start.ReleaseAndGetAddressOf());
	}
}


void GPUTimeStamp::Destroy()
{
	m_query_start.Reset();
	m_query_end.Reset();
}

void GPUTimeStamp::SetStart(ID3D11DeviceContext* context) const
{
	context->End(m_query_start.Get());
}

void GPUTimeStamp::SetEnd(ID3D11DeviceContext* context) const
{
	context->End(m_query_end.Get());
}

ID3D11Query* GPUTimeStamp::GetStartQuery() const
{
	return m_query_start.Get();
}

ID3D11Query* GPUTimeStamp::GetEndQuery() const
{
	return m_query_end.Get();
}

UINT64 GPUTimeStamp::WaitAndGetEndTimeStamp(ID3D11DeviceContext* context)
{
	while (context->GetData(m_query_end.Get(), &m_endtimestamp, sizeof(UINT64), 0) == S_FALSE)
	{
		Sleep(1);
	}

	return m_endtimestamp;
}

float GPUTimeStamp::WaitAndCalculateTime(const float msGpuTime, ID3D11DeviceContext* context)
{
	UINT64 r;

	while (context->GetData(m_query_start.Get(), &r, sizeof(UINT64), 0) == S_FALSE)
	{
		Sleep(1);
	}

	while (context->GetData(m_query_end.Get(), &m_endtimestamp, sizeof(UINT64), 0) == S_FALSE)
	{
		Sleep(1);
	}

	m_ms_time = (m_endtimestamp - r) * msGpuTime;
	return m_ms_time;
}

float GPUTimeStamp::WaitAndCalculateTime(const float msGpuTime, const UINT64 start, ID3D11DeviceContext* context)
{
	while (context->GetData(m_query_end.Get(), &m_endtimestamp, sizeof(UINT64), 0) == S_FALSE)
	{
		Sleep(1);
	}

	m_ms_time = (m_endtimestamp - start) * msGpuTime;
	return m_ms_time;
}

float GPUTimeStamp::GetTimeDuration() const
{
	return m_ms_time;
}

UINT64 GPUTimeStamp::GetTimeStamp() const
{
	return m_endtimestamp;
}

bool GPUTimeStamp::IsDataReady(ID3D11DeviceContext* context) const
{
	if (m_query_start)
		if (context->GetData(m_query_start.Get(), nullptr, 0, 0) == S_FALSE)
		{
			return false;
		}
	if (m_query_end)
		if (context->GetData(m_query_end.Get(), nullptr, 0, 0) == S_FALSE)
		{
			return false;
		}

	return true;
}


//////////////////////////////////////////////////////////////////////////
// GPU Time Stamp Dual Buffer
//////////////////////////////////////////////////////////////////////////



GPUTimeStamp_DualBuffer::GPUTimeStamp_DualBuffer(UINT queryNum, ID3D11Device *device)
{
	D3D11_QUERY_DESC desc;
	desc.MiscFlags = 0;
	desc.Query = D3D11_QUERY_TIMESTAMP;

	m_query_start.emplace_back();
	m_query_start.emplace_back();

	m_query_end.emplace_back();
	m_query_end.emplace_back();

	device->CreateQuery(&desc, m_query_end[0].ReleaseAndGetAddressOf());
	device->CreateQuery(&desc, m_query_end[1].ReleaseAndGetAddressOf());

	if (queryNum == 2)
	{
		device->CreateQuery(&desc, m_query_start[0].ReleaseAndGetAddressOf());
		device->CreateQuery(&desc, m_query_start[1].ReleaseAndGetAddressOf());
	}
}

void GPUTimeStamp_DualBuffer::Destroy()
{
	m_query_start.clear();
	m_query_end.clear();
}

void GPUTimeStamp_DualBuffer::SetStart(const int frame, ID3D11DeviceContext* context) const
{
	context->End(m_query_start[frame].Get());
}

void GPUTimeStamp_DualBuffer::SetEnd(const int frame, ID3D11DeviceContext* context) const
{
	context->End(m_query_end[frame].Get());
}

ID3D11Query* GPUTimeStamp_DualBuffer::GetStartQuery(const int frame) const
{
	return m_query_start[frame].Get();
}

ID3D11Query* GPUTimeStamp_DualBuffer::GetEndQuery(const int frame) const
{
	return m_query_end[frame].Get();
}

UINT64 GPUTimeStamp_DualBuffer::WaitAndGetEndTimeStamp(const int frame, ID3D11DeviceContext* context)
{
	while (context->GetData(m_query_end[frame].Get(), &m_endtimestamp, sizeof(UINT64), 0) == S_FALSE)
	{
		Sleep(1);
	}


	return m_endtimestamp;
}

void GPUTimeStamp_DualBuffer::WaitAndCalculateTime(const int frame, const float msGpuTime, ID3D11DeviceContext* context)
{
	UINT64 r;

	while (context->GetData(m_query_start[frame].Get(), &r, sizeof(UINT64), 0) == S_FALSE)
	{
		Sleep(1);
	}

	while (context->GetData(m_query_end[frame].Get(), &m_endtimestamp, sizeof(UINT64), 0) == S_FALSE)
	{
		Sleep(1);
	}

	m_ms_time = (m_endtimestamp - r) * msGpuTime;

}

void GPUTimeStamp_DualBuffer::WaitAndCalculateTime(const int frame, const float msGpuTime, const UINT64 start, ID3D11DeviceContext* context)
{
	while (context->GetData(m_query_end[frame].Get(), &m_endtimestamp, sizeof(UINT64), 0) == S_FALSE)
	{
		Sleep(1);
	}

	m_ms_time = (m_endtimestamp - start) * msGpuTime;
}

float GPUTimeStamp_DualBuffer::GetTimeDuration() const
{
	return m_ms_time;
}

UINT64 GPUTimeStamp_DualBuffer::GetTimeStamp() const
{
	return m_endtimestamp;
}

bool GPUTimeStamp_DualBuffer::IsDataReady(const int frame, ID3D11DeviceContext* context) const
{
	if (m_query_start[frame])
		if (context->GetData(m_query_start[frame].Get(), nullptr, 0, 0) == S_FALSE)
		{
			return false;
		}
	if (m_query_end[frame])
		if (context->GetData(m_query_end[frame].Get(), nullptr, 0, 0) == S_FALSE)
		{
			return false;
		}

	return true;
}
