#pragma once



#include "Globals.h"

class GPUTimeStamp
{
private:
	ComPtr<ID3D11Query> m_query_start;
	ComPtr<ID3D11Query> m_query_end;
	float m_ms_time;
	UINT64 m_endtimestamp;


public:
	GPUTimeStamp(UINT queryNum, ID3D11Device *device = Globals::gDevice.Get());

	void Destroy();

	void SetStart(ID3D11DeviceContext* context) const;
	void SetEnd(ID3D11DeviceContext* context) const;

	ID3D11Query* GetStartQuery() const;
	ID3D11Query* GetEndQuery() const;

	UINT64 WaitAndGetEndTimeStamp(ID3D11DeviceContext* context);
	float WaitAndCalculateTime(const float msGpuTime, ID3D11DeviceContext* context);
	float WaitAndCalculateTime(const float msGpuTime, const UINT64 start, ID3D11DeviceContext* context);
	float GetTimeDuration() const;
	UINT64 GetTimeStamp() const;
	bool IsDataReady(ID3D11DeviceContext* context) const;
};


class GPUTimeStamp_DualBuffer
{
private:
	std::vector<ComPtr<ID3D11Query>> m_query_start;
	std::vector<ComPtr<ID3D11Query>> m_query_end;
	float m_ms_time;
	UINT64 m_endtimestamp;

public:
	GPUTimeStamp_DualBuffer() = default;
	GPUTimeStamp_DualBuffer(UINT queryNum, ID3D11Device *device = Globals::gDevice.Get());

	void Destroy();

	void SetStart(const int frame, ID3D11DeviceContext* context) const;
	void SetEnd(const int frame, ID3D11DeviceContext* context) const;

	ID3D11Query* GetStartQuery(const int frame) const;
	ID3D11Query* GetEndQuery(const int frame) const;

	UINT64 WaitAndGetEndTimeStamp(const int frame, ID3D11DeviceContext* context);
	void WaitAndCalculateTime(const int frame, const float msGpuTime, ID3D11DeviceContext* context);
	void WaitAndCalculateTime(const int frame, const float msGpuTime, const UINT64 start, ID3D11DeviceContext* context);
	float GetTimeDuration() const;
	UINT64 GetTimeStamp() const;
	bool IsDataReady(const int frame, ID3D11DeviceContext* context) const;
};
