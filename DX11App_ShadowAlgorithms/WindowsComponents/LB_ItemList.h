#pragma once

#include <map>

class LB_ItemList
{
private:
	std::map<int, std::wstring> items;

	HWND* lb;

public:

	std::function<void(int)> Func_SelectItem;
	std::function<void(int)> Func_DeSelectItem;


	void AddItem(int data, std::wstring name);
	void ClearAllItems();
	void SetListBox(HWND* _lb);
	std::map<int, std::wstring>& GetItems();
};