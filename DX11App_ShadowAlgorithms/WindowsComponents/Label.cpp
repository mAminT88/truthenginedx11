#include "stdafx.h"
#include "Label.h"

Label::Label(HINSTANCE& _mhInstance, HWND &_mhwnd_parent, int _posx, int _posy, int _width, int _height, LPCWSTR _text)
{
	mhLabel = CreateWindowEx(WS_EX_CLIENTEDGE, L"STATIC", _text, WS_CHILD | WS_VISIBLE, _posx, _posy, _width, _height, _mhwnd_parent, NULL, _mhInstance, NULL);

	ID_LABEL = (int)GetWindowLong(mhLabel, GWL_ID);
}

Label::~Label()
{
	delete mhLabel;
}


