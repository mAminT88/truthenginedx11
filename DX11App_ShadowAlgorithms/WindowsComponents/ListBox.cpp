#include "stdafx.h"
#include "ListBox.h"
#include <Globals.h>

ListBox::ListBox() : mCurrentIndex(-1)
{
}

ListBox::~ListBox()
{
	delete mhlistbox;
}

void ListBox::Init(HINSTANCE & hInstance, HWND & _parentHWnd, int _posx, int _posy, int _width, int _height)
{
	mhlistbox = CreateWindowEx(WS_EX_CLIENTEDGE,
		WC_LISTBOX,
		NULL,
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | ES_AUTOVSCROLL | LBS_NOTIFY,
		_posx, _posy,
		_width, _height,
		_parentHWnd,
		NULL,
		hInstance,
		NULL);

	ID_LISTBOX = (int)GetWindowLong(mhlistbox, GWL_ID);
}

void ListBox::Init(HINSTANCE & hInstance, HWND & _parentHWnd, int _posx, int _posy, int _width, int _height, int _id)
{
	mhlistbox = CreateWindowEx(WS_EX_CLIENTEDGE,
		WC_LISTBOX,
		NULL,
		WS_CHILD | WS_VISIBLE | WS_VSCROLL | LBS_NOTIFY,
		_posx, _posy,
		_width, _height,
		_parentHWnd,
		(HMENU)_id,
		hInstance,
		NULL);

	ID_LISTBOX = _id;
}

//void ListBox::AddItem(std::vector<std::wstring> _itemText, std::vector<int> _itemValue)
//{
//	for (int i = 0; i < _itemText.size(); i++)
//	{
//		int pos = (int)SendMessage(mhlistbox, LB_ADDSTRING, 0, (LPARAM)_itemText[i].c_str());
//
//		SendMessage(mhlistbox, LB_SETITEMDATA, pos, (LPARAM)i);
//	}
//}

void ListBox::AddItem()
{
	for (auto& i : pItemList->GetItems())
	{
		int pos = (int)SendMessageW(mhlistbox, LB_ADDSTRING, 0, (LPARAM)((LPCWSTR)i.second.c_str()));

		SendMessageW(mhlistbox, LB_SETITEMDATA, pos, (LPARAM)i.first);
	}
}

void ListBox::ProcCommand(WORD command)
{
	switch (command)
	{
	case LBN_SELCHANGE:
	{
		auto selectedIndex = GetCurrentIndex();
		SetFocus(Globals::gHWND_Rendering);
		if (selectedIndex == mCurrentIndex)
		{
			pItemList->Func_DeSelectItem(-1);
			ListBox_SetCurSel(mhlistbox, -1);
			mCurrentIndex = -1;
			return;
		}
		pItemList->Func_SelectItem(GetCurrentData());
		mCurrentIndex = selectedIndex;
		break;
	}
	case LBN_SELCANCEL:
		pItemList->Func_DeSelectItem(-1);
		break;
	}
}


void ListBox::SetItemList(LB_ItemList *items)
{
	pItemList = items;

	pItemList->SetListBox(&mhlistbox);

	AddItem();

}

int ListBox::GetCurrentIndex()const
{
	return (int)SendMessage(mhlistbox, LB_GETCURSEL, 0, 0);
}

int ListBox::GetCurrentData()const
{
	auto _itemIndex = (int)SendMessage(mhlistbox, LB_GETCURSEL, 0, 0);

	return (int)SendMessage(mhlistbox, LB_GETITEMDATA, _itemIndex, 0);
}

int ListBox::GetIDC()const
{
	return ID_LISTBOX;
}




