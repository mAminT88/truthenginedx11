#pragma once

#include "LB_ItemList.h"

class ListBox
{
private:
	int ID_LISTBOX;
	int mCurrentIndex;
	HWND mhlistbox;
	
	LB_ItemList* pItemList = nullptr;
	

public:
	ListBox();	
	~ListBox();

	void Init(HINSTANCE& hInstance, HWND &_parentHWnd, int _posx, int _posy, int _width, int _height); 
	void Init(HINSTANCE& hInstance, HWND &_parentHWnd, int _posx, int _posy, int _width, int _height, int _id);

	//void AddItem(std::vector<std::wstring> _itemText, std::vector<int> _itemValue);
	void AddItem();

	void ProcCommand(WORD command);


	void SetItemList(LB_ItemList *items);

	int GetCurrentIndex()const;
	int GetCurrentData()const;
	int GetIDC()const;
};
