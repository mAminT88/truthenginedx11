#include "stdafx.h"
#include "LB_ItemList.h"

void LB_ItemList::AddItem(int data, std::wstring name)
{

	items[data] = name;

	if (lb != nullptr)
	{
		int pos = (int)SendMessage(*lb, LB_ADDSTRING, 0, (LPARAM)name.c_str());

		SendMessage(*lb, LB_SETITEMDATA, pos, (LPARAM)data);
	}

}

void LB_ItemList::ClearAllItems()
{
	items.clear();
}

void LB_ItemList::SetListBox(HWND* _lb)
{
	lb = _lb;
}

std::map<int, std::wstring>& LB_ItemList::GetItems()
{
	return items;
}

