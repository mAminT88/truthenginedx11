#pragma once


class Label
{
public:
	Label(HINSTANCE& _mhInstance, HWND &_mhwnd_parent, int _posx, int _posy, int _width, int _height, LPCWSTR _text);
	~Label();

	int ID_LABEL;

private:
	HWND mhLabel;
};