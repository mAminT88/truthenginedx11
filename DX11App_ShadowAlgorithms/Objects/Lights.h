#pragma once

//#include "D3DUtil.h"
//#include "Graphics/ConstantBuffer.h"
//#include "Graphics/TexViews.h"
//#include "Graphics/ShaderSlots_DeferredShading.h"

#include "XMathSerialization.h"

#include "Macros.h"

class Lights;
class TexViews;
template<class T> class ConstantBuffer;

enum LIGHT_TYPE
{
	LIGHT_TYPE_DIRECTIONAL,
	LIGHT_TYPE_SPOT,
	LIGHT_TYPE_POINT,
	LIGHT_TYPE_NONE
};


//////////////////////////////////////////////////////////////////////////
// Light Data Structures
//////////////////////////////////////////////////////////////////////////

struct ILight
{
	ILight() : ShadowMapID(-1)
	{
		ZeroMemory(this, sizeof(ILight));
	}

	XMFLOAT4X4 View;
	XMFLOAT4X4 ViewProj;
	XMFLOAT4X4 ShadowTransform;

	XMFLOAT4 PerpectiveValues;

	XMFLOAT4 Diffuse;
	XMFLOAT4 Ambient;
	XMFLOAT4 Specular;

	XMFLOAT3 Direction;
	float	 LightSize;

	XMFLOAT3 Position;
	float	 zNear;

	XMFLOAT2 DirInEyeScreen_Horz;
	XMFLOAT2 DirInEyeScreen_Vert;

	float	 zFar;
	int32_t      CastShadow;
	int32_t      ID;
	int32_t      ShadowMapID;

};


struct DirectionalLight : public ILight
{

};


struct SpotLight : public ILight
{

	XMFLOAT3 Attenuation;
	float Range;

	float Spot;
	XMFLOAT3 pad;
};



//////////////////////////////////////////////////////////////////////////
// Light Object Classes
//////////////////////////////////////////////////////////////////////////

class ILightObj
{

protected:
	
	int mIndex;


	ILight* ILightData = nullptr;


	static const XMFLOAT4X4 mProjToUV;


	XMFLOAT4X4 mProjMatrix;
	XMFLOAT4 mLightTarget;
	XMFLOAT4 mUpVector;


	bool mUpdated;
	bool mUpdated_View;
	bool mUpdated_Proj;


	float mFOVYAngle = XM_PI * 0.38f;
	float mAspectRatio = 1.0f;


	BoundingFrustum mBoundingFrustum;


	LIGHT_TYPE mLightType = LIGHT_TYPE_NONE;
	
	void InitGpuResources();
	void ReleaseGpuResources();

	std::function<void(const float&)> mFunc_Update = [](const float& dt) {return; };

#pragma region GPU Resources

	ComPtr<ID3D11DeviceContext> mDeferredContext;
	ComPtr<ID3D11CommandList> mCommandList;

	//Cascade Shadow Map Resources
	ComPtr<ID3D11DeviceContext> mDeferredContext_C[4];
	ComPtr<ID3D11CommandList> mCommandList_C[4];
	
	std::unique_ptr<TexViews> mTexView_ShadowMap;
	D3D11_VIEWPORT mViewport;

	struct CB_PER_OBJECT
	{
		XMFLOAT4X4 gWorld;
	};

	struct CB_PER_LIGHT
	{
		XMFLOAT4X4 gLightVP;
		XMFLOAT2 gZNearFar;
		XMFLOAT2 pad;
		XMFLOAT3 gLightPosition;
		float pad2;
	};


	CB_PER_OBJECT mDataCBPerObject;
	CB_PER_LIGHT  mDataCBPerLight;


	static std::unique_ptr<ConstantBuffer<CB_PER_OBJECT>> mCBPerObject;
	static std::unique_ptr<ConstantBuffer<CB_PER_LIGHT>> mCBperLight;


#pragma endregion

	


	//Serialization Functions

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive& ar, const unsigned int version)
	{
		ar& Name;
		ar& bDisabled;
		ar& bCascadedShadowMap;
		ar& bShadowDynamicObjects;
		ar& mLightTarget;
		ar& mUpVector;
		ar& mFOVYAngle;
		ar& mAspectRatio;
		ar& ILightData->Diffuse;
		ar& ILightData->Ambient;
		ar& ILightData->Specular;
		ar& ILightData->Direction;
		ar& ILightData->LightSize;
		ar& ILightData->Position;
		ar& ILightData->zNear;
		ar& ILightData->zFar;
		ar& ILightData->CastShadow;
		ar& ILightData->ID;
		ar& ILightData->ShadowMapID;

		UpdateViewMatrix();
		UpdateProjectionMatrix();
		UpdateViewProjMatrix();
		CreateBoundingFrustum();
	}

	template<class Archive>
	void save(Archive& ar, const unsigned int version) const {
		ar& Name;
		ar& bDisabled;
		ar& bCascadedShadowMap;
		ar& bShadowDynamicObjects;
		ar& mLightTarget;
		ar& mUpVector;
		ar& mFOVYAngle;
		ar& mAspectRatio;
		ar& ILightData->Diffuse;
		ar& ILightData->Ambient;
		ar& ILightData->Specular;
		ar& ILightData->Direction;
		ar& ILightData->LightSize;
		ar& ILightData->Position;
		ar& ILightData->zNear;
		ar& ILightData->zFar;
		ar& ILightData->CastShadow;
		ar& ILightData->ID;
		ar& ILightData->ShadowMapID;
	}

	BOOST_SERIALIZATION_SPLIT_MEMBER();

public:

	ILightObj();
	virtual ~ILightObj();

	ILightObj(ILightObj&&);
	ILightObj& operator=(ILightObj&&);

	std::string Name;
	bool bDisabled;
	bool bShadowDynamicObjects;
	bool bCascadedShadowMap;



	virtual ILight& GetILightData() const;


	virtual void UpdateDiffuseColor(XMFLOAT4 _diffuseColor);
	virtual void UpdateAmbientColor(XMFLOAT4 _ambientColor);
	virtual void UpdateSpecularColor(XMFLOAT4 _specularColor);
	virtual void UpdateViewMatrix();
	virtual void UpdateViewMatrix(XMFLOAT4 _position, XMFLOAT4 _target, XMFLOAT4 _up);
	virtual void UpdateViewMatrix(XMFLOAT4X4 _view);
	virtual void UpdateShadowTransformMatrix();
	virtual void UpdateViewProjMatrix();
	virtual void UpdateViewProjMatrix(XMFLOAT4X4 _view, XMFLOAT4X4 _proj);
	virtual void SetDirection(XMFLOAT3 _direction);
	virtual void SetDirection(float x, float y, float z);
	virtual void SetPosition(XMFLOAT3 _position);
	virtual void SetPosition(float x, float y, float z);
	virtual void SetLightTarget(XMFLOAT3 _targetPosition);
	virtual void SetUpVector(XMFLOAT4 _up);
	virtual void SetCastShadow(bool _castshadow);
	//Create And Update Bounding Frustum
	virtual void CreateBoundingFrustum();
	//Return state of BoundingBox relative to Light Bounding Frustum
	virtual ContainmentType BoundingBoxContainment(BoundingBox _boundingBox);
	virtual void XM_CALLCONV UpdateDirInEyeScreen(const FXMVECTOR EyePosition, const XMFLOAT3 EyeViewDir);

	
	virtual bool IsUpdatedFromLastFrame();
	virtual void SetUpdated(bool _updated);
	virtual void Update(const float& dt);

	int GetIndex()const;

	LIGHT_TYPE GetLightType()const;

	virtual void GenerateShadowMap();
	virtual void GenerateCascadedShadowMap(ID3D11DeviceContext* dc, ComPtr<ID3D11CommandList>& cl, XMMATRIX& proj_matrix, float view_top_left_x, float view_top_left_y);
	ID3D11ShaderResourceView* GetShadowMapSRV()const noexcept;
	ID3D11DeviceContext* GetDeferredContext()const noexcept;
	ID3D11DeviceContext* GetDeferredContext(int num)const noexcept;
	ID3D11CommandList* GetCmdList()const noexcept;
	ID3D11CommandList* GetCmdList(int num)const noexcept;
	const TexViews* GetShadowMapTexView()const noexcept;
	

	static void InitStaticConstantBuffers();

	/*
	*Virtual Functions
	*/

	virtual void UpdateProjectionMatrix() = 0;

};

	  ///////////////////////////////////////////////////////////////////////
	  ///Direct Lights
	  ///////////////////////////////////////////////////////////////////////
class DirectionalLightObj : public ILightObj
{

protected:

	float mFrustumRadius;

	DirectionalLight* LightData = nullptr;


	//Boost Serialization

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive& ar, const unsigned int version) {
		ar& mFrustumRadius;

		ar& boost::serialization::base_object<ILightObj>(*this);
	}


	template<class Archive>
	void save(Archive& ar, const unsigned int version) const {
		ar& mFrustumRadius;

		ar& boost::serialization::base_object<ILightObj>(*this);
	}


	BOOST_SERIALIZATION_SPLIT_MEMBER();


public:

	//DirectionalLightObj(FXMVECTOR _diffusecolor, FXMVECTOR _ambientColor, FXMVECTOR _specularColor, CXMVECTOR _target, XMFLOAT3 _direction, XMFLOAT3 _position, std::string _name, float _lightSize, float _znear, float _zfar, float _frustumRadius, int _castShadow, int _id, int _shadowMapID);
	DirectionalLightObj(DirectionalLight* lightData, XMFLOAT4 _diffusecolor, XMFLOAT4 _ambientColor, XMFLOAT4 _specularColor, XMFLOAT4 _target, XMFLOAT3 _direction, XMFLOAT3 _position, std::string _name, float _lightSize, float _znear, float _zfar, float _frustumRadius, int _castShadow, int index, int _id, int _shadowMapID, bool cascaded_shadow_map);
	DirectionalLightObj(DirectionalLight* lightData);

	//DirectionalLight& LightData = static_cast<DirectionalLight&>(ILightData);

	void SetLightData(DirectionalLight* directionalLightData);


	void UpdateProjectionMatrix() override;

	float GetFrustumRadius() const;
	void SetFrustumRadius(float frustumRadius);


	//void EditProperty(int _propIndex, float* value) override;


	//std::vector<std::tuple<std::string, int, std::vector<float>>> GetEditableProperties() override;


	void UpdateFrustum(float _znear, float _zfar, float _frustumRadius);


};


class DirectionalLightObj_Dynamic : public DirectionalLightObj
{
	bool updated = false;
};


class DirectionalLightObj_Static : public DirectionalLightObj
{

};


	  ///////////////////////////////////////////////////////////////////////
	  ///Spot Lights
	  ///////////////////////////////////////////////////////////////////////
class SpotLightObj : public ILightObj
{

protected:

	SpotLight* LightData;

	//Boost Serialization

	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive& ar, const unsigned int version) const {
		ar& boost::serialization::base_object<ILightObj>(*this);

		ar& LightData->Attenuation;
		ar& LightData->Range;
		ar& LightData->Spot;
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int version) {
		ar& boost::serialization::base_object<ILightObj>(*this);

		ar& LightData->Attenuation;
		ar& LightData->Range;
		ar& LightData->Spot;
	}


	BOOST_SERIALIZATION_SPLIT_MEMBER();


public:

	//SpotLightObj(FXMVECTOR _diffusecolor, FXMVECTOR _ambientColor, FXMVECTOR _specularColor, CXMVECTOR _target, XMFLOAT3 _direction, XMFLOAT3 _position, XMFLOAT3 _attenuation, std::string _name, float _lightSize, float _znear, float _zfar, float _range, float _spot, int _castShadow, int _id, int _shadowMapID, int _reversedZ);
	SpotLightObj(SpotLight* lightData, XMFLOAT4 _diffusecolor, XMFLOAT4 _ambientColor, XMFLOAT4 _specularColor, XMFLOAT4 _target, XMFLOAT3 _direction, XMFLOAT3 _position, XMFLOAT3 _attenuation, std::string _name, float _lightSize, float _znear, float _zfar, float _range, float _spot, int _castShadow, int index, int _id, int _shadowMapID, int _reversedZ, bool cascaded_shadow_map);
	SpotLightObj(SpotLight* lightData);

	void SetLightData(SpotLight* spotLightData);

	void UpdateFrustum(float _znear, float _zfar);
	void SetRange(float _range);
	void SetAttenuation(float _d1, float _d2, float _d3);
	void SetSpotSize(float _spotPow);

	float& GetSpotSize() const;
	float& GetRange() const;
	XMFLOAT3& GetAttenuation() const;

	void UpdateProjectionMatrix() override;

	//void EditProperty(int _propIndex, float* value) override;

	//std::vector<std::tuple<std::string, int, std::vector<float>>> GetEditableProperties() override;

};


class SpotLightObj_Dynamic : public SpotLightObj
{
	bool updated = false;
};


class SpotLightObj_Static : public SpotLightObj
{

};


//////////////////////////////////////////////////////////////////////////
//Lights Manager
//////////////////////////////////////////////////////////////////////////

struct CB_SPOTLIGHTS
{
	SpotLight gSpotLightArray[MAX_LIGHT_SPOTLIGHT];
};


struct CB_DIRECTIONALLIGHTS
{
	DirectionalLight gDirectionalLightArray[MAX_LIGHT_DIRECTIONALLIGHT];
};


class Lights
{
private:	

	CB_SPOTLIGHTS mData_CB_SpotLights;
	CB_DIRECTIONALLIGHTS mData_CB_DirectionalLight;

	UINT mDirectionalLightCount, mSpotLightCount;

	UINT mShadowMapCount = 0;

	bool mMoveLightByCamera = false;


	/*ComPtr<ID3D11Buffer> mD3DDirectionalLightsB, mD3DSpotLightsB;
	D3D11_MAPPED_SUBRESOURCE mMapDirectionalLights, mMapSpotLights;
	ComPtr<ID3D11ShaderResourceView> mSRVDirectionalLights, mSRVSpotLights;*/

	std::unique_ptr<ConstantBuffer<CB_SPOTLIGHTS>> mCB_SpotLights;
	std::unique_ptr<ConstantBuffer<CB_DIRECTIONALLIGHTS>> mCB_DirectLights;

	void InitBuffers();

	//Generate List Box Items Of Lights

	int mSelectedLightByListBox = -1;
// 	//LB_ItemList m_itemList;
// 	void GenerateLBItems();

	void SetLightsBuffer_PS_DeferredContext();
	void SetLightsBuffer_VS_DeferredContext();

	//Boost Serialization

	friend class boost::serialization::access;

	template<class Archive>
	void save(Archive& ar, const unsigned int version) const {
		auto directionalLightCount = mDirectionalLights.size();
		auto spotLightCount = mSpotLights.size();

		ar& directionalLightCount;
		ar& spotLightCount;

		for (int i = 0; i < mDirectionalLightCount; ++i)
		{
			ar& mDirectionalLights[i];
		}

		for (int i = 0; i < mSpotLightCount; ++i)
		{
			ar& mSpotLights[i];
		}
	}

	template<class Archive>
	void load(Archive& ar, const unsigned int version) {
		ar& mDirectionalLightCount;
		ar& mSpotLightCount;

		mDirectionalLights.reserve(mDirectionalLightCount);
		mSpotLights.reserve(mSpotLightCount);



		for (int i = 0; i < mDirectionalLightCount; ++i)
		{
			mDirectionalLights.emplace_back(DirectionalLightObj(&mData_CB_DirectionalLight.gDirectionalLightArray[i]));

			ar& mDirectionalLights.back();
		}

		for (int i = 0; i < mSpotLightCount; ++i)
		{
			mSpotLights.emplace_back(SpotLightObj(&mData_CB_SpotLights.gSpotLightArray[i]));

			ar& mSpotLights.back();
		}
	}

	BOOST_SERIALIZATION_SPLIT_MEMBER();

	ILightObj* mCurrentSelectedLight;

public:
	Lights();
	~Lights();

	Lights(Lights&&);
	Lights& operator = (Lights&&);

	void Destroy();

	void ResetShadowMapIDs();
	UINT GetShadowMapCount();


	std::vector<DirectionalLightObj> mDirectionalLights = {};
	std::vector<SpotLightObj> mSpotLights = {};


	std::vector<ILightObj*> mCollectionILights;


	void Update(const float& dt);
	void MoveLight(int lightIndex, XMFLOAT3 position, XMFLOAT3 direction);
	void MoveLight(XMFLOAT3 position, XMFLOAT3 direction);
	void LightSelected(int lightIndex);

	void ImportLights(std::wstring _lightFile, ID3D11DeviceContext* _context);
	void SaveLights(std::wstring _lightFile);
	void LoadLights(std::wstring _lightFile, ID3D11DeviceContext* _context);
	void UpdateLightBuffers(ID3D11DeviceContext* _context);
	void SetLightsBuffer_PS(ID3D11DeviceContext* _context, UINT spotLightRegisterSlot, UINT dirLightRegisterSlot);
	void SetLightsBuffer_VS(ID3D11DeviceContext* _context, UINT spotLightRegisterSlot, UINT dirLightRegisterSlot);

	void PrepareAllShadowMapsCMDListAsyc();
	void PrepareAllShadowMapsCMDList();
	void GenerateAllShadowMaps();

	//void SetListBoxItems(ListBox* listBox);

	void RenderConfigurationUI();

};


