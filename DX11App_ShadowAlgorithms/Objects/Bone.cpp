#include "stdafx.h"
#include "Bone.h"

BoneWeight::BoneWeight(UINT BoneIndex, float BoneWeight) : mBoneIndex(BoneIndex), mBoneWeight(BoneWeight)
{

}

Bone::Bone()
{

}

Bone::~Bone()
{
	ReleaseChildrens();
}

int Bone::ReleaseChildrens()
{
	for (auto ptr : mChildren)
		delete ptr;

	return 0;
}
