#pragma once



class Bone
{
public:
	Bone();
	~Bone();
	Bone(Bone&&) = default;
	Bone& operator = (Bone&&) = default;
	Bone(Bone&) = default;
	Bone& operator = (Bone&) = default;

	std::string mName;
	
	XMFLOAT4X4 mMatrix_Offset;

	XMFLOAT4X4 mMatrix_localTransform;

	XMFLOAT4X4 mMatrix_GlobalTransform;

	XMFLOAT4X4 mMatrix_OriginlaLocalTransform;

	Bone* mParent;

	std::vector<Bone*> mChildren;


private:

	int ReleaseChildrens();

};

class BoneWeight
{
public:

	BoneWeight(UINT BoneIndex, float BoneWeight);

	uint8_t mBoneIndex;
	float mBoneWeight;
};