#pragma once

#include "Mesh.h"
#include "XMathSerialization.h"
//#include "Bone.h"

class BoneWeight;
class SA_Animation;



class Model
{
public:

	Model();
	~Model();
	Model(Model&&);
	Model& operator = (Model&&);

	std::string mName;


	int ID;


	std::vector<IMesh*> mMeshes;

	XMFLOAT4X4A mWorldMatrix;
	XMFLOAT4X4A mWorldInvTransposeMatrix;


	bool mDisabled;
	bool mGetLight;
	bool mCastShadow;

	bool Initialize(const std::string& filepath, ID3D11Device *device, ID3D11DeviceContext* context);

	virtual bool LoadModel(const std::string& filepath);

	void Draw(ID3D11DeviceContext* context);

	virtual void Update();



protected:

	size_t mMeshCount;

	boost::filesystem::path mFilePath;

	int mMaterialIndexOffset = 0;

	/*std::map<std::string, std::unique_ptr<Texture>> gTextures;
	std::vector<Material> gMaterials;*/

	


	void CalcWorldInvTranspose();

	virtual bool ProcessNode(aiNode* node, const aiScene * scene);
	virtual IMesh* ProcessMesh(aiMesh *mesh, const aiScene* scene);
	virtual bool ProcessMaterials(const aiScene* scene);
	virtual bool ProcessTextures(const aiScene* scene);

	void AssignMaterialTextures();

	//virtual int InitBuffers();


	//Boost::Serialization Functions

private:
#pragma region Serialization

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive& ar, const unsigned int version)
	{
		ar& ID;
		ar& mName;
		ar& mDisabled;
		ar& mGetLight;
		ar& mCastShadow;
		ar& mMeshCount;
		ar& mWorldMatrix;

		/*size_t TexCount;
		ar& TexCount;

		for (int i = 0; i < TexCount; ++i)
		{
			std::string texKey;
			ar& texKey;

			Texture* tex = new Texture();

			ar& *tex;
			gTextures[texKey] = std::unique_ptr<Texture>(tex);
		}


		ar& mMaterials;*/

		AssignMaterialTextures();

		mMeshes.reserve(mMeshCount);
		mMeshes.clear();

		for (int i = 0; i < mMeshCount; ++i)
		{
			VERTEX_TYPE type;
			ar& type;

			switch (type)
			{
			case VERTEX_TYPE_BASIC32:
			{
				Mesh_Basic32* mesh = new Mesh_Basic32();

				ar& mesh;

				/*mesh->SetMaterial(&mMaterials[mesh->GetMatrialIndex()]);*/

				mMeshes.push_back(mesh);

				break;
			}
			default:
				break;
			}

		}

		CalcWorldInvTranspose();

	}



	template<class Archive>
	void save(Archive& ar, const unsigned int version) const
	{
		ar& ID;
		ar& mName;
		ar& mDisabled;
		ar& mGetLight;
		ar& mCastShadow;
		ar& mMeshCount;
		ar& mWorldMatrix;

		/*ar& gTextures.size();

		for (auto const& it : gTextures)
		{
			ar& it.first;
			ar& *it.second.get();
		}

		ar& mMaterials;*/

		for (int i = 0; i < mMeshes.size(); ++i)
		{

			IMesh* mesh = mMeshes[i];
			ar& mesh->mVertexType;

			switch (mesh->mVertexType)
			{
			case VERTEX_TYPE_BASIC32:
			{
				Mesh_Basic32* casted_mesh = dynamic_cast<Mesh_Basic32*>(mesh);
				ar& casted_mesh;
				break;
			}
			}
		}

	}

	BOOST_SERIALIZATION_SPLIT_MEMBER();

	                                                                

#pragma endregion Serialization


};

class SkinnedModel : public Model
{
private:

	std::unique_ptr<SA_Animation> mAnimation;

	std::string mCurrentAnimName;

	//std::map<std::string> mAnimQueue;

	float mTimePos;


	void ExtractBoneWeightsForVertecies(aiMesh* mesh, std::map<UINT, std::vector<BoneWeight>>* VertexToBone);

	//virtual int InitBuffers() override;

public:

	SkinnedModel();
	virtual ~SkinnedModel();
	SkinnedModel(const SkinnedModel&);
	SkinnedModel(SkinnedModel&&);
	SkinnedModel& operator = (SkinnedModel&&);


	virtual bool LoadModel(const std::string& filepath) override;
	virtual IMesh* ProcessMesh(aiMesh *mesh, const aiScene* scene) override;
	virtual void Update();
	virtual std::vector<XMFLOAT4X4>* GetBoneTransforms() const;
	virtual bool SetAnimation(const std::string& AnimName);
	virtual bool SetAnimation(int AnimIndex);
	virtual bool DisableAnimation(const std::string& AnimName);
	virtual bool DisableAnimation(int AnimIndex);


};



