#include "stdafx.h"
#include "Model.h"
#include "Globals.h"
#include "MathHelper.h"
#include "StringConverter.h"

#include "Objects/Mesh.h"
#include "Objects/Bone.h"
#include "Objects/SkinAnimation.h"

#include "Graphics/Vertex.h"
#include "Graphics/Material.h"
#include "Graphics/Texture.h"

using namespace Globals;

Model::Model() : mName(""), ID(-1), mDisabled(false), mGetLight(true), mCastShadow(true), mWorldMatrix(MathHelper::IdentityMatrix), mWorldInvTransposeMatrix(MathHelper::IdentityMatrix)
{
}

Model::~Model()
{
	for (auto& mesh : mMeshes)
	{
		delete mesh;
	}
}

Model::Model(Model&&) = default;

Model& Model::operator=(Model&&) = default;

bool Model::LoadModel(const std::string& filepath)
{
	mFilePath = filepath.c_str();

	Assimp::Importer importer;

	//importer.SetPropertyBool("AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS", false);
	importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
	auto b = importer.GetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS);

	const aiScene* pScene = importer.ReadFile(filepath, aiProcess_Triangulate | aiProcess_ConvertToLeftHanded | aiProcess_FlipUVs);


	if (pScene == nullptr)
		return false;

	ProcessMaterials(pScene);


	ProcessNode(pScene->mRootNode, pScene);


	mMeshCount = mMeshes.size();

	return true;

	return false;
}

void Model::CalcWorldInvTranspose()
{
	mWorldInvTransposeMatrix = MathHelper::InverseTranspose(&mWorldMatrix);
}

bool Model::ProcessNode(aiNode* node, const aiScene* scene)
{
	for (UINT i = 0; i < node->mNumMeshes; ++i)
	{

		aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
		mMeshes.emplace_back();
		mMeshes.back() = ProcessMesh(mesh, scene);

	}

	for (UINT i = 0; i < node->mNumChildren; ++i)
	{
		ProcessNode(node->mChildren[i], scene);
	}

	return true;
}

IMesh* Model::ProcessMesh(aiMesh* mesh, const aiScene* scene)
{

	/*std::vector<Vertex::Basic32>* vertecies = new std::vector<Vertex::Basic32>();

	std::vector<UINT32>* indecies = new std::vector<UINT32>();*/

	auto indexOffset = gIndecies.size();
	auto vertexOffset = gVertecies_Basic32.size();
	int indexCount = 0;

	gVertecies_Basic32.reserve(vertexOffset + (size_t)mesh->mNumVertices);

	for (UINT i = 0; i < mesh->mNumVertices; ++i)
	{

		gVertecies_Basic32.emplace_back();
		//Vertex::Basic32& vertex = vertecies->back();
		Vertex::Basic32& vertex = gVertecies_Basic32.back();

		vertex.Pos.x = mesh->mVertices[i].x;
		vertex.Pos.y = mesh->mVertices[i].y;
		vertex.Pos.z = mesh->mVertices[i].z;

		if (mesh->HasTextureCoords(0))
		{
			vertex.Tex.x = mesh->mTextureCoords[0][i].x;
			vertex.Tex.y = mesh->mTextureCoords[0][i].y;
		}

		if (mesh->HasNormals())
		{
			vertex.Normal.x = mesh->mNormals[i].x;
			vertex.Normal.y = mesh->mNormals[i].y;
			vertex.Normal.z = mesh->mNormals[i].z;
		}

	}

	for (UINT i = 0; i < mesh->mNumFaces; ++i)
	{
		aiFace face = mesh->mFaces[i];

		for (UINT j = 0; j < face.mNumIndices; ++j)
		{
			gIndecies.push_back(face.mIndices[j]);
			indexCount++;
		}
	}



	std::string name = mesh->mName.C_Str();

	auto material = gModelManager.gMaterials[mesh->mMaterialIndex + mMaterialIndexOffset].get();

	XMFLOAT4X4 texTransform;
	XMStoreFloat4x4(&texTransform, XMMatrixIdentity());

	BoundingBox AABB;
	BoundingBox::CreateFromPoints(AABB, mesh->mNumVertices, &gVertecies_Basic32[vertexOffset].Pos, sizeof(Vertex::Basic32));

	IMesh* m = new Mesh_Basic32(name, this, mesh->mNumVertices, indexCount, indexOffset, vertexOffset, material, mesh->mMaterialIndex + mMaterialIndexOffset, texTransform, AABB, gDevice.Get(), gContext.Get());

	return m;

}

bool Model::ProcessMaterials(const aiScene* scene)
{

	ProcessTextures(scene);

	//gModelManager.gMaterials.reserve(gModelManager.gMaterials.size() + (size_t)scene->mNumMaterials);

	mMaterialIndexOffset = gModelManager.gMaterials.size();

	for (UINT i = 0; i < scene->mNumMaterials; ++i)
	{
		gModelManager.gMaterials.emplace_back(std::make_unique<Material>());
		auto& mat = gModelManager.gMaterials.back();

		auto aiMat = scene->mMaterials[i];

		//retrieve Material Name
		aiString s; std::string str;
		aiMat->Get(AI_MATKEY_NAME, s);
		mat->mName = s.C_Str();

		float f;
		aiMat->Get(AI_MATKEY_OPACITY, f);

		aiColor3D f3;
		aiMat->Get(AI_MATKEY_COLOR_DIFFUSE, f3);
		mat->mShadingColor.diffuseColor = XMFLOAT4(f3.r, f3.g, f3.b, f);

		aiMat->Get(AI_MATKEY_COLOR_AMBIENT, f3);
		mat->mShadingColor.ambientColor = XMFLOAT4(f3.r, f3.g, f3.b, 1.0f);

		f;
		aiMat->Get(AI_MATKEY_SHININESS, f);

		aiMat->Get(AI_MATKEY_COLOR_SPECULAR, f3);
		mat->mShadingColor.specularColor = XMFLOAT4(f3.r, f3.g, f3.b, f);

		auto diffuseTextureCount = aiMat->GetTextureCount(aiTextureType_DIFFUSE);

		for (UINT j = 0; j < diffuseTextureCount; ++j)
		{

			mat->SetShaderClass_Material(SURFACE_SHADER_CLASSINSTANCES_MATERIAL_TEXTURED);

			if (aiMat->GetTexture(aiTextureType_DIFFUSE, j, &s) == AI_SUCCESS)
			{
				if (s.C_Str()[0] == '*')
				{
					int index = std::atoi(&s.C_Str()[1]);
					//str = std::string(scene->mTextures[index]->mFilename.C_Str());
					boost::filesystem::path texfilepath(scene->mTextures[index]->mFilename.C_Str());

					std::string texKey = texfilepath.filename().string();

					mat->SetDiffueMap(gModelManager.gTextures[texKey].get(), texKey);
				}
				else {
					//str = s.C_Str();
					boost::filesystem::path texFilePath(s.C_Str());
					std::string texKey = texFilePath.filename().string();
					if (gModelManager.gTextures.count(texKey) == 0)
					{
						if (texFilePath.is_relative())
						{
							std::string fullpath = mFilePath.make_preferred().parent_path().string() + "\\" + texFilePath.make_preferred().string();
							std::wstring wfullpath = StringConverter::StringToWide(fullpath);

							auto str_extension = texFilePath.extension().string();
							if (str_extension == "dds")
#ifdef USE_DIRECTXTEX
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wfullpath, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#else
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wfullpath, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#endif
							else
#ifdef USE_DIRECTXTEX
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wfullpath, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#else
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wfullpath, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#endif
						}
						else
						{
							std::wstring wTexFilePath = StringConverter::StringToWide(texFilePath.string());
							auto str_extension = texFilePath.extension().string();
							if (str_extension == "dds")
#ifdef USE_DIRECTXTEX
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wTexFilePath, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#else
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wTexFilePath, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#endif
							else
#ifdef USE_DIRECTXTEX
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wTexFilePath, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#else
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wTexFilePath, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#endif
						}
					}

					mat->SetDiffueMap(gModelManager.gTextures[texKey].get(), texKey);

				}
			}

		}

		auto normalMapCount = aiMat->GetTextureCount(aiTextureType_NORMALS);

		for (int j = 0; j < normalMapCount; ++j)
		{
			mat->SetShaderClass_Normal(SURFACE_SHADER_CLASSINSTANCES_NORMAL_NORMALMAP);

			if (aiMat->GetTexture(aiTextureType_NORMALS, j, &s) == AI_SUCCESS)
			{
				if (s.C_Str()[0] == '*')
				{
					int index = std::atoi(&s.C_Str()[1]);
					//str = scene->mTextures[index]->mFilename.C_Str();
					boost::filesystem::path texfilepath(scene->mTextures[index]->mFilename.C_Str());

					std::string texKey = texfilepath.filename().string();

					mat->SetNormalMap(gModelManager.gTextures[texKey].get(), texKey);
				}
				else {
					//str = s.C_Str();
					boost::filesystem::path texFilePath(s.C_Str());
					std::string texKey = texFilePath.filename().string();
					if (gModelManager.gTextures.count(texKey) == 0)
					{
						if (texFilePath.is_relative())
						{
							std::string fullpath = mFilePath.make_preferred().parent_path().string() + "\\" + texFilePath.make_preferred().string();
							std::wstring wfullpath = StringConverter::StringToWide(fullpath);

							auto str_extension = texFilePath.extension().string();
							if (str_extension == "dds")
#ifdef USE_DIRECTXTEX
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wfullpath, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#else
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wfullpath, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#endif
							else
#ifdef USE_DIRECTXTEX
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wfullpath, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#else
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wfullpath, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#endif

						}
						else
						{
							std::wstring wTexFilePath = StringConverter::StringToWide(texFilePath.string());
							auto str_extension = texFilePath.extension().string();
							if (str_extension == "dds")
#ifdef USE_DIRECTXTEX
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wTexFilePath, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#else
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wTexFilePath, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#endif
							else
#ifdef USE_DIRECTXTEX
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wTexFilePath, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#else
								gModelManager.gTextures[texKey] = std::make_unique<Texture>(wTexFilePath, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#endif
						}
					}

					mat->SetNormalMap(gModelManager.gTextures[texKey].get(), texKey);

				}
			}

		}


	}


	return true;
}


bool Model::ProcessTextures(const aiScene* scene)
{
	for (UINT i = 0; i < scene->mNumTextures; ++i)
	{
		auto tex = scene->mTextures[i];

		std::string str = tex->mFilename.C_Str();
		boost::filesystem::path texPath(str);

		if (texPath.extension().string() == "dds" || texPath.extension().string() == "DDS")
#ifdef USE_DIRECTXTEX
			gModelManager.gTextures[texPath.filename().string()] = std::make_unique<Texture>(reinterpret_cast<uint8_t*> (tex->pcData), tex->mWidth, tex->mHeight, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#else
			gModelManager.gTextures[texPath.filename().string()] = std::make_unique<Texture>(reinterpret_cast<uint8_t*> (tex->pcData), tex->mWidth, tex->mHeight, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_DDS);
#endif
		else
#ifdef USE_DIRECTXTEX
			gModelManager.gTextures[texPath.filename().string()] = std::make_unique<Texture>(reinterpret_cast<uint8_t*> (tex->pcData), tex->mWidth, tex->mHeight, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#else
			gModelManager.gTextures[texPath.filename().string()] = std::make_unique<Texture>(reinterpret_cast<uint8_t*> (tex->pcData), tex->mWidth, tex->mHeight, Texture_TYPE::TEXTURE_2D, TEXTURE_FORMAT::TEXTURE_FORMAT_WIC);
#endif

		/*mTextures.emplace_back();
		auto tex = mTextures.back();

		tex.InitTextureFromFile(StringConverter::StringToWide(scene->mTextures[i]->mFilename.C_Str()), TEXTURE_2D, TEXTURE_FORMAT_BITMAP);*/

	}

	return true;
}

void Model::AssignMaterialTextures()
{
	for (auto& mat : gModelManager.gMaterials)
	{
		if (!mat->mDiffuseTextureKey.empty())
		{
			mat->SetDiffueMap(gModelManager.gTextures[mat->mDiffuseTextureKey].get(), mat->mDiffuseTextureKey);
		}
		if (!mat->mNormalTextureKey.empty())
		{
			mat->SetNormalMap(gModelManager.gTextures[mat->mNormalTextureKey].get(), mat->mNormalTextureKey);
		}
		if (!mat->mDisplacementTextureKey.empty())
		{
			mat->SetNormalMap(gModelManager.gTextures[mat->mDisplacementTextureKey].get(), mat->mDisplacementTextureKey);
		}
	}
}

void Model::Update()
{

}



bool Model::Initialize(const std::string& filepath, ID3D11Device* device, ID3D11DeviceContext* context)
{

	LoadModel(filepath);

	return true;
}

void Model::Draw(ID3D11DeviceContext* context)
{
	if (mDisabled)
		return;

	for (auto mesh : mMeshes)
	{
		mesh->Draw(context);
	}
}













//Skinned Model

void SkinnedModel::ExtractBoneWeightsForVertecies(aiMesh* mesh, std::map<UINT, std::vector<BoneWeight>>* VertexToBone)
{
	for (int i = 0; i < mesh->mNumBones; ++i)
	{
		auto bone = mesh->mBones[i];

		std::string bname = bone->mName.C_Str();

		auto BoneIndex = mAnimation->GetBoneIndex(bname);

		for (int j = 0; j < bone->mNumWeights; ++j)
		{

			auto& weight = bone->mWeights[j];


			(*VertexToBone)[weight.mVertexId].push_back(BoneWeight(BoneIndex, weight.mWeight));
		}

	}
}


//int SkinnedModel::InitBuffers()
//{
//	D3D11_BUFFER_DESC desc = { 0 };
//	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
//	desc.ByteWidth = gVertecies_Skinned.size() * sizeof(Vertex::Skinned);
//	desc.CPUAccessFlags = 0;
//	desc.MiscFlags = 0;
//	desc.Usage = D3D11_USAGE_IMMUTABLE;
//
//	D3D11_SUBRESOURCE_DATA vertexInitData = { 0 };
//	vertexInitData.pSysMem = gVertecies_Skinned.data();
//	vertexInitData.SysMemPitch = 0;
//	vertexInitData.SysMemSlicePitch = 0;
//
//	gDevice->CreateBuffer(&desc, &vertexInitData, gVertexBuffer_Skinned.ReleaseAndGetAddressOf());
//
//	desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
//	desc.ByteWidth = gIndecies.size() * sizeof(UINT32);
//
//	D3D11_SUBRESOURCE_DATA indexInitData = { 0 };
//	indexInitData.pSysMem = gIndecies.data();
//
//	gDevice->CreateBuffer(&desc, &indexInitData, gIndexBuffer.ReleaseAndGetAddressOf());
//
//	return 0;
//}

SkinnedModel::SkinnedModel() : mAnimation(new SA_Animation())
{
}

SkinnedModel::~SkinnedModel() = default;


SkinnedModel::SkinnedModel(const SkinnedModel& sm) : mAnimation(new SA_Animation(*sm.mAnimation))
, mCurrentAnimName(sm.mCurrentAnimName)
, mTimePos(sm.mTimePos)
{

}

SkinnedModel::SkinnedModel(SkinnedModel&& sm) : mAnimation(std::move(sm.mAnimation))
, mCurrentAnimName(sm.mCurrentAnimName)
, mTimePos(sm.mTimePos)
{

}

SkinnedModel& SkinnedModel::operator=(SkinnedModel&&) = default;


bool SkinnedModel::LoadModel(const std::string& filepath)
{
	try {

		mFilePath = filepath.c_str();

		Assimp::Importer importer;

		//importer.SetPropertyBool("AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS", false);
		//importer.SetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS, false);
		//auto b = importer.GetPropertyBool(AI_CONFIG_IMPORT_FBX_PRESERVE_PIVOTS);

		const aiScene* pScene = importer.ReadFile(filepath, aiProcess_Triangulate | aiProcess_ConvertToLeftHanded);


		if (pScene == nullptr)
			return false;

		ProcessMaterials(pScene);

		if (pScene->HasAnimations())
		{

			mAnimation = std::make_unique<SA_Animation>(pScene);

		}

		ProcessNode(pScene->mRootNode, pScene);


		mMeshCount = mMeshes.size();


		return true;

	}
	catch (std::exception ex)
	{
		return false;
	}
}

IMesh* SkinnedModel::ProcessMesh(aiMesh* mesh, const aiScene* scene)
{


	std::map<UINT, std::vector<BoneWeight>> mVertexToBone;


	ExtractBoneWeightsForVertecies(mesh, &mVertexToBone);

	int indexCount = 0;

	auto vertexOffset = gVertecies_Skinned.size();
	auto indexOffset = gIndecies.size();

	/*std::vector<Vertex::Skinned>* vertecies = new std::vector<Vertex::Skinned>();

	std::vector<UINT32>* indecies = new std::vector<UINT32>();*/

	gVertecies_Skinned.reserve(mesh->mNumVertices);

	for (UINT i = 0; i < mesh->mNumVertices; ++i)
	{

		gVertecies_Skinned.emplace_back();
		Vertex::Skinned& vertex = gVertecies_Skinned.back();

		vertex.Pos.x = mesh->mVertices[i].x;
		vertex.Pos.y = mesh->mVertices[i].y;
		vertex.Pos.z = mesh->mVertices[i].z;

		if (mesh->HasTextureCoords(0))
		{
			vertex.Tex.x = mesh->mTextureCoords[0][i].x;
			vertex.Tex.y = mesh->mTextureCoords[0][i].y;
		}

		if (mesh->HasNormals())
		{
			vertex.Normal.x = mesh->mNormals[i].x;
			vertex.Normal.y = mesh->mNormals[i].y;
			vertex.Normal.z = mesh->mNormals[i].z;
		}

		int index = 0;

		float weight[3] = { 0.0f, 0.0f, 0.0f };

		for (auto& a : mVertexToBone[i])
		{
			if (index < 4)
			{
				vertex.BoneIndex[index] = (byte)a.mBoneIndex;

				if (index < 3)
					weight[index] = a.mBoneWeight;

				++index;
			}
			else
			{
				break;
			}
		}

		vertex.BoneWeights = XMFLOAT3(weight);

	}

	for (UINT i = 0; i < mesh->mNumFaces; ++i)
	{
		aiFace face = mesh->mFaces[i];

		for (UINT j = 0; j < face.mNumIndices; ++j)
		{
			gIndecies.push_back(face.mIndices[j]);
			indexCount++;
		}
	}


	std::string name = mesh->mName.C_Str();

	//auto& Materials = gModelManager.gMaterials;
	//auto modelsbasic = gModelManager.GetBasicModels();
	//auto modelsskinned = gModelManager.GetSkinnedModels();

	auto material = gModelManager.gMaterials[mesh->mMaterialIndex + mMaterialIndexOffset].get();

	XMFLOAT4X4 texTransform;
	XMStoreFloat4x4(&texTransform, XMMatrixIdentity());

	BoundingBox AABB;
	BoundingBox::CreateFromPoints(AABB, mesh->mNumVertices, &gVertecies_Skinned[vertexOffset].Pos, sizeof(Vertex::Skinned));

	IMesh* m = new Mesh_Skinned(name, this, mesh->mNumVertices, indexCount, indexOffset, vertexOffset, material, mesh->mMaterialIndex + mMaterialIndexOffset, texTransform, AABB, gDevice.Get(), gContext.Get());

	return m;

}

void SkinnedModel::Update()
{
	/*mTimePos += dt;

	if (mTimePos > mAnimation->GetDuration())
	{
		mTimePos = 0.0f;
	}*/

	mAnimation->UpdateTimePos(gDt);
}

std::vector<DirectX::XMFLOAT4X4>* SkinnedModel::GetBoneTransforms() const
{
	return mAnimation->GetTransform();
}

bool SkinnedModel::SetAnimation(const std::string& AnimName)
{
	return mAnimation->AddAnimToQueue(AnimName);
}

bool SkinnedModel::SetAnimation(int AnimIndex)
{
	return mAnimation->AddAnimToQueue(AnimIndex);
}

bool SkinnedModel::DisableAnimation(const std::string& AnimName)
{
	return mAnimation->RemoveAnimToQueue(AnimName);
}

bool SkinnedModel::DisableAnimation(int AnimIndex)
{
	return mAnimation->RemoveAnimToQueue(AnimIndex);

}
