#include "stdafx.h"
#include "Mesh.h"
#include "Model.h"

#include "Graphics\Vertex.h"
#include "Graphics\Material.h"
#include "..\Graphics\Mesh.h"

Mesh_Basic32::Mesh_Basic32()
{
	mName = "";
	mContext = nullptr;

	/*mVertecies = nullptr;
	mIndecies = nullptr;*/

	mVertexCount = 0;
	mIndexCount = 0;
	mIndexOffset = 0;
	mVertexOffset = 0;

	XMStoreFloat4x4(&mMatrix_TexTransform, XMMatrixIdentity());

	mVertexType = VERTEX_TYPE_BASIC32;

	/*mIndexBuffer = nullptr;
	mVertexBuffer = nullptr;*/

	mMaterial = nullptr;

	mMaterialIndex = 0;
}

Mesh_Basic32::Mesh_Basic32(std::string& name, Model* Parent, UINT32 vertexCount, UINT32 indexCount, UINT32 indexOffset, UINT32 vertexOffset, Material* pMaterial, UINT MaterialIndex, const XMFLOAT4X4& TexTransform, const BoundingBox& AABB, ID3D11Device* device /*= Globals::gDevice.Get()*/, ID3D11DeviceContext* context /*= Globals::gContext.Get()*/)
{
	mName = std::string(name.c_str());

	mContext = context;

	mParent = Parent;

	mAABB = AABB;

	/*mVertecies.reset(vertecies);
	mIndecies.reset(indecies);*/

	/*mVertexCount = vertecies->size();
	mIndexCount = indecies->size();*/
	mIndexOffset = indexOffset;
	mVertexOffset = vertexOffset;
	mVertexCount = vertexCount;
	mIndexCount = indexCount;

	mMatrix_TexTransform = TexTransform;

	mVertexType = VERTEX_TYPE_BASIC32;

	//InitBuffers(device);

	mMaterial = pMaterial;

	mMaterialIndex = MaterialIndex;
}

Mesh_Basic32::Mesh_Basic32(const Mesh_Basic32& mesh)
{
	this->mName = mesh.mName;
	this->mVertexType = mesh.mVertexType;
	this->mParent = mesh.mParent;
	this->mAABB = mesh.mAABB;
	/*this->mVertecies = mesh.mVertecies;
	this->mIndecies = mesh.mIndecies;*/
	this->mIndexOffset = mesh.mIndexOffset;
	this->mIndexCount = mesh.mIndexCount;
	this->mVertexCount = mesh.mVertexCount;
	this->mVertexOffset = mesh.mVertexOffset;
	this->mContext = mesh.mContext;
	/*this->mIndexBuffer = mesh.mIndexBuffer;
	this->mVertexBuffer = mesh.mVertexBuffer;*/
}

void Mesh_Basic32::Draw(ID3D11DeviceContext* context)const
{
	context->DrawIndexed(mIndexCount, mIndexOffset, mVertexOffset);
}

void Mesh_Basic32::InitBuffers(ID3D11Device* device)
{
	/*D3D11_BUFFER_DESC desc = { 0 };
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.ByteWidth = mVertexCount * sizeof(Vertex::Basic32);
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;
	desc.Usage = D3D11_USAGE_IMMUTABLE;

	D3D11_SUBRESOURCE_DATA vertexInitData = { 0 };
	vertexInitData.pSysMem = mVertecies->data();
	vertexInitData.SysMemPitch = 0;
	vertexInitData.SysMemSlicePitch = 0;

	device->CreateBuffer(&desc, &vertexInitData, mVertexBuffer.ReleaseAndGetAddressOf());

	desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	desc.ByteWidth = mIndexCount * sizeof(UINT32);

	D3D11_SUBRESOURCE_DATA indexInitData = { 0 };
	indexInitData.pSysMem = mIndecies->data();

	device->CreateBuffer(&desc, &indexInitData, mIndexBuffer.ReleaseAndGetAddressOf());*/
}


Mesh_Skinned::Mesh_Skinned() 
{

	XMStoreFloat4x4(&mMatrix_TexTransform, XMMatrixIdentity());

	mVertexType = VERTEX_TYPE_SKINNED;

}

Mesh_Skinned::Mesh_Skinned(std::string& name, Model* Parent, UINT32 vertexCount, UINT32 indexCount, UINT32 indexOffset, UINT32 vertexOffset, Material* pMaterial, UINT MaterialIndex, const XMFLOAT4X4& TexTransform, const BoundingBox& AABB, ID3D11Device* device /*= Globals::gDevice.Get()*/, ID3D11DeviceContext* context /*= Globals::gContext.Get()*/)
{
	mName = std::string(name.c_str());

	mContext = context;

	mParent = Parent;

	mAABB = AABB;

	/*mVertecies.reset(vertecies);
	mIndecies.reset(indecies);*/

	mVertexCount = vertexCount;
	mIndexCount = indexCount;
	mIndexOffset = indexOffset;
	mVertexOffset = vertexOffset;

	mMatrix_TexTransform = TexTransform;

	mVertexType = VERTEX_TYPE_SKINNED;

	//InitBuffers(device);

	mMaterial = pMaterial;

	mMaterialIndex = MaterialIndex;
}

Mesh_Skinned::Mesh_Skinned(const Mesh_Skinned& mesh)
{
	this->mName = mesh.mName;
	this->mVertexType = mesh.mVertexType;
	this->mParent = mesh.mParent;
	this->mAABB = mesh.mAABB;
	/*this->mVertecies = mesh.mVertecies;
	this->mIndecies = mesh.mIndecies;*/
	this->mIndexOffset = mesh.mIndexOffset;
	this->mIndexCount = mesh.mIndexCount;
	this->mVertexCount = mesh.mVertexCount;
	this->mVertexOffset = mesh.mVertexOffset;
	this->mContext = mesh.mContext;
	/*this->mIndexBuffer = mesh.mIndexBuffer;
	this->mVertexBuffer = mesh.mVertexBuffer;*/
}

void Mesh_Skinned::Draw(ID3D11DeviceContext* context) const
{
	context->DrawIndexed(mIndexCount, mIndexOffset, mVertexOffset);
}

void Mesh_Skinned::InitBuffers(ID3D11Device* device)
{
	/*D3D11_BUFFER_DESC desc = { 0 };
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.ByteWidth = mVertexCount * sizeof(Vertex::Skinned);
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;
	desc.Usage = D3D11_USAGE_IMMUTABLE;

	D3D11_SUBRESOURCE_DATA vertexInitData = { 0 };
	vertexInitData.pSysMem = mVertecies->data();
	vertexInitData.SysMemPitch = 0;
	vertexInitData.SysMemSlicePitch = 0;

	device->CreateBuffer(&desc, &vertexInitData, mVertexBuffer.ReleaseAndGetAddressOf());

	desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	desc.ByteWidth = mIndexCount * sizeof(UINT32);

	D3D11_SUBRESOURCE_DATA indexInitData = { 0 };
	indexInitData.pSysMem = mIndecies->data();

	device->CreateBuffer(&desc, &indexInitData, mIndexBuffer.ReleaseAndGetAddressOf());*/
}

void IMesh::Draw(ID3D11DeviceContext* context) const
{
}

Material* IMesh::GetMaterial() const
{
	return mMaterial;
}

const BoundingBox& IMesh::GetBoundingBox() const
{
	return mAABB;
}

void IMesh::SetMaterial(Material* material)
{
	mMaterial = material;

	material->AddMesh(this);
}

UINT IMesh::GetMatrialIndex() const
{
	return mMaterialIndex;
}
