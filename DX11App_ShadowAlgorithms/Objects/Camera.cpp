//***************************************************************************************
// Camera.h by Frank Luna (C) 2011 All Rights Reserved.
//***************************************************************************************
#include "stdafx.h"
#include "Camera.h"
#include "MathHelper.h"
#include "Input/KeyboardClass.h"
#include "Input/MouseClass.h"
#include "D3DUtil.h"


void Camera::UpdateViewProj()
{
	auto vp = XMMatrixMultiply(View(), Proj());
	XMStoreFloat4x4(&mViewProj, vp);
}

Camera::Camera()
	: mPosition(0.0f, 0.0f, 0.0f),
	mRight(1.0f, 0.0f, 0.0f),
	mUp(0.0f, 1.0f, 0.0f),
	mLook(0.0f, 0.0f, 1.0f),
	mSpeed(350.0),
	mUpdated(false)
{
	SetLens(0.25f * XM_PI, 1.0f, 1.0f, 1000.0f);
}


XMVECTOR Camera::GetPositionXM()const
{
	return XMLoadFloat3(&mPosition);
}

XMFLOAT3 Camera::GetPosition()const
{
	return mPosition;
}

void Camera::SetPosition(float x, float y, float z)
{
	mPosition = XMFLOAT3(x, y, z);
	mUpdated = true;
}

void Camera::SetPosition(const XMFLOAT3& v)
{
	mPosition = v;
	mUpdated = true;
}

XMVECTOR Camera::GetRightXM()const
{
	return XMLoadFloat3(&mRight);
}

XMFLOAT3 Camera::GetRight()const
{
	return mRight;
}

XMVECTOR Camera::GetUpXM()const
{
	return XMLoadFloat3(&mUp);
}

XMFLOAT3 Camera::GetUp()const
{
	return mUp;
}

XMVECTOR Camera::GetLookXM()const
{
	return XMLoadFloat3(&mLook);
}

XMFLOAT3 Camera::GetLook()const
{
	return mLook;
}

float Camera::GetNearZ()const
{
	return mNearZ;
}

float Camera::GetFarZ()const
{
	return mFarZ;
}

float Camera::GetAspect()const
{
	return mAspect;
}

float Camera::GetFovY()const
{
	return mFovY;
}

float Camera::GetFovX()const
{
	float halfWidth = 0.5f * GetNearWindowWidth();
	return 2.0f * atan(halfWidth / mNearZ);
}

float Camera::GetNearWindowWidth()const
{
	return mAspect * mNearWindowHeight;
}

float Camera::GetNearWindowHeight()const
{
	return mNearWindowHeight;
}

float Camera::GetFarWindowWidth()const
{
	return mAspect * mFarWindowHeight;
}

float Camera::GetFarWindowHeight()const
{
	return mFarWindowHeight;
}

void Camera::SetLens(float fovY, float aspect, float zn, float zf)
{
	// cache properties
	mFovY = fovY;
	mAspect = aspect;
	mNearZ = zn;
	mFarZ = zf;

	mNearWindowHeight = 2.0f * mNearZ * tanf(0.5f * mFovY);
	mFarWindowHeight = 2.0f * mFarZ * tanf(0.5f * mFovY);

	const auto nearWindowWidth = mAspect * mNearWindowHeight;
	mFovX = 2.0 * atanf(nearWindowWidth / 2.0f / zn);

	XMMATRIX P = XMMatrixPerspectiveFovLH(mFovY, mAspect, mNearZ, mFarZ);
	XMStoreFloat4x4(&mProj, P);

	mUpdated = true;

}

void Camera::SetLens()
{
	mNearWindowHeight = 2.0f * mNearZ * tanf(0.5f * mFovY);
	mFarWindowHeight = 2.0f * mFarZ * tanf(0.5f * mFovY);


	XMMATRIX P = XMMatrixPerspectiveFovLH(mFovY, mAspect, mNearZ, mFarZ);
	XMStoreFloat4x4(&mProj, P);

	mUpdated = true;


}

void XM_CALLCONV Camera::LookAt(FXMVECTOR pos, FXMVECTOR target, FXMVECTOR worldUp)
{
	XMVECTOR L = XMVector3Normalize(XMVectorSubtract(target, pos));
	XMVECTOR R = XMVector3Normalize(XMVector3Cross(worldUp, L));
	XMVECTOR U = XMVector3Cross(L, R);

	XMStoreFloat3(&mPosition, pos);
	XMStoreFloat3(&mLook, L);
	XMStoreFloat3(&mRight, R);
	XMStoreFloat3(&mUp, U);

	mUpdated = true;
}

void Camera::LookAt(const XMFLOAT3& pos, const XMFLOAT3& target, const XMFLOAT3& up)
{
	XMVECTOR P = XMLoadFloat3(&pos);
	XMVECTOR T = XMLoadFloat3(&target);
	XMVECTOR U = XMLoadFloat3(&up);

	LookAt(P, T, U);

	mUpdated = true;
}

XMMATRIX Camera::View()const
{
	return XMLoadFloat4x4(&mView);
}

XMMATRIX Camera::ViewInv()const
{
	return XMMatrixInverse(nullptr, View());
}

XMMATRIX Camera::ViewProjInv() const
{
	//return XMMatrixInverse(nullptr, Proj()) * XMMatrixInverse(nullptr, View());
	return XMMatrixInverse(nullptr, XMMatrixMultiply(View(), Proj()));
}

XMMATRIX Camera::Proj()const
{
	return XMLoadFloat4x4(&mProj);
}

XMMATRIX Camera::ProjInv() const
{
	return XMMatrixInverse(nullptr, Proj());
}

XMMATRIX Camera::ViewProj()const
{
	return XMLoadFloat4x4(&mViewProj);
}

XMVECTOR Camera::GetPerspectiveValues()const
{
	return 	XMVectorSet((1 / mProj._11), (1 / mProj._22), mProj._33, mProj._43);
}

void Camera::Strafe(float d)
{
	d *= mSpeed;
	// mPosition += d*mRight
	XMVECTOR s = XMVectorReplicate(d);
	XMVECTOR r = XMLoadFloat3(&mRight);
	XMVECTOR p = XMLoadFloat3(&mPosition);
	XMStoreFloat3(&mPosition, XMVectorMultiplyAdd(s, r, p));

	mUpdated = true;
}

void Camera::Walk(float d)
{
	d *= mSpeed;
	// mPosition += d*mLook
	XMVECTOR s = XMVectorReplicate(d);
	XMVECTOR l = XMLoadFloat3(&mLook);
	XMVECTOR p = XMLoadFloat3(&mPosition);
	XMStoreFloat3(&mPosition, XMVectorMultiplyAdd(s, l, p));

	mUpdated = true;
}

void Camera::Pitch(float angle)
{
	// Rotate up and look vector about the right vector.

	XMMATRIX R = XMMatrixRotationAxis(XMLoadFloat3(&mRight), angle);

	XMStoreFloat3(&mUp, XMVector3TransformNormal(XMLoadFloat3(&mUp), R));
	XMStoreFloat3(&mLook, XMVector3TransformNormal(XMLoadFloat3(&mLook), R));

	mUpdated = true;
}

void Camera::RotateY(float angle)
{
	// Rotate the basis vectors about the world y-axis.

	XMMATRIX R = XMMatrixRotationY(angle);

	XMStoreFloat3(&mRight, XMVector3TransformNormal(XMLoadFloat3(&mRight), R));
	XMStoreFloat3(&mUp, XMVector3TransformNormal(XMLoadFloat3(&mUp), R));
	XMStoreFloat3(&mLook, XMVector3TransformNormal(XMLoadFloat3(&mLook), R));

	mUpdated = true;
}

void Camera::RotateCamera(float d)
{
	auto angle = XMConvertToRadians(20 * d);
	auto R = XMMatrixRotationY(angle);

	XMStoreFloat3(&mPosition, XMVector3Transform(XMLoadFloat3(&mPosition), R));

	RotateY(angle);

	//mUpdated = true;
}

void Camera::UpdateViewMatrix()
{
	if (mUpdated)
	{
		XMVECTOR R = XMLoadFloat3(&mRight);
		XMVECTOR U = XMLoadFloat3(&mUp);
		XMVECTOR L = XMLoadFloat3(&mLook);
		XMVECTOR P = XMLoadFloat3(&mPosition);

		// Keep camera's axes orthogonal to each other and of unit length.
		L = XMVector3Normalize(L);
		U = XMVector3Normalize(XMVector3Cross(L, R));

		// U, L already ortho-normal, so no need to normalize cross product.
		R = XMVector3Cross(U, L);

		// Fill in the view matrix entries.
		float x = -XMVectorGetX(XMVector3Dot(P, R));
		float y = -XMVectorGetX(XMVector3Dot(P, U));
		float z = -XMVectorGetX(XMVector3Dot(P, L));

		XMStoreFloat3(&mRight, R);
		XMStoreFloat3(&mUp, U);
		XMStoreFloat3(&mLook, L);

		mView(0, 0) = mRight.x;
		mView(1, 0) = mRight.y;
		mView(2, 0) = mRight.z;
		mView(3, 0) = x;

		mView(0, 1) = mUp.x;
		mView(1, 1) = mUp.y;
		mView(2, 1) = mUp.z;
		mView(3, 1) = y;

		mView(0, 2) = mLook.x;
		mView(1, 2) = mLook.y;
		mView(2, 2) = mLook.z;
		mView(3, 2) = z;

		mView(0, 3) = 0.0f;
		mView(1, 3) = 0.0f;
		mView(2, 3) = 0.0f;
		mView(3, 3) = 1.0f;

		CreateBoundingFrustum();

		UpdateViewProj();

		mUpdated = false;
	}
}

void Camera::CreateBoundingFrustum()
{
	BoundingFrustum::CreateFromMatrix(mCamFrustum, Proj());
	const auto InvView = XMMatrixInverse(nullptr, View());
	mCamFrustum.Transform(mCamFrustum, InvView);
}

BoundingFrustum Camera::GetBoundingFrustum()const
{
	return mCamFrustum;
}

ContainmentType Camera::BoundingBoxContainment(const BoundingBox& _boundingBox) const
{
	return mCamFrustum.Contains(_boundingBox);
}

//void Camera::PrintProperties()
//{
//	system("cls");
//	std::cout << "\n Current Camera States:\n";
//	std::cout << CAMERA_POSITION << ". Position:\t" << Convertors::XMFLOAT3ToString(mPosition) << "\n";
//	std::cout << CAMERA_ZNEAR << ". zNear: " << GetNearZ() << "\n";
//	std::cout << CAMERA_ZFAR << ". zFar: " << GetFarZ() << "\n";
//	std::cout << CAMERA_SPEED << ". Speed: " << mSpeed << "\n";
//	GetPropValueFromConsole();
//}
//
//void Camera::GetPropValueFromConsole()
//{
//	std::cout << "\n Enter Number of Prop to Update Its Value - Press X to Cancel \n ";
//	std::string input;
//	std::cin >> input;
//	if (input == "x" || input == "X")
//	{
//		return;
//	}
//	std::cout << "\n Enter New Value:\t";
//	std::string value;
//	std::cin >> value;
//	int propIndex = std::stoi(input);
//	switch (propIndex)
//	{
//	case CAMERA_ZNEAR:
//		mNearZ = std::stof(value);
//		SetLens(mFovY, mAspect, mNearZ, mFarZ);
//		break;
//	case CAMERA_ZFAR:
//		mFarZ = std::stof(value);
//		SetLens(mFovY, mAspect, mNearZ, mFarZ);
//		break;
//	case CAMERA_POSITION:
//		mPosition = Convertors::StringToXMFLOAT3(value);
//		break;
//	case CAMERA_SPEED:
//		mSpeed = std::stof(value);
//		break;
//	default:
//		break;
//	}
//}

void Camera::GetUserInputs(const MouseEvent* mEvent, const MouseClass* Mouse, float dx, float dy)
{

	switch (mEvent->GetType())
	{
	case MouseEvent::EventType::Move:
	{
		if (Mouse->IsRightDown())
		{
			float dx_angle = XMConvertToRadians(0.25f * dx);
			float dy_angle = XMConvertToRadians(0.25f * dy);

			Pitch(dy_angle / 10);
			RotateY(dx_angle / 10);
			break;
		}
	}
	default:
		break;
	}

}

void Camera::GetUserInputs(const KeyboardEvent* kEvent, const KeyboardClass* Keyboard, float dt)
{

	switch (kEvent->GetkeyCode())
	{
	case 'W':
		Walk(dt);
		return;
	case 'S':
		Walk(-dt);
		return;
	case 'A':
		Strafe(-dt);
		break;
	case'D':
		Strafe(dt);
		break;
	case 'Q':
		RotateCamera(dt);
		break;
	default:
		break;
	}
}

void Camera::RenderConfigurationUI()
{
	ImGui::Begin("Camera");

	if (ImGui::DragFloat3("World Position", &mPosition.x))
	{
		mUpdated = true;
	}

	if (ImGui::DragFloat("Z Near", &mNearZ))
	{
		SetLens();
	}

	if (ImGui::DragFloat("Z Near", &mFarZ))
	{
		SetLens();
	}

	if (ImGui::DragFloat("FOV Y", &mFovY, 0.1))
	{
		SetLens();
	}

	ImGui::DragFloat("Movement Speed", &mSpeed);

	ImGui::End();
}

std::vector<std::vector<XMFLOAT4>> Camera::SplitFrustum() const
{
	const std::vector<float> viewDistance{ mNearZ, mNearZ + 50, mNearZ + 150, mNearZ + 500, mFarZ };

	std::vector<XMFLOAT4> projSplitPlanes(4, XMFLOAT4(0.0, 0.0, 0.0, 1.0f));
	std::vector<std::vector<XMFLOAT4>> worldSplitPlanes(viewDistance.size(), std::vector<XMFLOAT4>(4, XMFLOAT4(0.0, 0.0, 0.0, 1.0f)));

	projSplitPlanes[0].x = -1.0f;
	projSplitPlanes[0].y = 1.0f;

	projSplitPlanes[1].x = 1;
	projSplitPlanes[1].y = 1;

	projSplitPlanes[2].x = 1;
	projSplitPlanes[2].y = -1;

	projSplitPlanes[3].x = -1;
	projSplitPlanes[3].y = -1;

	int i = 0;

	for (const auto d : viewDistance)
	{
		worldSplitPlanes[i].resize(4, XMFLOAT4(0.0, 0.0, 0.0, 1.0f));


		const auto p = XMFLOAT4(0, 0, viewDistance[i], 1);
		const auto v = XMVector4Transform(XMLoadFloat4(&p), Proj());
		XMFLOAT4 v1;
		XMStoreFloat4(&v1, v);
		v1.z /= v1.w;



		for (int j = 0; j < 4; ++j)
		{

			projSplitPlanes[j].z = v1.z;

			//projSplitPlanes[j].z *= viewDistance[i];


			XMVECTOR worldpoint = XMLoadFloat4(&projSplitPlanes[j]);
			worldpoint = XMVector4Transform(XMLoadFloat4(&projSplitPlanes[j]), ViewProjInv());
			worldpoint = XMVectorScale(worldpoint, 1 / XMVectorGetW(worldpoint));

			XMStoreFloat4(&worldSplitPlanes[i][j], worldpoint);

		}

		i++;
	}

	return worldSplitPlanes;
}