#include "stdafx.h"
#include "Lights.h"
#include "MathHelper.h"
#include "StringConverter.h"
#include "Globals.h"
#include "D3D.h"

#include "Graphics/ConstantBuffer.h"
#include "Graphics/TexViews.h"
#include "Graphics/ShaderSlots_DeferredShading.h"
#include "Graphics/RenderStates.h"

#include "Objects/Mesh.h"

#include "Graphics/ShaderList/VS_BuildShadowMap.h"

using namespace Globals;

const XMFLOAT4X4 ILightObj::mProjToUV = XMFLOAT4X4(0.5f, 0.0f, 0.0f, 0.0f,
	0.0f, -0.5, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.5f, 0.5f, 0.0f, 1.0f);


std::unique_ptr<ConstantBuffer<ILightObj::CB_PER_OBJECT>> ILightObj::mCBPerObject;

std::unique_ptr<ConstantBuffer<ILightObj::CB_PER_LIGHT>> ILightObj::mCBperLight;

void ILightObj::InitGpuResources()
{
	if (!bCascadedShadowMap)
	{

		mViewport.Height = gShadowMapResolution;
		mViewport.Width = gShadowMapResolution;
		mViewport.MaxDepth = 1.0f;
		mViewport.MinDepth = 0.0f;
		mViewport.TopLeftX = 0.0f;
		mViewport.TopLeftY = 0.0f;


		mTexView_ShadowMap->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_DEPTH_STENCIL
			, gShadowMapResolution, gShadowMapResolution
			, DXGI_FORMAT_D32_FLOAT
			, D3D11_USAGE_DEFAULT
			, static_cast<D3D11_CPU_ACCESS_FLAG>(0)
			, 1, 1, 1, 0, 0, true);


		gDevice->CreateDeferredContext(0, mDeferredContext.ReleaseAndGetAddressOf());
		D3D::SetGlobalContextStates(mDeferredContext.Get(), false, false, false);

		//Set Deferred Context States
		mDeferredContext->RSSetViewports(1, &mViewport);
		mDeferredContext->PSSetShader(nullptr, nullptr, 0);
		mDeferredContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);



		RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, mDeferredContext.Get());
		RenderStates::SetDepthStencilState(RENDER_STATE_DS_DEFAULT, mDeferredContext.Get());
		RenderStates::SetRasterizerState(RENDER_STATE_RS_SHADOWMAP, mDeferredContext.Get());



		mCBPerObject->SetConstantBuffer(mDeferredContext.Get());
		mCBperLight->SetConstantBuffer(mDeferredContext.Get());

	}
	else
	{
		mViewport.Height = 1024.0f;
		mViewport.Width = 1024.0f;
		mViewport.MaxDepth = 1.0f;
		mViewport.MinDepth = 0.0f;
		mViewport.TopLeftX = 0.0f;
		mViewport.TopLeftY = 0.0f;

		mTexView_ShadowMap->Init(D3D11_BIND_SHADER_RESOURCE | D3D11_BIND_DEPTH_STENCIL
			, 2048, 2048
			, DXGI_FORMAT_D32_FLOAT
			, D3D11_USAGE_DEFAULT
			, static_cast<D3D11_CPU_ACCESS_FLAG>(0)
			, 1, 1, 1, 0, 0, true);



		for (auto& dc : mDeferredContext_C)
		{
			gDevice->CreateDeferredContext(0, dc.ReleaseAndGetAddressOf());
			D3D::SetGlobalContextStates(dc.Get(), false, false, false);



			//Set Deferred Context States
			dc->RSSetViewports(1, &mViewport);
			dc->PSSetShader(nullptr, nullptr, 0);
			dc->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);



			RenderStates::SetBlendState(RENDER_STATE_BS_DEFAULT, dc.Get());
			RenderStates::SetDepthStencilState(RENDER_STATE_DS_DEFAULT, dc.Get());
			RenderStates::SetRasterizerState(RENDER_STATE_RS_SHADOWMAP, dc.Get());



			mCBPerObject->SetConstantBuffer(dc.Get());
			mCBperLight->SetConstantBuffer(dc.Get());
		}


	}



}

void ILightObj::ReleaseGpuResources()
{
	mDeferredContext->Release();
	mCommandList->Release();
	mTexView_ShadowMap->Release();
}


void ILightObj::InitStaticConstantBuffers()
{
	mCBPerObject.reset(new ConstantBuffer<CB_PER_OBJECT>());
	mCBperLight.reset(new ConstantBuffer<CB_PER_LIGHT>());

	mCBPerObject->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PEROBJECT, CONSTANT_BUFFER_VS, nullptr, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);
	mCBperLight->InitConstantBuffer(SHADER_SLOTS_VS_CB_GENERATESHADOWMAP_PERLIGHT, CONSTANT_BUFFER_VS, nullptr, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);

}

void ILightObj::SetDirection(XMFLOAT3 _direction)
{
	ILightData->Direction = _direction;
	mUpdated_View = true;
	mUpdated = true;
}


void ILightObj::SetDirection(float x, float y, float z)
{
	ILightData->Direction.x = x;
	ILightData->Direction.y = y;
	ILightData->Direction.z = z;
	mUpdated_View = true;
	mUpdated = true;
}

void ILightObj::SetLightTarget(XMFLOAT3 _targetPosition)
{
	mLightTarget = XMFLOAT4(_targetPosition.x, _targetPosition.y, _targetPosition.z, 1.0);
	mUpdated_View = true;
	mUpdated = true;
}


void ILightObj::SetPosition(XMFLOAT3 _position)
{
	ILightData->Position = _position;
	mUpdated_View = true;
	mUpdated = true;
}


void ILightObj::SetPosition(float x, float y, float z)
{
	ILightData->Position.x = x;
	ILightData->Position.y = y;
	ILightData->Position.z = z;
	mUpdated_View = true;
	mUpdated = true;
}

void ILightObj::SetUpVector(XMFLOAT4 _up)
{
	mUpVector = _up;
	mUpdated_View = true;
	mUpdated = true;
}


void ILightObj::UpdateAmbientColor(XMFLOAT4 _ambientColor)
{
	ILightData->Ambient = _ambientColor;
}


ILightObj::ILightObj() : mLightTarget(XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f))
, mUpVector(XMFLOAT4(0.0f, 1.0f, 0.0f, 0.0f))
, Name("")
, bDisabled(false)
, bCascadedShadowMap(false)
, mUpdated(true)
, mUpdated_View(true)
, mUpdated_Proj(true)
, mTexView_ShadowMap(new TexViews())
{

}

ILightObj::~ILightObj() = default;

ILightObj::ILightObj(ILightObj&&) = default;

ILightObj& ILightObj::operator=(ILightObj&&) = default;


ILight& ILightObj::GetILightData() const
{
	return *ILightData;
}


void ILightObj::UpdateDiffuseColor(XMFLOAT4 _diffuseColor)
{
	ILightData->Diffuse = _diffuseColor;
}


void ILightObj::UpdateShadowTransformMatrix()
{
	auto viewProj = XMLoadFloat4x4(&ILightData->ViewProj);
	auto projToUV = XMLoadFloat4x4(&mProjToUV);

	auto shadowTransform = XMMatrixMultiply(viewProj, projToUV);
	XMStoreFloat4x4(&ILightData->ShadowTransform, shadowTransform);
}


void ILightObj::UpdateSpecularColor(XMFLOAT4 _specularColor)
{
	ILightData->Specular = _specularColor;
}


void ILightObj::UpdateViewMatrix()
{
	XMVECTOR EyePosition = XMLoadFloat3(&ILightData->Position);
	XMVECTOR EyeDirection = XMLoadFloat3(&ILightData->Direction);

	auto UpVector = XMLoadFloat4(&mUpVector);

	auto viewMatrix = XMMatrixLookToLH(EyePosition, EyeDirection, UpVector);
	XMStoreFloat4x4(&ILightData->View, viewMatrix);
}


void ILightObj::UpdateViewProjMatrix()
{
	auto viewMatrix = XMLoadFloat4x4(&ILightData->View);
	auto projMatrix = XMLoadFloat4x4(&mProjMatrix);

	auto viewProjMatrix = viewMatrix * projMatrix;
	XMStoreFloat4x4(&ILightData->ViewProj, viewProjMatrix);

	CreateBoundingFrustum();
	UpdateShadowTransformMatrix();
}


void ILightObj::UpdateViewMatrix(XMFLOAT4X4 _view)
{
	ILightData->View = _view;
}


void ILightObj::UpdateViewMatrix(XMFLOAT4 _position, XMFLOAT4 _direction, XMFLOAT4 _up)
{
	auto position = XMLoadFloat4(&_position);
	auto direction = XMLoadFloat4(&_direction);
	auto up = XMLoadFloat4(&_up);

	auto viewMatrix = XMMatrixLookToLH(position, direction, up);
	XMStoreFloat4x4(&ILightData->View, viewMatrix);
}


void ILightObj::SetCastShadow(bool _castshadow)
{
	ILightData->CastShadow = _castshadow ? 1 : 0;

	if (_castshadow)
	{
		InitGpuResources();
	}
	else {
		ReleaseGpuResources();
	}
}


void ILightObj::CreateBoundingFrustum()
{
	const auto projMatrix = XMLoadFloat4x4(&mProjMatrix);

	BoundingFrustum::CreateFromMatrix(mBoundingFrustum, projMatrix);

	const auto viewMatrix = XMLoadFloat4x4(&ILightData->View);

	const auto InvView = XMMatrixInverse(nullptr, viewMatrix);
	mBoundingFrustum.Transform(mBoundingFrustum, InvView);
}

void ILightObj::UpdateViewProjMatrix(XMFLOAT4X4 _view, XMFLOAT4X4 _proj)
{
	ILightData->View = _view;
	mProjMatrix = _proj;

	auto viewMatrix = XMLoadFloat4x4(&_view);
	auto projMatrix = XMLoadFloat4x4(&_proj);

	auto viewProjMatrix = viewMatrix * projMatrix;

	XMStoreFloat4x4(&ILightData->ViewProj, viewProjMatrix);

	//Update perspective Values

	ILightData->PerpectiveValues = XMFLOAT4(mProjMatrix._11, mProjMatrix._22, mProjMatrix._33, mProjMatrix._43);

	auto projToUV = XMLoadFloat4x4(&mProjToUV);

	auto shadowTransform = viewProjMatrix * projToUV;

	XMStoreFloat4x4(&ILightData->ShadowTransform, shadowTransform);

	CreateBoundingFrustum();
	UpdateShadowTransformMatrix();
}


ContainmentType ILightObj::BoundingBoxContainment(BoundingBox _boundingBox)
{
	return mBoundingFrustum.Contains(_boundingBox);
}


void ILightObj::UpdateDirInEyeScreen(const FXMVECTOR EyePosition, const XMFLOAT3 EyeViewDir)
{
	XMMATRIX view_xz = XMMatrixLookToLH(EyePosition
		, XMVector4Normalize(XMVectorSet(EyeViewDir.x, 0.0f, EyeViewDir.z, 0.0f))
		, XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f));

	auto lightDirW_XZ = ILightData->Direction;
	lightDirW_XZ.y = 0.0f;

	auto lightDir = XMVector4Normalize(XMVector4Transform(XMLoadFloat3(&lightDirW_XZ), view_xz));

	XMFLOAT3 dir_temp;

	XMStoreFloat3(&dir_temp, lightDir);

	ILightData->DirInEyeScreen_Vert.x = dir_temp.x;
	ILightData->DirInEyeScreen_Vert.y = dir_temp.z * -1.0f;

	static XMMATRIX MatrixRotationY_90 = XMMatrixRotationY(XMConvertToRadians(90.0f));

	lightDir = XMVector4Transform(lightDir, MatrixRotationY_90);

	XMStoreFloat3(&dir_temp, lightDir);

	ILightData->DirInEyeScreen_Horz.x = dir_temp.x;
	ILightData->DirInEyeScreen_Horz.y = dir_temp.z * -1.0f;
}


int ILightObj::GetIndex()const
{
	return mIndex;
}


LIGHT_TYPE ILightObj::GetLightType()const
{
	return mLightType;
}


void ILightObj::GenerateShadowMap()
{
	if (ILightData->CastShadow)
	{
		if (bCascadedShadowMap)
		{
			auto world_split_planes = gCamera_main.SplitFrustum();


			XMFLOAT4 min_max_point_splits[4][2];
			for (size_t i = 0; i < 4; i++)
			{
				XMVECTOR min = XMVectorSet(D3D11_FLOAT32_MAX, D3D11_FLOAT32_MAX, D3D11_FLOAT32_MAX, D3D11_FLOAT32_MAX);
				XMVECTOR max = XMVectorSet(-D3D11_FLOAT32_MAX, -D3D11_FLOAT32_MAX, -D3D11_FLOAT32_MAX, -D3D11_FLOAT32_MAX);
				for (size_t p = 0; p < 2; p++)
				{
					for (size_t v = 0; v < 4; v++)
					{
						XMVECTOR v1 = XMLoadFloat4(&world_split_planes[i + p][v]);

						//transform to light view space
						v1 = XMVector4Transform(v1, XMLoadFloat4x4(&ILightData->View));

						min = XMVectorMin(min, v1);
						max = XMVectorMax(max, v1);
					}
				}

				XMStoreFloat4(&min_max_point_splits[i][0], min);
				XMStoreFloat4(&min_max_point_splits[i][1], max);
			}

			XMMATRIX split_proj_matrices[4];
			for (size_t i = 0; i < 4; i++)
			{
				if (min_max_point_splits[i][0].z > 0 && min_max_point_splits[i][1].z > 0)
					split_proj_matrices[i] = XMMatrixOrthographicOffCenterLH(min_max_point_splits[i][0].x
						, min_max_point_splits[i][1].x
						, min_max_point_splits[i][0].y
						, min_max_point_splits[i][1].y
						, 10.0
						, min_max_point_splits[i][1].z);
			}

			const auto dsv = mTexView_ShadowMap->GetDSV();
			mDeferredContext_C[0]->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
			
			int it = 0;

			for (size_t i = 0; i < 2; ++i)
			{
				for (size_t j = 0; j < 2; ++j)
				{

					GenerateCascadedShadowMap(mDeferredContext_C[it].Get(), mCommandList_C[it], split_proj_matrices[it], j * 1024.0f, i * 1024.0f);
					it++;
				}
			}
		}
		else
		{

			//gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_GenerateShadowMap");

			mDeferredContext->IASetIndexBuffer(gIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

			const auto Models_Basic32 = gModelManager.GetBasicModels();

			const auto Models_Skinned = gModelManager.GetSkinnedModels();

			const auto dsv = mTexView_ShadowMap->GetDSV();
			mDeferredContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);

			mDeferredContext->OMSetRenderTargets(0, nullptr, dsv);

			mDataCBPerLight.gLightVP = ILightData->ViewProj;
			mCBperLight->Update(mDeferredContext.Get(), &mDataCBPerLight, sizeof(XMFLOAT4X4));


			if (Models_Basic32->size() > 0)
			{
				const UINT vertexOffset = 0;
				mDeferredContext->IASetInputLayout(Vertex::IL_Basic32.Get());
				mDeferredContext->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &vertexOffset);


				ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(mDeferredContext.Get());

				for (auto model : *Models_Basic32)
				{

					/*if (iLight->BoundingBoxContainment(model->pMesh->AABB) == ContainmentType::DISJOINT)
					{
						continue;
					}*/


					mDataCBPerObject.gWorld = model->mWorldMatrix;
					mCBPerObject->Update(mDeferredContext.Get(), &mDataCBPerObject, sizeof(CB_PER_OBJECT));

					for (auto mesh : model->mMeshes)
					{
						//mPiplineList_NormalShadowMap.Apply_Preliminiary(mesh->mVertexType);

						mesh->Draw(mDeferredContext.Get());
					}

				}

			}

			if (Models_Skinned->size() > 0)
			{
				const UINT vertexOffset = 0;
				mDeferredContext->IASetInputLayout(Vertex::IL_Skinned.Get());
				mDeferredContext->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &vertexOffset);


				ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(mDeferredContext.Get());

				for (auto skModel : *Models_Skinned)
				{

					mDataCBPerObject.gWorld = skModel->mWorldMatrix;
					mCBPerObject->Update(mDeferredContext.Get(), &mDataCBPerObject, sizeof(CB_PER_OBJECT));

					for (auto mesh : skModel->mMeshes)
					{

						//mPiplineList_NormalShadowMap.Apply_Preliminiary(mesh->mVertexType);

						mesh->Draw(mDeferredContext.Get());
					}

				}
			}


			//gD3DUserDefinedAnnotation->EndEvent();

			mDeferredContext->FinishCommandList(true, mCommandList.ReleaseAndGetAddressOf());
		}
	}
}


void ILightObj::GenerateCascadedShadowMap(ID3D11DeviceContext * dc, ComPtr<ID3D11CommandList> & cl, XMMATRIX & proj_matrix, float view_top_left_x, float view_top_left_y)
{
	if (ILightData->CastShadow)
	{

		dc->IASetIndexBuffer(gIndexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

#pragma region CalculateCascadeFrustum

		const BoundingFrustum split_bounding_frustum = MathHelper::CreateBoundingFrustum(proj_matrix, ILightData->View);

		mViewport.TopLeftX = view_top_left_x;
		mViewport.TopLeftY = view_top_left_y;
		dc->RSSetViewports(1, &mViewport);

#pragma endregion


		//gD3DUserDefinedAnnotation->BeginEvent(L"RenderPass_GenerateShadowMap");

		const auto Models_Basic32 = gModelManager.GetBasicModels();

		const auto Models_Skinned = gModelManager.GetSkinnedModels();

		

		dc->OMSetRenderTargets(0, nullptr, mTexView_ShadowMap->GetDSV());
		XMStoreFloat4x4(&mDataCBPerLight.gLightVP, XMMatrixMultiply(XMLoadFloat4x4(&ILightData->View), proj_matrix));
		mCBperLight->Update(dc, &mDataCBPerLight, sizeof(XMFLOAT4X4));





		if (Models_Basic32->size() > 0)
		{
			const UINT vertexOffset = 0;

			dc->IASetInputLayout(Vertex::IL_Basic32.Get());
			dc->IASetVertexBuffers(0, 1, gVertexBuffer_Basic32.GetAddressOf(), &Vertex::VertexStride_Basic32, &vertexOffset);
			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Basic32(dc);


			for (auto model : *Models_Basic32)
			{

				/*if (iLight->BoundingBoxContainment(model->pMesh->AABB) == ContainmentType::DISJOINT)
				{
					continue;
				}*/



				mDataCBPerObject.gWorld = model->mWorldMatrix;
				mCBPerObject->Update(dc, &mDataCBPerObject, sizeof(CB_PER_OBJECT));

				for (auto mesh : model->mMeshes)
				{

					if (split_bounding_frustum.Contains(mesh->GetBoundingBox()) != ContainmentType::DISJOINT)
						mesh->Draw(dc);
				}

			}

		}

		if (Models_Skinned->size() > 0)
		{
			const UINT vertexOffset = 0;
			dc->IASetInputLayout(Vertex::IL_Skinned.Get());
			dc->IASetVertexBuffers(0, 1, gVertexBuffer_Skinned.GetAddressOf(), &Vertex::VertexStride_Skinned, &vertexOffset);


			ShaderList::VertexShaders::VS_BuildShadowMap::SetShader_Skinned(dc);

			for (auto skModel : *Models_Skinned)
			{

				mDataCBPerObject.gWorld = skModel->mWorldMatrix;
				mCBPerObject->Update(dc, &mDataCBPerObject, sizeof(CB_PER_OBJECT));

				for (auto mesh : skModel->mMeshes)
				{

					if (split_bounding_frustum.Contains(mesh->GetBoundingBox()) != ContainmentType::DISJOINT)
						mesh->Draw(dc);
				}

			}
		}


		//gD3DUserDefinedAnnotation->EndEvent();

		dc->FinishCommandList(true, cl.ReleaseAndGetAddressOf());
		return;
	}
}


ID3D11ShaderResourceView* ILightObj::GetShadowMapSRV()const noexcept
{
	return mTexView_ShadowMap->GetSRV();
}


ID3D11DeviceContext* ILightObj::GetDeferredContext() const noexcept
{
	return mDeferredContext.Get();
}

ID3D11DeviceContext* ILightObj::GetDeferredContext(int num) const noexcept
{
	if (bCascadedShadowMap)
		return mDeferredContext_C[num].Get();
	else
		return mDeferredContext.Get();
}


ID3D11CommandList* ILightObj::GetCmdList() const noexcept
{
	return mCommandList.Get();
}

ID3D11CommandList* ILightObj::GetCmdList(int num) const noexcept
{
	if (bCascadedShadowMap)
		return mCommandList_C[num].Get();
	else
		return mCommandList.Get();
}


const TexViews* ILightObj::GetShadowMapTexView() const noexcept
{
	return mTexView_ShadowMap.get();
}


void ILightObj::SetUpdated(bool _updated)
{
	mUpdated = _updated;
}



void ILightObj::Update(const float& dt)
{
	mFunc_Update(dt);

	UpdateDirInEyeScreen(gCamera_main.GetPositionXM(), gCamera_main.GetLook());

	if (mUpdated)
	{
		if (mUpdated_View)
			UpdateViewMatrix();
		if (mUpdated_Proj)
			UpdateProjectionMatrix();
		if (mUpdated_View || mUpdated_Proj)
		{
			UpdateViewProjMatrix();
			mUpdated_Proj = false;
			mUpdated_View = false;
		}
	}
}



bool ILightObj::IsUpdatedFromLastFrame()
{
	return mUpdated;
}




//
///****************************** Directional Light
//

DirectionalLightObj::DirectionalLightObj(DirectionalLight * lightData, XMFLOAT4 _diffusecolor, XMFLOAT4 _ambientColor, XMFLOAT4 _specularColor, XMFLOAT4 _target, XMFLOAT3 _direction, XMFLOAT3 _position, std::string _name, float _lightSize, float _znear, float _zfar, float _frustumRadius, int _castShadow, int _index, int _id, int _shadowMapID, bool cascaded_shadow_map)
{
	/*ILightData = std::make_shared<DirectionalLight>();

	LightData.reset(static_cast<DirectionalLight*>(ILightData.get()));*/

	//LightData = static_cast<DirectionalLight&>(ILightData);

	LightData = lightData;
	ILightData = lightData;

	mIndex = _index;
	mLightType = LIGHT_TYPE_DIRECTIONAL;
	bDisabled = false;
	bCascadedShadowMap = cascaded_shadow_map;


	LightData->Diffuse = _diffusecolor;
	LightData->Ambient = _ambientColor;
	LightData->Specular = _specularColor;
	LightData->LightSize = _lightSize;
	LightData->zNear = _znear;
	LightData->zFar = _zfar;
	LightData->Position = _position;
	LightData->Direction = _direction;
	LightData->ID = _id;
	LightData->ShadowMapID = _shadowMapID;

	SetCastShadow(_castShadow);

	mFrustumRadius = _frustumRadius;
	mLightTarget = _target;

	Name = _name;

	UpdateViewMatrix();
	UpdateProjectionMatrix();
	UpdateViewProjMatrix();
}


DirectionalLightObj::DirectionalLightObj(DirectionalLight * lightData)
{
	LightData = lightData;
	ILightData = lightData;

	mLightType = LIGHT_TYPE_DIRECTIONAL;
	bDisabled = false;

	LightData->Diffuse = XMFLOAT4();
	LightData->Ambient = XMFLOAT4();
	LightData->Specular = XMFLOAT4();
	LightData->LightSize = 0.0f;
	LightData->zNear = 10.0f;
	LightData->zFar = 100.0f;
	LightData->Position = XMFLOAT3(10.0f, 10.0f, 10.0f);
	LightData->Direction = XMFLOAT3(1.0f, 1.0f, 1.0f);
	LightData->CastShadow = 0;
	LightData->ID = 0;
	LightData->ShadowMapID = -1;

	mFrustumRadius = 50.0f;
	mLightTarget = XMFLOAT4();

	Name = "";
}




void DirectionalLightObj::UpdateFrustum(float _znear, float _zfar, float _frustumRadius = -1.0f)
{
	LightData->zNear = _znear;
	LightData->zFar = _zfar;

	mFrustumRadius = _frustumRadius == -1.0f ? mFrustumRadius : _frustumRadius;
}


void DirectionalLightObj::SetLightData(DirectionalLight * directionalLightData)
{
	LightData = directionalLightData;

	ILightData = directionalLightData;
}


void DirectionalLightObj::UpdateProjectionMatrix()
{
	/*XMFLOAT3 sphereCenterLS;
	XMStoreFloat3(&sphereCenterLS, XMVector3TransformCoord(mLightTarget, LightData->View));

	float l = sphereCenterLS.x - mFrustumRadius;
	float r = sphereCenterLS.x + mFrustumRadius;
	float b = sphereCenterLS.y - mFrustumRadius;
	float t = sphereCenterLS.y + mFrustumRadius;
	float n = sphereCenterLS.z - mFrustumRadius;
	float f = sphereCenterLS.z + mFrustumRadius;

	mProjMatrix = XMMatrixOrthographicOffCenterLH(l, r, b, t, n, f);*/

	auto projMatrix = XMMatrixOrthographicLH(mFrustumRadius * 2, mFrustumRadius * 2, LightData->zNear, LightData->zFar);

	XMStoreFloat4x4(&mProjMatrix, projMatrix);

	LightData->PerpectiveValues = XMFLOAT4(mProjMatrix._11, mProjMatrix._22, mProjMatrix._33, mProjMatrix._43);
}


float DirectionalLightObj::GetFrustumRadius() const
{
	return mFrustumRadius;
}


void DirectionalLightObj::SetFrustumRadius(float FrustumRadius)
{
	if (FrustumRadius == mFrustumRadius)
		return;

	mFrustumRadius = FrustumRadius;
	mUpdated_Proj = true;
}









/*
*
******************************* Spot Light
*
*/

SpotLightObj::SpotLightObj(SpotLight * lightData, XMFLOAT4 _diffusecolor, XMFLOAT4 _ambientColor, XMFLOAT4 _specularColor, XMFLOAT4 _target, XMFLOAT3 _direction, XMFLOAT3 _position, XMFLOAT3 _attenuation, std::string _name, float _lightSize, float _znear, float _zfar, float _range, float _spot, int _castShadow, int _index, int _id, int _shadowMapID, int _reversedZ, bool cascaded_shadow_map)
{
	LightData = lightData;
	ILightData = lightData;

	mIndex = _index;
	mLightType = LIGHT_TYPE_SPOT;
	bDisabled = false;
	bCascadedShadowMap = cascaded_shadow_map;

	LightData->Diffuse = _diffusecolor;
	LightData->Ambient = _ambientColor;
	LightData->Specular = _specularColor;
	LightData->LightSize = _lightSize;
	LightData->Direction = _direction;
	LightData->Position = _position;
	LightData->Attenuation = _attenuation;
	LightData->zNear = _znear;
	LightData->zFar = _zfar;
	LightData->Range = _range;
	LightData->Spot = _spot;
	LightData->ID = _id;
	LightData->ShadowMapID = _shadowMapID;

	SetCastShadow(_castShadow);

	mLightTarget = _target;

	Name = _name;

	UpdateViewMatrix();
	UpdateProjectionMatrix();
	UpdateViewProjMatrix();
}


SpotLightObj::SpotLightObj(SpotLight * lightData)
{

	LightData = lightData;
	ILightData = lightData;

	mLightType = LIGHT_TYPE_SPOT;
	bDisabled = false;

	LightData->Diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LightData->Ambient = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LightData->Specular = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	LightData->LightSize = 0.5f;
	LightData->Direction = XMFLOAT3(1.0f, 1.0f, 1.0f);
	LightData->Position = XMFLOAT3(10.0f, 10.0f, 10.0f);
	LightData->Attenuation = XMFLOAT3(1.0f, 0.0f, 0.0f);
	LightData->zNear = 10.0f;
	LightData->zFar = 100.0f;
	LightData->Range = 50.0f;
	LightData->Spot = 10.0f;
	LightData->CastShadow = 0;
	LightData->ID = 0;
	LightData->ShadowMapID = -1;

	mLightTarget = XMFLOAT4();
	Name = "";

}



void SpotLightObj::SetLightData(SpotLight * spotLightData)
{
	LightData = spotLightData;

	ILightData = spotLightData;
}


void SpotLightObj::UpdateFrustum(float _znear, float _zfar)
{
	LightData->zNear = _znear;
	LightData->zFar = _zfar;
}


void SpotLightObj::UpdateProjectionMatrix()
{
	auto projMatrix = XMMatrixPerspectiveFovLH(mFOVYAngle, mAspectRatio, LightData->zNear, LightData->zFar);
	XMStoreFloat4x4(&mProjMatrix, projMatrix);

	LightData->PerpectiveValues = XMFLOAT4(mProjMatrix._11, mProjMatrix._22, mProjMatrix._33, mProjMatrix._43);
}


void SpotLightObj::SetAttenuation(float _d1, float _d2, float _d3)
{
	LightData->Attenuation = XMFLOAT3(_d1, _d2, _d3);
}


void SpotLightObj::SetRange(float _range)
{
	LightData->Range = _range;
}


void SpotLightObj::SetSpotSize(float _spotPow)
{
	LightData->Spot = _spotPow;
}


float& SpotLightObj::GetSpotSize() const
{
	return LightData->Spot;
}


float& SpotLightObj::GetRange() const
{
	return LightData->Range;
}


DirectX::XMFLOAT3& SpotLightObj::GetAttenuation() const
{
	return LightData->Attenuation;
}


//
/// Class Lights
//

Lights::Lights()
{

	mDirectionalLights.reserve(MAX_LIGHT_DIRECTIONALLIGHT);
	mSpotLights.reserve(MAX_LIGHT_SPOTLIGHT);

	mCB_DirectLights.reset(new ConstantBuffer<CB_DIRECTIONALLIGHTS>());
	mCB_SpotLights.reset(new ConstantBuffer<CB_SPOTLIGHTS>());
}

Lights::~Lights() = default;

Lights::Lights(Lights&&) = default;

Lights& Lights::operator=(Lights&&) = default;


void Lights::Destroy()
{
	mCB_SpotLights->Release();
}

void Lights::ResetShadowMapIDs()
{
	UINT ShadowMapID = 0;

	for (auto& dlight : mDirectionalLights)
	{
		if (mData_CB_DirectionalLight.gDirectionalLightArray[dlight.GetIndex()].CastShadow > 0)
		{
			mData_CB_DirectionalLight.gDirectionalLightArray[dlight.GetIndex()].ShadowMapID = ShadowMapID++;
		}
	}

	for (auto& slight : mSpotLights)
	{
		if (mData_CB_SpotLights.gSpotLightArray[slight.GetIndex()].CastShadow > 0)
		{
			mData_CB_SpotLights.gSpotLightArray[slight.GetIndex()].ShadowMapID = ShadowMapID++;
		}
	}

	mShadowMapCount = ShadowMapID;
}

UINT Lights::GetShadowMapCount()
{
	return mShadowMapCount;
}


void Lights::Update(const float& dt)
{
	/*if (mSelectedLightByListBox > -1)
	{
		MoveLight(mSelectedLightByListBox, gCamera_main.GetPosition(), gCamera_main.GetLook());
	}*/

	if (mMoveLightByCamera)
	{
		MoveLight(gCamera_main.GetPosition(), gCamera_main.GetLook());
	}

	for (auto ilight : mCollectionILights)
	{
		ilight->Update(dt);
	}
}

void Lights::MoveLight(int lightIndex, XMFLOAT3 position, XMFLOAT3 direction)
{

	auto light = mCollectionILights[lightIndex];

	light->SetPosition(position);
	light->SetDirection(direction);

}

void Lights::MoveLight(XMFLOAT3 position, XMFLOAT3 direction)
{

	mCurrentSelectedLight->SetPosition(position);
	mCurrentSelectedLight->SetDirection(direction);

}

void Lights::LightSelected(int lightIndex)
{
	mSelectedLightByListBox = lightIndex;
}

void Lights::ImportLights(std::wstring _lightFile, ID3D11DeviceContext * _context)
{
	std::ifstream fin(_lightFile);
	std::wstring s1 = L"Loading Light File Failed: ";
	std::wstring s2 = L" not founded";
	auto s = s1 + _lightFile + s2;

	if (!fin)
	{
		MessageBox(0, s.c_str(), L"error", 0);
	}

	size_t light_count = 0;
	std::string ignore;
	std::string command;
	char c;

	std::string light_name = "";
	std::string light_type = "";
	XMFLOAT4 light_diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	XMFLOAT4 light_ambient = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	XMFLOAT4 light_specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	XMFLOAT4 light_target = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT3 light_direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 light_position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 light_up = XMFLOAT3(0.0f, 0.0f, 0.0f);
	float light_frustumRadius = 0.0f;
	float light_size = 0.0f;
	float light_znear = 0.0f;
	float light_zfar = 0.0f;
	float light_range = 0.0f;
	float light_spot = 0.0f;
	XMFLOAT3 light_attenuation = XMFLOAT3(0.0f, 0.0f, 0.0f);
	int light_castShadow = 0;
	int light_disabled = 0;
	int light_ID = 0;
	int spotlight_index = 0;
	int directlight_index = 0;
	int light_ShadowMapID = -1;
	int light_reversedZ = 0;
	bool cascaded_shadow_map = false;

	while (!fin.eof())
	{

		fin >> command;
		if (command == "l")
		{
			if (light_count > 0)
			{
				if (light_type == "directional")
				{
					mDirectionalLights.emplace_back(
						&mData_CB_DirectionalLight.gDirectionalLightArray[directlight_index],
						light_diffuse,
						light_ambient,
						light_specular,
						light_target,
						light_direction,
						light_position,
						light_name,
						light_size,
						light_znear,
						light_zfar,
						//light_frustumRadius,
						gCamera_main.GetFarWindowWidth(),
						light_castShadow,
						directlight_index,
						light_ID,
						light_ShadowMapID,
						cascaded_shadow_map
					);

					mCollectionILights.push_back(&mDirectionalLights.back());

					directlight_index++;
				}
				else if (light_type == "spot")
				{
					mSpotLights.emplace_back(
						&mData_CB_SpotLights.gSpotLightArray[spotlight_index],
						light_diffuse,
						light_ambient,
						light_specular,
						light_target,
						light_direction,
						light_position,
						light_attenuation,
						light_name,
						light_size,
						light_znear,
						light_zfar,
						light_range,
						light_spot,
						light_castShadow,
						spotlight_index,
						light_ID,
						light_ShadowMapID,
						light_reversedZ,
						cascaded_shadow_map
					);

					mCollectionILights.push_back(&mSpotLights.back());

					spotlight_index++;
				}
			}

			fin >> light_name;
			light_count++;
		}
		else if (command == "t")
		{
			fin >> light_type;
		}
		else if (command == "diffuse")
		{
			fin >> light_diffuse.x >> light_diffuse.y >> light_diffuse.z >> light_diffuse.w;
		}
		else if (command == "ambient")
		{
			fin >> light_ambient.x >> light_ambient.y >> light_ambient.z >> light_ambient.w;
		}
		else if (command == "specular")
		{
			fin >> light_specular.x >> light_specular.y >> light_specular.z >> light_specular.w;
		}
		else if (command == "direction")
		{
			fin >> light_direction.x >> light_direction.y >> light_direction.z;
		}
		else if (command == "position")
		{
			fin >> light_position.x >> light_position.y >> light_position.z;
		}
		else if (command == "target")
		{
			fin >> light_target.x >> light_target.y >> light_target.z;
		}
		else if (command == "up")
		{
			fin >> light_up.x >> light_up.y >> light_up.z;
		}
		else if (command == "frustumradius")
		{
			fin >> light_frustumRadius;
		}
		else if (command == "lightsize")
		{
			fin >> light_size;
		}
		else if (command == "znear")
		{
			fin >> light_znear;
		}
		else if (command == "zfar")
		{
			fin >> light_zfar;
		}
		else if (command == "disabled")
		{
			fin >> light_disabled;
		}
		else if (command == "castshadow")
		{
			fin >> light_castShadow;
		}
		else if (command == "range")
		{
			fin >> light_range;
		}
		else if (command == "spot")
		{
			fin >> light_spot;
		}
		else if (command == "attenuation")
		{
			fin >> light_attenuation.x >> light_attenuation.y >> light_attenuation.z;
		}
		else if (command == "id")
		{
			fin >> light_ID;
		}
		else if (command == "cascadedshadow")
		{
			fin >> cascaded_shadow_map;
		}
		else {
			fin.ignore(1000, '\n');
		}
	}

	if (light_count > 0)
	{
		if (light_type == "directional")
		{
			mDirectionalLights.emplace_back(
				&mData_CB_DirectionalLight.gDirectionalLightArray[directlight_index],
				light_diffuse,
				light_ambient,
				light_specular,
				light_target,
				light_direction,
				light_position,
				light_name,
				light_size,
				light_znear,
				light_zfar,
				light_frustumRadius,
				light_castShadow,
				directlight_index,
				light_ID,
				light_ShadowMapID,
				cascaded_shadow_map
			);

			mCollectionILights.push_back(&mDirectionalLights.back());
		}
		else if (light_type == "spot")
		{

			mSpotLights.emplace_back(
				&mData_CB_SpotLights.gSpotLightArray[spotlight_index],
				light_diffuse,
				light_ambient,
				light_specular,
				light_target,
				light_direction,
				light_position,
				light_attenuation,
				light_name,
				light_size,
				light_znear,
				light_zfar,
				light_range,
				light_spot,
				light_castShadow,
				spotlight_index,
				light_ID,
				light_ShadowMapID,
				light_reversedZ,
				cascaded_shadow_map
			);

			mCollectionILights.push_back(&mSpotLights.back());

			//delete slight;
		}
	}
	InitBuffers();

	SetLightsBuffer_PS_DeferredContext();

	ResetShadowMapIDs();
	//GenerateLBItems();
}



void Lights::LoadLights(std::wstring _lightFile, ID3D11DeviceContext * _context)
{
	std::ifstream bfin(_lightFile, std::ios::in | std::ios::binary);
	if (bfin.is_open())
	{
		boost::archive::binary_iarchive ai(bfin);

		ai&* this;
	}
	InitBuffers();


	ResetShadowMapIDs();
}

void Lights::SaveLights(std::wstring _lightFile)
{
	std::ofstream bfout = std::ofstream(_lightFile, std::ios::out | std::ios::binary | std::ios::app);
	boost::archive::binary_oarchive ao(bfout);

	ao& (*this);
}

void Lights::UpdateLightBuffers(ID3D11DeviceContext * _context)
{

	mCB_SpotLights->Update(_context, &mData_CB_SpotLights, mSpotLights.size() * sizeof(SpotLight));

	mCB_DirectLights->Update(_context, &mData_CB_DirectionalLight, mDirectionalLights.size() * sizeof(DirectionalLight));

	/*std::vector<DirectionalLight> dlights;
	for (auto e : mDirectionalLights)
	{
		dlights.push_back(*e.LightData);
	}

	std::vector<SpotLight> slights;
	for (auto e : mSpotLights)
	{
		slights.push_back(*e.LightData);
	}
	_context->Map(mD3DDirectionalLightsB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mMapDirectionalLights);
	memcpy(mMapDirectionalLights.pData, &dlights[0], dlights.size() * sizeof(DirectionalLight));
	_context->Unmap(mD3DDirectionalLightsB.Get(), 0);

	_context->Map(mD3DSpotLightsB.Get(), 0, D3D11_MAP_WRITE_DISCARD, 0, &mMapSpotLights);
	memcpy(mMapSpotLights.pData, &slights[0], slights.size() * sizeof(SpotLight));
	_context->Unmap(mD3DSpotLightsB.Get(), 0);*/
}

void Lights::SetLightsBuffer_VS(ID3D11DeviceContext * _context, UINT spotLightRegisterSlot, UINT dirLightRegisterSlot)
{

	/*_context->VSSetShaderResources(_directionalLightRegisterNum, 1, &mSRVDirectionalLights);
	_context->VSSetShaderResources(_spotLightRegisterNum, 1, &mSRVSpotLights);*/
	/*mCB_SpotLights->ChangeRegisterSlot(spotLightRegisterSlot);
	mCB_SpotLights->SetConstantBuffer(_context);

	mCB_DirectLights->ChangeRegisterSlot(dirLightRegisterSlot);
	mCB_DirectLights->SetConstantBuffer(_context);*/
}

void Lights::PrepareAllShadowMapsCMDListAsyc()
{
	concurrency::task_group tasks;

	for (auto& dlight : mDirectionalLights)
	{
		if (dlight.IsUpdatedFromLastFrame())
			tasks.run([&dlight]() { dlight.GenerateShadowMap(); });
	}

	for (auto& slight : mSpotLights)
	{
		if (slight.IsUpdatedFromLastFrame())
			tasks.run([&slight]() {slight.GenerateShadowMap(); });
	}

	tasks.wait();
}

void Lights::PrepareAllShadowMapsCMDList()
{
	for (auto& dlight : mDirectionalLights)
	{
		if (dlight.IsUpdatedFromLastFrame())
			dlight.GenerateShadowMap();
	}

	for (auto& slight : mSpotLights)
	{
		if (slight.IsUpdatedFromLastFrame())
			slight.GenerateShadowMap();
	}

}

void Lights::GenerateAllShadowMaps()
{
	for (auto& light : mCollectionILights)
	{
		if (!light->bCascadedShadowMap)
			gContext->ExecuteCommandList(light->GetCmdList(), false);
		else
			for (size_t i = 0; i < 4; i++)
			{
				gContext->ExecuteCommandList(light->GetCmdList(i), false);
			}
	}
}

void Lights::RenderConfigurationUI()
{
	ImGui::Begin("Lights");

	//Lights List As a ComboBox
	static const char* currentSelectedLightName = NULL;

	if (ImGui::BeginCombo("Light List", currentSelectedLightName))
	{
		//De select
		if (ImGui::Selectable("", currentSelectedLightName == ""))
		{
			currentSelectedLightName = "";
			mCurrentSelectedLight = nullptr;
		}

		for (int i = 0; i < mCollectionILights.size(); ++i)
		{
			if (ImGui::Selectable(mCollectionILights[i]->Name.c_str(), currentSelectedLightName == mCollectionILights[i]->Name.c_str()))
			{
				mCurrentSelectedLight = mCollectionILights[i];
				currentSelectedLightName = mCurrentSelectedLight->Name.c_str();
			}
		}
		ImGui::EndCombo();
	}

	if (mCurrentSelectedLight != nullptr)
	{

		//Strings

		static std::string inputString;
		inputString = mCurrentSelectedLight->Name;

		if (ImGui::InputText("Name", &inputString, ImGuiInputTextFlags_EnterReturnsTrue))
		{
			mCurrentSelectedLight->Name = inputString;
		}

		//Vectors

		if (ImGui::ColorEdit4("Diffuse Color", &mCurrentSelectedLight->GetILightData().Diffuse.x))
		{
		}

		if (ImGui::ColorEdit4("Ambient Color", &mCurrentSelectedLight->GetILightData().Ambient.x))
		{
		}


		if (ImGui::DragFloat3("View Direction", &mCurrentSelectedLight->GetILightData().Direction.x, 0.001f, -1.0, 1.0f))
		{
		}

		if (ImGui::DragFloat3("World Position", &mCurrentSelectedLight->GetILightData().Position.x, 1.0f, -4000.0, 4000.0f))
		{
		}

		if (ImGui::DragFloat("Size", &mCurrentSelectedLight->GetILightData().LightSize, 0.0001f, 0.0f, 1.0f))
		{
		}

		if (ImGui::DragFloat("ZNear", &mCurrentSelectedLight->GetILightData().zNear, 1.0f, 1.0f, 10000.0f))
		{
		}

		if (ImGui::DragFloat("ZFar", &mCurrentSelectedLight->GetILightData().zFar, 1.0f, 1.0f, 10000.0f))
		{
		}




		if (typeid(*mCurrentSelectedLight) == typeid(SpotLightObj))
		{
			static SpotLightObj* SelectedSpotLight = static_cast<SpotLightObj*>(mCurrentSelectedLight);
			if (ImGui::DragFloat("Range", &SelectedSpotLight->GetRange(), 1.0f, 1.0f, 10000.0f))
			{
			}

			if (ImGui::DragFloat("Spot", &SelectedSpotLight->GetSpotSize(), 1.0f, 1.0f, 10000.0f))
			{
			}

			if (ImGui::DragFloat3("Attenuation", &SelectedSpotLight->GetAttenuation().x, 1.0f, 1.0f, 10000.0f))
			{
			}


		}

		if (typeid(*mCurrentSelectedLight) == typeid(DirectionalLightObj))
		{
			static DirectionalLightObj* SelectedDirectLight = static_cast<DirectionalLightObj*>(mCurrentSelectedLight);

			static float FrustumRadius = SelectedDirectLight->GetFrustumRadius();

			if (ImGui::DragFloat("Frustum Radius", &FrustumRadius, 1.0f, 1.0f, 10000.0f))
			{
				SelectedDirectLight->SetFrustumRadius(FrustumRadius);
			}


			//////////////////////////////////////////////////////////////////////////
			// Calculate Horz Light Direction from Eye ViewPoint

			/*XMFLOAT4 horzVector(1.0f, 0.0f, 0.0f, 0.0f);

			auto d = SelectedDirectLight->GetILightData();

			auto v_Inv = XMMatrixInverse(nullptr, XMLoadFloat4x4(&d.View));

			auto horzW = XMVector4Transform(XMLoadFloat4(&horzVector), v_Inv);

			auto horzEye = XMVector4Transform(horzW, gCamera_main.View());

			auto horzEyePerspective = XMVector4Transform(horzEye, gCamera_main.Proj());

			auto length = XMVector4Length(horzEyePerspective);

			std::string HorzEyeS = "Light Horz Direction In Eye Space : x= " + std::to_string(XMVectorGetX(horzEye)) + " y=" + std::to_string(XMVectorGetY(horzEye)) + " z=" + std::to_string(XMVectorGetZ(horzEye));
			std::string HorzEyePerspectiveS = "Light Horz Direction In Eye Space : x= " + std::to_string(XMVectorGetX(horzEyePerspective)) + " y=" + std::to_string(XMVectorGetY(horzEyePerspective)) + " z=" + std::to_string(XMVectorGetZ(horzEyePerspective));
			std::string HorzEyeLengthS = "Light Horz Direction In Eye Clip Space Length : " + std::to_string(XMVectorGetX(length));



			ImGui::Text(HorzEyeS.c_str());
			ImGui::Text(HorzEyePerspectiveS.c_str());
			ImGui::Text(HorzEyeLengthS.c_str());*/

			auto d = SelectedDirectLight->GetILightData();


			std::string HorzEyeS = "Selected Light Horiz Dir in Screen Space: x:" + std::to_string(d.DirInEyeScreen_Horz.x) + " y:" + std::to_string(d.DirInEyeScreen_Horz.y);
			ImGui::Text(HorzEyeS.c_str());

			float l;
			XMStoreFloat(&l, XMVector2Length(XMLoadFloat2(&d.DirInEyeScreen_Horz)));

			std::string HorzEyeLengthS = "Selected Light Horiz Dir in Screen Space Length: " + std::to_string(l);
			ImGui::Text(HorzEyeLengthS.c_str());
		}

		//Check Box

		if (ImGui::Checkbox("Disabled", &mCurrentSelectedLight->bDisabled))
		{
		}

		static bool castShadow = true;

		if (ImGui::Checkbox("Cast Shadow", &castShadow))
		{
			mCurrentSelectedLight->GetILightData().CastShadow = static_cast<int>(castShadow);
		}



		if (ImGui::Checkbox("Move By Camera", &mMoveLightByCamera))
		{

		}


		mCurrentSelectedLight->UpdateViewMatrix();
		mCurrentSelectedLight->UpdateProjectionMatrix();
		mCurrentSelectedLight->UpdateViewProjMatrix();
	}

	ImGui::End();
}

void Lights::SetLightsBuffer_PS(ID3D11DeviceContext * _context, UINT spotLightRegisterSlot, UINT dirLightRegisterSlot)
{

	/*_context->PSSetShaderResources(_directionalLightRegisterNum, 1, &mSRVDirectionalLights);
	_context->PSSetShaderResources(_spotLightRegisterNum, 1, &mSRVSpotLights);*/

	mCB_SpotLights->ChangeRegisterSlot(spotLightRegisterSlot);
	mCB_SpotLights->SetConstantBuffer(_context);

	mCB_DirectLights->ChangeRegisterSlot(dirLightRegisterSlot);
	mCB_DirectLights->SetConstantBuffer(_context);
}

void Lights::InitBuffers()
{

	mCB_SpotLights->InitConstantBuffer(SHADER_SLOTS_PS_CB_SPOTLIGHTS, CONSTANT_BUFFER_PS, &mData_CB_SpotLights, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);


	mCB_DirectLights->InitConstantBuffer(SHADER_SLOTS_PS_CB_DIRLIGHTS, CONSTANT_BUFFER_PS, &mData_CB_DirectionalLight, D3D11_CPU_ACCESS_WRITE, D3D11_USAGE_DYNAMIC);

	/*mD3DDirectionalLightsB.Reset();
	mD3DSpotLightsB.Reset();
	mSRVDirectionalLights.Reset();
	mSRVSpotLights.Reset();*/

	/*D3D11_BUFFER_DESC bdesc;
	bdesc.ByteWidth = mDirectionalLights.size() * sizeof(DirectionalLight);
	bdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
	bdesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	bdesc.MiscFlags = D3D11_RESOURCE_MISC_BUFFER_STRUCTURED;
	bdesc.StructureByteStride = sizeof(DirectionalLight);
	bdesc.Usage = D3D11_USAGE_DYNAMIC;

	mdevice->CreateBuffer(&bdesc, nullptr, &mD3DDirectionalLightsB);

	D3D11_SHADER_RESOURCE_VIEW_DESC srvdesc;
	srvdesc.Format = DXGI_FORMAT_UNKNOWN;
	srvdesc.ViewDimension = D3D_SRV_DIMENSION_BUFFEREX;
	srvdesc.BufferEx.Flags = 0;
	srvdesc.BufferEx.FirstElement = 0;
	srvdesc.BufferEx.NumElements = mDirectionalLights.size();

	mdevice->CreateShaderResourceView(mD3DDirectionalLightsB.Get(), &srvdesc, &mSRVDirectionalLights);

	bdesc.ByteWidth = mSpotLights.size() * sizeof(SpotLight);
	bdesc.StructureByteStride = sizeof(SpotLight);

	mdevice->CreateBuffer(&bdesc, nullptr, &mD3DSpotLightsB);

	srvdesc.BufferEx.NumElements = mSpotLights.size();

	mdevice->CreateShaderResourceView(mD3DSpotLightsB.Get(), &srvdesc, &mSRVSpotLights);*/
}

void Lights::SetLightsBuffer_PS_DeferredContext()
{
	for (auto& light : mCollectionILights)
	{
		if (!light->bCascadedShadowMap)
			SetLightsBuffer_PS(light->GetDeferredContext(), SHADER_SLOTS_PS_CB_SPOTLIGHTS, SHADER_SLOTS_PS_CB_DIRLIGHTS);
		else
		{
			for (size_t i = 0; i < 4; ++i)
				SetLightsBuffer_PS(light->GetDeferredContext(i), SHADER_SLOTS_PS_CB_SPOTLIGHTS, SHADER_SLOTS_PS_CB_DIRLIGHTS);
		}
	}
}

void Lights::SetLightsBuffer_VS_DeferredContext()
{
	for (auto& light : mCollectionILights)
	{
		SetLightsBuffer_VS(light->GetDeferredContext(), SHADER_SLOTS_PS_CB_SPOTLIGHTS, SHADER_SLOTS_PS_CB_DIRLIGHTS);
	}
}


// void Lights::GenerateLBItems()
// {
// 	m_itemList.ClearAllItems();
// 
// 	for (auto l : mCollectionILights)
// 	{
// 		m_itemList.AddItem(l->GetIndex(), StringConverter::StringToWide(l->Name));
// 	}
// 
// 	m_itemList.Func_SelectItem = std::bind(&Lights::LightSelected, this, std::placeholders::_1);
// 	m_itemList.Func_DeSelectItem = std::bind(&Lights::LightSelected, this, std::placeholders::_1);
// }

