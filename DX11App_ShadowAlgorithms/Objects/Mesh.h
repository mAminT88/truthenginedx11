#pragma once


//#include "SkinAnimation.h"
//#include "D3DUtil.h"
//#include "Globals.h"
//#include "Graphics\Vertex.h"
//#include "Graphics\Material.h"

#include "XMathSerialization.h"

class Model;
class Material;

enum VERTEX_TYPE : int;

class IMesh
{
public:

	std::string mName;

	VERTEX_TYPE mVertexType;

	XMFLOAT4X4 mMatrix_TexTransform;

	virtual void Draw(ID3D11DeviceContext* context) const;

	virtual Material* GetMaterial()const;
	

	virtual const BoundingBox& GetBoundingBox() const;
	

	virtual void SetMaterial(Material* material);
	

	UINT GetMatrialIndex()const;
	

protected:
	UINT mIndexCount, mVertexCount, mIndexOffset, mVertexOffset, mMaterialIndex;
	/*ComPtr<ID3D11Buffer> mVertexBuffer;
	ComPtr<ID3D11Buffer> mIndexBuffer;*/
	ID3D11DeviceContext* mContext;
	std::string mTextureKey;

	BoundingBox mAABB;

	Model* mParent;

	Material *mMaterial;

private:

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive &ar, const unsigned int version)
	{
	}

	template<class Archive>
	void save(Archive &ar, const unsigned int version) const
	{
	}


	BOOST_SERIALIZATION_SPLIT_MEMBER();
};

class Mesh_Basic32 : public IMesh
{

public:
	Mesh_Basic32();
	Mesh_Basic32(std::string& name, Model* Parent, UINT32 vertexCount, UINT32 indexCount, UINT32 indexOffset, UINT32 vertexOffset, Material* pMaterial, UINT MaterialIndex, const XMFLOAT4X4& TexTransform, const BoundingBox& AABB, ID3D11Device* device, ID3D11DeviceContext* context);
	Mesh_Basic32(const Mesh_Basic32& mesh);

	virtual void Draw(ID3D11DeviceContext* context)const override;

protected:
	
	void InitBuffers(ID3D11Device* device);


private:

#pragma region Serialization 

	friend class boost::serialization::access;

	template<class Archive>
	void load(Archive& ar, const unsigned int version) {
		ar& mName;
		ar& mVertexCount;
		ar& mIndexCount;
		ar& mVertexOffset;
		ar& mIndexOffset;
		ar& mMaterialIndex;
		ar& mMatrix_TexTransform;

		/*mVertecies = std::make_shared<std::vector<Vertex::Basic32>>();
		mIndecies = std::make_shared<std::vector<UINT32>>();

		mVertecies->reserve(mVertexCount);
		mIndecies->reserve(mIndexCount);

		for (int i = 0; i < mVertexCount; ++i)
		{
			mVertecies->emplace_back();
			auto& v = mVertecies->back();

			ar& v.Pos;
			ar& v.Normal;
			ar& v.Tex;
		}

		for (int i = 0; i < mIndexCount; ++i)
		{
			mIndecies->emplace_back();
			auto& index = mIndecies->back();

			ar& index;
		}*/

		InitBuffers(gDevice.Get());
	}

	template<class Archive>
	void save(Archive& ar, const unsigned int version) const {
		ar& mName;
		ar& mVertexCount;
		ar& mIndexCount;
		ar& mVertexOffset;
		ar& mIndexOffset;
		ar& mMaterialIndex;
		ar& mMatrix_TexTransform;

		/*for (int i = 0; i < mVertexCount; ++i)
		{
			ar& (*mVertecies)[i].Pos;
			ar& (*mVertecies)[i].Normal;
			ar& (*mVertecies)[i].Tex;
		}

		for (int i = 0; i < mIndexCount; ++i)
		{
			ar& (*mIndecies)[i];
		}*/
	}

	BOOST_SERIALIZATION_SPLIT_MEMBER();
#pragma endregion Serialization

};

class Mesh_Skinned : public IMesh
{

public:
	Mesh_Skinned();
	Mesh_Skinned(std::string& name, Model* Parent, UINT32 vertexCount, UINT32 indexCount, UINT32 indexOffset, UINT32 vertexOffset, Material* pMaterial, UINT MaterialIndex, const XMFLOAT4X4& TexTransform, const BoundingBox& AABB, ID3D11Device* device, ID3D11DeviceContext* context);
	Mesh_Skinned(const Mesh_Skinned& mesh);

	virtual void Draw(ID3D11DeviceContext* context) const override;

protected:
	

	void InitBuffers(ID3D11Device* device);

};
