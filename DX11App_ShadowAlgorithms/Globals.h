#pragma once



#include "Objects/Camera.h"
#include "Objects/Lights.h"
#include "Graphics/SwapChain.h"
#include "ObjectManagers/ModelManager.h"
#include "ObjectManagers/CharacterManager.h"
#include "Input/KeyboardClass.h"
#include "Input/MouseClass.h"

#include "Graphics/Vertex.h"
//#include "Graphics/ShaderList/CommonConstantBuffers_ComputeShader.h"

class IMesh;

namespace Globals
{

//--------------------------------------------------------------------------------------
// DirectX Globals
//--------------------------------------------------------------------------------------

	extern ComPtr<ID3D11Device> gDevice;
	extern ComPtr<ID3D11DeviceContext> gContext;

	extern ComPtr<ID3DUserDefinedAnnotation> gD3DUserDefinedAnnotation;

	extern ComPtr<ID3D11Buffer> gVertexBuffer_Basic32;
	extern ComPtr<ID3D11Buffer> gVertexBuffer_Skinned;
	extern ComPtr<ID3D11Buffer> gIndexBuffer;

	extern std::vector<Vertex::Basic32> gVertecies_Basic32;
	extern std::vector<Vertex::Skinned> gVertecies_Skinned;
	extern std::vector<UINT> gIndecies;

	extern SwapChain gSwapChain;
	extern D3D11_VIEWPORT gViewPort;

	extern int gD3DComplieShaderFlags;

//--------------------------------------------------------------------------------------
// Global units
//--------------------------------------------------------------------------------------

	extern float gDt;


	extern int gClientWidth;
	extern int gClientHeight;

	extern int gCurrentFrame, gLastFrame;

	extern float gShadowMapResolution;
	extern float gShadowMapDXY;

	extern bool gAppPaused;
	


//--------------------------------------------------------------------------------------
// Global Window App
//--------------------------------------------------------------------------------------

	extern HWND gHWND_App;
	extern HWND gHWND_Rendering;
	extern HWND gHWND_RightListBox;
	extern HINSTANCE gHInstance;

	extern POINT gLastMousePos;

	
//--------------------------------------------------------------------------------------
// Global Variable
//--------------------------------------------------------------------------------------

	extern KeyboardClass gKeyboard;
	extern MouseClass gMouse;

//--------------------------------------------------------------------------------------
// Global Resource Managers
//--------------------------------------------------------------------------------------
	extern ModelManager     gModelManager;
	extern CharacterManager gCharacterManager;

//--------------------------------------------------------------------------------------
// Global D3D App Resource
//--------------------------------------------------------------------------------------
	extern Camera gCamera_main;
	extern Lights gLights;

	extern IMesh* gSelectedMesh;

};
