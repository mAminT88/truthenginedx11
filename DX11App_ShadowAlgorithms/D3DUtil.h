#pragma once

#include "XMathSerialization.h"
#include "MathHelper.h"


//---------------------------------------------------------------------------------------
// Convenience macro for releasing COM objects.
//---------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------------
// Convenience macro for releasing COM objects.
//---------------------------------------------------------------------------------------

#define ReleaseCOM(x) { if(x){ x->Release(); x = nullptr; } }

#define ThrowIfFailed(hr){ if (FAILED(hr)) { throw std::exception(); } }

using VectorString = std::vector<std::string>;
using VectorFloat = std::vector<float>;
using VectorSRV = std::vector<ID3D11ShaderResourceView*>;
using VectorRTV = std::vector<ID3D11RenderTargetView*>;



namespace Convertors {
	static DirectX::XMVECTOR StringToXMVector(std::string _value)
	{
		std::stringstream val(_value);
		VectorFloat v_float;
		std::string str;

		while (std::getline(val, str, ','))
		{
			v_float.push_back(std::stof(str));
		}

		return DirectX::XMVectorSet(v_float[0], v_float[1], v_float[2], v_float[3]);
	}

	static DirectX::XMFLOAT4 StringToXMFLOAT4(std::string _value)
	{
		std::stringstream val(_value);
		VectorFloat v_float;
		std::string str;

		while (std::getline(val, str, ','))
		{
			v_float.push_back(std::stof(str));
		}

		return DirectX::XMFLOAT4(v_float[0], v_float[1], v_float[2], v_float[3]);
	}

	static DirectX::XMFLOAT3 StringToXMFLOAT3(std::string _value)
	{
		std::stringstream val(_value);
		VectorFloat v_float;
		std::string str;

		while (std::getline(val, str, ','))
		{
			v_float.push_back(std::stof(str));
		}

		return DirectX::XMFLOAT3(v_float[0], v_float[1], v_float[2]);
	}

	static DirectX::XMFLOAT2 StringToXMFLOAT2(std::string _value)
	{
		std::stringstream val(_value);
		VectorFloat v_float;
		std::string str;

		while (std::getline(val, str, ','))
		{
			v_float.push_back(std::stof(str));
		}

		return DirectX::XMFLOAT2(v_float[0], v_float[1]);
	}

	static float StringToFloat(std::string _value)
	{
		std::stringstream val(_value);
		VectorFloat v_float;
		std::string str;

		while (std::getline(val, str, ','))
		{
			v_float.push_back(std::stof(str));
		}

		return v_float[0];
	}

	static std::string XM_CALLCONV XMVectorToString(DirectX::FXMVECTOR _value)
	{
		return "x: " + std::to_string(DirectX::XMVectorGetX(_value)) + "\ty: " + std::to_string(DirectX::XMVectorGetY(_value)) + "\tz: " + std::to_string(DirectX::XMVectorGetZ(_value)) + "\tw: " + std::to_string(DirectX::XMVectorGetW(_value));
	}

	static std::string XMFLOAT4ToString(DirectX::XMFLOAT4 _value)
	{
		return "x: " + std::to_string(_value.x) + "\ty: " + std::to_string(_value.y) + "\tz: " + std::to_string(_value.z) + "\tw: " + std::to_string(_value.w);
	}

	static std::string XMFLOAT3ToString(DirectX::XMFLOAT3 _value)
	{
		return "x: " + std::to_string(_value.x) + "\ty: " + std::to_string(_value.y) + "\tz: " + std::to_string(_value.z);
	}

	static std::string XMFLOAT2ToString(DirectX::XMFLOAT2 _value)
	{
		return "x: " + std::to_string(_value.x) + "\ty: " + std::to_string(_value.y);
	}

	static std::string FloatToString(float _value)
	{
		std::ostringstream ss;
		ss << _value;
		std::string s(ss.str());
		return s;
	}

}

static inline const void SetNullRTVDSV(ID3D11DeviceContext* context, const int RTVNum = 1)
{
	static  ID3D11RenderTargetView* const nullRtv[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };

	context->OMSetRenderTargets(RTVNum, nullRtv, nullptr);
};


static inline const void SetNullUAV_CS(ID3D11DeviceContext* context, const int UAVNum = 1, const int StartSlot = 0)
{
	static  ID3D11UnorderedAccessView* const nullRtv[] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };

	context->CSSetUnorderedAccessViews(StartSlot, UAVNum, nullRtv, nullptr);
};