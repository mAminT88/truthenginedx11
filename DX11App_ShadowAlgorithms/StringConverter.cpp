#include "stdafx.h"
#include "StringConverter.h"

std::wstring StringConverter::StringToWide(std::string str)
{
	std::wstring output(str.begin(), str.end());
	return output;
}
