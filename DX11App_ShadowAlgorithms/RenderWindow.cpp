#include "stdafx.h"
#include "WindowContainer.h"
#include "RenderWindow.h"
#include "StringConverter.h"
#include "ErrorLogger.h"

bool RenderWindow::Initialize(WindowContainer* pWindowContainer, HINSTANCE hInstance, std::string window_title, std::string window_class, int width, int heigth)
{
	this->m_hInstance = hInstance;
	this->m_width = width;
	this->m_heigth = heigth;
	this->m_window_title = window_title;
	this->m_window_title_wide = StringConverter::StringToWide(window_title);
	this->m_window_class = window_class;
	this->m_window_class_wide = StringConverter::StringToWide(window_class);

	this->RegisterWindowClass();

	RECT wr = { 0, 0, width, heigth};
	AdjustWindowRect(&wr, WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU, FALSE);

	this->m_handle = CreateWindowEx(0
		, m_window_class_wide.c_str()
		, m_window_title_wide.c_str()
		, WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU
		, CW_USEDEFAULT
		, CW_USEDEFAULT
		, wr.right - wr.left
		, wr.bottom - wr.top
		, NULL
		, NULL
		, m_hInstance
		, pWindowContainer
	);

	if (m_handle == NULL)
	{
		ErrorLogger::Log(GetLastError(), ("CreateWindowEX Failed for window: " + m_window_title) );
		return false;
	}

	ShowWindow(m_handle, SW_SHOW);
	SetForegroundWindow(m_handle);
	SetFocus(m_handle);

	return true;

}

bool RenderWindow::ProcessMessages()
{
	MSG msg = { 0 };

	if (PeekMessage(&msg,
		m_handle,
		0,
		0,
		PM_REMOVE
	))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	if (msg.message == WM_NULL)
	{
		if (!IsWindow(m_handle))
		{
			m_handle = NULL;
			UnregisterClass(m_window_class_wide.c_str(), m_hInstance);
			return false;
		}
	}

	return true;
}

HWND RenderWindow::GetHWND() const
{
	return m_handle;
}

RenderWindow::~RenderWindow()
{
	if (m_handle != NULL)
	{
		UnregisterClass(m_window_class_wide.c_str(), m_hInstance);
		DestroyWindow(m_handle);
	}
}

LRESULT CALLBACK HandleMessageRedirect(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{

	case WM_CLOSE:
	{
		DestroyWindow(hwnd);
		return 0;
	}

	default:
	{
		WindowContainer* pWindowContainer = reinterpret_cast<WindowContainer*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));
		return pWindowContainer->WindowProc(hwnd, uMsg, wParam, lParam);
	}

	}
}

LRESULT CALLBACK HandleMessageSetup(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	switch (uMsg)
	{
	case WM_NCCREATE:
	{
		const CREATESTRUCTW* const pCreate = reinterpret_cast<CREATESTRUCTW*>(lParam);
		WindowContainer* pWindowContainer = reinterpret_cast<WindowContainer*>(pCreate->lpCreateParams);
		if (pWindowContainer == nullptr)
		{
			ErrorLogger::Log("Critical Error: Pointer to window container is null during NCCREATE ");
			exit(-1);
		}
		SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pWindowContainer));
		SetWindowLongPtr(hwnd, GWLP_WNDPROC, reinterpret_cast<LONG_PTR>(HandleMessageRedirect));
		return pWindowContainer->WindowProc(hwnd, uMsg, wParam, lParam);
	}
	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}

}

void RenderWindow::RegisterWindowClass()
{
	WNDCLASSEX wc = { 0 };
	wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = HandleMessageSetup;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hInstance;
	wc.hIcon = NULL;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = NULL;
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_window_class_wide.c_str();
	wc.cbSize = sizeof(WNDCLASSEX);

	RegisterClassEx(&wc);
}
