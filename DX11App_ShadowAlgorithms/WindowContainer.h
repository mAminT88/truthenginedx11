#pragma once


//#include "RenderWindow.h"

class RenderWindow;

class WindowContainer
{
public:
	WindowContainer();
	virtual ~WindowContainer();

	WindowContainer(WindowContainer&&);
	WindowContainer& operator=(WindowContainer&&);

	virtual LRESULT WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
protected:
	std::unique_ptr<RenderWindow> m_renderWindow;
};

