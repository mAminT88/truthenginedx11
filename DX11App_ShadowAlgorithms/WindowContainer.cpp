#include "stdafx.h"
#include "WindowContainer.h"
#include "ErrorLogger.h"
#include "Globals.h"
#include "RenderWindow.h"

using namespace Globals;

WindowContainer::WindowContainer()
{
	static bool raw_input_initialized = false;
	if (raw_input_initialized == false)
	{
		RAWINPUTDEVICE rid;

		rid.usUsagePage = 0x01; //Mouse
		rid.usUsage = 0x02;
		rid.dwFlags = 0;
		rid.hwndTarget = NULL;

		if (RegisterRawInputDevices(&rid, 1, sizeof(rid) == FALSE))
		{
			ErrorLogger::Log(GetLastError(), "Failed to register raw input devices");
			exit(-1);
		}
	}
	raw_input_initialized = true;
}

WindowContainer::~WindowContainer() = default;

WindowContainer::WindowContainer(WindowContainer&&) = default;

WindowContainer& WindowContainer::operator=(WindowContainer&&) = default;

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

LRESULT WindowContainer::WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	if (ImGui_ImplWin32_WndProcHandler(hwnd, uMsg, wParam, lParam))
		return true;

	switch (uMsg)
	{

		//Keyboard Messages

	case WM_KEYDOWN:
	{
		auto c = static_cast<unsigned char>(wParam);
		if (gKeyboard.IsKeysAutoRepeat())
		{
			gKeyboard.OnKeyPressed(c);
		}
		else {
			const bool wasPressed = lParam & 0x40000000;
			if (!wasPressed)
				gKeyboard.OnKeyPressed(c);
		}

		return 0;
	}

	case WM_KEYUP:
	{

		auto c = static_cast<unsigned char>(wParam);

		gKeyboard.OnKeyReleased(c);

	}

	case WM_CHAR:
	{
		auto c = static_cast<unsigned char>(wParam);

		if (gKeyboard.IsCharsAutoRepeat())
		{
			gKeyboard.OnChar(c);
		}

		else {
			const bool wasPressed = lParam & 0x40000000;
			if (!wasPressed)
				gKeyboard.OnChar(c);
		}

		return 0;
	}

	//Mouse Messages

	case WM_MOUSEMOVE:
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);
		gMouse.OnMouseMove(x, y);
		return 0;
	}
	case WM_LBUTTONDOWN:
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);
		gMouse.OnLeftPressed(x, y);
		return 0;
	}
	case WM_LBUTTONUP:
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);
		gMouse.OnLeftReleased(x, y);
		return 0;
	}
	case WM_RBUTTONDOWN:
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);
		gMouse.OnRightPressed(x, y);
		return 0;
	}
	case WM_RBUTTONUP:
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);
		gMouse.OnRightReleased(x, y);
		return 0;
	}
	case WM_MBUTTONDOWN:
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);
		gMouse.OnMiddlePressed(x, y);
		return 0;
	}
	case WM_MBUTTONUP:
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);
		gMouse.OnMiddleReleased(x, y);
		return 0;
	}
	case WM_MOUSEWHEEL:
	{
		int x = LOWORD(lParam);
		int y = HIWORD(lParam);
		if (GET_WHEEL_DELTA_WPARAM(wParam) > 0)
			gMouse.OnWheelUp(x, y);
		else if (GET_WHEEL_DELTA_WPARAM(wParam) < 0)
			gMouse.OnWheelDown(x, y);

		return 0;
	}

	case WM_INPUT:
	{
		UINT dataSize;

		GetRawInputData(reinterpret_cast<HRAWINPUT>(lParam), RID_INPUT, NULL, &dataSize, sizeof(RAWINPUTHEADER));

		if (dataSize > 0)
		{

			std::unique_ptr<BYTE[]> rawdata = std::make_unique<BYTE[]>(dataSize);
			if (GetRawInputData(reinterpret_cast<HRAWINPUT>(lParam), RID_INPUT, rawdata.get(), &dataSize, sizeof(RAWINPUTHEADER)) == dataSize)
			{
				RAWINPUT* raw = reinterpret_cast<RAWINPUT*>(rawdata.get());
				if (raw->header.dwType == RIM_TYPEMOUSE)
				{
					gMouse.OnMouseMoveRaw(raw->data.mouse.lLastX, raw->data.mouse.lLastY);
				}
			}
		}

	}

	default:
		return DefWindowProc(hwnd, uMsg, wParam, lParam);
	}
}
