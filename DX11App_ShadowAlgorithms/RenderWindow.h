#pragma once


//#include "resource.h"
//#include "StringConverter.h"
//#include "ErrorLogger.h"
//#include "Globals.h"
//#include "StaticInstances.h"

class WindowContainer;

class RenderWindow
{
public:
	bool Initialize(WindowContainer* pWindowContainer, HINSTANCE hInstance, std::string window_title, std::string window_class, int width, int heigth);
	bool ProcessMessages();
	HWND GetHWND() const;
	~RenderWindow();
private:
	void RegisterWindowClass();
	HWND m_handle = NULL;
	HINSTANCE m_hInstance = NULL;
	std::string m_window_title = "";
	std::wstring m_window_title_wide = L"";
	std::string m_window_class = "";
	std::wstring m_window_class_wide = L"";
	int m_width = 0;
	int m_heigth = 0;
};
