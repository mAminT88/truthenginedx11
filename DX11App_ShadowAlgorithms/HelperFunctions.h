#pragma once

namespace HelperFunctions
{
	template <typename F, typename... Ts>
	inline auto runAsync(F&& f, Ts&& ... params) {
		return std::async(std::launch::async, std::forward<F>(f),
			std::forward<Ts>(params)...);
	}
}
