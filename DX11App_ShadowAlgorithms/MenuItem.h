#pragma once



class MenuItem
{

protected:
	HMENU m_menu;

public:
	void Init();
	void AddItem(std::wstring text, int id);
	void SetParent(HMENU& menu);

};