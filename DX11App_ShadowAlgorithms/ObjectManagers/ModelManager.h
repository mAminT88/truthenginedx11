#pragma once

#include "Objects/Model.h"

class Material;
class Texture;

class ModelManager
{
private:

	Model* pSelectedModel;

	std::unordered_map<std::string, Model*> mAllModels;

	std::vector<Model*> mBasicModels;
	std::vector<SkinnedModel*> mSkinnedModels;
	
	int ReleaseModels();




public:

	ModelManager();
	~ModelManager();
	ModelManager(ModelManager&&);
	ModelManager& operator = (ModelManager&&);


	std::vector<std::unique_ptr<Material>> gMaterials;
	std::map<std::string, std::unique_ptr<Texture>> gTextures;


	const std::vector<Model*>* GetBasicModels() const noexcept;
	const std::vector<SkinnedModel*>* GetSkinnedModels() const noexcept;

	int Update();

	int AddBasicModel(const std::string& modelPath);
	int AddSkinnedModel(const std::string& modelPath);

	int SaveModels(const std::string& outputPath);
	int LoadModels(const std::string& inputPath);

	//int InitBuffers();
	int InitBuffers_Basic32();
	int InitBuffers_Skinned();

	std::unordered_map<std::string, Model*>& GetAllModels() noexcept;
	void SelectModel(Model* selectedModel) noexcept;
	Model* GetSelectedModel() const noexcept;
	
};
