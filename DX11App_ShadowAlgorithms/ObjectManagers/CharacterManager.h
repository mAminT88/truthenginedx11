#pragma once


class Character;

class CharacterManager
{
private:
	std::vector<std::unique_ptr<Character>> mCharacters;

	Character* mUserCharacter;

public:
	CharacterManager();
	~CharacterManager();


	int AddCharacter(std::string& filePath);

	bool UpdateCharacters();

	std::vector<std::unique_ptr<Character>>* GetCharacters();

	Character* GetUserCharacter();
};