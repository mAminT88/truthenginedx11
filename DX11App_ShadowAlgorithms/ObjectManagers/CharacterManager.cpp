#include "stdafx.h"
#include "CharacterManager.h"
#include "Objects\Character.h"


CharacterManager::CharacterManager() = default;

CharacterManager::~CharacterManager() = default;

int CharacterManager::AddCharacter(std::string& filePath)
{
	mCharacters.emplace_back();

	/*if (!mCharacters.back().LoadModel(filePath))
		mCharacters.pop_back();*/

	return 0;
}

bool CharacterManager::UpdateCharacters()
{
	for (auto& ch : mCharacters)
	{
		ch->Update();
	}

	return true;
}

std::vector<std::unique_ptr<Character>>* CharacterManager::GetCharacters()
{
	return &mCharacters;
}

Character* CharacterManager::GetUserCharacter()
{
	return mUserCharacter;
}
