#include "stdafx.h"
#include "Globals.h"
#include "ModelManager.h"

#include "Graphics/Vertex.h"
#include "Graphics/Material.h"
#include "Graphics/Texture.h"

using namespace Globals;

int ModelManager::ReleaseModels()
{

	for (auto ptr : mBasicModels)
	{
		delete ptr;
	}

	for (auto ptr : mSkinnedModels)
	{
		delete ptr;
	}

	mBasicModels.clear();
	mBasicModels.shrink_to_fit();

	mSkinnedModels.clear();
	mSkinnedModels.shrink_to_fit();

	return 0;

}

ModelManager::ModelManager() : pSelectedModel(nullptr)
{

}

ModelManager::~ModelManager()
{
	ReleaseModels();
}

ModelManager::ModelManager(ModelManager&&) = default;

ModelManager& ModelManager::operator=(ModelManager&&) = default;

int ModelManager::AddBasicModel(const std::string& modelPath)
{

	boost::filesystem::path path(modelPath);

	auto m = new Model();

	m->mName = path.filename().string();

	mBasicModels.push_back(m);

	if (!m->LoadModel(modelPath))
	{
		delete m;
		mBasicModels.pop_back();

		return -1;
	}

	mAllModels[m->mName] = (Model*)m;

	InitBuffers_Basic32();

	return 0;
}

int ModelManager::AddSkinnedModel(const std::string& modelPath)
{
	boost::filesystem::path path(modelPath);

	auto sm = new SkinnedModel();

	mSkinnedModels.push_back(sm);

	sm->mName = path.filename().string();

	if (!sm->LoadModel(modelPath))
	{
		delete sm;
		mSkinnedModels.pop_back();

		return -1;
	}

	mAllModels[sm->mName] = (Model*)sm;

	InitBuffers_Skinned();

	return 0;
}

const std::vector<Model*>* ModelManager::GetBasicModels() const noexcept
{
	return &mBasicModels;
}

const std::vector<SkinnedModel*>* ModelManager::GetSkinnedModels() const noexcept
{
	return &mSkinnedModels;
}

int ModelManager::Update()
{
	for (auto sm : mSkinnedModels)
	{
		sm->Update();

		sm->SetAnimation(0);
		/*if (gKeyboard.KeyIsPressed('Q'))
		{
			sm->SetAnimation(0);
		}
		else {
			sm->DisableAnimation(0);
		}*/

	}

	return 0;
}

int ModelManager::SaveModels(const std::string& outputPath)
{
	std::ofstream bfout = std::ofstream(outputPath, std::ios::out | std::ios::binary);


	if (bfout.is_open())
	{

		boost::archive::binary_oarchive ao(bfout);

		/*ao& mBasicModels.size();

		for (auto model : mBasicModels)
		{
			model->SaveModel(ao);
		}*/

		ao& mBasicModels;

		return 0;
	}

	return -1;
}

int ModelManager::LoadModels(const std::string& inputPath)
{
	std::ifstream bfin(inputPath, std::ios::in | std::ios::binary);
	if (bfin.is_open())
	{
		boost::archive::binary_iarchive ai(bfin);

		/*size_t basic32ModelCount = 0;

		ai& basic32ModelCount;

		if (basic32ModelCount > 0)
		{
			ReleaseModels();
			for (size_t i = 0; i < basic32ModelCount; i++)
			{
				Model basic32Model;
				basic32Model.LoadModel(ai);

				mBasicModels.push_back(&basic32Model);
			}
		}*/
		ai& mBasicModels;
	}

	return 0;
}

//int ModelManager::InitBuffers()
//{
//		
//}

int ModelManager::InitBuffers_Basic32()
{
	//Generate Basic32 VertexBuffer

	D3D11_BUFFER_DESC desc = {  };
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.ByteWidth = gVertecies_Basic32.size() * sizeof(Vertex::Basic32);
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;
	desc.Usage = D3D11_USAGE_IMMUTABLE;

	D3D11_SUBRESOURCE_DATA data = { };
	data.pSysMem = gVertecies_Basic32.data();
	data.SysMemPitch = 0;
	data.SysMemSlicePitch = 0;

	gDevice->CreateBuffer(&desc, &data, gVertexBuffer_Basic32.ReleaseAndGetAddressOf());

	//Generate Index Buffer

	desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	desc.ByteWidth = gIndecies.size() * sizeof(UINT32);

	data.pSysMem = gIndecies.data();

	gDevice->CreateBuffer(&desc, &data, gIndexBuffer.ReleaseAndGetAddressOf());

	return 0;
}

int ModelManager::InitBuffers_Skinned()
{
	//Generate Skinned VertexBuffer

	D3D11_BUFFER_DESC desc = {  };
	desc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	desc.ByteWidth = gVertecies_Basic32.size() * sizeof(Vertex::Basic32);
	desc.CPUAccessFlags = 0;
	desc.MiscFlags = 0;
	desc.Usage = D3D11_USAGE_IMMUTABLE;

	D3D11_SUBRESOURCE_DATA data = { };
	data.pSysMem = gVertecies_Basic32.data();
	data.SysMemPitch = 0;
	data.SysMemSlicePitch = 0;

	desc.ByteWidth = gVertecies_Skinned.size() * sizeof(Vertex::Skinned);

	data.pSysMem = gVertecies_Skinned.data();

	gDevice->CreateBuffer(&desc, &data, gVertexBuffer_Skinned.ReleaseAndGetAddressOf());

	//Generate Index Buffer

	desc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	desc.ByteWidth = gIndecies.size() * sizeof(UINT32);

	data.pSysMem = gIndecies.data();

	gDevice->CreateBuffer(&desc, &data, gIndexBuffer.ReleaseAndGetAddressOf());

	return 0;
}

std::unordered_map<std::string, Model*>& ModelManager::GetAllModels() noexcept
{
	return mAllModels;
}

void ModelManager::SelectModel(Model* selectedModel) noexcept
{
	pSelectedModel = selectedModel;
}

Model* ModelManager::GetSelectedModel() const noexcept
{
	return pSelectedModel;
}
