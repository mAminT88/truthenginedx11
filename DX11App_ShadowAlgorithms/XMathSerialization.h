#pragma once



namespace boost
{
	namespace serialization
	{
		template<class Archive>
		void save(Archive& a, const XMFLOAT4& v, const unsigned int version)
		{
			a& v.x;
			a& v.y;
			a& v.z;
			a& v.w;
		}

		template<class Archive>
		void load(Archive& a, XMFLOAT4& v, const unsigned int version)
		{
			a& v.x;
			a& v.y;
			a& v.z;
			a& v.w;

		}

		template<class Archive>
		void serialize(Archive& a, XMFLOAT4& v, const unsigned int version)
		{
			split_free(a, v, version);
		}


		//the above function can be shortened by "BOOST_SERIALIZATION_SPLIT_MEMBER(XMFLOAT4);"

		template<class Archive>
		void serialize(Archive& a, XMFLOAT3& v, const unsigned int version)
		{
			a& v.x;
			a& v.y;
			a& v.z;
		}

		template<class Archive>
		void serialize(Archive& a, XMFLOAT2& v, const unsigned int version)
		{
			a& v.x;
			a& v.y;
		}

		template<class Archive>
		void serialize(Archive& a, XMFLOAT4X4& v, const unsigned int version)
		{
			a& v._11;
			a& v._12;
			a& v._13;
			a& v._14;
			a& v._21;
			a& v._22;
			a& v._23;
			a& v._24;
			a& v._31;
			a& v._32;
			a& v._33;
			a& v._34;
			a& v._41;
			a& v._42;
			a& v._43;
			a& v._44;
		}

		template<class Archive>
		void serialize(Archive& a, XMFLOAT4X4A& v, const unsigned int version)
		{
			a& v._11;
			a& v._12;
			a& v._13;
			a& v._14;
			a& v._21;
			a& v._22;
			a& v._23;
			a& v._24;
			a& v._31;
			a& v._32;
			a& v._33;
			a& v._34;
			a& v._41;
			a& v._42;
			a& v._43;
			a& v._44;
		}
		
	}
}
