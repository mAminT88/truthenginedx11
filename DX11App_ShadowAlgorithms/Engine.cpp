#include "stdafx.h"
#include "Engine.h"
#include "Globals.h"

#include "HelperFunctions.h"

#include "Graphics/RenderEngine.h"
#include "RenderWindow.h"
#include "Objects/Model.h"

using namespace Globals;


Engine::Engine() = default;

Engine::~Engine() = default;

bool Engine::Initialize(HINSTANCE hInstance, std::string window_title, std::string window_class, int width, int heigth)
{

	m_renderWindow = std::make_unique<RenderWindow>();
	mRenderer = std::make_unique<RenderEngine>();

	m_renderWindow->Initialize(this, hInstance, window_title, window_class, width, heigth);
	gHWND_Rendering = m_renderWindow->GetHWND();

	mRenderer->InitDXDevice();

	std::string LevelPath;

	//concurrency::task_group tasksg;

	//tasksg.run([this]() {mRenderer->Init(this); });
	mRenderer->Init(this, gClientWidth, gClientHeight);

	const COMDLG_FILTERSPEC fileExtensions[] =
	{
		{L"Wave OBJ File (*.obj)", L"*.obj"},
		{L"Autodesk FBX File", L"*.fbx"},
		{L"Collada", L"*.dae"},
	};

	SelectFile(LevelPath, fileExtensions, 3);
	//gFuture_LoadingModel = HelperFunctions::runAsync(this->ImportModel, LevelPath);
	//tasksg.run([this, &LevelPath]() { ImportModel(LevelPath); });
	ImportModel(LevelPath);
	//gModelManager.AddSkinnedModel("Data/Models/Looking Around.fbx");

	//tasksg.wait();


	return true;
}

bool Engine::ProcessMessages()
{
	return m_renderWindow->ProcessMessages();
}

void Engine::Update()
{
	mRenderer->Update();

	gModelManager.Update();

	while (!gMouse.IsEventBufferEmpty())
	{
		auto mEvent = gMouse.PopEvent();

		gCamera_main.GetUserInputs(&mEvent, &gMouse, static_cast<float>(gMouse.GetDX()), static_cast<float>(gMouse.GetDY()));
	}

	while (!gKeyboard.KeyBufferIsEmpty())
	{
		auto kEvent = gKeyboard.PopKey();

		//gCamera_main.GetUserInputs(&kEvent, &gKeyboard, gDt);
	}
}

void Engine::Run()
{
	mRenderer->Draw();

}

void Engine::ImportModel(const std::string& filepath)
{
	gModelManager.AddBasicModel(filepath);
}

void Engine::ImportSkinnedModel(const std::string& filepath)
{

	gModelManager.AddSkinnedModel(filepath);

}

HRESULT Engine::SelectFile(std::string& filePath, const COMDLG_FILTERSPEC* c_rgSaveTypes, const  int extensionCount)
{

	/*const COMDLG_FILTERSPEC c_rgSaveTypes[] =
	{
		{L"Wave OBJ File (*.obj)",       L"*.obj"},
		{L"Autodesk FBX File",    L"*.fbx"},
	};*/

	// CoCreate the File Open Dialog object.
	IFileDialog* pfd = NULL;
	HRESULT hr = CoCreateInstance(CLSID_FileOpenDialog,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_PPV_ARGS(&pfd));
	if (SUCCEEDED(hr))
	{
		// Set the options on the dialog.
		DWORD dwFlags;

		// Before setting, always get the options first in order 
		// not to override existing options.
		hr = pfd->GetOptions(&dwFlags);
		if (SUCCEEDED(hr))
		{
			// In this case, get shell items only for file system items.
			hr = pfd->SetOptions(dwFlags | FOS_FORCEFILESYSTEM);
			if (SUCCEEDED(hr))
			{
				// Set the file types to display only. 
				// Notice that this is a 1-based array.
				hr = pfd->SetFileTypes(extensionCount, c_rgSaveTypes);
				if (SUCCEEDED(hr))
				{
					// Set the default extension to be ".doc" file.
					hr = pfd->SetDefaultExtension(L"obj");
					if (SUCCEEDED(hr))
					{
						// Show the dialog
						hr = pfd->Show(NULL);
						if (SUCCEEDED(hr))
						{
							// Obtain the result once the user clicks 
							// the 'Open' button.
							// The result is an IShellItem object.
							IShellItem* psiResult;
							hr = pfd->GetResult(&psiResult);
							if (SUCCEEDED(hr))
							{
								// We are just going to print out the 
								// name of the file for sample sake.
								PWSTR pszFilePath = NULL;
								hr = psiResult->GetDisplayName(SIGDN_FILESYSPATH,
									&pszFilePath);
								if (SUCCEEDED(hr))
								{
									std::wstring filePathW(pszFilePath);
									filePath = std::string(filePathW.begin(), filePathW.end());
								}
								psiResult->Release();
							}
						}
					}

				}
			}
		}

		pfd->Release();
	}
	return hr;
}
//
//void Engine::SaveModels(const std::string& filePath)
//{
//	std::ofstream bfout = std::ofstream(filePath, std::ios::out | std::ios::binary);
//	boost::archive::binary_oarchive ao(bfout);
//
//	ao& mBasicModels;
//}
//
//void Engine::LoadModels(const std::string& filePath)
//{
//	ReleaseModels();
//
//	std::ifstream bfin(filePath, std::ios::in | std::ios::binary);
//	if (bfin.is_open())
//	{
//		boost::archive::binary_iarchive ai(bfin);
//
//		ai& mBasicModels;
//	}
//}
//
//bool Engine::ReleaseModels()
//{
//	for (auto ptr : mBasicModels)
//	{
//		delete ptr;
//	}
//
//	for (auto ptr : mSkinnedModels)
//	{
//		delete ptr;
//	}
//
//	mBasicModels.clear();
//
//	mSkinnedModels.clear();
//
//	return true;
//}
