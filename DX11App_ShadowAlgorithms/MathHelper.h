#pragma once
 


class MathHelper
{
public:

	static const XMFLOAT4X4A IdentityMatrix;

	// Returns random float in [0, 1).
	static float RandF()
	{
		return (float)(rand()) / (float)RAND_MAX;
	}

	// Returns random float in [a, b).
	static float RandF(float a, float b)
	{
		return a + RandF() * (b - a);
	}

	static XMMATRIX XM_CALLCONV InverseTranspose(FXMMATRIX M)
	{
		// Inverse-transpose is just applied to normals.  So zero out 
		// translation row so that it doesn't get into our inverse-transpose
		// calculation--we don't want the inverse-transpose of the translation.
		XMMATRIX A = M;
		A.r[3] = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);

		XMVECTOR det = XMMatrixDeterminant(A);
		return XMMatrixTranspose(XMMatrixInverse(&det, A));
	}

	static XMFLOAT4X4A InverseTranspose(XMFLOAT4X4A const * M)
	{
		// Inverse-transpose is just applied to normals.  So zero out 
		// translation row so that it doesn't get into our inverse-transpose
		// calculation--we don't want the inverse-transpose of the translation.
		XMMATRIX A = XMLoadFloat4x4(M);
		A.r[3] = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);

		XMVECTOR det = XMMatrixDeterminant(A);
		XMFLOAT4X4A r;
		XMStoreFloat4x4A(&r, XMMatrixTranspose(XMMatrixInverse(&det, A)));
		return r;
	}

	static BoundingFrustum XM_CALLCONV CreateBoundingFrustum(const XMMATRIX& projection_matrix, const XMFLOAT4X4& view_matrix)
	{
		BoundingFrustum bf;
		
		BoundingFrustum::CreateFromMatrix(bf, projection_matrix);
		
		const auto InvView = XMMatrixInverse(nullptr, XMLoadFloat4x4(&view_matrix));

		bf.Transform(bf, InvView);

		return bf;
	}

	static XMFLOAT4 XM_CALLCONV CastXVectorToXFloat4(FXMVECTOR v)
	{
		XMFLOAT4 xf;
		XMStoreFloat4(&xf, v);
		return xf;
	}

	static float AspectRatio(UINT _width, UINT _height)
	{
		return static_cast<float>(_width) / _height;
	}

	static void PackValue(XMFLOAT4A& _v, float value, int index)
	{
		switch (index)
		{
		case 0:
			_v.x = value;
			break;
		case 1:
			_v.y = value;
			break;
		case 2:
			_v.z = value;
			break;
		case 3:
			_v.w = value;
			break;
		default:
			break;
		}
	}
};
