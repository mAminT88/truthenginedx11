#include "stdafx.h"
#include "GameTimer.h"

GameTimer::GameTimer() : mSecondsPerCount(0.0), mDeltaTime(-1.0), mBaseTime(0), mPrevTime(0), mCurrTime(0), mPausedTime(0), mStoppedTime(0), mStopped(false)
{
	__int64 countsPerSecond;
	QueryPerformanceFrequency((LARGE_INTEGER*)&countsPerSecond);
	mSecondsPerCount = 1.0 / (double)countsPerSecond;
}

float GameTimer::TotalTime()const
{
	if (mStopped)
	{
		return ((mStoppedTime - mPausedTime) - mBaseTime) * mSecondsPerCount;
	}
	else
	{
		return ((mCurrTime - mPausedTime) - mBaseTime) * mSecondsPerCount;
	}
}

float GameTimer::DeltaTime()const
{
	return mDeltaTime;
}

void GameTimer::Reset()
{
	__int64 currTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

	mBaseTime = currTime;
	mPrevTime = currTime;
	mStoppedTime = 0;
	mStopped = false;
}

void GameTimer::Start()
{
	__int64 startTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&startTime);

	if (mStopped) {
		mPausedTime += (startTime - mStoppedTime);

		mPrevTime = startTime;
		mStoppedTime = 0;
		mStopped = false;
	}
}

void GameTimer::Stop()
{
	if(!mStopped)
	{
		__int64 currTime;
		QueryPerformanceCounter((LARGE_INTEGER*)&currTime);

		mStoppedTime = currTime;
		mStopped = true;
	}
}

void GameTimer::Tick()
{
	if (mStopped)
	{
		mDeltaTime = 0.0;
		return;
	}

	__int64 currTime;
	QueryPerformanceCounter((LARGE_INTEGER*)&currTime);
	mCurrTime = currTime;

	mDeltaTime = (mCurrTime - mPrevTime) * mSecondsPerCount;

	mPrevTime = mCurrTime;

	if (mDeltaTime < 0.0)
	{
		mDeltaTime = 0.0;
	}
}

void GameTimer::CalculateFrameStats(HWND mainWindow, std::wstring mainWindowCaption)
{
	static int frameCount = 0;
	static float timeElapsed = 0.0f;

	frameCount++;

	float tTime = TotalTime();

	if ((tTime - timeElapsed) >= 1.0f )
	{
		float fps = (float)frameCount;
		float mspf = 1000.0f / frameCount;

		std::wostringstream outs;
		outs << mainWindowCaption << L"    "
			<< L"FPS: " << fps << L"    "
			<< L"Frame Time: " << mspf << L" (ms)";
		SetWindowText(mainWindow, outs.str().c_str());

		frameCount = 0;
		timeElapsed += 1.0f;
	}
}