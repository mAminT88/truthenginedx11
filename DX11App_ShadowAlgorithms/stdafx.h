// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory>
#include <tchar.h>
#include <string>
#include <fstream>
#include <sstream>
#include <WindowsX.h>
#include <vector>
#include <stdint.h>
#include <iostream> // cin
#include <io.h>	// create console
#include <fcntl.h> //_O_Text
//#include <ShObjIdl.h> // open file dialog
#include <conio.h> //clear console clrscr()
#include <wrl.h> //ComPtr
#include <comdef.h>
#include <wincodec.h> // WIC Texture Formats (i.e GUID_ContainerFormatJpeg)
#include <tuple>
#include <functional> //std::function
#include <map> //std::map
#include <unordered_map> //std::unordered_map
#include <future> //std::async
#include <ppl.h> //concurrency::task_group
#include <ppltasks.h>// concurrency::task<T>

//Window Common Item Dialog
#include <shlobj.h>
#include <objbase.h>      // For COM headers
#include <shobjidl.h>     // for IFileDialogEvents and IFileDialogControlEvents
#include <shlwapi.h>
#include <knownfolders.h> // for KnownFolder APIs/datatypes/function headers
#include <propvarutil.h>  // for PROPVAR-related functions
#include <propkey.h>      // for the Property key APIs/datatypes
#include <propidl.h>      // for the Property System APIs
#include <strsafe.h>      // for StringCchPrintfW
#include <shtypes.h> // for COMDLG_FILTERSPEC

//Boost Library
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/binary_object.hpp>
#include "boost/serialization/vector.hpp"
#include "boost/serialization/export.hpp"
#include "boost/archive/binary_iarchive.hpp"
#include "boost/archive/binary_oarchive.hpp"
#include "boost/archive/text_iarchive.hpp"
#include "boost/archive/text_oarchive.hpp"
#include "boost/algorithm/string.hpp"
#include "boost/filesystem.hpp"

//DirectX Header Files
#include <d3d11_2.h>
#include <d3d11.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXColors.h> // DirectX::Colors
#include <d3dcompiler.h>
#include <d2d1.h>
#include <d2d1_1.h>
#include <dwrite.h>
#include <DirectXCollision.h>
#include <DXGItype.h>
#include <dxgi1_2.h>
#include <dxgi1_3.h>
#include <DXProgrammableCapture.h>
#include <HelperComponent/DirectXTex/DirectXTexP.h>

//Assimp Importer Includes
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>

//ImGUI
#include <ImGui/imgui.h>
#include <ImGui/imgui_stdlib.h>
#include <ImGui/imgui_impl_win32.h>
#include <ImGui/imgui_impl_dx11.h>



// TODO: reference additional headers your program requires here


using namespace DirectX;
using Microsoft::WRL::ComPtr;
